import type { RequestHandler } from 'express';

export const noCache: RequestHandler = (request: any, response, next) => {
  if (request.method !== 'GET') return next();

  response.setHeader(
    'Cache-Control',
    'no-cache, no-store, must-revalidate, proxy-revalidate'
  );
  response.setHeader('Pragma', ' no-cache');
  response.setHeader('Expires', '0');
  return next();
};
