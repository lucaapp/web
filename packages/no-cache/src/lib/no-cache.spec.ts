import type { Request, Response } from 'express';
import { noCache } from './no-cache';

const getRequest = {
  method: 'GET',
} as Request;

const postRequest = {
  method: 'POST',
} as Request;

const mockSetHeader = jest.fn();

const mockResponse = ({
  setHeader: mockSetHeader,
} as unknown) as Response;

const mockNext = jest.fn();

describe('noCache', () => {
  it('should set cache control headers on GET request', () => {
    noCache(getRequest, mockResponse, mockNext);
    expect(mockNext).toHaveBeenCalledTimes(1);
    expect(mockSetHeader).toHaveBeenCalledTimes(3);
    expect(mockSetHeader).toHaveBeenCalledWith(
      'Cache-Control',
      'no-cache, no-store, must-revalidate, proxy-revalidate'
    );
    expect(mockSetHeader).toHaveBeenCalledWith('Pragma', ' no-cache');
    expect(mockSetHeader).toHaveBeenCalledWith('Expires', '0');
  });

  it('should skip POST requests', () => {
    noCache(postRequest, mockResponse, mockNext);
    expect(mockSetHeader).not.toHaveBeenCalled();
    expect(mockNext).toHaveBeenCalledTimes(1);
  });
});
