#!/bin/sh
set -euxo pipefail
yarn run lint
yarn run ts:check
yarn run test:ci
yarn run audit
