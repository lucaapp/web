module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  displayName: 'no-cache',
  rootDir: './',
  resetMocks: true,
  globals: { 'ts-jest': { tsconfig: 'tsconfig.spec.json' } },
  reporters: [
    'default',
    [
      'jest-junit',
      {
        classNameTemplate: '{suitename}',
        titleTemplate: '{classname} {title}',
        addFileAttribute: 'true',
        filePathPrefix: 'packages/no-cache',
      },
    ],
  ],
  collectCoverageFrom: [
    '<rootDir>/src/**/*.{js,ts}',
    '!<rootDir>/src/index.ts',
  ],
  coverageDirectory: '<rootDir>/coverage',
  coverageReporters: ['lcov', 'text', 'cobertura'],
  coverageThreshold: {
    global: {
      statements: 100,
      branches: 100,
      lines: 100,
      functions: 100,
    },
  },
};
