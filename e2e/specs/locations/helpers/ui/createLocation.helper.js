import { type } from '../../../utils/commands';
import { getByCy, getDOMElement } from '../../../utils/selectors';
import {
  AREA_TYPE_OPTION_INDOOR,
  ENTRY_POLICY_OPTION_2G,
  ID_CITY,
  ID_STREET_NAME,
  ID_STREET_NUMBER,
  ID_ZIP_CODE,
  MEDICAL_MASKS_OPTION_YES,
  ROOM_DEPTH,
  ROOM_HEIGHT,
  ROOM_WIDTH,
  SELECT_AREA_TYPE,
  SELECT_ENTRY_POLICY_INFO,
  SELECT_MASKS,
  SELECT_VENTILATION,
  VENTILATION_OPTION_ELECTRIC,
} from '../../constants/selectorKeys';

export const typeManualAddress = (streetName, streetNr, zipCode, city) => {
  type(getDOMElement(ID_STREET_NAME).focus(), streetName);
  type(getDOMElement(ID_STREET_NUMBER).focus(), streetNr);
  type(getDOMElement(ID_ZIP_CODE).focus(), zipCode);
  type(getDOMElement(ID_CITY).focus(), city);
};

export const setAreaDetails = () => {
  getByCy(SELECT_AREA_TYPE).click();
  getByCy(AREA_TYPE_OPTION_INDOOR).click();
  getByCy(SELECT_MASKS).click();
  getByCy(MEDICAL_MASKS_OPTION_YES).click();
  getByCy(SELECT_ENTRY_POLICY_INFO).click();
  getByCy(ENTRY_POLICY_OPTION_2G).click();
  getByCy(SELECT_VENTILATION).click();
  getByCy(VENTILATION_OPTION_ELECTRIC).click();
};

export const typeRoomSize = (roomWidth, roomDepth, roomHeight) => {
  type(getByCy(ROOM_WIDTH).focus(), roomWidth);
  type(getByCy(ROOM_DEPTH).focus(), roomDepth);
  type(getByCy(ROOM_HEIGHT).focus(), roomHeight);
};
