import { readFile } from '../../../utils/commands';
import { shouldBeChecked, shouldBeVisible } from '../../../utils/assertions';
import { getByCy, getDOMElement } from '../../../utils/selectors';
import {
  NEXT_BUTTON,
  INPUT_TYPE_FILE,
  DOWNLOAD_PRIVATE_KEY_BUTTON,
  PRIVATE_KEY_DOWNLOADED_CHECKBOX,
} from '../../constants/selectorKeys';

export const downloadLocationPrivateKeyFile = () => {
  getByCy(DOWNLOAD_PRIVATE_KEY_BUTTON).click();
  getByCy(PRIVATE_KEY_DOWNLOADED_CHECKBOX).check();
  shouldBeChecked(getByCy(PRIVATE_KEY_DOWNLOADED_CHECKBOX));
  shouldBeVisible(getByCy(NEXT_BUTTON)).click();
};

export const uploadLocationPrivateKeyFile = (filename, name) => {
  readFile(filename).then(fileContent => {
    getDOMElement(INPUT_TYPE_FILE, 10000).attachFile({
      fileContent,
      mimeType: 'text/plain',
      fileName: name,
    });
  });
};
