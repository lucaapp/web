import { getDOMElement } from '../../../utils/selectors';
import { shouldBeVisible } from '../../../utils/assertions';
import {
  ANT_NOTIFICATION_NOTICE_WITH_ICON,
  ERROR_ICON,
} from '../../constants/selectorKeys';
import { DEFAULT_LOCATION_TITLES_DE_EN } from '../../constants/locations';

export const verifyLinkOpensInNewTab = element => {
  element.then(link => {
    cy.request(link.prop('href')).its('status').should('eq', 200);
  });
};

export const antErrorModalAppears = () => {
  shouldBeVisible(getDOMElement(ANT_NOTIFICATION_NOTICE_WITH_ICON));
  shouldBeVisible(getDOMElement(ERROR_ICON));
};

export const verifyDefaultLocationTitle = selector => {
  selector.invoke('text').then(headline => {
    expect(headline).to.be.oneOf(DEFAULT_LOCATION_TITLES_DE_EN);
  });
};
