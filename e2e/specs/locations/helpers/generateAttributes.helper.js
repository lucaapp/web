import faker from 'faker';

export const getGroupAttributes = (name = null, type = null) => ({
  type: type || 'restaurant',
  name: name || faker.company.companyName().replace("'", ' '),
  phone: '017612345678',
  tableCount: 12,
  radius: 100,
  streetName: 'Charlottenstraße',
  streetNr: '59',
  zipCode: '10117',
  city: 'Berlin',
  roomWidth: '111',
  roomDepth: '222',
  roomHeight: '333',
});

export const getAreaAttributes = (name = null) => ({
  name: name || faker.company.companyName().replace("'", ' '),
  phone: '017612345678',
});
