const getQrCodePrefix = hasTable =>
  hasTable ? './downloads/luca_QRCodes_' : './downloads/luca_QRCode_';

const getQrCodeEnding = (hasTable, format) =>
  hasTable ? `_Tables.${format}` : `.${format}`;

// format according to file generator
const formatName = name => name.replaceAll(' ', '_');

export const getQrCodeFilePath = ({
  locationName,
  isArea = false,
  areaName = '',
  hasTable = false,
  format = 'pdf',
}) =>
  isArea
    ? `${getQrCodePrefix(hasTable)}${formatName(locationName)}_${formatName(
        areaName
      )}${getQrCodeEnding(hasTable, format)}`
    : `${getQrCodePrefix(hasTable)}${formatName(locationName)}${getQrCodeEnding(
        hasTable,
        format
      )}`;
