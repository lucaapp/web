// eslint-disable-next-line require-await
export async function getMe() {
  // eslint-disable-next-line require-await
  cy.request('GET', '/api/v4/auth/operator/me').then(async response => {
    cy.wrap(response.body).as('operator');
    return response;
  });
}
