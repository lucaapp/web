import { EC_KEYPAIR_GENERATE, hexToBase64 } from '@lucaapp/crypto';
import {
  STANDARD_PASSWORD,
  createOperatorValues,
  createOperator,
  storePublicKey,
  login,
  activateOperator,
  getMe,
} from './createOperator.helper';
import { APP_ROUTE } from '../constants/routes';

import {
  downloadLocationPrivateKeyFile,
  uploadLocationPrivateKeyFile,
} from '../helpers/ui/handlePrivateKeyFile';
import { getByCy } from '../../utils/selectors';
import { visit } from '../../utils/commands';

export const createTestOperator = async (
  withPrivateKey = false,
  route = APP_ROUTE
) => {
  cy.window().then(win => {
    win.sessionStorage.removeItem('PRIVATE_KEY_MODAL_SEEN');
  });
  // Create a new operator
  const newOperator = createOperatorValues();
  createOperator(newOperator);
  // Activate account
  activateOperator({
    email: newOperator.email,
  });
  // Login
  login({ username: newOperator.email, password: STANDARD_PASSWORD });
  // Get operator
  const testOperator = await getMe();

  if (withPrivateKey) {
    visit(route);
    downloadLocationPrivateKeyFile();
    const KEY_PATH = `./downloads/luca_locations_${testOperator.firstName}_${testOperator.lastName}_privateKey.luca`;
    const KEY_NAME = `luca_locations_${testOperator.firstName}_${testOperator.lastName}_privateKey.luca`;
    uploadLocationPrivateKeyFile(KEY_PATH, KEY_NAME);
    getByCy('finish', 4000).click();

    return { ...testOperator, keyName: KEY_NAME };
  }
  // Create key pair
  const keyPair = EC_KEYPAIR_GENERATE();
  // Store public key in order to not download it on every test
  storePublicKey({ publicKey: hexToBase64(keyPair.publicKey) });
  visit(route);
  return {
    ...testOperator,
    publicKey: hexToBase64(keyPair.publicKey),
  };
};

export const createBasicOperator = () => {
  // Create a new operator
  const newOperator = createOperatorValues();
  createOperator(newOperator);
  // Activate account
  activateOperator({
    email: newOperator.email,
  });
  return newOperator;
};
