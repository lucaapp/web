import faker from 'faker';

export const getGroupPayload = ({
  name = null,
  tableCount = null,
  zipCode = null,
} = {}) => ({
  type: 'store',
  name: name || faker.company.companyName().replace("'", ' '),
  phone: '017612345678',
  streetName: faker.address.streetName().replace("'", ' '),
  streetNr: faker.datatype.number().toString(),
  zipCode: zipCode || faker.address.zipCode('#####'),
  city: faker.address.city().replace("'", ' '),
  isIndoor: true,
  lat: Number(faker.address.latitude()),
  lng: Number(faker.address.longitude()),
  tableCount,
});

export const getAreaPayload = ({
  groupId,
  locationName = null,
  tableCount = null,
} = {}) => ({
  groupId,
  locationName: locationName || faker.company.companyName().replace("'", ' '),
  type: 'restaurant',
  phone: '017612345678',
  streetName: faker.address.streetName().replace("'", ' '),
  streetNr: faker.datatype.number().toString(),
  zipCode: faker.address.zipCode('#####'),
  city: faker.address.city().replace("'", ' '),
  isIndoor: true,
  tableCount,
});
