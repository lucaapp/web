import { getAreaPayload } from './createLocations.helper';

export const createTestGroup = groupValues => {
  return new Cypress.Promise(resolve => {
    // eslint-disable-next-line require-await
    cy.request('POST', 'api/v3/locationGroups', groupValues).then(
      // eslint-disable-next-line require-await
      async response => {
        const group = response.body;
        // eslint-disable-next-line promise/no-nesting
        cy.request(
          'GET',
          `api/v3/operators/locations/${group.location.locationId}`
        ).then(
          // eslint-disable-next-line require-await, no-shadow
          async response => {
            const location = response.body;
            resolve(location);
          }
        );
      }
    );
  });
};

export const createTestGroups = groupPayloads =>
  Cypress.Promise.all(
    groupPayloads.map(groupPayload => createTestGroup(groupPayload))
  );

export const createTestArea = (groupId, locationName) => {
  return new Cypress.Promise(resolve => {
    cy.request(
      'POST',
      'api/v3/operators/locations',
      getAreaPayload({ groupId, locationName })
      // eslint-disable-next-line require-await
    ).then(async response => {
      const newArea = response.body;
      resolve(newArea);
    });
  });
};
