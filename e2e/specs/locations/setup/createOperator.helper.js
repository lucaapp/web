import faker from 'faker';
import { E2E_INTERNAL_ACCESS_TOKEN } from '../../utils/tokens';

// The password is used for testing purpose only
export const STANDARD_PASSWORD = '5@4vKJDJwvWBFi4R';

export const STANDARD_PHONE = '0150 00123123';

export const createOperatorValues = () => ({
  firstName: faker.name.firstName().replace("'", ' '),
  lastName: faker.name.lastName().replace("'", ' '),
  email: faker.internet.email(),
  password: STANDARD_PASSWORD,
  agreement: true,
  avvAccepted: true,
  lastVersionSeen: '1.0.0',
  lang: 'de',
  phone: STANDARD_PHONE,
  businessEntityName: 'Nexenio',
  businessEntityCity: 'Berlin',
  businessEntityStreetName: 'Charlottenstraße',
  businessEntityStreetNumber: '59',
  businessEntityZipCode: '10117',
});

export const DEFAULT_COOKIE = '__Secure-connect.sid';

const { baseUrl } = Cypress.config();

// eslint-disable-next-line require-await
export async function createOperator(operatorValues) {
  cy.request('POST', '/api/v3/operators', operatorValues).then(
    // eslint-disable-next-line require-await
    async response => {
      // eslint-disable-next-line require-await
      cy.wrap(response.body).as('newOperator');
      return response;
    }
  );
}

// eslint-disable-next-line require-await
export async function storePublicKey(publicKey) {
  cy.request('POST', '/api/v3/operators/publicKey', publicKey).then(
    response => response
  );
}

// eslint-disable-next-line require-await
export async function activateOperator(user) {
  cy.request({
    method: 'POST',
    url: `${baseUrl}/api/internal/end2end/activateOperator`,
    headers: {
      'internal-access-authorization': E2E_INTERNAL_ACCESS_TOKEN,
    },
    body: user,
  }).then(response => response);
}

// eslint-disable-next-line require-await
export async function login(credentials) {
  cy.request({
    method: 'POST',
    url: 'api/v4/auth/operator/login',
    failOnStatusCode: false,
    body: credentials,
    headers: {
      origin: baseUrl,
    },
  });
}

export const getMe = () => {
  return new Cypress.Promise(resolve => {
    // eslint-disable-next-line require-await
    cy.request('GET', '/api/v4/auth/operator/me').then(async response => {
      const me = response.body;
      resolve(me);
    });
  });
};
