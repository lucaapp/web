import { createTestOperator } from '../../setup/createOperator';
import { createTestGroup } from '../../setup/createLocations';
import { getGroupPayload } from '../../setup/createLocations.helper';
import { DEFAULT_COOKIE } from '../../setup/createOperator.helper';
import {
  deleteFileIfExists,
  readFile,
  visit,
  triggerEvent,
} from '../../../utils/commands';
import { APP_ROUTE } from '../../constants/routes';
import { getByCy, getDOMElement } from '../../../utils/selectors';
import {
  ANT_COLLAPSE_ITEM_ACTIVE,
  AREA_CHECKED,
  DIV_ARIA_EXPANDED_TRUE,
  LOCATION_GENERATE_QR_CODES,
  QR_CODE_FOR_ENTIRE_AREA_SECTION,
  QR_CODE_FOR_ENTIRE_AREA_TOGGLE,
  QR_CODE_COMPATABILITY_SECTION,
  QR_CODE_COMPATABILITY_TOGGLE,
  QR_CODE_COMPATABILITY_INFO_ICON,
  ANT_TOOLTIP_OPEN,
  QR_CODES_CSV_INFO_ICON,
  QR_PRINT_SECTION,
  QR_PRINT_LINK,
  QR_CODE_DOWNLOAD_BUTTON_CSV,
  QR_CODE_DOWNLOAD_BUTTON_PDF,
  GENERATE_QR_CODES_TITLE,
  ANT_CLOSE_MODAL_ICON,
} from '../../constants/selectorKeys';
import {
  shouldBeDisabled,
  shouldBeVisible,
  shouldHaveAttribute,
  shouldNotBeDisabled,
  shouldNotExist,
} from '../../../utils/assertions';
import { getQrCodeFilePath } from '../../helpers/generateQrCodes.helper';
import { verifyLinkOpensInNewTab } from '../../helpers/ui/validations';

let testGroup;
let qrCodeEntireAreaPdf;
let qrCodeEntireAreaCsv;

describe(
  'Dashboard Overview/Generate QR codes for area',
  { retries: 1 },
  () => {
    before(() => {
      createTestOperator();
      createTestGroup(getGroupPayload({ name: 'AAA' })).then(group => {
        testGroup = group;
        qrCodeEntireAreaPdf = getQrCodeFilePath({
          locationName: testGroup.groupName,
        });
        qrCodeEntireAreaCsv = getQrCodeFilePath({
          locationName: testGroup.groupName,
          format: 'csv',
        });
      });
    });
    beforeEach(() => {
      Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
      visit(APP_ROUTE);
      getDOMElement(ANT_CLOSE_MODAL_ICON).click();
      getByCy('location-General').click();
      // open Generate QR codes collapsible
      getByCy(GENERATE_QR_CODES_TITLE).should('be.visible').click();
      getByCy(LOCATION_GENERATE_QR_CODES).click();
      getByCy(QR_CODE_FOR_ENTIRE_AREA_SECTION).should('be.visible');
    });

    afterEach(() => {
      cy.clearLocalStorage();
    });

    it('should deactivate/activate "Qr code for entire area" toggle', () => {
      // Expect 'One QR code for the entre area' toggle to be activated by default
      shouldBeVisible(getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE));
      shouldHaveAttribute(
        getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE),
        AREA_CHECKED,
        'true'
      );
      // When toggle is activated, expect 'Download qr codes' button to be clickable
      shouldNotBeDisabled(getByCy(QR_CODE_DOWNLOAD_BUTTON_PDF));
      // Deactivate toggle
      getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE).click();
      getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE).should('not.be.checked');
      // When toggle is de-activated, expect 'Download qr codes' button to be unclickable
      shouldBeDisabled(getByCy(QR_CODE_DOWNLOAD_BUTTON_PDF));
    });

    it('should deactivate/activate "Corona-Warn-App compatability" toggle', () => {
      shouldBeVisible(getByCy(QR_CODE_COMPATABILITY_SECTION));
      // Expect 'Corona-Warn-App compatability' toggle to be inactive by default
      shouldHaveAttribute(
        getByCy(QR_CODE_COMPATABILITY_TOGGLE),
        AREA_CHECKED,
        'true'
      );
      // Activate toggle
      getByCy(QR_CODE_COMPATABILITY_TOGGLE).click();
      shouldHaveAttribute(
        getByCy(QR_CODE_COMPATABILITY_TOGGLE),
        AREA_CHECKED,
        'false'
      );
    });

    it('should display information tooltip when hovering over qr code compatability information icon', () => {
      shouldBeVisible(getByCy(QR_CODE_COMPATABILITY_INFO_ICON));
      triggerEvent(getByCy(QR_CODE_COMPATABILITY_INFO_ICON), 'mouseover');
      shouldBeVisible(getDOMElement(ANT_TOOLTIP_OPEN));
    });

    it('should download qr code as PDF file when clicking on "Create qr-codes" button', () => {
      getByCy(QR_CODE_DOWNLOAD_BUTTON_PDF).click();
      readFile(qrCodeEntireAreaPdf, 10000);
      deleteFileIfExists(qrCodeEntireAreaPdf);
    });

    it('should download qr code as CSV file when clicking on "Download as CSV" link', () => {
      getByCy(QR_CODE_DOWNLOAD_BUTTON_CSV).click();
      readFile(qrCodeEntireAreaCsv, 10000);
      deleteFileIfExists(qrCodeEntireAreaCsv);
    });

    it('should display information tooltip when hovering over CSV information icon', () => {
      shouldBeVisible(getByCy(QR_CODES_CSV_INFO_ICON));
      triggerEvent(getByCy(QR_CODES_CSV_INFO_ICON), 'mouseover');
      shouldBeVisible(getDOMElement(ANT_TOOLTIP_OPEN));
    });

    it('should open website of "qr-print" in new tab', () => {
      shouldBeVisible(getByCy(QR_PRINT_SECTION));
      shouldHaveAttribute(getByCy(QR_PRINT_LINK), 'target', '_blank');
      getByCy(QR_PRINT_LINK).click();
      verifyLinkOpensInNewTab(getByCy(QR_PRINT_LINK));
    });

    it('opens and closes collapsible upon clicking on location card', () => {
      // opened collapsible should be visible
      shouldBeVisible(getDOMElement(ANT_COLLAPSE_ITEM_ACTIVE));
      // opened collapsible should be closed
      getDOMElement(DIV_ARIA_EXPANDED_TRUE).click();
      shouldNotExist(getDOMElement(ANT_COLLAPSE_ITEM_ACTIVE));
    });
  }
);
