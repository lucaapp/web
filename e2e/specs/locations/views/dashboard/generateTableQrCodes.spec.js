import { createTestOperator } from '../../setup/createOperator';
import { createTestGroup } from '../../setup/createLocations';
import { getGroupPayload } from '../../setup/createLocations.helper';
import { DEFAULT_COOKIE } from '../../setup/createOperator.helper';
import { deleteFileIfExists, readFile, visit } from '../../../utils/commands';
import { APP_ROUTE } from '../../constants/routes';
import { getByCy, getDOMElement } from '../../../utils/selectors';
import {
  AREA_CHECKED,
  LOCATION_GENERATE_QR_CODES,
  QR_CODE_FOR_ENTIRE_AREA_SECTION,
  QR_CODE_FOR_ENTIRE_AREA_TOGGLE,
  QR_CODES_FOR_TABLES_SECTION,
  QR_CODES_FOR_TABLES_TOGGLE,
  QR_CODE_DOWNLOAD_BUTTON_CSV,
  QR_CODE_DOWNLOAD_BUTTON_PDF,
  GENERATE_QR_CODES_TITLE,
  ANT_CLOSE_MODAL_ICON,
} from '../../constants/selectorKeys';
import {
  shouldBeVisible,
  shouldHaveAttribute,
} from '../../../utils/assertions';
import { getQrCodeFilePath } from '../../helpers/generateQrCodes.helper';

let testGroup;
let qrCodesTablesPdf;
let qrCodesTablesCsv;

describe(
  'Dashboard Overview/Generate QR codes for tables',
  { retries: 3 },
  () => {
    before(() => {
      createTestOperator();
      createTestGroup(getGroupPayload({ name: 'AAA', tableCount: 1 })).then(
        group => {
          testGroup = group;
          qrCodesTablesPdf = getQrCodeFilePath({
            locationName: testGroup.groupName,
            hasTable: true,
          });
          qrCodesTablesCsv = getQrCodeFilePath({
            locationName: testGroup.groupName,
            hasTable: true,
            format: 'csv',
          });
        }
      );
    });

    beforeEach(() => {
      Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
      visit(APP_ROUTE);
      getDOMElement(ANT_CLOSE_MODAL_ICON).click();
      getByCy('location-General').click();
      // open Generate QR codes collapsible
      getByCy(GENERATE_QR_CODES_TITLE).should('be.visible').click();
      getByCy(LOCATION_GENERATE_QR_CODES).click();
      shouldBeVisible(getByCy(QR_CODE_FOR_ENTIRE_AREA_SECTION));
    });

    afterEach(() => {
      cy.clearLocalStorage();
    });

    it('should display a new section with activated toggle by default', () => {
      // Qr codes for tables should not be activated by default
      shouldBeVisible(getByCy(QR_CODES_FOR_TABLES_SECTION));
      shouldHaveAttribute(
        getByCy(QR_CODES_FOR_TABLES_TOGGLE),
        AREA_CHECKED,
        'true'
      );
      // qr code for entire area should now be de-activated
      shouldHaveAttribute(
        getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE),
        AREA_CHECKED,
        'false'
      );
    });

    it('should automatically deactivate "Qr codes for tables" toggle when "Qr code for entire area" toggle is activated and vice versa', () => {
      // Activate qrCodeForEntireAreaToggle
      getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE).click();
      // qrCodesForTablesToggle should automatically be deactivated
      shouldHaveAttribute(
        getByCy(QR_CODES_FOR_TABLES_TOGGLE),
        AREA_CHECKED,
        'false'
      );
      // Activate qrCodesForTablesToggle
      getByCy(QR_CODES_FOR_TABLES_TOGGLE).click();
      // qrCodeForEntireAreaToggle should automatically be deactivated
      shouldHaveAttribute(
        getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE),
        AREA_CHECKED,
        'false'
      );
    });
    it('should download qr codes for all tables as CSV file when clicking on "Download as CSV" link', () => {
      getByCy(QR_CODE_DOWNLOAD_BUTTON_CSV).click();
      readFile(qrCodesTablesCsv, 10000);
      deleteFileIfExists(qrCodesTablesCsv);
    });
    it('should download qr codes for all tables as PDF file when clicking on "Create qr-codes" button', () => {
      getByCy(QR_CODE_DOWNLOAD_BUTTON_PDF).click();
      readFile(qrCodesTablesPdf, 10000);
      deleteFileIfExists(qrCodesTablesPdf);
    });
  }
);
