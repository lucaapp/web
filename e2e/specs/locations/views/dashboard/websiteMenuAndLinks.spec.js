import { createTestOperator } from '../../setup/createOperator';
import { DEFAULT_COOKIE } from '../../setup/createOperator.helper';
import { createTestGroup } from '../../setup/createLocations';
import { getGroupPayload } from '../../setup/createLocations.helper';
import { visit } from '../../../utils/commands';
import { APP_ROUTE } from '../../constants/routes';
import { getByCy, getDOMElement } from '../../../utils/selectors';
import {
  ANT_CLOSE_MODAL_ICON,
  ANT_VALIDATION_ERROR,
  DASHBOARD_WEBSITE_MENU_AND_LINKS,
  LINK_INPUT_FIELD_GENERAL,
  LINK_INPUT_FIELD_MAP,
  LINK_INPUT_FIELD_MENU,
  LINK_INPUT_FIELD_SCHEDULE,
  LINK_INPUT_FIELD_WEBSITE,
  WEBSITE_LINKS_SAVE_CHANGES_BUTTON,
  WEBSITE_MENU_LINKS_SECTION,
} from '../../constants/selectorKeys';

const inputFields = [
  LINK_INPUT_FIELD_MENU,
  LINK_INPUT_FIELD_WEBSITE,
  LINK_INPUT_FIELD_SCHEDULE,
  LINK_INPUT_FIELD_MAP,
  LINK_INPUT_FIELD_GENERAL,
];

describe('Dashboard Overview/Website, menu and other links card', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload());
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
    getDOMElement(ANT_CLOSE_MODAL_ICON).click();
    // Open 'Website, menu and other links' collapsible
    getByCy(DASHBOARD_WEBSITE_MENU_AND_LINKS).click();
    getByCy(WEBSITE_MENU_LINKS_SECTION).should('be.visible');
  });

  it('should accept valid inputs and allow to save changes', () => {
    const validInput = 'https://www.nexenio.com';

    inputFields.map(inputField => {
      getByCy(inputField).type(validInput, { delay: 2 });
      getDOMElement(ANT_VALIDATION_ERROR).should('not.exist');
    });

    getByCy(WEBSITE_LINKS_SAVE_CHANGES_BUTTON).click();

    // After saving, button should no longer be clickable without any new changes to any input field
    getByCy(WEBSITE_LINKS_SAVE_CHANGES_BUTTON).should('be.disabled');

    // Website links should be visible in input fields
    inputFields.map(inputField =>
      getByCy(inputField).should('have.value', validInput)
    );
  });
});
