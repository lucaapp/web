import { APP_ROUTE } from '../../constants/routes';
import {
  ANT_COLLAPSE_ITEM_ACTIVE,
  ANT_TOOLTIP_OPEN,
  AREA_CHECKED,
  DIV_ARIA_EXPANDED_TRUE,
  LOCATION_GENERATE_QR_CODES,
  QR_CODES_CSV_INFO_ICON,
  QR_CODE_COMPATABILITY_INFO_ICON,
  QR_CODE_COMPATABILITY_SECTION,
  QR_CODE_COMPATABILITY_TOGGLE,
  QR_CODE_DOWNLOAD_BUTTON_CSV,
  QR_CODE_DOWNLOAD_BUTTON_PDF,
} from '../../constants/selectorKeys';
import { deleteFileIfExists, readFile, visit } from '../../../utils/commands';
import { getQrCodeFilePath } from '../../helpers/generateQrCodes.helper';
import { getByCy, getDOMElement } from '../../../utils/selectors';
import { createTestOperator } from '../../setup/createOperator';
import { DEFAULT_COOKIE } from '../../setup/createOperator.helper';
import { createTestGroup } from '../../setup/createLocations';
import { getGroupPayload } from '../../setup/createLocations.helper';

let testGroup;
let qrCodesTablesPdf;
let qrCodesTablesCsv;

describe('QR code download in location/area tables-tab', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload({ name: 'AAA', tableCount: 10 })).then(
      group => {
        testGroup = group;
        qrCodesTablesPdf = getQrCodeFilePath({
          locationName: testGroup.groupName,
          hasTable: true,
        });
        qrCodesTablesCsv = getQrCodeFilePath({
          locationName: testGroup.groupName,
          hasTable: true,
          format: 'csv',
        });
      }
    );
  });
  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(`${APP_ROUTE}/${testGroup.groupId}/tables/${testGroup.uuid}`);
    getByCy(LOCATION_GENERATE_QR_CODES).click();
  });

  afterEach(() => {
    getDOMElement(DIV_ARIA_EXPANDED_TRUE).click();
    getDOMElement(ANT_COLLAPSE_ITEM_ACTIVE).should('not.exist');
    deleteFileIfExists(qrCodesTablesPdf);
    deleteFileIfExists(qrCodesTablesCsv);
    cy.clearLocalStorage();
  });

  it('displays activated compatibility toggle and enabled download button by default', () => {
    getDOMElement(ANT_COLLAPSE_ITEM_ACTIVE).should('be.visible');
    getByCy(QR_CODE_COMPATABILITY_SECTION).should('be.visible');
    getByCy(QR_CODE_COMPATABILITY_TOGGLE).should(
      'have.attr',
      AREA_CHECKED,
      'true'
    );
    getByCy(QR_CODE_DOWNLOAD_BUTTON_PDF).should('not.be.disabled');
    getByCy(QR_CODE_DOWNLOAD_BUTTON_CSV).should('be.visible');
    // should display information tooltip when hovering over qr code compatability information icon
    getByCy(QR_CODE_COMPATABILITY_INFO_ICON).trigger('mouseover');
    getDOMElement(ANT_TOOLTIP_OPEN).should('be.visible');
    getByCy(QR_CODE_COMPATABILITY_INFO_ICON).trigger('mouseout');
    // should display information tooltip when hovering over CSV information icon
    getByCy(QR_CODES_CSV_INFO_ICON).trigger('mouseover');
    getDOMElement(ANT_TOOLTIP_OPEN).should('be.visible');
    getByCy(QR_CODE_COMPATABILITY_INFO_ICON).trigger('mouseout');
  });

  it('can download files with activated compatibility', () => {
    getByCy(QR_CODE_COMPATABILITY_TOGGLE).should(
      'have.attr',
      AREA_CHECKED,
      'true'
    );
    // PDF
    getByCy(QR_CODE_DOWNLOAD_BUTTON_PDF).click();
    readFile(qrCodesTablesPdf, 10000);
    // CSV
    getByCy(QR_CODE_DOWNLOAD_BUTTON_CSV).click();
    readFile(qrCodesTablesCsv);
  });

  it('can download files with deactivated compatibility', () => {
    // dectivate CWA compatibilty
    getByCy(QR_CODE_COMPATABILITY_TOGGLE).click();
    getByCy(QR_CODE_COMPATABILITY_TOGGLE).should(
      'have.attr',
      AREA_CHECKED,
      'false'
    );
    // PDF
    getByCy(QR_CODE_DOWNLOAD_BUTTON_PDF).click();
    readFile(qrCodesTablesPdf, 10000);
    // CSV
    getByCy(QR_CODE_DOWNLOAD_BUTTON_CSV).click();
    readFile(qrCodesTablesCsv);
  });
});
