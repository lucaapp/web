import { GET_TABLES } from '../../../constants/aliases';
import { APP_ROUTE } from '../../../constants/routes';
import {
  EDIT_TABLES_BUTTON,
  EDIT_TABLES_NUMBER_BUTTON,
  TABLE_COUNT,
  TABLES_NUMBER_INPUT_FIELD,
  SAVE_TABLES_NUMBER_BUTTON,
  SAVE_TABLE_CONFIGURATION_BUTTON,
  CONSECUTIVE_RADIO,
  CUSTOM_RADIO,
  TABLE_BLOCKS,
  TABLE_ITEMS,
  CANCEL_BUTTON,
  ANT_MODAL_CONTENT,
  TABLE_ITEM_INPUTS,
} from '../../../constants/selectorKeys';
import { visit } from '../../../../utils/commands';
import { getByCy, getDOMElement } from '../../../../utils/selectors';
import { createTestOperator } from '../../../setup/createOperator';
import { DEFAULT_COOKIE } from '../../../setup/createOperator.helper';
import { createTestGroup } from '../../../setup/createLocations';
import { getGroupPayload } from '../../../setup/createLocations.helper';
import {
  interceptAllTablesCall,
  validateConsecutiveInputValues,
  validateEmptyCustomInputValues,
  validateTableBlockNames,
  validateTableBlockTooltips,
} from './configureTables.helper';

let testGroup;

const INITIAL_TABLE_COUNT = 5;
const CONSECUTIVE_NUMBERS = ['1', '2', '3', '4', '5'];
const CUSTOM_TABLE_NAMES = ['24', '555', '556', 'BAR', 'Garden'];

const CUSTOM_TABLE_CHANGED = '1001';
const REDUCED_TABLE_COUNT = 2;

describe('Edit table numbering', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload({ tableCount: INITIAL_TABLE_COUNT })).then(
      group => {
        testGroup = group;
      }
    );
  });
  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(`${APP_ROUTE}/${testGroup.groupId}/tables/${testGroup.uuid}`);
    // open edit tables modal
    getByCy(EDIT_TABLES_BUTTON).click();
  });

  describe('Configure table numbering', () => {
    it('has configured table numbering in consecutive order by default', () => {
      getByCy(CONSECUTIVE_RADIO).should('be.checked');
      // display all iputs  with consecutive value
      getByCy(TABLE_ITEMS).should('have.length', INITIAL_TABLE_COUNT);
      validateConsecutiveInputValues();
      // leave without change
      getByCy(CANCEL_BUTTON).click();
      getDOMElement(ANT_MODAL_CONTENT).should('not.exist');
      // assert floorplan numbering and count
      getByCy(TABLE_COUNT).should('contain', INITIAL_TABLE_COUNT);
      validateTableBlockNames(CONSECUTIVE_NUMBERS);
      validateTableBlockTooltips(CONSECUTIVE_NUMBERS);
    });

    describe('Configure custom table numbering', () => {
      it('can configure table numbering with custom numbers', () => {
        getByCy(CUSTOM_RADIO).check();
        getByCy(CUSTOM_RADIO).should('be.checked');
        // display all iputs without existing value
        getByCy(TABLE_ITEMS).should('have.length', INITIAL_TABLE_COUNT);
        validateEmptyCustomInputValues();
        getByCy(SAVE_TABLE_CONFIGURATION_BUTTON).should('be.disabled');
        // change all inputs
        getByCy(TABLE_ITEMS).each((tableItem, index) => {
          getByCy(`table-${index + 1}`).type(CUSTOM_TABLE_NAMES[index], {
            delay: 0,
          });
        });
        getByCy(SAVE_TABLE_CONFIGURATION_BUTTON).click();
        cy.reload();
        getByCy(TABLE_COUNT).should('contain', INITIAL_TABLE_COUNT);
        getDOMElement(TABLE_BLOCKS).should('have.length', INITIAL_TABLE_COUNT);
        validateTableBlockNames(CUSTOM_TABLE_NAMES);
        validateTableBlockTooltips(CUSTOM_TABLE_NAMES);
      });

      it('can change saved custom numbers', () => {
        getByCy(CUSTOM_RADIO).should('be.checked');
        // can change single number
        getByCy(TABLE_ITEMS).last().click().clear();
        getByCy(SAVE_TABLE_CONFIGURATION_BUTTON).should('be.disabled');
        getByCy(TABLE_ITEMS).last().type(CUSTOM_TABLE_CHANGED);
        getByCy(SAVE_TABLE_CONFIGURATION_BUTTON).click();
        getDOMElement(ANT_MODAL_CONTENT).should('not.exist');
        cy.reload();
        getDOMElement(TABLE_BLOCKS).should('have.length', INITIAL_TABLE_COUNT);
        // assert floorplan numbering
        validateTableBlockNames([
          ...CUSTOM_TABLE_NAMES.slice(0, -1),
          CUSTOM_TABLE_CHANGED,
        ]);
      });

      it('can reduce number of tables, while  keeping saved custom numbers', () => {
        interceptAllTablesCall(testGroup.uuid);
        getByCy(EDIT_TABLES_NUMBER_BUTTON).click();
        getByCy(TABLES_NUMBER_INPUT_FIELD).clear().type(REDUCED_TABLE_COUNT);
        getByCy(SAVE_TABLES_NUMBER_BUTTON).click();
        getByCy(CUSTOM_RADIO).should('be.checked');
        cy.wait(`@${GET_TABLES}`);
        getByCy(TABLE_ITEMS).should('have.length', REDUCED_TABLE_COUNT);
        getByCy(TABLE_ITEMS).each((tableItem, index) => {
          getByCy(`table-${index + 1}`).should(
            'have.value',
            CUSTOM_TABLE_NAMES[index]
          );
        });
        // leave without changing
        getByCy(CANCEL_BUTTON).click();
        cy.reload();
        getDOMElement(TABLE_BLOCKS).should('have.length', REDUCED_TABLE_COUNT);
        // assert floorplan numbering
        validateTableBlockNames(
          CUSTOM_TABLE_NAMES.slice(0, REDUCED_TABLE_COUNT)
        );
      });

      it('can increase number of tables and switch between consecutive order and adding new value to saved custom numbers', () => {
        interceptAllTablesCall(testGroup.uuid);
        getByCy(EDIT_TABLES_NUMBER_BUTTON).click();
        getByCy(TABLES_NUMBER_INPUT_FIELD).clear().type(INITIAL_TABLE_COUNT);
        getByCy(SAVE_TABLES_NUMBER_BUTTON).click();
        getByCy(CUSTOM_RADIO).should('be.checked');
        cy.wait(`@${GET_TABLES}`);
        getByCy(TABLE_ITEMS).should('have.length', INITIAL_TABLE_COUNT);
        // saved custom numbers are prefilled
        getDOMElement(TABLE_ITEM_INPUTS)
          .first()
          .should('have.value', CUSTOM_TABLE_NAMES[0]);
        getDOMElement(TABLE_ITEM_INPUTS).last().should('have.value', '');
        getByCy(SAVE_TABLE_CONFIGURATION_BUTTON).should('be.disabled');
        // can overwrite with consecutive order
        getByCy(CONSECUTIVE_RADIO).check();
        validateConsecutiveInputValues();
        getByCy(SAVE_TABLE_CONFIGURATION_BUTTON).click();
        cy.reload();
        // assert floorplan numbering
        getDOMElement(TABLE_BLOCKS).should('have.length', INITIAL_TABLE_COUNT);
        validateTableBlockNames(CONSECUTIVE_NUMBERS);
        // default radio is set to consecutive order, custom values are overwritten
        getByCy(EDIT_TABLES_BUTTON).click();
        getByCy(CONSECUTIVE_RADIO).should('be.checked');
        // display all iputs  with consecutive value
        getByCy(TABLE_ITEMS).should('have.length', INITIAL_TABLE_COUNT);
        validateConsecutiveInputValues();
        getByCy(CUSTOM_RADIO).check();
        validateEmptyCustomInputValues();
      });
    });
  });
});
