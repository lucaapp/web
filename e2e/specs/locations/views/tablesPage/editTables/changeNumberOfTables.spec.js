import { APP_ROUTE } from '../../../constants/routes';
import {
  CANCEL_BUTTON,
  EDIT_TABLES_BUTTON,
  EDIT_TABLES_NUMBER_BUTTON,
  TABLE_COUNT,
  TABLES_NUMBER_INPUT_FIELD,
  SAVE_TABLES_NUMBER_BUTTON,
  SAVE_TABLE_CONFIGURATION_BUTTON,
  ANT_VALIDATION_ERROR,
  TABLES_FLOORPLAN,
  TABLE_BLOCKS,
} from '../../../constants/selectorKeys';
import { visit } from '../../../../utils/commands';
import { getByCy, getDOMElement } from '../../../../utils/selectors';
import { createTestOperator } from '../../../setup/createOperator';
import { DEFAULT_COOKIE } from '../../../setup/createOperator.helper';
import { createTestGroup } from '../../../setup/createLocations';
import { getGroupPayload } from '../../../setup/createLocations.helper';

let testGroup;
const INITIAL_TABLE_COUNT = 5;
const NEW_TABLE_COUNT = 12;
const INVALID_INPUT = 'abc';

describe('Change number of tables', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload({ tableCount: INITIAL_TABLE_COUNT })).then(
      group => {
        testGroup = group;
      }
    );
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(`${APP_ROUTE}/${testGroup.groupId}/tables/${testGroup.uuid}`);
    // open edit tables modal
    getByCy(EDIT_TABLES_BUTTON).click();
    getByCy(TABLE_COUNT).should('contain', INITIAL_TABLE_COUNT);
    getByCy(EDIT_TABLES_NUMBER_BUTTON).click();
  });

  it('cannot submit an invalid number of tables', () => {
    // cannot submit empty string
    getByCy(TABLES_NUMBER_INPUT_FIELD).clear().blur();
    getDOMElement(ANT_VALIDATION_ERROR).should('be.visible');
    // cannot submit invalid value
    getByCy(TABLES_NUMBER_INPUT_FIELD).clear().type(INVALID_INPUT);
    getDOMElement(ANT_VALIDATION_ERROR).should('be.visible');
    // submit on configuration is disabled on active input field
    getByCy(SAVE_TABLE_CONFIGURATION_BUTTON).should('be.disabled');
    getByCy(CANCEL_BUTTON).click();
    // assert no change on floorplan
    getByCy(TABLES_FLOORPLAN).should('be.visible');
    getByCy(TABLE_COUNT).should('contain', INITIAL_TABLE_COUNT);
    getDOMElement(TABLE_BLOCKS).should('have.length', INITIAL_TABLE_COUNT);
  });

  it('can change number of tables with valid value', () => {
    getByCy(TABLES_NUMBER_INPUT_FIELD).clear().type(NEW_TABLE_COUNT);
    getDOMElement(ANT_VALIDATION_ERROR).should('not.exist');
    getByCy(SAVE_TABLES_NUMBER_BUTTON).click();
    // assert change on floorplan
    getByCy(TABLES_FLOORPLAN).should('be.visible');
    getByCy(TABLE_COUNT).should('contain', NEW_TABLE_COUNT);
    getDOMElement(TABLE_BLOCKS).should('have.length', NEW_TABLE_COUNT);
  });
});
