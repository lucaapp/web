import { GET_TABLES } from '../../../constants/aliases';
import {
  ANT_TOOLTIP_OPEN,
  TABLE_BLOCKS,
  TABLE_ITEMS,
} from '../../../constants/selectorKeys';
import { getByCy, getDOMElement } from '../../../../utils/selectors';

export const interceptAllTablesCall = locationId => {
  cy.intercept('GET', `/api/v4/locations/${locationId}/tables`).as(GET_TABLES);
};

export const validateConsecutiveInputValues = () => {
  getByCy(TABLE_ITEMS).each((tableItem, index) => {
    getByCy(`table-${index + 1}`).should('have.value', index + 1);
    getByCy(`table-${index + 1}`).should('have.attr', 'disabled');
  });
};

export const validateEmptyCustomInputValues = () => {
  getByCy(TABLE_ITEMS).each((tableItem, index) => {
    getByCy(`table-${index + 1}`).should('have.value', '');
    getByCy(`table-${index + 1}`).should('not.have.attr', 'disabled');
  });
};

export const validateTableBlockNames = tableNames => {
  getDOMElement(TABLE_BLOCKS).each((tableBlock, index) => {
    getDOMElement(tableBlock).should('contain', tableNames[index]);
  });
};

export const validateTableBlockTooltips = tableNames => {
  getDOMElement(TABLE_BLOCKS).each((tableBlock, index) => {
    getDOMElement(tableBlock).trigger('mouseover');
    getDOMElement(ANT_TOOLTIP_OPEN).should('be.visible');
    getDOMElement(ANT_TOOLTIP_OPEN).should('contain', tableNames[index]);
    getDOMElement(tableBlock).trigger('mouseout');
  });
};
