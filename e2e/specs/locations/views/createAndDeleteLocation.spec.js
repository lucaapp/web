import { readFile, type, visit, waitForAlias } from '../../utils/commands';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { APP_ROUTE } from '../constants/routes';
import {
  ANT_PRIMARY_POPOVER_BUTTON,
  AREA_LIST_GENERAL_LOCATION,
  CREATE_GROUP,
  DELETE_GROUP,
  LOCATION_PREFIX,
  MENU_ITEM_ARCHIVE,
  NEXT_STEP_BUTTON,
  OPEN_AREA_SETTINGS_LINK,
  OPEN_LOCATION_SETTINGS_LINK,
  STEP_COUNT,
  SUBMIT_BUTTON,
  ID_GROUP_NAME,
  ID_PHONE,
  TABLES_TOGGLE,
  ID_TABLE_COUNT,
  AREA_DEVISION_TOGGLE,
  AREA_TYPE_0,
  AREA_TYPE_OPTION_OUTDOOR,
  ID_AREA_NAME_0,
  DOWNLOAD_QR_CODE_BUTTON,
  GROUP_NAME,
  LOCATION_NAME,
  TABLES_PREFIX,
  ANT_MESSAGE_CHECK,
  GROUP_PREFIX,
  PASSWORD_MODAL_INPUT_FIELD,
  NOTIFICATION_SUCCESS,
  HEADER_DROPDOWN_MENU_TRIGGER,
  DELETED_GROUP_NAME,
  REACTIVATE_GROUP_BUTTON,
  HEADER_LOGO_HOME,
  ANT_CLOSE_MODAL_ICON,
  HOTEL_TYPE,
} from '../constants/selectorKeys';
import {
  getAreaAttributes,
  getGroupAttributes,
} from '../helpers/generateAttributes.helper';
import { getQrCodeFilePath } from '../helpers/generateQrCodes.helper';
import { verifyDefaultLocationTitle } from '../helpers/ui/validations';
import { createTestOperator } from '../setup/createOperator';
import {
  DEFAULT_COOKIE,
  STANDARD_PASSWORD,
} from '../setup/createOperator.helper';
import { typeManualAddress } from '../helpers/ui/createLocation.helper';

let testOperator;
let testLocationId;
let testGroupId;

const testGroup = getGroupAttributes('neXenio');
const testArea = getAreaAttributes('Area one');
const TABLE_COUNT = 4;

const TEST_GROUP_QR_CODE_PATH = getQrCodeFilePath({
  locationName: testGroup.name,
  hasTable: true,
});

describe('Create, update and delete location', () => {
  before(() => {
    createTestOperator().then(operator => {
      testOperator = operator;
    });
    cy.intercept('POST', 'api/v3/locationGroups').as(CREATE_GROUP);
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
    getDOMElement(ANT_CLOSE_MODAL_ICON).click();
  });

  afterEach(() => {
    cy.clearLocalStorage();
  });

  it('can create a location restaurant with area and qr code download', () => {
    getByCy(CREATE_GROUP).should('have.length', 2);
    getByCy(CREATE_GROUP).first().click();
    // Step 1: Select group type
    getByCy(STEP_COUNT).should('contain', '1 / 8');
    getByCy(HOTEL_TYPE).click();
    // Step 2: Group name input
    getByCy(STEP_COUNT).should('contain', '2 / 8');
    type(getDOMElement(ID_GROUP_NAME), testGroup.name);
    getByCy(NEXT_STEP_BUTTON).click();
    // Step 3: Phone number input is prefilled
    getByCy(STEP_COUNT).should('contain', '3 / 8');
    getDOMElement(ID_PHONE).should('have.value', testOperator.phone);
    getByCy(NEXT_STEP_BUTTON).click();
    // Step 4: Google maps step is left out
    getByCy(STEP_COUNT).should('contain', '4 / 8');
    getByCy(NEXT_STEP_BUTTON).click();
    // Manual address input
    typeManualAddress(
      testGroup.streetName,
      testGroup.streetNr,
      testGroup.zipCode,
      testGroup.city
    );
    getByCy(NEXT_STEP_BUTTON).click();
    // Step 5: Area Type Selection
    getByCy(STEP_COUNT).should('contain', '5 / 8');
    getByCy(NEXT_STEP_BUTTON).click();
    // Step 6: Table allocation is set to 4
    getByCy(STEP_COUNT).should('contain', '6 / 8');
    getByCy(TABLES_TOGGLE).click();
    getByCy(NEXT_STEP_BUTTON).click();
    type(getDOMElement(ID_TABLE_COUNT).focus(), TABLE_COUNT);
    getByCy(NEXT_STEP_BUTTON).click();
    // Step 7: Area division
    getByCy(STEP_COUNT).should('contain', '7 / 8');
    getByCy(AREA_DEVISION_TOGGLE).click();
    type(getDOMElement(ID_AREA_NAME_0), testArea.name);
    getByCy(AREA_TYPE_0).click();
    getByCy(AREA_TYPE_OPTION_OUTDOOR).click();
    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement(ANT_MESSAGE_CHECK).should('be.visible');
    // Step 8: Finish and QR code download

    getByCy(STEP_COUNT).should('contain', '8 / 8');
    waitForAlias(`@${CREATE_GROUP}`)
      .its('response.statusCode')
      .should('eq', 201);
    getDOMElement(`@${CREATE_GROUP}`).then(xhr => {
      testLocationId = xhr.response.body.location.locationId;
      testGroupId = xhr.response.body.groupId;
    });
    getDOMElement(NOTIFICATION_SUCCESS).should('be.visible');
    getByCy(DOWNLOAD_QR_CODE_BUTTON).click();
    readFile(TEST_GROUP_QR_CODE_PATH, 30000);
  });

  describe('created location', () => {
    it('shows created location with tables tab and area in sidebar', () => {
      // verify base group names
      getByCy(GROUP_NAME).should('contain', testGroup.name);
      verifyDefaultLocationTitle(getByCy(LOCATION_NAME));
      // verify sidebar
      getByCy(AREA_LIST_GENERAL_LOCATION).should('be.visible');
      getByCy(LOCATION_PREFIX + testArea.name).should('contain', testArea.name);
      getByCy(TABLES_PREFIX + testLocationId).should('be.visible');
      getByCy(LOCATION_PREFIX + testArea.name).click();
      // verify area name overview
      getByCy(GROUP_NAME).should('contain', testGroup.name);
      getByCy(LOCATION_NAME).should('contain', testArea.name);
      // verify base group overview
      getByCy(GROUP_PREFIX + testGroupId).click();
    });

    describe('Location deletion and reactivation', () => {
      it('cannot delete the base location', () => {
        getDOMElement(OPEN_AREA_SETTINGS_LINK).click();
        getByCy(DELETE_GROUP).should('not.exist');
      });
      it('can delete a non-default location and reactivate', () => {
        getByCy(LOCATION_PREFIX + testGroup.name).click();
        getDOMElement(OPEN_LOCATION_SETTINGS_LINK).click();
        getByCy('locationCard-deleteGroup').should('be.visible').click();
        getByCy(DELETE_GROUP).click();
        getDOMElement(ANT_PRIMARY_POPOVER_BUTTON).click();
        // enter password
        type(getByCy(PASSWORD_MODAL_INPUT_FIELD).focus(), STANDARD_PASSWORD);
        getDOMElement(SUBMIT_BUTTON).click();
        // verify group and area are deleted
        getDOMElement(NOTIFICATION_SUCCESS).should('be.visible');
        getDOMElement(ANT_MESSAGE_CHECK).should('be.visible');
        getByCy(LOCATION_PREFIX + testGroup.name).should('not.exist');
        getByCy(LOCATION_PREFIX + testArea.name).should('not.exist');
        getByCy(CREATE_GROUP).should('have.length', 2);
        // verify group in archive
        getByCy(HEADER_DROPDOWN_MENU_TRIGGER).click({ force: true });
        getByCy(MENU_ITEM_ARCHIVE).click();
        getByCy(DELETED_GROUP_NAME, 5000).should('contain', testGroup.name);
        // reactivate group
        getByCy(REACTIVATE_GROUP_BUTTON).click();
        getDOMElement(NOTIFICATION_SUCCESS).should('be.visible');
        getDOMElement(ANT_MESSAGE_CHECK).should('be.visible');
        getByCy(DELETED_GROUP_NAME).should('not.exist');
        getByCy(HEADER_LOGO_HOME).click();
        getByCy(AREA_LIST_GENERAL_LOCATION).should('be.visible');
        getByCy(LOCATION_PREFIX + testArea.name).should(
          'contain',
          testArea.name
        );
        getByCy(TABLES_PREFIX + testLocationId).should('be.visible');
        getByCy(LOCATION_PREFIX + testArea.name);
      });
    });
  });
});
