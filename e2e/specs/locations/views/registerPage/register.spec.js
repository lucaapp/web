import {
  NEW_E2E_EMAIL,
  NEW_E2E_FIRST_NAME,
  NEW_E2E_LAST_NAME,
  NEW_E2E_PHONE,
  STRENGTH4_PASSWORD,
  BUSINESS_NAME,
  BUSINESS_ADDRESS,
} from '../../constants/user';
import {
  AVV_CHECKBOX,
  TERMS_AND_CONDITIONS_CHECKBOX,
  FINISH_REGISTER,
  LEGAL_TERMS_SUBMIT_BUTTON,
  END_REGISTRATION_BUTTON,
  LOGIN_EMAIL_INPUT_FIELD,
  CREATE_ACCOUNT_EMAIL_INPUT_FIELD,
  CREATE_NEW_ACCOUNT_BUTTON,
  PHONE,
  LAST_NAME,
  FIRST_NAME,
  CONFIRM_NAME_BUTTON,
  BUSINESS_NAME_INPUT,
  CONFIRM_BUSINESS_INFO_BUTTON,
  SET_PASSWORD_FIELD,
  SET_PASSWORD_CONFIRM_FIELD,
  SET_PASSWORD_SUBMIT_BUTTON,
  LEGAL_TERMS,
  BUSINESS_STREET_INPUT,
  BUSINESS_STREET_NUMBER_INPUT,
  BUSINESS_ZIP_CODE_INPUT,
  BUSINESS_CITY_INPUT,
  CONFIRM_LOCACTION_GROUP_BUTTON,
  TABLE_NUMBER,
  ID_NAME,
} from '../../constants/selectorKeys';
import { REGISTER_ROUTE } from '../../constants/routes';
import { getByCy, getDOMElement } from '../../../utils/selectors';
import { type, visit, waitForAlias } from '../../../utils/commands';
import { REGISTER_OPERATOR } from '../../constants/aliases';

describe('Registration', () => {
  it('registers a new user', () => {
    cy.intercept('POST', 'api/v3/operators').as(REGISTER_OPERATOR);
    visit(REGISTER_ROUTE);
    // Enter new email
    type(getByCy(CREATE_ACCOUNT_EMAIL_INPUT_FIELD), NEW_E2E_EMAIL);
    getByCy(CREATE_NEW_ACCOUNT_BUTTON).click();
    // Enter contact information
    type(getDOMElement(FIRST_NAME), NEW_E2E_FIRST_NAME);
    type(getDOMElement(LAST_NAME), NEW_E2E_LAST_NAME);
    type(getDOMElement(PHONE), NEW_E2E_PHONE);
    getByCy(CONFIRM_NAME_BUTTON).click();
    // enter business information
    type(getDOMElement(BUSINESS_NAME_INPUT), BUSINESS_NAME);
    type(getDOMElement(BUSINESS_STREET_INPUT), BUSINESS_ADDRESS.street);
    type(
      getDOMElement(BUSINESS_STREET_NUMBER_INPUT),
      BUSINESS_ADDRESS.streetNr
    );
    type(getDOMElement(BUSINESS_ZIP_CODE_INPUT), BUSINESS_ADDRESS.zipCode);
    type(getDOMElement(BUSINESS_CITY_INPUT), BUSINESS_ADDRESS.city);
    getByCy(CONFIRM_BUSINESS_INFO_BUTTON).click();
    // Set password
    type(getByCy(SET_PASSWORD_FIELD), STRENGTH4_PASSWORD);
    type(getByCy(SET_PASSWORD_CONFIRM_FIELD), STRENGTH4_PASSWORD);
    getByCy(SET_PASSWORD_SUBMIT_BUTTON).click();
    // Location group
    getDOMElement(ID_NAME).type('Nexenio Location');
    getDOMElement(TABLE_NUMBER).type(10);
    getByCy(CONFIRM_LOCACTION_GROUP_BUTTON).click();
    // Set legal
    getByCy(LEGAL_TERMS).should('be.visible');
    getByCy(AVV_CHECKBOX).check();
    getByCy(TERMS_AND_CONDITIONS_CHECKBOX).check();
    getByCy(LEGAL_TERMS_SUBMIT_BUTTON).click();
    // Finish
    waitForAlias(`@${REGISTER_OPERATOR}`, 7000).should(xhr => {
      expect(xhr.response).to.have.property('statusCode', 204);
      expect(xhr.request.body).to.have.property('email', NEW_E2E_EMAIL);
    });
    getByCy(FINISH_REGISTER).should('be.visible');
    getByCy(END_REGISTRATION_BUTTON).click();
    getByCy(LOGIN_EMAIL_INPUT_FIELD).should('be.visible');
  });
});
