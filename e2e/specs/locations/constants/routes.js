export const REGISTER_ROUTE = '/register';
export const APP_ROUTE = '/app/group';
export const CONTACT_FORM_ROUTE = '/contact-form';
export const PROFILE_ROUTE = 'app/profile';
export const DATA_TRANSFERS_ROUTE = 'app/dataTransfers';
export const DEVICES_ROUTE = '/app/devices';
export const ARCHIVE_ROUTE = '/app/archive';

export const OPERATOR_LOCATIONS_ROUTE = 'api/v3/operators/locations';
export const HELP_CENTER_ROUTE = '/app/help';
