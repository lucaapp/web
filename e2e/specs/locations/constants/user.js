// email
export const NEW_E2E_EMAIL = 'unknown@nexenio.com';

// name
export const NEW_E2E_FIRST_NAME = 'E2E';
export const NEW_E2E_LAST_NAME = 'User';
export const NEW_E2E_PHONE = '+49 176 12345678';

// Business entity
export const BUSINESS_NAME = 'neXenio';
export const BUSINESS_ADDRESS = {
  street: 'Charlottenstraße',
  streetNr: '59',
  zipCode: '10117',
  city: 'Berlin',
};

// Location Group entity
export const LOCATION_GROUP_TYPE = 'restaurant';
export const LOCATION_GROUP_NAME = 'neXenio location';
export const LOCATION_GROUP_TABLE_COUNT = 20;

// password
export const NEW_E2E_VALID_PASSWORD = 'Nexenio123!';

export const SOME_OTHER_INVALID_PASSWORD = 'Some0therVal1dPassword!';

export const STRENGTH0_PASSWORD = 'Abc';

export const STRENGTH1_PASSWORD = 'Abcde';

export const STRENGTH2_PASSWORD = 'Abcdefga';

export const STRENGTH3_PASSWORD = 'Abcdefga1';

export const STRENGTH4_PASSWORD = 'Abcdefga1!123';

export const INVALID_WEBSITE_INPUTS = [
  'www.nexenio.com',
  'https:/www.nexenio.com',
  'nexenio.com',
  'https/nexenio.com',
  ' ',
];

// Additional Data
export const CHECKIN_QUESTION_1 = 'Question 1';
export const CHECKIN_QUESTION_2 = 'Question 2';
