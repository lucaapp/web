// Default location names
export const DEFAULT_LOCATION_TITLE_EN = 'General';
export const DEFAULT_LOCATION_TITLE_DE = 'Allgemein';
export const DEFAULT_LOCATION_TITLES_DE_EN = [
  DEFAULT_LOCATION_TITLE_DE,
  DEFAULT_LOCATION_TITLE_EN,
];

// Additional Data
export const CHECKIN_QUESTION_1 = 'Question 1';
export const CHECKIN_QUESTION_2 = 'Question 2';
