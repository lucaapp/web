// General
export const PROCEED = 'proceed';
export const NEXT_BUTTON = 'next';
export const NEXT_STEP_BUTTON = 'nextStep';
export const INPUT_TYPE_FILE = 'input[type=file]';
export const CANCEL_BUTTON = 'cancelButton';
export const STEP_COUNT = 'stepCount';
export const SUBMIT_BUTTON = 'button[type=submit]';
export const DONE_BUTTON = 'done';
export const PASSWORD_MODAL_INPUT_FIELD = 'passwordModal-passwordInputField';
export const NOTIFICATION_SUCCESS = '.generalNotificationSuccess';
export const TOGGLE_GOOGLE_SERVICE = 'toggleGoogleService';

// General: Ids
export const ROOT = '#root';
export const CITY = '#city';
export const PHONE = '#phone';
export const FIRST_NAME = '#firstName';
export const LAST_NAME = '#lastName';
export const BUSINESS_NAME_INPUT = '#businessEntityName';
export const BUSINESS_STREET_INPUT = '#businessEntityStreetName';
export const BUSINESS_STREET_NUMBER_INPUT = '#businessEntityStreetNumber';
export const BUSINESS_ZIP_CODE_INPUT = '#businessEntityZipCode';
export const BUSINESS_CITY_INPUT = '#businessEntityCity';
export const TERMS_AND_CONDITIONS = '#termsAndConditions';
export const ID_GROUP_NAME = '#groupName';
export const ID_PHONE = '#phone';
export const ID_TABLE_COUNT = '#tableCount';
export const ID_STREET_NAME = '#streetName';
export const ID_STREET_NUMBER = '#streetNr';
export const ID_ZIP_CODE = '#zipCode';
export const ID_CITY = '#city';
export const ID_NAME = '#name';

// Login
export const LOGIN_EMAIL_INPUT_FIELD = 'login-emailInputField';
export const LOGIN_SUBMIT_BUTTON = 'loginSubmitButton';
export const DOWNLOAD_PRIVATE_KEY_BUTTON = 'downloadPrivateKey';
export const PRIVATE_KEY_DOWNLOADED_CHECKBOX = 'checkPrivateKeyIsDownloaded';

// Registration
export const CREATE_ACCOUNT_EMAIL_INPUT_FIELD = 'createAccount-emailInputField';
export const CREATE_NEW_ACCOUNT_BUTTON = 'createNewAccountButton';
export const CONFIRM_NAME_BUTTON = 'confirmContactsButton';
export const CONFIRM_BUSINESS_INFO_BUTTON = 'confirmBusinessInfoButton';
export const SET_PASSWORD = 'setPassword';
export const SET_PASSWORD_SUBMIT_BUTTON = 'setPasswordSubmitButton';
export const SET_PASSWORD_FIELD = 'setPasswordField';
export const SET_PASSWORD_CONFIRM_FIELD = 'setPasswordConfirmField';

export const TABLE_NUMBER = '#tableCount';
export const CONFIRM_LOCACTION_GROUP_BUTTON = 'confirmLocationGroupButton';

export const LEGAL_TERMS = 'legalTerms';
export const AVV_CHECKBOX = 'avvCheckbox';
export const TERMS_AND_CONDITIONS_CHECKBOX = 'termsAndConditionsCheckbox';
export const LEGAL_TERMS_SUBMIT_BUTTON = 'legalTermsSubmitButton';
export const FINISH_REGISTER = 'finishRegister';
export const END_REGISTRATION_BUTTON = 'endRegistrationButton';
export const SET_TERMS_BACK_BUTTON = 'setTermsBackButton';

// Header
export const HEADER_LOGO_HOME = 'headerLogoHome';
export const DEVICES_ICON = 'devicesIcon';
export const OPEN_HELPCENTER_BUTTON = 'buttonHelpCenter';
export const HEADER_DROPDOWN_MENU_TRIGGER = 'dropdownMenuTrigger';
export const MENU_ITEM_PROFILE = 'menuItemProfile';
export const MENU_ITEM_DATA_REQUESTS = 'menuItemDataRequests';
export const MENU_ITEM_ARCHIVE = 'menuItemArchive';

// Create Area / Location Modal
export const RESTAURANT_TYPE = 'restaurant';
export const HOTEL_TYPE = 'hotel';
export const SELECT_AREA_TYPE = 'selectAreaType';
export const AREA_TYPE_OPTION_INDOOR = 'areaTypeOptionIndoor';
export const SELECT_MASKS = 'selectMasks';
export const MEDICAL_MASKS_OPTION_YES = 'medicalMasksOptionYes';
export const SELECT_ENTRY_POLICY_INFO = 'selectEntryPolicyInfo';
export const ENTRY_POLICY_OPTION_2G = 'entryPolicyInfoOption2G';
export const SELECT_VENTILATION = 'selectVentilation';
export const VENTILATION_OPTION_ELECTRIC = 'roomVentilationOptionElectric';
export const TABLES_TOGGLE = 'toggleTableInput';
export const AREA_DEVISION_TOGGLE = 'toggleAreaDivision';
export const ID_AREA_NAME_0 = '#areaName0';
export const AREA_TYPE_0 = 'areaType0';
export const AREA_TYPE_OPTION_OUTDOOR = 'areaTypeOptionOutdoor';

export const ROOM_WIDTH = 'roomWidth';
export const ROOM_DEPTH = 'roomDepth';
export const ROOM_HEIGHT = 'roomHeight';
export const DOWNLOAD_QR_CODE_BUTTON = 'downloadQRCode';

// Sidebar
export const AREA_LIST_GENERAL_LOCATION = 'location-General';
export const CREATE_GROUP = 'createGroup';
export const LOCATION_PREFIX = 'location-';
export const TABLES_PREFIX = 'tables-';
export const GROUP_PREFIX = 'groupItem-';

// Group DELETION
export const DELETE_GROUP = 'deleteGroup';

// GROUP OVERVIEW
export const GROUP_OVERVIEW_CARD = 'groupOverviewCard';
export const GUESTS_TOTAL_COUNT = 'guestsTotalCount';
export const LOCATIONS_COUNT = 'locationsCount';

// Group
export const GROUPNAME = 'groupName';

// Tables
export const TABLES_FLOORPLAN = 'tables-floorplan';
export const TABLE_COUNT = 'tableCount';
export const TABLE_BLOCKS = '*[data-cy^="tableBlock-"]';
export const EDIT_TABLES_BUTTON = 'editTablesButton';
export const EDIT_TABLES_NUMBER_BUTTON = 'editTablesNumberButton';
export const SAVE_TABLES_NUMBER_BUTTON = 'saveTablesNumberButton';
export const TABLES_NUMBER_INPUT_FIELD = 'tableNumberInput';
export const CONSECUTIVE_RADIO = 'consecutiveNumberingRadio';
export const CUSTOM_RADIO = 'customNumberingRadio';
export const TABLE_ITEMS = 'tableItem';
export const TABLE_ITEM_INPUTS = '[id^=table-]';
export const SAVE_TABLE_CONFIGURATION_BUTTON = 'saveTableConfigurationButton';

// Data Transfers Page
export const SHARE_DATA_FEEDBACK = 'shareDataFeedback';
export const SHARE_DATA_ERROR_MESSAGE = 'shareDataErrorMessage';

// Location Dashboard
export const GROUP_NAME = 'groupName';
export const LOCATION_NAME = 'locationName';
export const AREA_CHECKED = 'aria-checked';
export const OPEN_AREA_SETTINGS_LINK = "[id$='-tab-openAreaSettings']";
export const OPEN_LOCATION_SETTINGS_LINK = "[id$='-tab-openLocationSettings']";
export const NOTIFICATION_AREA_UPDATE_SUCCESS =
  'notification-updateLocationSuccesss';
export const LOCATION_DISPLAY_NAME = 'locationDisplayName';
export const SCANNER = 'scanner';
export const CONTACT_FORM = 'contactForm';
export const STATUS2G = 'status2G';
export const STATUS3G = 'status3G';

// Location dashboard - Generate Qr Codes
export const QR_CODE_DOWNLOAD_BUTTON_PDF = 'qrCodeDownload';
export const LOCATION_GENERATE_QR_CODES = 'locationCard-generateQRCodes';
export const GENERATE_QR_CODES_TITLE = 'generateQRCodesTitle';
export const QR_CODE_FOR_ENTIRE_AREA_SECTION = 'qrCodeForEntireAreaSection';
export const QR_CODE_FOR_ENTIRE_AREA_TOGGLE = 'qrCodeForEntireAreaToggle';
export const QR_CODE_DOWNLOAD_BUTTON_CSV = 'qrCodeDownloadCsv';

export const QR_CODE_COMPATABILITY_SECTION = 'qrCodeCompatabilitySection';
export const QR_CODE_COMPATABILITY_TOGGLE = 'qrCodeCompatabilityToggle';
export const QR_CODE_COMPATABILITY_INFO_ICON = 'qrCodeCompatabilityInfoIcon';
export const QR_CODES_CSV_INFO_ICON = 'qrCodeCsvInfoIcon';

export const QR_PRINT_SECTION = 'qrPrintSection';
export const QR_PRINT_LINK = 'qrPrintLink';

export const QR_CODES_FOR_TABLES_SECTION = 'qrCodesForTablesSection';
export const QR_CODES_FOR_TABLES_TOGGLE = 'qrCodesForTablesToggle';

// Location dashboard - Checkin questions
export const LOCATION_CARD_CHECKIN_QUESTIONS = 'locationCard-additionalData';
export const ADD_CHECKIN_QUESTION_BUTTON = 'addCheckInQuestionButton';
export const CHECKIN_QUESTION_INPUT_FIELD_NUMBER_PREFIX = `checkInQuestionInputField-`;
export const ADDITIONAL_DATA_QUESTION_INPUT_FIELD_NUMBER_PREFIX =
  'additionalDataQuestionInputField-';

// Location dashboard - Subdivision into tables
export const LOCATION_CARD_TABLE_SUBDEVISION = 'locationCard-tableSubdivision';
export const TABLE_SUBDIVISION_TOGGLE = 'tableSubdivisionToggle';

// Location dashboard - Website, Menu and other Links
export const DASHBOARD_WEBSITE_MENU_AND_LINKS =
  'locationCard-websiteMenuAndLinks';
export const LINK_INPUT_FIELD_MENU = 'linkInput-menu';
export const LINK_INPUT_FIELD_WEBSITE = 'linkInput-website';
export const LINK_INPUT_FIELD_SCHEDULE = 'linkInput-schedule';
export const LINK_INPUT_FIELD_MAP = 'linkInput-map';
export const LINK_INPUT_FIELD_GENERAL = 'linkInput-general';
export const WEBSITE_LINKS_SAVE_CHANGES_BUTTON =
  'websiteLinksSaveChangesButton';
export const WEBSITE_MENU_LINKS_SECTION = 'websiteMenuLinksSection';

// Ant design
export const ANT_MODAL_CONTENT = '.ant-modal-content';
export const ANT_VALIDATION_ERROR = '.ant-form-item-explain-error';
export const ANT_NOTIFICATION_NOTICE_WITH_ICON =
  '.ant-notification-notice-with-icon';
export const ANT_POPOVER_BUTTONS = ' .ant-btn.ant-btn-sm';
export const ANT_PRIMARY_POPOVER_BUTTON =
  '.ant-popover-buttons .ant-btn-primary';
export const ANT_MESSAGE_CHECK = '.anticon-check-circle';
export const ANT_MESSAGE_ERROR = '.anticon-close-circle';
export const ANT_FORM_ITEM_ERROR_NOTIFICATION = '.ant-form-item-has-error';
export const ANT_SUCCESS_NOTIFICATION_MESSAGE = '.ant-notification-notice';
export const ANT_COLLAPSE_ITEM_ACTIVE = '.ant-collapse-item-active';
export const ANT_TOOLTIP_OPEN = '.ant-tooltip-open';
export const ANT_CLOSE_MODAL_ICON = '.ant-modal-close-x > .anticon > svg';

// Icons
export const ERROR_ICON = 'span[aria-label="close-circle"]';

// Profile Page
export const PROFILE_PAGE_WRAPPER = 'profilePageWrapper';
export const OPERATOR_NAME = 'operatorName';
export const DIV_ARIA_EXPANDED_TRUE = 'div[aria-expanded="true"]';

// Data Transfers Page
export const DATA_TRANSFERS_PAGE_WRAPPER = 'dataTransfersPageWrapper';
export const DATA_TRANSFERS_TITLE = 'dataTransfersTitle';
export const SHARE_NOW_BUTTON = 'next';

// Archive Page
export const ARCHIVE_PAGE_WRAPPER = 'archivePageWrapper';
export const ARCHIVE_PAGE_INFO = 'archivePageInfo';

// Helpcenter Page
export const HELP_CENTER_TITLE = 'helpCenterTitle';
export const HELP_CENTER_PAGE_WRAPPER = 'helpCenterPageWrapper';

// Scanner
export const VIDEO = 'video';

// Banner
export const BANNER = 'banner';
export const CLOSE_BANNER_BUTTON = 'closeBanner';

// Devices
export const DEVICES_PAGE_WRAPPER = 'devicesPageWrapper';

// Contact form
export const CONTACT_FORM_CARD = 'contactFormCard';
export const ACCEPT_AGB_CHECKBOX = 'acceptAGB';
export const SAVE_BUTTON = 'contactSubmit';
export const FIRST_NAME_INPUT_FIELD = 'firstName';
export const LAST_NAME_INPUT_FIELD = 'lastName';
export const STREET_NAME_INPUT_FIELD = 'street';
export const HOUSE_NUMBER_INPUT_FIELD = 'number';
export const CITY_NAME_INPUT_FIELD = 'city';
export const ZIP_CODE_INPUT_FIELD = 'zip';
export const PHONE_NUMBER_INPUT_FIELD = 'phone';
export const EMAIL_INPUT_FIELD = 'email';

// archive
export const DELETED_GROUP_NAME = 'deletedGroup';
export const REACTIVATE_GROUP_BUTTON = 'reactivateButton';
export const CONTACT_FORM_ADDITIONAL_DATA_DIVIDER =
  'contactFormAdditionalDataDivider';
export const ADDITIONAL_DATA_TABLE_NUMBER_SELECTOR =
  'additionalDataTableNumberSelector';
