import {
  DEVICE_TYPE_ANDROID,
  DEVICE_TYPE_IOS,
  DEVICE_TYPE_WEBAPP,
} from './deviceTypes';

export const E2E_DYNAMIC_TRACE_ID = 'w2RiSHiitpfJ0hxcJz5ZYw==';
export const E2E_FORM_TRACE_ID = 'MPc2dmPJQkfS5uqIc4yG1A==';

export const DYNAMIC_DEVICE_TYPES = {
  DEVICE_TYPE_IOS,
  DEVICE_TYPE_ANDROID,
  DEVICE_TYPE_WEBAPP,
};

export const createGroupPayload = {
  type: 'base',
  name: 'Testing group',
  firstName: 'Torsten',
  lastName: 'Tester',
  phone: '+4917612345678',
  streetName: 'Charlottenstr.',
  streetNr: '59',
  zipCode: '10117',
  city: 'Berlin',
  state: 'Berlin',
  lat: 0,
  lng: 0,
  radius: 0,
  tableCount: null,
  isIndoor: true,
};
