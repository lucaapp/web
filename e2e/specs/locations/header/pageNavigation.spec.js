import { createTestOperator } from '../setup/createOperator';
import { DEFAULT_COOKIE } from '../setup/createOperator.helper';
import { createTestGroups } from '../setup/createLocations';
import { getGroupPayload } from '../setup/createLocations.helper';
import {
  HELP_CENTER_ROUTE,
  DATA_TRANSFERS_ROUTE,
  PROFILE_ROUTE,
  APP_ROUTE,
  DEVICES_ROUTE,
  ARCHIVE_ROUTE,
} from '../constants/routes';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { visit } from '../../utils/commands';

import { urlShouldInclude } from '../../utils/assertions';
import {
  OPEN_HELPCENTER_BUTTON,
  HELP_CENTER_TITLE,
  DEVICES_ICON,
  DEVICES_PAGE_WRAPPER,
  HEADER_DROPDOWN_MENU_TRIGGER,
  MENU_ITEM_PROFILE,
  HEADER_LOGO_HOME,
  MENU_ITEM_ARCHIVE,
  ARCHIVE_PAGE_INFO,
  PROFILE_PAGE_WRAPPER,
  ARCHIVE_PAGE_WRAPPER,
  HELP_CENTER_PAGE_WRAPPER,
  ANT_CLOSE_MODAL_ICON,
  GROUP_NAME,
  LOCATION_NAME,
} from '../constants/selectorKeys';

// base group is decided by group name in ascending alphabet order
let testGroups = [];

describe('Header/Page navigation', () => {
  before(() => {
    createTestOperator();
    createTestGroups([
      getGroupPayload({ name: 'AAA' }),
      getGroupPayload({ name: 'BBB' }),
    ]).then(groups => {
      testGroups = groups;
    });
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
    getDOMElement(ANT_CLOSE_MODAL_ICON).click();
  });

  afterEach(() => {
    cy.clearLocalStorage();
  });

  describe('Open different pages', () => {
    it('should open devices page', () => {
      getByCy(DEVICES_ICON).click();
      urlShouldInclude(DEVICES_ROUTE);
      getByCy(DEVICES_PAGE_WRAPPER).should('be.visible');
    });

    it('should open profile page', () => {
      getByCy(HEADER_DROPDOWN_MENU_TRIGGER).click();
      getByCy(MENU_ITEM_PROFILE).click();
      urlShouldInclude(PROFILE_ROUTE);
      getByCy(PROFILE_PAGE_WRAPPER).should('be.visible');
    });

    it('should open archives page', () => {
      getByCy(HEADER_DROPDOWN_MENU_TRIGGER).click();
      getByCy(MENU_ITEM_ARCHIVE).click();
      urlShouldInclude(ARCHIVE_ROUTE);
      getByCy(ARCHIVE_PAGE_WRAPPER).should('be.visible');
      getByCy(ARCHIVE_PAGE_INFO).should('be.visible');
    });

    it('should open help center page', () => {
      getByCy(OPEN_HELPCENTER_BUTTON).click();
      urlShouldInclude(HELP_CENTER_ROUTE);
      getByCy(HELP_CENTER_PAGE_WRAPPER).should('be.visible');
      getByCy(HELP_CENTER_TITLE).should('be.visible');
    });
  });

  describe('When clicking on Luca Logo', () => {
    it('should open base group location overview from data transfers page', () => {
      visit(DATA_TRANSFERS_ROUTE);
      getByCy(GROUP_NAME).should('not.exist');
      getByCy(HEADER_LOGO_HOME).click();
      urlShouldInclude(APP_ROUTE);
      getByCy(GROUP_NAME).contains(testGroups[0].groupName);
      getByCy(LOCATION_NAME).should('be.visible');
    });
    it('should open base group location overview from profile page', () => {
      visit(PROFILE_ROUTE);
      getByCy(GROUP_NAME).should('not.exist');
      getByCy(HEADER_LOGO_HOME).click();
      urlShouldInclude(APP_ROUTE);
      getByCy(GROUP_NAME).contains(testGroups[0].groupName);
      getByCy(LOCATION_NAME).should('be.visible');
    });
  });
});
