export const type = (element, value) => element.type(value, { delay: 0 });

export const readFile = (file, timeout = 4000) =>
  cy.readFile(file, { timeout });

export const task = (event, argument) => cy.task(event, argument);

export const log = message => cy.log(message);

export const visit = url => cy.visit(url);

export const waitForAlias = (alias, timeout = 5000) =>
  cy.wait(alias, { timeout });

export const deleteFileIfExists = file => cy.task('deleteFileIfExists', file);

export const triggerEvent = (element, event) => {
  element.trigger(event);
};

export const check = element => element.check();

export const clickOutside = () => cy.get('body').click(0, 0);
