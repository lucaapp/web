export const getByCy = (key, timeout = 4000) => cy.getByCy(key, { timeout });

export const getDOMElement = (key, timeout = 4000) => cy.get(key, { timeout });
