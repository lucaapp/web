export const shouldBeVisible = element => element.should('be.visible');

export const shouldNotExist = element => element.should('not.exist');

export const contains = value => cy.contains(value);

export const urlShouldInclude = path => cy.url().should('include', path);

export const shouldBeDisabled = element => element.should('be.disabled');

export const shouldNotBeDisabled = element => element.should('not.be.disabled');

export const shouldBeEmpty = element => element.should('be.empty');

export const shouldHaveAttribute = (element, attribute, attributeValue) =>
  element.should('have.attr', attribute, attributeValue);

export const shouldBeChecked = element => element.should('be.checked');
