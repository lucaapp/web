const { hdBaseUrl } = Cypress.config();

export const HEALTH_DEPARTMENT_BASE_ROUTE = `${hdBaseUrl}/health-department`;
export const HEALTH_DEPARTMENT_APP_ROUTE = `${HEALTH_DEPARTMENT_BASE_ROUTE}/app`;
export const HEALTH_DEPARTMENT_PROFILE_ROUTE = `${HEALTH_DEPARTMENT_BASE_ROUTE}/app/profile`;
export const HEALTH_DEPARTMENT_HELPCENTER_ROUTE = `${HEALTH_DEPARTMENT_BASE_ROUTE}/app/help`;
