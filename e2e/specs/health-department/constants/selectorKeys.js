// General
export const NEXT_BUTTON = 'next';
export const FINISH_BUTTON = 'finish';
export const TRY_AGAIN_BUTTON = 'tryAgain';

// Login
export const DOWNLOAD_PRIVATE_KEY_BUTTON = 'downloadPrivateKey';
export const INPUT_TYPE_FILE = 'input[type="file"]';

// Ant
export const ANT_MODAL = '.ant-modal';
export const ANT_TOOLTIP_OPEN = '.ant-tooltip-open';
export const ANT_POPOVER_BUTTONS = '.ant-popover-buttons button';

// Header
export const HEADER = 'header';
export const VERIFICATION_ICON = 'verificationIcon';
export const PROFILE_ICON = 'profileIcon';
export const HELPCENTER_ICON = 'helpCenterIcon';
export const LINK_MENU_ICON = 'linkMenuIcon';
export const HEADER_DROPDOWN_MENU = 'headerDropdownMenu';
export const HEADER_LICENSE_LINK = 'headerLicenseLink';
export const HEADER_PRIVACY_POLICY_LINK = 'headerPrivacyPolicyLink';

// Profile Page
export const PROFILE_BACK_ICON = 'profileBackIcon';
export const PROFILE_INFORMATION_SECTION = 'profileInformationSection';
export const PROFILE_CHANGE_PASSWORD_SECTION = 'profileChangePasswordSection';

// Help Center Page
export const HELP_CENTER_BACK_ICON = 'helpCenterBackIcon';
export const HELP_CENTER_LINKS_SECTION = 'helpCenterLinksSection';
export const HELP_CENTER_CONTACT_SECTION = 'helpCenterContactSection';

// Group
export const GROUP_PREFIX = 'group_';
export const SEARCH_GROUP_BUTTON = 'searchGroup';
export const GROUP_NAME_INPUT_FIELD = 'groupNameInput';
export const START_GROUP_SEARCH_BUTTON = 'startGroupSearch';
export const REQUEST_GROUP_DATA_BUTTON = 'requestGroupData';
export const PROCESS_ENTRY = 'processEntry';

// Data Requests
export const COMPLETE_DATA_TRANSFER = 'completeDataTransfer';
export const CONTACT_PERSONS_TABLE = '#contactPersonsTable';
export const CONFIRMED_LOCATION_PREFIX = 'confirmedLocation_';
export const CONTACT_LOCATION_PREFIX = 'contactLocation_';
