import {
  HD_CORRECT_PRIVATE_KEY_FILE_NAME,
  HEALTH_DEPARTMENT_PRIVATE_KEY_PATH,
} from '../../constants/keyFiles';
import {
  ANT_MODAL,
  DOWNLOAD_PRIVATE_KEY_BUTTON,
  NEXT_BUTTON,
  FINISH_BUTTON,
  INPUT_TYPE_FILE,
} from '../../constants/selectorKeys';
import { getByCy, getDOMElement } from '../../../utils/selectors';
import { shouldBeVisible } from '../../../utils/assertions';
import { readFile, task, log } from '../../../utils/commands';

export const readHDPrivateKeyFile = (filepath, filename) => {
  readFile(filepath).then(fileContent => {
    getDOMElement(INPUT_TYPE_FILE).attachFile({
      fileContent,
      fileName: filename,
      mimeType: 'text/plain',
    });
  });
};

export const downloadHealthDepartmentPrivateKey = () => {
  shouldBeVisible(getDOMElement(ANT_MODAL));
  getByCy(DOWNLOAD_PRIVATE_KEY_BUTTON, 8000).click();
  shouldBeVisible(getByCy(NEXT_BUTTON)).click();
  shouldBeVisible(getByCy(FINISH_BUTTON)).click();
};

export const uploadHealthDepartmentPrivateKeyFile = () => {
  shouldBeVisible(getDOMElement(ANT_MODAL));
  readHDPrivateKeyFile(
    HEALTH_DEPARTMENT_PRIVATE_KEY_PATH,
    HD_CORRECT_PRIVATE_KEY_FILE_NAME
  );
};

export const removeHealthDepartmentPrivateKeyFile = () => {
  task('deleteFileIfExists', HEALTH_DEPARTMENT_PRIVATE_KEY_PATH);
};

export const addHealthDepartmentPrivateKeyFile = () => {
  task('fileExists', HEALTH_DEPARTMENT_PRIVATE_KEY_PATH).then(exists => {
    if (!exists) {
      log('Private key should be downloaded');
      downloadHealthDepartmentPrivateKey();
    } else {
      uploadHealthDepartmentPrivateKeyFile();
    }
  });
};
