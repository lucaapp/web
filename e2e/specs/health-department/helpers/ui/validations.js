export const verifyLinkOpensInNewTab = element => {
  element.then(link => {
    cy.request(link.prop('href')).its('status').should('eq', 200);
  });
};
