import { shouldBeVisible } from '../../../utils/assertions';
import { getDOMElement } from '../../../utils/selectors';
import { type } from '../../../utils/commands';

export const setDatePickerTime = () => {
  cy.get('.ant-picker-dropdown')
    .not('.ant-picker-dropdown-hidden')
    .should('be.visible')
    .within(() => {
      cy.get('.ant-picker-time-panel-cell').eq(0).click().type('{enter}');
    });
};

export const setDatePickerStartDate = startDate => {
  shouldBeVisible(getDOMElement('#startDate')).click();
  type(getDOMElement('#startDate'), `${startDate}{enter}`);
  shouldBeVisible(getDOMElement('#startTime')).click();
  setDatePickerTime();
};

export const setDatePickerEndDate = endDate => {
  shouldBeVisible(getDOMElement('#endDate')).click({ force: true });
  type(getDOMElement('#endDate'), `${endDate}{enter}`);
  shouldBeVisible(getDOMElement('#endTime')).click();
  setDatePickerTime();
};
