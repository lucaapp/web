import { E2E_INTERNAL_ACCESS_TOKEN } from '../../utils/tokens';
import {
  addHealthDepartmentPrivateKeyFile,
  removeHealthDepartmentPrivateKeyFile,
} from '../helpers/ui/handlePrivateKeyFile';
import { getHealthDepartmentAdminPayload } from './setup.helper';
import { KEY_ROTATION, TRACING_PROCESS } from '../constants/aliases';
import { loginHealthDepartment } from '../helpers/api/auth';
import { signHealthDepartment } from '../helpers/api/signHealthDepartment';

export const removeHDPrivateKeyFile = () => {
  cy.request({
    method: 'POST',
    url: '/api/internal/end2end/clean',
    headers: {
      'internal-access-authorization': E2E_INTERNAL_ACCESS_TOKEN,
    },
  });
  removeHealthDepartmentPrivateKeyFile();
};

export const setupUnsignedHealthDepartment = () => {
  removeHealthDepartmentPrivateKeyFile();

  cy.request({
    method: 'POST',
    url: '/api/internal/end2end/unsignHealthDepartment',
    headers: {
      'internal-access-authorization': E2E_INTERNAL_ACCESS_TOKEN,
    },
  });
};

export const createHealthDepartmentAdmin = () => {
  return new Cypress.Promise(resolve => {
    // eslint-disable-next-line require-await
    cy.request({
      method: 'GET',
      url: '/api/internal/end2end/getHealthDepartment',
      headers: {
        'internal-access-authorization': E2E_INTERNAL_ACCESS_TOKEN,
      },
    }).then(
      // eslint-disable-next-line require-await
      async response => {
        // eslint-disable-next-line promise/no-nesting
        cy.request({
          method: 'POST',
          url: '/api/internal/end2end/healthDepartmentEmployees',
          headers: {
            'internal-access-authorization': E2E_INTERNAL_ACCESS_TOKEN,
          },
          body: getHealthDepartmentAdminPayload(response.body.departmentId),
        }).then(
          // eslint-disable-next-line require-await, no-shadow
          async response => {
            const admin = response.body;
            resolve(admin);
          }
        );
      }
    );
  });
};

export const setupSignedHdWithKeyRoation = () => {
  cy.intercept('POST', '/api/v4/keys/daily/rotate').as(KEY_ROTATION);
  cy.intercept('POST', '/api/v4/locationTransfers/').as(TRACING_PROCESS);
  removeHDPrivateKeyFile();
  loginHealthDepartment();
  addHealthDepartmentPrivateKeyFile();
  signHealthDepartment();
  cy.reload();
  addHealthDepartmentPrivateKeyFile();
  cy.wait(`@${KEY_ROTATION}`);
  cy.logoutHD();
};
