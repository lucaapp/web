import faker from 'faker';

export const getHealthDepartmentAdminPayload = departmentId => ({
  firstName: faker.name.firstName().replace("'", ' '),
  lastName: faker.name.lastName().replace("'", ' '),
  phone: faker.phone.phoneNumber('0049176#######'),
  email: faker.internet.email(),
  isAdmin: true,
  departmentId,
});

export const DEFAULT_COOKIE = '__Secure-connect.sid';
