import moment from 'moment';
import {
  fillForm,
  getAdditionalDataQuestionInputFieldByIndex,
  getCheckinQuestionInputFieldByIndex,
} from './helpers/contactForm.helper';
import { loginHealthDepartment } from '../health-department/helpers/api/auth';
import {
  removeHDPrivateKeyFile,
  setupSignedHdWithKeyRoation,
} from '../health-department/setup/setup';

import {
  setDatePickerStartDate,
  setDatePickerEndDate,
} from '../health-department/helpers/ui/tracking';
import {
  APP_ROUTE,
  CONTACT_FORM_ROUTE,
  DATA_TRANSFERS_ROUTE,
} from '../locations/constants/routes';
import { addHealthDepartmentPrivateKeyFile } from '../health-department/helpers/ui/handlePrivateKeyFile';
import { getByCy, getDOMElement } from '../utils/selectors';
import {
  ADD_CHECKIN_QUESTION_BUTTON,
  LOCATION_CARD_CHECKIN_QUESTIONS,
  SHARE_DATA_ERROR_MESSAGE,
  SHARE_DATA_FEEDBACK,
  SHARE_NOW_BUTTON,
  TABLE_SUBDIVISION_TOGGLE,
} from '../locations/constants/selectorKeys';
import {
  SEARCH_GROUP_BUTTON,
  ANT_MODAL,
  GROUP_NAME_INPUT_FIELD,
  START_GROUP_SEARCH_BUTTON,
  REQUEST_GROUP_DATA_BUTTON,
  PROCESS_ENTRY,
  COMPLETE_DATA_TRANSFER,
  CONTACT_PERSONS_TABLE,
  GROUP_PREFIX,
  CONFIRMED_LOCATION_PREFIX,
  CONTACT_LOCATION_PREFIX,
  ANT_POPOVER_BUTTONS,
} from '../health-department/constants/selectorKeys';
import { shouldBeVisible } from '../utils/assertions';
import { clickOutside, type, visit } from '../utils/commands';
import { createTestOperator } from '../locations/setup/createOperator';
import {
  STANDARD_PASSWORD,
  login,
} from '../locations/setup/createOperator.helper';
import { createTestGroup } from '../locations/setup/createLocations';
import { getGroupPayload } from '../locations/setup/createLocations.helper';
import {
  PROCESS_UUID,
  TRACING_PROCESS,
} from '../health-department/constants/aliases';
import {
  CHECKIN_QUESTION_1,
  CHECKIN_QUESTION_2,
} from '../locations/constants/locations';

const yesterdayDate = moment().subtract(1, 'days').format('DD.MM.YYYY');
const tomorrowDate = moment().add(1, 'days').format('DD.MM.YYYY');

let testOperator;
let testGroup;

context('Workflow', { retries: 2 }, () => {
  describe('when the Luca workflow will checked with contact form data', () => {
    beforeEach(() => {
      setupSignedHdWithKeyRoation();
      // create location and operator
      createTestOperator(true).then(operator => {
        testOperator = operator;
      });
      createTestGroup(getGroupPayload()).then(group => {
        testGroup = group;
      });

      visit(APP_ROUTE);
      // Add table count
      getByCy(TABLE_SUBDIVISION_TOGGLE).click();

      // Add checkin questions
      getByCy(LOCATION_CARD_CHECKIN_QUESTIONS).click();
      getByCy(ADD_CHECKIN_QUESTION_BUTTON).click();
      getCheckinQuestionInputFieldByIndex(1).type(CHECKIN_QUESTION_1);
      clickOutside();

      getByCy(ADD_CHECKIN_QUESTION_BUTTON).click();
      getCheckinQuestionInputFieldByIndex(2).type(CHECKIN_QUESTION_2);
      clickOutside();
    });

    afterEach(() => {
      removeHDPrivateKeyFile();
    });

    it('decrypt all data successfully', () => {
      // Checkin with contact form
      cy.visit(`${CONTACT_FORM_ROUTE}/${testGroup.formId}`);

      // Additional Data fields should be visible in contact form
      getByCy('additionalDataTableNumberSelector').should('be.visible');

      getAdditionalDataQuestionInputFieldByIndex(1).should('be.visible');
      getAdditionalDataQuestionInputFieldByIndex(2).should('be.visible');

      const users = [];
      for (let index = 0; index < 2; index += 1) {
        users.push(fillForm());
      }
      // Request data from testing Location
      loginHealthDepartment();
      addHealthDepartmentPrivateKeyFile();
      getByCy(SEARCH_GROUP_BUTTON).click();
      shouldBeVisible(getDOMElement(ANT_MODAL));
      type(getByCy(GROUP_NAME_INPUT_FIELD), testGroup.groupName);
      getByCy(START_GROUP_SEARCH_BUTTON).click();
      getByCy(GROUP_PREFIX + testGroup.groupName).click();
      setDatePickerStartDate(yesterdayDate);
      setDatePickerEndDate(tomorrowDate);
      // submit process opening
      getByCy(REQUEST_GROUP_DATA_BUTTON).click();
      // Wait for request to complete
      cy.wait(`@${TRACING_PROCESS}`);
      getDOMElement(`@${TRACING_PROCESS}`).then(({ response }) => {
        cy.wrap(response.body.tracingProcessId).as(PROCESS_UUID);
      });
      // Find and get the entry
      getDOMElement(`@${PROCESS_UUID}`).then(uuid => {
        getByCy(`${PROCESS_ENTRY}-${uuid}`).click();
      });
      getByCy(CONTACT_LOCATION_PREFIX + testGroup.groupName)
        .first()
        .click();
      shouldBeVisible(getDOMElement(ANT_POPOVER_BUTTONS));
      getDOMElement(ANT_POPOVER_BUTTONS)
        .eq(1)
        .then($element => {
          cy.wrap($element).click();
        });
      cy.logoutHD();
      // Share testing data from testing Location
      login({ username: testOperator.email, password: STANDARD_PASSWORD });
      cy.visit(DATA_TRANSFERS_ROUTE);
      cy.stubNewWindow();
      getByCy(COMPLETE_DATA_TRANSFER, 10000).first().click();
      getByCy(SHARE_NOW_BUTTON, 10000).click();
      getByCy(SHARE_DATA_FEEDBACK, { timeout: 20000 }).should('be.visible');
      getByCy(SHARE_DATA_ERROR_MESSAGE).should('not.exist');
      cy.logoutLocations();
      // Check requested data in Health department
      loginHealthDepartment();
      addHealthDepartmentPrivateKeyFile();
      getDOMElement(`@${PROCESS_UUID}`).then(uuid => {
        getByCy(`${PROCESS_ENTRY}-${uuid}`).click();
      });
      getByCy(CONFIRMED_LOCATION_PREFIX + testGroup.groupName, {
        timeout: 10000,
      })
        .first()
        .click();
      shouldBeVisible(getDOMElement(ANT_MODAL));
      for (const user of users) {
        getDOMElement(CONTACT_PERSONS_TABLE, 10000).should(
          'contain',
          user.lastName
        );
        getDOMElement(CONTACT_PERSONS_TABLE).should('contain', user.firstName);
        getDOMElement(CONTACT_PERSONS_TABLE).should(
          'contain',
          user.phoneNumber
        );
      }
    });
  });
});
