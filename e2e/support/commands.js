// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import { CONNECTION_STRING } from './database';
import {
  E2E_HEALTH_DEPARTMENT_PASSWORD,
  E2E_HEALTH_DEPARTMENT_USERNAME,
} from '../specs/health-department/constants/user';

const { baseUrl, hdBaseUrl } = Cypress.config();

Cypress.Commands.add('getByCy', (selector, ...arguments_) => {
  return cy.get(`[data-cy="${selector}"]`, ...arguments_);
});

Cypress.Commands.add('executeQuery', query => {
  return cy.task('dbQuery', { query, connection: CONNECTION_STRING });
});

Cypress.Commands.add('stubNewWindow', () => {
  cy.window().then(win => {
    cy.stub(win, 'open').callsFake(link => {
      // eslint-disable-next-line no-param-reassign
      win.location.href = link;
    });
  });
});

// eslint-disable-next-line consistent-return
Cypress.Commands.overwrite('visit', (originalFunction, url, options) => {
  if (url.includes(hdBaseUrl)) {
    cy.window().then(win => {
      return win.open(url, '_self');
    });
  } else {
    return originalFunction(url, options);
  }
});

// Locations
Cypress.Commands.add('loginLocations', (username, password) => {
  cy.request({
    method: 'POST',
    url: 'api/v4/auth/operator/login',
    failOnStatusCode: false,
    body: {
      username,
      password,
    },
    headers: {
      origin: baseUrl,
    },
  });
});

Cypress.Commands.add('logoutLocations', () => {
  cy.request({
    method: 'POST',
    url: 'api/v4/auth/logout',
    headers: {
      origin: baseUrl,
    },
  });
});

// Health Department
Cypress.Commands.add(
  'basicLoginHD',
  (
    username = E2E_HEALTH_DEPARTMENT_USERNAME,
    password = E2E_HEALTH_DEPARTMENT_PASSWORD
  ) => {
    cy.request({
      method: 'POST',
      url: 'api/v4/auth/healthDepartmentEmployee/login',
      failOnStatusCode: false,
      body: {
        username,
        password,
      },
      headers: {
        origin: hdBaseUrl,
      },
    });
  }
);

Cypress.Commands.add('logoutHD', () => {
  cy.request({
    method: 'POST',
    url: 'api/v4/auth/logout',
    headers: {
      origin: hdBaseUrl,
    },
  });
});
