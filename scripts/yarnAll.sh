#!/bin/sh
set -euo pipefail

SERVICES="backend backend-attestation backend-id contact-form discovery register-badge health-department locations scanner webapp guide"

for SERVICE in ${SERVICES}
do
  pushd "services/$SERVICE"
  yarn "$@"
  popd
done
