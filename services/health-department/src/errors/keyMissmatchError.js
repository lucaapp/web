export class KeyMissmatchError extends Error {
  constructor(keyId, expectedPublicKey, recievedPublicKey) {
    super(
      `Encrypted daily private key with keyId ${keyId} does not match daily public key. Expected ${expectedPublicKey}, received ${recievedPublicKey}`
    );
  }
}
