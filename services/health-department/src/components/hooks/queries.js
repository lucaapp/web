import { useQuery } from 'react-query';
import { getValidatedExtractedPublicDailyKey } from 'utils/dailyKeys';

const QUERY_KEYS = {
  DAILY_KEY: 'DAILY_KEY',
};

export const useDailyKey = () =>
  useQuery(QUERY_KEYS.DAILY_KEY, getValidatedExtractedPublicDailyKey);
