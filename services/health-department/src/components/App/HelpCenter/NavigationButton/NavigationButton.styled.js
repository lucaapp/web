import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-bottom: 32px;
`;

export const Text = styled.div`
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;
