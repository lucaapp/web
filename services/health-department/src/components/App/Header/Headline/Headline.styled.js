import styled from 'styled-components';

export const SubTitle = styled.div`
  display: flex;
  align-items: flex-end;
  margin-left: 24px;
  margin-bottom: -4px;
  font-family: Montserrat-SemiBold, sans-serif;
  font-size: 16px;
  font-weight: 600;
`;

export const Title = styled.div`
  display: flex;
  align-items: baseline;
`;

export const VerificationWrapper = styled.div`
  margin-left: 16px;
`;

export const Logo = styled.img`
  height: 48px;
  color: black;
`;
