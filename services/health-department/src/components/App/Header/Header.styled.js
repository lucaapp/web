import styled from 'styled-components';

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  margin: 0 40px;
`;

export const MenuWrapper = styled.div`
  margin-top: 10px;
  display: flex;
`;
