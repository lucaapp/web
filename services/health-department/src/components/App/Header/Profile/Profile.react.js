import React from 'react';
import { Badge } from 'antd';
import Icon from '@ant-design/icons';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

// API
import { getSigningTool } from 'network/api';

// Constants
import { PROFILE_ROUTE } from 'constants/routes';

import { ProfileIcon, ProfileActiveIcon } from 'assets/icons';

import { IconWrapper, badgeStyle } from './Profile.styled';

const getProfileIcon = isProfileRoute => (
  <Icon
    data-cy="profileIcon"
    component={isProfileRoute ? ProfileActiveIcon : ProfileIcon}
    style={{ color: 'black', fontSize: 32 }}
  />
);

export const Profile = ({ healthDepartment }) => {
  const history = useHistory();
  const intl = useIntl();
  const currentRoute = useSelector(state => state.router.location.pathname);
  const isProfileRoute = currentRoute === PROFILE_ROUTE;
  const handleClick = () => history.push(PROFILE_ROUTE);

  const { data: signingTool = [] } = useQuery('signingTool', () =>
    getSigningTool()
  );

  const showDot =
    signingTool.length > 0 &&
    !healthDepartment.signedPublicHDEKP &&
    !healthDepartment.signedPublicHDSKP;

  return (
    <Badge dot={showDot} style={badgeStyle}>
      <IconWrapper
        title={intl.formatMessage({
          id: 'profile',
        })}
        onClick={handleClick}
      >
        {getProfileIcon(isProfileRoute)}
      </IconWrapper>
    </Badge>
  );
};
