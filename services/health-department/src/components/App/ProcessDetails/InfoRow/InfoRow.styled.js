import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
`;

export const Title = styled.div`
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const Value = styled.div`
  font-size: 16px;
  font-weight: 500;
`;

export const AttributeWrapper = styled.div`
  margin-right: 40px;
`;
