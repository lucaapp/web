import styled from 'styled-components';
import Icon from '@ant-design/icons';

import { BellIcon } from 'assets/icons';

export const ButtonWrapper = styled.div`
  text-align: left;
`;

export const BellIconComp = styled(Icon).attrs({ component: BellIcon })`
  color: black;
  font-size: 32px;
  margin-left: 32px;
  cursor: ${({ disabled }) => (disabled ? 'not-allowed' : 'pointer')};
  opacity: ${({ disabled }) => (disabled ? '50%' : '100%')};
`;
