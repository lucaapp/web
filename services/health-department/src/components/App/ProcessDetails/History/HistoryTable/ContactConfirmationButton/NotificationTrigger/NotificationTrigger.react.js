import React from 'react';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';

import {
  getWarningLevelsForLocationTransfer,
  getLocationTransferTraces,
  getMe,
} from 'network/api';

import { useModal } from 'components/hooks/useModal';
import { NotificationModal } from 'components/App/modals/NotificationModal';

import { StyledTooltip } from 'components/general/StyledTooltip';
import { BellIconComp, ButtonWrapper } from './NotificationTrigger.styled';
import { checkIfAnyContactPersonsAreNotifiable } from './NotificationTrigger.helper';

export const NotificationTrigger = ({ location, isNoCheckins }) => {
  const intl = useIntl();
  const [openModal] = useModal();
  const {
    uuid: locationId,
    name: locationName,
    transferId: locationTransferId,
    time,
  } = location;

  const { data: riskLevels } = useQuery(
    ['getWarningLevelsForLocationTransfer', { locationTransferId }],
    () => getWarningLevelsForLocationTransfer(locationTransferId),
    { refetchOnWindowFocus: false }
  );

  const { data: contactPersons } = useQuery(
    ['contactPersons', { locationTransferId }],
    () => getLocationTransferTraces(locationTransferId),
    { refetchOnWindowFocus: false }
  );

  const { data: healthDepartmentEmployee } = useQuery('me', getMe, {
    refetchOnWindowFocus: false,
  });

  const openNotificationModal = () =>
    openModal({
      title: intl.formatMessage({
        id: 'modal.notification.title',
      }),
      content: (
        <NotificationModal
          locationId={locationId}
          locationName={locationName}
          locationTransferId={locationTransferId}
          time={time}
          departmentId={healthDepartmentEmployee.departmentId}
        />
      ),
      wide: true,
    });

  if (
    !riskLevels ||
    !contactPersons ||
    !healthDepartmentEmployee ||
    !healthDepartmentEmployee.notificationsEnabled
  )
    return null;

  const isNotificationEnabled = checkIfAnyContactPersonsAreNotifiable(
    contactPersons,
    riskLevels
  );

  const tooltipTitle = intl.formatMessage({
    id: isNoCheckins
      ? 'contactPersons.tooltip.notificationTrigger.emptyCheckins'
      : isNotificationEnabled
      ? 'modal.notification.button'
      : 'contactPersons.tooltip.notificationTrigger.notEnabled',
  });

  const tooltipPlacement =
    isNoCheckins || !isNotificationEnabled ? 'topLeft' : 'top';

  return (
    <ButtonWrapper
      onClick={isNotificationEnabled ? openNotificationModal : () => {}}
    >
      <StyledTooltip title={tooltipTitle} placement={tooltipPlacement}>
        <BellIconComp disabled={!isNotificationEnabled} />
      </StyledTooltip>
    </ButtonWrapper>
  );
};
