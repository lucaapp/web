import React from 'react';
import { useIntl } from 'react-intl';
import { Popconfirm } from 'antd';
import moment from 'moment';
import { useQuery } from 'react-query';

import { PrimaryButton, SuccessButton } from 'components/general';
import { FlexWrapper } from 'components/App/modals/ContactPersonsModal/ContactPersons/ContactPersons.styled';
import { DATE_FORMAT } from 'utils/time';
import { getLocationTransferTraces } from 'network/api';
import { StyledTooltip } from 'components/general/StyledTooltip';
import { NotificationTrigger } from './NotificationTrigger';
import { SimilarDataRequestButton } from './SimilarDataRequestButton';
import {
  Expiry,
  ButtonWrapper,
  Wrapper,
} from './ContactConfirmationButton.styled';

const MAX_NR_TRANSFER_TRACES = 10000;

export const ContactConfirmationButton = ({ location, callback }) => {
  const intl = useIntl();
  const {
    contactedAt,
    isCompleted,
    time,
    name: locationName,
    transferId,
  } = location;

  const { data: contactPersons } = useQuery(
    ['contactPersons', { transferId }],
    () => getLocationTransferTraces(transferId),
    { refetchOnWindowFocus: false }
  );

  const renderExpiration = () => (
    <Expiry>{`${intl.formatMessage({
      id: 'history.expiry',
    })}: ${moment.unix(time[0]).add(28, 'days').format(DATE_FORMAT)}`}</Expiry>
  );

  const hasCheckins = contactPersons?.traces?.length > 0;

  if (!hasCheckins) {
    return (
      <FlexWrapper>
        <StyledTooltip
          title={intl.formatMessage({
            id: 'contactPersons.tooltip.emptyCheckins.contactButton',
          })}
        >
          <ButtonWrapper>
            <PrimaryButton disabled style={{ pointerEvents: 'none' }}>
              {intl.formatMessage({ id: 'history.contact' })}
            </PrimaryButton>
          </ButtonWrapper>
        </StyledTooltip>
        <NotificationTrigger isNoCheckins location={location} />
        <SimilarDataRequestButton location={location} />
      </FlexWrapper>
    );
  }

  if (!isCompleted && !!contactedAt) {
    return (
      <FlexWrapper>
        <ButtonWrapper>
          <PrimaryButton disabled>
            {intl.formatMessage({ id: 'history.contacted' })}
          </PrimaryButton>
          {renderExpiration()}
        </ButtonWrapper>
        <NotificationTrigger location={location} />
        <SimilarDataRequestButton location={location} />
      </FlexWrapper>
    );
  }

  if (isCompleted) {
    return (
      <FlexWrapper>
        <ButtonWrapper>
          <SuccessButton
            data-cy={`confirmedLocation_${locationName}`}
            onClick={() => callback(location)}
          >
            {intl.formatMessage({ id: 'history.confirmed' })}
          </SuccessButton>
          {renderExpiration()}
        </ButtonWrapper>
        <NotificationTrigger location={location} />
        <SimilarDataRequestButton location={location} />
      </FlexWrapper>
    );
  }

  return (
    <Wrapper>
      <Popconfirm
        placement="top"
        disabled={isCompleted}
        onConfirm={() => callback(location)}
        title={intl.formatMessage(
          {
            id:
              contactPersons?.traces?.length >= MAX_NR_TRANSFER_TRACES
                ? 'modal.dataRequest.checkinOverload'
                : 'modal.dataRequest.confirmation',
          },
          {
            venue: locationName,
            br: <br />,
          }
        )}
        okText={intl.formatMessage({
          id: 'modal.dataRequest.confirmButton',
        })}
        cancelText={intl.formatMessage({
          id: 'modal.dataRequest.declineButton',
        })}
      >
        <ButtonWrapper>
          <PrimaryButton
            disabled={!isCompleted && !!contactedAt}
            data-cy={`contactLocation_${locationName}`}
            onClick={() => {
              if (contactedAt) callback(location);
            }}
          >
            {intl.formatMessage({ id: 'history.contact' })}
          </PrimaryButton>
          {renderExpiration()}
        </ButtonWrapper>
      </Popconfirm>
      <NotificationTrigger location={location} />
      <SimilarDataRequestButton location={location} />
    </Wrapper>
  );
};
