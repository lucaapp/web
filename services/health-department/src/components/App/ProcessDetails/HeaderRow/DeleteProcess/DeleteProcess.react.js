import React from 'react';
import { notification, Popconfirm } from 'antd';
import { useIntl } from 'react-intl';
import { deleteProcess } from 'network/api';
import { useHistory } from 'react-router-dom';
import { TRACKING_ROUTE } from 'constants/routes';
import { StyledButton } from './DeleteProcess.styled';

export const DeleteProcess = ({ process }) => {
  const intl = useIntl();
  const history = useHistory();

  const handleError = () =>
    notification.error({
      message: intl.formatMessage({ id: 'deleteProcess.error' }),
    });

  const onDeleteProcess = () => {
    deleteProcess(process.uuid)
      .then(response => {
        if (response.status !== 204) {
          handleError();
          return;
        }
        notification.success({
          message: intl.formatMessage({ id: 'deleteProcess.success' }),
        });
        history.push(TRACKING_ROUTE);
      })
      .catch(handleError);
  };

  return (
    <Popconfirm
      placement="bottomLeft"
      title={intl.formatMessage({ id: 'table.header.deleteProcess' })}
      onConfirm={onDeleteProcess}
      okText={intl.formatMessage({ id: 'deleteProcess.yes' })}
      cancelText={intl.formatMessage({ id: 'deleteProcess.cancel' })}
    >
      <StyledButton data-cy="deleteProcess">
        {intl.formatMessage({ id: 'processTable.deleteProcess' })}
      </StyledButton>
    </Popconfirm>
  );
};
