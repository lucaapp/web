import styled from 'styled-components';

export const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100vw;
  padding-top: 40px;
  background-color: #f5f5f4;
`;
