import React, { useState, useEffect } from 'react';
import { useQuery } from 'react-query';
import Mark from 'mark.js';

import { pick, values } from 'lodash';

// Api
import { getEmployees } from 'network/api';

// Components
import { AddEmployeeButton } from './AddEmployeeButton';
import { EmployeeSearch } from './EmployeeSearch';
import { TableHeader } from './TableHeader';
import { EmployeeTable } from './EmployeeTable';
import { TableWrapper, HeaderWrapper } from './EmployeeList.styled';

export const EmployeeList = ({ profileData }) => {
  const [searchTerm, setSearchTerm] = useState('');
  const [filteredEmployees, setFilteredEmployees] = useState(null);

  const { isLoading, error, data: employees, refetch } = useQuery(
    'employees',
    () => getEmployees(),
    { refetchOnWindowFocus: false }
  );

  const filterEmployeesByValue = (employeeList, search) => {
    if (!search) {
      return employeeList;
    }

    return employeeList.filter(employee => {
      const employeeValues = values(
        pick(employee, ['firstName', 'lastName', 'email', 'phone'])
      );

      return employeeValues
        .join(' ')
        .toLowerCase()
        .includes(search.toString().toLowerCase());
    });
  };

  // mark search results
  useEffect(() => {
    if (!searchTerm) return () => {};
    const mark = new Mark(document.querySelector('#employeeTable'));
    mark.mark(searchTerm);

    return () => mark.unmark();
  }, [searchTerm]);

  useEffect(() => {
    if (!employees) return;
    setFilteredEmployees(
      filterEmployeesByValue(
        employees.filter(employee => employee.uuid !== profileData.employeeId),
        searchTerm
      )
    );
  }, [employees, searchTerm, profileData.employeeId]);

  if (isLoading || error || !filteredEmployees) {
    return null;
  }

  return (
    <>
      <HeaderWrapper>
        <EmployeeSearch
          onSearch={value => setSearchTerm(value.toLowerCase())}
        />
        <AddEmployeeButton />
      </HeaderWrapper>
      <TableWrapper>
        <TableHeader />
        <EmployeeTable
          searchTerm={searchTerm}
          filteredEmployees={filteredEmployees}
          refetch={refetch}
          profileData={profileData}
        />
      </TableWrapper>
    </>
  );
};
