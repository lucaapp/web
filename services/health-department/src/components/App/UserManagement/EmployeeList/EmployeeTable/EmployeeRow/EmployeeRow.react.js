import React, { useState } from 'react';
import { useIntl } from 'react-intl';

import { Form, notification } from 'antd';

// Api
import { updateEmployee } from 'network/api';

import { getFormattedPhoneNumber } from 'utils/checkPhoneNumber';

// Components
import { RowContent } from './RowContent';

export const EmployeeRow = ({ employee, refetch }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [editing, setEditing] = useState(null);

  const onEdit = ({ firstName, lastName, phone }) => {
    const formattedPhone = getFormattedPhoneNumber(phone);
    updateEmployee({
      employeeId: editing,
      data: { firstName, lastName, phone: formattedPhone },
    })
      .then(() => {
        refetch();
        form.setFieldsValue({ phone: formattedPhone });
        setEditing(null);
        notification.success({
          message: intl.formatMessage({
            id: 'userManagement.updateEmployee.success',
          }),
        });
      })
      .catch(() =>
        notification.error({
          message: intl.formatMessage({
            id: 'userManagement.updateEmployee.error',
          }),
        })
      );
  };

  return employee.uuid === editing ? (
    <Form
      onFinish={onEdit}
      initialValues={{
        firstName: employee.firstName,
        lastName: employee.lastName,
        phone: employee.phone,
      }}
      form={form}
    >
      <RowContent
        employee={employee}
        refetch={refetch}
        setEditing={setEditing}
        editing={editing}
      />
    </Form>
  ) : (
    <RowContent
      employee={employee}
      refetch={refetch}
      setEditing={setEditing}
      editing={editing}
    />
  );
};
