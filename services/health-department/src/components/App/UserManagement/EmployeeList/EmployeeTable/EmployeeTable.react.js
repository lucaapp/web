import React from 'react';

// Components
import { EmptyEmployeeList } from './EmptyEmployeeList';
import { ProfileRow } from './ProfileRow';
import { EmployeeRow } from './EmployeeRow';

export const EmployeeTable = ({
  searchTerm,
  filteredEmployees,
  refetch,
  profileData,
}) => {
  return (
    <div id="employeeTable">
      <ProfileRow searchTerm={searchTerm} profileData={profileData} />
      {filteredEmployees.map(employee => (
        <EmployeeRow
          key={employee.uuid}
          employee={employee}
          refetch={refetch}
        />
      )) || <EmptyEmployeeList />}
    </div>
  );
};
