import React from 'react';

// Components
import { SelectRole } from './SelectRole';
import { EmployeeActions } from './EmployeeActions';
import { EmployeeName } from './EmployeeName';
import { EmployeePhone } from './EmployeePhone';
import { Row, Column } from '../../../EmployeeList.styled';

export const RowContent = ({ employee, refetch, editing, setEditing }) => {
  return (
    <Row data-cy={`employeeEntry-${employee.uuid}`}>
      <Column flex="25%">
        <EmployeeName editing={employee.uuid === editing} employee={employee} />
      </Column>
      <Column flex="25%">{employee.email}</Column>
      <Column flex="20%">
        <EmployeePhone
          editing={employee.uuid === editing}
          employee={employee}
        />
      </Column>
      <Column flex="15%">
        <SelectRole employee={employee} />
      </Column>
      <Column flex="15%" align="flex-end">
        {!employee.isAdmin && (
          <EmployeeActions
            employee={employee}
            refetch={refetch}
            setEditing={setEditing}
            editing={employee.uuid === editing}
          />
        )}
      </Column>
    </Row>
  );
};
