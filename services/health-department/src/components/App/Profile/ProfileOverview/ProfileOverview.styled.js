import styled from 'styled-components';

export const ItemsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 65%;
`;

export const ItemWrapper = styled.div`
  display: flex;
  margin-bottom: 16px;
`;

export const InformationValue = styled.div`
  margin-right: 16px;
  text-align: left;
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-weight: 500;
`;

export const InformationKey = styled.div`
  font-weight: bold;
  width: 250px;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
`;
