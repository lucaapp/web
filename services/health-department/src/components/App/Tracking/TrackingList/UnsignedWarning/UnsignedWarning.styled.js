import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
`;

export const Card = styled.div`
  background: rgb(255, 255, 255);
  border-radius: 4px;
  padding: 40px 32px;
  box-shadow: 0 2px 4px 0 rgba(78, 97, 128, 0.5);
`;

export const Title = styled.div`
  font-family: Montserrat-Medium, sans-serif;
  font-size: 34px;
  font-weight: 500;
  margin-bottom: 24px;
`;

export const Text = styled.div`
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
`;
