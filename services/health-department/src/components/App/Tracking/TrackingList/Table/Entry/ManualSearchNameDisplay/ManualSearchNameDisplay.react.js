import React from 'react';
import moment from 'moment';
import { DATE_FORMAT, TIME_FORMAT } from 'utils/time';
import { useGetEnrichedLocationTransfers } from 'components/hooks/useLocationTransfers';

export const ManualSearchNameDisplay = ({ processId, onProcessName }) => {
  const [enrichedLocationTransfers] = useGetEnrichedLocationTransfers(
    processId
  );

  if (!enrichedLocationTransfers.length) return null;

  const baseLocation = enrichedLocationTransfers.find(
    location => !location.locationName
  );
  onProcessName(processId, baseLocation.name);

  return (
    <div>
      <div>{baseLocation.name}</div>
      <div>
        {`(${moment
          .unix(baseLocation.time[0])
          .format(`${DATE_FORMAT} ${TIME_FORMAT}`)} -`}
      </div>
      <div>{`${moment
        .unix(baseLocation.time[1])
        .format(`${DATE_FORMAT} ${TIME_FORMAT}`)})`}</div>
    </div>
  );
};
