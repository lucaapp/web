import React, { useState, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { Alert } from 'antd';
import { INITIAL_OVERLAP_VALUE } from 'constants/timeOverlap';

import { GuestListTable } from './GuestListTable';
import { OverlappingTime } from './OverlappingTime';
import { filterForTimeOverlap } from './GuestList.helper';

export const GuestList = ({
  decryptedTraces,
  indexPersonData,
  setSelectedTraces,
  location,
  emptyTracesCount,
}) => {
  const [filteredTraces, setFilteredTraces] = useState(null);
  const [minTimeOverlap, setMinTimeOverlap] = useState(INITIAL_OVERLAP_VALUE);
  const intl = useIntl();

  useEffect(() => {
    if (!decryptedTraces) return;
    if (!indexPersonData) {
      setFilteredTraces(decryptedTraces);
      return;
    }
    setFilteredTraces(
      filterForTimeOverlap(decryptedTraces, minTimeOverlap, indexPersonData)
    );
  }, [minTimeOverlap, indexPersonData, decryptedTraces]);

  if (!filteredTraces) return null;

  return (
    <>
      {!!indexPersonData && (
        <OverlappingTime setMinTimeOverlap={setMinTimeOverlap} />
      )}
      <GuestListTable
        location={location}
        traces={filteredTraces}
        indexPersonData={indexPersonData}
        setSelectedTraces={setSelectedTraces}
      />
      {emptyTracesCount > 0 && (
        <Alert
          message={intl.formatMessage(
            { id: 'contactPersonTable.error.missingData' },
            { emptyTraces: emptyTracesCount }
          )}
          type="error"
        />
      )}
    </>
  );
};
