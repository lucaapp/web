import styled from 'styled-components';

export const ContactPersonsWrapper = styled.div`
  padding-bottom: 32px;
`;

export const Heading = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 24px;
  font-weight: bold;
  margin-top: 36px;
`;

export const FlexWrapper = styled.div`
  display: flex;
`;
