import styled from 'styled-components';
import { InfoCircleOutlined } from '@ant-design/icons';

export const StyledText = styled.span`
  width: 376px;
  height: 80px;
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  line-height: 20px;
  text-align: center;
`;

export const StyledInfoCircleOutlined = styled(InfoCircleOutlined)`
  font-size: 16px;
  background: rgb(195, 206, 217);
  border-radius: 50%;
`;

export const StyledToolTip = styled.div`
  & .ant-tooltip-inner {
    height: auto;
    width: 408px;
    padding: 16px;
    border-radius: 4px;
    background-color: rgb(195, 206, 217);
  }
`;

export const ToolTipWrapper = styled.div`
  margin: 10px 0 0 16px;
`;
