import { getLocationTransferTraces } from 'network/api';
import { decryptTrace } from 'utils/cryptoOperations';
import { IncompleteDataError } from 'errors/incompleteDataError';

export const getDecryptedTraces = async location => {
  const transferTraces = await getLocationTransferTraces(location.transferId);
  const reencryptedTracesByOperator = transferTraces.traces.filter(
    trace => trace.data
  );

  const corruptedTracesOperator = transferTraces.traces.filter(
    trace => !trace.data
  );

  const decryptedTraces = await Promise.all(
    reencryptedTracesByOperator.map(trace =>
      decryptTrace(trace).catch(decryptionError => {
        if (decryptionError instanceof IncompleteDataError) {
          return { isUnregistered: true, decryptionError, trace };
        }
        return { isInvalid: true, decryptionError, trace };
      })
    )
  );
  return { corruptedTracesOperator, decryptedTraces };
};
