import React, { useMemo } from 'react';
import { useIntl } from 'react-intl';
import { Menu, Dropdown } from 'antd';
import { DownOutlined } from '@ant-design/icons';

import { FILES } from './DownloadFiles';
import { INTERFACE } from './Interfaces';

import { Wrapper, StyledLink, StyledMenuItem } from './Export.styled';

export const Export = ({ traces, location }) => {
  const intl = useIntl();
  const tracesContactDataIncluded = useMemo(
    () => traces?.filter(trace => trace.isContactDataIncluded) || [],
    [traces]
  );

  if (tracesContactDataIncluded.length === 0) return null;

  const menu = (
    <Menu>
      <StyledMenuItem key="CSVDownload">
        <FILES.CSVDownload
          traces={tracesContactDataIncluded}
          location={location}
        />
      </StyledMenuItem>
      <StyledMenuItem key="ExcelDownload">
        <FILES.ExcelDownload
          traces={tracesContactDataIncluded}
          location={location}
        />
      </StyledMenuItem>
      <StyledMenuItem key="OctoWareTNDownload">
        <FILES.OctoWareTNDownload
          traces={tracesContactDataIncluded}
          location={location}
        />
      </StyledMenuItem>
      <StyledMenuItem key="SormasDownload">
        <FILES.SormasDownload
          traces={tracesContactDataIncluded}
          location={location}
        />
      </StyledMenuItem>
      <StyledMenuItem key="SormasAPI">
        <INTERFACE.SormasAPI
          traces={tracesContactDataIncluded}
          location={location}
        />
      </StyledMenuItem>
    </Menu>
  );

  return (
    <Wrapper>
      <Dropdown overlay={menu}>
        <StyledLink
          className="ant-dropdown-link"
          onClick={event => event.preventDefault()}
        >
          {intl.formatMessage({
            id: 'contactPersonTable.download',
          })}
          <DownOutlined style={{ fontSize: 16, marginLeft: 8 }} />
        </StyledLink>
      </Dropdown>
    </Wrapper>
  );
};
