import styled from 'styled-components';

export const DataName = styled.div`
  font-weight: bold;
`;

export const AdditionalDataWrapper = styled.div`
  width: inherit;
  overflow: auto;
`;

export const ContentWrapper = styled.div``;
