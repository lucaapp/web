export const getSortedTraces = (traces, indexPersonData) => {
  for (let index = 0; index < traces; index++) {
    if (traces[index].userData.uuid === indexPersonData.uuid) {
      // eslint-disable-next-line no-param-reassign
      [traces[0], traces[index]] = [traces[index], traces[0]];
    }
  }
  return traces;
};

const isUserIndexPerson = (personA, personB, indexPersonData) =>
  indexPersonData?.uuid != null &&
  (personA.userData?.uuid === indexPersonData?.uuid ||
    personB.userData?.uuid === indexPersonData?.uuid);

export const nameSort = indexPersonData => (personA, personB) => {
  if (isUserIndexPerson(personA, personB, indexPersonData)) return 0;
  if (personA.userData?.ln !== personB.userData?.ln)
    return personA.userData?.ln?.localeCompare(personB.userData?.ln, 'de');
  return personA.userData?.fn?.localeCompare(personB.userData?.fn, 'de');
};

export const addressSort = indexPersonData => (personA, personB) => {
  if (isUserIndexPerson(personA, personB, indexPersonData)) return 0;
  if (personA.userData?.pc !== personB.userData?.pc)
    return personA.userData?.pc?.localeCompare(personB.userData?.pc);
  return personA.userData?.st?.localeCompare(personB.userData?.st, 'de');
};

export const checkinSort = indexPersonData => (personA, personB) => {
  if (isUserIndexPerson(personA, personB, indexPersonData)) return 0;
  return personA.checkin - personB.checkin;
};

export const checkoutSort = indexPersonData => (personA, personB) => {
  if (isUserIndexPerson(personA, personB, indexPersonData)) return 0;
  return personA.checkout - personB.checkout;
};

export const additionalDataSort = indexPersonData => (personA, personB) => {
  if (isUserIndexPerson(personA, personB, indexPersonData)) return 0;
  return personA.additionalData?.table?.localeCompare(
    personB.additionalData?.table,
    'de',
    {
      numeric: true,
    }
  );
};

export const Sorter = {
  NAME: indexPersonData => nameSort(indexPersonData),
  ADDRESS: indexPersonData => addressSort(indexPersonData),
  CHECKIN: indexPersonData => checkinSort(indexPersonData),
  CHECKOUT: indexPersonData => checkoutSort(indexPersonData),
  ADDITIONAL_DATA: indexPersonData => additionalDataSort(indexPersonData),
};

export const sortDirectionsOrder = ['ascend', 'descend', 'ascend'];
