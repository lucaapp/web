import React from 'react';
import { useIntl } from 'react-intl';
import { Tooltip } from 'antd';
import Icon from '@ant-design/icons';
import { BellNotifiedIcon } from 'assets/icons';

export const Notified = () => {
  const intl = useIntl();
  return (
    <Tooltip title={intl.formatMessage({ id: 'contactperson.notified' })}>
      <Icon
        component={BellNotifiedIcon}
        style={{
          color: 'black',
          fontSize: 32,
          marginLeft: 32,
        }}
      />
    </Tooltip>
  );
};
