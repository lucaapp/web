import styled from 'styled-components';
import { InfoCircleOutlined } from '@ant-design/icons';

export const Count = styled.div`
  display: flex;
  align-items: center;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
  margin-top: 8px;
`;

export const AnonymousCheckins = styled.span`
  margin-left: 4px;
`;

export const StyledInfoCircleOutlined = styled(InfoCircleOutlined)`
  font-size: 16px;
  background: rgb(195, 206, 217);
  border-radius: 50%;
`;

export const StyledToolTip = styled.div`
  margin-left: 8px;
  & .ant-tooltip-inner {
    height: auto;
    width: 408px;
    padding: 16px;
    border-radius: 4px;
    background-color: rgb(195, 206, 217);
  }
`;

export const StyledText = styled.span`
  width: 376px;
  height: 80px;
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  line-height: 20px;
  text-align: center;
`;
