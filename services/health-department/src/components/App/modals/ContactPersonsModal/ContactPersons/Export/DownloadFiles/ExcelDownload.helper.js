import { sanitizeForCSV } from 'utils/sanitizer';
import {
  getFormattedDate,
  getFormattedTime,
  isFormattedDateValid,
  isFormattedTimeValid,
} from 'utils/time';
import { formatAdditionalDataKey } from './helpers';

const locationColumnIds = [
  'contactPersonTable.locationName',
  'history.label.locationCategory',
  'history.label.areaDetails',
  'contactPersonTable.location.street',
  'contactPersonTable.location.streetNumber',
  'contactPersonTable.location.postalCode',
  'contactPersonTable.location.city',
  'contactPersonTable.locationOwner.firstName',
  'contactPersonTable.locationOwner.lastName',
  'contactPersonTable.locationOwner.phone',
];
const tracesColumnIds = [
  'contactPersonTable.firstName',
  'contactPersonTable.lastName',
  'contactPersonTable.phone',
  'contactPersonTable.email',
  'contactPersonTable.street',
  'contactPersonTable.houseNumber',
  'contactPersonTable.city',
  'contactPersonTable.postalCode',
  'contactPersonTable.checkinDate',
  'contactPersonTable.checkinTime',
  'contactPersonTable.checkoutDate',
  'contactPersonTable.checkoutTime',
  'contactPersonTable.additionalData',
];

export const columnIds = [...locationColumnIds, ...tracesColumnIds];

export const getSanitizedTraces = (traces, intl) =>
  traces.map(({ userData = '', additionalData, checkin, checkout }) => [
    sanitizeForCSV(userData.fn),
    userData
      ? sanitizeForCSV(userData.ln)
      : intl.formatMessage({
          id: 'contactPersonTable.unregistredBadgeUser',
        }),
    sanitizeForCSV(userData.pn),
    sanitizeForCSV(userData.e),
    sanitizeForCSV(userData.st),
    sanitizeForCSV(userData.hn),
    sanitizeForCSV(userData.c),
    sanitizeForCSV(userData.pc),
    isFormattedDateValid(checkin)
      ? sanitizeForCSV(getFormattedDate(checkin))
      : '',
    isFormattedTimeValid(checkin)
      ? sanitizeForCSV(getFormattedTime(checkin))
      : '',
    isFormattedDateValid(checkout)
      ? sanitizeForCSV(getFormattedDate(checkout))
      : '',
    isFormattedTimeValid(checkout)
      ? sanitizeForCSV(getFormattedTime(checkout))
      : '',
    additionalData
      ? Object.keys(additionalData)
          .map(
            key =>
              `${sanitizeForCSV(
                formatAdditionalDataKey(key, intl)
              )}: ${sanitizeForCSV(additionalData[key])}`
          )
          .join()
      : '',
  ]);

export const getSanitizedLocation = (location, intl) => [
  sanitizeForCSV(location.name),
  intl.formatMessage({
    id: `history.location.category.${location.type}`,
  }),
  location.isIndoor
    ? intl.formatMessage({ id: 'history.label.indoor' })
    : intl.formatMessage({ id: 'history.label.outdoor' }),
  sanitizeForCSV(location.streetName),
  sanitizeForCSV(location.streetNr),
  sanitizeForCSV(location.zipCode),
  sanitizeForCSV(location.city),
  sanitizeForCSV(location.firstName),
  sanitizeForCSV(location.lastName),
  sanitizeForCSV(location.phone),
];
