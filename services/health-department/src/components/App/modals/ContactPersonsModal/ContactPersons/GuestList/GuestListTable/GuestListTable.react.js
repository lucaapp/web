import React, { useState, useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { Table } from 'antd';
import moment from 'moment';
import { getWarningLevelsForLocationTransfer, getMe } from 'network/api';

import { useQuery } from 'react-query';

import { NOTIFIABLE_DEVICE_TYPES } from 'constants/deviceTypes';
import { AdditionalData } from './AdditionalData';
import { Notified } from './Notified';
import { Name } from './Name';
import { Address } from './Address';
import { Phone } from './Phone';
import { Time } from './Time';
import { SelectionTitle } from './SelectionTitle';
import { Selection } from './Selection';
import { DeviceIcon } from './DeviceIcon';
import {
  getSortedTraces,
  sortDirectionsOrder,
  Sorter,
} from './GuestListTable.helper';

export const GuestListTable = ({
  traces,
  indexPersonData,
  setSelectedTraces,
  location,
}) => {
  const intl = useIntl();

  const { transferId, time } = location;

  const {
    data: riskLevels,
  } = useQuery(
    `getWarningLevelsForLocationTransfer${transferId}`,
    () => getWarningLevelsForLocationTransfer(transferId),
    { refetchOnWindowFocus: false }
  );

  const { data: healthDepartmentEmployee } = useQuery('me', getMe, {
    refetchOnWindowFocus: false,
  });

  const updateAllCheckboxes = useCallback(
    checked =>
      traces.map(trace => {
        return {
          traceId: trace.traceId,
          checked,
        };
      }),
    [traces]
  );

  const [tracesChecked, updateTracesChecked] = useState(
    updateAllCheckboxes(true)
  );
  const [selectAllChecked, updateSelectAllChecked] = useState(false);

  useEffect(() => {
    updateTracesChecked(updateAllCheckboxes(true));
    updateSelectAllChecked(true);
  }, [traces, updateAllCheckboxes]);

  useEffect(() => {
    const checkedTracesData = traces.filter(trace =>
      tracesChecked.some(
        ({ traceId, checked }) => trace.traceId === traceId && checked
      )
    );
    setSelectedTraces(checkedTracesData);
  }, [traces, tracesChecked, setSelectedTraces]);

  const onSelectAll = event => {
    updateSelectAllChecked(event.target.checked);
    updateTracesChecked(updateAllCheckboxes(event.target.checked));
  };

  const onSelectionUpdate = traceId => {
    const index = tracesChecked.findIndex(trace => trace.traceId === traceId);
    const newTracesChecked = [...tracesChecked];
    newTracesChecked[index] = {
      traceId,
      checked: !newTracesChecked[index].checked,
    };
    updateSelectAllChecked(false);
    updateTracesChecked(newTracesChecked);
  };

  const isTraceChecked = lookupTrace => {
    return (
      tracesChecked.find(trace => trace.traceId === lookupTrace.traceId)
        ?.checked || false
    );
  };

  const columns = [
    {
      key: 'Notified',
      render: function renderBell(notificationTrace) {
        if (
          !riskLevels ||
          !healthDepartmentEmployee ||
          !healthDepartmentEmployee.notificationsEnabled ||
          !(
            notificationTrace.deviceType === NOTIFIABLE_DEVICE_TYPES.IOS ||
            notificationTrace.deviceType === NOTIFIABLE_DEVICE_TYPES.ANDROID
          )
        )
          return null;

        const traceRiskLevels = riskLevels.find(
          riskLevelObject =>
            riskLevelObject.traceId === notificationTrace.traceId
        )?.riskLevels;

        if (!traceRiskLevels?.length) return null;

        return <Notified />;
      },
    },
    {
      title: intl.formatMessage({ id: 'contactPersonTable.name' }),
      key: 'name',
      render: function renderName(trace) {
        return trace.isContactDataMandatory ? (
          <Name trace={trace} />
        ) : (
          <div>{intl.formatMessage({ id: 'guestList.anonymousCheckin' })}</div>
        );
      },
      sorter: Sorter.NAME(indexPersonData),
      sortDirections: sortDirectionsOrder,
    },
    {
      title: intl.formatMessage({ id: 'contactPersonTable.address' }),
      key: 'address',
      render: function renderAddress(trace) {
        return trace.isContactDataMandatory ? <Address trace={trace} /> : '-';
      },
      sorter: Sorter.ADDRESS(indexPersonData),
      sortDirections: sortDirectionsOrder,
    },
    {
      title: intl.formatMessage({ id: 'contactPersonTable.phone' }),
      key: 'phone',
      render: function renderPhone(trace) {
        return trace.isContactDataMandatory ? <Phone trace={trace} /> : '-';
      },
    },
    {
      title: intl.formatMessage({ id: 'contactPersonTable.checkin' }),
      key: 'checkin',
      render: function renderCheckin(trace) {
        return <Time time={trace.checkin} />;
      },
      sorter: Sorter.CHECKIN(indexPersonData),
      sortDirections: sortDirectionsOrder,
    },
    {
      title: intl.formatMessage({ id: 'contactPersonTable.checkout' }),
      key: 'checkout',
      render: function renderCheckout(trace) {
        return <Time time={trace.checkout} />;
      },
      sorter: Sorter.CHECKOUT(indexPersonData),
      sortDirections: sortDirectionsOrder,
    },
    {
      title: intl.formatMessage({ id: 'contactPersonTable.additionalData' }),
      key: 'additionalData',
      render: function renderAdditionalData(trace) {
        return <AdditionalData trace={trace} />;
      },
      sorter: Sorter.ADDITIONAL_DATA(indexPersonData),
      sortDirections: sortDirectionsOrder,
    },
    {
      title: intl.formatMessage({ id: 'modal.guestList.deviceType' }),
      key: 'deviceType',
      render: function renderDeviceType(trace) {
        return <DeviceIcon deviceType={trace.deviceType} />;
      },
    },
    {
      title: function renderSelectionTitle() {
        return (
          <SelectionTitle
            traces={traces}
            onSelectAll={onSelectAll}
            selectAllChecked={selectAllChecked}
          />
        );
      },
      key: '',
      render: function renderSelection(trace) {
        return (
          <Selection
            trace={trace}
            onSelectionUpdate={onSelectionUpdate}
            checked={isTraceChecked(trace)}
          />
        );
      },
    },
  ];

  return (
    <Table
      id="contactPersonsTable"
      columns={columns}
      dataSource={getSortedTraces(traces, indexPersonData)}
      pagination={false}
      rowClassName={record =>
        indexPersonData !== null &&
        record.userData?.uuid === indexPersonData?.uuid
          ? 'indexPerson'
          : ''
      }
      rowKey={record => record.traceId}
      locale={{
        emptyText:
          moment.unix(time[0]).add(28, 'days').unix() > moment().unix()
            ? intl.formatMessage({
                id: 'contactPersonTable.noData',
              })
            : intl.formatMessage({
                id: 'contactPersonTable.noData.expired',
              }),
      }}
      showSorterTooltip={false}
    />
  );
};
