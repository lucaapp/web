import React from 'react';
import { useIntl } from 'react-intl';
import { Tooltip } from 'antd';
import {
  Count,
  AnonymousCheckins,
  StyledInfoCircleOutlined,
  StyledToolTip,
  StyledText,
} from './GuestCount.styled';

export const GuestCount = ({ guestCount, amountOfAnonymousCheckins }) => {
  const intl = useIntl();
  const toolTipText = (
    <StyledText>
      {intl.formatMessage({
        id: 'contactPersons.anonymousCheckinsInfo',
      })}
    </StyledText>
  );

  return (
    <Count>
      {intl.formatMessage(
        {
          id: 'contactPersons.guests',
        },
        { count: guestCount }
      )}
      {amountOfAnonymousCheckins > 0 && (
        <>
          <AnonymousCheckins>
            {intl.formatMessage(
              {
                id: 'contactPersons.amountOfAnonymousCheckins',
              },
              { amount: <b>{amountOfAnonymousCheckins}</b> }
            )}
          </AnonymousCheckins>
          <StyledToolTip>
            <Tooltip
              placement="right"
              title={toolTipText}
              getPopupContainer={triggerNode => triggerNode}
            >
              <StyledInfoCircleOutlined />
            </Tooltip>
          </StyledToolTip>
        </>
      )}
    </Count>
  );
};
