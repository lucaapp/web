import styled from 'styled-components';
import { PrimaryButton } from 'components/general';

export const Wrapper = styled.div`
  padding-bottom: 40px;
`;

export const NamesCount = styled.div`
  font-size: 14px;
  font-weight: bold;
  padding-bottom: 40px;
`;

export const NameElementWrapper = styled.div`
  padding-bottom: 40px;
`;

export const NameElement = styled.div`
  font-size: 16px;
  padding-bottom: 4px;
`;

export const CloseModalButton = styled(PrimaryButton)`
  float: right;
`;
