import React from 'react';
import { useIntl } from 'react-intl';
import {
  Wrapper,
  NamesCount,
  NameElement,
  CloseModalButton,
} from './NotFoundListModal.styled';

export const NotFoundListModal = ({ names, closeModal }) => {
  const intl = useIntl();

  return (
    <Wrapper>
      <NamesCount>
        {intl.formatMessage(
          { id: 'contact.details.namesCount' },
          { count: names.length }
        )}
      </NamesCount>
      {names
        .sort((a, b) => a.localeCompare(b, 'de'))
        .map(name => (
          <NameElement key={name}>{name}</NameElement>
        ))}
      <CloseModalButton onClick={closeModal}>
        {intl.formatMessage({
          id: 'contact.details.modal.close',
        })}
      </CloseModalButton>
    </Wrapper>
  );
};
