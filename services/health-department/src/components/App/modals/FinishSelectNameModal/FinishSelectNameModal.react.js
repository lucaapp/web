import React from 'react';
import { PrimaryButton, SecondaryButton } from 'components/general';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router';
import { CONTACT_ROUTE } from 'constants/routes';
import {
  ButtonWrapper,
  InfoWrapper,
  StyledCheckIcon,
  StyledHeadline,
  StyledText,
} from './FinishSelectNameModal.styled';

export const FinishSelectNameModal = ({ closeModal }) => {
  const intl = useIntl();
  const history = useHistory();

  const navigateToSearchView = () => {
    history.push(CONTACT_ROUTE);
    closeModal();
  };

  const navigateToContactListView = () => {
    history.push({
      pathname: CONTACT_ROUTE,
      state: { openContactList: true },
    });
    closeModal();
  };

  return (
    <>
      <InfoWrapper>
        <StyledCheckIcon />
        <StyledHeadline>
          {intl.formatMessage({
            id: 'contact.nameSelectionTable.finish.headline',
          })}
        </StyledHeadline>
        <StyledText>
          {intl.formatMessage({
            id: 'contact.nameSelectionTable.finish.text',
          })}
        </StyledText>
      </InfoWrapper>

      <ButtonWrapper>
        <SecondaryButton onClick={navigateToSearchView}>
          {intl.formatMessage({
            id: 'contact.nameSelectionTable.finish.button.newSearch',
          })}
        </SecondaryButton>
        <PrimaryButton onClick={navigateToContactListView}>
          {intl.formatMessage({
            id: 'contact.nameSelectionTable.finish.button.contactPerson',
          })}
        </PrimaryButton>
      </ButtonWrapper>
    </>
  );
};
