import React from 'react';
import QRCode from 'qrcode.react';
import { useIntl } from 'react-intl';

import {
  Wrapper,
  TitleWrapper,
  QrCodeWrapper,
  Name,
  Title,
  QrCodeItem,
} from './CertificateModal.styled';

export const CertificateModal = ({ entry }) => {
  const intl = useIntl();

  return (
    <Wrapper>
      <TitleWrapper>
        <Name>{`${entry.firstName} ${entry.lastName}`}</Name>
        <Title>
          {intl.formatMessage({ id: 'contactDetails.vaccinationStatus' })}
        </Title>
      </TitleWrapper>
      <QrCodeWrapper>
        {entry.rawCertificateData.map(rawCertificate => (
          <QrCodeItem key={rawCertificate}>
            <QRCode size={300} value={rawCertificate} />
          </QrCodeItem>
        ))}
      </QrCodeWrapper>
    </Wrapper>
  );
};
