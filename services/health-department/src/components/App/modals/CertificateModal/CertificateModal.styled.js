import styled from 'styled-components';

export const Wrapper = styled.div``;

export const TitleWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 32px;
`;

export const Name = styled.div`
  font-size: 16px;
`;

export const Title = styled.div`
  font-size: 24px;
  font-weight: bold;
`;

export const QrCodeWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const QrCodeItem = styled.div`
  padding: 20px;
`;
