import React from 'react';
import { Collapse } from 'antd';
import { useQuery } from 'react-query';
import { useIntl } from 'react-intl';

import { getNotificationConfig, getAssociatedDataRequests } from 'network/api';
import { PrimaryButton } from 'components/general';

import { InfoPanel } from './InfoPanel';
import { PanelHeader } from './PanelHeader';
import {
  ButtonWrapper,
  Wrapper,
  CollapseWrapper,
} from './SimilarDataRequestModal.styled';

const { Panel } = Collapse;

export const SimilarDataRequestModal = ({ location, closeModal }) => {
  const intl = useIntl();

  const { data: config } = useQuery(
    'notificationConfig',
    getNotificationConfig,
    {
      refetchOnWindowFocus: false,
      staleTime: Number.POSITIVE_INFINITY,
    }
  );

  const { data: associatedRequests } = useQuery(
    ['associatedDataRequests', location.transferId],
    () => getAssociatedDataRequests(location.transferId)
  );

  if (!config || !associatedRequests) return null;

  return (
    <Wrapper>
      <CollapseWrapper>
        <Collapse accordion>
          {associatedRequests.map(associatedRequest => (
            <Panel
              header={<PanelHeader associatedRequest={associatedRequest} />}
              key={associatedRequest.uuid}
            >
              <InfoPanel
                associatedRequest={associatedRequest}
                location={location}
                config={config}
                key={associatedRequest.uuid}
              />
            </Panel>
          ))}
        </Collapse>
      </CollapseWrapper>
      <ButtonWrapper>
        <PrimaryButton onClick={closeModal}>
          {intl.formatMessage({ id: 'modal.similarDataRequest.button' })}
        </PrimaryButton>
      </ButtonWrapper>
    </Wrapper>
  );
};
