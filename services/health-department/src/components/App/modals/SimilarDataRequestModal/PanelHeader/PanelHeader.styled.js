import styled from 'styled-components';

export const HeaderWrapper = styled.span``;
export const HeaderDateTime = styled.span`
  font-size: 16px;
  font-weight: bold;
  float: left;
  width: 42%;
`;

export const HeaderHDName = styled.span`
  font-size: 16px;
  font-weight: 500;
`;
