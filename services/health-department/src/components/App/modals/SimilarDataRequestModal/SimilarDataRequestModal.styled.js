import styled from 'styled-components';

export const Wrapper = styled.div`
  padding-bottom: 40px;
`;

export const ButtonWrapper = styled.div`
  float: right;
`;

export const CollapseWrapper = styled.div`
  padding-bottom: 24px;
  max-height: 600px;
  overflow: auto;
`;
