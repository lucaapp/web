import React from 'react';
import { useIntl } from 'react-intl';

import { getHealthDepartmentMessages } from 'utils/healthDepartmentMessages';

import { formatDateTimeframe } from 'utils/time';
import {
  InfoFieldWrapper,
  InfoFieldName,
  InfoFieldData,
  InfoText,
  NotificationTextContentHeader,
  NotificationTextTitle,
  NotificationTextMessage,
  SeperationBorder,
} from './InfoPanel.styled';

export const InfoPanel = ({ associatedRequest, location, config }) => {
  const intl = useIntl();

  const { time: baseRequestTime, groupName, locationName } = location;
  const {
    time: associatedRequestTime,
    departmentId,
    departmentName,
  } = associatedRequest;

  const { messages } = getHealthDepartmentMessages(
    config,
    departmentId,
    4,
    intl
  );

  return (
    <div>
      <InfoFieldWrapper>
        <InfoFieldName>
          {intl.formatMessage({ id: 'modal.similarDataRequest.locationName' })}
        </InfoFieldName>
        <InfoFieldData>{groupName}</InfoFieldData>
      </InfoFieldWrapper>
      {locationName && (
        <InfoFieldWrapper>
          <InfoFieldName>
            {intl.formatMessage({ id: 'modal.similarDataRequest.areaName' })}
          </InfoFieldName>
          <InfoFieldData>{locationName}</InfoFieldData>
        </InfoFieldWrapper>
      )}
      <InfoFieldWrapper>
        <InfoFieldName>
          {intl.formatMessage({ id: 'modal.similarDataRequest.timeframe' })}
        </InfoFieldName>
        <InfoFieldData>{formatDateTimeframe(baseRequestTime)}</InfoFieldData>
      </InfoFieldWrapper>
      <InfoFieldWrapper>
        <InfoFieldName>
          {intl.formatMessage(
            { id: 'modal.similarDataRequest.timeframeHD' },
            { br: <br />, name: departmentName }
          )}
        </InfoFieldName>
        <InfoFieldData>
          {formatDateTimeframe(associatedRequestTime)}
        </InfoFieldData>
      </InfoFieldWrapper>
      <SeperationBorder />
      <InfoText>
        {intl.formatMessage({ id: 'modal.similarDataRequest.infoText' })}
      </InfoText>
      <NotificationTextContentHeader>
        {intl.formatMessage({
          id: 'modal.similarDataRequest.notificationTextHeader',
        })}
      </NotificationTextContentHeader>
      <NotificationTextTitle>
        {intl.formatMessage({
          id: messages.title,
          defaultMessage: messages.title,
        })}
      </NotificationTextTitle>
      <NotificationTextMessage>
        {intl.formatMessage(
          {
            id: messages.message,
            defaultMessage: messages.message,
          },
          { br: <br /> }
        )}
      </NotificationTextMessage>
    </div>
  );
};
