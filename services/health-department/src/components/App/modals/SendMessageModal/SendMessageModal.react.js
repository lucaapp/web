import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useQuery, useQueryClient } from 'react-query';
import { Spin } from 'antd';

import { sendCoversationMessage } from 'utils/reachableContacts';
import { getMe } from 'network/api';

import { handleResponse, throwGeneralError } from './SendMessageModal.helper';

import { Wrapper, Header } from './SendMessageModal.styled';
import { MessageTemplate } from './MessageTemplate/MessageTemplate.react';
import { MultipleMessagesResponse } from './MultipleMessagesResponse';

export const SendMessageModal = ({
  entry,
  multipleEntries,
  isMultipleEntries,
  closeModal,
  setUpdateMessages,
}) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const [sendMessageResponses, setSendMessageResponses] = useState(null);

  const {
    data: hdEmployee,
    isLoading: isEmployeeLoading,
    error: isEmployeeError,
  } = useQuery('me', getMe);

  if (isEmployeeLoading) return <Spin />;
  if (isEmployeeError) return null;

  const sendMultipleMessages = async values => {
    const results = await Promise.allSettled(
      multipleEntries.map(selectedPerson =>
        sendCoversationMessage({
          messageObject: values,
          conversationId: selectedPerson.uuid,
          hdId: hdEmployee.departmentId,
          seed: selectedPerson.seed,
          mekp: selectedPerson.mekp,
        })
      )
    );
    setSendMessageResponses(results.map(result => result.value));
    queryClient.invalidateQueries(['conversationMessages']);
  };

  const sendSingleMessage = values => {
    sendCoversationMessage({
      messageObject: values,
      conversationId: entry.uuid,
      hdId: hdEmployee.departmentId,
      seed: entry.seed,
      mekp: entry.mekp,
    })
      .then(response => {
        handleResponse(
          response,
          intl,
          closeModal,
          queryClient,
          entry,
          setUpdateMessages
        );
      })
      .catch(() => {
        throwGeneralError(intl);
        closeModal();
      });
  };

  return (
    <Wrapper>
      <Header>{intl.formatMessage({ id: 'sendMessageModal.title' })}</Header>
      {isMultipleEntries && sendMessageResponses ? (
        <MultipleMessagesResponse
          multipleEntries={multipleEntries}
          sendMessageResponses={sendMessageResponses}
        />
      ) : (
        <MessageTemplate
          isMultipleEntries={isMultipleEntries}
          sendMultipleMessages={sendMultipleMessages}
          sendSingleMessage={sendSingleMessage}
          hdEmployee={hdEmployee}
        />
      )}
    </Wrapper>
  );
};
