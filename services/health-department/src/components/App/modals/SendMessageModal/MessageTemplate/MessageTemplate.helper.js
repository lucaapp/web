export const POSITIVE_TEST = 'positive_test';
export const CONTACT_PERSON = 'contact_person';
export const CONTACT_PERSON_NOT_IMMUNIZED = 'contact_person_not_immunized';
export const INDIVIDUAL_NOTIFICATION = 'individual_notification';

export const getTemplateOptions = (intl, departmentName) => [
  {
    name: intl.formatMessage({ id: 'sendMessageModal.option.positive_test' }),
    value: POSITIVE_TEST,
    templateTitle: intl.formatMessage({
      id: 'sendMessageModal.option.positive_test.title',
    }),
    templateMessage: intl.formatMessage(
      {
        id: 'sendMessageModal.option.positive_test.message',
      },
      { departmentName }
    ),
  },
  {
    name: intl.formatMessage({ id: 'sendMessageModal.option.contact_person' }),
    value: CONTACT_PERSON,
    templateTitle: intl.formatMessage({
      id: 'sendMessageModal.option.contact_person.title',
    }),
    templateMessage: intl.formatMessage({
      id: 'sendMessageModal.option.contact_person.message',
    }),
  },
  {
    name: intl.formatMessage({
      id: 'sendMessageModal.option.contact_person_not_immunized',
    }),
    value: CONTACT_PERSON_NOT_IMMUNIZED,
    templateTitle: intl.formatMessage({
      id: 'sendMessageModal.option.contact_person_not_immunized.title',
    }),
    templateMessage: intl.formatMessage(
      {
        id: 'sendMessageModal.option.contact_person_not_immunized.message',
      },
      { departmentName }
    ),
  },
  {
    name: intl.formatMessage({
      id: 'sendMessageModal.option.individual_notification',
    }),
    value: INDIVIDUAL_NOTIFICATION,
    templateTitle: '',
    templateMessage: '',
  },
];
