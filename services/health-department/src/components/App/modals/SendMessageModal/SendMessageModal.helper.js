import { notification } from 'antd';

export const throwGeneralError = intl =>
  notification.error({
    message: intl.formatMessage({
      id: 'notification.sendConversationMessage.error.general',
    }),
  });

export const handleResponse = (
  response,
  intl,
  closeModal,
  queryClient,
  entry,
  setUpdateMessages
) => {
  const resetModal = () => {
    queryClient.invalidateQueries(['conversationMessages', entry.uuid]);
    closeModal();
  };
  if (response.status === 204) {
    resetModal();
    setUpdateMessages(true);
    notification.success({
      message: intl.formatMessage({
        id: 'notification.sendConversationMessage.success',
      }),
    });
    return;
  }
  if (response.status === 409) {
    notification.error({
      message: intl.formatMessage({
        id: 'notification.sendConversationMessage.error.conflict',
      }),
    });
    closeModal();
    return;
  }
  resetModal();
  throwGeneralError(intl);
};
