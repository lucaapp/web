import styled from 'styled-components';
import { SecondaryButton } from 'components/general';

export const Wrapper = styled.div`
  padding: 80px 32px;
  background-color: rgb(218, 224, 231);
  position: relative;
  height: 100vh;
`;

export const Header = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 32px;
`;

export const SelectNavigationWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding-bottom: 20px;
`;

export const SearchNavigationWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const ContentWrapper = styled.div`
  padding: 40px 32px;
  background-color: white;
  margin-bottom: 24px;
`;

export const VersionFooterWrapper = styled.div`
  padding-right: 24px;
  float: right;
`;

export const StyledSecondaryButton = styled(SecondaryButton)`
  background-color: white;
  &:focus {
    background-color: white;
  }
`;
