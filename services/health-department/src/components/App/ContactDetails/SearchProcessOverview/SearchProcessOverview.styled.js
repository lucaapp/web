import styled from 'styled-components';
import { PrimaryButton } from 'components/general';

export const SearchOverviewWrapper = styled.div`
  padding: 0 14px 48px 14px;
`;

export const SearchOverviewTitle = styled.div`
  font-size: 34px;
  padding-bottom: 40px;
`;

export const SearchOverviewDateTime = styled.div`
  font-size: 16px;
  padding-bottom: 32px;
`;

export const StatusListElement = styled.div`
  font-size: 16px;
  padding-bottom: 16px;
`;

export const StatusListWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding-bottom: 44px;
`;
export const StatusListSide = styled.div`
  display: flex;
  flex-direction: column;
  width: 50%;
`;

export const ShowNotFoundList = styled.a`
  text-transform: uppercase;
  text-decoration: none;
  font-size: 14px;
  font-weight: bold;
  color: rgb(80, 102, 124);
  padding-left: 16px;
`;

export const NotFoundListWrapper = styled.div`
  display: flex;
`;

export const InfoText = styled.div`
  font-size: 14px;
  padding-bottom: 40px;
`;

export const SearchAndSelectButton = styled(PrimaryButton)`
  float: right;
`;
