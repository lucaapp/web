import React, { useEffect } from 'react';
import moment from 'moment';
import { useIntl } from 'react-intl';

import { useModal } from 'components/hooks/useModal';
import { NotFoundListModal } from 'components/App/modals/NotFoundListModal';

import {
  SearchOverviewWrapper,
  SearchOverviewTitle,
  SearchOverviewDateTime,
  StatusListWrapper,
  StatusListSide,
  StatusListElement,
  InfoText,
  SearchAndSelectButton,
  NotFoundListWrapper,
  ShowNotFoundList,
} from './SearchProcessOverview.styled';

import { getNotFoundNames } from './SearchProcessOverview.helper';

export const SearchProcessOverview = ({
  searchProcess,
  showNameSelection,
  setCurrentSearchIndex,
  setPotentialFoundCount,
}) => {
  const intl = useIntl();
  const [openModal, closeModal] = useModal();
  const nameCountMessage = 'contact.details.namesCount';

  const searched = searchProcess.searches.length;
  const notFoundNames = getNotFoundNames(searchProcess.searches);
  const notFound = notFoundNames.length;
  const found = searched - notFound;
  const doneCount = searchProcess.searches.filter(({ done }) => done).length;
  const status = `${doneCount}/${found}`;

  useEffect(() => {
    setPotentialFoundCount(found);
    setCurrentSearchIndex(0);
  }, [found, setCurrentSearchIndex, setPotentialFoundCount]);

  const openNotFoundModal = () =>
    openModal({
      title: intl.formatMessage({
        id: 'contact.details.modal.notFound',
      }),
      content: (
        <NotFoundListModal names={notFoundNames} closeModal={closeModal} />
      ),
      closable: true,
    });

  return (
    <SearchOverviewWrapper>
      <SearchOverviewTitle>
        {intl.formatMessage({ id: 'contact.details.title' })}
      </SearchOverviewTitle>
      <SearchOverviewDateTime>
        {intl.formatMessage(
          { id: 'contact.details.dateTime' },
          {
            date: moment.unix(searchProcess.createdAt).format('DD.MM.YYYY'),
            time: moment.unix(searchProcess.createdAt).format('HH:mm'),
          }
        )}
      </SearchOverviewDateTime>
      <StatusListWrapper>
        <StatusListSide>
          <StatusListElement>
            {intl.formatMessage({ id: 'contact.details.searched' })}
          </StatusListElement>
          <StatusListElement>
            {intl.formatMessage({ id: 'contact.details.found' })}
          </StatusListElement>
          <StatusListElement>
            {intl.formatMessage({ id: 'contact.details.notFound' })}
          </StatusListElement>
          <StatusListElement>
            {intl.formatMessage({ id: 'contact.details.status' })}
          </StatusListElement>
        </StatusListSide>
        <StatusListSide>
          <StatusListElement>
            {intl.formatMessage({ id: nameCountMessage }, { count: searched })}
          </StatusListElement>
          <StatusListElement>
            {intl.formatMessage({ id: nameCountMessage }, { count: found })}
          </StatusListElement>
          <NotFoundListWrapper>
            <StatusListElement>
              {intl.formatMessage(
                { id: nameCountMessage },
                { count: notFound }
              )}
            </StatusListElement>
            <ShowNotFoundList onClick={openNotFoundModal}>
              {intl.formatMessage({ id: 'contact.details.viewList' })}
            </ShowNotFoundList>
          </NotFoundListWrapper>

          <StatusListElement>
            {intl.formatMessage(
              { id: 'contact.details.namesChecked' },
              { status }
            )}
          </StatusListElement>
        </StatusListSide>
      </StatusListWrapper>

      <InfoText>
        {intl.formatMessage({ id: 'contact.details.infoText' })}
      </InfoText>
      <SearchAndSelectButton
        onClick={showNameSelection}
        disabled={notFound === searched || doneCount === found}
      >
        {intl.formatMessage({ id: 'contact.details.selectButton' })}
      </SearchAndSelectButton>
    </SearchOverviewWrapper>
  );
};
