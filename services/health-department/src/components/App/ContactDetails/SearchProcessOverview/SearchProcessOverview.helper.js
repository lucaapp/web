import { hexToBytes, decodeUtf8 } from '@lucaapp/crypto';
import { decryptWithHDEKP } from 'utils/cryptoKeyOperations';

export const getNotFoundNames = searches =>
  searches
    .filter(({ available, done }) => !available && !done)
    .map(({ term }) => {
      try {
        return decodeUtf8(
          hexToBytes(
            decryptWithHDEKP(term.publicKey, term.iv, term.mac, term.data)
          )
        );
      } catch (error) {
        console.error(error);
        return null;
      }
    })
    .filter(decryptionResult => !!decryptionResult);
