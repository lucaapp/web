import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const SearchIndex = styled.div`
  margin-bottom: 16px;
  color: rgb(0, 0, 0);
  font-size: 12px;
  font-weight: 500;
`;

export const NameWrapper = styled.div`
  display: flex;
  align-items: baseline;
  margin-bottom: 40px;
`;

export const Name = styled.div`
  margin-right: 16px;
  color: rgb(0, 0, 0);
  font-size: 24px;
  font-weight: bold;
`;

export const Results = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-weight: 500;
`;
