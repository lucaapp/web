import React, { useState } from 'react';
import { Spin, Table } from 'antd';
import { PrimaryButton } from 'components/general';
import { createConversation } from 'utils/reachableContacts';
import { useIntl } from 'react-intl';
import { useModal } from 'components/hooks/useModal';

import { FinishSelectNameModal } from 'components/App/modals/FinishSelectNameModal/FinishSelectNameModal.react';
import { CONTACT_ROUTE } from 'constants/routes';
import { NameSelectionHeader } from './NameSelectionHeader';
import {
  getEmptyText,
  useNameSelectionTableColumns,
  useSearchPossibleUsers,
} from './NameSelectionTable.helper';
import { ButtonWrapper } from './NameSelectionTable.styled';

export const NameSelectionTable = ({
  publicHDEKP,
  searchProcess,
  refetch,
  currentSearchIndex,
  setCurrentSearchIndex,
}) => {
  const intl = useIntl();
  const [selectedRows, setSelectedRows] = useState([]);
  const [selectedKeys, setSelectedKeys] = useState([]);
  const [openModal, closeModal] = useModal();

  const { columns } = useNameSelectionTableColumns();

  const filteredSearches = searchProcess.searches.filter(
    search => search.available && !search.done
  );

  const {
    isLoading,
    decryptedSearches,
    searchesWithPossibleUsers,
  } = useSearchPossibleUsers(filteredSearches);

  if (isLoading) return <Spin />;

  const searchInfo = decryptedSearches[currentSearchIndex];
  const currentPossibleUsers = searchesWithPossibleUsers[currentSearchIndex];

  const potentialUserCount = currentPossibleUsers
    ? currentPossibleUsers.length
    : 0;

  const data = currentPossibleUsers?.map((user, index) => ({
    key: index,
    firstName: user.cd.fn,
    lastName: user.cd.ln,
    streetName: user.cd.st,
    streetNumber: user.cd.hn,
    postCode: user.cd.pc,
    city: user.cd.c,
    phone: user.cd.pn,
    birthdate: user.vct.bd,
    certificate: user.vct,
    user,
    selected: false,
  }));

  const rowSelection = {
    selectedRowKeys: selectedKeys,
    onChange: (rowKeys, rows) => {
      setSelectedKeys(rowKeys);
      setSelectedRows(rows);
    },
  };

  const openFinishModal = () =>
    openModal({
      content: <FinishSelectNameModal closeModal={closeModal} />,
      closable: true,
      redirect: true,
      route: CONTACT_ROUTE,
    });

  const onClick = () => {
    if (potentialUserCount !== 0 && selectedRows.length > 0) {
      selectedRows.forEach(row => {
        createConversation(publicHDEKP, searchInfo?.uuid, row.user)
          .then(refetch)
          .catch(error => console.error('conversation error', error));
      });
    }
    if (currentSearchIndex < searchesWithPossibleUsers.length - 1) {
      setCurrentSearchIndex(currentSearchIndex + 1);
      setSelectedKeys([]);
      setSelectedRows([]);
    }
    if (currentSearchIndex >= searchesWithPossibleUsers.length - 1) {
      openFinishModal();
    }
  };

  return (
    <>
      <NameSelectionHeader
        searchInfo={searchInfo}
        currentSearchCount={currentSearchIndex + 1}
        potentialFoundCount={searchesWithPossibleUsers.length}
        potentialUserCount={potentialUserCount}
      />
      <Table
        columns={columns}
        dataSource={data}
        rowSelection={rowSelection}
        pagination={false}
        locale={{
          emptyText: getEmptyText(intl),
        }}
      />
      <ButtonWrapper>
        <PrimaryButton onClick={onClick}>
          {intl.formatMessage({ id: 'nameSelection.select' })}
        </PrimaryButton>
      </ButtonWrapper>
    </>
  );
};
