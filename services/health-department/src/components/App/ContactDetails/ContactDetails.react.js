import React, { useState } from 'react';
import { useQuery } from 'react-query';
import { useParams, useHistory } from 'react-router-dom';

import { getSearchProcess } from 'network/api';
import { CONTACT_ROUTE } from 'constants/routes';
import { VersionFooter } from 'components/App/VersionFooter';
import { useKeyLoader } from 'components/hooks/useKeyLoader';

import { Spin } from 'antd';
import { StyledBackIcon, StyledCloseIcon } from 'components/general';
import { SearchProcessOverview } from './SearchProcessOverview';
import { NameSelectionTable } from './NameSelectionTable';

import {
  Wrapper,
  ContentWrapper,
  VersionFooterWrapper,
  SelectNavigationWrapper,
  SearchNavigationWrapper,
} from './ContactDetails.styled';

export const ContactDetails = () => {
  const history = useHistory();
  const { searchProcessId } = useParams();
  const [showNameSelection, setShowNameSelection] = useState(false);
  const { keysData, isLoading, error } = useKeyLoader();
  const [currentSearchIndex, setCurrentSearchIndex] = useState(0);
  const [potentialFoundCount, setPotentialFoundCount] = useState(0);

  const {
    data: searchProcess,
    isLoading: isSearchProcessLoading,
    error: isSearchProcessError,
    refetch,
  } = useQuery(['searchProcess', { searchProcessId }], () =>
    getSearchProcess(searchProcessId)
  );

  if (isSearchProcessLoading || isLoading) return <Spin />;
  if (isSearchProcessError || error) return null;

  return (
    <Wrapper>
      <ContentWrapper>
        {showNameSelection ? (
          <>
            <SelectNavigationWrapper>
              <StyledBackIcon onClick={() => setShowNameSelection(false)} />
              <StyledCloseIcon onClick={() => history.push(CONTACT_ROUTE)} />
            </SelectNavigationWrapper>
            <NameSelectionTable
              publicHDEKP={keysData.publicHDEKP}
              searchProcess={searchProcess}
              refetch={refetch}
              currentSearchIndex={currentSearchIndex}
              setCurrentSearchIndex={setCurrentSearchIndex}
              potentialFoundCount={potentialFoundCount}
            />
          </>
        ) : (
          <>
            <SearchNavigationWrapper>
              <StyledCloseIcon onClick={() => history.push(CONTACT_ROUTE)} />
            </SearchNavigationWrapper>
            <SearchProcessOverview
              searchProcess={searchProcess}
              showNameSelection={() => setShowNameSelection(true)}
              setCurrentSearchIndex={setCurrentSearchIndex}
              setPotentialFoundCount={setPotentialFoundCount}
            />
          </>
        )}
      </ContentWrapper>
      <VersionFooterWrapper>
        <VersionFooter />
      </VersionFooterWrapper>
    </Wrapper>
  );
};
