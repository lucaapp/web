import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 40px;
`;

export const Title = styled.div`
  color: rgb(0, 0, 0);
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 32px;
`;
