import React from 'react';
import { useIntl } from 'react-intl';
import { RowContainer, StatusLabel, Time } from './TimeStatusRow.styled';

export const TimeStatusRow = ({ time, status }) => {
  const intl = useIntl();

  return (
    <RowContainer>
      <Time>{time}</Time>
      <StatusLabel $status={status}>
        {intl.formatMessage({ id: `contactDetails.messages.${status}` })}
      </StatusLabel>
    </RowContainer>
  );
};
