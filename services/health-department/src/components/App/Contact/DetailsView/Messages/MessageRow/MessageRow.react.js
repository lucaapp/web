import React, { useEffect, useState } from 'react';
import moment from 'moment';
import { useIntl } from 'react-intl';

import { useModal } from 'components/hooks/useModal';
import { MessageDetailsModal } from 'components/App/modals/MessageDetailsModal';
import {
  MessageRowWrapper,
  MessageSubject,
  PaddingWrapper,
} from './MessageRow.styled';
import { TimeStatusContainer } from './TimeStatusContainer';

export const MessageRow = ({ message }) => {
  const intl = useIntl();
  const [isMessageDelivered, setIsMessageDelivered] = useState(false);
  const [isMessageRead, setIsMessageRead] = useState(false);
  const [openModal, closeModal] = useModal();

  useEffect(() => {
    setIsMessageDelivered(message.receivedAt && !message.readAt);
    setIsMessageRead(message.readAt);
  }, [message.readAt, message.receivedAt]);

  const getFormattedTime = time =>
    `${moment.unix(time).format('DD.MM.YYYY - HH:mm')} ${intl.formatMessage({
      id: 'contactDetails.messages.time',
    })}`;

  const createdTime = getFormattedTime(message.createdAt);
  const receivedTime = getFormattedTime(message.receivedAt);
  const readTime = getFormattedTime(message.readAt);

  const openMessageDetailsModal = () => {
    openModal({
      content: (
        <MessageDetailsModal
          message={message}
          createdTime={createdTime}
          receivedTime={receivedTime}
          readTime={readTime}
          isMessageDelivered={isMessageDelivered}
          isMessageRead={isMessageRead}
          closeModal={closeModal}
        />
      ),
      minWidth: '728px',
      minHeight: '536px',
    });
  };

  return (
    <MessageRowWrapper
      key={message.messageId}
      onClick={openMessageDetailsModal}
    >
      <PaddingWrapper>
        <MessageSubject>{message.decryptedData.sub}</MessageSubject>
        <TimeStatusContainer
          createdTime={createdTime}
          receivedTime={receivedTime}
          readTime={readTime}
          isMessageDelivered={isMessageDelivered}
          isMessageRead={isMessageRead}
        />
      </PaddingWrapper>
    </MessageRowWrapper>
  );
};
