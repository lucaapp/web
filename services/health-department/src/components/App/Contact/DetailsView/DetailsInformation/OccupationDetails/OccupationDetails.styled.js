import styled from 'styled-components';

export const OccupationCardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border-left: 2px solid #303d4b;
  padding-left: 24px;
  margin: 32px 0;
`;

export const OccupationGrid = styled.div`
  &.occupationGrid:not(:last-child) {
    padding-bottom: 30px;
  }
`;
