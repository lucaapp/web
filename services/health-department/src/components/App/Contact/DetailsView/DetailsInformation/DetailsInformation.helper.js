export const getOccupationOptions = (entry, intl) => [
  {
    field: 'CRITICAL_INFRA',
    value: entry.ci,
    label: intl.formatMessage({
      id: 'contactDetails.occupation.options.ci',
    }),
  },
  {
    field: 'VULNERABLE_GROUP',
    value: entry.vg,
    label: intl.formatMessage({
      id: 'contactDetails.occupation.options.vg',
    }),
  },
  {
    field: 'JOB',
    value: entry.jb,
    label: intl.formatMessage({
      id: 'contactDetails.occupation.options.jb',
    }),
  },
  {
    field: 'EMPLOYER',
    value: entry.em,
    label: intl.formatMessage({
      id: 'contactDetails.occupation.options.em',
    }),
  },
];
