import styled from 'styled-components';

export const Headline = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-right: 20px;
`;

export const HeadlineText = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  padding-right: 16px;
`;

export const HeadlineWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;
