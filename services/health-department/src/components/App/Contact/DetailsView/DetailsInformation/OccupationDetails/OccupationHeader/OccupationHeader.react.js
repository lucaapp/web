import React from 'react';
import { DownOutlined, UpOutlined } from '@ant-design/icons';
import { useIntl } from 'react-intl';
import { OccupationIcon } from 'components/App/Contact/reusableComponents/OccupationIcon';
import {
  HeadlineWrapper,
  Headline,
  HeadlineText,
} from './OccupationHeader.styled';

export const OccupationHeader = ({
  entry,
  showOccupationDetails,
  setShowOccupationDetails,
  hasAnyOccupationDetails,
}) => {
  const intl = useIntl();
  const toggleOccupationCard = () => {
    setShowOccupationDetails(!showOccupationDetails);
  };
  return (
    <HeadlineWrapper>
      <Headline onClick={toggleOccupationCard}>
        <HeadlineText>
          {intl.formatMessage({ id: 'contactDetails.occupation' })}
        </HeadlineText>
        {showOccupationDetails ? <UpOutlined /> : <DownOutlined />}
      </Headline>
      {hasAnyOccupationDetails && (
        <OccupationIcon vg={entry.vg} ci={entry.ci} />
      )}
    </HeadlineWrapper>
  );
};
