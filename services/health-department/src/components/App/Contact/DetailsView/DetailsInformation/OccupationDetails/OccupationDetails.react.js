import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { DetailsContainer } from '../DetailsInformation.styled';
import { OccupationCard } from './OccupationCard';
import { getOccupationOptions } from '../DetailsInformation.helper';
import { OccupationHeader } from './OccupationHeader';
import {
  OccupationCardWrapper,
  OccupationGrid,
} from './OccupationDetails.styled';

export const OccupationDetails = ({ entry }) => {
  const intl = useIntl();
  const occupationOptions = getOccupationOptions(entry, intl);
  const [showOccupationDetails, setShowOccupationDetails] = useState(false);

  const hasAnyOccupationDetails =
    entry.ci != null || entry.vg != null || !!entry.jb || !!entry.em;

  return (
    <DetailsContainer>
      <OccupationHeader
        entry={entry}
        hasAnyOccupationDetails={hasAnyOccupationDetails}
        showOccupationDetails={showOccupationDetails}
        setShowOccupationDetails={setShowOccupationDetails}
      />
      {showOccupationDetails &&
        (!hasAnyOccupationDetails ? (
          <OccupationCardWrapper>
            <OccupationGrid>
              <OccupationCard
                field="empty"
                value=""
                label={intl.formatMessage({
                  id: 'contactDetails.occupation.options.notSpecified',
                })}
              />
            </OccupationGrid>
          </OccupationCardWrapper>
        ) : (
          <OccupationCardWrapper>
            {occupationOptions.map(option => (
              <OccupationGrid key={option.field} className="occupationGrid">
                <OccupationCard
                  field={option.field}
                  value={option.value}
                  label={option.label}
                />
              </OccupationGrid>
            ))}
          </OccupationCardWrapper>
        ))}
    </DetailsContainer>
  );
};
