import React, { useState } from 'react';

import { Header } from './Header';
import { DetailsInformation } from './DetailsInformation';
import { Messages } from './Messages';
import { Wrapper } from './DetailsView.styled';

export const DetailsView = ({ entry }) => {
  const [updateMessages, setUpdateMessages] = useState(false);

  return (
    <Wrapper>
      <Header entry={entry} setUpdateMessages={setUpdateMessages} />
      <DetailsInformation entry={entry} />
      <Messages
        entry={entry}
        setUpdateMessages={setUpdateMessages}
        updateMessages={updateMessages}
      />
    </Wrapper>
  );
};
