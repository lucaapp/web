import React, { useCallback, useEffect, useState } from 'react';
import Icon from '@ant-design/icons';

import {
  BackIcon,
  ContactActiveIcon,
  ContactDefaultIcon,
  SearchActiveIcon,
  SearchDefaultIcon,
} from 'assets/icons';

import { VersionFooter } from 'components/App/VersionFooter';
import { useKeyLoader } from 'components/hooks/useKeyLoader';

import { useIntl } from 'react-intl';
import { useLocation } from 'react-router';
import { PersonSearch } from './PersonSearch';
import { ContactList } from './ContactList';
import { DetailsView } from './DetailsView';
import {
  ContactWrapper,
  VersionFooterWrapper,
  SubMenu,
  StyledBackButton,
} from './Contact.styled';

const SearchIcon = ({ isActive, onClick }) => (
  <Icon
    onClick={onClick}
    component={isActive ? SearchActiveIcon : SearchDefaultIcon}
    style={{ fontSize: 32, marginRight: 16 }}
  />
);

const ContactIcon = ({ isActive, onClick }) => (
  <Icon
    onClick={onClick}
    component={isActive ? ContactActiveIcon : ContactDefaultIcon}
    style={{ fontSize: 32 }}
  />
);

const BackIconComp = () => (
  <Icon
    component={BackIcon}
    style={{ color: 'black', marginRight: 10, fontSize: 16 }}
  />
);

export const Contact = () => {
  const intl = useIntl();
  const [openPersonSearchView, setOpenPersonSearchView] = useState(true);
  const [openDetailsView, setOpenDetailsView] = useState(false);
  const [openContactListView, setOpenContactListView] = useState(false);
  const [contactDetails, setContactDetails] = useState(null);
  const { keysData, isLoading, error } = useKeyLoader();
  const location = useLocation();

  const clearHistory = useCallback(() => {
    location.replace({ ...location, state: undefined });
  }, [location]);

  useEffect(() => {
    window.addEventListener('beforeunload', () => clearHistory);
    return () => {
      window.removeEventListener('beforeunload', clearHistory);
    };
  }, [clearHistory]);

  useEffect(() => {
    if (location.state?.openContactList) {
      setOpenContactListView(true);
      setOpenPersonSearchView(false);
      setOpenDetailsView(false);
    }
  }, [location.state?.openContactList]);

  if (error || isLoading) return null;

  const renderPersonSearchView = () => {
    setOpenPersonSearchView(true);
    setOpenContactListView(false);
    setOpenDetailsView(false);
  };
  const renderContactListView = () => {
    setOpenContactListView(true);
    setOpenPersonSearchView(false);
    setOpenDetailsView(false);
  };

  const renderViewComponent = () => {
    if (openPersonSearchView && !openContactListView && !openDetailsView) {
      return <PersonSearch publicHDEKP={keysData.publicHDEKP} />;
    }
    if (openContactListView && !openPersonSearchView && !openDetailsView) {
      return (
        <ContactList
          setContactDetails={setContactDetails}
          setOpenDetailsView={setOpenDetailsView}
          setOpenContactListView={setOpenContactListView}
          setOpenPersonSearchView={setOpenPersonSearchView}
        />
      );
    }
    if (
      openDetailsView &&
      contactDetails &&
      !openPersonSearchView &&
      !openContactListView
    ) {
      return <DetailsView entry={contactDetails} />;
    }
    return <PersonSearch publicHDEKP={keysData.publicHDEKP} />;
  };

  return (
    <ContactWrapper>
      {openDetailsView && contactDetails ? (
        <SubMenu>
          <StyledBackButton
            type="text"
            onClick={() => {
              setOpenContactListView(true);
              setOpenDetailsView(false);
              setOpenPersonSearchView(false);
            }}
          >
            <BackIconComp />
            {intl.formatMessage({ id: 'contact.contactList.overview.button' })}
          </StyledBackButton>
        </SubMenu>
      ) : (
        <SubMenu>
          <SearchIcon
            isActive={openPersonSearchView}
            onClick={renderPersonSearchView}
          />
          <ContactIcon
            isActive={openContactListView}
            onClick={renderContactListView}
          />
        </SubMenu>
      )}
      {renderViewComponent()}
      <VersionFooterWrapper>
        <VersionFooter />
      </VersionFooterWrapper>
    </ContactWrapper>
  );
};
