import React from 'react';
import { StyledTooltip } from 'components/general';
import { useIntl } from 'react-intl';
import {
  StyledCriticalInfraIcon,
  StyledVulnerableGroupIcon,
} from './OccupationIcon.styled';

export const OccupationIcon = ({ vg, ci }) => {
  const intl = useIntl();
  return (
    <StyledTooltip
      title={intl.formatMessage({
        id: vg
          ? 'contact.table.occupation.tooltip.vg'
          : 'contact.table.occupation.tooltip.ci',
      })}
      placement="topLeft"
      minWidth="300px"
    >
      {vg ? (
        <StyledVulnerableGroupIcon />
      ) : ci ? (
        <StyledCriticalInfraIcon />
      ) : null}
    </StyledTooltip>
  );
};
