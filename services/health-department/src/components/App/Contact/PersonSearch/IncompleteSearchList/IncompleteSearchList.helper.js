import React from 'react';
import moment from 'moment';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';
import { getAllSearchProcesses } from 'network/api';
import { VerifySearch } from './VerifySearch';

export const useIncompleteSearchListData = () => {
  const intl = useIntl();
  const { data: searchProcesses } = useQuery(
    'searchProcesses',
    getAllSearchProcesses
  );

  const columns = [
    {
      key: 'date',
      render: function renderDate({ date }) {
        return (
          <>
            {intl.formatMessage(
              {
                id: 'contact.incompleteSearchList.searchDate',
              },
              { date }
            )}
          </>
        );
      },
    },
    {
      key: 'time',
      render: function renderTime({ time }) {
        return (
          <>
            {intl.formatMessage(
              {
                id: 'contact.incompleteSearchList.searchTime',
              },
              { time }
            )}
          </>
        );
      },
    },
    {
      key: 'searchCount',
      render: function renderSearchCount({ searchCount }) {
        return (
          <>
            {intl.formatMessage(
              {
                id: 'contact.incompleteSearchList.searchCount',
              },
              { searchCount }
            )}
          </>
        );
      },
    },
    {
      key: 'verifiedCount',
      render: function renderVerifiedCount({ verifiedCount, totalCount }) {
        return (
          <>
            {intl.formatMessage(
              {
                id: 'contact.incompleteSearchList.verifiedCount',
              },
              { verifiedCount, totalCount }
            )}
          </>
        );
      },
    },
    {
      key: 'verify',
      render: function renderVerifySearch({
        verify,
        verifiedCount,
        totalCount,
      }) {
        return (
          <VerifySearch
            searchProcessId={verify}
            verifiedCount={verifiedCount}
            totalCount={totalCount}
          />
        );
      },
      align: 'right',
    },
  ];

  if (!searchProcesses) return { columns, data: [] };

  const data = searchProcesses
    .map(searchProcess => ({
      key: searchProcess.uuid,
      createdAt: searchProcess.createdAt,
      date: moment.unix(searchProcess.createdAt).format('DD.MM.YYYY'),
      time: moment.unix(searchProcess.createdAt).format('HH:mm'),
      searchCount: searchProcess.length,
      verifiedCount: searchProcess.completed,
      totalCount: searchProcess.length,
      verify: searchProcess.uuid,
    }))
    .sort((a, b) => b.createdAt - a.createdAt);

  return { columns, data };
};
