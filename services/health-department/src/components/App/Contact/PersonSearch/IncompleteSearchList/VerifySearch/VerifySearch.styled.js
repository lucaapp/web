import styled from 'styled-components';
import { PrimaryButton } from 'components/general';

export const StyledVerifyButton = styled(PrimaryButton)`
  width: 285px;
`;
