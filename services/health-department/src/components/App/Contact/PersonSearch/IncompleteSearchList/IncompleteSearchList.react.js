import React from 'react';
import { Spin } from 'antd';
import { useIntl } from 'react-intl';
import { Headline, StyledTable, Subtitle } from './IncompleteSearchList.styled';
import { useIncompleteSearchListData } from './IncompleteSearchList.helper';

export const IncompleteSearchList = () => {
  const intl = useIntl();
  const { columns, data } = useIncompleteSearchListData();

  if (!data) return <Spin />;

  return (
    <>
      <Headline>
        {intl.formatMessage({ id: 'contact.incompleteSearchList.headline' })}
      </Headline>
      <Subtitle>
        {intl.formatMessage({ id: 'contact.incompleteSearchList.subtitle' })}
      </Subtitle>
      <StyledTable columns={columns} dataSource={data} pagination={false} />
    </>
  );
};
