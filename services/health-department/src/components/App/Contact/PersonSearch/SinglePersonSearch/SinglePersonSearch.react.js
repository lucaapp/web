import React from 'react';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import { Form } from 'antd';

import { CONTACT_ROUTE } from 'constants/routes';
import { createSearch } from 'utils/reachableContacts';

import { SearchIcon } from 'assets/icons';

import { PrimaryButton } from 'components/general';
import { usePersonNameValidator } from 'components/hooks/useValidators';
import {
  Headline,
  Description,
  SearchFieldsWrapper,
  StyledInput,
  Wrapper,
  ButtonWrapper,
  StyledFirstNameFormInput,
  StyledLastNameFormInput,
} from './SinglePersonSearch.styled';

export const SinglePersonSearch = ({ publicHDEKP }) => {
  const intl = useIntl();
  const history = useHistory();
  const firstNameValidator = usePersonNameValidator('firstName');
  const lastNameValidator = usePersonNameValidator('lastName');

  const onFinish = ({ firstName, lastName }) => {
    const trimmedFirstName = firstName.trim();
    const trimmedLastName = lastName.trim();
    return createSearch(publicHDEKP, [
      { firstName: trimmedFirstName, lastName: trimmedLastName },
    ])
      .then(uuid => history.push(`${CONTACT_ROUTE}/${uuid}`))
      .catch(() => {});
  };

  return (
    <Wrapper>
      <Headline>
        {intl.formatMessage({ id: 'contact.singlePersonSearch.headline' })}
      </Headline>
      <Description>
        {intl.formatMessage({ id: 'contact.singlePersonSearch.description' })}
      </Description>
      <Form onFinish={onFinish}>
        <SearchFieldsWrapper>
          <StyledFirstNameFormInput name="firstName" rules={firstNameValidator}>
            <StyledInput
              placeholder={intl.formatMessage({
                id: 'profile.firstname',
              })}
              prefix={<SearchIcon />}
            />
          </StyledFirstNameFormInput>
          <StyledLastNameFormInput name="lastName" rules={lastNameValidator}>
            <StyledInput
              placeholder={intl.formatMessage({
                id: 'profile.lastname',
              })}
              prefix={<SearchIcon />}
            />
          </StyledLastNameFormInput>
        </SearchFieldsWrapper>
        <Form.Item>
          <ButtonWrapper>
            <PrimaryButton type="primary" htmlType="submit">
              {intl.formatMessage({
                id: 'contact.personSearch.search.button',
              })}
            </PrimaryButton>
          </ButtonWrapper>
        </Form.Item>
      </Form>
    </Wrapper>
  );
};
