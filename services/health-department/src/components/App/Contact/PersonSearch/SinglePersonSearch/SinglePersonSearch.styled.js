import styled from 'styled-components';
import { Form, Input } from 'antd';

export const Wrapper = styled.div`
  width: 60%;
`;

export const Headline = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 24px;
`;

export const SearchFieldsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 16px 0;
`;

export const StyledFirstNameFormInput = styled(Form.Item)`
  width: 50%;
  margin-right: 48px;
`;

export const StyledLastNameFormInput = styled(Form.Item)`
  width: 50%;
`;

export const StyledInput = styled(Input)`
  border-top: none;
  border-left: none;
  border-right: none;
  border-bottom: 1px solid #000000;
  border-radius: 0;
  box-shadow: none;
  &:hover,
  &:focus,
  &:active {
    border-top: none;
    border-left: none;
    border-right: none;
    border-bottom: 1px solid #000000;
    border-radius: 0;
    margin-right: 48px;
    box-shadow: none;
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 48px 0;
`;
