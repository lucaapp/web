import React, { useState } from 'react';
import { DownloadIcon } from 'assets/icons';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import { CONTACT_ROUTE } from 'constants/routes';
import { FileUploadComp } from 'components/general/FileUpload';
import { PrimaryButton } from 'components/general';
import { createSearch } from 'utils/reachableContacts';
import {
  parseFile,
  downloadTemplate,
  handleErrorMessages,
} from './ListSearch.helper';
import {
  ButtonWrapper,
  Description,
  Headline,
  SearchPersonTemplate,
  SearchPhoneNumberTemplate,
  StyledButtonLink,
  TemplateWrapper,
  Wrapper,
} from './ListSearch.styled';

export const ListSearch = ({ publicHDEKP }) => {
  const intl = useIntl();
  const history = useHistory();

  const [progressPercent, setProgressPercent] = useState(0);
  const [uploadStatus, setUploadStatus] = useState('');
  const [uploadMessageId, setUploadMessageId] = useState(
    'contact.listSearch.uploadPersonList'
  );
  const [fileData, setFileData] = useState(null);

  const onFile = ({ file }) => {
    parseFile(file)
      .then(data => {
        setFileData(data);
        setProgressPercent(100);
        setUploadStatus('success');
        setUploadMessageId('contact.listSearch.uploadFileMessage.success');
      })
      .catch(error => {
        setProgressPercent(100);
        setUploadStatus('exception');
        handleErrorMessages(error, setUploadMessageId);
      });
  };

  return (
    <Wrapper>
      <Headline>
        {intl.formatMessage({ id: 'contact.listSearch.headline' })}
      </Headline>
      <Description>
        {intl.formatMessage({ id: 'contact.listSearch.description' })}
      </Description>
      <TemplateWrapper>
        <SearchPersonTemplate>
          <DownloadIcon />
          <StyledButtonLink
            type="link"
            onClick={() => downloadTemplate('name')}
          >
            {intl.formatMessage({
              id: 'contact.listSearch.template.searchPersons',
            })}
          </StyledButtonLink>
        </SearchPersonTemplate>
        <SearchPhoneNumberTemplate>
          <DownloadIcon />
          <StyledButtonLink
            type="link"
            onClick={() => downloadTemplate('phone')}
          >
            {intl.formatMessage({
              id: 'contact.listSearch.template.searchPhoneNumbers',
            })}
          </StyledButtonLink>
        </SearchPhoneNumberTemplate>
      </TemplateWrapper>

      <FileUploadComp
        type="file"
        accept=".xlsx, .xls"
        customRequest={onFile}
        buttonType="secondary"
        buttonTextId="contact.listSearch.uploadFile.button"
        progressPercent={progressPercent}
        uploadStatus={uploadStatus}
        uploadMessageId={uploadMessageId}
      />
      <ButtonWrapper>
        <PrimaryButton
          disabled={!fileData}
          onClick={() =>
            createSearch(publicHDEKP, fileData)
              .then(uuid => history.push(`${CONTACT_ROUTE}/${uuid}`))
              .catch(() => {})
          }
        >
          {intl.formatMessage({
            id: 'contact.personSearch.search.button',
          })}
        </PrimaryButton>
      </ButtonWrapper>
    </Wrapper>
  );
};
