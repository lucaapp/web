import React from 'react';
import { useHistory } from 'react-router-dom';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import { CONTACT_ROUTE } from 'constants/routes';
import { createSearch } from 'utils/reachableContacts';
import { SearchIcon } from 'assets/icons';

import { PrimaryButton } from 'components/general';
import { usePhoneValidator } from 'components/hooks/useValidators';
import {
  ButtonWrapper,
  Description,
  Headline,
  StyledInput,
  Wrapper,
} from './PhoneSearch.styled';

export const PhoneSearch = ({ publicHDEKP }) => {
  const intl = useIntl();
  const history = useHistory();
  const phoneValidator = usePhoneValidator('phone');

  const onFinish = ({ phone }) => {
    const trimmedPhone = phone.trim();
    return createSearch(publicHDEKP, [{ phone: trimmedPhone }])
      .then(uuid => history.push(`${CONTACT_ROUTE}/${uuid}`))
      .catch(() => {});
  };

  return (
    <Wrapper>
      <Headline>
        {intl.formatMessage({ id: 'contact.phoneSearch.headline' })}
      </Headline>
      <Description>
        {intl.formatMessage({ id: 'contact.phoneSearch.description' })}
      </Description>
      <Form onFinish={onFinish}>
        <Form.Item name="phone" rules={phoneValidator}>
          <StyledInput
            placeholder={intl.formatMessage({
              id: 'contact.phoneSearch.phoneNumber',
            })}
            prefix={<SearchIcon />}
          />
        </Form.Item>

        <ButtonWrapper>
          <PrimaryButton type="primary" htmlType="submit">
            {intl.formatMessage({
              id: 'contact.personSearch.search.button',
            })}
          </PrimaryButton>
        </ButtonWrapper>
      </Form>
    </Wrapper>
  );
};
