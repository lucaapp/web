import React from 'react';
import moment from 'moment';
import { Badge, Spin } from 'antd';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';

import { getAllConversations } from 'network/api';
import { decryptAndValidateHDReencryptedContactData } from 'utils/reachableContacts';

import { getFormattedDate } from 'utils/time';
import { VaccinationCertificateInfo } from '../VaccinationCertificateInfo';

import { Info, StatusWrapper, Section } from './ContactList.styled';
import { OccupationIcon } from '../reusableComponents/OccupationIcon';

const groupConversationsByDate = conversations =>
  conversations.reduce((groupedByDate, conversationObject) => {
    const key = moment
      .unix(conversationObject.createdAt)
      .startOf('day')
      .diff(moment.unix(0).startOf('day'), 'days');
    return {
      ...groupedByDate,
      [key]: [...(groupedByDate[key] || []), conversationObject],
    };
  }, {});

const getDataEntry = (key, conversationsList) => {
  const children = conversationsList
    .map(conversation => {
      let decryptedUserData;

      try {
        decryptedUserData = decryptAndValidateHDReencryptedContactData(
          conversation.contact
        );
      } catch (error) {
        console.error(error);
        return null;
      }

      const contactData = decryptedUserData.cd;

      return {
        key: conversation.uuid,
        createdAt: conversation.createdAt,
        uuid: conversation.uuid,
        firstName: contactData.fn,
        lastName: contactData.ln,
        phone: contactData.pn,
        email: contactData.e,
        address: {
          zipCode: contactData.pc,
          city: contactData.c,
          street: contactData.st,
          streetNo: contactData.hn,
        },
        contacted: !!conversation.lastContactedAt,
        certificate: decryptedUserData.vct,
        rawCertificateData: decryptedUserData.ct,
        seed: decryptedUserData.ns,
        mekp: decryptedUserData.ep,
        ci: decryptedUserData.ci,
        vg: decryptedUserData.vg,
        jb: decryptedUserData.jb,
        em: decryptedUserData.em,
      };
    })
    .filter(result => !!result);

  return {
    key,
    children,
    section: conversationsList[0].createdAt,
  };
};

export const useContactListData = () => {
  const intl = useIntl();

  const {
    data: conversations,
    isLoading: isConversationsLoading,
    error: conversationsError,
  } = useQuery('conversations', getAllConversations);

  const columns = [
    {
      title: intl.formatMessage({ id: 'contact.table.header.firstName' }),
      key: 'firstName',
      render: ({ section, firstName }) => {
        if (section) {
          return <Section>{getFormattedDate(section)}</Section>;
        }
        return <Info>{firstName}</Info>;
      },
    },
    {
      title: intl.formatMessage({ id: 'contact.table.header.lastName' }),
      dataIndex: 'lastName',
      key: 'lastName',
      render: text => <Info>{text}</Info>,
    },
    {
      title: intl.formatMessage({ id: 'contact.table.header.address' }),
      dataIndex: 'address',
      key: 'address',
      render: address =>
        address ? <Info>{`${address.zipCode} ${address.city}`}</Info> : '',
    },
    {
      title: intl.formatMessage({ id: 'contact.table.header.birthdate' }),
      key: 'birthday',
      render: ({ certificate }) => <Info>{certificate?.bd}</Info>,
    },
    {
      title: intl.formatMessage({
        id: 'contact.table.header.vaccinationStatus',
      }),
      key: 'vaccinationStatus',
      render: ({ certificate }) => (
        <VaccinationCertificateInfo certificate={certificate} />
      ),
    },
    {
      title: intl.formatMessage({
        id: 'contact.table.header.contacted',
      }),
      key: 'contacted',
      render: ({ contacted, section }) => {
        if (section) {
          return null;
        }
        return (
          <StatusWrapper>
            <Badge status={contacted ? 'success' : 'error'} />
            <Info>
              {intl.formatMessage({
                id: contacted
                  ? 'contact.table.processStatus.yes'
                  : 'contact.table.processStatus.no',
              })}
            </Info>
          </StatusWrapper>
        );
      },
    },
    {
      key: 'occupation',
      render: ({ vg, ci, section }) => {
        if (section) {
          return null;
        }
        return <OccupationIcon vg={vg} ci={ci} />;
      },
    },
  ];
  if (!conversations) return { columns, data: [] };
  const conversationsGroupedByDate = groupConversationsByDate(conversations);
  const data = [];
  for (const [key, conversationsList] of Object.entries(
    conversationsGroupedByDate
  )) {
    data.push(getDataEntry(key, conversationsList));
  }

  if (isConversationsLoading) return <Spin />;

  if (conversationsError) {
    console.error(conversationsError);
    return { columns, data: [] };
  }

  return {
    columns,
    data: data.sort((a, b) => b.section - a.section),
  };
};
