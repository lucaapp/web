import styled from 'styled-components';
import { Table } from 'antd';

export const ContactListWrapper = styled.div`
  background-color: #ffffff;
  box-shadow: 0px 2px 4px 0px rgba(78, 97, 128, 0.5);
  border-radius: 4px;
  margin-bottom: 48px;
  min-height: 100vh;
`;

export const ContactListHeadline = styled.div`
  color: rgb(0, 0, 0);
  font-size: 34px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  padding: 32px;
`;

export const StyledTable = styled(Table)`
  .ant-table table {
    margin-bottom: 20px;
    padding: 0 32px;
  }

  .ant-table-thead > tr > .ant-table-cell::before {
    display: none;
  }

  .ant-table-tbody > .ant-table-row-level-1 {
    cursor: pointer;
  }
`;

export const Info = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;

export const StatusWrapper = styled.div`
  display: flex;
`;

export const Section = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 32px 32px 48px 0;
`;
