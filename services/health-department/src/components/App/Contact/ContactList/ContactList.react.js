import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { PrimaryButton } from 'components/general';
import { useModal } from 'components/hooks/useModal';
import { SendMessageModal } from 'components/App/modals/SendMessageModal';
import { Popconfirm, Spin } from 'antd';
import {
  ButtonWrapper,
  StyledTable,
  ContactListWrapper,
  ContactListHeadline,
} from './ContactList.styled';

import { useContactListData } from './ContactList.helper';

export const ContactList = ({
  setContactDetails,
  setOpenDetailsView,
  setOpenContactListView,
  setOpenPersonSearchView,
}) => {
  const { columns, data } = useContactListData();
  const intl = useIntl();
  const [openModal, closeModal] = useModal();
  const [selectedPersons, setSelectedPersons] = useState(null);

  if (!data) return <Spin />;

  const rowSelection = {
    checkStrictly: false,
    onChange: (selectedRowKeys, selectedRows) => {
      const filteredChildrenRows = selectedRows.filter(
        selectedRow => !selectedRow.children
      );
      setSelectedPersons(filteredChildrenRows);
    },
  };

  const openSendMessageModal = () => {
    openModal({
      content: (
        <SendMessageModal
          multipleEntries={selectedPersons}
          isMultipleEntries
          closeModal={closeModal}
        />
      ),
    });
  };

  return (
    <ContactListWrapper>
      <ContactListHeadline>
        {intl.formatMessage({
          id: 'contact.contactList.headline',
        })}
      </ContactListHeadline>
      <StyledTable
        columns={columns}
        dataSource={data}
        pagination={false}
        locale={{
          emptyText: (
            <span>
              {intl.formatMessage({
                id: 'contact.contactList.noData',
              })}
            </span>
          ),
        }}
        onRow={record => ({
          onClick: () => {
            if (!record.children) {
              setContactDetails(record);
              setOpenDetailsView(true);
              setOpenContactListView(false);
              setOpenPersonSearchView(false);
            }
          },
        })}
        rowSelection={{ ...rowSelection }}
      />
      <ButtonWrapper>
        <Popconfirm
          disabled={!selectedPersons}
          placement="bottomRight"
          title={intl.formatMessage({
            id: 'modal.lucaConnect.contacting',
          })}
          okText={intl.formatMessage({
            id: 'modal.lucaConnect.contacting.confirm',
          })}
          cancelText={intl.formatMessage({
            id: 'modal.lucaConnect.contacting.cancel',
          })}
          onConfirm={openSendMessageModal}
        >
          <PrimaryButton disabled={!selectedPersons}>
            {intl.formatMessage({
              id: 'contact.contactList.select.button',
            })}
          </PrimaryButton>
        </Popconfirm>
      </ButtonWrapper>
    </ContactListWrapper>
  );
};
