import styled from 'styled-components';

export const Divider = styled.div`
  border: 1px solid rgb(151, 151, 151);
  height: 1px;
  width: 100%;
  margin-bottom: 24px;
`;
