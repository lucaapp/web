import styled from 'styled-components';
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';

export const SuccessCheckIcon = styled(CheckCircleOutlined)`
  font-size: 20px;
  color: #819e57;
`;

export const UnsuccessCheckIcon = styled(CloseCircleOutlined)`
  font-size: 20px;
  color: #ff0000;
`;
