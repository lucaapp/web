export const getOverlayInnerStyle = (minWidth = 0) => ({
  color: '#000000',
  fontFamily: 'Montserrat-Medium, sans-serif',
  fontSize: 14,
  fontWeight: 500,
  background: 'rgb(195, 206, 217)',
  boxShadow: 'none',
  padding: 16,
  borderRadius: 8,
  minWidth,
});
