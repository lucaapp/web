export const DEVICE_TYPE_IOS = 0;
export const DEVICE_TYPE_ANDROID = 1;
export const DEVICE_TYPE_STATIC = 2;
export const DEVICE_TYPE_WEBAPP = 3;
export const DEVICE_TYPE_FORM = 4;

export const NOTIFIABLE_DEVICE_TYPES = {
  IOS: DEVICE_TYPE_IOS,
  ANDROID: DEVICE_TYPE_ANDROID,
};

export const DEVICE_APP = 'app';
export const DEVICE_BADGE = 'badge';
export const DEVICE_CONTACT_FORM = 'contactForm';
