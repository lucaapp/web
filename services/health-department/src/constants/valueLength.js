export const MAX_PRIVATE_KEY_FILE_SIZE = 2000;
export const MAX_NAME_LENGTH = 120;
export const MAX_PHONE_LENGTH = 20;
export const MAX_EMAIL_LENGTH = 255;
export const MAX_PROCESS_NOTE_LENGTH = 500;
