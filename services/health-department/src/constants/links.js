export const GITLAB_LINK =
  'https://gitlab.com/lucaapp/web/-/tree/master/services/health-department';
export const FAQ_LINK = 'https://www.luca-app.de/faq/';

export const INTELIGENT_CONTACT_TRACING_VIDEO =
  'https://www.youtube.com/watch?v=o88e4JFYazE';

export const INTELIGENT_CONTACT_TRACING_PRESENTATION =
  'https://shared-link.bdrive.cloud/shared/1f7c2a5f-b57c-49f5-8034-6f33db71e348#8f685fe9b30930ad14555e8b7dcbf3dd2e4803f120a6647bbc29f3fd06c18c08';

export const HEALTH_DEPARTMENT_HANDOUT =
  'https://shared-link.bdrive.cloud/shared/6a2c3892-bc6b-46fc-a84f-d1bc4f5d7363#96cdb1a92be0a4653abee216ce847b30e5acc39cf2253eaa524be39a2b5b8dd4';

export const SHARE_HISTORY_HELP_LINK =
  'https://shared-link.bdrive.cloud/shared/dc76b124-ce17-46db-8ac9-74fec45f61dd#525688ea801e0d2fc5a661393519b57a322ab8a10bcc6192f6fb4aac2d042879';

export const LUCA_LOCATIONS_HANDOUT =
  'https://shared-link.bdrive.cloud/shared/fd5c3f1d-6615-4671-99dd-bf30bef835dd#c07aed5c27fe61388c02e11f97d4ff55a45e5c001b1a1f2c9c817edcfad9e227';

export const WEBINAR_LINK =
  'https://outlook.office365.com/owa/calendar/neXenioEngineering@nexenio.com/bookings/s/h1-tEMPLEkO6ziQNb0pblQ2';
