import {
  bytesToHex,
  base64ToHex,
  VERIFY_EC_SHA256_IEEE_SIGNATURE,
} from '@lucaapp/crypto';
import {
  InvalidDSCSignatureError,
  parseEUDigitalGreenCertificate,
} from '@lucaapp/eudgc';
import { DSC_TRUST_ANCHOR } from 'constants/eudgc';
import { getTrustList } from 'network/api';

import { normalizeName } from './normalizeName';

export const getEUDGCIssuers = async () => {
  const responseText = await getTrustList();
  const [signature, payload] = responseText.split('\n');

  const isValid = VERIFY_EC_SHA256_IEEE_SIGNATURE(
    DSC_TRUST_ANCHOR,
    bytesToHex(payload),
    base64ToHex(signature)
  );

  if (!isValid) {
    throw new InvalidDSCSignatureError();
  }

  const issuers = {};
  const trustList = JSON.parse(payload);

  for (const issuer of trustList.certificates) {
    issuers[issuer.kid] = issuer;
  }

  return issuers;
};

export const validateAndParseCertData = async ({
  certificate,
  issuers,
  firstName,
  lastName,
}) => {
  const parsedCert = await parseEUDigitalGreenCertificate(
    certificate,
    kid => issuers[kid]
  );

  const certFirstName = parsedCert.nam.gn;
  const certLastName = parsedCert.nam.fn;

  if (
    normalizeName(firstName) !== normalizeName(certFirstName) ||
    normalizeName(lastName) !== normalizeName(certLastName)
  ) {
    throw new Error('Name missmatch in eudgc cert');
  }

  if (parsedCert.v?.[0])
    return {
      bd: parsedCert.dob,
      vc: {
        cd: parsedCert.v[0].dn,
        td: parsedCert.v[0].sd,
        d: parsedCert.v[0].dt,
      },
    };

  if (parsedCert.r?.[0])
    return {
      bd: parsedCert.dob,
      r: { d: parsedCert.r[0].fr },
    };

  throw new Error('Neither a recovery nor a vaccination cert');
};
