import { z as zod } from 'zod';
import validator from 'validator';
import { base64ToBytes } from '@lucaapp/crypto';
import {
  DEVICE_TYPE_IOS,
  DEVICE_TYPE_ANDROID,
  DEVICE_TYPE_STATIC,
  DEVICE_TYPE_WEBAPP,
  DEVICE_TYPE_FORM,
} from 'constants/deviceTypes';

export const z = {
  ...zod,

  base64: ({ min, max, length, rawLength } = {}) =>
    zod
      .string()
      .min(min)
      .max(max)
      .length(length)
      .refine(value => {
        if (!validator.isBase64(value)) return false;
        if (!rawLength) return true;

        return base64ToBytes(value).length === rawLength;
      }),

  uuid: () =>
    zod
      .string()
      .length(36)
      .refine(value => validator.isUUID(value, 'all')),

  unixTimestamp: () => zod.number().int().positive(),

  userDataSecret: () => z.base64({ rawLength: 16 }),
  userTracingSecret: () => z.base64({ rawLength: 16 }),

  deviceType: () =>
    z.union([
      z.literal(DEVICE_TYPE_IOS),
      z.literal(DEVICE_TYPE_ANDROID),
      z.literal(DEVICE_TYPE_STATIC),
      z.literal(DEVICE_TYPE_WEBAPP),
      z.literal(DEVICE_TYPE_FORM),
    ]),

  ecCompressedPublicKey: () => z.base64({ length: 44, rawLength: 33 }),
};
