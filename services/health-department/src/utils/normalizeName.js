export const normalizeName = name => {
  const whiteSpacePattern = /^\s+|\s+$/gm;
  const titlePattern = /\b(?:DR *|PROF *|)\b/g;
  const commaPattern = /,/g;
  const dotPattern = /\./g;

  const normalizedName = name
    .toUpperCase()
    .replace(titlePattern, '')
    .replace(commaPattern, '')
    .replace(dotPattern, '')
    .replace(whiteSpacePattern, '');
  const nameParts = normalizedName.split(' ');
  const firstNamePart = nameParts[0].split('-');
  return firstNamePart[0];
};
