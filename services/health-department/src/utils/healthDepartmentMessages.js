export const getHealthDepartmentMessages = (
  localeConfig,
  departmentId,
  level,
  intl
) => {
  const departmentInfo = localeConfig.departments.find(
    departmentMessageObject => departmentMessageObject.uuid === departmentId
  );

  const messageLocale = intl.locale === 'de' ? 'de' : 'en';
  const defaultMessages = localeConfig.default[level].messages[messageLocale];

  const messages = Object.assign(
    defaultMessages,
    departmentInfo.config[level]?.messages?.[messageLocale]
  );

  for (const [key, value] of Object.entries(messages)) {
    messages[key] = value
      .replace(/\(\(/g, '{')
      .replace(/\)\)/g, '}')
      .replace(/^\s*\n/gm, '{br}')
      .replace(/\n/g, '{br}');
  }
  return {
    messages,
    healthDepartmentName: departmentInfo.name,
    email: departmentInfo.email,
    phone: departmentInfo.phone,
  };
};
