/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  coveragePathIgnorePatterns: [],
  modulePaths: ['<rootDir>', '<rootDir>/src'],
  moduleDirectories: ['node_modules'],
  testPathIgnorePatterns: ['/node_modules/'],
  transform: {
    '^.+\\.(js|jsx)?$': 'babel-jest',
    '^.+\\.svg$': 'jest-svg-transformer',
  },
  testEnvironment: 'jsdom',
  setupFilesAfterEnv: ['./src/utils/jest.setup.js'],
  reporters: [
    'default',
    [
      'jest-junit',
      {
        classNameTemplate: '{suitename}',
        titleTemplate: '{classname} {title}',
        addFileAttribute: 'true',
        filePathPrefix: 'services/health-department',
      },
    ],
  ],
};
