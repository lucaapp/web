const CracoLessPlugin = require('craco-less');
const { LicenseWebpackPlugin } = require('license-webpack-plugin');
const GenerateJsonPlugin = require('generate-json-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const { DefinePlugin } = require('webpack');
const WorkerPlugin = require('worker-plugin');
const CspHtmlWebpackPlugin = require('csp-html-webpack-plugin');
const UnusedWebpackPlugin = require('unused-webpack-plugin');
const path = require('path');

const { THEME } = require('./ant.theme');

function generateVersionFile() {
  return {
    commit: process.env.GIT_COMMIT,
    version: process.env.GIT_VERSION,
  };
}

const licenseTypeOverrides = {
  // part of libphonenumber-js which is licensed under MIT
  'libphonenumber-js-core': 'MIT',
  'libphonenumber-js-min': 'MIT',
  'libphonenumber-js-max': 'MIT',
  yaqrcode: 'MIT', // https://github.com/zenozeng/node-yaqrcode/blob/master/LICENSE
};

module.exports = {
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: THEME,
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
  webpack: {
    plugins: {
      add: [
        new UnusedWebpackPlugin({
          directories: [path.join(__dirname, 'src')],
          // Exclude patterns
          exclude: ['*.test.js', '__mocks__', 'jest.setup.js', 'testing.js'],
          root: __dirname,
        }),
        new CspHtmlWebpackPlugin(
          {
            'img-src':
              "'self' *.gstatic.com *.googleapis.com data: *.intercomassets.com *.intercomassets.eu *.intercomcdn.com *.intercomcdn.eu",
            'font-src':
              "'self' *.gstatic.com *.intercomcdn.com *.intercomcdn.eu *.googleapis.com data:",
            'script-src':
              "'self' *.googleapis.com *.intercom.io *.intercomcdn.com *.intercomcdn.eu 'unsafe-eval'",
            'script-src-elem':
              "'self' *.googleapis.com *.intercom.io *.intercomcdn.com *.intercomcdn.eu lucaapp.matomo.cloud 'unsafe-inline'",
            'connect-src':
              "'self' wss: *.googleapis.com *.intercom.io *.intercomcdn.com *.intercomcdn.eu lucaapp.matomo.cloud 'unsafe-eval'",
            'worker-src': "'self' blob:",
            'child-src': "'self' blob: https://intercom-sheets.com/*",
            'style-src': "'self' *.googleapis.com 'unsafe-inline'",
            'default-src':
              "'self' wss://localhost:8080 luca-app.de *.luca-app.de",
          },
          {
            enabled: true,
            hashingMethod: 'sha256',
            hashEnabled: {
              'script-src': true,
              'style-src': true,
            },
            nonceEnabled: {
              'script-src': false,
              'style-src': false,
            },
          }
        ),
        new CompressionPlugin({
          algorithm: 'gzip',
          test: /\.(js|css|html|svg|json|ico|eot|otf|ttf)$/,
          deleteOriginalAssets: false,
        }),
        new DefinePlugin({
          'process.env.REACT_APP_VERSION': JSON.stringify(
            require('./package.json').version
          ),
        }),
        new LicenseWebpackPlugin({
          perChunkOutput: false,
          outputFilename: 'licenses.json',
          stats: {
            warnings: false,
            errors: false,
          },
          licenseTypeOverrides,
          renderLicenses: modules => {
            const licenseList = [];
            modules.forEach(
              ({ packageJson: { name, version, licenses }, licenseId }) => {
                if (!licenseId && !licenses) {
                  console.error(`ERROR: Unknown license for package ${name}`);
                  // eslint-disable-next-line unicorn/no-process-exit
                  process.exit(1);
                }
                licenseList.push({
                  name,
                  version,
                  license: licenseId || licenses,
                });
              }
            );
            return JSON.stringify(licenseList);
          },
        }),
        new LicenseWebpackPlugin({
          perChunkOutput: false,
          outputFilename: 'licenses-full.txt',
          stats: {
            warnings: false,
            errors: false,
          },
          licenseTypeOverrides,
        }),
        new GenerateJsonPlugin('version.json', generateVersionFile()),
        new WorkerPlugin(),
      ],
    },
    configure: config => {
      config.module.rules.push({
        test: /\.wasm$/,
        loader: 'base64-loader',
        type: 'javascript/auto',
      });

      // eslint-disable-next-line no-param-reassign
      config.module.noParse = /\.wasm$/;
      config.module.rules.forEach(rule => {
        (rule.oneOf || []).forEach(oneOf => {
          if (oneOf.loader && oneOf.loader.includes('file-loader')) {
            oneOf.exclude.push(/\.wasm$/);
          }
        });
      });
      return config;
    },
  },
};
