import { getJwt } from '../utils/jwtStorage';
import { requestWithStatusHandlers, checkResponse } from './utils';

const POS_API_PATH = '/pos/api';

const headers = jwt => ({
  'Content-Type': 'application/json',
  'X-Auth': jwt,
});

export const getPOSSystems = async locationGroupId =>
  requestWithStatusHandlers(
    'GET',
    `${POS_API_PATH}/v1/locationGroups/${locationGroupId}/systems`,
    headers(await getJwt()),
    null,
    () => null
  );

export const createPOSSystem = async (locationGroupId, posSystemData) =>
  fetch(`${POS_API_PATH}/v1/locationGroups/${locationGroupId}/systems`, {
    method: 'POST',
    headers: headers(await getJwt()),
    body: JSON.stringify(posSystemData),
  }).then(checkResponse);

export const editPOSSystem = async (locationGroupId, systemId, posSystemData) =>
  fetch(
    `${POS_API_PATH}/v1/locationGroups/${locationGroupId}/systems/${systemId}`,
    {
      method: 'PATCH',
      headers: headers(await getJwt()),
      body: JSON.stringify(posSystemData),
    }
  ).then(checkResponse);

export const removePOSSystem = async (locationGroupId, systemId) =>
  fetch(
    `${POS_API_PATH}/v1/locationGroups/${locationGroupId}/systems/${systemId}`,
    {
      method: 'DELETE',
      headers: headers(await getJwt()),
    }
  ).then(checkResponse);
