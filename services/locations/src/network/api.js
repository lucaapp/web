/* eslint-disable max-lines */
import { base64ToHex } from '@lucaapp/crypto';
import moment from 'moment';
import * as ApiUtils from './utils';

const API_PATH = '/api';

const headers = {
  'Content-Type': 'application/json',
};

const getRequest = path => ApiUtils.getRequest(path, headers);

// AUTH
export const login = data =>
  fetch(`${API_PATH}/v4/auth/operator/login`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  });

export const logout = () =>
  fetch(`${API_PATH}/v4/auth/logout`, {
    method: 'POST',
    headers,
  });

export const getMe = () => getRequest(`${API_PATH}/v4/auth/operator/me`);

export const getPrivateKeySecret = () =>
  getRequest(`${API_PATH}/v3/operators/privateKeySecret`).then(data =>
    data && data.privateKeySecret ? base64ToHex(data.privateKeySecret) : null
  );
export const getLastKeyUpdate = () =>
  getRequest(`${API_PATH}/v3/operators/lastKeyUpdate`).then(data =>
    data && data.lastUpdated ? data.lastUpdated : null
  );

export const checkEmail = email =>
  getRequest(`${API_PATH}/v3/operators/email/${email}`);

export const requestAccountDeletion = () =>
  fetch(`${API_PATH}/v3/operators`, {
    method: 'DELETE',
    headers,
  }).then(ApiUtils.checkResponse);

export const undoAccountDeletion = () =>
  fetch(`${API_PATH}/v3/operators/restore`, {
    method: 'POST',
    headers,
  }).then(ApiUtils.checkResponse);

// GROUPS
export const getGroups = () => getRequest(`${API_PATH}/v3/locationGroups`);

export const getDeletedGroups = () =>
  getRequest(`${API_PATH}/v3/operators/locationGroups/deleted`);

export const reactivateGroup = groupId =>
  fetch(`${API_PATH}/v3/operators/locationGroups/${groupId}/recover`, {
    method: 'POST',
    headers,
  });

export const getGroup = groupId =>
  getRequest(`${API_PATH}/v3/locationGroups/${groupId}`);

export const updateGroup = parameters =>
  fetch(`${API_PATH}/v3/locationGroups/${parameters.groupId}`, {
    method: 'PATCH',
    headers,
    body: JSON.stringify(parameters.data),
  }).then(ApiUtils.checkResponse);

export const deleteGroup = (groupId, data) =>
  fetch(`${API_PATH}/v3/locationGroups/${groupId}`, {
    method: 'DELETE',
    headers,
    body: JSON.stringify(data),
  });

export const createGroup = data =>
  fetch(`${API_PATH}/v3/locationGroups`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  })
    .then(ApiUtils.checkResponse)
    .then(response => response.json());

// OPERATOR
export const registerOperator = data =>
  fetch(`${API_PATH}/v3/operators`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  });

export const sendSupportMail = data => {
  return fetch(`${API_PATH}/v3/operators/support`, {
    method: 'POST',
    body: JSON.stringify(data),
    headers,
  });
};

export const storePublicKey = data =>
  fetch(`${API_PATH}/v3/operators/publicKey`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  });

export const updateLastSeenPrivateKey = () =>
  fetch(`${API_PATH}/v3/operators/confirmKeyImported`, {
    method: 'POST',
    headers,
  });

export const updatePublicKey = data =>
  fetch(`${API_PATH}/v3/operators/publicKey`, {
    method: 'PATCH',
    headers,
    body: JSON.stringify(data),
  });

export const forgotPassword = data =>
  fetch(`${API_PATH}/v3/operators/password/forgot`, {
    method: 'POST',
    body: JSON.stringify(data),
    headers,
  });

export const changePassword = data =>
  fetch(`${API_PATH}/v3/operators/password/change`, {
    method: 'POST',
    body: JSON.stringify(data),
    headers,
  });

export const resetPassword = data =>
  fetch(`${API_PATH}/v3/operators/password/reset`, {
    method: 'POST',
    body: JSON.stringify(data),
    headers,
  });

export const checkPassword = data =>
  fetch(`${API_PATH}/v3/operators/password/checkPassword`, {
    method: 'POST',
    body: JSON.stringify(data),
    headers,
  });

export const getPasswordResetRequest = resetId =>
  getRequest(`${API_PATH}/v3/operators/password/reset/${resetId}`);

export const updateOperator = data =>
  fetch(`${API_PATH}/v3/operators`, {
    method: 'PATCH',
    body: JSON.stringify(data),
    headers,
  });

export const updateEmail = data =>
  fetch(`${API_PATH}/v3/operators/email`, {
    method: 'PATCH',
    body: JSON.stringify(data),
    headers,
  });

export const isEmailUpdatePending = () =>
  getRequest(`${API_PATH}/v3/operators/email/isChangeActive`);

export const confirmEmail = data =>
  fetch(`${API_PATH}/v3/operators/email/confirm`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  });

// KEYS
export const getDailyKey = () =>
  getRequest(`${API_PATH}/v4/keys/daily/current`);

// LOCATION
export const getLocation = locationId =>
  getRequest(`${API_PATH}/v3/operators/locations/${locationId}`);

export const getLocationLinks = locationId =>
  getRequest(`${API_PATH}/v3/locations/${locationId}/urls`);

export const updateLocationLink = (locationId, data) =>
  fetch(`${API_PATH}/v3/locations/${locationId}/urls`, {
    method: 'PATCH',
    headers,
    body: JSON.stringify(data),
  });

export const activateAccount = data =>
  fetch(`${API_PATH}/v3/operators/activate`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  });

export const deleteLocation = (locationId, data) =>
  fetch(`${API_PATH}/v3/operators/locations/${locationId}`, {
    method: 'DELETE',
    headers,
    body: JSON.stringify(data),
  });

export const updateLocation = parameters =>
  fetch(`${API_PATH}/v3/operators/locations/${parameters.locationId}`, {
    method: 'PATCH',
    headers,
    body: JSON.stringify(parameters.data),
  });

export const updateAddress = parameters =>
  fetch(`${API_PATH}/v3/operators/locations/${parameters.locationId}/address`, {
    method: 'PATCH',
    headers,
    body: JSON.stringify(parameters.data),
  });

export const createLocation = data =>
  fetch(`${API_PATH}/v3/operators/locations`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  }).then(response => response.json());

export const forceCheckoutUsers = locationId =>
  fetch(`${API_PATH}/v3/operators/locations/${locationId}/check-out`, {
    method: 'POST',
    headers,
  });

export const forceCheckoutAllUsers = groupId =>
  fetch(`${API_PATH}/v3/locationGroups/${groupId}/checkout`, {
    method: 'POST',
    headers,
  });

export const forceCheckoutByOperator = traceId =>
  fetch(`${API_PATH}/v3/operators/traces/checkout`, {
    method: 'POST',
    body: JSON.stringify({
      traceId,
      timestamp: moment().seconds(0).unix(),
    }),
    headers,
  });

export const updateOpeningHours = (locationId, openingHours) =>
  fetch(`${API_PATH}/v4/operators/locations/${locationId}/openingHours`, {
    method: 'PATCH',
    headers,
    body: JSON.stringify(openingHours),
  });

export const getLocationTables = locationId =>
  getRequest(`${API_PATH}/v4/locations/${locationId}/tables`);

export const createLocationTable = (locationId, data) =>
  fetch(`${API_PATH}/v4/locations/${locationId}/tables`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  });

export const updateLocationTable = (locationId, tableId, data) =>
  fetch(`${API_PATH}/v4/locations/${locationId}/tables/${tableId}`, {
    method: 'PUT',
    headers,
    body: JSON.stringify(data),
  });

export const deleteLocationTable = (locationId, tableId) =>
  fetch(`${API_PATH}/v4/locations/${locationId}/tables/${tableId}`, {
    method: 'DELETE',
    headers,
  });

export const getLocationTableByTableId = tableId =>
  getRequest(`${API_PATH}/v4/locationTables/${tableId}`);

// COUNTER
export const getCurrentCount = scannerId =>
  getRequest(`${API_PATH}/v3/scanners/${scannerId}/traces/count/current`);

export const getTotalCount = groupId =>
  getRequest(`${API_PATH}/v3/locationGroups/${groupId}/traces/count/total`);

export const getAllTransfers = () =>
  getRequest(`${API_PATH}/v4/locationTransfers?deleted=false`);

export const getAllUncompletedTransfers = () =>
  getRequest(`${API_PATH}/v4/locationTransfers?deleted=false&completed=false`);

export const getLocationTransfer = transferId =>
  getRequest(`${API_PATH}/v4/locationTransfers/${transferId}`);

export const getLocationTransferTraces = transferId =>
  getRequest(`${API_PATH}/v4/locationTransfers/${transferId}/traces`);

export const getHealthDepartment = departmentId =>
  getRequest(`${API_PATH}/v3/healthDepartments/${departmentId}`);

export const shareData = data =>
  fetch(`${API_PATH}/v3/locationTransfers/${data.locationTransferId}`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data.traces),
  });

export const getAdditionalData = locationId =>
  getRequest(`${API_PATH}/v3/locations/additionalDataSchema/${locationId}`);

export const createAdditionalData = (additionalDataId, body = {}) =>
  fetch(`${API_PATH}/v3/locations/additionalDataSchema/${additionalDataId}`, {
    method: 'POST',
    headers,
    body: JSON.stringify(body),
  });

export const updateAdditionalData = (additionalDataId, data) =>
  fetch(`${API_PATH}/v3/locations/additionalDataSchema/${additionalDataId}`, {
    method: 'PATCH',
    headers,
    body: JSON.stringify(data),
  });

export const deleteAdditionalData = additionalDataId =>
  fetch(`${API_PATH}/v3/locations/additionalDataSchema/${additionalDataId}`, {
    method: 'DELETE',
    headers,
  });

// TRACES
export const getTraces = (locationId, duration) =>
  getRequest(
    `${API_PATH}/v3/operators/locations/traces/${locationId}/${
      duration ? `?duration=${duration}` : ''
    }`
  );

// TAN
export const requestTan = data =>
  fetch(`${API_PATH}/v3/sms/request`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  }).then(response => response.json());

export const verifyTan = async data => {
  const { status } = await fetch(`${API_PATH}/v3/sms/verify`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  });

  if (status !== 204) throw Error;
};

// CHALLENGES
export const getChallengeState = (
  challengeId,
  challengeType = 'operatorDevice'
) => getRequest(`${API_PATH}/v4/challenges/${challengeType}/${challengeId}`);

export const createChallenge = (
  challengeType = 'operatorDevice',
  state = 'READY'
) =>
  fetch(`${API_PATH}/v4/challenges/${challengeType}`, {
    headers,
    method: 'POST',
    body: JSON.stringify({ state }),
  })
    .then(ApiUtils.checkResponse)
    .then(response => response.json());

// Get Feature flags
export const getClientConfig = () =>
  getRequest(`${API_PATH}/v4/clientConfigs/locationFrontend`);

// DEVICES
export const getOperatorDevices = () =>
  getRequest(`${API_PATH}/v4/operatorDevices`);

export const createDevice = role =>
  fetch(`${API_PATH}/v4/operatorDevices`, {
    headers,
    method: 'POST',
    body: JSON.stringify({ role }),
  })
    .then(ApiUtils.checkResponse)
    .then(response => response.json());

export const reactivateDevice = deviceId =>
  fetch(`${API_PATH}/v4/operatorDevices/${deviceId}/reactivate`, {
    headers,
    method: 'POST',
  })
    .then(ApiUtils.checkResponse)
    .then(response => response.json());

export const logoutDevice = deviceId =>
  fetch(`${API_PATH}/v4/operatorDevices/deactivate`, {
    headers,
    method: 'POST',
    body: JSON.stringify({ deviceId }),
  });

export const deleteDevice = deviceId =>
  fetch(`${API_PATH}/v4/operatorDevices`, {
    headers,
    method: 'DELETE',
    body: JSON.stringify({ deviceId }),
  });

// ISSUERS
export const getIssuer = issuerId =>
  getRequest(`${API_PATH}/v4/keys/issuers/${issuerId}`);

// JWT
export const getBackendJwt = () => getRequest(`${API_PATH}/v4/auth/jwt`);

// EMPLOYEES
export const createEmployee = data =>
  fetch(`${API_PATH}/v3/operators/employees`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  }).then(ApiUtils.checkResponse);

export const updateEmployee = ({ employeeId, data }) =>
  fetch(`${API_PATH}/v3/operators/employees/${employeeId}`, {
    method: 'PATCH',
    headers,
    body: JSON.stringify(data),
  });

export const deleteEmployee = employeeId =>
  fetch(`${API_PATH}/v3/operators/employees/${employeeId}`, {
    headers,
    method: 'DELETE',
  });

export const getEmployees = groupId =>
  getRequest(`${API_PATH}/v3/operators/locationGroups/${groupId}/employees`);

// CAMPAIGNS
export const getCampaignsForOperator = () =>
  getRequest(`${API_PATH}/v4/campaigns`);

export const createCampaign = data =>
  fetch(`${API_PATH}/v4/campaigns`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  });

export const requestLoginEmail = data =>
  fetch(`${API_PATH}/v4/auth/magicLink`, {
    method: 'POST',
    headers,
    body: JSON.stringify(data),
  });

export const loginWithMagicLink = token =>
  fetch(`${API_PATH}/v4/auth/magicLink/login`, {
    method: 'POST',
    headers,
    body: JSON.stringify({ token }),
  });
