import { KYB_STATUS, ONBOARDING_STATUS } from 'constants/paymentOnboarding';
import * as ApiUtils from './utils';
import { requestWithStatusHandlers } from './utils';
import { getJwt } from '../utils/jwtStorage';

const PAYMENT_API_PATH = '/pay/api';

const headers = jwt => ({
  'Content-Type': 'application/json',
  'X-Auth': jwt,
});

export const getOperatorPaymentEnabled = async operatorId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/operators/${operatorId}/paymentEnabled`,
    headers(await getJwt()),
    null,
    () => ({ paymentEnabled: false, payTermsAccepted: false })
  ).catch(() => ({ paymentEnabled: false, payTermsAccepted: false }));

export const getOnboardingStatus = async locationGroupId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/onboarding`,
    headers(await getJwt()),
    {
      200: response => response.json(),
      404: () => ({
        status: ONBOARDING_STATUS.GROUP_NOT_FOUND,
      }),
    }
  );

export const getLocationGroups = async () =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups`,
    headers(await getJwt()),
    null,
    () => null
  );

export const getLocationGroup = async locationGroupId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}`,
    headers(await getJwt()),
    null,
    () => null
  );

export const getPayoutDetails = async locationGroupId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/payoutDetails`,
    headers(await getJwt())
  );

export const createLocationGroup = async ({
  locationGroupId,
  phoneNumber,
  companyName,
  businessFirstName,
  businessLastName,
  street,
  streetNumber,
  zipCode,
  city,
  invoiceEmail,
  vatNumber,
}) =>
  fetch(`${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}`, {
    method: 'POST',
    headers: headers(await getJwt()),
    body: JSON.stringify({
      locationGroupId,
      phoneNumber,
      companyName,
      businessFirstName,
      businessLastName,
      street,
      streetNumber,
      zipCode,
      city,
      invoiceEmail,
      vatNumber,
    }),
  }).then(ApiUtils.checkResponse);

export const patchLocationGroup = async (payload, locationGroupId) =>
  fetch(`${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}`, {
    method: 'PATCH',
    headers: headers(await getJwt()),
    body: JSON.stringify(payload),
  });

export const startRapydKybProcess = async locationGroupId =>
  fetch(
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/onboarding/kyb`,
    {
      method: 'POST',
      headers: headers(await getJwt()),
    }
  ).then(ApiUtils.checkResponse);

export const startRapydKybProcessWithPhoneNumber = async (
  locationGroupId,
  phoneNumber
) =>
  fetch(
    `${PAYMENT_API_PATH}/v2/locationGroups/${locationGroupId}/onboarding/kyb`,
    {
      method: 'POST',
      body: JSON.stringify({ phoneNumber }),
      headers: headers(await getJwt()),
    }
  ).then(ApiUtils.checkResponse);

export const initiatePayoutOnboarding = async locationGroupId =>
  fetch(
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/onboarding/payout`,
    {
      method: 'POST',
      headers: headers(await getJwt()),
    }
  ).then(ApiUtils.checkResponse);

export const changePayoutOnboarding = async locationGroupId =>
  fetch(
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/onboarding/payout`,
    {
      method: 'PUT',
      headers: headers(await getJwt()),
    }
  ).then(ApiUtils.checkResponse);

export const getPaymentActive = async locationId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locations/${locationId}/paymentActive`,
    headers(await getJwt())
  );

export const patchLocation = async (locationId, paymentActive) =>
  fetch(`${PAYMENT_API_PATH}/v1/locations/${locationId}`, {
    method: 'PATCH',
    headers: headers(await getJwt()),
    body: JSON.stringify({ paymentActive }),
  }).then(ApiUtils.checkResponse);

export const getLocationGroupPayments = async (locationGroupId, cursor) =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/payments?cursor=${cursor}`,
    headers(await getJwt()),
    null,
    () => ({ cursor: null, payments: [] })
  );

export const getLocationGroupTipTopUpAmount = async locationGroupId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/tipTopUpAmount`,
    headers(await getJwt())
  );

export const getLocationPayments = async (locationId, cursor) =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locations/${locationId}/payments?cursor=${cursor}`,
    headers(await getJwt()),
    null,
    () => ({ cursor: null, payments: [] })
  );

export const getPayouts = async (locationGroupId, cursor) =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/payouts?cursor=${cursor}`,
    headers(await getJwt()),
    null,
    () => ({ cursor: null, payouts: [] })
  );

export const getBalance = async locationGroupId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/balance`,
    headers(await getJwt()),
    null,
    () => {}
  );

export const triggerPayout = async locationGroupId =>
  requestWithStatusHandlers(
    'POST',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/payouts`,
    headers(await getJwt()),
    {
      200: () => true,
      201: () => true,
    },
    () => false
  );

export const triggerRefund = async (locationGroupId, paymentId) =>
  requestWithStatusHandlers(
    'POST',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/payments/${paymentId}/refund`,
    headers(await getJwt()),
    {
      200: () => true,
      201: () => true,
      401: () => false,
      403: () => false,
      404: () => false,
      502: () => false,
    },
    () => false
  );

export const acceptPaymentTerms = async () =>
  requestWithStatusHandlers(
    'POST',
    `${PAYMENT_API_PATH}/v1/operators/acceptPaymentTerms`,
    headers(await getJwt()),
    {
      204: () => true,
    },
    () => false
  );

export const getPaymentOperator = async operatorId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/operators/${operatorId}`,
    headers(await getJwt()),
    {
      403: () => null,
      404: () => null,
    },
    () => null
  );

export const createPaymentOperator = async () =>
  requestWithStatusHandlers(
    'POST',
    `${PAYMENT_API_PATH}/v1/operators`,
    headers(await getJwt()),
    null,
    () => false
  );

export const getKybOnboardingStatus = async locationGroupId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}v2/locationGroups/${locationGroupId}/onboarding/kyb`,
    headers(await getJwt()),
    {
      401: () => ({ status: KYB_STATUS.NOT_STARTED }),
      403: () => ({ status: KYB_STATUS.NOT_STARTED }),
      404: () => ({ status: KYB_STATUS.NOT_STARTED }),
    },
    () => ({ status: KYB_STATUS.NOT_STARTED })
  );

export const startKybOnboarding = async locationGroupId =>
  requestWithStatusHandlers(
    'POST',
    `${PAYMENT_API_PATH}v2/locationGroups/${locationGroupId}/onboarding/kyb`,
    headers(await getJwt()),
    null,
    () => ({ status: KYB_STATUS.NOT_STARTED })
  );

// GET ALL LUCA CAMPAIGNS
export const getLucaCampaigns = async () =>
  fetch(`${PAYMENT_API_PATH}/v1/paymentCampaigns`, {
    method: 'GET',
    headers: headers(await getJwt()),
  }).then(response => response.json());

// CAMPAIGN PARTICIPATION
export const participateCampaigns = async (locationGroupId, data) =>
  fetch(`${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/campaigns`, {
    method: 'POST',
    headers: headers(await getJwt()),
    body: JSON.stringify(data),
  }).then(response => response.json());

export const getParticipatedCampaigns = async locationGroupId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/campaigns`,
    headers(await getJwt()),
    null,
    () => false
  );

export const deleteParticipatedCampaign = async (
  locationGroupId,
  campaignParticipationId
) =>
  fetch(
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/campaigns/${campaignParticipationId}`,
    {
      method: 'DELETE',
      headers: headers(await getJwt()),
    }
  );

export const patchOperator = async (operatorId, body) =>
  fetch(`${PAYMENT_API_PATH}/v1/operators/${operatorId}`, {
    method: 'PATCH',
    headers: headers(await getJwt()),
    body: JSON.stringify({
      hasSeenPaymentInfoModal: !!body?.hasSeenPaymentInfoModal,
    }),
  }).then(ApiUtils.checkResponse);

export const getDisplayPaymentModal = async operatorId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/operators/${operatorId}/displayPaymentModal`,
    headers(await getJwt()),
    {
      200: async response => {
        const json = await response.json();
        return json.displayPaymentModal ?? false;
      },
    },
    () => false
  );
