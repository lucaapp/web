import React, { useCallback, useEffect } from 'react';
import { Redirect, Route, Switch } from 'react-router';
import { useIntercom } from 'react-use-intercom';

import {
  ACTIVATE_EMAIL_ROUTE,
  ACTIVATION_ROUTE,
  AUTHENTICATION_CONFIRMATION_ROUTE,
  APP_ROUTE,
  FORGOT_PASSWORD_ROUTE,
  LICENSES_ROUTE,
  LOGIN_ROUTE,
  REGISTER_ROUTE,
  RESET_PASSWORD_ROUTE,
  SHARE_ALL_DATA_ROUTE,
  SHARE_DATA_ROUTE,
  LANDING_ROUTE,
  MAGIC_LOGIN_ROUTE,
} from 'constants/routes';

import { App } from 'components/App';
import { Login } from 'components/Authentication/Login';
import { StartCampaign } from 'components/Authentication/StartCampaign';
import { Confirmation } from 'components/Authentication/Confirmation';
import { Register } from 'components/Authentication/Register';
import { ForgotPassword } from 'components/ForgotPassword';
import { ResetPassword } from 'components/ResetPassword';
import { Activation } from 'components/Activation';
import { MagicLogin } from 'components/Authentication/MagicLogin';
import { ActivateEmail } from 'components/ActivateEmail';
import { ShareData } from 'components/ShareData';
import { Licenses } from 'components/Licenses';

import { UserRoute } from './components/routes';

export const MainRouting = () => {
  const { boot, trackEvent } = useIntercom();
  const setupIntercom = useCallback(() => boot(), [boot]);

  useEffect(() => {
    setupIntercom();
    trackEvent('INIT');
  }, [setupIntercom, trackEvent]);

  return (
    <Switch>
      <Route path={LANDING_ROUTE} component={StartCampaign} exact />
      <UserRoute path={LOGIN_ROUTE} component={Login} exact />
      <UserRoute
        path={AUTHENTICATION_CONFIRMATION_ROUTE}
        component={Confirmation}
        exact
      />
      <UserRoute path={REGISTER_ROUTE} component={() => <Register />} />
      <Route path={LICENSES_ROUTE} component={Licenses} />
      <UserRoute path={FORGOT_PASSWORD_ROUTE} component={ForgotPassword} />
      <UserRoute path={RESET_PASSWORD_ROUTE} component={ResetPassword} />
      <Route path={APP_ROUTE} component={() => <App />} />
      <Route path={SHARE_ALL_DATA_ROUTE} component={ShareData} />
      <Route path={SHARE_DATA_ROUTE} component={ShareData} />
      <Route path={MAGIC_LOGIN_ROUTE} component={MagicLogin} />
      <Route path={ACTIVATION_ROUTE} component={Activation} />
      <Route path={ACTIVATE_EMAIL_ROUTE} component={ActivateEmail} />
      <Redirect to={LOGIN_ROUTE} />
    </Switch>
  );
};
