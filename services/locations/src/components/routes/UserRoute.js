/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Route } from 'react-router';

import { useUserIsLoggedIn } from 'components/hooks';

export const UserRoute = ({ component: Component, render, ...rest }) => {
  const isLoggedIn = useUserIsLoggedIn();

  if (isLoggedIn) return null;

  return (
    <Route
      {...rest}
      render={properties => {
        return Component ? <Component {...properties} /> : render(properties);
      }}
    />
  );
};
