import React from 'react';

// Components
import { LucaLogoWhiteIconSVG } from 'assets/icons';
import {
  SubTitle,
  HeaderWrapper,
  Logo,
  ActionsRow,
  LogoWrapper,
} from './Header.styled';

// Assets

const HeaderRaw = ({ title, actions }) => {
  return (
    <HeaderWrapper>
      <LogoWrapper>
        <Logo src={LucaLogoWhiteIconSVG} />
        <SubTitle>{title}</SubTitle>
      </LogoWrapper>
      {actions && <ActionsRow>{actions}</ActionsRow>}
    </HeaderWrapper>
  );
};

export const Header = React.memo(HeaderRaw);
