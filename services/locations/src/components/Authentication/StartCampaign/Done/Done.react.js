import React from 'react';
import { useIntl } from 'react-intl';
import {
  CardSubTitle,
  CardTitle,
  IconWrapper,
  StyledCheckIcon,
} from './Done.styled';

export const Done = () => {
  const intl = useIntl();

  return (
    <>
      <CardTitle>
        {intl.formatMessage({
          id: 'landing.done.title',
        })}
      </CardTitle>
      <CardSubTitle>
        {intl.formatMessage({
          id: 'landing.done.subtitle',
        })}
      </CardSubTitle>
      <IconWrapper>
        <StyledCheckIcon />
      </IconWrapper>
    </>
  );
};
