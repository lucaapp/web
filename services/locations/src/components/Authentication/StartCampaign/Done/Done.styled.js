import styled from 'styled-components';
import { Media } from 'utils/media';
import { CheckIcon } from 'assets/icons';

export const StyledCheckIcon = styled(CheckIcon)`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 76px auto;
  width: 100px;
  height: 100px;
`;

export const IconWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

export const CardTitle = styled.div`
  font-family: Montserrat-Bold, sans-serif;
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 24px;
  text-align: center;
  color: rgb(0, 0, 0);

  ${Media.mobile`
    font-size: 28px;
    margin-bottom: 16px;
  `}
`;
export const CardSubTitle = styled.div`
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 24px;
  text-align: center;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
`;
