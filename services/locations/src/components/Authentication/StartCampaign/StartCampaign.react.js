import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { Helmet } from 'react-helmet-async';
import { useIntercom } from 'react-use-intercom';

import { IS_MOBILE } from 'constants/environment';

import { CampaignForm } from './CampaignForm';
import { Done } from './Done';

import { Background } from '../Background';
import { Footer } from '../Footer';
import {
  AuthenticationWrapper,
  AuthenticationCard,
  Wrapper,
} from '../Authentication.styled';

export const StartCampaign = () => {
  const intl = useIntl();
  const { trackEvent } = useIntercom();
  const [done, setDone] = useState(false);

  useEffect(() => {
    trackEvent('START_CAMPAIGN');
  }, [trackEvent]);
  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'locations.site.title' })}</title>
        <meta
          name="description"
          content={intl.formatMessage({ id: 'locations.site.meta' })}
        />
      </Helmet>
      <Wrapper id={IS_MOBILE ? 'isMobile' : ''}>
        <Background isRegistration />
        <AuthenticationWrapper id="noSteps">
          <AuthenticationCard>
            {done ? <Done /> : <CampaignForm done={() => setDone(true)} />}
          </AuthenticationCard>
          <Footer />
        </AuthenticationWrapper>
      </Wrapper>
    </>
  );
};
