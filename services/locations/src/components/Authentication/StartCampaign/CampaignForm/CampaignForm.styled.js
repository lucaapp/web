import styled, { css } from 'styled-components';
import { Media } from 'utils/media';
import { Form, Input } from 'antd';

const sharedInputStyle = css`
  border: 1px solid #696969 !important;
  background-color: transparent !important;

  &:hover,
  &:active,
  &:focus {
    border: 1px solid #696969 !important;
    background-color: transparent !important;
  }

  ${Media.mobile`
    border-radius: 0px !important;
    border: 0.5px solid rgb(0, 0, 0) !important;
    line-height: 48px !important;
    height: 48px !important;
  `}
`;

export const StyledFormItem = styled(Form.Item)`
  margin-bottom: 24px;
  & label {
    font-weight: 500;
  }
  ${Media.mobile`
     margin-bottom: 64px;
  `}
`;

export const StyledInput = styled(Input)`
  ${sharedInputStyle}
`;

export const PoweredByRapid = styled.div`
  color: rgb(0, 0, 0);
  margin-bottom: 8px;
  font-family: Montserrat-MediumItalic, sans-serif;
  font-size: 12px;
  font-style: italic;
  font-weight: 500;
  width: 100%;
  text-align: right;
`;
