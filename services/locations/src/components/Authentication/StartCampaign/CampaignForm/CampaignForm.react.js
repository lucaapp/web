import React from 'react';
import { useIntl } from 'react-intl';
import { useLocation } from 'react-router';
import { Form, notification } from 'antd';

import { requestLoginEmail } from 'network/api';
import { useCampaign } from 'utils/campaign';

import { TERMS_CONDITIONS_LINK } from 'constants/links';
import { useEmailValidator, useNotifications } from 'components/hooks';

import { ConsentCheckbox } from 'components/general/ConsentCheckbox';
import {
  ButtonWrapper,
  CardTitle,
  CardSubTitle,
  StyledWhiteButton,
} from '../../Authentication.styled';
import {
  PoweredByRapid,
  StyledFormItem,
  StyledInput,
} from './CampaignForm.styled';

export const CampaignForm = ({ done }) => {
  const intl = useIntl();
  const queryParameters = useLocation().search;
  const campaign = useCampaign(queryParameters);
  const { errorMessage } = useNotifications();

  const emailValidator = useEmailValidator();

  const onFinish = ({ email }) => {
    requestLoginEmail({
      email,
      campaign,
    })
      .then(response => {
        if (response.status !== 204) {
          errorMessage('campaignForm.notification.error');

          return;
        }

        notification.success({
          message: intl.formatMessage({
            id: 'campaignForm.notification.success',
          }),
        });
        done();
      })
      .catch(() => {
        errorMessage('campaignForm.notification.error');
      });
  };

  return (
    <>
      <PoweredByRapid>
        {intl.formatMessage({ id: 'payment.poweredBy' })}
      </PoweredByRapid>
      <CardTitle>
        {intl.formatMessage({
          id: 'campaignForm.title',
        })}
      </CardTitle>
      <CardSubTitle>
        {intl.formatMessage({ id: 'campaignForm.subtitle' })}
      </CardSubTitle>
      <Form onFinish={onFinish}>
        <StyledFormItem
          colon={false}
          label={intl.formatMessage({
            id: 'registration.form.email',
          })}
          name="email"
          rules={emailValidator}
        >
          <StyledInput autoFocus autoComplete="username" />
        </StyledFormItem>
        <ConsentCheckbox
          name="termsAndConditions"
          errorMessage="error.termsAndConditions"
          dataCy="termsAndConditionsCheckbox"
          label="authentication.registration.acceptTerms"
          link={TERMS_CONDITIONS_LINK}
        />
        <ButtonWrapper>
          <StyledWhiteButton htmlType="submit">
            {intl.formatMessage({
              id: 'campaignForm.button.title',
            })}
          </StyledWhiteButton>
        </ButtonWrapper>
      </Form>
    </>
  );
};
