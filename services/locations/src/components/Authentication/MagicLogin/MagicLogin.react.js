import React, { useEffect, useCallback, useState } from 'react';
import { useIntl } from 'react-intl';
import { useLocation } from 'react-router';
import { Helmet } from 'react-helmet-async';
import { useIntercom } from 'react-use-intercom';
import queryString from 'query-string';

import { IS_MOBILE } from 'constants/environment';
import { BASE_GROUP_ROUTE, LOGIN_ROUTE } from 'constants/routes';
import { loginWithMagicLink } from 'network/api';

import { clearHasSeenPrivateKeyModal } from 'utils/storage';
import { clearJwt } from 'utils/jwtStorage';
import { usePrivateKey } from 'utils/privateKey';

import {
  AuthenticationCard,
  AuthenticationWrapper,
  ButtonWrapper,
  CardTitle,
  StyledWhiteButton,
  Wrapper,
} from 'components/Authentication/Authentication.styled';

import { useNotifications } from 'components/hooks';
import { Background } from '../Background';
import { Footer } from '../Footer';

export const MagicLogin = () => {
  const intl = useIntl();
  const location = useLocation();
  const [error, setError] = useState(false);
  const [, clearPrivateKey] = usePrivateKey(null);
  const { errorMessage } = useNotifications();

  const queryParameters = location.search;
  const token = location.hash.slice(1);

  const { trackEvent } = useIntercom();

  const onRedirect = () => {
    window.location = `${LOGIN_ROUTE}${queryParameters}`;
  };

  useEffect(() => {
    trackEvent('MAGIC_LOGIN_LINK_OPENED');
  }, [trackEvent]);

  const handleError = useCallback(() => {
    setError(true);

    errorMessage('magicLogin.notification.error');
  }, [errorMessage]);

  useEffect(() => {
    loginWithMagicLink(token)
      .then(response => {
        if (response.status !== 204) {
          handleError();
          return;
        }
        setError(null);
        clearJwt();
        clearPrivateKey(null);
        clearHasSeenPrivateKeyModal();

        const { redirectTo } = queryString.parse(queryParameters);
        const newLocation = redirectTo ?? BASE_GROUP_ROUTE;

        window.location = `${newLocation}${queryParameters}`;
      })
      .catch(() => handleError());
  }, [token, queryParameters, handleError, clearPrivateKey]);

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'locations.site.title' })}</title>
        <meta
          name="description"
          content={intl.formatMessage({ id: 'locations.site.meta' })}
        />
      </Helmet>
      <Wrapper id={IS_MOBILE ? 'isMobile' : ''}>
        <Background isRegistration />
        <AuthenticationWrapper id="noSteps">
          <AuthenticationCard>
            <CardTitle>
              {intl.formatMessage({
                id: error ? 'magicLogin.error' : 'magicLogin.loading',
              })}
            </CardTitle>
            {error && (
              <ButtonWrapper>
                <StyledWhiteButton onClick={onRedirect}>
                  {intl.formatMessage({
                    id: 'authentication.returnToLogin',
                  })}
                </StyledWhiteButton>
              </ButtonWrapper>
            )}
          </AuthenticationCard>
          <Footer />
        </AuthenticationWrapper>
      </Wrapper>
    </>
  );
};
