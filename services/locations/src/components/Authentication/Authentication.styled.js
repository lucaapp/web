import styled, { css } from 'styled-components';
import { Media } from 'utils/media';
import { Form, Input, InputNumber, Select } from 'antd';
import { WhiteButton, SecondaryButton } from 'components/general';
import { CheckIcon } from 'assets/icons';

export const inputStyle = {
  border: '1px solid #696969',
  backgroundColor: 'transparent',
};
export const BackgroundWrapper = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
`;

export const Wrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`;

export const AuthenticationWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100%;

  @media (max-height: 600px) and (orientation: landscape) {
    align-items: stretch;
    justify-content: flex-end;
  }

  ${Media.mobile`
    flex-direction: column;
  `}
`;

export const AuthenticationCard = styled.div`
  margin: 20px 0;
  max-width: 50%;
  max-height: 100%;

  padding: 40px;
  background: rgb(195, 206, 217);
  border-radius: 2px;
  width: 624px;
  z-index: 10;

  ${Media.mobile`
    width: 100%;
    max-width: none;
    
    padding: 42px 40px;
    margin: 16px 0 0;
    margin-top: 138px;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
    flex-grow: 1;
  `}
`;

export const Headline = styled.h1`
  font-size: 42px;
  margin-bottom: 32px;
`;

export const HeaderWrapper = styled.div`
  display: flex;
`;

export const Description = styled.div`
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 24px;
  color: rgb(0, 0, 0);

  ${Media.mobile`
    font-family: 'Montserrat-Medium', sans-serif;
    font-style: italic;
  `}
`;

export const AuthenticationButton = styled.button.attrs({
  type: 'submit',
})`
  min-width: 200px;
  height: 48px;

  background: rgb(255, 255, 255);

  border: 1px solid transparent;
  border-radius: 24px;

  cursor: pointer;

  &:active,
  &:focus {
    outline: none;
  }

  &:active {
    border: 1px solid #999;
  }

  transition: border 200ms ease;

  ${Media.mobile`
    width: 100%;
  `}
`;

export const CardTitle = styled.div`
  font-family: Montserrat-Bold, sans-serif;
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 24px;
  color: rgb(0, 0, 0);

  ${Media.mobile`
    font-size: 28px;
    margin-bottom: 16px;
  `}
`;

export const CardSubTitle = styled.div`
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 24px;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
`;

export const Step = styled.div`
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 8px;

  ${Media.mobile`
    margin-bottom: 6px;
  `}
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: ${({ multipleButtons }) =>
    multipleButtons ? 'space-between' : 'flex-end'};
  margin-top: 24px;

  @media (max-width: 1096px) {
    margin-top: 40px;
    flex-flow: column-reverse;

    button:not(:last-child) {
      margin-top: 24px;
    }
  }
`;

export const StyledPasswordButtonWrapper = styled(ButtonWrapper)`
  ${Media.mobile`
    margin-top: 52px;
  `}
`;

export const StyledFormItem = styled(Form.Item)`
  width: ${({ width }) => (width ? `${width}` : '100%')};
  padding-right: ${({ $paddingRight }) =>
    $paddingRight ? `${$paddingRight}` : '0'};

  ${Media.mobile`
    width: 100%;
    padding-right: 0;
  `}
`;

const sharedInputStyle = css`
  border: 1px solid #696969 !important;
  background-color: transparent !important;

  &:hover,
  &:active,
  &:focus {
    border: 1px solid #696969 !important;
    background-color: transparent !important;
  }

  ${Media.mobile`
    border-radius: 0px !important;
    border: 0.5px solid rgb(0, 0, 0) !important;
    line-height: 48px !important;
    height: 48px !important;
  `}
`;
export const StyledInput = styled(Input)`
  ${sharedInputStyle}
`;

export const StyledInputNumber = styled(InputNumber)`
  width: 100% !important;
  ${sharedInputStyle}
`;
export const StyledPasswordInput = styled(Input.Password)`
  ${sharedInputStyle}
`;

export const StyledSelect = styled(Select)`
  & .ant-select-selector {
    ${sharedInputStyle}
    span {
      ${Media.mobile`
        height: 48px !important;
        line-height: 48px !important;
    `}
    }
  }
`;

export const StyledSelectOption = styled(Select.Option)``;

export const RowWrapper = styled.div`
  display: flex;
  justify-content: space-between;

  ${Media.mobile`
    display: block;
  `}
`;

export const StyledWhiteButton = styled(WhiteButton)`
  min-width: unset;
  ${Media.mobile`
    height: 48px;
  `}
`;

export const StyledSecondaryButton = styled(SecondaryButton)`
  min-width: unset;
  ${Media.mobile`
    height: 48px;
  `}
`;

export const StyledCheckIcon = styled(CheckIcon)`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 76px auto 90px auto;
  width: 100px;
  height: 100px;
`;

export const ConfirmationWrapper = styled.div`
  margin-top: 30px;
`;

export const IconWrapper = styled.div`
  display: flex;
  justify-content: center;
`;
