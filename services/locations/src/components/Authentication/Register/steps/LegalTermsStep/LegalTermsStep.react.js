import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import {
  CardTitle,
  CardSubTitle,
  ButtonWrapper,
  Step,
  Description,
  StyledSecondaryButton,
  StyledWhiteButton,
} from 'components/Authentication/Authentication.styled';
import { TERMS_CONDITIONS_LINK } from 'constants/links';

import AVV from 'assets/documents/AVV_Luca.pdf';
import { ConsentCheckbox } from 'components/general/ConsentCheckbox';

export const LegalTermsStep = ({ next, back, navigation }) => {
  const intl = useIntl();

  const onFinish = () => {
    next();
  };

  return (
    <>
      <Step>{navigation}</Step>
      <CardTitle data-cy="legalTerms">
        {intl.formatMessage({
          id: 'authentication.legalTerms.title',
        })}
      </CardTitle>
      <CardSubTitle>
        {intl.formatMessage(
          {
            id: 'authentication.legalTerms.subTitle',
          },
          { br: <br /> }
        )}
      </CardSubTitle>

      <Form onFinish={onFinish}>
        <ConsentCheckbox
          name="avv"
          errorMessage="error.avv"
          dataCy="avvCheckbox"
          label="authentication.registration.acceptAvv"
          link={AVV}
          download
        />
        <ConsentCheckbox
          name="termsAndConditions"
          errorMessage="error.termsAndConditions"
          dataCy="termsAndConditionsCheckbox"
          label="authentication.registration.acceptTerms"
          link={TERMS_CONDITIONS_LINK}
        />

        <Description>
          {intl.formatMessage({ id: 'authentication.registration.legalHint' })}
        </Description>
        <ButtonWrapper multipleButtons>
          <StyledSecondaryButton onClick={back} data-cy="setTermsBackButton">
            {intl.formatMessage({
              id: 'authentication.form.button.back',
            })}
          </StyledSecondaryButton>
          <StyledWhiteButton htmlType="submit" data-cy="legalTermsSubmitButton">
            {intl.formatMessage({
              id: 'authentication.form.button.next',
            })}
          </StyledWhiteButton>
        </ButtonWrapper>
      </Form>
    </>
  );
};
