import React from 'react';
import { useIntl } from 'react-intl';
import {
  useCityValidator,
  useHouseNoValidator,
  useStreetValidator,
  useZipCodeValidator,
} from 'components/hooks/useValidators';
import {
  RowWrapper,
  StyledFormItem,
  StyledInput,
} from 'components/Authentication/Authentication.styled';

export const BusinessAddress = () => {
  const intl = useIntl();
  const streetValidator = useStreetValidator('streetName');
  const houseNoValidator = useHouseNoValidator('streetNr');
  const zipCodeValidator = useZipCodeValidator('zipCode');
  const cityValidator = useCityValidator('city');

  return (
    <>
      <RowWrapper>
        <StyledFormItem
          data-cy="businessEntityStreetName"
          colon={false}
          name="businessEntityStreetName"
          label={intl.formatMessage({
            id: 'card.form.streetName',
          })}
          width="70%"
          $paddingRight="16px"
          rules={streetValidator}
        >
          <StyledInput
            autoComplete="street-address"
            data-cy="businessStreetInputField"
          />
        </StyledFormItem>
        <StyledFormItem
          data-cy="businessEntityStreetNumber"
          colon={false}
          name="businessEntityStreetNumber"
          label={intl.formatMessage({
            id: 'card.form.streetNr',
          })}
          width="30%"
          rules={houseNoValidator}
        >
          <StyledInput data-cy="businessStreetNumInputField" />
        </StyledFormItem>
      </RowWrapper>
      <RowWrapper>
        <StyledFormItem
          data-cy="businessEntityZipCode"
          colon={false}
          name="businessEntityZipCode"
          label={intl.formatMessage({
            id: 'card.form.zipCode',
          })}
          width="30%"
          $paddingRight="16px"
          rules={zipCodeValidator}
        >
          <StyledInput
            autoComplete="postal-code"
            data-cy="businessZipInputField"
          />
        </StyledFormItem>
        <StyledFormItem
          data-cy="businessEntityCity"
          colon={false}
          name="businessEntityCity"
          label={intl.formatMessage({
            id: 'card.form.city',
          })}
          width="70%"
          rules={cityValidator}
        >
          <StyledInput
            autoComplete="home city"
            data-cy="businessCityInputField"
          />
        </StyledFormItem>
      </RowWrapper>
    </>
  );
};
