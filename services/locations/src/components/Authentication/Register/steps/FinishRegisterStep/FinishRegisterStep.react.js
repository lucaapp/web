import React from 'react';
import { useIntl } from 'react-intl';

import { IS_MOBILE } from 'constants/environment';

import {
  Step,
  CardTitle,
  CardSubTitle,
  ButtonWrapper,
  StyledWhiteButton,
} from 'components/Authentication/Authentication.styled';

export const FinishRegisterStep = ({ next, navigation }) => {
  const intl = useIntl();

  return (
    <>
      <Step>{navigation}</Step>
      <CardTitle data-cy="finishRegister">
        {intl.formatMessage({
          id: 'authentication.finishRegister.title',
        })}
      </CardTitle>
      <CardSubTitle>
        {intl.formatMessage({
          id: 'authentication.finishRegister.subTitle',
        })}
      </CardSubTitle>

      {!IS_MOBILE && (
        <ButtonWrapper>
          <StyledWhiteButton onClick={next} data-cy="endRegistrationButton">
            {intl.formatMessage({
              id: 'authentication.button.ok',
            })}
          </StyledWhiteButton>
        </ButtonWrapper>
      )}
    </>
  );
};
