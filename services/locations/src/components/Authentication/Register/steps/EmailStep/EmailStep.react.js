import React from 'react';
import { useIntl } from 'react-intl';
import { useHistory, useLocation } from 'react-router';
import { Form } from 'antd';

import { LOGIN_ROUTE } from 'constants/routes';

import { useEmailValidator } from 'components/hooks/useValidators';
import {
  ButtonWrapper,
  CardTitle,
  Step,
  StyledInput,
  StyledSecondaryButton,
  StyledWhiteButton,
} from 'components/Authentication/Authentication.styled';

export const EmailStep = ({
  email: currentEmail,
  setEmail,
  next,
  navigation,
}) => {
  const intl = useIntl();
  const emailValidator = useEmailValidator();

  const history = useHistory();
  const queryParameters = useLocation().search;

  const onRedirect = () => history.push(`${LOGIN_ROUTE}${queryParameters}`);

  const handleSubmit = values => {
    const { email } = values;
    setEmail(email);
    next();
  };

  const initialValues = { email: currentEmail ?? '' };

  return (
    <>
      <Step>{navigation}</Step>
      <CardTitle data-cy="loginPage">
        {intl.formatMessage({
          id: 'authentication.register.title',
        })}
      </CardTitle>
      <Form onFinish={handleSubmit} initialValues={initialValues}>
        <Form.Item
          data-cy="createAccount-emailInputField"
          colon={false}
          label={intl.formatMessage({
            id: 'registration.form.email',
          })}
          name="email"
          rules={emailValidator}
        >
          <StyledInput autoFocus autoComplete="email" type="email" />
        </Form.Item>
        <ButtonWrapper multipleButtons>
          <StyledSecondaryButton onClick={onRedirect}>
            {intl.formatMessage({
              id: 'authentication.returnToLogin',
            })}
          </StyledSecondaryButton>
          <StyledWhiteButton htmlType="submit" data-cy="createNewAccountButton">
            {intl.formatMessage({
              id: 'authentication.form.button.next',
            })}
          </StyledWhiteButton>
        </ButtonWrapper>
      </Form>
    </>
  );
};
