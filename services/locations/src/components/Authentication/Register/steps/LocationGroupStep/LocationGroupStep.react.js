import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import { ArrowDownBlueIcon } from 'assets/icons';
import {
  groupEnumTypes,
  groupOptions,
} from 'components/general/locationGroupOptions';
import {
  useNameValidator,
  useTableNumberValidator,
} from 'components/hooks/useValidators';
import { MIN_TABLE_NUMBER, MAX_TABLE_NUMBER } from 'constants/tableNumber';

import {
  CardTitle,
  CardSubTitle,
  ButtonWrapper,
  Step,
  StyledSecondaryButton,
  StyledWhiteButton,
  StyledInput,
  StyledInputNumber,
  StyledSelect,
  StyledSelectOption,
} from 'components/Authentication/Authentication.styled';
import { inputOnlyNumbersFilter } from 'utils/inputFilter';

export const LocationGroupStep = ({
  setInitialLocationGroup,
  back,
  next,
  navigation,
}) => {
  const intl = useIntl();

  const groupNameValidator = useNameValidator('groupName');
  const tableNumberValidator = useTableNumberValidator();

  const onFinish = locationGroupInfo => {
    setInitialLocationGroup(locationGroupInfo);
    next();
  };

  return (
    <>
      <Step>{navigation}</Step>
      <CardTitle>
        {intl.formatMessage({
          id: 'authentication.locationGroup.title',
        })}
      </CardTitle>
      <CardSubTitle>
        {intl.formatMessage({
          id: 'authentication.locationGroup.description',
        })}
      </CardSubTitle>
      <Form onFinish={onFinish}>
        <Form.Item
          data-cy="locationGroupType"
          name="type"
          initialValue={groupEnumTypes.RESTAURANT_TYPE}
          colon={false}
          label={intl.formatMessage({
            id: 'authentication.locationGroup.type',
          })}
        >
          <StyledSelect suffixIcon={<ArrowDownBlueIcon />}>
            {groupOptions.map(option => {
              return (
                <StyledSelectOption
                  value={option.type}
                  key={option.type}
                  data-cy={`locationGroupType-${option.type}`}
                >
                  {intl.formatMessage({
                    id: option.intlId,
                  })}
                </StyledSelectOption>
              );
            })}
          </StyledSelect>
        </Form.Item>
        <Form.Item
          data-cy="locationGroupName"
          name="name"
          colon={false}
          label={intl.formatMessage({
            id: 'authentication.locationGroup.name',
          })}
          rules={groupNameValidator}
        >
          <StyledInput />
        </Form.Item>
        <Form.Item
          name="tableCount"
          colon={false}
          label={intl.formatMessage({
            id: 'authentication.locationGroup.tableCount',
          })}
          rules={tableNumberValidator}
        >
          <StyledInputNumber
            onKeyDown={inputOnlyNumbersFilter}
            type="number"
            data-cy="locationGroupTableCount"
            min={MIN_TABLE_NUMBER}
            max={MAX_TABLE_NUMBER}
          />
        </Form.Item>
        <ButtonWrapper multipleButtons>
          <StyledSecondaryButton onClick={back}>
            {intl.formatMessage({
              id: 'authentication.form.button.back',
            })}
          </StyledSecondaryButton>
          <StyledWhiteButton
            htmlType="submit"
            data-cy="confirmLocationGroupButton"
          >
            {intl.formatMessage({
              id: 'authentication.form.button.next',
            })}
          </StyledWhiteButton>
        </ButtonWrapper>
      </Form>
    </>
  );
};
