import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import {
  useMobilePhoneValidator,
  usePersonNameValidator,
} from 'components/hooks/useValidators';

import {
  CardTitle,
  CardSubTitle,
  ButtonWrapper,
  Step,
  StyledInput,
  StyledSecondaryButton,
  StyledWhiteButton,
} from 'components/Authentication/Authentication.styled';
import { getFormattedPhoneNumber } from 'utils/formatters';

export const ContactInputStep = ({
  contacts,
  setContacts,
  next,
  back,
  navigation,
}) => {
  const intl = useIntl();
  const firstNameValidator = usePersonNameValidator('firstName');
  const lastNameValidator = usePersonNameValidator('lastName');
  const phoneValidator = useMobilePhoneValidator();

  const onFinish = ({ firstName, lastName, phone }) => {
    const formattedPhone = getFormattedPhoneNumber(phone);

    setContacts({ firstName, lastName, phone: formattedPhone });
    next();
  };

  return (
    <>
      <Step>{navigation}</Step>
      <CardTitle>
        {intl.formatMessage({
          id: 'authentication.contactInput.title',
        })}
      </CardTitle>
      <CardSubTitle>
        {intl.formatMessage({
          id: 'authentication.contactInput.subTitle',
        })}
      </CardSubTitle>
      <Form
        onFinish={onFinish}
        initialValues={
          contacts
            ? {
                firstName: contacts.firstName,
                lastName: contacts.lastName,
                phone: contacts.phone,
              }
            : {}
        }
      >
        <Form.Item
          data-cy="registerFirstName"
          colon={false}
          name="firstName"
          label={intl.formatMessage({
            id: 'generic.firstName',
          })}
          rules={firstNameValidator}
        >
          <StyledInput autoFocus autoComplete="given-name" />
        </Form.Item>
        <Form.Item
          data-cy="registerLastName"
          colon={false}
          name="lastName"
          label={intl.formatMessage({
            id: 'generic.lastName',
          })}
          rules={lastNameValidator}
        >
          <StyledInput autoComplete="family-name" />
        </Form.Item>
        <Form.Item
          colon={false}
          label={intl.formatMessage({
            id: 'settings.location.phone',
          })}
          name="phone"
          rules={phoneValidator}
        >
          <StyledInput type="tel" autoComplete="tel" />
        </Form.Item>
        <ButtonWrapper multipleButtons>
          <StyledSecondaryButton onClick={back}>
            {intl.formatMessage({
              id: 'authentication.form.button.back',
            })}
          </StyledSecondaryButton>
          <StyledWhiteButton htmlType="submit" data-cy="confirmContactsButton">
            {intl.formatMessage({
              id: 'authentication.form.button.next',
            })}
          </StyledWhiteButton>
        </ButtonWrapper>
      </Form>
    </>
  );
};
