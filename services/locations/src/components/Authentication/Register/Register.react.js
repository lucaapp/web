import React, { useCallback, useEffect, useState } from 'react';

import { useIntl } from 'react-intl';
import { useHistory } from 'react-router';
import { Steps } from 'antd';
import { useLocation } from 'react-router-dom';
import { useIntercom } from 'react-use-intercom';

import { registerOperator } from 'network/api';

import { useCampaign } from 'utils/campaign';
import { LOGIN_ROUTE } from 'constants/routes';
import { IS_MOBILE } from 'constants/environment';

import { SiteHeader } from 'components/general';
import { useNotifications } from 'components/hooks';
import { Footer } from '../Footer';
import { Background } from '../Background';

import { EmailStep } from './steps/EmailStep';
import { LegalTermsStep } from './steps/LegalTermsStep';
import { SetPasswordStep } from './steps/SetPasswordStep';
import { ContactInputStep } from './steps/ContactInputStep';
import { BusinessInfoStep } from './steps/BusinessInfoStep';
import { LocationGroupStep } from './steps/LocationGroupStep';
import { FinishRegisterStep } from './steps/FinishRegisterStep';

import {
  Wrapper,
  AuthenticationCard,
  AuthenticationWrapper,
} from '../Authentication.styled';

const STEP_LENGTH = 7;

export const Register = () => {
  const intl = useIntl();
  const history = useHistory();
  const queryParameters = useLocation().search;
  const { boot, trackEvent, update } = useIntercom();
  const setupIntercom = useCallback(() => boot(), [boot]);
  const { errorMessage } = useNotifications();

  useEffect(() => {
    setupIntercom();
  }, [setupIntercom]);

  const [email, setEmail] = useState(null);
  const [contacts, setContacts] = useState(null);
  const [password, setPassword] = useState(null);
  const [currentStep, setCurrentStep] = useState(0);
  const [businessEntityName, setBusinessEntityName] = useState('');
  const [businessEntityAddress, setBusinessEntityAddress] = useState({
    businessEntityStreetName: '',
    businessEntityStreetNumber: '',
    businessEntityZipCode: '',
    businessEntityCity: '',
  });
  const [initialLocationGroup, setInitialLocationGroup] = useState();
  const campaign = useCampaign(queryParameters);

  const nextStep = () => setCurrentStep(currentStep + 1);
  const previousStep = () => setCurrentStep(currentStep - 1);

  const navigateToLogin = () =>
    history.push(`${LOGIN_ROUTE}${queryParameters}`);

  useEffect(() => {
    const queryParameterEmail = new URLSearchParams(queryParameters).get(
      'email'
    );
    if (queryParameterEmail) {
      setEmail(queryParameterEmail);
      setCurrentStep(1);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (email) {
      update({ email });
    }
    if (contacts) {
      update({
        phone: contacts.phone,
        name: `${contacts.firstName} ${contacts.lastName}`,
      });
    }

    trackEvent(`OPEN_REGISTRATION_STEP_${currentStep}`);
  }, [currentStep, trackEvent, update, email, contacts]);

  const resetAuthentication = () => {
    setEmail(null);
    setContacts(null);
    setPassword(null);
    setCurrentStep(0);

    if (IS_MOBILE) {
      return;
    }

    navigateToLogin();
  };

  const getNavigation = () => {
    return `${currentStep + 1}/${STEP_LENGTH}`;
  };

  const register = async () => {
    const { firstName, lastName, phone } = contacts;
    const {
      businessEntityStreetName,
      businessEntityStreetNumber,
      businessEntityZipCode,
      businessEntityCity,
    } = businessEntityAddress;

    const registrationData = {
      email,
      firstName,
      lastName,
      phone,
      businessEntityName,
      businessEntityStreetName,
      businessEntityStreetNumber,
      businessEntityZipCode,
      businessEntityCity,
      password,
      agreement: true,
      avvAccepted: true,
      lastVersionSeen: process.env.REACT_APP_VERSION,
      initialLocationGroup,
      ...(campaign ? { campaign } : {}),
    };

    const response = await registerOperator(registrationData);

    if (response.ok) {
      return nextStep();
    }

    if (response.status === 403) {
      return errorMessage('register.error.ipAddress');
    }

    if (response.status === 400) {
      return errorMessage('register.error.invalidSchema');
    }

    return errorMessage('registration.server.error.msg');
  };

  const steps = [
    {
      id: '0',
      content: (
        <EmailStep
          email={email}
          setEmail={setEmail}
          next={nextStep}
          navigation={getNavigation()}
        />
      ),
    },
    {
      id: '1',
      content: (
        <ContactInputStep
          contacts={contacts}
          setContacts={setContacts}
          back={previousStep}
          next={nextStep}
          navigation={getNavigation()}
        />
      ),
    },
    {
      id: '2',
      content: (
        <BusinessInfoStep
          businessEntityName={businessEntityName}
          setBusinessEntityName={setBusinessEntityName}
          businessEntityAddress={businessEntityAddress}
          setBusinessEntityAddress={setBusinessEntityAddress}
          back={previousStep}
          next={nextStep}
          navigation={getNavigation()}
        />
      ),
    },
    {
      id: '3',
      content: (
        <SetPasswordStep
          setPassword={setPassword}
          previousSetPassword={password}
          name={{
            firstName: contacts?.firstName,
            lastName: contacts?.lastName,
          }}
          email={email}
          back={previousStep}
          next={nextStep}
          navigation={getNavigation()}
        />
      ),
    },
    {
      id: '4',
      content: (
        <LocationGroupStep
          initialLocationGroup={initialLocationGroup}
          setInitialLocationGroup={setInitialLocationGroup}
          back={previousStep}
          next={nextStep}
          navigation={getNavigation()}
        />
      ),
    },
    {
      id: '5',
      content: (
        <LegalTermsStep
          back={previousStep}
          next={register}
          navigation={getNavigation()}
        />
      ),
    },
    {
      id: '6',
      content: (
        <FinishRegisterStep
          next={resetAuthentication}
          navigation={getNavigation()}
        />
      ),
    },
  ];

  return (
    <>
      <SiteHeader
        title={intl.formatMessage({ id: 'locations.site.title' })}
        meta={intl.formatMessage({ id: 'locations.site.meta' })}
      />
      <Wrapper id={IS_MOBILE ? 'isMobile' : ''}>
        <Background isRegistration />
        <AuthenticationWrapper id="noSteps">
          <AuthenticationCard>
            <Steps
              progressDot={() => null}
              current={currentStep}
              style={{
                marginTop: 16,
                marginBottom: 48,
              }}
            >
              {steps.map(step => (
                <Steps.Step key={step.id} />
              ))}
            </Steps>
            {steps[currentStep].content}
          </AuthenticationCard>
          <Footer />
        </AuthenticationWrapper>
      </Wrapper>
    </>
  );
};
