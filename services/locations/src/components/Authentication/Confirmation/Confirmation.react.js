import React from 'react';
import { useIntl } from 'react-intl';

import { IS_MOBILE } from 'constants/environment';

import { SecondaryButton, SiteHeader } from 'components/general';

import { useGetPaymentLocationGroups } from 'components/hooks/useQueries';
import { isEmpty } from 'lodash';
import { DoneConfirmation } from './DoneConfirmation';
import {
  Wrapper,
  AuthenticationCard,
  AuthenticationWrapper,
} from '../Authentication.styled';
import { Footer } from '../Footer';
import { Background } from '../Background';

import {
  ShareExplainText,
  StyledShareAltOutlined,
  StyledShareButtonWrapper,
} from './Confirmation.styled';

export const Confirmation = () => {
  const { data: groups = [], isLoading } = useGetPaymentLocationGroups();
  const paymentLocationExists = !isEmpty(groups);
  const intl = useIntl();

  const onShare = () => {
    if (navigator.share) {
      navigator.share({
        // eslint-disable-next-line no-restricted-globals
        url: location.origin,
        title: 'Luca locations',
      });
    }
  };

  if (isLoading) {
    return null;
  }

  return (
    <>
      <SiteHeader
        title={intl.formatMessage({ id: 'locations.site.title' })}
        meta={intl.formatMessage({ id: 'locations.site.meta' })}
      />
      <Wrapper id={IS_MOBILE ? 'isMobile' : ''}>
        <Background />
        <AuthenticationWrapper id="noSteps">
          <AuthenticationCard>
            <DoneConfirmation
              finishMobileTitleId={
                paymentLocationExists
                  ? 'login.finish.mobile.trusted.title'
                  : 'login.finish.mobile.title'
              }
              finishMobileSubtitleId={
                paymentLocationExists
                  ? 'login.finish.mobile.trusted.subTitle'
                  : 'login.finish.mobile.subTitle'
              }
            />
            {navigator.share && (
              <StyledShareButtonWrapper>
                <SecondaryButton onClick={onShare}>
                  <StyledShareAltOutlined />{' '}
                  {intl.formatMessage({ id: 'login.finish.mobile.share' })}
                </SecondaryButton>
                <ShareExplainText>
                  {intl.formatMessage({
                    id: 'login.finish.mobile.shareDescription',
                  })}
                </ShareExplainText>
              </StyledShareButtonWrapper>
            )}
          </AuthenticationCard>
          <Footer />
        </AuthenticationWrapper>
      </Wrapper>
    </>
  );
};
