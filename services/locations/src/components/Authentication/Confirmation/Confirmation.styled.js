import styled from 'styled-components';

import { ShareAltOutlined } from '@ant-design/icons';

export const StyledShareButtonWrapper = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
`;

export const ShareExplainText = styled.div`
  font-size: 14px;
  padding-top: 8px;
  font-weight: 500;
  text-align: center;
  margin-bottom: 24px;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
`;

export const StyledShareAltOutlined = styled(ShareAltOutlined)`
  font-size: 14px;
  font-weight: bold;
`;
