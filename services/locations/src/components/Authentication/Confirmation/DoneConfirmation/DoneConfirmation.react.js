import React from 'react';
import { useIntl } from 'react-intl';
import {
  CardSubTitle,
  CardTitle,
  StyledCheckIcon,
  ConfirmationWrapper,
  IconWrapper,
} from 'components/Authentication/Authentication.styled';

export const DoneConfirmation = ({
  finishMobileTitleId,
  finishMobileSubtitleId,
}) => {
  const intl = useIntl();

  return (
    <ConfirmationWrapper>
      <CardTitle>
        {intl.formatMessage({
          id: finishMobileTitleId,
        })}
      </CardTitle>
      <CardSubTitle>
        {intl.formatMessage({
          id: finishMobileSubtitleId,
        })}
      </CardSubTitle>
      <IconWrapper>
        <StyledCheckIcon />
      </IconWrapper>
    </ConfirmationWrapper>
  );
};
