import React, { useEffect } from 'react';

import { useIntl } from 'react-intl';
import { useIntercom } from 'react-use-intercom';

import { SiteHeader } from 'components/general';
import { IS_MOBILE } from 'constants/environment';

import { Background } from '../Background';
import { Footer } from '../Footer';

import { AuthenticationWrapper, Wrapper } from '../Authentication.styled';
import { LoginCard } from './LoginCard';

export const Login = () => {
  const intl = useIntl();
  const { trackEvent } = useIntercom();

  useEffect(() => {
    trackEvent('OPEN_LOGIN');
  }, [trackEvent]);

  return (
    <>
      <SiteHeader
        title={intl.formatMessage({ id: 'locations.site.title' })}
        meta={intl.formatMessage({ id: 'locations.site.meta' })}
      />
      <Wrapper id={IS_MOBILE ? 'isStyledInputMobile' : ''}>
        <Background />
        <AuthenticationWrapper id="noSteps">
          <LoginCard />
          <Footer />
        </AuthenticationWrapper>
      </Wrapper>
    </>
  );
};
