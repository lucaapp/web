import React from 'react';
import { useIntl } from 'react-intl';
import { LOCATION_RAPYD_LINK } from 'constants/links';

import {
  CardTitle,
  CardSubTitle,
} from 'components/Authentication/Authentication.styled';

export const LoginCardHeader = ({ isCampaignRoute }) => {
  const intl = useIntl();

  const getLoginDescriptionContent = () => {
    if (isCampaignRoute) {
      return intl.formatMessage(
        {
          id: 'loginCard.description',
        },
        {
          link: (
            <a href={LOCATION_RAPYD_LINK} rel="noopener noreferrer">
              {intl.formatMessage({ id: 'login.blogpost' })}
            </a>
          ),
        }
      );
    }
    return intl.formatMessage({
      id: 'loginCard.login.description',
    });
  };
  return (
    <>
      <CardTitle data-cy="loginPage">
        {intl.formatMessage({
          id: 'loginCard.title',
        })}
      </CardTitle>
      <CardSubTitle>{getLoginDescriptionContent()}</CardSubTitle>
    </>
  );
};
