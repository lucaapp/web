import React from 'react';
import { useIntl } from 'react-intl';
import { useHistory, useLocation } from 'react-router';

import { IS_MOBILE } from 'constants/environment';
import { REGISTER_ROUTE } from 'constants/routes';

import {
  StyledSecondaryButton,
  StyledWhiteButton,
} from 'components/Authentication/Authentication.styled';
import { ButtonWrapper, Divider } from './LoginActions.styled';

export const LoginActions = () => {
  const intl = useIntl();
  const history = useHistory();
  const queryParameters = useLocation().search;

  const onRedirect = () => history.push(`${REGISTER_ROUTE}${queryParameters}`);

  return (
    <ButtonWrapper>
      <StyledWhiteButton data-cy="loginSubmitButton" htmlType="submit">
        {intl.formatMessage({
          id: 'authentication.login.title',
        })}
      </StyledWhiteButton>
      {!IS_MOBILE && <Divider />}
      <StyledSecondaryButton onClick={onRedirect}>
        {intl.formatMessage({
          id: 'authentication.register.title',
        })}
      </StyledSecondaryButton>
    </ButtonWrapper>
  );
};
