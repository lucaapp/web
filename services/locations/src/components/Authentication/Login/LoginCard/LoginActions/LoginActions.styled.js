import styled from 'styled-components';

import { Media } from 'utils/media';

export const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  margin-top: 24px;
  row-gap: 24px;

  ${Media.tablet`
    margin-top: 40px;
  `}
`;

export const Divider = styled.div`
  border-bottom: 1px solid rgb(0, 0, 0);
`;
