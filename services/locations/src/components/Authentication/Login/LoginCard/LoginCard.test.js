import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { fireEvent, waitFor } from '@testing-library/dom';
import { act } from 'react-dom/test-utils';
import * as api from 'network/api';
import * as jwtStorage from 'utils/jwtStorage';
import * as utilsStorage from 'utils/storage';
import { LoginCard } from './LoginCard.react';

const email = 'some@address.com';
const password = 's0m3p@55w0rd';

// Mocking functions from network/api
const mockLoginForStatus = status =>
  jest.spyOn(api, 'login').mockResolvedValue({ status });

describe('LoginCardComponent', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  const setup = async () => {
    const renderResult = render(<LoginCard />);

    // can be found by data-cy="loginCard" inside the react dom
    const loginCard = await screen.findByTestId('loginCard');
    const loginCardTitle = await screen.findByTestId('loginPage');
    const loginEmailInputField = await screen.findByTestId(
      'login-emailInputField'
    );
    const loginPasswordInputField = await screen.findByTestId(
      'login-passwordInputField'
    );
    const forgotPasswordLink = await screen.findByTestId('forgotPasswordLink');
    const loginSubmitButton = await screen.findByTestId('loginSubmitButton');

    return {
      loginCard,
      renderResult,
      loginCardTitle,
      loginEmailInputField,
      loginPasswordInputField,
      forgotPasswordLink,
      loginSubmitButton,
    };
  };

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });

  // Check if the error message for a string that is not an email address is displayed
  test('Email validation error displayed', async () => {
    const { loginEmailInputField, renderResult } = await setup();
    mockLoginForStatus(400);
    const notAnEmail = 'notAnEmail';

    fireEvent.change(loginEmailInputField, {
      target: { value: notAnEmail },
    });
    expect(loginEmailInputField.value).toEqual(notAnEmail);
    loginEmailInputField.blur();
    const result = await renderResult.findAllByText(
      'error.email' // Using translation keys to check appearance
    );

    expect(result).toBeTruthy();
  });

  test('Login error message is displayed', async () => {
    const {
      loginEmailInputField,
      loginPasswordInputField,
      loginSubmitButton,
    } = await setup();

    mockLoginForStatus(400);

    fireEvent.change(loginEmailInputField, {
      target: { value: email },
    });
    expect(loginEmailInputField.value).toEqual(email);

    fireEvent.change(loginPasswordInputField, {
      target: { value: password },
    });

    expect(loginPasswordInputField.value).toEqual(password);

    await act(async () => {
      // This will call the mocked network function named 'login'
      await loginSubmitButton.click();
    });

    expect(screen.getByText('notification.login.error')).toBeInTheDocument();

    // Check if error message is displayed underneath password field
    const errorWrongLoginDetails = await screen.findByTestId(
      'error-wrongLoginDetails'
    );
    expect(errorWrongLoginDetails).toBeDefined();
  });

  test('Test successful Login', async () => {
    const {
      loginEmailInputField,
      loginPasswordInputField,
      loginSubmitButton,
    } = await setup();
    mockLoginForStatus(204);

    const mockedClearJwt = jest.spyOn(jwtStorage, 'clearJwt');
    mockedClearJwt.mockImplementation();

    const mockedClearHasSeenPrivateKeyModal = jest.spyOn(
      utilsStorage,
      'clearHasSeenPrivateKeyModal'
    );
    mockedClearHasSeenPrivateKeyModal.mockImplementation();

    fireEvent.change(loginEmailInputField, {
      target: { value: email },
    });
    expect(loginEmailInputField.value).toEqual(email);

    fireEvent.change(loginPasswordInputField, {
      target: { value: password },
    });
    expect(loginPasswordInputField.value).toEqual(password);
    await act(async () => {
      await loginSubmitButton.click();
    });

    expect(mockedClearJwt).toHaveBeenCalledTimes(1);
    expect(mockedClearHasSeenPrivateKeyModal).toHaveBeenCalledTimes(1);
    await waitFor(() => {
      expect(window.location.toString()).toBe('/app/group/'); // make this better some day
    });
  });
});
