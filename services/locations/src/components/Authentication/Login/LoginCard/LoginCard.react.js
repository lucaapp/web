import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useLocation } from 'react-router';
import { Form } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';

import { useEmailValidator } from 'components/hooks/useValidators';

import { login } from 'network/api';
import { BASE_GROUP_ROUTE } from 'constants/routes';

import { useCampaign } from 'utils/campaign';
import { usePrivateKey } from 'utils/privateKey';
import { clearHasSeenPrivateKeyModal } from 'utils/storage';

import { clearJwt } from 'utils/jwtStorage';
import {
  AuthenticationCard,
  StyledInput,
  StyledPasswordInput,
} from 'components/Authentication/Authentication.styled';

import { useNotifications, useRedirectTo } from 'components/hooks';
import { ForgotPasswordLink } from './ForgotPasswordLink';
import { LoginError } from './LoginError';
import { LoginActions } from './LoginActions';
import { LoginCardHeader } from './LoginCardHeader';

import { StyledFormItem } from './LoginCard.styled';

export const LoginCard = () => {
  const intl = useIntl();
  const [, clearPrivateKey] = usePrivateKey(null);
  const [error, setError] = useState(null);
  const location = useLocation();
  const campaign = useCampaign(location.search);
  const isCampaignRoute = !!campaign;
  const { redirectToSearchLocation } = useRedirectTo();

  const emailValidator = useEmailValidator();
  const { errorMessage } = useNotifications();

  const handleLoginErrors = response => {
    if (response.status === 429) {
      setError({
        message: 'registration.server.error.tooManyRequests.title',
      });

      errorMessage(
        'registration.server.error.tooManyRequests.desc',
        'registration.server.error.tooManyRequests.title'
      );

      return;
    }
    if (response.status === 423) {
      setError({
        message: 'registration.server.error.notActivated.title',
      });

      errorMessage(
        'registration.server.error.notActivated.desc',
        'registration.server.error.notActivated.title'
      );

      return;
    }

    setError({
      message: 'login.error',
    });

    errorMessage('notification.login.error');
  };

  const onFinish = values => {
    const { email, password } = values;
    login({ username: email, password })
      .then(response => {
        if (response.status !== 204) {
          handleLoginErrors(response);
          return;
        }
        setError(null);
        clearPrivateKey(null);
        clearJwt();
        clearHasSeenPrivateKeyModal();

        const newLocation = redirectToSearchLocation() ?? BASE_GROUP_ROUTE;

        window.location = newLocation;
      })
      .catch(() => {
        errorMessage(
          'registration.server.error.desc',
          'registration.server.error.msg'
        );
      });
  };

  return (
    <AuthenticationCard data-cy="loginCard">
      <LoginCardHeader isCampaignRoute={isCampaignRoute} />
      <Form onFinish={onFinish}>
        <StyledFormItem
          colon={false}
          label={intl.formatMessage({
            id: 'registration.form.email',
          })}
          name="email"
          rules={emailValidator}
        >
          <StyledInput
            data-cy="login-emailInputField"
            autoFocus
            autoComplete="username"
          />
        </StyledFormItem>
        <StyledFormItem
          colon={false}
          label={intl.formatMessage({
            id: 'registration.form.password',
          })}
          name="password"
          required
        >
          <StyledPasswordInput
            data-cy="login-passwordInputField"
            autoComplete="current-password"
            iconRender={visible =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
          />
        </StyledFormItem>
        <LoginError error={error} />
        <ForgotPasswordLink campaign={campaign} />
        <LoginActions />
      </Form>
    </AuthenticationCard>
  );
};
