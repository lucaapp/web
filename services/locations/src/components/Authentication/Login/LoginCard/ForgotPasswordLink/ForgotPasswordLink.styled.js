import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const ForgotPasswordLinkWrapper = styled.div`
  text-decoration: underline;
  display: inline-block;
  text-align: left;
  width: 100%;
`;

export const StyledLink = styled(Link)`
  font-weight: 600;
`;
