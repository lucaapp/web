import React from 'react';
import { useIntl } from 'react-intl';
import queryString from 'query-string';

// Constants
import { FORGOT_PASSWORD_ROUTE } from 'constants/routes';

import {
  StyledLink,
  ForgotPasswordLinkWrapper,
} from './ForgotPasswordLink.styled';

export const ForgotPasswordLink = ({ campaign }) => {
  const intl = useIntl();

  return (
    <ForgotPasswordLinkWrapper>
      <StyledLink
        to={{
          pathname: `${FORGOT_PASSWORD_ROUTE}`,
          search: campaign ? `?${queryString.stringify(campaign || {})}` : '',
        }}
        data-cy="forgotPasswordLink"
      >
        {intl.formatMessage({ id: 'login.error.forgotPassword' })}
      </StyledLink>
    </ForgotPasswordLinkWrapper>
  );
};
