import styled from 'styled-components';
import { Media } from 'utils/media';

export const Link = styled.a`
  font-family: Montserrat-SemiBold, sans-serif;
  font-size: 14px;
  font-weight: 600;
  color: white;
  display: block;
  width: fit-content;
  text-decoration: none;

  ${Media.mobile`
    color: rgba(0, 0, 0, 0.87);
    font-size: 12px;
    text-decoration: underline;
  `}
`;

export const Version = styled.span`
  cursor: default;
  user-select: none;
  font-family: Montserrat-SemiBold, sans-serif;
  font-size: 14px;
  font-weight: 600;
  color: white;
  display: block;
  text-decoration: none;
  word-break: keep-all;

  ${Media.mobile`
    color: rgba(129, 129, 129, 0.87);
    font-size: 12px;
  `}
`;

export const LegalWrapper = styled.div`
  position: absolute;
  left: 24px;
  bottom: 24px;
  right: 24px;
  z-index: 1;

  @media (max-width: 720px) {
    position: unset;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    background: rgb(195, 206, 217);
    width: 100%;
    padding: 24px 24px 40px;
    margin-top: -1px;
  }
`;

export const VersionNumber = styled.div`
  margin-top: 16px;
`;
