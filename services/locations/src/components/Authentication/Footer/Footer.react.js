import React from 'react';
import { useIntl } from 'react-intl';

import { useGetVersion } from 'components/hooks';
import {
  TERMS_CONDITIONS_LINK,
  FAQ_LINK,
  GITLAB_LINK,
  IMPRESSUM_LINK,
} from 'constants/links';

import { IS_MOBILE } from 'constants/environment';
import { Link, Version, LegalWrapper, VersionNumber } from './Footer.styled';

export const Footer = () => {
  const intl = useIntl();
  const { isSuccess, data: info } = useGetVersion();

  const versionNumber = (
    <Version
      data-cy="lucaVersionNumber"
      title={isSuccess ? `(${info.version})` : ''}
    >
      {isSuccess ? (IS_MOBILE ? info.version : `(${info.version})`) : ''}
    </Version>
  );

  return (
    <LegalWrapper>
      <Link
        data-cy="faqLink"
        href={FAQ_LINK}
        target="_blank"
        rel="noopener noreferrer"
      >
        {intl.formatMessage({ id: 'location.footer.faq' })}
      </Link>
      <Link
        data-cy="gitLabLink"
        href={GITLAB_LINK}
        target="_blank"
        rel="noopener noreferrer"
      >
        {intl.formatMessage({ id: 'location.footer.repository' })}
      </Link>
      <Link data-cy="impressumLink" href={IMPRESSUM_LINK} target="_blank">
        {intl.formatMessage({ id: 'location.footer.impressum' })}
      </Link>
      <Link
        data-cy="termsAndConditionsLink"
        href={TERMS_CONDITIONS_LINK}
        target="_blank"
      >
        {intl.formatMessage({ id: 'authentication.background.legal.agb' })}
      </Link>
      {IS_MOBILE ? (
        <VersionNumber>{versionNumber}</VersionNumber>
      ) : (
        <Version data-cy="lucaVersionHeadline">
          {isSuccess ? 'luca Locations' : ''}
        </Version>
      )}

      {!IS_MOBILE && <div>{versionNumber}</div>}
    </LegalWrapper>
  );
};
