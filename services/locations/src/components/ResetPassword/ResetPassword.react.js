import React from 'react';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';
import { useParams } from 'react-router';
import { getPasswordResetRequest } from 'network/api';
import { SiteHeader } from 'components/general';
import { Header } from 'components/Header';
import { Footer } from '../Authentication/Footer';
import {
  AuthenticationWrapper,
  AuthenticationCard,
} from '../Authentication/Authentication.styled';
import { ErrorView } from './ErrorView';
import { ResetPasswordForm } from './ResetPasswordForm';
import { Wrapper } from './ResetPassword.styled';

export const ResetPassword = () => {
  const intl = useIntl();

  const { requestId } = useParams();

  const {
    isLoading,
    error: requestError,
  } = useQuery(
    `passwordRequest/${requestId}`,
    () => getPasswordResetRequest(requestId),
    { cacheTime: 0, retry: 0 }
  );

  if (isLoading) {
    return null;
  }

  return (
    <>
      <SiteHeader
        title={intl.formatMessage({ id: 'resetPassword.site.title' })}
        meta={intl.formatMessage({ id: 'resetPassword.site.meta' })}
      />
      <Wrapper>
        <Header title={intl.formatMessage({ id: 'header.subtitle' })} />
        <AuthenticationWrapper>
          <AuthenticationCard>
            {requestError ? <ErrorView /> : <ResetPasswordForm />}
          </AuthenticationCard>
          <Footer />
        </AuthenticationWrapper>
      </Wrapper>
    </>
  );
};
