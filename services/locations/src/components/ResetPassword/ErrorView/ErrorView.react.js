import React from 'react';
import { useIntl } from 'react-intl';
import { Alert } from 'antd';

export const ErrorView = () => {
  const intl = useIntl();

  return (
    <Alert
      message={intl.formatMessage({
        id: 'resetPassword.error.expired',
      })}
      type="error"
    />
  );
};
