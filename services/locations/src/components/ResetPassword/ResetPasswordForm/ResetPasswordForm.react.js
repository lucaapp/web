import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';
import { useHistory, useParams } from 'react-router';
import { useLocation } from 'react-router-dom';
import { zxcvbnWithLanguageOption } from 'config.zxcvbn';

import { MIN_STRENGTH_SCORE } from 'constants/general';
import { LOGIN_ROUTE } from 'constants/routes';
import { getLanguage } from 'services';
import { resetPassword } from 'network/api';
import { useCampaign } from 'utils/campaign';
import { useNotifications } from 'components/hooks';
import { StrengthMeter } from 'components/general';
import { ResetPasswordFormHeader } from './ResetPasswordFormHeader';
import {
  ButtonWrapper,
  StyledPasswordInput,
  StyledSecondaryButton,
  StyledWhiteButton,
} from '../../Authentication/Authentication.styled';

export const ResetPasswordForm = () => {
  const intl = useIntl();
  const { successMessage, errorMessage } = useNotifications();
  const history = useHistory();
  const queryParameters = useLocation().search;
  const campaign = useCampaign(queryParameters);

  const onRedirect = () => history.push(LOGIN_ROUTE);

  const { requestId } = useParams();
  const [strengthScore, setStrengthScore] = useState(0);
  const [warningsList, setWarningsList] = useState([]);
  const [displayStrengthMeter, setDisplayStrengthMeter] = useState(false);

  const onChange = values => {
    const { newPassword } = values;

    if (newPassword) {
      const clientLang = getLanguage();
      setDisplayStrengthMeter(!!newPassword);

      const passwordStrength = zxcvbnWithLanguageOption(
        newPassword,
        [],
        clientLang
      );

      setStrengthScore(passwordStrength.score);
      setWarningsList([
        passwordStrength?.feedback?.warning,
        ...passwordStrength?.feedback?.suggestions,
      ]);
    }
  };

  const onFinish = values => {
    const { newPassword } = values;

    resetPassword({
      newPassword,
      resetId: requestId,
      ...(campaign ? { campaign } : {}),
    })
      .then(response => {
        switch (response.status) {
          case 204:
            successMessage('notification.resetPassword.success');

            setDisplayStrengthMeter(false);
            setStrengthScore(0);

            history.push(`${LOGIN_ROUTE}${queryParameters}`);
            break;
          case 409:
            errorMessage('notification.resetPassword.error.matchesOldPassword');
            break;
          default:
            errorMessage('notification.resetPassword.error');
            break;
        }
      })
      .catch(() => errorMessage('notification.network.error'));
  };

  return (
    <Form onFinish={onFinish} onValuesChange={onChange}>
      <ResetPasswordFormHeader />
      <Form.Item
        colon={false}
        name="newPassword"
        label={intl.formatMessage({
          id: 'profile.changePassword.newPassword',
        })}
        hasFeedback
        rules={[
          {
            required: true,
            message: intl.formatMessage({
              id: 'error.password',
            }),
          },
          () => ({
            validator() {
              if (displayStrengthMeter && strengthScore < MIN_STRENGTH_SCORE) {
                return strengthScore !== MIN_STRENGTH_SCORE - 1
                  ? Promise.reject(warningsList)
                  : Promise.reject(
                      intl.formatMessage({
                        id: 'error.password.simple',
                      })
                    );
              }

              return Promise.resolve();
            },
          }),
        ]}
      >
        <StyledPasswordInput />
      </Form.Item>
      {displayStrengthMeter && (
        <StrengthMeter strengthScore={strengthScore} lightVariation />
      )}
      <Form.Item
        colon={false}
        name="newPasswordConfirm"
        label={intl.formatMessage({
          id: 'profile.changePassword.newPasswordRepeat',
        })}
        hasFeedback
        dependencies={['newPassword']}
        rules={[
          {
            required: true,
            message: intl.formatMessage({
              id: 'error.passwordConfirm',
            }),
          },
          ({ getFieldValue }) => ({
            validator(rule, value) {
              if (!value || getFieldValue('newPassword') === value) {
                return Promise.resolve();
              }
              return Promise.reject(
                intl.formatMessage({
                  id: 'error.passwordConfirm',
                })
              );
            },
          }),
        ]}
      >
        <StyledPasswordInput />
      </Form.Item>
      <ButtonWrapper multipleButtons>
        <StyledSecondaryButton onClick={onRedirect}>
          {intl.formatMessage({
            id: 'authentication.returnToLogin',
          })}
        </StyledSecondaryButton>
        <StyledWhiteButton htmlType="submit">
          {intl.formatMessage({
            id: 'resetPassword.form.button',
          })}
        </StyledWhiteButton>
      </ButtonWrapper>
    </Form>
  );
};
