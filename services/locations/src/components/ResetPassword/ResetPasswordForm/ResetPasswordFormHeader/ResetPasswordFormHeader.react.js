import React from 'react';
import { useIntl } from 'react-intl';
import {
  CardTitle,
  CardSubTitle,
} from 'components/Authentication/Authentication.styled';

export const ResetPasswordFormHeader = () => {
  const intl = useIntl();

  return (
    <>
      <CardTitle>
        {intl.formatMessage({
          id: 'forgotPassword.title',
        })}
      </CardTitle>
      <CardSubTitle>
        {intl.formatMessage({
          id: 'resetPassword.subtitle',
        })}
      </CardSubTitle>
    </>
  );
};
