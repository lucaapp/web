import React, { useEffect, useState } from 'react';
import { isEmpty } from 'lodash';

import { getCampaignParameters } from 'utils/campaign';
import {
  createCampaign,
  getCampaignsForOperator,
  getGroups,
} from 'network/api';
import {
  getLocationGroups,
  createPaymentOperator,
  getPaymentOperator,
} from 'network/payment';

import { EnableLocationPaymentMobile } from 'components/App/EnableLocationPaymentMobile';
import { Confirmation } from 'components/Authentication/Confirmation';
import { IS_MOBILE } from 'constants/environment';
import { useNotifications, useOperator } from 'components/hooks';

export const PaymentOnboardingWrapper = ({ children, location }) => {
  const { errorMessage } = useNotifications();
  const [paymentOperator, setPaymentOperator] = useState(null);
  const [locationGroups, setLocationGroups] = useState(null);
  const [paymentLocationGroups, setPaymentLocationGroups] = useState(null);

  const { data: operator } = useOperator();

  useEffect(() => {
    const campaignParameters = getCampaignParameters(location.search);
    const isCampaignRoute = !!campaignParameters;

    const checkAndCreatePaymentOperator = async () => {
      try {
        let fetchedPaymentOperator = await getPaymentOperator(
          operator.operatorId
        );

        if (fetchedPaymentOperator) {
          const fetchedPaymentLocationGroups = await getLocationGroups();
          setPaymentLocationGroups(fetchedPaymentLocationGroups);
        } else {
          fetchedPaymentOperator = await createPaymentOperator();
        }
        setPaymentOperator(fetchedPaymentOperator);

        const fetchedLocationGroups = await getGroups();
        setLocationGroups(fetchedLocationGroups);
      } catch (error) {
        console.error(error);
      }
    };

    const checkCampaignAndPaymentOperator = async () => {
      try {
        const campaigns = await getCampaignsForOperator();

        if (campaigns.length > 0) {
          await checkAndCreatePaymentOperator();
        } else if (isCampaignRoute) {
          await createCampaign(campaignParameters);
          await checkAndCreatePaymentOperator();
        }
      } catch {
        errorMessage('error.description');
      }
    };

    checkCampaignAndPaymentOperator();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (IS_MOBILE && paymentOperator) {
    if (!paymentOperator.skipKybOnboarding || !isEmpty(paymentLocationGroups)) {
      return <Confirmation />;
    }

    if (!isEmpty(locationGroups) && isEmpty(paymentLocationGroups)) {
      return <EnableLocationPaymentMobile />;
    }
  }

  return <>{children}</>;
};
