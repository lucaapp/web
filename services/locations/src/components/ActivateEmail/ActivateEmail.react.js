import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { Alert } from 'antd';
import { useQueryClient } from 'react-query';
import { useIntl } from 'react-intl';

import { confirmEmail, logout } from 'network/api';
import { Header } from 'components/Header';

import { clearJwt } from 'utils/jwtStorage';
import { useLoginRoute } from 'components/hooks';
import { Main, Wrapper } from './ActivateEmail.styled';

const REDIRECT_TIMEOUT = 1000 * 5;

const ActivateEmailRaw = () => {
  const intl = useIntl();
  const loginRoute = useLoginRoute();
  const { activationId } = useParams();
  const queryClient = useQueryClient();

  const [status, setStatus] = useState(null);
  const [remainingTime, setRemainingTime] = useState(5);

  useEffect(() => {
    confirmEmail({ activationId })
      .then(response => {
        setStatus(response.status);

        logout()
          .then(() => {
            queryClient.clear();
            clearJwt();
          })
          .catch(() => {
            window.location = loginRoute;
          });

        setTimeout(() => {
          window.location = loginRoute;
        }, REDIRECT_TIMEOUT);

        setInterval(() => {
          setRemainingTime(oldTime => oldTime - 1);
        }, 1000);
      })
      .catch(setStatus);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Main>
      <Header title={intl.formatMessage({ id: 'activation' })} />
      <Wrapper>
        {status && (
          <Alert
            type={status >= 400 ? 'error' : 'success'}
            message={intl.formatMessage(
              {
                id:
                  status >= 400
                    ? `changeMail.alert.${status}`
                    : 'changeMail.alert.200',
              },
              { seconds: remainingTime }
            )}
          />
        )}
      </Wrapper>
    </Main>
  );
};

export const ActivateEmail = React.memo(ActivateEmailRaw);
