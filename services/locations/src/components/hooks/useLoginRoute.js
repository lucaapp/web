import { useLocation } from 'react-router';

import { LOGIN_ROUTE } from 'constants/routes';

export const useLoginRoute = () => {
  const { pathname } = useLocation();

  if (!pathname) {
    return LOGIN_ROUTE;
  }

  return `${LOGIN_ROUTE}?redirectTo=${pathname}`;
};
