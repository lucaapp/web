import { useQueries, useQuery } from 'react-query';
import { COUNTER_REFETCH_INTERVAL_MS } from 'constants/timings';
import { defaultFeatureFlags } from 'constants/featureFlags';
import {
  getAdditionalData,
  getAllTransfers,
  getChallengeState,
  getClientConfig,
  getCurrentCount,
  getDeletedGroups,
  getEmployees,
  getGroup,
  getGroups,
  getHealthDepartment,
  getLocation,
  getLocationLinks,
  getLocationTables,
  getOperatorDevices,
  getPrivateKeySecret,
  getTraces,
  isEmailUpdatePending,
  getLocationTableByTableId,
} from 'network/api';
import { getVersion } from 'network/static';
import { getSignatureValidityForAllTransfers } from 'utils/signatures';
import { getValidatedExtractedDailyKey } from 'utils/dailyKey';
import {
  getBalance,
  getDisplayPaymentModal,
  getKybOnboardingStatus,
  getLocationGroup,
  getLocationGroups,
  getParticipatedCampaigns,
  getOnboardingStatus,
  getOperatorPaymentEnabled,
  getPaymentActive,
  getPaymentOperator,
  getLocationGroupPayments,
  getLucaCampaigns,
  getLocationGroupTipTopUpAmount,
} from 'network/payment';
import { getPOSSystems } from 'network/pos';

export const QUERY_KEYS = {
  ME: 'ME',
  LOCATION: 'LOCATION',
  SETTINGS: 'SETTINGS',
  GROUPS: 'GROUPS',
  PRIVATE_KEY_SECRET: 'PRIVATE_KEY_SECRET',
  DELETED_GROUPS: 'DELETED_GROUPS',
  LOCATION_LINKS: 'LOCATION_LINKS',
  ADDITIONAL_DATA: 'ADDITIONAL_DATA',
  OPERATOR_DEVICES: 'OPERATOR_DEVICES',
  TRANSFERS: 'TRANSFERS',
  TRANSFERS_WITH_VALIDITY: 'TRANSFERS_WITH_VALIDITY',
  HEALTH_DEPARTMENT: 'HEALTH_DEPARTMENT',
  GROUP: 'GROUP',
  LOCATION_TABLES: 'LOCATION_TABLES',
  LOCATION_TABLES_BY_ID: 'LOCATION_TABLES_BY_ID',
  CURRENT_COUNT: 'CURRENT',
  VERSION: 'VERSION',
  TRACES: 'TRACES',
  CLIENT_CONFIG: 'CLIENT_CONFIG',
  CHALLENGE: 'CHALLENGE',
  IS_MAIL_CHALLENGE_IN_PROGRESS: 'IS_MAIL_CHALLENGE_IN_PROGRESS',
  EMPLOYEES: 'EMPLOYEES',
  PAYMENT_ENABLED: 'PAYMENT_ENABLED',
  PAYMENTS: 'GET_PAYMENTS',
  PAYOUTS: 'GET_PAYOUTS',
  BALANCE: 'GET_BALANCE',
  DAILY_KEY: 'DAILY_KEY',
  PAYMENT_ONBOARDING_STATUS: 'PAYMENT_ONBOARDING_STATUS', // v1 deprecated
  KYB_ONBOARDING_STATUS: 'KYB_ONBOARDING_STATUS',
  PAYOUT_ONBOARDING_STATUS: 'PAYOUT_ONBOARDING_STATUS',
  LOCATION_PAYMENT_ACTIVE: 'LOCATION_PAYMENT_ACTIVE',
  PAYMENT_OPERATOR: 'PAYMENT_OPERATOR',
  PAYMENT_LOCATION_GROUP: 'PAYMENT_LOCATION_GROUP',
  PAYMENT_LOCATION_GROUPS: 'PAYMENT_LOCATION_GROUPS',
  PAYMENT_DISPLAY_INFO_MODAL: 'PAYMENT_DISPLAY_INFO_MODAL',
  CAMPAIGNS: 'CAMPAIGNS',
  POS_SYSTEMS: 'POST_SYSTEMS',
  LOCATION_GROUP_PAYMENTS: 'LOCATION_GROUP_PAYMENTS',
  ALL_LUCA_CAMPAIGNS: 'ALL_LUCA_CAMPAIGNS',
  LOCATION_GROUP_TIP_TOP_UP_AMOUNT: 'LOCATION_GROUP_TIP_TOP_UP_AMOUNT',
};

export const useDailyKey = () =>
  useQuery(QUERY_KEYS.DAILY_KEY, getValidatedExtractedDailyKey, {
    cacheTime: 0,
    retry: 0,
  });

export const useGetLocationById = (locationId, options) =>
  useQuery([QUERY_KEYS.LOCATION, locationId], () => getLocation(locationId), {
    cacheTime: 0,
    ...options,
  });

export const useGetLocationTables = locationId =>
  useQuery(
    [QUERY_KEYS.LOCATION_TABLES, locationId],
    () => getLocationTables(locationId),
    {
      cacheTime: 0,
    }
  );

export const useGetLocationTableByTableId = (tableId, options = {}) =>
  useQuery(
    [QUERY_KEYS.LOCATION_TABLES_BY_ID, tableId],
    () => getLocationTableByTableId(tableId),
    {
      ...options,
      retry: false,
      cacheTime: 1000 * 60 * 10, // cache for 10 minutes
    }
  );

export const useGetLocationsOfGroup = (queryKey, locationIds) =>
  useQueries(
    locationIds.map(locationId => {
      return {
        queryKey: [queryKey, locationId],
        queryFn: () => getLocation(locationId),
        cacheTime: 0,
      };
    })
  );

export const useGetGroups = (otherKeys, options) =>
  useQuery([QUERY_KEYS.GROUPS, otherKeys ?? ''], getGroups, options);

export const useGetPrivateKeySecret = () =>
  useQuery(QUERY_KEYS.PRIVATE_KEY_SECRET, getPrivateKeySecret, {
    retry: false,
  });

export const useDeletedGroups = () =>
  useQuery(QUERY_KEYS.DELETED_GROUPS, getDeletedGroups);

export const useGetLocationLinks = locationId =>
  useQuery([QUERY_KEYS.LOCATION_LINKS, locationId], () =>
    getLocationLinks(locationId)
  );

export const useGetAdditionalData = locationId =>
  useQuery([QUERY_KEYS.ADDITIONAL_DATA, locationId], () =>
    getAdditionalData(locationId)
  );

export const useGetTransfers = () =>
  useQuery(QUERY_KEYS.TRANSFERS, getAllTransfers);

export const useGetAllTransfersWithValidity = () =>
  useQuery(QUERY_KEYS.TRANSFERS_WITH_VALIDITY, async () => {
    return getSignatureValidityForAllTransfers(await getAllTransfers());
  });

export const useGetOperatorDevices = () =>
  useQuery(QUERY_KEYS.OPERATOR_DEVICES, getOperatorDevices);

export const useGetHealthDepartment = departmentId =>
  useQuery([QUERY_KEYS.HEALTH_DEPARTMENT, departmentId], () =>
    getHealthDepartment(departmentId)
  );

export const useGetMultipleHealthDepartments = departmentIds =>
  useQueries(
    departmentIds.map(departmentId => ({
      queryKey: [QUERY_KEYS.HEALTH_DEPARTMENT, departmentId],
      queryFn: () => getHealthDepartment(departmentId),
    }))
  );

export const useGetGroup = (groupId, options = {}) =>
  useQuery([QUERY_KEYS.GROUP, groupId], () => getGroup(groupId), {
    ...options,
  });

export const useGetBalance = (locationGroupId, options) =>
  useQuery(QUERY_KEYS.BALANCE, () => getBalance(locationGroupId), options);

export const useGetPaymentActive = locationId =>
  useQuery([QUERY_KEYS.LOCATION_PAYMENT_ACTIVE, locationId], () =>
    getPaymentActive(locationId)
  );

export const useGetPaymentEnabled = (operatorId, options) =>
  useQuery(
    QUERY_KEYS.PAYMENT_ENABLED,
    () => getOperatorPaymentEnabled(operatorId),
    { ...options }
  );

export const useGetPaymentOnboardingStatus = (locationGroupId, options = {}) =>
  useQuery(
    [QUERY_KEYS.PAYMENT_ONBOARDING_STATUS, locationGroupId],
    () => getOnboardingStatus(locationGroupId),
    { enabled: !!locationGroupId, ...options }
  );

export const useGetKybOnboardingStatus = (locationGroupId, options = {}) =>
  useQuery(
    [QUERY_KEYS.PAYMENT_ONBOARDING_STATUS, locationGroupId],
    () => getKybOnboardingStatus(locationGroupId),
    { enabled: !!locationGroupId, ...options }
  );

export const useGetCurrentCount = scannerAccessId =>
  useQuery(
    [QUERY_KEYS.CURRENT_COUNT, scannerAccessId],
    () => getCurrentCount(scannerAccessId),
    {
      refetchInterval: COUNTER_REFETCH_INTERVAL_MS,
    }
  );

export const useGetVersion = () =>
  useQuery([QUERY_KEYS.VERSION], () => getVersion(), {
    refetchOnWindowFocus: false,
  });

export const useGetTraces = (locationId, duration) =>
  useQuery(
    [QUERY_KEYS.TRACES, locationId, duration],
    () => getTraces(locationId, duration),
    {
      refetchOnWindowFocus: false,
    }
  );

export const useFeatureFlags = () => {
  const { data: clientConfig = {} } = useQuery(
    QUERY_KEYS.CLIENT_CONFIG,
    getClientConfig,
    {
      staleTime: Number.POSITIVE_INFINITY,
      refetchOnWindowFocus: false,
    }
  );

  return { ...defaultFeatureFlags, ...clientConfig };
};

export const useGetChallengeState = (challengeId, intervalMs) =>
  useQuery(
    [QUERY_KEYS.CHALLENGE, challengeId],
    () => getChallengeState(challengeId),
    {
      refetchInterval: intervalMs,
      enabled: !!challengeId,
    }
  );

export const useQueryMailChange = () =>
  useQuery(
    QUERY_KEYS.IS_MAIL_CHALLENGE_IN_PROGRESS,
    () =>
      isEmailUpdatePending()
        .then(() => true)
        .catch(() => false),
    { cacheTime: 0 }
  );

export const useGetEmployees = groupId =>
  useQuery([QUERY_KEYS.EMPLOYEES, groupId], () => getEmployees(groupId));

export const useGetPaymentOperator = (operatorId, options) =>
  useQuery(QUERY_KEYS.PAYMENT_OPERATOR, () => getPaymentOperator(operatorId), {
    ...options,
  });

export const useGetPaymentLocationGroup = (locationGroupId, options) =>
  useQuery(
    [QUERY_KEYS.PAYMENT_LOCATION_GROUP, locationGroupId],
    () => getLocationGroup(locationGroupId),
    options
  );
export const useGetPaymentLocationGroups = options =>
  useQuery(
    [QUERY_KEYS.PAYMENT_LOCATION_GROUPS],
    () => getLocationGroups(),
    options
  );

export const useGetParticipatedCampaigns = locationGroupId =>
  useQuery([QUERY_KEYS.CAMPAIGNS, locationGroupId], () =>
    getParticipatedCampaigns(locationGroupId)
  );

export const useGetDisplayPaymentModal = (operatorId, options) =>
  useQuery(
    [QUERY_KEYS.PAYMENT_DISPLAY_INFO_MODAL, operatorId],
    () => getDisplayPaymentModal(operatorId),
    options
  );

export const useGetPOSSytems = locationGroupId => {
  useQuery([QUERY_KEYS.POS_SYSTEMS, locationGroupId], () =>
    getPOSSystems(locationGroupId)
  );
};

export const useGetLocationGroupPayments = (locationGroupId, cursor) =>
  useQuery([QUERY_KEYS.LOCATION_GROUP_PAYMENTS], () =>
    getLocationGroupPayments(locationGroupId, cursor)
  );

export const useGetLucaCampaigns = () =>
  useQuery(QUERY_KEYS.ALL_LUCA_CAMPAIGNS, () => getLucaCampaigns());

export const useGetLocationGroupTipTopUpAmount = locationGroupId =>
  useQuery(QUERY_KEYS.LOCATION_GROUP_TIP_TOP_UP_AMOUNT, () =>
    getLocationGroupTipTopUpAmount(locationGroupId)
  );
