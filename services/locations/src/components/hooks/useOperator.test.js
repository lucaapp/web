/* eslint-disable sonarjs/no-duplicate-string */
import React from 'react';

import { render } from 'utils/testing';
import { useOperator } from './useOperator';

function setup() {
  const returnValue = {};

  function TestComponent() {
    Object.assign(returnValue, useOperator());

    return null;
  }

  render(<TestComponent />);

  return returnValue;
}

describe('useOperator /', () => {
  let operator;

  beforeEach(() => {
    operator = setup();
  });

  it('should return an object', () => {
    expect(operator).toBeInstanceOf(Object);
  });

  describe('Object structure', () => {
    it('should contain data property as object', () => {
      expect(operator.data).toBeInstanceOf(Object);
    });

    describe('hasError property /', () => {
      it('should be a function', () => {
        expect(operator.hasErrors).toBeInstanceOf(Function);
      });

      describe('if is called', () => {
        it('should return false if has no errors', () => {
          expect(operator.hasErrors()).toBe(false);
        });
      });
    });

    describe('isLoading property /', () => {
      it('should be a function', () => {
        expect(operator.isLoading).toBeInstanceOf(Function);
      });

      describe('if is called', () => {
        it('should return false if is not loading', () => {
          expect(operator.isLoading()).toBe(false);
        });
      });
    });

    describe('hasExpired property /', () => {
      it('should be a function', () => {
        expect(operator.hasExpired).toBeInstanceOf(Function);
      });

      describe('if is called', () => {
        it('should return false if user is not expired', () => {
          expect(operator.hasExpired()).toBe(false);
        });
      });
    });

    describe('isLoadingOrFailed property /', () => {
      it('should be a function', () => {
        expect(operator.isLoadingOrFailed).toBeInstanceOf(Function);
      });

      describe('if  is called', () => {
        it('should return false if user is not loading or has failed', () => {
          expect(operator.isLoadingOrFailed()).toBe(false);
        });
      });
    });

    describe('logOut property /', () => {
      it('should be a function', () => {
        expect(operator.logOut).toBeInstanceOf(Function);
      });

      describe('if is called', () => {
        it('should return false if user is not loading or has failed', () => {
          expect(operator.logOut()).toBe(undefined);
        });
      });
    });
  });
});
