/* eslint-disable sonarjs/no-duplicate-string */
/* eslint-disable sonarjs/no-identical-functions */
/* eslint-disable unicorn/prevent-abbreviations */
import { setupCustomHooks } from 'utils/setupCustomHooks';
import { useRedirectTo } from './useRedirectTo';

jest.mock('react-router', () => ({
  ...jest.requireActual('react-router'),
  useLocation: () => ({
    search: '?redirectTo=/a/b',
    pathname: '/pathname',
  }),
}));

describe('useRedirectTo /', () => {
  let hooks;

  beforeEach(() => {
    hooks = setupCustomHooks([{ fn: useRedirectTo, args: ['route'] }]);
  });

  it('should return an object', () => {
    expect(hooks.useRedirectTo).toBeInstanceOf(Object);
    expect(hooks.useRedirectTo).toHaveProperty('redirectToLocation');
    expect(hooks.useRedirectTo).toHaveProperty('redirectToSearchLocation');
    expect(hooks.useRedirectTo).toHaveProperty('withQueryParameters');
  });

  describe('redirectToLocation /', () => {
    describe('if is called', () => {
      it('should return a string', () => {
        const result = hooks.useRedirectTo.redirectToLocation();

        expect(result).toEqual(expect.stringContaining('route'));
      });

      it('should return a string starting with the passed route as argument', () => {
        const result = hooks.useRedirectTo.redirectToLocation();

        expect(result).toMatch(/^route/);
      });
    });
  });

  describe('redirectToSearchLocation /', () => {
    describe('if is called', () => {
      it('should return a non empty string', () => {
        const result = hooks.useRedirectTo.redirectToSearchLocation();

        expect(result).not.toBe('');
      });

      it('should match with the string from search', () => {
        const result = hooks.useRedirectTo.redirectToSearchLocation();

        expect(result).toEqual('/a/b');
      });
    });
  });

  describe('withQueryParameters /', () => {
    describe('if is called', () => {
      it('should return a string', () => {
        const result = hooks.useRedirectTo.withQueryParameters();

        expect(result).toEqual(
          expect.stringContaining('route?redirectTo=/pathname')
        );
      });

      it('should return a string starting with the passed route as argument', () => {
        const result = hooks.useRedirectTo.withQueryParameters({
          a: 'a',
          b: 'b',
        });

        const queryParameters = result.replace(/.*\?(.*)$/, '$1');
        const decoded = decodeURIComponent(queryParameters);

        expect(decoded).toEqual('a=a&b=b');
        expect(result).toMatch(/^route/);
      });
    });
  });
});
