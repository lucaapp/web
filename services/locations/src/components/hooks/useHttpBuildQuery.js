import { useMemo } from 'react';

export const useHttpBuildQuery = () => {
  const hasQueryParameters = useMemo(
    () => values => {
      const parameters = Array.from(values);

      const { length } = parameters.filter(parameter => parameter !== '');

      return length > 0;
    },
    []
  );

  return data => {
    const searchParameters = new URLSearchParams(data);

    const parameters = hasQueryParameters(searchParameters.values());
    if (!parameters) {
      return '';
    }

    const encodedURI = encodeURIComponent(searchParameters.toString());

    return `?${encodedURI}`;
  };
};
