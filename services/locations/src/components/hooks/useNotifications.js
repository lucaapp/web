import { notification } from 'antd';
import { useIntl } from 'react-intl';

export const useNotifications = () => {
  const intl = useIntl();

  const successMessage = (description, message) =>
    notification.success({
      message: intl.formatMessage({
        id: message ?? 'general.notification.successTitle',
      }),
      description:
        description &&
        intl.formatMessage({
          id: description,
        }),
      className: 'generalNotificationSuccess',
    });

  const errorMessage = (description, message) =>
    notification.error({
      message: intl.formatMessage({
        id: message ?? 'general.notification.errorTitle',
      }),
      description:
        description &&
        intl.formatMessage({
          id: description,
        }),
    });

  const infoMessage = (description, message) =>
    notification.info({
      message: intl.formatMessage({
        id: message ?? 'general.notification.infoTitle',
      }),
      description:
        description &&
        intl.formatMessage({
          id: description,
        }),
    });
  return { successMessage, errorMessage, infoMessage };
};
