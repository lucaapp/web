import { useQueryClient } from 'react-query';

import { clearHasSeenPrivateKeyModal } from 'utils/storage';
import { usePrivateKey } from 'utils/privateKey';
import { useUser } from 'store-context/selectors';

export const useOperator = () => {
  const queryClient = useQueryClient();

  const [, clearPrivateKey] = usePrivateKey();
  const { error, isLoading: userIsLoading, data } = useUser();

  const hasErrors = () => error;
  const isLoading = () => userIsLoading;
  const hasExpired = () => error && !userIsLoading;
  const isLoadingOrFailed = () => userIsLoading || error;

  const logOut = () => {
    queryClient.clear();

    clearPrivateKey(null);
    clearHasSeenPrivateKeyModal();
  };

  return {
    data,
    hasErrors,
    isLoading,
    isLoadingOrFailed,
    logOut,
    hasExpired,
  };
};
