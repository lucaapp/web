import { useEffect, useState } from 'react';

import { createChallenge } from 'network/api';
import { useGetChallengeState } from 'components/hooks/useQueries';

const challengeStates = state => ({
  authPinRequired: state === 'AUTHENTICATION_PIN_REQUIRED',
  canceled: state === 'CANCELED',
  done: state === 'DONE',
  privateKeyPinRequired: state === 'PRIVATE_KEY_PIN_REQUIRED',
  privateKeyRequired: state === 'PRIVATE_KEY_REQUIRED',
});

export const useChallenge = (intervalMs = 750) => {
  const [challengeUuid, setChallengeUuid] = useState(null);

  const { data: challenge } = useGetChallengeState(challengeUuid, intervalMs);

  useEffect(() => {
    if (challengeUuid) return;

    const generateChallendeUid = async () => {
      const { uuid } = await createChallenge();

      setChallengeUuid(uuid);
    };

    generateChallendeUid();
  }, [challengeUuid]);

  const challengeState = challengeStates(challenge?.state);

  return { challengeId: challengeUuid, challenge, challengeState };
};
