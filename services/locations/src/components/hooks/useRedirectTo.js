import queryString from 'query-string';
import { useLocation } from 'react-router';

import { useHttpBuildQuery } from './useHttpBuildQuery';

const REDIRECT_TO = 'redirectTo';

export const useRedirectTo = route => {
  const { search, pathname } = useLocation();
  const httpQueryBuilder = useHttpBuildQuery();

  const redirectToSearchLocation = () => queryString.parse(search)[REDIRECT_TO];

  const redirectToLocation = () => `${route}?${REDIRECT_TO}=${pathname}`;

  const withQueryParameters = queryParameters => {
    return `${redirectToLocation()}${httpQueryBuilder(queryParameters)}`;
  };

  return {
    redirectToLocation,
    redirectToSearchLocation,
    withQueryParameters,
  };
};
