import { useState, useCallback } from 'react';

export const useHoverState = () => {
  const [isHovered, setIsHovered] = useState(false);
  const onPointerEnter = useCallback(event => {
    if ('pointerType' in event && event.pointerType === 'mouse') {
      setIsHovered(true);
    }
  }, []);
  const onPointerLeave = useCallback(() => {
    setIsHovered(false);
  }, []);
  return {
    isHovered,
    onPointerEnter,
    onPointerLeave,
  };
};
