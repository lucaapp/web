/* eslint-disable sonarjs/no-identical-functions */
/* eslint-disable unicorn/prevent-abbreviations */
import { setupCustomHooks } from 'utils/setupCustomHooks';
import { useHttpBuildQuery } from './useHttpBuildQuery';

describe('useHttpBuildQuery /', () => {
  let hooks;

  beforeEach(() => {
    hooks = setupCustomHooks([{ fn: useHttpBuildQuery }]);
  });

  it('should return a function', () => {
    expect(hooks.useHttpBuildQuery).toBeInstanceOf(Function);
  });

  describe('if an empty object is passed to the function returned by the hook', () => {
    it('should return an empty string', () => {
      const result = hooks.useHttpBuildQuery({});

      expect(result).toBe('');
    });
  });

  describe('if a non empty object is passed to the function returned by the hook', () => {
    it('should return a non empty string', () => {
      const result = hooks.useHttpBuildQuery({ a: 'a' });

      expect(result).not.toBe('');
    });

    it('should return a string as query parameters', () => {
      const result = hooks.useHttpBuildQuery({ a: 'a', b: 'b' });

      const decoded = decodeURIComponent(result);

      expect(decoded).toEqual('?a=a&b=b');
    });
  });
});
