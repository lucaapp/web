import { useLanguage } from 'store-context/selectors';
import { useFormatCurrencyByLang } from './useFormatCurrency';

jest.mock('store-context/selectors', () => ({
  useLanguage: jest.fn(),
}));

describe('Format currency validation', () => {
  it('DE language selected', () => {
    useLanguage.mockImplementation(() => ({ currentLanguage: 'de' }));
    const { formatCurrencyByLang } = useFormatCurrencyByLang();
    const amount = 1.23;
    expect(formatCurrencyByLang(amount)).toBe(`${amount} €`);
  });

  it('EN language selected', () => {
    useLanguage.mockImplementation(() => ({ currentLanguage: 'en' }));
    const { formatCurrencyByLang } = useFormatCurrencyByLang();
    const amount = 200;
    expect(formatCurrencyByLang(amount)).toBe(`€${amount.toFixed(2)}`);
  });
});
