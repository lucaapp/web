import { useCallback, useEffect, useState } from 'react';

import { isSupportedLanguage } from 'utils/language';
import { getLanguage, saveLanguage } from 'services';
import { updateOperator } from 'network/api';

export const useLanguage = languageOverwrite => {
  const defaultLanguage = getLanguage();
  const [currentLanguage, setCurrentLanguage] = useState(() => defaultLanguage);

  const saveSelectedLanguage = (language, callback) => {
    setCurrentLanguage(previousLanguage => {
      if (previousLanguage === language || !isSupportedLanguage(language))
        return previousLanguage;

      callback(language);

      return language;
    });
  };

  useEffect(() => {
    if (!languageOverwrite) return;

    saveSelectedLanguage(languageOverwrite, language => saveLanguage(language));
  }, [languageOverwrite]);

  const changeLanguage = useCallback(selectedLanguage => {
    saveSelectedLanguage(selectedLanguage, language => {
      try {
        updateOperator({ languageOverwrite: language });
      } catch (error) {
        console.log(error);
      }
    });
  }, []);

  return {
    changeLanguage,
    currentLanguage,
  };
};
