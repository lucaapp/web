import { useLanguage } from 'store-context/selectors';

export const useFormatCurrencyByLang = () => {
  const { currentLanguage } = useLanguage();

  const formatCurrencyByLang = amount => {
    const parsedAmount = parseFloat(amount).toFixed(2);

    return currentLanguage === 'de' ? `${parsedAmount} €` : `€${parsedAmount}`;
  };

  return { formatCurrencyByLang };
};
