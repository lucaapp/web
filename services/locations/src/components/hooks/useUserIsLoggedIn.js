import { useHistory } from 'react-router-dom';

import { useUser } from 'store-context/selectors';
import { BASE_GROUP_ROUTE } from 'constants/routes';

export const useUserIsLoggedIn = () => {
  const history = useHistory();

  const { data: getCurrentUser, isFetched } = useUser();

  if (getCurrentUser) {
    history.push(BASE_GROUP_ROUTE);

    return true;
  }

  return !getCurrentUser && !isFetched;
};
