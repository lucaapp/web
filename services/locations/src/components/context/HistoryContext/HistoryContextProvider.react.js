import React, { useEffect, useMemo, useState } from 'react';
import { useLocation } from 'react-router';

import { HistoryContext } from './HistoryContext.react';

export const HistoryContextProvider = ({ children }) => {
  const { pathname } = useLocation();

  const [lastVisitedLocationGroup, setLastVisitedLocationGroup] = useState(
    null
  );

  const matchLocationGroup = path => {
    const match = path.match(/^\/app\/group\/([\w-]+)\/location\/([\w-]+)/i);

    return path === match?.[0];
  };

  const isLocationGroup = useMemo(() => matchLocationGroup(pathname), [
    pathname,
  ]);

  useEffect(() => {
    if (!isLocationGroup) return;

    setLastVisitedLocationGroup(pathname);
  }, [pathname, isLocationGroup]);

  const values = { lastVisitedLocationGroup };

  return (
    <HistoryContext.Provider value={values}>{children}</HistoryContext.Provider>
  );
};
