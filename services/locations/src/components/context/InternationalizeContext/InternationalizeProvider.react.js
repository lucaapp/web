import React from 'react';

import { IntlProvider } from 'react-intl';
import { messages } from 'messages';
import { useLanguage } from 'store-context/selectors';

export const InternationalizeProvider = ({ children }) => {
  const { currentLanguage } = useLanguage();

  return (
    <IntlProvider
      locale={currentLanguage}
      messages={messages[currentLanguage]}
      wrapRichTextChunksInFragment
    >
      {children}
    </IntlProvider>
  );
};
