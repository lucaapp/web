import React from 'react';
import { useIntl } from 'react-intl';
import { Form, notification } from 'antd';
import { useHistory } from 'react-router';
import { useLocation } from 'react-router-dom';

import { useCampaign } from 'utils/campaign';
import { IS_MOBILE } from 'constants/environment';
import { LOGIN_ROUTE } from 'constants/routes';
import { forgotPassword } from 'network/api';
import { SiteHeader } from 'components/general';
import { Header } from 'components/Header';
import { useNotifications } from 'components/hooks';
import { Footer } from '../Authentication/Footer';

import {
  AuthenticationWrapper,
  AuthenticationCard,
  ButtonWrapper,
  CardTitle,
  StyledInput,
  StyledSecondaryButton,
  StyledWhiteButton,
  CardSubTitle,
} from '../Authentication/Authentication.styled';
import { Wrapper } from './ForgotPassword.styled';

export const ForgotPassword = () => {
  const intl = useIntl();
  const history = useHistory();
  const queryParameters = useLocation().search;
  const campaign = useCampaign(queryParameters);
  const { errorMessage } = useNotifications();
  const onRedirect = () => history.push(`${LOGIN_ROUTE}${queryParameters}`);

  const onFinish = values => {
    forgotPassword({ email: values.email, ...(campaign ? { campaign } : {}) })
      .then(({ status }) => {
        if (status === 204) {
          notification.success({
            message: intl.formatMessage({
              id: 'notification.forgotPassword.success',
            }),
          });

          history.push(
            `${LOGIN_ROUTE}${
              campaign ? `?${new URLSearchParams(campaign).toString()}` : ''
            }`
          );

          return;
        }

        errorMessage('notification.forgotPassword.error');
      })
      .catch(() => {
        errorMessage('notification.network.error');
      });
  };

  return (
    <>
      <SiteHeader
        title={intl.formatMessage({ id: 'forgotPassword.site.title' })}
        meta={intl.formatMessage({ id: 'forgotPassword.site.meta' })}
      />
      <Wrapper id={IS_MOBILE ? 'isMobile' : ''} data-cy="forgotPasswordPage">
        <Header title={intl.formatMessage({ id: 'header.subtitle' })} />
        <AuthenticationWrapper id="noSteps">
          <AuthenticationCard>
            <CardTitle>
              {intl.formatMessage({
                id: 'forgotPassword.title',
              })}
            </CardTitle>
            <CardSubTitle>
              {intl.formatMessage({ id: 'forgotPassword.site.description' })}
            </CardSubTitle>
            <Form onFinish={onFinish}>
              <Form.Item
                colon={false}
                label={intl.formatMessage({
                  id: 'registration.form.email',
                })}
                name="email"
                rules={[
                  {
                    type: 'email',
                    message: intl.formatMessage({
                      id: 'error.email',
                    }),
                  },
                  {
                    required: true,
                    message: intl.formatMessage({
                      id: 'error.email',
                    }),
                  },
                ]}
              >
                <StyledInput
                  autoFocus
                  autoComplete="username"
                  style={{
                    border: '1px solid #696969',
                    backgroundColor: 'transparent',
                  }}
                />
              </Form.Item>
              <ButtonWrapper multipleButtons>
                <StyledSecondaryButton onClick={onRedirect}>
                  {intl.formatMessage({
                    id: 'authentication.returnToLogin',
                  })}
                </StyledSecondaryButton>
                <StyledWhiteButton
                  htmlType="submit"
                  data-cy="sentResetLinkSubmit"
                >
                  {intl.formatMessage({
                    id: 'forgotPassword.form.button',
                  })}
                </StyledWhiteButton>
              </ButtonWrapper>
            </Form>
          </AuthenticationCard>
          <Footer />
        </AuthenticationWrapper>
      </Wrapper>
    </>
  );
};
