import styled from 'styled-components';
import { Media } from 'utils/media';

export const Wrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  background-color: #262626;
`;

export const ForgotPasswordWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100%;

  ${Media.mobile`
    flex-direction: column;
  `}
`;

export const Headline = styled.h1`
  font-size: 42px;
  margin-bottom: 32px;
`;

export const Title = styled.div`
  font-size: 24px;
  margin-bottom: 16px;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 24px;

  ${Media.mobile`
    margin-top: 40px;  
  `}
`;

export const buttonStyle = {
  fontFamily: 'Montserrat-Bold, sans-serif',
  fontSize: 14,
  fontWeight: 'bold',
  padding: '0 40px',
};
