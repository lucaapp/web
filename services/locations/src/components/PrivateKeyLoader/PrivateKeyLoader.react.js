import React, { useState } from 'react';
import { Steps } from 'antd';

import { updateLastSeenPrivateKey } from 'network/api';

import { KeyInput } from './KeyInput';
import { ResetKey } from './ResetKey';
import { ConfirmationPrivateKey } from '../App/modals/ConfirmationPrivateKey';

export const PrivateKeyLoader = ({
  onSuccess = () => {},
  onError = () => {},
  infoTextId = 'privateKey.modal.info',
  footerItem = null,
  remainingDaysToUploadKey,
  privateKeySecret,
  showSuccessScreen,
}) => {
  const [isResetKey, setIsResetKey] = useState(false);
  const [currentStep, setCurrentStep] = useState(0);

  const nextStep = () => setCurrentStep(currentStep + 1);

  const handleSuccess = parameter => {
    updateLastSeenPrivateKey()
      .then(() => {
        onSuccess(parameter);

        if (showSuccessScreen) {
          nextStep();
        }
      })
      // eslint-disable-next-line no-console
      .catch(error => console.error(error));
  };

  const steps = [
    {
      id: '0',
      content: (
        <KeyInput
          footerItem={footerItem}
          infoTextId={infoTextId}
          onSuccess={handleSuccess}
          onError={onError}
          remainingDaysToUploadKey={remainingDaysToUploadKey}
          setIsResetKey={setIsResetKey}
        />
      ),
    },
    {
      id: '1',
      content: <ConfirmationPrivateKey />,
    },
  ];

  const conditionalKeyInput = () => {
    if (showSuccessScreen) {
      return (
        <div>
          <Steps progressDot={() => null} current={currentStep}>
            {steps.map(step => (
              <Steps.Step key={step.id} />
            ))}
          </Steps>
          {steps[currentStep].content}
        </div>
      );
    }

    return (
      <KeyInput
        footerItem={footerItem}
        infoTextId={infoTextId}
        onSuccess={handleSuccess}
        onError={onError}
        remainingDaysToUploadKey={remainingDaysToUploadKey}
        setIsResetKey={setIsResetKey}
      />
    );
  };

  return (
    <>
      {isResetKey ? (
        <ResetKey
          back={() => setIsResetKey(false)}
          privateKeySecret={privateKeySecret}
        />
      ) : (
        conditionalKeyInput()
      )}
    </>
  );
};
