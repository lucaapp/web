import styled from 'styled-components';

export const UploadProgress = styled.div`
  display: flex;
  width: 200px;
`;

export const UploadMessage = styled.p`
  font-size: 14px;
  font-weight: 500;
  text-align: center;
`;
