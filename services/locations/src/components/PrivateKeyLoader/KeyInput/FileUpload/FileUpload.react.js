import React from 'react';
import { useIntl } from 'react-intl';
import { Progress, Upload } from 'antd';

import { base64ToHex, EC_KEYPAIR_FROM_PRIVATE_KEY } from '@lucaapp/crypto';
import { PrimaryButton } from 'components/general';

import { MAX_PRIVATE_KEY_FILE_SIZE } from 'constants/valueLength';
import { parsePrivateKeyFile } from 'utils/privateKey';

import { useOperator } from 'components/hooks';
import { uploadMessages, statusProgress } from '../../PrivateKeyLoader.helper';

import { UploadMessage, UploadProgress } from './FileUpload.styled';

export const FileUpload = ({
  onSuccess,
  setProgressPercent,
  setUploadStatus,
  setUploadMessageId,
  setPrivateKey,
  privateKeySecret,
  progressPercent,
  uploadStatus,
  uploadMessageId,
}) => {
  const intl = useIntl();
  const { data: operator } = useOperator();

  const setStatus = (percent, exception, messageId) => {
    setProgressPercent(percent);
    setUploadStatus(exception);
    setUploadMessageId(messageId);
  };

  const processPrivateKey = (privateKey, fileData) => {
    try {
      const keyPair = EC_KEYPAIR_FROM_PRIVATE_KEY(privateKey);
      if (keyPair?.publicKey !== base64ToHex(operator.publicKey)) {
        setStatus(100, statusProgress.exception, uploadMessages.error);
        return;
      }
      setStatus(100, statusProgress.success, uploadMessages.done);
      setPrivateKey(fileData);
      onSuccess(privateKey);
    } catch {
      setStatus(100, statusProgress.exception, uploadMessages.error);
    }
  };

  const onFile = ({ file }) => {
    if (!file) return;

    if (file.size > MAX_PRIVATE_KEY_FILE_SIZE) {
      setStatus(100, statusProgress.exception, uploadMessages.size);
      return;
    }

    const reader = new FileReader();
    reader.addEventListener('load', event => {
      const stringValue = event.target.result;
      const privateKey = parsePrivateKeyFile(stringValue, privateKeySecret);
      processPrivateKey(privateKey, stringValue);
    });

    reader.readAsText(file);
  };

  return (
    <Upload
      type="file"
      accept=".luca"
      customRequest={onFile}
      showUploadList={false}
    >
      <UploadMessage>
        {intl.formatMessage({ id: uploadMessageId })}
      </UploadMessage>
      {progressPercent <= 0 && (
        <PrimaryButton>
          {intl.formatMessage({ id: 'shareData.privateKey.btnLabel' })}
        </PrimaryButton>
      )}
      {progressPercent > 0 && (
        <UploadProgress>
          <Progress
            percent={progressPercent}
            status={uploadStatus}
            trailColor="#fff"
          />
        </UploadProgress>
      )}
    </Upload>
  );
};
