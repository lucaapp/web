import styled from 'styled-components';
import { Button } from 'antd';

export const FinishButtonWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 40px;
`;

export const LinkButton = styled(Button)`
  border: none;
  box-shadow: none;
  background-color: transparent;
  color: rgb(80, 102, 124);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 12px;
  font-weight: bold;
  padding-left: 0;
  :hover {
    border: none;
    box-shadow: none;
    background: none;
  }
`;
