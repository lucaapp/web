import React from 'react';
import { useIntl } from 'react-intl';

import { SecondaryButton } from 'components/general';

import { uploadMessages, statusProgress } from '../../PrivateKeyLoader.helper';

import { FinishButtonWrapper, LinkButton } from './ButtonRow.styled';

export const ButtonRow = ({
  footerItem,
  setProgressPercent,
  setUploadStatus,
  uploadStatus,
  setUploadMessageId,
  setIsResetKey,
}) => {
  const intl = useIntl();

  const setStatus = (percent, exception, messageId) => {
    setProgressPercent(percent);
    setUploadStatus(exception);
    setUploadMessageId(messageId);
  };

  const reset = () => {
    setStatus(0, statusProgress.initial, uploadMessages.initial);
  };

  return (
    <FinishButtonWrapper>
      <LinkButton onClick={() => setIsResetKey(true)}>
        {intl.formatMessage({ id: 'keyUpload.lostKey' })}
      </LinkButton>
      {uploadStatus !== statusProgress.exception ? (
        footerItem
      ) : (
        <SecondaryButton onClick={reset}>
          {intl.formatMessage({ id: 'shareData.tryAgain' })}
        </SecondaryButton>
      )}
    </FinishButtonWrapper>
  );
};
