import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Steps } from 'antd';
import { useQueryClient } from 'react-query';
import { hexToBase64 } from '@lucaapp/crypto';
import { QUERY_KEYS, useNotifications } from 'components/hooks';
import { usePrivateKey } from 'utils/privateKey';

import { updateLastSeenPrivateKey, updatePublicKey } from 'network/api';
import { DownloadPrivateKey } from 'components/App/modals/RegisterOperatorModal/steps/DownloadPrivateKey';
import { VerifyPrivateKey } from 'components/App/modals/RegisterOperatorModal/steps/VerifyPrivateKey';
import { ConfirmationPrivateKey } from 'components/App/modals/ConfirmationPrivateKey';
import { ResetKeyConfirmation } from './ResetKeyConfirmation';
import { Title } from './ResetKey.styled';

export const ResetKey = ({ back, privateKeySecret }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const [publicKey, setPublicKey] = useState(null);
  const [password, setPassword] = useState(null);
  const [currentStep, setCurrentStep] = useState(0);
  const [, clearPrivateKey] = usePrivateKey();
  const { errorMessage } = useNotifications();

  const nextStep = () => setCurrentStep(currentStep + 1);
  const previousStep = () => setCurrentStep(currentStep - 1);

  const handleResponse = async response => {
    if (response.status === 204) {
      // wait for state to be updated before closing modal
      await queryClient.invalidateQueries(QUERY_KEYS.ME);
      await updateLastSeenPrivateKey();
      nextStep();
    } else if (response.status === 403) {
      errorMessage('notification.resetKey.incorrectPassword');

      setCurrentStep(0);
      clearPrivateKey();
    } else {
      errorMessage('notification.resetKey.error');

      clearPrivateKey();
    }
  };

  const onConfirmKey = () => {
    updatePublicKey({
      publicKey,
      password,
      privateKeySecret: hexToBase64(privateKeySecret),
    })
      .then(response => {
        handleResponse(response);
      })
      .catch(() => {
        clearPrivateKey();
      });
  };

  const steps = [
    {
      id: '0',
      title: 'modal.resetKey.title',
      content: (
        <ResetKeyConfirmation
          next={nextStep}
          back={back}
          setPassword={setPassword}
        />
      ),
    },
    {
      id: '1',
      title: 'modal.registerOperator.title',
      content: (
        <DownloadPrivateKey
          privateKeySecret={privateKeySecret}
          next={nextStep}
          back={previousStep}
          hasBack
          setPublicKey={setPublicKey}
        />
      ),
    },
    {
      id: '2',
      title: 'modal.registerOperator.keyTest',
      content: (
        <VerifyPrivateKey
          privateKeySecret={privateKeySecret}
          publicKey={publicKey}
          back={previousStep}
          confirmKey={onConfirmKey}
        />
      ),
    },
    {
      id: '3',
      content: <ConfirmationPrivateKey />,
    },
  ];

  return (
    <div>
      {steps[currentStep].title && (
        <Title>{intl.formatMessage({ id: steps[currentStep].title })}</Title>
      )}
      <Steps progressDot={() => null} current={currentStep}>
        {steps.map(step => (
          <Steps.Step key={step.id} />
        ))}
      </Steps>
      {steps[currentStep].content}
    </div>
  );
};
