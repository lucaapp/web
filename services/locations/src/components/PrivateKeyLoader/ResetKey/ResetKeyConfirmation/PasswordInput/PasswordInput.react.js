import React from 'react';
import { Form } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { useIntl } from 'react-intl';
import { getRequiredRule } from 'utils/antFormRules';
import { StyledPasswordInput } from './PasswordInput.styled';

export const PasswordInput = () => {
  const intl = useIntl();

  return (
    <Form.Item
      colon={false}
      label={intl.formatMessage({
        id: 'registration.form.password',
      })}
      name="password"
      rules={[getRequiredRule(intl, 'password')]}
    >
      <StyledPasswordInput
        data-cy="resetPrivateKey-passwordInputField"
        autoComplete="current-password"
        iconRender={visible =>
          visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
        }
      />
    </Form.Item>
  );
};
