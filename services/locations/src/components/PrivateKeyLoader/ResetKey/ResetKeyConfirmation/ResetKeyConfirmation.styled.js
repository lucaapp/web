import styled from 'styled-components';

export const InfoBlock = styled.p`
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 21px;
`;

export const RequestContent = styled.div`
  margin-bottom: 40px;
`;

export const ButtonRow = styled.div`
  margin-top: 40px;
  display: flex;
  justify-content: space-between;
`;
