import React from 'react';
import { useIntl } from 'react-intl';

import { Form } from 'antd';

import { PrimaryButton, SecondaryButton } from 'components/general';
import {
  InfoBlock,
  RequestContent,
  ButtonRow,
} from './ResetKeyConfirmation.styled';
import { PasswordInput } from './PasswordInput';
import { ConfirmCheckbox } from './ConfirmCheckbox';

export const ResetKeyConfirmation = ({ next, back, setPassword }) => {
  const intl = useIntl();

  const onFinish = ({ password }) => {
    setPassword(password);
    next();
  };

  return (
    <RequestContent>
      <InfoBlock data-cy="resetPrivateKeyInfo">
        {intl.formatMessage({ id: 'resetKey.confirmationText' })}
      </InfoBlock>
      <Form onFinish={onFinish}>
        <ConfirmCheckbox />
        <InfoBlock>
          {intl.formatMessage({ id: 'resetKey.passwordRequired' })}
        </InfoBlock>
        <PasswordInput />
        <ButtonRow
          style={{ justifyContent: back ? 'space-between' : 'flex-end' }}
        >
          {back && (
            <SecondaryButton onClick={back}>
              {intl.formatMessage({ id: 'resetKey.back' })}
            </SecondaryButton>
          )}
          <PrimaryButton
            data-cy="resetPrivateKeySubmitButton"
            htmlType="submit"
          >
            {intl.formatMessage({ id: 'resetKey.createKey' })}
          </PrimaryButton>
        </ButtonRow>
      </Form>
    </RequestContent>
  );
};
