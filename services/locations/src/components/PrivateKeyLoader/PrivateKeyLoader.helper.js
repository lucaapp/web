export const uploadMessages = {
  initial: 'shareData.privateKey.uploadMessage',
  size: 'shareData.privateKey.keySize',
  inProgress: 'shareData.privateKey.uploadAction',
  error: 'shareData.privateKey.errorMessage',
  done: 'shareData.privateKey.done',
};

export const statusProgress = {
  initial: '',
  success: 'success',
  exception: 'exception',
};
