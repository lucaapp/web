import React from 'react';
import { useIntl } from 'react-intl';
import { Layout } from 'antd';
import { Content, NavigationButton, Sider } from 'components/general';

import { ArchiveContent } from './ArchiveContent';
import { Header, Wrapper } from './Archive.styled';

export const Archive = () => {
  const intl = useIntl();

  return (
    <Layout>
      <Sider>
        <NavigationButton />
      </Sider>
      <Content>
        <Wrapper>
          <Header>{intl.formatMessage({ id: 'header.archive' })}</Header>
          <ArchiveContent />
        </Wrapper>
      </Content>
    </Layout>
  );
};
