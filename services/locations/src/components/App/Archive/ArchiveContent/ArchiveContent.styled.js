import styled from 'styled-components';

export const StyledCard = styled.div`
  background: rgb(255, 255, 255);
  border-radius: 8px;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.15);
  padding: 24px 32px;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const Info = styled.div`
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 40px;
`;
