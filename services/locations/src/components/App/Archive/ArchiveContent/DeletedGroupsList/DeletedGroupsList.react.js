import React from 'react';

import { useDeletedGroups } from 'components/hooks';
import { EmptyDeletedGroups } from './EmptyDeletedGroups';
import { DeletedGroupsTable } from './DeletedGroupsTable';

import { Wrapper } from './DeletedGroupsList.styled';

export const DeletedGroupsList = () => {
  const { isLoading, error, data: deletedGroups } = useDeletedGroups();

  if (isLoading || error) return null;

  const userHasNoDeletedGroups = deletedGroups.length === 0;
  return (
    <Wrapper>
      {userHasNoDeletedGroups ? (
        <EmptyDeletedGroups />
      ) : (
        <DeletedGroupsTable deletedGroups={deletedGroups} />
      )}
    </Wrapper>
  );
};
