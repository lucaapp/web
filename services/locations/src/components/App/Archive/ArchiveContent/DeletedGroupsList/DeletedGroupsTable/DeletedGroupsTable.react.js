import React from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';

import { reactivateGroup } from 'network/api';
import { QUERY_KEYS, useNotifications } from 'components/hooks';

import { getFormattedDate } from 'utils/formatters';
import { PrimaryButton } from 'components/general';

import { StyledTable, Wrapper } from './DeletedGroupsTable.styled';

export const DeletedGroupsTable = ({ deletedGroups }) => {
  const intl = useIntl();
  const { successMessage, errorMessage } = useNotifications();
  const queryClient = useQueryClient();

  const sortedDeletedGroups = deletedGroups.sort(
    (a, b) => b.availableUntil - a.availableUntil
  );

  const showError = () => errorMessage('notification.deleteGroup.error');

  const reactivate = groupId => {
    reactivateGroup(groupId)
      .then(response => {
        if (response.status === 204) {
          queryClient.invalidateQueries(QUERY_KEYS.DELETED_GROUPS);
          successMessage('notification.reactivateGroup.success');
          return;
        }
        showError();
      })
      .catch(showError);
  };

  const columns = [
    {
      title: intl.formatMessage({ id: 'archive.name' }),
      key: 'name',
      render: function renderName(deletedGroup) {
        return <div data-cy="deletedGroup">{deletedGroup.name}</div>;
      },
    },
    {
      title: intl.formatMessage({ id: 'archive.availableUntil' }),
      key: 'availableUntil',
      render: function renderAvailableUntil(deletedGroup) {
        return <div>{getFormattedDate(deletedGroup.availableUntil)}</div>;
      },
    },
    {
      title: '',
      key: 'reactivate',
      render: function renderReactivationButton(deletedGroup) {
        return (
          <Wrapper>
            <PrimaryButton
              data-cy="reactivateButton"
              onClick={() => reactivate(deletedGroup.groupId)}
            >
              {intl.formatMessage({ id: 'archive.reactivate' })}
            </PrimaryButton>
          </Wrapper>
        );
      },
    },
  ];

  return (
    <StyledTable
      columns={columns}
      dataSource={sortedDeletedGroups}
      rowKey={record => record.groupId}
      pagination={false}
    />
  );
};
