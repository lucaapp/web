import React, { useEffect } from 'react';
import { useIntercom } from 'react-use-intercom';
import { useIntl } from 'react-intl';
import { Route, Switch, Redirect, useLocation } from 'react-router';

import { useCampaign } from 'utils/campaign';

import { SiteHeader } from 'components/general';
import { PaymentOnboardingWrapper } from 'components/PaymentOnboardingWrapper';
import { SmallDeviceWrapper } from 'components/SmallDeviceWrapper';

import {
  APP_ROUTE,
  ARCHIVE_ROUTE,
  BASE_DATA_TRANSFER_ROUTE,
  DEVICES_ROUTE,
  GROUP_SETTINGS_ROUTE,
  HELP_CENTER_ROUTE,
  LOGIN_ROUTE,
  PROFILE_ROUTE,
  TABLES_ROUTE,
} from 'constants/routes';

import { useRedirectTo, useOperator } from 'components/hooks';
import { Header } from './Header';
import { Archive } from './Archive';
import { Dashboard } from './Dashboard';
import { DataTransfers } from './DataTransfers';
import { Devices } from './Devices';
import { GroupSettings } from './GroupSettings';
import { HelpCenter } from './HelpCenter';
import { TableOverview } from './TableOverview';

import { AppWrapper } from './App.styled';
import { HistoryContextProvider } from '../context/HistoryContext';
import { LocationFooter } from './LocationFooter';
import { Profile } from './Profile';

export const App = () => {
  const intl = useIntl();
  const redirectToLogin = useRedirectTo(LOGIN_ROUTE);
  const { trackEvent, update } = useIntercom();

  const {
    data: operator,
    hasExpired,
    logOut,
    isLoadingOrFailed,
  } = useOperator();
  const location = useLocation();
  const campaign = useCampaign(location.search);

  useEffect(() => {
    trackEvent('DASHBOARD');

    if (operator) {
      update({
        phone: operator.phone,
        email: operator.username,
        name: `${operator.firstName} ${operator.lastName}`,
        customAttributes: {
          companyName: operator.businessEntityName,
          operatorId: operator.operatorId,
          supportCode: operator.supportCode,
        },
      });
    }
  }, [operator, trackEvent, update]);

  if (hasExpired()) {
    logOut();

    window.location = redirectToLogin.withQueryParameters(campaign);
  }

  if (isLoadingOrFailed()) return null;

  return (
    <HistoryContextProvider>
      <PaymentOnboardingWrapper location={location}>
        <SmallDeviceWrapper>
          <SiteHeader
            title={intl.formatMessage({
              id: 'locations.site.title',
            })}
            meta={intl.formatMessage({
              id: 'locations.site.meta',
            })}
          />
          <AppWrapper>
            <Header />
            <Switch>
              <Route path={PROFILE_ROUTE}>
                <Profile />
              </Route>
              <Route path={ARCHIVE_ROUTE}>
                <Archive />
              </Route>
              <Route path={DEVICES_ROUTE}>
                <Devices />
              </Route>
              <Route path={HELP_CENTER_ROUTE}>
                <HelpCenter />
              </Route>
              <Route path={GROUP_SETTINGS_ROUTE}>
                <GroupSettings />
              </Route>
              <Route path={TABLES_ROUTE}>
                <TableOverview />
              </Route>
              <Route path={BASE_DATA_TRANSFER_ROUTE}>
                <DataTransfers />
              </Route>
              <Route path={APP_ROUTE}>
                <Dashboard />
              </Route>
              <Redirect to={APP_ROUTE} />
            </Switch>
            <LocationFooter />
          </AppWrapper>
        </SmallDeviceWrapper>
      </PaymentOnboardingWrapper>
    </HistoryContextProvider>
  );
};
