import React from 'react';
import { useIntl } from 'react-intl';

import { getFormattedDate, getFormattedTime } from 'utils/formatters';

import { useGetHealthDepartment } from 'components/hooks';
import { TableRow, TableEntry } from './Transfer.styled';

export const Transfer = ({ transfer }) => {
  const intl = useIntl();

  const { data: healthDepartment } = useGetHealthDepartment(
    transfer.departmentId
  );

  if (!healthDepartment) return null;

  return (
    <TableRow>
      <TableEntry style={{ flex: '14%' }}>
        {getFormattedDate(transfer.createdAt)}
      </TableEntry>
      <TableEntry style={{ flex: '14%' }}>
        <div>{getFormattedDate(transfer.time[0])}</div>
        <div>{getFormattedTime(transfer.time[0])}</div>
      </TableEntry>
      <TableEntry style={{ flex: '14%' }}>
        <div>{getFormattedDate(transfer.time[1])}</div>
        <div>{getFormattedTime(transfer.time[1])}</div>
      </TableEntry>
      <TableEntry style={{ flex: '28%' }}>
        <div>
          {`${transfer.groupName} - ${
            transfer.locationName ||
            intl.formatMessage({ id: 'location.defaultName' })
          }`}
        </div>
      </TableEntry>
      <TableEntry style={{ flex: '16%' }}>{healthDepartment.name}</TableEntry>
      <TableEntry style={{ flex: '14%' }}>
        <div>{getFormattedDate(transfer.approvedAt)}</div>
        <div>{getFormattedTime(transfer.approvedAt)}</div>
      </TableEntry>
    </TableRow>
  );
};
