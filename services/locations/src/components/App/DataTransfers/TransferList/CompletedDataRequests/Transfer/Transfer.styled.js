import styled from 'styled-components';

export const TableRow = styled.div`
  display: flex;
  padding: 32px 0 16px 0;
  border-bottom: 1px solid rgb(218, 224, 231);
`;

export const TableEntry = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 14px;
  font-weight: 500;
  padding: 0 8px;
`;
