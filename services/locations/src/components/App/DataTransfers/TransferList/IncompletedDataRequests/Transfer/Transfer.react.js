import { getLocationTransferTraces } from 'network/api';

import React from 'react';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';

import { getFormattedDate, getFormattedTime } from 'utils/formatters';
import { BASE_SHARE_DATA_ROUTE } from 'constants/routes';
import { SignatureDownload } from 'components/general/SignatureDownload';
import { useGetHealthDepartment } from 'components/hooks';
import { WarningButton } from 'components/general';

import { VerificationIcon } from 'assets/icons';
import {
  TransferHeader,
  TransferWrapper,
  TransferContent,
  ContentHeader,
  Content,
  ContentValue,
  ButtonWrapper,
  HealthDepartmentName,
  HealthDepartmentDate,
  HealthDepartmentFormDescription,
  HealthDepartmentDateDescription,
  StyledIcon,
  ContentSignature,
  SectionHeaderWrapper,
} from './Transfer.styled';

export const Transfer = ({ transfer }) => {
  const intl = useIntl();

  const { data: healthDepartment } = useGetHealthDepartment(
    transfer.departmentId
  );

  const { data: traces } = useQuery(['traces', transfer.uuid], () =>
    getLocationTransferTraces(transfer.uuid)
  );

  const openShareDataView = tracingProcessId => {
    window.open(`${BASE_SHARE_DATA_ROUTE}${tracingProcessId}`, '_blank');
  };

  if (!healthDepartment) return null;

  return (
    <TransferWrapper>
      <SectionHeaderWrapper>
        <TransferHeader>
          <HealthDepartmentFormDescription>
            {intl.formatMessage({
              id: 'dataTransfers.transfer.form',
            })}
          </HealthDepartmentFormDescription>
          <HealthDepartmentDateDescription>
            {intl.formatMessage({
              id: 'dataTransfers.transfer.date',
            })}
          </HealthDepartmentDateDescription>
        </TransferHeader>

        <TransferHeader>
          <HealthDepartmentName>
            {healthDepartment.name}
            <StyledIcon component={VerificationIcon} />
          </HealthDepartmentName>
          <HealthDepartmentDate>
            {getFormattedDate(transfer.createdAt)}
          </HealthDepartmentDate>
        </TransferHeader>

        <ContentSignature>
          <SignatureDownload transfers={[transfer]} />
        </ContentSignature>
      </SectionHeaderWrapper>

      <TransferContent>
        <Content
          style={{
            flex: '25%',
          }}
        >
          <ContentHeader>
            {intl.formatMessage({
              id: 'dataTransfers.transfer.dataRequestTo',
            })}
          </ContentHeader>
          <ContentValue>{transfer.groupName}</ContentValue>
        </Content>
        <Content
          style={{
            flex: '20%',
          }}
        >
          <ContentHeader>
            {intl.formatMessage({
              id: 'dataTransfers.transfer.area',
            })}
          </ContentHeader>
          <ContentValue>
            {transfer.locationName ||
              intl.formatMessage({ id: 'location.defaultName' })}
          </ContentValue>
        </Content>
        <Content
          style={{
            flex: '35%',
          }}
        >
          <ContentHeader>
            {intl.formatMessage({
              id: 'dataTransfers.transfer.period',
            })}
          </ContentHeader>
          <ContentValue>{`${getFormattedDate(
            transfer.time[0]
          )} ${getFormattedTime(transfer.time[0])} ${intl.formatMessage({
            id: 'dataTransfers.transfer.timeLabel',
          })} - `}</ContentValue>
          <ContentValue>{`${getFormattedDate(
            transfer.time[1]
          )} ${intl.formatMessage({
            id: 'dataTransfers.transfer.timeLabel',
          })}`}</ContentValue>
        </Content>
        <Content
          style={{
            flex: '20%',
          }}
        >
          <ContentHeader>
            {intl.formatMessage({
              id: 'dataTransfers.transfer.totalCheckins',
            })}
          </ContentHeader>
          <ContentValue>{traces?.length}</ContentValue>
        </Content>
      </TransferContent>
      <ButtonWrapper>
        <WarningButton
          data-cy="completeDataTransfer"
          onClick={() => openShareDataView(transfer.uuid)}
        >
          {intl.formatMessage({
            id: 'dataTransfers.transfer.completeRequest',
          })}
        </WarningButton>
      </ButtonWrapper>
    </TransferWrapper>
  );
};
