import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const TransferWrapper = styled.div`
  background-color: white;
  margin-top: 24px;
  border-radius: 8px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.15);
`;

export const SectionHeaderWrapper = styled.div`
  padding: 24px 32px 0 32px;
  border-bottom: 1px solid rgb(0, 0, 0);
`;

export const TransferHeader = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 2px;
`;

export const HealthDepartmentName = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;

export const HealthDepartmentFormDescription = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 12px;
  font-weight: 500;
`;

export const HealthDepartmentDateDescription = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 12px;
  font-weight: 500;
`;

export const HealthDepartmentDate = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;

export const TransferContent = styled.div`
  display: flex;
  padding: 24px 32px 0 32px;
`;

export const StyledIcon = styled(Icon)`
  margin-left: 8px;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

export const ContentSignature = styled.div`
  margin-bottom: 24px;
`;

export const ContentHeader = styled.div`
  font-family: Montserrat-Medium, sans-serif;
  margin-bottom: 8px;
  color: rgb(0, 0, 0);
  font-size: 12px;
  font-weight: 500;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 24px;
  padding: 0 32px 32px 32px;
`;

export const ContentValue = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;
