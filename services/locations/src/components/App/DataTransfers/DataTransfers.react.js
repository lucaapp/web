import React from 'react';
import { useIntl } from 'react-intl';
import { Layout } from 'antd';
import { SHARE_ALL_DATA_ROUTE } from 'constants/routes';

import {
  Content,
  NavigationButton,
  Sider,
  WarningButton,
} from 'components/general';

import { useGetAllTransfersWithValidity } from 'components/hooks';
import { TransferList } from './TransferList';
import {
  DataTransfersWrapper,
  Wrapper,
  Header,
  ButtonWrapper,
} from './DataTransfers.styled';

export const DataTransfers = () => {
  const intl = useIntl();

  const {
    isLoading,
    error,
    data: transfers,
  } = useGetAllTransfersWithValidity();

  const uncompletedTransfers = (transfers || [])
    .filter(transfer => !!transfer.contactedAt && !transfer.isCompleted)
    .map(transfer => transfer.uuid);

  const shareAll = () => window.open(SHARE_ALL_DATA_ROUTE);

  if (error || isLoading) return null;

  return (
    <Layout>
      <Sider>
        <NavigationButton />
      </Sider>
      <Content>
        <Wrapper data-cy="dataTransfersPageWrapper">
          <Header data-cy="dataTransfersTitle">
            {intl.formatMessage({
              id: 'shareData.title',
            })}
          </Header>
          <ButtonWrapper>
            {!!uncompletedTransfers.length && (
              <WarningButton
                onClick={shareAll}
                disabled={!uncompletedTransfers.length}
              >
                {intl.formatMessage({ id: 'shareData.shareAll' })}
              </WarningButton>
            )}
          </ButtonWrapper>
          <DataTransfersWrapper>
            <TransferList transfers={transfers} />
          </DataTransfersWrapper>
        </Wrapper>
      </Content>
    </Layout>
  );
};
