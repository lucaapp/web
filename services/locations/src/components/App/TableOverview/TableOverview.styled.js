import styled from 'styled-components';

export const Wrapper = styled.div`
  margin: 40px 80px;
  display: flex;
  overflow-x: hidden;
  flex-direction: column;
`;

export const Header = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 34px;
  font-weight: 500;
  margin-bottom: 36px;
`;

export const LocationBreadcrum = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 8px;
`;
