import { useGetLocationTables } from 'components/hooks';

export const useGetNumberedTables = locationUuid => {
  const { isLoading, error, data: locationTables } = useGetLocationTables(
    locationUuid
  );

  const numberedTables = locationTables?.map(table => ({
    ...table,
    key: table.internalNumber,
    name: table.customName ? table.customName : table.internalNumber,
  }));

  return { isLoading, error, numberedTables };
};
