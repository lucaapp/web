import React from 'react';

import { useGetLocationTables } from 'components/hooks/useQueries';
import { BasicGenerateQRCodes } from 'components/general/BasicGenerateQRCodes';

export const TableQrCodes = ({ location }) => {
  const { error, data: locationTables, isLoading } = useGetLocationTables(
    location.uuid
  );

  if (error || isLoading) return null;

  return (
    <BasicGenerateQRCodes
      location={location}
      isTableQRCodeEnabled
      locationTables={locationTables}
    />
  );
};
