import React from 'react';

import { FloorPlan } from './FloorPlan';
import { TableQrCodes } from './TableQrCodes';

export const TableOverviewContent = ({ location }) => {
  return (
    <>
      <FloorPlan location={location} />
      <TableQrCodes location={location} />
    </>
  );
};
