import styled from 'styled-components';

export const TablesCompWrapper = styled.div`
  padding: 16px 24px 24px 24px;
  background: rgb(245, 245, 244);
  border-radius: 8px;
  display: flex;
  flex-wrap: wrap;
`;

export const TableComp = styled.div`
  background: rgb(80, 102, 124);
  border-radius: 2px;
  box-shadow: -1px 2px 4px 0 rgba(0, 0, 0, 0.15);
  height: 64px;
  width: 80px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 8px 8px 0 0;
`;

export const TableName = styled.div`
  color: #fff;
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding: 10px;
`;

export const Wrapper = styled.div`
  background: rgb(255, 255, 255);
  border-radius: 8px;
  border: 1px solid rgb(218, 224, 231);
  box-shadow: 0 0 8px 0 rgba(155, 173, 191, 0.5);
  padding: 36px 42px;
`;

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 24px;
`;

export const TablesCountWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const LastEdited = styled.div`
  color: rgb(150, 145, 136);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;

export const Tables = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
  margin-right: 5px;
`;

export const TablesCount = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 20px;
  font-weight: bold;
`;
