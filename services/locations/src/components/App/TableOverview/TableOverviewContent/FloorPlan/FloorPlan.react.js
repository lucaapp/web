import React, { useState, useEffect } from 'react';
import { useIntl } from 'react-intl';

import {
  SecondaryButton,
  InformationIcon,
  StyledTooltip,
} from 'components/general';

import { EditTablesModal } from 'components/App/modals/EditTablesModal';
import { useMatomo } from '@jonkoops/matomo-tracker-react';
import { useGetNumberedTables } from '../TableOverviewContent.helper';

import {
  TablesCompWrapper,
  TableComp,
  TableName,
  Wrapper,
  HeaderWrapper,
  Tables,
  TablesCount,
  TablesCountWrapper,
  LastEdited,
} from './FloorPlan.styled';

export const FloorPlan = ({ location }) => {
  const intl = useIntl();
  const { trackPageView } = useMatomo();
  const [isModalOpen, setIsModalOpen] = useState(false);

  useEffect(() => {
    trackPageView({
      documentTitle: 'Edit Tables',
    });
  });

  const { isLoading, error, numberedTables } = useGetNumberedTables(
    location.uuid
  );

  if (isLoading || error) return null;

  return (
    <>
      <Wrapper>
        <HeaderWrapper>
          <TablesCountWrapper>
            <Tables>{`${intl.formatMessage({
              id: 'sidebar.tables',
            })}:`}</Tables>
            <TablesCount data-cy="tableCount">
              {numberedTables.length}
            </TablesCount>
            <StyledTooltip
              title={intl.formatMessage({
                id: 'table.overview.tableNumbering.tooltip',
              })}
            >
              <InformationIcon />
            </StyledTooltip>
          </TablesCountWrapper>
          <LastEdited />
          <SecondaryButton
            onClick={() => setIsModalOpen(true)}
            data-cy="editTablesButton"
          >
            {intl.formatMessage({ id: 'editTables' })}
          </SecondaryButton>
        </HeaderWrapper>
        {!!numberedTables.length && (
          <TablesCompWrapper data-cy="tables-floorplan">
            {numberedTables.map(table => (
              <TableComp key={table.key}>
                <StyledTooltip title={table.name}>
                  <TableName data-cy={`tableBlock-${table.key}-`}>
                    {table.name}
                  </TableName>
                </StyledTooltip>
              </TableComp>
            ))}
          </TablesCompWrapper>
        )}
      </Wrapper>
      {isModalOpen && (
        <EditTablesModal
          onClose={() => setIsModalOpen(false)}
          locationId={location.uuid}
          tableCount={numberedTables.length}
          numberedTables={numberedTables}
        />
      )}
    </>
  );
};
