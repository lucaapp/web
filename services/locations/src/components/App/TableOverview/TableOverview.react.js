import React from 'react';
import { useParams } from 'react-router-dom';
import { Layout } from 'antd';
import { useIntl } from 'react-intl';

import { useGetLocationById } from 'components/hooks';
import { Content, Sider } from 'components/general';

import { GroupList } from 'components/App/GroupList';
import { TableOverviewContent } from './TableOverviewContent';
import { Wrapper, Header, LocationBreadcrum } from './TableOverview.styled';

export const TableOverview = () => {
  const intl = useIntl();
  const { locationId } = useParams();

  const { isLoading, error, data: location } = useGetLocationById(locationId);

  if (isLoading || error) return null;

  return (
    <Layout>
      <Sider>
        <GroupList />
      </Sider>
      <Layout>
        <Content>
          <Wrapper>
            <LocationBreadcrum data-cy="groupName">{`${location.groupName} / ${
              location.name ||
              intl.formatMessage({ id: 'location.defaultName' })
            }`}</LocationBreadcrum>
            <Header data-cy="tablesHeader">
              {intl.formatMessage({ id: 'sidebar.tables' })}
            </Header>
            <TableOverviewContent location={location} />
          </Wrapper>
        </Content>
      </Layout>
    </Layout>
  );
};
