import React from 'react';
import { useIntl } from 'react-intl';

import {
  Wrapper,
  Header,
  Description,
  DescriptionWrapper,
  StyledQuestionCircleOutlined,
} from './DevicesHeader.styled';

export const DevicesHeader = () => {
  const intl = useIntl();

  return (
    <Wrapper>
      <Header>{intl.formatMessage({ id: 'devices.title' })}</Header>
      <DescriptionWrapper>
        <StyledQuestionCircleOutlined />
        <Description>
          {intl.formatMessage({ id: 'device.description' }, { br: <br /> })}
        </Description>
      </DescriptionWrapper>
    </Wrapper>
  );
};
