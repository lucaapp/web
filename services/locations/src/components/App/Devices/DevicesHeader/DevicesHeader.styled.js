import styled from 'styled-components';
import { QuestionCircleOutlined } from '@ant-design/icons';

export const Header = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 24px;
  font-weight: 600;
  margin-bottom: 24px;
  font-family: Montserrat-Bold, sans-serif;
`;

export const DescriptionWrapper = styled.div`
  display: flex;
`;

export const Description = styled.p`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
  margin-left: 19px;
  margin-bottom: 0;
`;

export const StyledQuestionCircleOutlined = styled(QuestionCircleOutlined)`
  font-size: 26px;
  display: flex;
  align-items: center;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
