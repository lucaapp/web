import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';

import moment from 'moment';

import { Modal } from 'components/general';
import { ReactivateDeviceModal } from 'components/App/modals/ReactivateDeviceModal';
import { QUERY_KEYS } from 'components/hooks';

import { deleteDevice, logoutDevice } from 'network/api';
import { notify } from './DeviceList.helper';

import { ListTitle } from './ListTitle';
import { DeviceStatusSwitch } from './DeviceStatusSwitch';
import { DeviceDeleteConfirm } from './DeviceDeleteConfirm';

import {
  DeviceValue,
  StyledTable,
  StyledMobilePhoneIcon,
} from './DeviceList.styled';

const DEVICE_LOGOUT = 'logout';
const DEVICE_DELETE = 'delete';

const http = {
  deleteDevice,
  logoutDevice,
};

export const DeviceList = ({ devices }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();

  const [operatorDevices, setOperatorDevices] = useState(devices);
  const [reactivateDeviceId, setReactivateDeviceId] = useState(null);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const unknownDeviceName = intl.formatMessage({ id: 'device.unknown' });

  const apiService = async (deviceId, action) => {
    try {
      const method = `${action}Device`;
      await http[method](deviceId);
      queryClient.invalidateQueries(QUERY_KEYS.OPERATOR_DEVICES);
      notify('success', action, intl);
    } catch {
      notify('error', action, intl);
    }
  };

  const handleDeleteDevice = deviceId => {
    apiService(deviceId, DEVICE_DELETE);
  };

  const handleLogoutDevice = deviceId => {
    apiService(deviceId, DEVICE_LOGOUT);
  };

  useEffect(() => {
    setOperatorDevices([...devices]);
  }, [devices]);

  const modalContent = () => {
    return <ReactivateDeviceModal deviceId={reactivateDeviceId} />;
  };

  const onReactivate = (deviceId, deviceActivated) => {
    if (!deviceActivated) {
      setReactivateDeviceId(deviceId);
      setIsModalVisible(!!deviceId);
      return;
    }
    handleLogoutDevice(deviceId);
  };

  const reset = () => {
    setIsModalVisible(false);
    setReactivateDeviceId(null);
  };

  const columns = [
    {
      title: intl.formatMessage({ id: 'device.list.deviceName' }),
      key: 'deviceName',
      render: function renderDeviceName(device) {
        return (
          <DeviceValue>
            <StyledMobilePhoneIcon />
            {device.name || unknownDeviceName}
          </DeviceValue>
        );
      },
    },
    {
      title: intl.formatMessage({ id: 'device.list.lastActive' }),
      key: 'deviceLastActive',
      render: function renderDeviceLastActive(device) {
        return (
          <DeviceValue>
            {moment(device.refreshedAt).format('DD.MM.YYYY - HH:mm')}
          </DeviceValue>
        );
      },
    },
    {
      title: (
        <ListTitle
          title="device.list.role"
          tooltipTitle="device.list.role.information"
        />
      ),
      key: 'deviceRole',
      render: function renderDeviceRole(device) {
        return (
          <DeviceValue>
            {intl.formatMessage({
              id: `device.role.${device.role}`,
            })}
          </DeviceValue>
        );
      },
    },
    {
      title: (
        <ListTitle
          title="device.list.status"
          tooltipTitle="device.list.active.information"
        />
      ),
      key: 'deviceStatus',
      render: function renderDeviceStatus(device) {
        return (
          <DeviceStatusSwitch device={device} onReactivate={onReactivate} />
        );
      },
    },
    {
      render: function renderDeviceDelete(device) {
        return (
          <DeviceDeleteConfirm
            deviceId={device.deviceId}
            handleDeleteDevice={handleDeleteDevice}
          />
        );
      },
    },
  ];

  return (
    <>
      <StyledTable
        columns={columns}
        dataSource={operatorDevices}
        id="deviceListTable"
        pagination={false}
        showSorterTooltip={false}
        rowKey={device => device.deviceId}
      />
      <Modal
        content={modalContent}
        visible={isModalVisible}
        afterClose={reset}
      />
    </>
  );
};
