import { notification } from 'antd';

export const notify = (status, action, intl) =>
  notification.success({
    message: intl.formatMessage({
      id: `device.list.${action}.notification.${status}`,
    }),
  });
