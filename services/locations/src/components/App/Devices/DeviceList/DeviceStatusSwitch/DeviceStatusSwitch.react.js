import React from 'react';
import { Switch } from 'components/general';
import { StyledTableSwitchContainer } from './DeviceStatusSwitch.styled';

export const DeviceStatusSwitch = ({ device, onReactivate }) => {
  const { isExpired, activated, deviceId } = device;

  return (
    <StyledTableSwitchContainer>
      <Switch
        checked={!isExpired && activated}
        data-cy="activateCheckInEntryPolicyToggle"
        onChange={() => onReactivate(deviceId, !isExpired && activated)}
      />
    </StyledTableSwitchContainer>
  );
};
