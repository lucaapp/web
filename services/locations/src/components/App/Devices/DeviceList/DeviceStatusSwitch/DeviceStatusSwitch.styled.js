import styled from 'styled-components';
import { StyledSwitchContainer } from 'components/App/modals/generalOnboarding/Onboarding.styled';

export const StyledTableSwitchContainer = styled(StyledSwitchContainer)`
  width: 70px;
  justify-content: flex-end;
  margin-top: 0;
`;
