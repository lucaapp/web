import React from 'react';
import { useIntl } from 'react-intl';
import { InformationIcon, StyledTooltip } from 'components/general';

export const ListTitle = ({ title, tooltipTitle }) => {
  const intl = useIntl();

  return (
    <>
      {intl.formatMessage({ id: title })}
      <StyledTooltip
        title={intl.formatMessage(
          {
            id: tooltipTitle,
          },
          { br: <br /> }
        )}
      >
        <InformationIcon />
      </StyledTooltip>
    </>
  );
};
