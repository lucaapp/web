import React from 'react';
import { useIntl } from 'react-intl';

import Icon from '@ant-design/icons';
import { Popconfirm } from 'antd';

import { BinBlueIcon } from 'assets/icons';

export const DeviceDeleteConfirm = ({ handleDeleteDevice, deviceId }) => {
  const intl = useIntl();

  return (
    <Popconfirm
      cancelText={intl.formatMessage({
        id: 'generic.cancel',
      })}
      okText={intl.formatMessage({
        id: 'device.list.delete.confirmButton',
      })}
      onConfirm={() => handleDeleteDevice(deviceId)}
      placement="leftTop"
      title={intl.formatMessage({
        id: 'device.list.delete.confirmation',
      })}
    >
      <Icon component={BinBlueIcon} />
    </Popconfirm>
  );
};
