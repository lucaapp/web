import styled from 'styled-components';
import { Table } from 'antd';
import { PrimaryButton } from 'components/general';
import { MobilePhoneIcon } from 'assets/icons';

export const DeviceValue = styled.div`
  flex: 1;
  font-size: 14px;
  font-weight: 500;
  overflow: hidden;
  text-overflow: ellipsis;
  color: rgba(0, 0, 0, 0.87);
  font-family: Montserrat-Medium, sans-serif;
  display: flex;
  align-items: center;
`;

export const StyledTable = styled(Table)`
  margin-top: 24px;
  .ant-table-thead > tr > th {
    background-color: white;
    font-family: Montserrat-Bold, sans-serif;
    font-size: 14px;
  }
  .ant-table-thead
    > tr
    > th:not(:last-child):not(.ant-table-selection-column):not(.ant-table-row-expand-icon-cell):not([colspan])::before {
    background-color: white;
  }
`;

export const ReactivateDeviceAction = styled(PrimaryButton)`
  border: none;
  outline: none;
  cursor: pointer;
  font-size: 14px;
  box-shadow: none;
  font-weight: bold;
  color: rgb(80, 102, 124);
  background-color: transparent;
  font-family: Montserrat-Bold, sans-serif;
`;

export const StyledMobilePhoneIcon = styled(MobilePhoneIcon)`
  margin-right: 10px;
`;
