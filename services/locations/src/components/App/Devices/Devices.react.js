import React, { useEffect } from 'react';
import { gt } from 'semver';
import { Layout } from 'antd';
import { useIntl } from 'react-intl';

import { RELEASE_OPERATOR_APP_NOTIFICATION } from 'constants/releaseVersions';
import { updateOperator } from 'network/api';
import { useGetOperatorDevices } from 'components/hooks';

import {
  Content,
  LocationCard,
  NavigationButton,
  Sider,
} from 'components/general';

// Components
import { useUser } from 'store-context/selectors';
import { NoDevices } from './NoDevices';
import { DeviceList } from './DeviceList';
import { AddDeviceButton } from './AddDeviceButton';
import { DevicesHeader } from './DevicesHeader';

import { Wrapper } from './Devices.styled';

export const Devices = () => {
  const intl = useIntl();
  const { data: devices, isError, isLoading } = useGetOperatorDevices();

  const {
    data: operator,
    isError: operatorError,
    isLoading: operatorLoading,
  } = useUser();

  useEffect(() => {
    if (gt(RELEASE_OPERATOR_APP_NOTIFICATION, operator.lastVersionSeen)) {
      updateOperator({
        lastVersionSeen: process.env.REACT_APP_VERSION,
      });
    }
  }, [operator]);

  if (isError || isLoading || operatorError || operatorLoading) {
    return null;
  }

  const registeredDevices = devices.filter(device => device.name);

  return (
    <Layout>
      <Sider>
        <NavigationButton />
      </Sider>
      <Content>
        <Wrapper data-cy="devicesPageWrapper">
          {registeredDevices.length > 0 ? (
            <>
              <DevicesHeader />
              <AddDeviceButton />
              <LocationCard
                isFlexEnd
                isDeviceList
                title={intl.formatMessage(
                  {
                    id: 'devices.tableDeviceCount',
                  },
                  { count: registeredDevices.length }
                )}
              >
                <DeviceList devices={registeredDevices} />
              </LocationCard>
            </>
          ) : (
            <NoDevices />
          )}
        </Wrapper>
      </Content>
    </Layout>
  );
};
