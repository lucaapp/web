import React from 'react';
import { useIntl } from 'react-intl';

import { Modal, PrimaryButton } from 'components/general';
import { CreateDeviceModal } from 'components/App/modals/CreateDeviceModal';

import { AddDeviceWrapper } from './AddDeviceButton.styled';

export const AddDeviceButton = ({ isCentered = false }) => {
  const intl = useIntl();

  const modalOpenButton = (
    <PrimaryButton data-cy="addDeviceButton">
      {intl.formatMessage({ id: 'device.addDevice' })}
    </PrimaryButton>
  );

  const modalContent = () => <CreateDeviceModal />;

  return (
    <AddDeviceWrapper isCentered={isCentered}>
      <Modal content={modalContent} openButton={modalOpenButton} />
    </AddDeviceWrapper>
  );
};
