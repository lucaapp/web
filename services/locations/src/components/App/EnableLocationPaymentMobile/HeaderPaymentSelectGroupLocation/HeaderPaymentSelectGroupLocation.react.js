import React from 'react';
import { useIntl } from 'react-intl';

import {
  ActivateSubTitle,
  ActivateTitle,
  PoweredBy,
} from '../EnableLocationPaymentMobile.styled';

export const HeaderPaymentSelectGroupLocation = () => {
  const intl = useIntl();

  return (
    <>
      <ActivateTitle>
        {intl.formatMessage({
          id: 'payment.mobile.activateTitle',
        })}
      </ActivateTitle>
      <ActivateSubTitle>
        {intl.formatMessage({
          id: 'payment.mobile.activate.subTitle',
        })}
      </ActivateSubTitle>
      <PoweredBy>
        {intl.formatMessage({
          id: 'payment.poweredBy',
        })}
      </PoweredBy>
    </>
  );
};
