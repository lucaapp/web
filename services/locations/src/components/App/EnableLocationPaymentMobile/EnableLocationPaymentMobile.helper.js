export const getInitialData = operator => {
  return {
    vatNumber: '',
    phone: operator?.phone,
    businessEntityName: operator?.businessEntityName,
    businessEntityStreetName: operator?.businessEntityStreetName,
    businessEntityStreetNumber: operator?.businessEntityStreetNumber,
    businessEntityZipCode: operator?.businessEntityZipCode,
    businessEntityCity: operator?.businessEntityCity,
  };
};
