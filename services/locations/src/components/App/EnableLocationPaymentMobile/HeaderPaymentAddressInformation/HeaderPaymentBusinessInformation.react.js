import React from 'react';
import { useIntl } from 'react-intl';

import {
  CardSubTitle,
  CardTitle,
} from 'components/Authentication/Authentication.styled';

export const HeaderPaymentBusinessInformation = () => {
  const intl = useIntl();

  return (
    <>
      <CardTitle>
        {intl.formatMessage({
          id: 'authentication.businessInfo.title',
        })}
      </CardTitle>
      <CardSubTitle>
        {intl.formatMessage({
          id: 'authentication.businessInfo.accountCreated.subTitle',
        })}
      </CardSubTitle>
    </>
  );
};
