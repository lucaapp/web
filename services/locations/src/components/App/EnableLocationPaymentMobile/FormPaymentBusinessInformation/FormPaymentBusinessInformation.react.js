import React from 'react';
import { Form } from 'antd';
import { useIntl } from 'react-intl';

import {
  ButtonWrapper,
  StyledInput,
  StyledSecondaryButton,
  StyledWhiteButton,
} from 'components/Authentication/Authentication.styled';
import { BusinessAddress } from 'components/Authentication/Register/steps/BusinessInfoStep/BusinessAddress';
import {
  useMobilePhoneValidator,
  useNameValidator,
  useVATValidator,
} from 'components/hooks/useValidators';
import { VATDescription } from '../EnableLocationPaymentMobile.styled';

export const FormPaymentBusinessInformation = ({
  initialValues,
  back,
  onFinish,
  isFormSubmitting,
}) => {
  const intl = useIntl();
  const businessNameValidator = useNameValidator('businessEntityName');
  const phoneValidator = useMobilePhoneValidator();
  const vatValidator = useVATValidator();

  return (
    <Form onFinish={onFinish} initialValues={initialValues}>
      <VATDescription>
        {intl.formatMessage({
          id: 'payment.mobile.VATDescription',
        })}
      </VATDescription>
      <Form.Item
        data-cy="vatNumber"
        colon={false}
        name="vatNumber"
        label={intl.formatMessage({
          id: 'payment.mobile.vatNumber',
        })}
        rules={vatValidator}
      >
        <StyledInput autoFocus />
      </Form.Item>
      <Form.Item
        data-cy="phone"
        colon={false}
        name="phone"
        label={intl.formatMessage({
          id: 'payment.mobile.phone',
        })}
        rules={phoneValidator}
      >
        <StyledInput />
      </Form.Item>
      <Form.Item
        data-cy="businessEntityName"
        colon={false}
        name="businessEntityName"
        label={intl.formatMessage({
          id: 'register.businessEntityName',
        })}
        rules={businessNameValidator}
      >
        <StyledInput data-cy="businessNameInputField" />
      </Form.Item>

      <BusinessAddress />

      <ButtonWrapper multipleButtons>
        <StyledSecondaryButton onClick={back} disabled={isFormSubmitting}>
          {intl.formatMessage({
            id: 'authentication.form.button.back',
          })}
        </StyledSecondaryButton>
        <StyledWhiteButton
          htmlType="submit"
          data-cy="confirmBusinessInfoButton"
          disabled={isFormSubmitting}
        >
          {intl.formatMessage({
            id: 'payment.mobile.activateNow',
          })}
        </StyledWhiteButton>
      </ButtonWrapper>
    </Form>
  );
};
