import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import { ConsentBoxes } from './ConsentBoxes';
import {
  ButtonWrapper,
  StyledSecondaryButton,
  StyledWhiteButton,
} from '../EnableLocationPaymentMobile.styled';

export const FormPaymentSelectGroupLocation = ({
  onCancel,
  onFinish,
  hasAcceptedPayTerms,
  isSubmitDisabled,
}) => {
  const intl = useIntl();

  const [acceptedDPA, setAcceptedDPA] = useState(hasAcceptedPayTerms);
  const [acceptedTermsAndConditions, setAcceptedTermsAndConditions] = useState(
    hasAcceptedPayTerms
  );

  return (
    <Form onFinish={onFinish}>
      {!hasAcceptedPayTerms && (
        <ConsentBoxes
          acceptedDPA={acceptedDPA}
          setAcceptedDPA={setAcceptedDPA}
          acceptedTermsAndConditions={acceptedTermsAndConditions}
          setAcceptedTermsAndConditions={setAcceptedTermsAndConditions}
        />
      )}

      <ButtonWrapper multipleButtons>
        <StyledSecondaryButton onClick={onCancel} data-cy="enablePaymentNotNow">
          {intl.formatMessage({
            id: 'payment.mobile.activateNotNow',
          })}
        </StyledSecondaryButton>
        <StyledWhiteButton
          htmlType="submit"
          data-cy="enablePaymentNow"
          disabled={
            (!hasAcceptedPayTerms &&
              (!acceptedDPA || !acceptedTermsAndConditions)) ||
            isSubmitDisabled
          }
        >
          {intl.formatMessage({
            id: 'authentication.form.button.next',
          })}
        </StyledWhiteButton>
      </ButtonWrapper>
    </Form>
  );
};
