import React from 'react';
import { Checkbox } from 'antd';
import { useIntl } from 'react-intl';

import {
  PAYMENT_DPA_LINK,
  PAYMENT_TERMS_CONDITIONS_LINK,
} from 'constants/links';
import { getConsentBoxLabel } from 'components/App/GroupSettings/Payment/PaymentOnboarding/PaymentCreateLocationGroup/ConsentSection';
import { StyledFormItem } from './ConsentBoxes.styled';

export const ConsentBoxes = ({
  acceptedDPA,
  setAcceptedDPA,
  acceptedTermsAndConditions,
  setAcceptedTermsAndConditions,
}) => {
  const intl = useIntl();

  const termsLabel = getConsentBoxLabel(
    intl,
    'authentication.registration.termsPayment',
    PAYMENT_TERMS_CONDITIONS_LINK
  );

  const dpaLabel = getConsentBoxLabel(
    intl,
    'authentication.registration.acceptDPA',
    PAYMENT_DPA_LINK
  );

  return (
    <>
      <StyledFormItem
        name="termsAndConditions"
        valuePropName="checked"
        rules={[
          {
            validator: (_, value) =>
              value
                ? Promise.resolve()
                : Promise.reject(
                    intl.formatMessage({
                      id: 'error.termsAndConditions',
                    })
                  ),
          },
        ]}
      >
        <Checkbox
          onChange={() => {
            setAcceptedTermsAndConditions(!acceptedTermsAndConditions);
          }}
        >
          {termsLabel}
        </Checkbox>
      </StyledFormItem>
      <StyledFormItem
        name="dpa"
        valuePropName="checked"
        rules={[
          {
            validator: (_, value) =>
              value
                ? Promise.resolve()
                : Promise.reject(
                    intl.formatMessage({
                      id: 'error.dpa',
                    })
                  ),
          },
        ]}
      >
        <Checkbox
          onChange={() => {
            setAcceptedDPA(!acceptedDPA);
          }}
        >
          {dpaLabel}
        </Checkbox>
      </StyledFormItem>
    </>
  );
};
