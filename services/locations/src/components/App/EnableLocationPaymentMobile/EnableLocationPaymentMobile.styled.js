import styled from 'styled-components';
import { Media } from 'utils/media';
import { Input } from 'antd';
import { SecondaryButton, WhiteButton } from 'components/general';

export const Wrapper = styled.div`
  padding: 32px 40px 60px 40px;
  background: rgb(195, 206, 217);
  border-radius: 4px;
  box-shadow: -5px 5px 5px 0 rgba(0, 0, 0, 0.1);
`;

export const PoweredBy = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 12px;
  font-style: italic;
  font-weight: 500;
  display: flex;
  justify-content: end;
  margin-bottom: 18px;
`;

export const ActivateTitle = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 28px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const ActivateSubTitle = styled(ActivateTitle)`
  font-size: 14px;
  margin-bottom: 24px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
`;

export const EnableGroup = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 16px;
  padding-top: 16px;
  border-bottom: 0.5px solid rgb(0, 0, 0);
`;

export const GroupName = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;

export const GroupsWrapper = styled.div`
  margin-bottom: 24px;

  & > *:last-child {
    border-bottom: none;
  }
`;

export const StyledInput = styled(Input)`
  border: 1px solid #696969 !important;
  background-color: transparent !important;
  &:hover,
  &:active,
  &:focus {
    border: 1px solid #696969;
    background-color: transparent;
  }

  ${Media.mobile`
    border-radius: 0px;
    border: 0.5px solid rgb(0, 0, 0);
    height: 48px;
  `}
`;

export const StyledWhiteButton = styled(WhiteButton)`
  min-width: unset;
  ${Media.mobile`
    height: 48px;
  `}
`;

export const StyledSecondaryButton = styled(SecondaryButton)`
  min-width: unset;
  ${Media.mobile`
    height: 48px;
  `}
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: ${({ multipleButtons }) =>
    multipleButtons ? 'space-between' : 'flex-end'};
  margin-top: 24px;

  @media (max-width: 1096px) {
    margin-top: 40px;
    flex-flow: column-reverse;

    button:not(:last-child) {
      margin-top: 24px;
    }
  }
`;

export const VATDescription = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 24px;
`;
