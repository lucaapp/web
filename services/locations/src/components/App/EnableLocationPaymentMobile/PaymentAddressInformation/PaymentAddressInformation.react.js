import React from 'react';

import { useOperator } from 'components/hooks';

import { HeaderPaymentBusinessInformation } from '../HeaderPaymentAddressInformation';
import { FormPaymentBusinessInformation } from '../FormPaymentBusinessInformation';
import { getInitialData } from '../EnableLocationPaymentMobile.helper';

export const PaymentAddressInformation = ({
  back,
  onFinish,
  isFormSubmitting,
}) => {
  const { data: operator } = useOperator();
  return (
    <>
      <HeaderPaymentBusinessInformation />
      <FormPaymentBusinessInformation
        initialValues={getInitialData(operator)}
        back={back}
        onFinish={onFinish}
        isFormSubmitting={isFormSubmitting}
      />
    </>
  );
};
