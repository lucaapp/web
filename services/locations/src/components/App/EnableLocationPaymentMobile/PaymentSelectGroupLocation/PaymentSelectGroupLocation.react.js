import React from 'react';
import { Switch } from 'components/general';
import { HeaderPaymentSelectGroupLocation } from '../HeaderPaymentSelectGroupLocation';
import {
  EnableGroup,
  GroupName,
  GroupsWrapper,
} from '../EnableLocationPaymentMobile.styled';
import { FormPaymentSelectGroupLocation } from '../FormPaymentSelectGroupLocation';

export const PaymentSelectGroupLocation = ({
  groups,
  onCancel,
  next,
  locationGroupId,
  setLocationGroupId,
}) => {
  return (
    <>
      <HeaderPaymentSelectGroupLocation />
      <GroupsWrapper>
        {groups.map(group => (
          <EnableGroup key={group.groupId}>
            <GroupName>{group.name}</GroupName>
            <Switch
              onChange={() =>
                group.groupId === locationGroupId
                  ? setLocationGroupId(null)
                  : setLocationGroupId(group.groupId)
              }
              checked={locationGroupId === group.groupId}
            />
          </EnableGroup>
        ))}
      </GroupsWrapper>
      <FormPaymentSelectGroupLocation
        onCancel={onCancel}
        onFinish={next}
        isSubmitDisabled={!locationGroupId}
      />
    </>
  );
};
