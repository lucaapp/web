import React, { useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useIntercom } from 'react-use-intercom';
import { Steps } from 'antd';
import { useQueryClient } from 'react-query';

import {
  useGetGroups,
  useGetPaymentOperator,
} from 'components/hooks/useQueries';
import {
  AUTHENTICATION_CONFIRMATION_ROUTE,
  LOGIN_ROUTE,
} from 'constants/routes';
import { logout, getGroup } from 'network/api';
import {
  acceptPaymentTerms,
  createLocationGroup,
  patchLocation,
} from 'network/payment';
import { clearJwt } from 'utils/jwtStorage';
import { clearHasSeenPrivateKeyModal } from 'utils/storage';
import { usePrivateKey } from 'utils/privateKey';
import { getFormattedPhoneNumber } from 'utils/formatters';

import { Footer } from 'components/Authentication/Footer';
import { LogoBackground } from 'components/general/LogoBackground';
import { useNotifications, useOperator } from 'components/hooks';
import { PaymentSelectGroupLocation } from './PaymentSelectGroupLocation';
import { PaymentAddressInformation } from './PaymentAddressInformation';
import { Wrapper } from './EnableLocationPaymentMobile.styled';

export const EnableLocationPaymentMobile = () => {
  const history = useHistory();
  const queryClient = useQueryClient();
  const [, clearPrivateKey] = usePrivateKey(null);
  const [currentStep, setCurrentStep] = useState(0);
  const [isFormSubmitting, setIsFormSubmitting] = useState(false);
  const [locationGroupId, setLocationGroupId] = useState(null);
  const { boot } = useIntercom();
  const setupIntercom = useCallback(() => boot(), [boot]);
  const { errorMessage } = useNotifications();

  const { data: operator } = useOperator();
  useEffect(() => {
    setupIntercom();
  }, [setupIntercom]);

  const { isLoading, error, data: groups } = useGetGroups();

  // select first group by default
  useEffect(() => {
    if (groups && groups.length > 0) {
      setLocationGroupId(groups[0].groupId);
    }
  }, [isLoading, groups]);

  const {
    isLoading: isPayOperatorLoading,
    error: payOperatorError,
    data: payOperator,
  } = useGetPaymentOperator(operator.operatorId);

  const hasAcceptedPayTerms = !!payOperator?.payTermsAccepted;

  const nextStep = () => setCurrentStep(currentStep + 1);
  const previousStep = () => setCurrentStep(currentStep - 1);

  const handleError = errorId => {
    errorMessage(errorId);
  };

  const nextSelectGroup = async () => {
    if (!hasAcceptedPayTerms) {
      const payTermsPatched = await acceptPaymentTerms();

      if (!payTermsPatched) {
        handleError('payment.location.startOnboarding.error');
        return;
      }
    }

    nextStep();
  };

  const onFinish = ({
    phone,
    businessEntityCity,
    businessEntityName,
    businessEntityStreetName,
    businessEntityStreetNumber,
    businessEntityZipCode,
    vatNumber,
  }) => {
    setIsFormSubmitting(true);
    createLocationGroup({
      locationGroupId,
      phoneNumber: getFormattedPhoneNumber(phone),
      companyName: businessEntityName,
      businessFirstName: operator.firstName,
      businessLastName: operator.lastName,
      street: businessEntityStreetName,
      streetNumber: businessEntityStreetNumber,
      zipCode: businessEntityZipCode,
      city: businessEntityCity,
      invoiceEmail: operator.email,
      vatNumber,
    })
      .then(async response => {
        if (response.status === 201) {
          const locationGroup = await getGroup(locationGroupId);
          await Promise.all(
            locationGroup.locations.map(({ uuid }) => patchLocation(uuid, true))
          );
          history.push(AUTHENTICATION_CONFIRMATION_ROUTE);
        }
      })
      .catch(() => {
        handleError('payment.location.startOnboarding.error');
        setIsFormSubmitting(false);
      });
  };

  const onCancel = () => {
    logout()
      .then(response => {
        if (response.status >= 400) {
          handleError('notification.logout.error');
          return;
        }

        queryClient.clear();
        clearJwt();
        clearHasSeenPrivateKeyModal();
        clearPrivateKey(null);
        history.push(LOGIN_ROUTE);
      })
      .catch(() => handleError('notification.logout.error'));
  };

  const paymentEnableSteps = [
    {
      id: '0',
      content: (
        <PaymentSelectGroupLocation
          next={nextSelectGroup}
          onCancel={onCancel}
          hasAcceptedPayTerms={hasAcceptedPayTerms}
          groups={groups}
          locationGroupId={locationGroupId}
          setLocationGroupId={setLocationGroupId}
        />
      ),
    },
    {
      id: '1',
      content: (
        <PaymentAddressInformation
          back={previousStep}
          onFinish={onFinish}
          isFormSubmitting={isFormSubmitting}
        />
      ),
    },
  ];

  if (
    isLoading ||
    error ||
    !operator ||
    isPayOperatorLoading ||
    payOperatorError
  ) {
    return null;
  }

  return (
    <>
      <LogoBackground />
      <Wrapper>
        <Steps progressDot={() => null} current={currentStep}>
          {paymentEnableSteps.map(step => (
            <Steps.Step key={step.id} />
          ))}
        </Steps>
        {paymentEnableSteps[currentStep].content}
      </Wrapper>
      <Footer />
    </>
  );
};
