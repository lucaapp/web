import styled from 'styled-components';

export const Description = styled.div`
  margin-top: 16px;
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;
