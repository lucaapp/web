import React, { useEffect, useMemo, useState } from 'react';
import { useIntl } from 'react-intl';

import { Popconfirm } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';

import { useQuery, useQueryClient } from 'react-query';
import { forceCheckoutAllUsers, getTotalCount } from 'network/api';
import {
  QUERY_KEYS,
  useFeatureFlags,
  useGetLocationsOfGroup,
  useNotifications,
} from 'components/hooks';

import { PrimaryButton } from 'components/general';
import { GuestHeader, GuestWrapper, Info } from '../GroupOverview.styled';

import { AnonymousCheckins } from './AnonymousCheckins';
import {
  Counter,
  InfoWrapper,
  HorizontalLine,
  ButtonWrapper,
} from './WithLocations.styled';
import { sortLocations } from './AnonymousCheckins/AnonymousCheckins.helper';

export const WithLocations = ({ group }) => {
  const intl = useIntl();
  const { successMessage, errorMessage } = useNotifications();
  const queryClient = useQueryClient();
  const featureFlags = useFeatureFlags();
  const { groupId } = group;
  const [allChecked, setAllChecked] = useState(false);
  const [indeterminate, setIndeterminate] = useState(false);

  const { data: totalCount, isLoading, isError } = useQuery('totalCount', () =>
    getTotalCount(groupId)
  );

  const sortedLocations = sortLocations(group);
  const locationIds = sortedLocations.map(loc => loc.uuid);
  const apiResult = useGetLocationsOfGroup(QUERY_KEYS.LOCATION, locationIds);
  const locationsComplete = apiResult.every(query => query.isSuccess);

  const locations = useMemo(
    () =>
      locationsComplete ? apiResult.map(locationData => locationData.data) : [],
    [locationsComplete, apiResult]
  );

  useEffect(() => {
    if (locationsComplete) {
      setAllChecked(
        locations.every(location => location.isContactDataMandatory)
      );
      setIndeterminate(
        locations.some(location => location.isContactDataMandatory)
      );
    }
  }, [locations, locationsComplete]);

  if (isLoading || isError || !locationsComplete) return null;

  const checkoutDisabled = totalCount === 0;

  const onCheckout = () => {
    forceCheckoutAllUsers(groupId)
      .then(response => {
        if (response.status !== 204) return;

        queryClient.invalidateQueries(QUERY_KEYS.GROUPS);
        queryClient.invalidateQueries('totalCount');
        successMessage('notification.checkOut.success');
      })
      .catch(() => errorMessage('notification.checkOut.error'));
  };

  return (
    <>
      <GuestWrapper>
        <InfoWrapper>
          <GuestHeader>
            {intl.formatMessage({ id: 'group.view.overview.guests' })}
          </GuestHeader>
          <Info>
            <Counter data-cy="guestsTotalCount">{totalCount}</Counter>
          </Info>
        </InfoWrapper>
        <HorizontalLine />
        <InfoWrapper style={{ paddingLeft: 32 }}>
          <GuestHeader>
            {intl.formatMessage({ id: 'group.view.overview.locations' })}
          </GuestHeader>
          <Info>
            <Counter data-cy="locationsCount">{group.locations.length}</Counter>
          </Info>
        </InfoWrapper>
        <ButtonWrapper>
          <Popconfirm
            placement="topLeft"
            onConfirm={onCheckout}
            disabled={checkoutDisabled}
            title={intl.formatMessage({
              id: 'location.checkout.confirmText',
            })}
            okText={intl.formatMessage({
              id: 'location.checkout.confirmButton',
            })}
            cancelText={intl.formatMessage({
              id: 'generic.cancel',
            })}
            icon={<QuestionCircleOutlined />}
          >
            <PrimaryButton disabled={checkoutDisabled}>
              {intl.formatMessage({ id: 'group.view.overview.checkout.all' })}
            </PrimaryButton>
          </Popconfirm>
        </ButtonWrapper>
      </GuestWrapper>
      {featureFlags.anonymous_checkins && (
        <AnonymousCheckins
          group={group}
          locations={locations}
          allChecked={allChecked}
          indeterminate={indeterminate}
        />
      )}
    </>
  );
};
