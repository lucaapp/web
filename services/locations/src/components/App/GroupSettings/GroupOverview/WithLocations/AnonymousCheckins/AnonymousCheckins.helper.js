import { getBaseLocationFromGroup } from 'utils/group';

export const sortLocations = group => {
  const baseLocation = getBaseLocationFromGroup(group);
  const rest = group.locations
    .filter(location => location.name)
    .sort((a, b) => a.name.localeCompare(b.name));

  return [baseLocation, ...rest];
};
