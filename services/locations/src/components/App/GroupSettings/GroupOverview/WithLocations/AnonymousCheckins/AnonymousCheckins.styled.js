import styled from 'styled-components';

export const Wrapper = styled.div`
  margin-top: 24px;
  padding: 0 32px 0;
  border-top: 1px solid rgba(0, 0, 0, 0.87);
`;

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Header = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 16px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  margin-top: 32px;
`;

export const StyledAreaContainer = styled.div`
  margin-top: 24px;
`;

export const AreaRow = styled.div`
  display: flex;
  justify-content: space-between;
  border-top: 1px solid black;
  padding: 16px 0;

  > div {
    font-size: 14px;
    font-weight: 500;
  }
`;

export const StyledCheckboxContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  padding-bottom: 16px;
`;

export const StyledCheckboxLabel = styled.div`
  font-size: 12px;
  font-weight: 500;
  margin-right: 8px;
`;

export const Description = styled.div`
  margin-top: 16px;
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;
