import React from 'react';
import { useIntl } from 'react-intl';

import { InformationIcon, StyledTooltip } from 'components/general';
import { Description } from './HeaderSection.styled';

export const HeaderSection = () => {
  const intl = useIntl();

  return (
    <Description>
      {intl.formatMessage({
        id: 'group.view.overview.entryPolicy.description',
      })}
      <StyledTooltip
        title={intl.formatMessage(
          {
            id: 'entryPolicy.tooltip',
          },
          { br: <br /> }
        )}
      >
        <InformationIcon />
      </StyledTooltip>
    </Description>
  );
};
