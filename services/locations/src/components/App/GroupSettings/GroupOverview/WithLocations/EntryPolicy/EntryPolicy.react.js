import React from 'react';
import { useQueryClient } from 'react-query';

import { updateLocation } from 'network/api';

import {
  getEntryPolicyOptions,
  LocationEntryPolicyTypes,
} from 'utils/entryPolicy';
import {
  QUERY_KEYS,
  useFeatureFlags,
  useNotifications,
} from 'components/hooks';
import { sortLocations } from './EntryPolicy.helper';
import { TableHeader } from './TableHeader';
import { HeaderSection } from './HeaderSection';
import { Wrapper, StyledAreaContainer } from './EntryPolicy.styled';
import { PolicyRow } from './PolicyRow';

export const EntryPolicy = ({ group }) => {
  const { errorMessage } = useNotifications();
  const queryClient = useQueryClient();
  const sortedLocations = sortLocations(group);

  const featureFlags = useFeatureFlags();
  const entryPolicies = getEntryPolicyOptions(featureFlags?.enable_two_g_plus);
  const getDefaultValue = location =>
    location.entryPolicy ??
    entryPolicies?.[0].value ??
    LocationEntryPolicyTypes.TWO_G;

  const showError = () => errorMessage('notification.updateLocation.error');

  const toggleEntryPolicy = location => {
    updateLocation({
      locationId: location.uuid,
      data: {
        entryPolicy: location.entryPolicy ? null : getDefaultValue(location),
      },
    })
      .then(() => {
        queryClient.invalidateQueries([QUERY_KEYS.GROUP, group.groupId]);
      })
      .catch(showError);
  };

  const handlePolicyChange = (location, value) => {
    updateLocation({
      locationId: location.uuid,
      data: {
        entryPolicy: value,
      },
    })
      .then(() => {
        queryClient.invalidateQueries([QUERY_KEYS.GROUP, group.groupId]);
      })
      .catch(showError);
  };

  return (
    <Wrapper>
      <HeaderSection />
      <StyledAreaContainer>
        <TableHeader />
        {sortedLocations.map(location => (
          <PolicyRow
            key={location.uuid}
            locationUuid={location.uuid}
            getDefaultValue={getDefaultValue}
            handlePolicyChange={handlePolicyChange}
            entryPolicies={entryPolicies}
            toggleEntryPolicy={toggleEntryPolicy}
          />
        ))}
      </StyledAreaContainer>
    </Wrapper>
  );
};
