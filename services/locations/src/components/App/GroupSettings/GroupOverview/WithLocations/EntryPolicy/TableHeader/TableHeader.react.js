import React from 'react';
import { useIntl } from 'react-intl';
import {
  HeaderRow,
  RowTitleSwitch,
  RowTitlePolicy,
  RowTitleName,
} from './TableHeader.styled';

export const TableHeader = () => {
  const intl = useIntl();

  return (
    <HeaderRow>
      <RowTitleName>
        {intl.formatMessage({
          id: 'group.view.overview.entryPolicy.header.area',
        })}
      </RowTitleName>
      <RowTitlePolicy>
        {intl.formatMessage({
          id: 'group.view.overview.entryPolicy.header.policy',
        })}
      </RowTitlePolicy>
      <RowTitleSwitch />
    </HeaderRow>
  );
};
