import React from 'react';
import { useIntl } from 'react-intl';

import { LocationCard } from 'components/general';
import { WithLocations } from './WithLocations';

import { Wrapper, Title } from './GroupOverview.styled';
import { EntryPolicy } from './WithLocations/EntryPolicy';

export const GroupOverview = ({ group }) => {
  const intl = useIntl();

  return (
    <>
      <Wrapper data-cy="groupOverviewCard">
        <Title>{intl.formatMessage({ id: 'group.view.overview' })}</Title>
        <WithLocations group={group} />
      </Wrapper>
      <LocationCard
        isCollapse
        title={intl.formatMessage({
          id: 'group.view.overview.entryPolicy.header.policy',
        })}
      >
        <EntryPolicy group={group} />
      </LocationCard>
    </>
  );
};
