import styled from 'styled-components';

export const ContentWrapper = styled.div`
  padding: 16px;
`;

export const ActionWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding-top: 16px;
`;

export const Text = styled.p`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
`;
