import React from 'react';
import { useIntl } from 'react-intl';
import { LocationCard } from 'components/general';
import { PAYOUT_STATUS } from 'constants/paymentOnboarding';
import { AddBankDetailsButton } from '../AddBankDetailsButton';
import { ActionWrapper, ContentWrapper, Text } from './PayoutSetupCard.styled';
import { DeactivateLucaPayButton } from '../DeactivateLucaPayButton';

export const PayoutSetupCard = ({ locationGroup, payoutStatus }) => {
  const intl = useIntl();

  const getTitleId = () =>
    payoutStatus === PAYOUT_STATUS.NOT_STARTED
      ? 'payment.location.payoutSetup.title'
      : 'payment.location.pendingPayout.title';

  return (
    <LocationCard open title={intl.formatMessage({ id: getTitleId() })}>
      <ContentWrapper>
        {payoutStatus === PAYOUT_STATUS.NOT_STARTED && (
          <>
            <Text>
              {intl.formatMessage({
                id: 'payment.location.payoutSetup.description',
              })}
            </Text>
            <Text>
              {intl.formatMessage({ id: 'payment.location.payoutSetup.hint' })}
            </Text>
          </>
        )}
        {payoutStatus === PAYOUT_STATUS.PENDING && (
          <Text>
            {intl.formatMessage({ id: 'payment.location.pendingPayout.text' })}
          </Text>
        )}
        <ActionWrapper>
          <DeactivateLucaPayButton locationGroup={locationGroup} />
          <AddBankDetailsButton locationGroup={locationGroup} />
        </ActionWrapper>
      </ContentWrapper>
    </LocationCard>
  );
};
