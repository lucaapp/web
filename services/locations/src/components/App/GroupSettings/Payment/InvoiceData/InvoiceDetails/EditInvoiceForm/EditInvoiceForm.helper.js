export const getInvoiceValues = paymentLocationGroup => ({
  companyName: paymentLocationGroup?.companyName ?? '',
  businessFirstName: paymentLocationGroup?.businessFirstName ?? '',
  businessLastName: paymentLocationGroup?.businessLastName ?? '',
  street: paymentLocationGroup?.street ?? '',
  streetNumber: paymentLocationGroup?.streetNumber ?? '',
  zipCode: paymentLocationGroup?.zipCode ?? '',
  city: paymentLocationGroup?.city ?? '',
  invoiceEmail: paymentLocationGroup?.invoiceEmail ?? '',
  vatNumber: paymentLocationGroup?.vatNumber ?? '',
});
