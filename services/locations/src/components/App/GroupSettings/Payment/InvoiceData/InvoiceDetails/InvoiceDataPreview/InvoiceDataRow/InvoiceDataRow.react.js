import React from 'react';
import { useIntl } from 'react-intl';
import {
  ProfileDescription,
  ProfileTitle,
  RowWrapper,
} from '../InvoiceDataPreview.styled';

export const InvoiceDataRow = ({
  titleId,
  description,
  secondDescription = false,
}) => {
  const intl = useIntl();

  return (
    <RowWrapper>
      <ProfileTitle>{intl.formatMessage({ id: titleId })}</ProfileTitle>
      <ProfileDescription>{description}</ProfileDescription>
      {secondDescription && (
        <ProfileDescription>{secondDescription}</ProfileDescription>
      )}
    </RowWrapper>
  );
};
