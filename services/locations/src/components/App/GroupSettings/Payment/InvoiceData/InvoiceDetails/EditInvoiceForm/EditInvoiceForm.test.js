import React from 'react';
import { render } from 'utils/testing';
import { fireEvent } from '@testing-library/dom';
import { act, cleanup } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as paymentApi from 'network/payment';
import { EditInvoiceForm } from './EditInvoiceForm.react';

afterEach(cleanup);
jest.mock('network/payment');

paymentApi.patchLocationGroup.mockResolvedValue('success');

describe('EditInvoiceForm', () => {
  const mockInvoiceData = {
    companyName: 'neXenio',
    businessFirstName: 'luca',
    businessLastName: 'testing',
    street: 'Charlottenburgstr',
    streetNumber: '59',
    zipCode: '10117',
    city: 'Berlin',
    invoiceEmail: 'luca@nexenio.com',
    vatNumber: '',
  };

  it('can update the invoice data', async () => {
    const { findByTestId } = render(
      <EditInvoiceForm setIsEditable paymentLocationGroup={mockInvoiceData} />
    );

    const user = userEvent.setup();
    const companyNameInput = await findByTestId('companyName');
    const businessFirstNameInput = await findByTestId('businessFirstName');
    const vatNumberInput = await findByTestId('vatNumber');

    const submitButton = await findByTestId('saveInvoiceDataButton');

    await act(async () => {
      await user.type(companyNameInput, 'testCompanyName');
      await user.type(businessFirstNameInput, 'testFirstName');
      await user.type(vatNumberInput, 'DE123456789');
    });

    // eslint-disable-next-line require-await
    await act(async () => {
      fireEvent.click(submitButton);
    });
    expect(paymentApi.patchLocationGroup).toHaveBeenCalled();
  });

  it('can update other invoice data with empty vatNumber', async () => {
    const { findByTestId } = render(
      <EditInvoiceForm setIsEditable paymentLocationGroup={mockInvoiceData} />
    );

    const user = userEvent.setup();
    const cityInput = await findByTestId('city');
    const vatNumberInput = await findByTestId('vatNumber');

    const submitButton = await findByTestId('saveInvoiceDataButton');

    await act(async () => {
      await user.type(cityInput, 'testCity');
      await user.clear(vatNumberInput);
    });

    // eslint-disable-next-line require-await
    await act(async () => {
      fireEvent.click(submitButton);
    });
    expect(paymentApi.patchLocationGroup).toHaveBeenCalled();
  });
});
