import React from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';

import { AppForm, SubmitButton } from 'components/general/Form';
import { useInvoiceDataSchema } from 'components/general/Form/hooks';
import { patchLocationGroup } from 'network/payment';
import { QUERY_KEYS, useNotifications } from 'components/hooks';
import { InvoiceForm } from 'components/App/GroupSettings/Payment/common';
import { getInvoiceValues } from './EditInvoiceForm.helper';
import { ButtonWrapper } from '../InvoiceDataPreview/InvoiceDataPreview.styled';

export const EditInvoiceForm = ({ setIsEditable, paymentLocationGroup }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const invoiceSchema = useInvoiceDataSchema();
  const { errorMessage, successMessage } = useNotifications();

  const handleSubmit = values => {
    const { vatNumber } = values;
    patchLocationGroup(
      { ...values, vatNumber: vatNumber || null },
      paymentLocationGroup.uuid
    )
      .then(response => {
        if (response.status !== 200) {
          errorMessage();
          return;
        }

        successMessage();
        queryClient.invalidateQueries([
          QUERY_KEYS.PAYMENT_LOCATION_GROUP,
          paymentLocationGroup.uuid,
        ]);
        setIsEditable(false);
      })
      .catch(() => {
        errorMessage();
      });
  };

  return (
    <AppForm
      initialValues={getInvoiceValues(paymentLocationGroup)}
      onSubmit={handleSubmit}
      validationSchema={invoiceSchema}
    >
      <InvoiceForm />

      <ButtonWrapper>
        <SubmitButton
          dataCy="saveInvoiceDataButton"
          title={intl.formatMessage({ id: 'payment.invoiceData.button.save' })}
          disableWhenInvalid
        />
      </ButtonWrapper>
    </AppForm>
  );
};
