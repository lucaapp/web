import styled from 'styled-components';

export const ProfilesWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const ProfileTitle = styled.div`
  color: rgba(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
`;

export const ProfileDescription = styled.div`
  color: rgba(0, 0, 0);
  font-size: 16px;
  font-weight: 500;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 20px;
`;

export const RowWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-bottom: 16px;
`;
