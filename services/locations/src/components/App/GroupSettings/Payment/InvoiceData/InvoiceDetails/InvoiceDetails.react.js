import React, { useState } from 'react';
import { InvoiceDataPreview } from './InvoiceDataPreview';
import { EditInvoiceForm } from './EditInvoiceForm';

export const InvoiceDetails = ({ paymentLocationGroup }) => {
  const [isEditable, setIsEditable] = useState(false);

  if (!paymentLocationGroup) {
    return null;
  }

  return (
    <>
      {!isEditable ? (
        <InvoiceDataPreview
          data-cy="invoiceDataPreview"
          setIsEditable={setIsEditable}
          paymentLocationGroup={paymentLocationGroup}
        />
      ) : (
        <EditInvoiceForm
          setIsEditable={setIsEditable}
          paymentLocationGroup={paymentLocationGroup}
        />
      )}
    </>
  );
};
