import styled from 'styled-components';

export const ContentWrapper = styled.div`
  display: flex;
  margin: 16px 0;
`;

export const Content = styled.div`
  line-height: 20px;
`;
