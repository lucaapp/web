import { LocationCard } from 'components/general';
import React from 'react';
import { useIntl } from 'react-intl';
import { ContentWrapper, Content } from './InvoiceData.styled';
import { CollapsableWrapper } from '../ActivatePayments/ActivatePayments.styled';
import { InvoiceDetails } from './InvoiceDetails';

export const InvoiceData = ({ paymentLocationGroup }) => {
  const intl = useIntl();

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'payment.location.invoiceData.title' })}
    >
      <CollapsableWrapper>
        <ContentWrapper>
          <Content>
            {intl.formatMessage({
              id: 'payment.location.invoiceData.description',
            })}
          </Content>
        </ContentWrapper>
        <ContentWrapper>
          <InvoiceDetails paymentLocationGroup={paymentLocationGroup} />
        </ContentWrapper>
      </CollapsableWrapper>
    </LocationCard>
  );
};
