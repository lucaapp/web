import React from 'react';
import { useIntl } from 'react-intl';

import { PrimaryButton } from 'components/general';

import { ProfilesWrapper, ButtonWrapper } from './InvoiceDataPreview.styled';

import { InvoiceDataRow } from './InvoiceDataRow';

export const InvoiceDataPreview = ({ setIsEditable, paymentLocationGroup }) => {
  const intl = useIntl();

  const {
    businessFirstName,
    businessLastName,
    companyName,
    vatNumber,
    street,
    streetNumber,
    zipCode,
    city,
    invoiceEmail,
  } = paymentLocationGroup;

  const setDescription = description =>
    description || `${intl.formatMessage({ id: 'empty.data' })}`;

  return (
    <ProfilesWrapper>
      <InvoiceDataRow
        titleId="name"
        description={`${businessFirstName} ${businessLastName}`}
      />
      <InvoiceDataRow
        titleId="companyName"
        description={setDescription(companyName)}
      />
      <InvoiceDataRow
        titleId="vatNumber"
        description={setDescription(vatNumber)}
      />
      <InvoiceDataRow
        titleId="address"
        description={`${street} ${streetNumber}`}
        secondDescription={`${zipCode} ${city}`}
      />
      <InvoiceDataRow
        titleId="invoiceEmail"
        description={setDescription(invoiceEmail)}
      />

      <ButtonWrapper>
        <PrimaryButton
          data-cy="payment-editInvoiceDataButton"
          onClick={() => {
            setIsEditable(true);
          }}
        >
          {intl.formatMessage({ id: 'payment.invoiceData.button.edit' })}
        </PrimaryButton>
      </ButtonWrapper>
    </ProfilesWrapper>
  );
};
