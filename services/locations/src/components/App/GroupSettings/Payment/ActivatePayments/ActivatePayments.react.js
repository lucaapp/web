import React from 'react';
import { useIntl } from 'react-intl';
import { Steps } from 'antd';

import { LocationCard } from 'components/general';
import { CollapsableWrapper, StyledStep } from './ActivatePayments.styled';
import { RapydWalletActivation } from './steps/RapydWalletActivation';
import { AccountDetails } from './steps/AccountDetails';

export const ActivatePayments = ({ currentStep, locationGroup }) => {
  const intl = useIntl();

  const steps = [
    {
      id: 0,
      title:
        currentStep === 0
          ? intl.formatMessage({
              id: 'payment.location.rapydWalletActivation.title',
            })
          : intl.formatMessage({
              id: 'payment.location.state3Headline',
            }),
      content: <RapydWalletActivation />,
    },
    {
      // TODO can be removed as it is not used anymore
      id: 1,
      title: intl.formatMessage({
        id: 'payment.location.accountDetails.title',
      }),
      content: <AccountDetails locationGroup={locationGroup} />,
    },
  ];

  return (
    <LocationCard
      title={intl.formatMessage({
        id: 'location.tab.payment.activatePayment.title',
      })}
    >
      <CollapsableWrapper>
        <StyledStep direction="vertical" size="small" current={currentStep}>
          {steps.map(step => (
            <Steps.Step
              key={step.id}
              title={step.title}
              description={currentStep === step.id ? step.content : null}
            />
          ))}
        </StyledStep>
      </CollapsableWrapper>
    </LocationCard>
  );
};
