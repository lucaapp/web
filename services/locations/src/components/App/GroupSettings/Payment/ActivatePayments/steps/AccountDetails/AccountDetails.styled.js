import styled from 'styled-components';

export const ContentWrapper = styled.div`
  display: flex;
  justify-content: end;
`;

export const Content = styled.p`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
  margin-bottom: 40px;
`;
