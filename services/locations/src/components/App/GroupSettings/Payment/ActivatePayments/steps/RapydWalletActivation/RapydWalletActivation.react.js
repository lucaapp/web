import React from 'react';
import { useIntl } from 'react-intl';
import { Content } from 'components/App/GroupSettings/Payment/ActivatePayments/ActivatePayments.styled';

export const RapydWalletActivation = () => {
  const intl = useIntl();

  return (
    <Content>
      {intl.formatMessage({
        id: 'payment.location.rapydWalletActivation.text',
      })}
    </Content>
  );
};
