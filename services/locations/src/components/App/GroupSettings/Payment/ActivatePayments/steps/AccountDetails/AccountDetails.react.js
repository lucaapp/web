import React from 'react';
import { useIntl } from 'react-intl';
import { AddBankDetailsButton } from 'components/App/GroupSettings/Payment/AddBankDetailsButton';
import { Content, ContentWrapper } from './AccountDetails.styled';

// TODO remove as payout will be configured separately
export const AccountDetails = ({ locationGroup }) => {
  const intl = useIntl();

  return (
    <>
      <Content>
        {intl.formatMessage(
          { id: 'payment.location.accountDetails.text' },
          { br: <br /> }
        )}
      </Content>
      <ContentWrapper>
        <AddBankDetailsButton locationGroup={locationGroup} />
      </ContentWrapper>
    </>
  );
};
