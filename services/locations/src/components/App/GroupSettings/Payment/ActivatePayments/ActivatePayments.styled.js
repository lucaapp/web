import styled from 'styled-components';
import { Steps } from 'antd';
import { WarningButton } from 'components/general';

export const CollapsableWrapper = styled.div`
  padding: 0 32px;
`;

export const StyledStep = styled(Steps)`
  & .ant-steps-item-title {
    font-weight: bold;
  }

  & .ant-steps-item-container {
    display: block;
  }

  & .ant-steps-item-wait {
    & .ant-steps-icon {
      color: rgb(0, 0, 0, 0.5);
      font-size: 14px;
      font-weight: bold;
      letter-spacing: 0;
    }

    & .ant-steps-item-icon {
      border: 2px solid rgb(218, 224, 231);
    }
  }

  & .ant-steps-item-finish {
    & .ant-steps-item-icon {
      background-color: #819e57;
      border: 2px solid #819e57;

      & .anticon {
        & svg {
          fill: white;
        }
      }
    }
  }

  & .ant-steps-item-process {
    & > .ant-steps-item-container {
      & .ant-steps-item-tail {
        &::after {
          width: 2px;
          background-color: rgb(218, 224, 231);
        }
      }

      & .ant-steps-item-icon {
        background: #c3ced9;
        border: 2px solid rgb(84, 101, 123);

        & .ant-steps-icon {
          color: rgb(0, 0, 0);
          font-size: 14px;
          font-weight: bold;
          letter-spacing: 0;
        }
      }
    }
  }
`;

export const Title = styled.p`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: ${({ isActive }) => (isActive ? 'bold' : 'normal')};
  letter-spacing: 0;
  line-height: 24px;
`;

export const Content = styled.p`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
  margin-bottom: 40px;
`;

export const ExternalLinkButton = styled(WarningButton)`
  background-color: #ffdc5f;
  border-color: #ffdc5f;
`;
