import { useIntl } from 'react-intl';
import React from 'react';
import { ArrowRightOutlined } from '@ant-design/icons';
import { ContactFormModal } from 'components/App/modals/ContactFormModal';
import { LocationCard, Modal, PrimaryButton } from 'components/general';
import { Content, ButtonWrapper } from './PaymentOnboardingRejected.styled';

export const PaymentOnboardingRejected = () => {
  const intl = useIntl();

  const openModalButton = (
    <ButtonWrapper>
      <PrimaryButton data-cy="helpCenterModalTrigger">
        {intl.formatMessage({ id: 'payment.onboarding.rejected.contact' })}
        <ArrowRightOutlined />
      </PrimaryButton>
    </ButtonWrapper>
  );

  const modalContent = <ContactFormModal />;

  return (
    <LocationCard
      title={intl.formatMessage({
        id: 'payment.onboarding.rejected.title',
      })}
    >
      <Content>
        {intl.formatMessage({ id: 'payment.onboarding.rejected.description' })}
      </Content>
      <Modal content={modalContent} openButton={openModalButton} />
    </LocationCard>
  );
};
