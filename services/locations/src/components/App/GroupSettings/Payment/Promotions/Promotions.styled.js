import styled from 'styled-components';

export const StyledPromotionsList = styled.div`
  > * {
    border-bottom: 1px solid #9badbf;
    padding-bottom: 24px;

    &:last-child {
      border-bottom: none;
      padding-bottom: 0;
    }
  }
`;
