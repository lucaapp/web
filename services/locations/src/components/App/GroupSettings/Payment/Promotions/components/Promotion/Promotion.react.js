import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { getFormattedDate } from 'utils/formatters';
import { DrawerRight, InformationIcon } from 'components/general';
import { Modal, Popconfirm } from 'antd';
import {
  PromotionModalContent,
  useParticipation,
  termsAndConditionsLink,
} from 'components/App/GroupSettings/Payment/Promotions';
import {
  Description,
  FlexWrapper,
  InformationIconWrapper,
  PromotionWrapper,
  StyledDrawerHeading,
  StyledHeader,
  StyledIconWrapper,
  StyledSwitch,
  SwitchWrapper,
  SwitchWrapperContent,
} from './Promotion.styled';
import { ExtraTip } from './ExtraTip';

export const Promotion = ({ promotion }) => {
  const intl = useIntl();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isDrawerVisible, setIsDrawerVisible] = useState(false);
  const [isPopConfirmVisible, setIsPopConfirmVisible] = useState(false);
  const [isChecked, setIsChecked] = useState(false);
  const { subscribe, unsubscribe } = useParticipation();

  const onSwitchChange = active => {
    if (active && !promotion.restricted) {
      unsubscribe(promotion);
      return;
    }
    if (active && promotion.restricted) {
      setIsPopConfirmVisible(true);
      return;
    }

    setIsModalOpen(true);
  };

  const deactivateRestrictedCampaign = async () => {
    await unsubscribe(promotion);
  };

  const handleCancel = () => {
    setIsPopConfirmVisible(false);
  };

  const onSubscribe = async (_, { resetForm }) => {
    await subscribe(promotion);
    resetForm();
    setIsModalOpen(false);
  };

  useEffect(() => {
    setIsChecked(promotion.currentlyParticipating);
  }, [promotion]);

  const promotionDateRange = `${getFormattedDate(
    promotion.startsAt
  )} - ${getFormattedDate(promotion.endsAt)}`;

  const promotionHeader =
    promotion.header &&
    intl.formatMessage(
      { id: promotion.header },
      {
        discountPercentage: promotion.discountPercentage,
        tipTopUpPercentage: promotion.tipTopUpPercentage,
      }
    );

  const onCancelTermsAndConditions = () => {
    setIsChecked(false);
    setIsModalOpen(false);
  };

  return (
    <>
      <PromotionWrapper>
        <StyledIconWrapper>{promotion.icon}</StyledIconWrapper>
        <FlexWrapper>
          <>
            <StyledHeader>{promotionDateRange}</StyledHeader>
            <StyledHeader>
              {promotionHeader}
              <InformationIconWrapper onClick={() => setIsDrawerVisible(true)}>
                <InformationIcon />
              </InformationIconWrapper>
            </StyledHeader>
          </>
          <Description>
            {promotion.description &&
              intl.formatMessage(
                { id: promotion.description },
                {
                  discountPercentage: promotion.discountPercentage,
                  discountMaxAmount: promotion.discountMaxAmount,
                }
              )}
          </Description>
        </FlexWrapper>
        <Popconfirm
          placement="topLeft"
          title={intl.formatMessage({
            id: 'payment.promotions.deactivate.popConfirm',
          })}
          visible={isPopConfirmVisible}
          onConfirm={deactivateRestrictedCampaign}
          onCancel={handleCancel}
          okText={intl.formatMessage({
            id: 'deactivate.popConfirm.ok',
          })}
          cancelText={intl.formatMessage({
            id: 'generic.cancel',
          })}
        >
          <SwitchWrapper>
            <SwitchWrapperContent>
              {promotion.currentlyParticipating &&
                intl.formatMessage({
                  id: 'payment.promotions.promotionActive',
                })}
              <StyledSwitch
                onClick={() => onSwitchChange(promotion.currentlyParticipating)}
                checked={isChecked}
                data-cy={`${promotion.campaignId}_togglePromotionActive`}
              />
            </SwitchWrapperContent>
            {promotion.tipTopUpPercentage && (
              <ExtraTip locationGroupId={promotion.locationGroupId} />
            )}
          </SwitchWrapper>
        </Popconfirm>
      </PromotionWrapper>
      <Modal
        title={promotionHeader}
        visible={isModalOpen}
        footer={null}
        onOk={() => setIsModalOpen(false)}
        onCancel={() => onCancelTermsAndConditions()}
      >
        <PromotionModalContent
          onCancel={onCancelTermsAndConditions}
          onFormSubmit={onSubscribe}
          promotion={promotion}
        />
      </Modal>
      {promotion.drawer && (
        <DrawerRight
          title={<StyledDrawerHeading>{promotionHeader}</StyledDrawerHeading>}
          heading={promotionDateRange}
          visible={isDrawerVisible}
          onClose={() => setIsDrawerVisible(false)}
        >
          {intl.formatMessage(
            {
              id: promotion.drawer.description,
            },
            {
              termsAndConditions: termsAndConditionsLink(
                intl,
                promotion.termsAndConditionsLink
              ),
              discountPercentage: promotion.discountPercentage,
              discountMaxAmount: promotion.discountMaxAmount,
              tipTopUpPercentage: promotion.tipTopUpPercentage,
              br: <br />,
            }
          )}
        </DrawerRight>
      )}
    </>
  );
};
