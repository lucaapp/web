import React from 'react';
import { useIntl } from 'react-intl';
import { LocationCard } from 'components/general';

import { Promotion } from './components/Promotion';
import {
  lucaDiscountPromotion,
  payRaffleAndTipTopUpCampaignPromotion,
  usePromotions,
} from './Promotions.helper';
import { StyledPromotionsList } from './Promotions.styled';

export const Promotions = ({ locationGroup }) => {
  const intl = useIntl();
  const { lucaDiscount, payRaffleAndTipTopUp, tipTopUp } = usePromotions(
    locationGroup.groupId
  );

  if (!lucaDiscount && !payRaffleAndTipTopUp && !tipTopUp) return null;

  const highestDiscount = lucaDiscount?.sort(
    (a, b) => b.discountPercentage - a.discountPercentage
  )[0];

  const highestTipTopUp = tipTopUp?.sort(
    (a, b) => b.tipTopUpPercentage - a.tipTopUpPercentage
  )[0];

  return (
    <LocationCard
      testId="payment-promotionsCard"
      title={intl.formatMessage({
        id: 'payment.promotions.card.title',
      })}
    >
      <StyledPromotionsList>
        {payRaffleAndTipTopUp && (
          <Promotion
            promotion={payRaffleAndTipTopUpCampaignPromotion(
              payRaffleAndTipTopUp,
              locationGroup,
              highestTipTopUp
            )}
            key={payRaffleAndTipTopUp.name}
          />
        )}
        {highestDiscount && (
          <Promotion
            promotion={lucaDiscountPromotion(highestDiscount, locationGroup)}
            key={highestDiscount.name}
          />
        )}
      </StyledPromotionsList>
    </LocationCard>
  );
};
