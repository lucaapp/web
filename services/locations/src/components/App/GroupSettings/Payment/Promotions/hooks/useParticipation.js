import { useQueryClient } from 'react-query';
import { QUERY_KEYS, useNotifications } from 'components/hooks';
import { participateCampaigns, deleteParticipatedCampaign } from 'network';

export const useParticipation = () => {
  const queryClient = useQueryClient();
  const { successMessage, errorMessage } = useNotifications();

  const throwError = () => errorMessage('payment.raffleParticipation.error');

  const unsubscribe = async promotion => {
    try {
      const response = await deleteParticipatedCampaign(
        promotion.locationGroupId,
        promotion.relationId
      );
      if (response.status !== 204) {
        throwError();
        return;
      }
      if (promotion.tipTopUpCampaign && promotion.tipTopUpCampaign.name) {
        const tipTopUpResponse = await deleteParticipatedCampaign(
          promotion.locationGroupId,
          promotion.tipTopUpCampaign.relationId
        );
        if (tipTopUpResponse.status !== 204) {
          throwError();
          return;
        }
      }
      successMessage('payment.raffleParticipation.success');
      queryClient.invalidateQueries(QUERY_KEYS.CAMPAIGNS);
    } catch {
      throwError();
    }
  };

  const subscribe = async promotion => {
    try {
      const response = await participateCampaigns(promotion.locationGroupId, {
        campaign: promotion.name,
      });
      if (!response.id) {
        throwError();
        return;
      }
      if (promotion.tipTopUpCampaign && promotion.tipTopUpCampaign.name) {
        const tipTopUpResponse = await participateCampaigns(
          promotion.locationGroupId,
          {
            campaign: promotion.tipTopUpCampaign.name,
          }
        );
        if (!tipTopUpResponse.id) {
          throwError();
          return;
        }
      }
      successMessage('payment.raffleParticipation.success');
      queryClient.invalidateQueries(QUERY_KEYS.CAMPAIGNS);
    } catch {
      throwError();
    }
  };

  return { subscribe, unsubscribe };
};
