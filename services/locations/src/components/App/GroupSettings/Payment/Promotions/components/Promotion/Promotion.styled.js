import styled from 'styled-components';
import { Switch } from 'antd';

export const PromotionWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin: 14px 16px 0 16px;
`;

export const StyledHeader = styled.div`
  color: rgb(0, 0, 0);
  font-family: 'Montserrat-Bold', sans-serif;
  font-size: 14px;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 24px;
  display: flex;
  flex-direction: row;
`;

export const InformationIconWrapper = styled.div`
  cursor: pointer;
  display: flex;
`;

export const StyledIconWrapper = styled.div`
  display: flex;
  flex-direction: row;
  height: 100%;
  width: 67px;
  min-width: 67px;
  padding-top: 8px;
`;

export const FlexWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const SwitchWrapper = styled.div`
  display: flex;
  align-items: end;
  flex-direction: column;
  flex: 1;
  justify-content: space-between;
  min-width: 100px;
`;

export const SwitchWrapperContent = styled.div`
  align-items: end;
  flex-direction: row;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
  max-width: 60%;
  margin-top: 8px;
`;

export const StyledSwitch = styled(Switch)`
  margin-left: 8px;
`;

export const StyledDrawerHeading = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 24px;
  font-weight: 500;
  height: 102px;
  letter-spacing: 0;
  line-height: 34px;
`;
