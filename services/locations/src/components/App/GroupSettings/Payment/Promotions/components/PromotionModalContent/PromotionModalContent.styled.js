import styled from 'styled-components';

export const CheckboxWrapper = styled.div`
  padding: 30px 0 40px 0;

  .ant-alert {
    padding-left: 0;
  }
`;

export const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: -32px;
`;
