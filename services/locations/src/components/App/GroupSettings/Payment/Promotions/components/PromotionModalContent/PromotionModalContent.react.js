import React from 'react';
import { useIntl } from 'react-intl';
import { AppForm, FormInputType, SubmitButton } from 'components/general/Form';
import { SecondaryButton } from 'components/general';
import { termsAndConditionsLink } from 'components/App/GroupSettings/Payment/Promotions/Promotions.helper';
import {
  CheckboxWrapper,
  ButtonsWrapper,
} from './PromotionModalContent.styled';
import { usePromotionSchema } from './usePromotionSchema';

export const PromotionModalContent = ({
  promotion,
  onFormSubmit,
  onCancel,
}) => {
  const intl = useIntl();
  const promotionSchema = usePromotionSchema();

  return (
    <>
      {intl.formatMessage(
        { id: promotion.modal.content },
        {
          discountPercentage: promotion.discountPercentage,
          discountMaxAmount: promotion.discountMaxAmount,
          tipTopUpPercentage: promotion.tipTopUpPercentage,
        }
      )}
      <AppForm
        initialValues={{ termsAndConditions: false }}
        onSubmit={onFormSubmit}
        validationSchema={promotionSchema}
      >
        <CheckboxWrapper>
          <FormInputType.Checkbox
            name="termsAndConditions"
            label={intl.formatMessage(
              { id: promotion.modal.checkBoxText },
              {
                termsAndCondition: termsAndConditionsLink(
                  intl,
                  promotion.termsAndConditionsLink
                ),
              }
            )}
          />
        </CheckboxWrapper>
        <ButtonsWrapper>
          <SecondaryButton onClick={onCancel}>
            {intl.formatMessage({
              id: 'generic.cancel',
            })}
          </SecondaryButton>
          <SubmitButton
            title={intl.formatMessage({
              id: 'payment.raffleParticipation.participate.button',
            })}
          />
        </ButtonsWrapper>
      </AppForm>
    </>
  );
};
