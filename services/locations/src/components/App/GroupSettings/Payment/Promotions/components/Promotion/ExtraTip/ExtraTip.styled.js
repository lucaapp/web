import styled from 'styled-components';

export const ExtraTipWrapper = styled.div`
  text-align: right;
`;

export const StyledExtraTipText = styled.p`
  margin: 0;
`;

export const StyledExtraTipAmount = styled.p`
  font-weight: bold;
  color: green;
`;
