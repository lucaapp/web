import React from 'react';
import {
  DISCOUNT_CAMPAIGN_TERMS_LINK,
  RAFFLE_PARTICIPATION_TERMS_LINK,
} from 'constants/links';
import { CAMPAIGNS } from 'constants/campaigns';
import { Money, MoneyHand } from 'assets/icons';
import {
  useGetLucaCampaigns,
  useGetParticipatedCampaigns,
} from 'components/hooks';

export const usePromotions = locationGroupId => {
  const {
    data: lucaCampaigns,
    isLoading: isCampaignsLoading,
    error: isCampaignsError,
  } = useGetLucaCampaigns();

  const {
    isLoading,
    error,
    data: participatedCampaigns,
  } = useGetParticipatedCampaigns(locationGroupId);

  if (
    isLoading ||
    error ||
    isCampaignsLoading ||
    isCampaignsError ||
    !lucaCampaigns.length
  )
    return {};

  return lucaCampaigns
    .map(lucaCampaign => ({
      ...lucaCampaign,
      relationId: participatedCampaigns.find(
        ({ campaign }) => campaign === lucaCampaign.name
      )?.id,
    }))
    .filter(({ relationId, restricted }) => !restricted || relationId)
    .reduce((accumulator, current) => {
      if (current.name.startsWith(CAMPAIGNS.LUCA_DISCOUNT))
        return {
          ...accumulator,
          lucaDiscount: [...(accumulator.lucaDiscount || []), current],
        };
      if (current.name === CAMPAIGNS.PAY_RAFFLE_AND_TIPTOPUP)
        return {
          ...accumulator,
          payRaffleAndTipTopUp: current,
        };
      if (current.name.startsWith(CAMPAIGNS.LUCA_TIP_TOP_UP))
        return {
          ...accumulator,
          tipTopUp: [...(accumulator.tipTopUp || []), current],
        };
      return accumulator;
    }, {});
};

export const payRaffleAndTipTopUpCampaignPromotion = (
  campaign,
  locationGroup,
  highestTipTopUpCampaign
) => ({
  icon: <MoneyHand />,
  termsAndConditionsLink: RAFFLE_PARTICIPATION_TERMS_LINK,
  name: campaign.name,
  header: `payment.promotions.${CAMPAIGNS.PAY_RAFFLE_AND_TIPTOPUP}.header`,
  description: `payment.promotions.${CAMPAIGNS.PAY_RAFFLE_AND_TIPTOPUP}.description`,
  drawer: {
    description: `payment.promotions.${CAMPAIGNS.PAY_RAFFLE_AND_TIPTOPUP}.drawer.description`,
  },
  modal: {
    content: `payment.promotions.${CAMPAIGNS.PAY_RAFFLE_AND_TIPTOPUP}.modalContent`,
    checkBoxText: `payment.promotions.${CAMPAIGNS.PAY_RAFFLE_AND_TIPTOPUP}.modalCheckBox`,
  },
  startsAt: campaign.startsAt,
  endsAt: campaign.endsAt,
  tipTopUpPercentage: highestTipTopUpCampaign.tipTopUpPercentage,
  tipTopUpMaxAmount: highestTipTopUpCampaign.tipTopUpMaxAmount,
  locationGroupId: locationGroup.groupId,
  currentlyParticipating: !!campaign.relationId,
  relationId: campaign.relationId,
  restricted: campaign.restricted,
  tipTopUpCampaign: highestTipTopUpCampaign,
});

export const lucaDiscountPromotion = (campaign, locationGroup) => ({
  icon: <Money />,
  termsAndConditionsLink: DISCOUNT_CAMPAIGN_TERMS_LINK,
  name: campaign.name,
  header: `payment.promotions.${CAMPAIGNS.LUCA_DISCOUNT}.header`,
  description: `payment.promotions.${CAMPAIGNS.LUCA_DISCOUNT}.description`,
  drawer: {
    description: `payment.promotions.${CAMPAIGNS.LUCA_DISCOUNT}.drawer.description`,
  },
  modal: {
    content: `payment.promotions.${CAMPAIGNS.LUCA_DISCOUNT}.modalContent`,
    checkBoxText: `payment.promotions.${CAMPAIGNS.LUCA_DISCOUNT}.modalCheckBox`,
  },
  startsAt: campaign.startsAt,
  endsAt: campaign.endsAt,
  discountPercentage: campaign.discountPercentage,
  discountMaxAmount: campaign.discountMaxAmount,
  locationGroupId: locationGroup.groupId,
  currentlyParticipating: !!campaign.relationId,
  relationId: campaign.relationId,
  restricted: campaign.restricted,
});

export const termsAndConditionsLink = (intl, url) => (
  <a href={url} target="_blank" rel="noreferrer">
    {intl.formatMessage({
      id: 'payment.promotions.termsAndConditions',
    })}
  </a>
);
