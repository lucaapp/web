import React from 'react';

import { useIntl } from 'react-intl';

import {
  useFormatCurrencyByLang,
  useGetLocationGroupTipTopUpAmount,
} from 'components/hooks';

import {
  StyledExtraTipText,
  StyledExtraTipAmount,
  ExtraTipWrapper,
} from './ExtraTip.styled';

export const ExtraTip = ({ locationGroupId }) => {
  const intl = useIntl();
  const { formatCurrencyByLang } = useFormatCurrencyByLang();
  const { data } = useGetLocationGroupTipTopUpAmount(locationGroupId);

  if (!data) return null;

  return (
    <ExtraTipWrapper>
      <StyledExtraTipText>
        {intl.formatMessage({
          id: 'payment.promotions.PayRaffleAndTipTopUp.tipSince',
        })}
      </StyledExtraTipText>
      <StyledExtraTipAmount>{`+ ${formatCurrencyByLang(
        data.tipTopUpAmount
      )}`}</StyledExtraTipAmount>
    </ExtraTipWrapper>
  );
};
