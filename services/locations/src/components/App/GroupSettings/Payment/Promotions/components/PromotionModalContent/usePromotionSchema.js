import * as Yup from 'yup';
import { useIntl } from 'react-intl';

export const usePromotionSchema = () => {
  const intl = useIntl();

  return Yup.object().shape({
    termsAndConditions: Yup.bool().oneOf(
      [true],
      intl.formatMessage(
        {
          id: 'payment.promotions.modalContent',
        },
        {
          termsAndCondition: intl.formatMessage({
            id: 'payment.promotions.termsAndConditions',
          }),
        }
      )
    ),
  });
};
