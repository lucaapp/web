import React from 'react';
import { queryDoNotRetryOnCode } from 'network';
import { PaymentHistory } from 'components/general/PaymentHistory';
import { KYB_STATUS, PAYOUT_STATUS } from 'constants/paymentOnboarding';
import {
  useGetPaymentLocationGroup,
  useGetPaymentOperator,
} from 'components/hooks/useQueries';
import { PoweredByRapyd } from 'components/general/PoweredByRapyd';
import { useFeatureFlags, useOperator } from 'components/hooks';
import { groupEnumTypes } from 'components/general/locationGroupOptions';
import { PayoutHistory } from 'components/general/PayoutHistory';
import { ActivatePayments } from './ActivatePayments';
import { PayoutConfiguration } from './PayoutConfiguration';
import { PayoutBalance } from './PayoutBalance';
import { PaymentOnboardingRejected } from './PaymentOnboardingRejected';
import { PaymentOnboarding } from './PaymentOnboarding';
import { PayoutSetupCard } from './PayoutSetupCard';
import { PaymentKybSkippedCard } from './PaymentKybSkippedCard';
import { POSIntegration } from './POSIntegration';
import { InvoiceData } from './InvoiceData';
import { Promotions } from './Promotions';

// eslint-disable-next-line complexity
export const PaymentStep = ({
  locationGroup, // luca-web location group
  paymentLocationGroup, // can be null, payment backen locationGroup object
  paymentOperator, // payment backend operator object
}) => {
  if (!locationGroup || !paymentOperator) return null;

  const payoutDetails = paymentLocationGroup
    ? paymentLocationGroup.payoutDetails
    : { iban: '' };
  const kybStatus = paymentLocationGroup
    ? paymentLocationGroup?.kybStatus
    : KYB_STATUS.NOT_STARTED;
  const payoutStatus = paymentLocationGroup
    ? paymentLocationGroup?.payoutStatus
    : PAYOUT_STATUS.NOT_STARTED;

  const { skipKybOnboarding } = paymentOperator;

  const isKybSkipped =
    paymentLocationGroup?.paymentAllowed &&
    [KYB_STATUS.NOT_STARTED, KYB_STATUS.PENDING, KYB_STATUS.SUBMITTED].includes(
      paymentLocationGroup?.kybStatus
    );

  if (
    !paymentLocationGroup ||
    (!skipKybOnboarding &&
      (kybStatus === KYB_STATUS.NOT_STARTED ||
        kybStatus === KYB_STATUS.PENDING))
  ) {
    // create location group and start kyb onboarding
    return (
      <PaymentOnboarding
        payOperator={paymentOperator}
        locationGroup={locationGroup}
        payLocationGroup={paymentLocationGroup}
      />
    );
  }

  return (
    <>
      {isKybSkipped && (
        <PaymentKybSkippedCard payLocationGroup={paymentLocationGroup} />
      )}
      {kybStatus === KYB_STATUS.REJECTED && <PaymentOnboardingRejected />}
      {!skipKybOnboarding && kybStatus === KYB_STATUS.SUBMITTED && (
        <ActivatePayments currentStep={0} locationGroup={locationGroup} />
      )}
      {payoutStatus === PAYOUT_STATUS.CONFIGURED && (
        <PayoutBalance locationGroupId={locationGroup.groupId} />
      )}
      {(payoutStatus === PAYOUT_STATUS.NOT_STARTED ||
        payoutStatus === PAYOUT_STATUS.PENDING) && (
        <PayoutSetupCard
          locationGroup={locationGroup}
          payoutStatus={payoutStatus}
        />
      )}
      {payoutStatus === PAYOUT_STATUS.CONFIGURED && (
        <PayoutConfiguration
          locationGroup={locationGroup}
          payoutDetails={payoutDetails}
        />
      )}
      <PaymentHistory locationGroupId={locationGroup.groupId} />
      {payoutStatus === PAYOUT_STATUS.CONFIGURED && (
        <PayoutHistory locationGroupId={locationGroup.groupId} />
      )}
      <InvoiceData paymentLocationGroup={paymentLocationGroup} />
    </>
  );
};

export const Payment = ({ locationGroup }) => {
  const featureFlag = useFeatureFlags();
  const { data: operator } = useOperator();

  const {
    isLoading: isLocationGroupLoading,
    error: LocationGroupError,
    data: paymentLocationGroup,
  } = useGetPaymentLocationGroup(locationGroup.groupId, {
    retry: queryDoNotRetryOnCode([401, 404]),
  });

  const {
    isLoading: isPayOperatorLoading,
    error: payOperatorError,
    data: paymentOperator,
  } = useGetPaymentOperator(operator.operatorId);

  if (
    isLocationGroupLoading ||
    isPayOperatorLoading ||
    LocationGroupError ||
    payOperatorError
  ) {
    // todo check alternative handling or notifications
    return null;
  }

  const showRaffleParticipation =
    (paymentLocationGroup &&
      paymentLocationGroup.kybStatus === KYB_STATUS.COMPLETED) ||
    (paymentOperator && paymentOperator.skipKybOnboarding);

  return (
    <>
      <PoweredByRapyd />
      {showRaffleParticipation &&
        locationGroup.type === groupEnumTypes.RESTAURANT_TYPE && (
          <Promotions locationGroup={locationGroup} />
        )}
      <PaymentStep
        locationGroup={locationGroup}
        paymentLocationGroup={paymentLocationGroup}
        paymentOperator={paymentOperator}
      />
      {featureFlag.pos_integration && (
        <POSIntegration locationGroup={locationGroup} />
      )}
    </>
  );
};
