import React, { useState } from 'react';
import { patchLocation } from 'network/payment';
import { useIntl } from 'react-intl';
import { useNotifications } from 'components/hooks';
import { Popconfirm } from 'antd';
import { DangerButton } from 'components/general';
import {
  getButtonText,
  getTitleText,
} from '../PayoutConfiguration/PayoutConfiguration.helper';

export const DeactivateLucaPayButton = ({ locationGroup }) => {
  const intl = useIntl();
  const { successMessage, errorMessage } = useNotifications();
  const [isInProgress, setInProgress] = useState(false);

  const onConfirm = async () => {
    const promises = [];
    for (const location of locationGroup.locations) {
      promises.push(patchLocation(location.uuid, false));
    }

    setInProgress(true);

    try {
      await Promise.all(promises);
      setInProgress(false);
      successMessage(
        'payment.location.payoutConfiguration.deactivationSuccess.description',
        'payment.location.payoutConfiguration.deactivationSuccess.header'
      );
    } catch {
      errorMessage('payment.location.payoutConfiguration.deactivationError');
      setInProgress(false);
    }
  };

  return (
    <Popconfirm
      placement="bottomRight"
      title={getTitleText('deactivateBankAccount', intl)}
      okText={getButtonText('deactivateBankAccount', 'ok', intl)}
      cancelText={intl.formatMessage({ id: 'generic.cancel' })}
      onConfirm={onConfirm}
    >
      <DangerButton loading={isInProgress}>
        {intl.formatMessage({
          id:
            'payment.location.payoutConfiguration.button.deactivateBankAccount',
        })}
      </DangerButton>
    </Popconfirm>
  );
};
