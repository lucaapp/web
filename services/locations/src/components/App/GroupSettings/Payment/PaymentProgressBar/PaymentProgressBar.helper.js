import { formatCurrency } from 'components/App/shared/FormatHelpers/Format.helpers';

export const formatProgressBarSteps = (intl, numbers) =>
  numbers.map(number => formatCurrency(intl, number));
