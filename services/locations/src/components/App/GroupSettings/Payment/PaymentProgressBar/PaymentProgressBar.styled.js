import styled from 'styled-components';
import { Progress } from 'antd';

export const StyledProgress = styled(Progress)`
  margin: 10px 0;

  .ant-progress-inner {
    border: 1px solid;
  }
`;

export const StyledProgressLegend = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin-top: -10px;
  font-size: 12px;
  font-family: 'Montserrat-Medium', sans-serif;
`;

export const StyledCenterIndicator = styled.div`
  width: 5px;
  border-top: 1px solid;
  transform: rotate(90deg);
  margin: auto;
  right: 20px;
  top: -13px;
  position: relative;
`;

export const StyledPaymentProgressBarWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
