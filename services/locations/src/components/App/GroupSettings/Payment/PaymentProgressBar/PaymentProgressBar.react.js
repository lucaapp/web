import React from 'react';
import {
  StyledCenterIndicator,
  StyledPaymentProgressBarWrapper,
  StyledProgress,
  StyledProgressLegend,
} from './PaymentProgressBar.styled';

export const PaymentProgressBar = ({ percent, steps }) => {
  return (
    <StyledPaymentProgressBarWrapper>
      <StyledProgress percent={percent} showInfo={false} />
      <StyledCenterIndicator />
      <StyledProgressLegend>
        {steps.map(step => (
          <div key={step}>{step}</div>
        ))}
      </StyledProgressLegend>
    </StyledPaymentProgressBarWrapper>
  );
};
