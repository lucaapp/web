import styled from 'styled-components';

export const FieldRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  gap: 40px;
  width: 100%;
`;

export const styledInput = {
  borderRadius: 0,
  border: '0.5px solid rgb(0, 0, 0)',
  height: '40px',
  with: '100%',
};

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 24px;
  margin-bottom: -32px;

  @media (max-width: 1096px) {
    margin-top: 40px;
    flex-flow: column-reverse;

    button:not(:last-child) {
      margin-top: 24px;
    }
  }
`;

export const Wrapper = styled.div`
  padding: 0 16px;
`;
