import React from 'react';
import { useIntl } from 'react-intl';

import { QUERY_KEYS, useNotifications } from 'components/hooks';
import { removePOSSystem } from 'network/pos';
import { useQueryClient } from 'react-query';
import { StyledSecondaryButton } from './RemovePOSSystem.styled';

export const RemovePOSSystem = ({ groupId, pos }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();

  const { systemId } = pos?.systems[0] || '';

  const { successMessage, errorMessage } = useNotifications();

  const onRemovePOSSystem = () => {
    removePOSSystem(groupId, systemId)
      .then(response => {
        if (response.status !== 204) {
          errorMessage();
          return;
        }
        queryClient.invalidateQueries(QUERY_KEYS.POS_SYSTEMS);
        successMessage('payment.posIntegration.remove.success');
      })
      .catch(() => errorMessage('payment.posIntegration.submit.error'));
  };

  return (
    <StyledSecondaryButton
      onClick={onRemovePOSSystem}
      disabled={!pos?.systems.length > 0}
    >
      {intl.formatMessage({ id: 'payment.posIntegration.form.remove' })}
    </StyledSecondaryButton>
  );
};
