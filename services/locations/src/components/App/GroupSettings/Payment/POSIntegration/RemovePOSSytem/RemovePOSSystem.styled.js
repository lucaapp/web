import styled from 'styled-components';
import { SecondaryButton } from 'components/general';

export const StyledSecondaryButton = styled(SecondaryButton)`
  margin-right: 16px;
`;
