import React from 'react';
import { useIntl } from 'react-intl';

import { LocationCard } from 'components/general';
import { useQuery, useQueryClient } from 'react-query';

import { createPOSSystem, editPOSSystem, getPOSSystems } from 'network/pos';
import { QUERY_KEYS, useNotifications } from 'components/hooks';

import { AppForm, SubmitButton, FormInputType } from 'components/general/Form';
import { ButtonWrapper, FieldRow, Wrapper } from './POSIntegration.styled';
import { fieldItems, POS_TYPE } from './POSIntegration.helper';
import { usePOSIntegrationValidationSchema } from './POSIntegration.schema';

import { RemovePOSSystem } from './RemovePOSSytem';

export const POSIntegration = ({ locationGroup }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const posIntegrationValidation = usePOSIntegrationValidationSchema();
  const { groupId } = locationGroup;

  const { successMessage, errorMessage } = useNotifications();

  const { data: pos, isLoading, error } = useQuery(
    [QUERY_KEYS.POS_SYSTEMS, groupId],
    () => getPOSSystems(groupId)
  );

  if (isLoading || error) return null;

  // pos?.systems[0] we get the first item of the array because we still do not have a patch route yet.
  // Thus be aware for later on that this might get change.
  const { systemId, type } = pos?.systems[0] || '';
  const { apiKey, businessUnitId, operatorId } =
    pos?.systems[0]?.credentials || '';

  const onSubmit = values => {
    const payload = {
      type: type || POS_TYPE,
      credentials: {
        apiKey: values.apiKey,
        businessUnitId: parseInt(values.businessUnitId, 10),
        operatorId: parseInt(values.operatorId, 10),
      },
    };

    const POSApiCall = !systemId
      ? createPOSSystem(groupId, payload)
      : editPOSSystem(groupId, systemId, payload);

    POSApiCall.then(response => {
      if (!systemId && response.status !== 200) {
        errorMessage('payment.posIntegration.submit.error');
        return;
      }
      if (!!systemId && response.status !== 204) {
        errorMessage('payment.posIntegration.submit.edit.error');
        return;
      }

      queryClient.invalidateQueries(QUERY_KEYS.POS_SYSTEMS);
      successMessage(
        !systemId
          ? 'payment.posIntegration.submit.success'
          : 'payment.posIntegration.submit.edit.success'
      );
    }).catch(() => errorMessage('payment.posIntegration.submit.error'));
  };

  return (
    <LocationCard
      title={intl.formatMessage({ id: 'payment.posIntegration.card.header' })}
    >
      <Wrapper>
        <AppForm
          initialValues={{
            apiKey: apiKey || '',
            businessUnitId: businessUnitId || '',
            operatorId: operatorId || '',
          }}
          onSubmit={onSubmit}
          validationSchema={posIntegrationValidation}
          enableReinitialize
        >
          {fieldItems.map(fieldItem => (
            <FieldRow
              key={fieldItem.NAMED_KEY}
              className="POSIntegrationFormItem"
            >
              <FormInputType.Input
                name={fieldItem.NAMED_KEY}
                labelId={fieldItem.TRANSLATION_ID}
                required
              />
            </FieldRow>
          ))}
          <ButtonWrapper>
            <RemovePOSSystem groupId={groupId} pos={pos} />
            <SubmitButton
              title={intl.formatMessage({
                id: !systemId
                  ? 'payment.posIntegration.form.save'
                  : 'payment.posIntegration.submit.edit.button',
              })}
            />
          </ButtonWrapper>
        </AppForm>
      </Wrapper>
    </LocationCard>
  );
};
