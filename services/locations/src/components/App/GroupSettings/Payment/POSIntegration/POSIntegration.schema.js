import * as Yup from 'yup';
import { useIntl } from 'react-intl';

export const usePOSIntegrationValidationSchema = () => {
  const intl = useIntl();

  const errorMissingFiled = intl.formatMessage({
    id: 'createGroup.errorMissingField',
  });

  const errorNumberType = intl.formatMessage({
    id: 'payment.posIntegration.error.number',
  });

  return Yup.object().shape({
    apiKey: Yup.string().required(errorMissingFiled),
    businessUnitId: Yup.number()
      .typeError(errorNumberType)
      .required(errorMissingFiled),
    operatorId: Yup.number()
      .typeError(errorNumberType)
      .required(errorMissingFiled),
  });
};
