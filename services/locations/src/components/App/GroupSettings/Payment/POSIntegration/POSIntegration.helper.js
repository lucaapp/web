export const POS_TYPE = 'GASTROFIX';

export const fieldItems = [
  {
    NAMED_KEY: 'apiKey', // API Key
    TRANSLATION_ID: 'payment.posIntegration.apikey',
  },
  {
    NAMED_KEY: 'businessUnitId', // Business Unit ID
    TRANSLATION_ID: 'payment.posIntegration.businessUnitId',
  },
  {
    NAMED_KEY: 'operatorId', // Operator ID (not from luca)
    TRANSLATION_ID: 'payment.posIntegration.operatorId',
  },
];
