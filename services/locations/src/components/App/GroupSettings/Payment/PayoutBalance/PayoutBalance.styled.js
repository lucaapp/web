import styled from 'styled-components';
import { QuestionCircleOutlined } from '@ant-design/icons';

export const CollapsableWrapper = styled.div`
  padding: 12px 16px 0 16px;
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const DateTitle = styled.p`
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
`;

export const MoneyAmount = styled.div`
  font-size: 24px;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 24px;
`;

export const StyledQuestionCircleOutlined = styled(QuestionCircleOutlined)`
  color: red;
`;
