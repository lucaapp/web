import React, { useState } from 'react';
import { Popconfirm } from 'antd';
import { useIntl } from 'react-intl';
import { initiatePayoutOnboarding } from 'network/payment';
import { queryDoNotRetryOnCode } from 'network/utils';
import {
  useGetPaymentOnboardingStatus,
  useNotifications,
} from 'components/hooks';
import { ExternalLinkButton } from '../ActivatePayments/ActivatePayments.styled';
import { ButtonWrapper } from './AddBankDetailsButton.styled';

export const AddBankDetailsButton = ({ locationGroup }) => {
  const intl = useIntl();
  const { errorMessage } = useNotifications();
  const [isLoading, setIsLoading] = useState(false);

  const {
    isOnboardingStatusLoading,
    data: onboardingStatus,
  } = useGetPaymentOnboardingStatus(locationGroup.groupId, {
    retry: queryDoNotRetryOnCode([401, 404]),
  });

  const displayErrorNotification = () => {
    errorMessage('payment.location.paymentAccountDetails.error');
  };

  const accountDetailsURLHandler = () => {
    if (onboardingStatus?.payoutUrl) {
      window.open(onboardingStatus.payoutUrl);
      return;
    }
    setIsLoading(true);
    initiatePayoutOnboarding(locationGroup.groupId)
      .then(response => response.json())
      .then(data => {
        setIsLoading(false);
        if (data?.payoutUrl) {
          window.open(data.payoutUrl);
        } else {
          displayErrorNotification();
        }
      })
      .catch(() => {
        setIsLoading(false);
        displayErrorNotification();
      });
  };

  return (
    <ButtonWrapper>
      <Popconfirm
        placement="bottomRight"
        title={intl.formatMessage(
          {
            id:
              'payment.location.accountDetails.button.popConfirm.addBankDetails.text',
          },
          {
            br: <br />,
          }
        )}
        okText={intl.formatMessage({
          id:
            'payment.location.accountDetails.button.popConfirm.addBankDetails.ok',
        })}
        cancelText={intl.formatMessage({
          id: 'generic.cancel',
        })}
        onConfirm={accountDetailsURLHandler}
      >
        <ExternalLinkButton loading={isOnboardingStatusLoading || isLoading}>
          {intl.formatMessage({
            id: 'payment.location.accountDetails.button',
          })}
        </ExternalLinkButton>
      </Popconfirm>
    </ButtonWrapper>
  );
};
