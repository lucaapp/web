import styled from 'styled-components';
import { Collapse, Divider } from 'antd';

export const ContentWrapper = styled.div`
  padding: 0 16px;
  position: relative;
`;

export const ActionWrapper = styled.div`
  display: flex;
  justify-content: end;
`;

export const Text = styled.p`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
`;

export const StyledDivider = styled(Divider)`
  border-top: 1px solid #d8d8d8;
`;

export const StyledContentCollapse = styled(Collapse)`
  background: none;
  border: none;

  .ant-collapse-content,
  > .ant-collapse-item {
    border: none;
  }

  .ant-collapse-header {
    width: auto;
    position: absolute !important;
    top: -32px;
    right: 24px;
    padding: 0 !important;
  }

  .ant-collapse-content-box {
    padding: 0 !important;
    margin-top: 16px;
  }
`;
