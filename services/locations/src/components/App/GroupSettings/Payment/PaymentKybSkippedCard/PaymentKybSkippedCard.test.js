import React from 'react';
import userEvent from '@testing-library/user-event';
import { act, cleanup } from '@testing-library/react';
import { render } from 'utils/testing';
import { fireEvent } from '@testing-library/dom';
import * as paymentApi from 'network/payment';
import { PaymentKybSkippedCard } from './PaymentKybSkippedCard.react';

afterEach(cleanup);

jest.mock('network/payment');
paymentApi.startRapydKybProcessWithPhoneNumber.mockResolvedValue('success');

describe('PaymentKybSkippedCard', () => {
  it('Shows phone number input and disabled activation button when no valid phone number', async () => {
    const payLocationGroup = {
      kybStatus: 'NOT_STARTED',
      kybUrl: '',
      uuid: '48a5bff2-a0ca-4a2c-aa6e-22da77a5f08f',
    };
    const { container, findByTestId } = render(
      <PaymentKybSkippedCard payLocationGroup={payLocationGroup} />
    );
    const user = userEvent.setup();

    const collapsableContentHeader = document.querySelector(
      '.ant-collapse-header'
    );
    expect(collapsableContentHeader).toBeDefined();

    collapsableContentHeader.click();

    const phoneInput = container.querySelector("input[name='phone']");
    expect(phoneInput).toBeDefined();
    const submitButton = await findByTestId('activatePayment');
    await act(async () => {
      await user.type(phoneInput, '+');
    });

    expect(submitButton).toBeDisabled();
    // eslint-disable-next-line require-await
    await act(async () => {
      fireEvent.click(submitButton);
    });
    expect(
      paymentApi.startRapydKybProcessWithPhoneNumber
    ).not.toHaveBeenCalled();

    await act(async () => {
      await user.type(phoneInput, '491701231231');
    });

    expect(submitButton).not.toBeDisabled();
    // eslint-disable-next-line require-await
    await act(async () => {
      fireEvent.click(submitButton);
    });

    await expect(
      paymentApi.startRapydKybProcessWithPhoneNumber
    ).toHaveBeenCalledWith(payLocationGroup.uuid, '+491701231231');
  });
});
