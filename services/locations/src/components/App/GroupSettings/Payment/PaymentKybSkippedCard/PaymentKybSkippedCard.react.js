import React from 'react';
import { Collapse } from 'antd';
import { useIntl } from 'react-intl';
import { KYB_STATUS } from 'constants/paymentOnboarding';
import { PaymentStartKybOnboarding } from 'components/App/GroupSettings/Payment/PaymentOnboarding/PaymentStartKybOnboarding';
import { LocationCard, ArrowIcon } from 'components/general';
import {
  ActionWrapper,
  ContentWrapper,
  StyledContentCollapse,
  Text,
} from './PaymentKybSkippedCard.styled';
import { PaymentProgressBar } from '../PaymentProgressBar';
import { formatProgressBarSteps } from '../PaymentProgressBar/PaymentProgressBar.helper';

const PANEL_KEY = 'PaymentKybSkippedCardContent';
const showTotalRevenue = false;

export const PaymentKybSkippedCard = ({ payLocationGroup }) => {
  const intl = useIntl();
  const { Panel } = Collapse;

  const { totalRevenue, revenueWithoutKybLimit } = payLocationGroup;

  const percentOfLimit = totalRevenue / (revenueWithoutKybLimit / 100);

  const textForKybStatus = status => {
    switch (status) {
      case KYB_STATUS.PENDING:
        return 'payment.activate.descriptionActivated';
      case KYB_STATUS.SUBMITTED:
        return 'payment.location.rapydWalletActivation.text';
      default:
        return null;
    }
  };

  const kybStatusTextMessageId = textForKybStatus(payLocationGroup.kybStatus);
  const startKybButtonNeeded = [
    KYB_STATUS.NOT_STARTED,
    KYB_STATUS.PENDING,
  ].includes(payLocationGroup.kybStatus);

  const paymentProgressNumbers = [0, 10000, 20000];
  const paymentProgressSteps = formatProgressBarSteps(
    intl,
    paymentProgressNumbers
  );

  return (
    <LocationCard
      title={intl.formatMessage({ id: 'payment.location.kybskipped.title' })}
    >
      <ContentWrapper>
        {showTotalRevenue && (
          <>
            <Text>
              {intl.formatMessage(
                {
                  id: 'payment.location.kybskipped.currentRevenueOfLimit',
                },
                {
                  current: totalRevenue || 0,
                  limit: revenueWithoutKybLimit,
                }
              )}
            </Text>
            <PaymentProgressBar
              percent={percentOfLimit}
              steps={paymentProgressSteps}
            />
          </>
        )}
        <StyledContentCollapse
          accordion
          isOpen={false}
          expandIcon={({ isActive }) => <ArrowIcon isActive={isActive} />}
        >
          <Panel header="" key={PANEL_KEY}>
            <Text>
              {intl.formatMessage({
                id: 'payment.location.kybskipped.description',
              })}
            </Text>

            {kybStatusTextMessageId && (
              <Text>{intl.formatMessage({ id: kybStatusTextMessageId })}</Text>
            )}

            {startKybButtonNeeded && (
              <ActionWrapper>
                <PaymentStartKybOnboarding
                  payLocationGroup={payLocationGroup}
                  withPhoneNumber
                />
              </ActionWrapper>
            )}
          </Panel>
        </StyledContentCollapse>
      </ContentWrapper>
    </LocationCard>
  );
};
