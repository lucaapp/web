import React from 'react';

export const getTitleText = (title, intl) =>
  intl.formatMessage(
    {
      id: `payment.location.payoutConfiguration.button.popConfirm.${title}.text`,
    },
    {
      br: <br />,
    }
  );

export const getButtonText = (button, status, intl) =>
  intl.formatMessage({
    id: `payment.location.payoutConfiguration.button.popConfirm.${button}.${status}`,
  });
