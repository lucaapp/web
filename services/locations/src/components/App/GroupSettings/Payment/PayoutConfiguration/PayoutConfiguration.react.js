import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Popconfirm } from 'antd';
import { changePayoutOnboarding } from 'network/payment';
import { LocationCard } from 'components/general';
import { useNotifications } from 'components/hooks';
import {
  CollapsableWrapper,
  Content,
  ContentWrapper,
  DetailsContent,
  ExternalLinkButton,
  Title,
  ButtonWrapper,
} from './PayoutConfiguration.styled';
import { getTitleText, getButtonText } from './PayoutConfiguration.helper';
import { DeactivateLucaPayButton } from '../DeactivateLucaPayButton';

export const PayoutConfiguration = ({ locationGroup, payoutDetails }) => {
  const intl = useIntl();
  const { errorMessage } = useNotifications();
  const [isInProgress, setInProgress] = useState(false);

  if (!locationGroup) {
    return null;
  }

  const onChangeDetailsHandler = () => {
    setInProgress(true);
    changePayoutOnboarding(locationGroup.groupId)
      .then(response => response.json())
      .then(data => {
        window.open(data.payoutUrl);
        setInProgress(false);
      })
      .catch(() => {
        errorMessage(
          'payment.location.payoutConfiguration.changeBankDetails.error'
        );

        setInProgress(false);
      });
  };

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({
        id: 'payment.location.payoutConfiguration.title',
      })}
    >
      <CollapsableWrapper>
        <ContentWrapper>
          <Content>
            {intl.formatMessage({
              id: 'payment.location.payoutConfiguration.text',
            })}
          </Content>
        </ContentWrapper>
        <ContentWrapper>
          <Title>
            {intl.formatMessage({
              id: 'payment.location.payoutConfiguration.label.iban',
            })}
          </Title>
          <DetailsContent>{payoutDetails?.iban || ''}</DetailsContent>
        </ContentWrapper>
        <ButtonWrapper>
          <DeactivateLucaPayButton locationGroup={locationGroup} />
          <Popconfirm
            placement="bottomRight"
            title={getTitleText('changeBankDetails', intl)}
            okText={getButtonText('changeBankDetails', 'ok', intl)}
            cancelText={intl.formatMessage({
              id: 'generic.cancel',
            })}
            onConfirm={onChangeDetailsHandler}
          >
            <ExternalLinkButton loading={isInProgress}>
              {intl.formatMessage({
                id:
                  'payment.location.payoutConfiguration.button.changeBankDetails',
              })}
            </ExternalLinkButton>
          </Popconfirm>
        </ButtonWrapper>
      </CollapsableWrapper>
    </LocationCard>
  );
};
