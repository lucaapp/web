import React from 'react';
import { FormInputType } from 'components/general/Form';
import { FieldRow } from './InvoiceForm.styled';

export const InvoiceForm = () => {
  return (
    <div className="invoiceForm">
      <FieldRow>
        <FormInputType.Input
          name="companyName"
          labelId="createGroup.companyName"
          dataCy="companyName"
          required
        />
        <FormInputType.Input
          name="vatNumber"
          labelId="createGroup.vatNumber"
          dataCy="vatNumber"
        />
      </FieldRow>
      <FieldRow>
        <FormInputType.Input
          name="businessFirstName"
          labelId="createGroup.businessFirstName"
          dataCy="businessFirstName"
          required
        />
        <FormInputType.Input
          name="businessLastName"
          labelId="createGroup.businessLastName"
          dataCy="businessLastName"
          required
        />
      </FieldRow>

      <FieldRow>
        <FormInputType.Input
          name="street"
          labelId="createGroup.street"
          dataCy="street"
          required
        />
        <FormInputType.Input
          name="streetNumber"
          labelId="createGroup.streetNumber"
          dataCy="streetNumber"
          required
        />
      </FieldRow>
      <FieldRow>
        <FormInputType.Input
          name="zipCode"
          labelId="createGroup.zipCode"
          dataCy="zipCode"
          required
        />
        <FormInputType.Input
          name="city"
          labelId="createGroup.city"
          dataCy="city"
          required
        />
      </FieldRow>
      <FieldRow>
        <FormInputType.Input
          name="invoiceEmail"
          labelId="createGroup.invoiceEmail"
          dataCy="invoiceEmail"
          required
        />
      </FieldRow>
    </div>
  );
};
