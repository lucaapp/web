import styled from 'styled-components';

export const FieldRow = styled.div`
  display: flex;
  flex-direction: row;
  gap: 40px;
  width: 100%;

  & > * {
    width: 100%;
  }
`;
