import React from 'react';
import { useIntl } from 'react-intl';
import { PaymentInfo } from './PaymentInfo';
import { PaymentCreateLocationGroup } from './PaymentCreateLocationGroup';
import { PaymentStartKybOnboarding } from './PaymentStartKybOnboarding';
import {
  Container,
  Description,
  StyledDivider,
  Title,
  Wrapper,
} from './PaymentOnboarding.styled';

export const PaymentOnboarding = ({
  payOperator,
  locationGroup,
  payLocationGroup,
}) => {
  const intl = useIntl();

  // payment location group does not exist yet
  const needsGroupCreation = !payLocationGroup;

  return (
    <Wrapper>
      <PaymentInfo />
      <StyledDivider />
      <Container>
        <Title>{intl.formatMessage({ id: 'payment.activate.title' })}</Title>
        <Description>
          {intl.formatMessage({
            id: needsGroupCreation
              ? 'payment.activate.description'
              : 'payment.activate.descriptionActivated',
          })}
        </Description>
        {needsGroupCreation ? (
          <PaymentCreateLocationGroup
            payOperator={payOperator}
            locationGroup={locationGroup}
          />
        ) : (
          <PaymentStartKybOnboarding payLocationGroup={payLocationGroup} />
        )}
      </Container>
    </Wrapper>
  );
};
