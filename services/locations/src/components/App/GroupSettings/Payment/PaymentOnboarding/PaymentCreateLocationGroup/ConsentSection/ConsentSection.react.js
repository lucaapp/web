import React from 'react';

import { FormInputType } from 'components/general/Form';

import { PAYMENT_TERMS_CONDITIONS_LINK } from 'constants/links';
import { PAYMENT_DPA_PDF } from 'constants/pdf';
import AVV from 'assets/documents/AVV_Luca.pdf';

import { useIntl } from 'react-intl';
import { ConsentWrapper } from '../PaymentCreateLocationGroup.styled';
import {
  getConsentBoxLabel,
  getDownloadConsentBoxLabel,
} from './ConsentSection.helper';

export const ConsentSection = () => {
  const intl = useIntl();

  const termsLabel = getConsentBoxLabel(
    intl,
    'payment.activePayment.consentCheckbox.acceptTermsAndConditions',
    PAYMENT_TERMS_CONDITIONS_LINK
  );

  const dpaLabel = getDownloadConsentBoxLabel(
    intl,
    'payment.activePayment.consentCheckbox.acceptDPA',
    AVV,
    PAYMENT_DPA_PDF
  );

  return (
    <>
      <ConsentWrapper>
        <FormInputType.Checkbox name="dpa" label={dpaLabel} />
      </ConsentWrapper>
      <ConsentWrapper>
        <FormInputType.Checkbox name="termsAndConditions" label={termsLabel} />
      </ConsentWrapper>
    </>
  );
};
