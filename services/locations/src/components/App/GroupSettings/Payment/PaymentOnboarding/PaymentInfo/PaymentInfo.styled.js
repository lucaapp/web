import styled from 'styled-components';

export const Container = styled.div`
  padding: 24px 32px;
`;

export const PaymentTitle = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
`;

export const ActivateTitle = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin: 32px 0 16px 0;
`;

export const PaymentDescription = styled(ActivateTitle)`
  margin: 16px 0;
`;

export const PaymentLink = styled.a`
  color: rgb(80, 102, 124);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  display: block;
  text-decoration: none;
`;

export const Conditions = styled.ul``;

export const Condition = styled.li`
  font-size: 14px;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  list-style: disc;
`;

export const ActivateDescription = styled(ActivateTitle)`
  margin: 16px 0 16px 0;
`;
