import React from 'react';
import { useIntl } from 'react-intl';

import { LOCATION_RAPYD_LINK } from 'constants/links';
import {
  Container,
  ActivateDescription,
  ActivateTitle,
  Condition,
  Conditions,
  PaymentDescription,
  PaymentLink,
  PaymentTitle,
} from './PaymentInfo.styled';

export const PaymentInfo = () => {
  const intl = useIntl();

  return (
    <Container>
      <PaymentTitle>
        {intl.formatMessage({ id: 'payment.location.title' })}
      </PaymentTitle>
      <PaymentDescription>
        {intl.formatMessage({ id: 'payment.location.description' })}
      </PaymentDescription>
      <PaymentLink
        href={LOCATION_RAPYD_LINK}
        target="_blank"
        rel="noopener noreferrer"
      >
        {intl.formatMessage({ id: 'payment.location.link' })}
      </PaymentLink>
      <ActivateTitle>
        {intl.formatMessage({ id: 'payment.location.activateTitle' })}
      </ActivateTitle>
      <Conditions>
        <Condition>
          {intl.formatMessage({ id: 'payment.location.conditionBusiness' })}
        </Condition>
        <Condition>
          {intl.formatMessage({ id: 'payment.location.conditionRegNumber' })}
        </Condition>
        <Condition>
          {intl.formatMessage({
            id: 'payment.location.conditionBusinessOwner',
          })}
        </Condition>
        <Condition>
          {intl.formatMessage({ id: 'payment.location.conditionDocumentId' })}
        </Condition>
      </Conditions>
      <ActivateDescription>
        {intl.formatMessage(
          { id: 'payment.location.activateDescription' },
          { br: <br /> }
        )}
      </ActivateDescription>
    </Container>
  );
};
