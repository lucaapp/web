import styled from 'styled-components';
import { PrimaryButton } from 'components/general';
import { Input } from 'antd';

export const StyledPrimaryButton = styled(PrimaryButton)`
  background: rgb(255, 220, 95);
  border: 2px solid rgb(255, 220, 95);
  margin-bottom: 32px;

  &:hover,
  &:active,
  &:focus,
  &:disabled,
  &:focus-visible {
    background: rgb(255, 220, 95) !important;
    border: 2px solid rgb(255, 220, 95) !important;
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 20px;
`;

export const inputStyles = {
  borderRadius: 0,
  border: '1px solid rgb(0, 0, 0)',
  height: '40px',
};

export const StyledInput = styled(Input)`
  border-radius: 0;
  border: 0.5px solid rgb(0, 0, 0);
  height: 40px;

  :focus,
  :hover,
  :active {
    border-radius: 0;
    border: 0.5px solid rgb(0, 0, 0);
    height: 40px;
  }
`;
