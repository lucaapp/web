import { useIntl } from 'react-intl';
import React, { useState } from 'react';
import { whitespaceTrim } from 'utils/stringHelper';
import {
  createLocationGroup,
  acceptPaymentTerms,
  patchLocation,
  startRapydKybProcess,
} from 'network/payment';
import { FormInputType, AppForm } from 'components/general/Form';
import { useQueryClient } from 'react-query';
import { QUERY_KEYS, useOperator } from 'components/hooks';

import { useActivatePaymentSchema } from 'components/general/Form/hooks';
import { SubmitButton } from 'components/general/Form/AppForm/SubmitButton';
import { paymentPrimaryButtonTheme } from 'components/general';
import { InvoiceForm } from 'components/App/GroupSettings/Payment/common';
import { ACTIVATE_PAYMENT_HELP_LINK } from 'constants/links';
import {
  ButtonWrapper,
  ErrorText,
  StyledHelpLink,
  StyledTitle,
} from './PaymentCreateLocationGroup.styled';
import { ConsentSection } from './ConsentSection';
import { Description } from '../PaymentOnboarding.styled';

// eslint-disable-next-line complexity
export const PaymentCreateLocationGroup = ({ payOperator, locationGroup }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const { data: operator } = useOperator();

  const [hasError, setHasError] = useState(false);
  const [isLoading, setLoading] = useState(false);

  const validatorSchema = useActivatePaymentSchema();

  if (!operator) {
    return null;
  }

  const payTermsAlreadyAccepted = !!payOperator?.payTermsAccepted;

  const initialFormValues = {
    phone: operator.phone ?? '',
    companyName: operator.businessEntityName ?? '',
    businessFirstName: operator.firstName,
    businessLastName: operator.lastName,
    street: operator.businessEntityStreetName ?? '',
    streetNumber: operator.businessEntityStreetNumber ?? '',
    zipCode: operator.businessEntityZipCode ?? '',
    city: operator.businessEntityCity ?? '',
    invoiceEmail: operator.email,
    vatNumber: '',
    dpa: payTermsAlreadyAccepted,
    termsAndConditions: payTermsAlreadyAccepted,
  };

  const onSubmit = async ({
    phone,
    companyName,
    businessFirstName,
    businessLastName,
    street,
    streetNumber,
    zipCode,
    city,
    invoiceEmail,
    vatNumber,
  }) => {
    setLoading(true);

    if (!payTermsAlreadyAccepted) {
      const payTermsPatched = await acceptPaymentTerms();

      if (!payTermsPatched) {
        setLoading(false);
        setHasError(true);
        return;
      }
    }

    const phoneNumberTrim = whitespaceTrim(phone);

    try {
      await createLocationGroup({
        locationGroupId: locationGroup.groupId,
        phoneNumber: phoneNumberTrim,
        companyName,
        businessFirstName,
        businessLastName,
        street,
        streetNumber,
        zipCode,
        city,
        invoiceEmail,
        vatNumber: vatNumber ?? undefined,
      }).then(async response => {
        if (response.status === 201) {
          await Promise.all(
            locationGroup.locations.map(({ uuid }) => patchLocation(uuid, true))
          );
        }
      });

      if (payOperator.skipKybOnboarding) {
        window.location.reload();
        return;
      }

      await startRapydKybProcess(locationGroup.groupId).then(async response => {
        if (response.status === 201) {
          const { kybUrl: responseKybUrl } = await response.json();
          if (responseKybUrl) {
            window.open(responseKybUrl);
          }
          await queryClient.invalidateQueries(
            QUERY_KEYS.PAYMENT_ONBOARDING_STATUS
          );
          await queryClient.invalidateQueries(
            QUERY_KEYS.PAYMENT_LOCATION_GROUP
          );
        } else {
          setHasError(true);
        }

        setLoading(false);
      });
    } catch {
      setLoading(false);
      setHasError(true);
    }
  };

  return (
    <>
      <Description>
        {intl.formatMessage({
          id: 'payment.activate.requiredField',
        })}
      </Description>
      <AppForm
        initialValues={initialFormValues}
        onSubmit={onSubmit}
        validationSchema={validatorSchema}
      >
        <FormInputType.Input
          name="phone"
          labelId="createGroup.phone"
          required
        />

        <StyledTitle>
          {intl.formatMessage({
            id: 'payment.activate.invoiceDetailsHeadline',
          })}
        </StyledTitle>

        <InvoiceForm />

        {hasError && (
          <ErrorText>
            {intl.formatMessage({
              id: 'payment.location.startOnboarding.error',
            })}
          </ErrorText>
        )}

        {!payTermsAlreadyAccepted && <ConsentSection />}

        <ButtonWrapper>
          <StyledHelpLink>
            {intl.formatMessage(
              { id: 'payment.activate.helpLink' },
              {
                a: function addLink(...label) {
                  return (
                    <a
                      href={ACTIVATE_PAYMENT_HELP_LINK}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      {label}
                    </a>
                  );
                },
              }
            )}
          </StyledHelpLink>
          <SubmitButton
            theme={paymentPrimaryButtonTheme}
            title={intl.formatMessage({
              id: 'payment.activate.btn',
            })}
            dataCy="activatePayment"
            loading={isLoading}
            disableWhenInvalid
          />
        </ButtonWrapper>
      </AppForm>
    </>
  );
};
