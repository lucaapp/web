import styled from 'styled-components';

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 20px;
  align-items: center;
`;

export const PoweredByWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;

export const styledInput = {
  borderRadius: 0,
  border: '0.5px solid rgb(0, 0, 0)',
  height: '40px',
};

export const FieldRow = styled.div`
  display: flex;
  flex-direction: row;
  gap: 40px;
  width: 100%;

  & > * {
    width: 100%;
  }
`;

export const StyledTitle = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const ErrorText = styled.p`
  color: red;
`;

export const ConsentWrapper = styled.div`
  padding: 10px 0;
`;

export const StyledHelpLink = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat, sans-serif;
  font-size: 14px;
  margin-bottom: 16px;
  padding: 0 30px 20px 0;
  width: 60%;
  text-align: justify;
`;
