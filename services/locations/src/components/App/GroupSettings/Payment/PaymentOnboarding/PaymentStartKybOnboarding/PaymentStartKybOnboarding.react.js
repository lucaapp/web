import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import {
  startRapydKybProcess,
  startRapydKybProcessWithPhoneNumber,
} from 'network/payment';
import { useQueryClient } from 'react-query';
import { QUERY_KEYS, useNotifications } from 'components/hooks';
import { AppForm, FormInputType, SubmitButton } from 'components/general/Form';
import { useMobilePhoneSchema } from 'components/general/Form/hooks';
import { paymentPrimaryButtonTheme } from 'components/general';
import {
  ButtonWrapper,
  StyledPrimaryButton,
} from './PaymentStartKybOnboarding.styled';

export const PaymentStartKybOnboarding = ({
  payLocationGroup,
  withPhoneNumber = false,
}) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const { errorMessage } = useNotifications();
  const [isLoading, setLoading] = useState(false);
  const validationSchema = useMobilePhoneSchema();

  const kybUrl = payLocationGroup?.kybUrl || null;

  const errorNotification = () =>
    errorMessage('payment.location.startOnboarding.error');

  const startKybProcess = ({ phone }) => {
    if (withPhoneNumber && !phone) {
      return;
    }
    setLoading(true);
    (withPhoneNumber
      ? startRapydKybProcessWithPhoneNumber(payLocationGroup.uuid, phone)
      : startRapydKybProcess(payLocationGroup.uuid)
    )
      .then(async response => {
        if (response.status === 201) {
          const { kybUrl: responseKybUrl } = await response.json();
          if (responseKybUrl) {
            window.open(responseKybUrl);
          }
          await queryClient.invalidateQueries(
            QUERY_KEYS.PAYMENT_ONBOARDING_STATUS
          );
        } else {
          errorNotification();
        }
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
        errorNotification();
      });
  };

  return (
    <AppForm
      initialValues={{
        phone: '',
      }}
      validationSchema={withPhoneNumber && validationSchema}
      onSubmit={startKybProcess}
    >
      {withPhoneNumber && !kybUrl && (
        <FormInputType.Input name="phone" labelId="settings.location.phone" />
      )}
      <ButtonWrapper>
        {kybUrl ? (
          <StyledPrimaryButton
            data-cy="continueActivatePayment"
            onClick={() => window.open(kybUrl, '_self', 'noopener')}
          >
            {intl.formatMessage({
              id: 'payment.activate.continueBtn',
            })}
          </StyledPrimaryButton>
        ) : (
          <SubmitButton
            theme={paymentPrimaryButtonTheme}
            title={
              withPhoneNumber
                ? intl.formatMessage({
                    id: 'payment.activate.verifyBtn',
                  })
                : intl.formatMessage({
                    id: 'payment.activate.continueBtn',
                  })
            }
            dataCy="activatePayment"
            loading={isLoading}
            disableWhenInvalid
          />
        )}
      </ButtonWrapper>
    </AppForm>
  );
};
