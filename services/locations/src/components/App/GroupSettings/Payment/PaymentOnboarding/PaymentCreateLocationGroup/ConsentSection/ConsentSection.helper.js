import React from 'react';

export const getConsentBoxLabel = (intl, intlId, link) =>
  intl.formatMessage(
    {
      id: intlId,
    },
    {
      a: function addLink(...label) {
        return (
          <a href={link} target="_blank" rel="noopener noreferrer">
            {label}
          </a>
        );
      },
    }
  );

export const getDownloadConsentBoxLabel = (intl, intlId, href, download) =>
  intl.formatMessage(
    {
      id: intlId,
    },
    {
      a: function addDownloadLink(...label) {
        return (
          <a
            href={href}
            download={download}
            target="_blank"
            rel="noopener noreferrer"
          >
            {label}
          </a>
        );
      },
    }
  );
