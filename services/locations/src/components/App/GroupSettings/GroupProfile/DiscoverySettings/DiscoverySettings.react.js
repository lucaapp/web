import React from 'react';
import { LocationCard, Switch } from 'components/general';

import { QUERY_KEYS, useNotifications } from 'components/hooks';
import { useQueryClient } from 'react-query';

import { useIntl } from 'react-intl';
import { updateGroup } from 'network';
import {
  Wrapper,
  StyledSwitchContainer,
  SwitchWrapper,
  SwitchLabel,
} from './DiscoverySettings.styled';

export const DiscoverySettings = ({ group }) => {
  const intl = useIntl();
  const { errorMessage } = useNotifications();
  const queryClient = useQueryClient();

  const onChangeEnableDiscovery = isEnabled => {
    updateGroup({
      groupId: group.groupId,
      data: {
        ...group,
        discoveryEnabled: !!isEnabled,
      },
    })
      .then(async () => {
        await queryClient.invalidateQueries([QUERY_KEYS.GROUP]);
      })
      .catch(() => errorMessage());
  };

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({
        id: 'locationGroup.discoverySettings.headline',
      })}
    >
      <Wrapper>
        <SwitchWrapper>
          <SwitchLabel>
            {intl.formatMessage({
              id: 'settings.location.discoveryEnabledSwitch',
            })}
          </SwitchLabel>
          <StyledSwitchContainer>
            <Switch
              checked={group.discoveryEnabled}
              onChange={onChangeEnableDiscovery}
            />
          </StyledSwitchContainer>
        </SwitchWrapper>
      </Wrapper>
    </LocationCard>
  );
};
