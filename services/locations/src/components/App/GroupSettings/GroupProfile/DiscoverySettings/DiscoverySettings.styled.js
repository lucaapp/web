import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 0 32px 32px 32px;
`;

export const StyledSwitchContainer = styled.div`
  flex: 1;
  display: flex;
  justify-content: flex-end;
`;

export const SwitchWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 24px 0;
`;

export const SwitchLabel = styled.p`
  font-size: 14px;
  font-weight: 500;
  font-family: Montserrat-Medium, sans-serif;
  margin: 0;
`;
