import React, { useState } from 'react';
import { LocationCard, PrimaryButton } from 'components/general';
import { useIntl } from 'react-intl';
import { useGetEmployees } from 'components/hooks';

import { EmployeeFormModal } from 'components/App/modals/EmployeeFormModal';

import {
  Wrapper,
  ButtonWrapper,
  StyledEmployeeContainer,
} from './Employees.styled';
import { EmptyEmployee } from './EmptyEmployee';
import { EmployeeTable } from './EmployeeTable';

export const Employees = ({ group }) => {
  const intl = useIntl();
  const [selectedEmployee, setSelectedEmployee] = useState();
  const [isCreateModalVisible, setIsCreateModalVisible] = useState(false);
  const [isEditModalVisible, setIsEditModalVisible] = useState(false);

  const { data: employees, isLoading, error } = useGetEmployees(group.groupId);

  if (isLoading || error) return null;

  const openCreateModal = () => {
    setIsCreateModalVisible(true);
    setIsEditModalVisible(false);
  };

  const closeCreateModal = () => {
    setIsCreateModalVisible(false);
  };

  const openEditModal = employee => {
    setIsEditModalVisible(true);
    setIsCreateModalVisible(false);
    setSelectedEmployee(employee);
  };

  const closeEditModal = () => {
    setIsEditModalVisible(false);
  };

  return (
    <LocationCard
      component={StyledEmployeeContainer}
      title={intl.formatMessage({ id: 'groupProfile.employees.title' })}
      isCollapse
      testId="employees"
    >
      <Wrapper>
        {!employees || !employees.length ? (
          <EmptyEmployee />
        ) : (
          <EmployeeTable openEditModal={openEditModal} employees={employees} />
        )}
        <ButtonWrapper>
          <PrimaryButton onClick={openCreateModal}>
            {intl.formatMessage({
              id: 'groupProfile.employees.createEmployee.button',
            })}
          </PrimaryButton>

          {/* Create employee modal */}
          <EmployeeFormModal
            isVisible={isCreateModalVisible}
            closeModal={closeCreateModal}
            group={group}
          />
        </ButtonWrapper>
      </Wrapper>

      {/* Edit employee modal */}
      <EmployeeFormModal
        isVisible={isEditModalVisible}
        closeModal={closeEditModal}
        group={group}
        selectedEmployee={selectedEmployee}
        isEdit
      />
    </LocationCard>
  );
};
