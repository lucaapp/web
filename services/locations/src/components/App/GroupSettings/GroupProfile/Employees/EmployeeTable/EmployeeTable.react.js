import React from 'react';
import { isEven } from 'utils/math';
import { sortByName } from 'utils/sort';
import { avatarColors } from 'constants/avatarColors';
import { IconWrapper, StyledPenIcon, StyledTable } from '../Employees.styled';
import { EmployeeName } from '../EmployeeName';
import { DeleteEmployee } from '../DeleteEmployee';

export const EmployeeTable = ({ openEditModal, employees }) => {
  const modifiedEmployees = employees
    ?.sort((employeeA, employeeB) => sortByName(employeeA, employeeB))
    ?.map((employee, index) => ({
      ...employee,
      avatarColor: avatarColors[index % avatarColors.length],
    }));

  const columns = [
    {
      key: 'employeeName',
      render: function renderEmployeeName(employee) {
        return <EmployeeName employee={employee} />;
      },
    },
    {
      key: 'action',
      render: function renderAction(employee) {
        return (
          <IconWrapper>
            <StyledPenIcon onClick={() => openEditModal(employee)} />
            <DeleteEmployee employee={employee} />
          </IconWrapper>
        );
      },
    },
  ];

  return (
    <StyledTable
      dataSource={modifiedEmployees}
      columns={columns}
      pagination={false}
      rowClassName={(record, index) =>
        isEven(index) ? 'table-row-dark' : 'table-row-light'
      }
    />
  );
};
