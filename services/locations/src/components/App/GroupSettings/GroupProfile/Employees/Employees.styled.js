import styled from 'styled-components';
import { Table } from 'antd';
import { CrossIcon, PenIcon } from 'assets/icons';

export const Wrapper = styled.div`
  padding: 0 16px;
`;

export const StyledTable = styled(Table)`
  .ant-table {
    max-height: 444px;
    overflow: auto;
    padding: 0 16px;
    margin-bottom: 24px;
  }

  .ant-table-container {
    border-bottom: 1px solid #979797;
  }

  .ant-table-tbody > tr > td {
    border-bottom: 0 solid #f0f0f0;
  }

  .ant-table-tbody > tr.ant-table-row:hover > td {
    background: none !important;
  }

  .ant-table-cell {
    padding: 10px;
  }

  .ant-table-thead > tr > th {
    background: transparent;
    border-bottom: 2px solid #000000;
    padding: 0;
  }

  .table-row-light {
    background-color: transparent;
    opacity: 1;
  }

  .table-row-dark {
    background-color: rgb(243, 245, 247);
    opacity: 1;
  }
`;

export const StyledAvatarOutline = styled.div`
  width: 35px;
  height: 35px;
  border-radius: 100%;
  border: 2px solid ${({ color }) => color};
`;

export const StyledAvatarText = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  font-family: Montserrat-SemiBold, sans-serif;
  width: 32px;
  height: 32px;
  border-radius: 100%;
  color: ${({ color }) => color};
  font-size: 14px;
  font-weight: 600;
`;

export const StyledName = styled.div`
  padding-left: 15px;
`;

export const NameWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export const StyledPenIcon = styled(PenIcon)`
  width: 16px;
  height: 16px;
  margin-right: 16px;
  cursor: pointer;

  &:hover,
  &:focus {
    filter: invert(92%) sepia(9%) saturate(316%) hue-rotate(170deg)
      brightness(90%) contrast(89%);
    transition: all ease 0.3s;
  }
`;

export const StyledCrossIcon = styled(CrossIcon)`
  width: 16px;
  height: 16px;
  cursor: pointer;
  fill: #1b1c1e;

  &:hover,
  &:focus {
    filter: invert(92%) sepia(9%) saturate(316%) hue-rotate(170deg)
      brightness(90%) contrast(89%);
    transition: all ease 0.3s;
  }
`;

export const IconWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 0 16px 32px 16px;
`;

export const StyledEmployeeContainer = styled.div`
  display: flex;
  flex-direction: column;
  background: rgb(255, 255, 255);
  border-radius: 8px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.15);
  margin-top: 32px;
  padding: 4px 16px;
`;
