import React from 'react';
import { getNameInitials } from 'utils/strings';
import {
  NameWrapper,
  StyledAvatarOutline,
  StyledAvatarText,
  StyledName,
} from '../Employees.styled';

export const EmployeeName = ({ employee }) => {
  const avatarNameLabel = getNameInitials(
    employee.firstName,
    employee.lastName
  );

  return (
    <NameWrapper>
      <StyledAvatarOutline color={employee.avatarColor}>
        <StyledAvatarText color={employee.avatarColor}>
          {avatarNameLabel}
        </StyledAvatarText>
      </StyledAvatarOutline>

      <StyledName>{`${employee.firstName} ${employee.lastName}`}</StyledName>
    </NameWrapper>
  );
};
