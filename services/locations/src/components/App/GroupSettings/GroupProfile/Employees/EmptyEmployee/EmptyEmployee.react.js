import React from 'react';
import { PeopleIcon } from 'assets/icons';
import { useIntl } from 'react-intl';
import { OutlineWrapper, Title, Description } from './EmptyEmployee.styled';

export const EmptyEmployee = () => {
  const intl = useIntl();

  return (
    <OutlineWrapper>
      <Title>
        {intl.formatMessage({
          id: 'groupProfile.employees.empty.title',
        })}
      </Title>
      <Description>
        {intl.formatMessage({
          id: 'groupProfile.employees.empty.description',
        })}
      </Description>
      <PeopleIcon />
    </OutlineWrapper>
  );
};
