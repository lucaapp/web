import styled from 'styled-components';

export const OutlineWrapper = styled.div`
  min-height: 185px;
  border: 0.5px solid rgb(217, 215, 212);
  border-radius: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 0 16px 24px 16px;
`;

export const Title = styled.div`
  width: 100%;
  height: 24px;
  color: rgb(129, 129, 129);
  font-size: 14px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  text-align: center;
`;

export const Description = styled.div`
  width: 80%;
  min-height: 48px;
  color: rgb(129, 129, 129);
  font-size: 14px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  text-align: center;
  margin: 10px 0 15px 0;
`;
