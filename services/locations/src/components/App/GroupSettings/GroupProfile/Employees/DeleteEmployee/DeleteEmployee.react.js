import React from 'react';
import { Popconfirm } from 'antd';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { deleteEmployee } from 'network/api';
import { QUERY_KEYS, useNotifications } from 'components/hooks';
import { StyledCrossIcon } from '../Employees.styled';

export const DeleteEmployee = ({ employee }) => {
  const intl = useIntl();
  const { successMessage, errorMessage } = useNotifications();
  const queryClient = useQueryClient();

  const handleDeleteEmployee = () =>
    deleteEmployee(employee.uuid)
      .then(response => {
        if (response.status !== 204) {
          errorMessage('notification.deleteEmployee.error');
          return;
        }
        successMessage('notification.deleteEmployee.success');
        queryClient.invalidateQueries(QUERY_KEYS.EMPLOYEES);
      })
      .catch(() => errorMessage('notification.deleteEmployee.error'));

  return (
    <Popconfirm
      placement="leftTop"
      onConfirm={handleDeleteEmployee}
      title={intl.formatMessage({
        id: 'groupProfile.employees.deleteEmployee.popConfirm.title',
      })}
      okText={intl.formatMessage({
        id: 'groupProfile.employees.deleteEmployee.popConfirm.ok',
      })}
      cancelText={intl.formatMessage({
        id: 'generic.cancel',
      })}
    >
      <StyledCrossIcon />
    </Popconfirm>
  );
};
