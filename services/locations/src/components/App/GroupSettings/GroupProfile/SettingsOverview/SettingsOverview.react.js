import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { updateGroup } from 'network/api';
import { useQueryClient } from 'react-query';

import {
  ProfileSection,
  useGetDisplayFields,
} from 'components/general/EditLocationsProfile/ProfileSection';
import { AddressSection } from 'components/general/EditLocationsProfile/AddressSection';
import { useEditProfileSchema } from 'components/general/Form/hooks';
import { getFormattedPhoneNumber } from 'utils/formatters';
import { Spin } from 'antd';
import {
  QUERY_KEYS,
  useGetLocationById,
  useNotifications,
} from 'components/hooks';
import { LocationCard } from 'components/general';
import { Wrapper, StyledDivider } from './SettingsOverview.styled';

export const SettingsOverview = ({ group }) => {
  const intl = useIntl();
  const { successMessage, errorMessage } = useNotifications();
  const queryClient = useQueryClient();
  const [isEditProfileMode, setIsEditProfileMode] = useState(false);
  const { groupSchema } = useEditProfileSchema();

  const baseLocationInfo = group.locations.find(location => !location.name);

  const { data: baseLocation, isLoading, error } = useGetLocationById(
    baseLocationInfo.uuid
  );

  const profileFields = useGetDisplayFields(group, baseLocation, 'group');

  if (isLoading) return <Spin />;
  if (error) return null;

  const handleSubmit = ({ phone, name }) => {
    const formattedPhoneNumber = getFormattedPhoneNumber(phone);
    const formattedGroupName = name.trim();

    if (
      formattedGroupName === group.name &&
      formattedPhoneNumber === baseLocation.phone
    ) {
      return;
    }

    updateGroup({
      groupId: group.groupId,
      data: { name: formattedGroupName, phone: formattedPhoneNumber },
    })
      .then(response => {
        if (response.status !== 204) {
          errorMessage('notification.updateGroup.error');
        }
        queryClient.invalidateQueries(QUERY_KEYS.GROUPS);
        queryClient.invalidateQueries([QUERY_KEYS.GROUP, group.groupId]);
        queryClient.invalidateQueries([QUERY_KEYS.LOCATION, baseLocation.uuid]);
        successMessage('notification.updateGroup.success');
        setIsEditProfileMode(false);
      })
      .catch(() => errorMessage('notification.updateGroup.error'));
  };

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'profile.overview' })}
    >
      <Wrapper>
        <ProfileSection
          group={group}
          location={baseLocation}
          isEditProfileMode={isEditProfileMode}
          setIsEditProfileMode={setIsEditProfileMode}
          onSubmit={handleSubmit}
          validationSchema={groupSchema}
          profileFields={profileFields}
          dataCy="editGroup"
        />
        <StyledDivider />
        <AddressSection location={baseLocation} isGroup dataCy="editAddress" />
      </Wrapper>
    </LocationCard>
  );
};
