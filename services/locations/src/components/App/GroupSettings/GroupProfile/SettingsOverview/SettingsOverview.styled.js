import styled from 'styled-components';
import { Divider } from 'antd';
import { Media } from 'utils/media';

export const SettingsContent = styled.div`
  padding: 24px 32px;
  background-color: white;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;

  ${Media.mobile`
    margin-top: 40px;
    flex-direction: column;
  `}
`;

export const OverviewHeading = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 4px;
  margin-top: 16px;
`;

export const OverviewValue = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
`;

export const Wrapper = styled.div`
  padding: 0 32px 32px 32px;
`;

export const Address = styled.div`
  margin-bottom: 40px;
`;

export const AddressHeader = styled(Address)`
  margin-bottom: 8px;
`;

export const AddressRow = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 14px;
  font-weight: 500;
`;

export const StyledDivider = styled(Divider)`
  border-top: 1px solid #d8d8d8;
`;

export const contentStyles = {
  backgroundColor: '#f3f5f7',
};

export const sliderStyles = {
  ...contentStyles,
  borderRight: '1px solid rgb(151, 151, 151)',
};

export const buttonStyles = {
  fontFamily: 'Montserrat-Bold, sans-serif',
  fontSize: 14,
  fontWeight: 'bold',
  padding: '0 40px',
  border: '1px solid rgb(80, 102, 124)',
  color: 'black',
};

export const inputStyles = {
  borderRadius: 0,
  border: '1px solid rgb(0, 0, 0)',
  height: '40px',
};
