import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';

import { LocationCard, PrimaryButton } from 'components/general';
import { updateGroup } from 'network/api';
import { QUERY_KEYS, useNotifications } from 'components/hooks';
import { CategoryRadio } from './CategoryRadio';

import { allowedCategories } from './GroupCategory.helper';

import {
  Wrapper,
  StyledRadioGroup,
  StyledSpace,
  ButtonWrapper,
} from './GroupCategory.styled';

export const GroupCategory = ({ group }) => {
  const intl = useIntl();
  const { successMessage, errorMessage } = useNotifications();
  const queryClient = useQueryClient();
  const [radioValue, setRadioValue] = useState(group?.type ?? '');

  const baseLocationId = group?.locations?.find(location => !location.name)
    .uuid;

  const handleChangeCategory = values => setRadioValue(values.target.value);

  const updateGroupCategory = () => {
    updateGroup({
      groupId: group.groupId,
      data: { type: radioValue },
    })
      .then(response => {
        if (response.status !== 204) {
          errorMessage('notification.updateGroup.error');
          return;
        }
        queryClient.invalidateQueries([QUERY_KEYS.GROUP, group.groupId]);
        queryClient.invalidateQueries([QUERY_KEYS.GROUPS]);
        queryClient.invalidateQueries([QUERY_KEYS.LOCATION, baseLocationId]);
        successMessage('notification.updateGroup.success');
      })
      .catch(() => errorMessage('notification.updateGroup.error'));
  };

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'groupCategory.card.title' })}
    >
      <Wrapper>
        <StyledRadioGroup
          onChange={handleChangeCategory}
          name="category"
          defaultValue={radioValue}
        >
          <StyledSpace direction="vertical">
            {Object.keys(allowedCategories).map(category => (
              <CategoryRadio
                key={category}
                category={category}
                radioValue={radioValue}
              />
            ))}
          </StyledSpace>
        </StyledRadioGroup>
        <ButtonWrapper>
          <PrimaryButton
            disabled={!radioValue}
            onClick={updateGroupCategory}
            data-cy="categoryBtnSave"
            htmlType="submit"
          >
            {intl.formatMessage({
              id: 'button.save',
            })}
          </PrimaryButton>
        </ButtonWrapper>
      </Wrapper>
    </LocationCard>
  );
};
