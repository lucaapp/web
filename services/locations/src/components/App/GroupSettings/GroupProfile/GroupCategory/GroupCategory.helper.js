export const allowedCategories = {
  restaurant: 'groupCategory.gastronomy',
  hotel: 'groupCategory.accommodation',
  nursing_home: 'groupCategory.careFacility',
  store: 'groupCategory.store',
  base: 'groupCategory.other',
};
