import React from 'react';
import { useIntl } from 'react-intl';
import { Radio } from 'antd';
import { allowedCategories } from '../GroupCategory.helper';

import { Label, Wrapper } from './CategoryRadio.styled';

export const CategoryRadio = ({ category, radioValue }) => {
  const intl = useIntl();

  return (
    <Wrapper className="categoryRadio">
      <Label $isChecked={radioValue === category}>
        {intl.formatMessage({
          id: allowedCategories[category],
        })}
      </Label>
      <Radio value={category} data-cy={`radio-${category}`} />
    </Wrapper>
  );
};
