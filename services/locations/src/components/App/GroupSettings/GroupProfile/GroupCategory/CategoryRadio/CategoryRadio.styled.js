import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-bottom: 16px;
  padding-top: 16px;
  border-bottom: 0.5px solid rgb(197, 195, 190);
`;

export const Label = styled.div`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: ${({ $isChecked }) => ($isChecked ? 600 : 500)}; ;
`;
