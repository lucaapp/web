import styled from 'styled-components';
import { Radio, Space } from 'antd';

export const Wrapper = styled.div`
  padding: 0 32px 32px 32px;
`;

export const StyledRadioGroup = styled(Radio.Group)`
  width: 100%;

  .ant-radio-inner::after {
    background-color: rgb(48, 61, 75);
  }
  .ant-space {
    width: 100%;
  }
`;

export const StyledSpace = styled(Space)`
  .ant-space-item:last-child {
    .categoryRadio {
      border-bottom: none;
    }
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: ${({ multipleButtons }) =>
    multipleButtons ? 'space-between' : 'flex-end'};
  margin-top: 24px;

  @media (max-width: 1096px) {
    margin-top: 40px;
    flex-flow: column-reverse;

    button:not(:last-child) {
      margin-top: 24px;
    }
  }
`;
