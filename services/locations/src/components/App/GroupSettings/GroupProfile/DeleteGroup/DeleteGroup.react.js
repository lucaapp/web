import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { useHistory } from 'react-router';
import { notification, Popconfirm } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';

import {
  QUERY_KEYS,
  useGetPaymentOnboardingStatus,
  useNotifications,
} from 'components/hooks';
import { BASE_GROUP_ROUTE } from 'constants/routes';
import { deleteGroup } from 'network/api';

import { DangerButton, LocationCard } from 'components/general';

import { PasswordModal } from 'components/App/modals/PasswordModal';

import { Wrapper, ButtonWrapper, Info } from './DeleteGroup.styled';

export const DeleteGroup = ({ group }) => {
  const intl = useIntl();
  const history = useHistory();
  const queryClient = useQueryClient();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { errorMessage } = useNotifications();
  const { data: paymentOnboardingStatus } = useGetPaymentOnboardingStatus(
    group.groupId
  );

  const handleError = errorString => {
    errorMessage(errorString);
  };

  const onDelete = password => {
    deleteGroup(group.groupId, { password })
      .then(async response => {
        if (response.status === 204) {
          setIsModalOpen(false);
          notification.success({
            message: intl.formatMessage({
              id: 'notification.deleteGroup.success',
            }),
            className: 'generalNotificationSuccess',
          });
          await queryClient.invalidateQueries(QUERY_KEYS.GROUPS);
          history.push(BASE_GROUP_ROUTE);
        } else if (response.status === 403) {
          handleError('notification.deleteLocation.error.incorrectPassword');
        } else {
          handleError('notification.deleteGroup.error');
        }
      })
      .catch(() => {
        setIsModalOpen(false);
        handleError('notification.deleteGroup.error');
      });
  };

  const isPaymentEnabled = paymentOnboardingStatus?.status === 'DONE';

  return (
    <>
      <LocationCard
        isCollapse
        testId="deleteGroup"
        title={intl.formatMessage({
          id: isPaymentEnabled
            ? 'settings.group.delete.paymentEnabled'
            : 'settings.group.delete',
        })}
      >
        <Wrapper>
          <Info>
            {intl.formatMessage({
              id: isPaymentEnabled
                ? 'settings.group.delete.info.paymentEnabled'
                : 'settings.group.delete.info',
            })}
          </Info>
          <ButtonWrapper>
            <Popconfirm
              placement="topLeft"
              onConfirm={() => setIsModalOpen(true)}
              title={intl.formatMessage({
                id: isPaymentEnabled
                  ? 'group.delete.confirmText.paymentEnabled'
                  : 'group.delete.confirmText',
              })}
              okText={intl.formatMessage({
                id: 'location.delete.confirmButton',
              })}
              cancelText={intl.formatMessage({
                id: 'generic.cancel',
              })}
              icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
            >
              <DangerButton data-cy="deleteGroup">
                {intl.formatMessage({ id: 'settings.group.delete.submit' })}
              </DangerButton>
            </Popconfirm>
          </ButtonWrapper>
        </Wrapper>
      </LocationCard>
      {isModalOpen && (
        <PasswordModal
          callback={onDelete}
          onClose={() => setIsModalOpen(false)}
        />
      )}
    </>
  );
};
