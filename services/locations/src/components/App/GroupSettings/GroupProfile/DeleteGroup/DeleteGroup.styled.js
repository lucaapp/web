import styled from 'styled-components';

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const Wrapper = styled.div`
  padding: 0 32px 32px 32px;
`;

export const Info = styled.div`
  margin-bottom: 24px;
`;
