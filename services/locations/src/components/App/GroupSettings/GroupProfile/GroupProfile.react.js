import React from 'react';

import { OpeningHours } from 'components/App/shared';
import { useFeatureFlags, useGetGroup } from 'components/hooks';

import { RESTAURANT_TYPE } from 'constants/locations';
import { SettingsOverview } from './SettingsOverview';
import { Employees } from './Employees';

import { DeleteGroup } from './DeleteGroup';
import { GroupCategory } from './GroupCategory';
import { DiscoverySettings } from './DiscoverySettings';

export const GroupProfile = ({ groupId }) => {
  const featureFlags = useFeatureFlags();
  const { isLoading, error, data: group } = useGetGroup(groupId, {
    cacheTime: 0,
  });

  if (isLoading || error) return null;

  return (
    <>
      <SettingsOverview group={group} />
      {featureFlags.enable_staff_members && <Employees group={group} />}
      <GroupCategory group={group} />
      {group.type === RESTAURANT_TYPE && <DiscoverySettings group={group} />}
      <OpeningHours group={group} />
      <DeleteGroup group={group} />
    </>
  );
};
