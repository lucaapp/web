import React, { useMemo } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { Layout } from 'antd';
import { useIntl } from 'react-intl';
import { GroupList } from 'components/App/GroupList';
import { Content, Sider } from 'components/general';
import {
  useGetGroups,
  useOperator,
  useDailyKey,
  useGetPaymentEnabled,
} from 'components/hooks';

import { GroupOverview } from './GroupOverview';
import { GroupProfile } from './GroupProfile';
import {
  Header,
  NoHeadlineSpacer,
  StyledTabPane,
  StyledTabs,
  Wrapper,
} from './GroupSettings.styled';
import { Payment } from './Payment';
import { validTabs } from './GroupSettings.helper';

export const GroupSettings = () => {
  const intl = useIntl();
  const { groupId } = useParams();
  const { search } = useLocation();
  const { data: operator } = useOperator();

  const queryParameters = React.useMemo(() => new URLSearchParams(search), [
    search,
  ]);

  const tabQueryParameter = queryParameters.get('tab');

  const { isLoading, error, data: paymentEnabled } = useGetPaymentEnabled(
    operator.operatorId
  );

  const isPaymentEnabled = paymentEnabled && paymentEnabled.paymentEnabled;

  const getDefaultTab = () => {
    if (tabQueryParameter && validTabs.has(tabQueryParameter)) {
      return tabQueryParameter;
    }
    if (isPaymentEnabled) {
      return 'openLocationPayment';
    }
    return 'openLocationCheckin';
  };

  const { data: groups } = useGetGroups();

  const group = useMemo(() => groups?.find(item => item.groupId === groupId), [
    groups,
    groupId,
  ]);

  const { data: dailyKey } = useDailyKey();

  if (isLoading || error || !group) return null;

  return (
    <Layout>
      <Sider>
        <GroupList />
      </Sider>
      <Content>
        <Wrapper>
          <Header data-cy="groupSettingsHeader">{group.name}</Header>
          <StyledTabs defaultActiveKey={getDefaultTab()}>
            <StyledTabPane
              tab={intl.formatMessage({ id: 'location.tabs.profile' })}
              key="openLocationSettings"
            >
              <NoHeadlineSpacer />
              <GroupProfile groupId={groupId} />
            </StyledTabPane>
            {dailyKey && (
              <StyledTabPane
                tab={intl.formatMessage({ id: 'location.tabs.checkin' })}
                key="openLocationCheckin"
              >
                <NoHeadlineSpacer />
                <GroupOverview group={group} />
              </StyledTabPane>
            )}

            {isPaymentEnabled && (
              <StyledTabPane
                tab={intl.formatMessage({ id: 'groupSettings.tabs.payment' })}
                key="openLocationPayment"
              >
                <Payment locationGroup={group} />
              </StyledTabPane>
            )}
          </StyledTabs>
        </Wrapper>
      </Content>
    </Layout>
  );
};
