import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { TERMS_CONDITIONS_LINK } from 'constants/links';
import { Services } from '../ProfileContent/ProfileServices/Services';

const privacyMandatoryHref = '/static/media/DSE_Luca_mandatory.pdf';
const privacyOptionalHref = '/static/media/DSE_Luca_optional.pdf';
const avvHref = '/static/media/AVV_Luca.pdf';
const tomsHref = '/static/media/TOMS_Luca.pdf';

jest.mock(
  'assets/documents/DSE_Luca_mandatory.pdf',
  () => privacyMandatoryHref
);
jest.mock('assets/documents/DSE_Luca_optional.pdf', () => privacyOptionalHref);
jest.mock('assets/documents/AVV_Luca.pdf', () => avvHref);
jest.mock('assets/documents/TOMS_Luca.pdf', () => tomsHref);

const mockPayOperator = { payTermsAccepted: true };

jest.mock('components/hooks/useQueries', () => ({
  useGetPaymentOperator: () => ({
    data: mockPayOperator,
    isError: false,
    isLoading: false,
  }),
}));

const mockOperator = {
  operatorId: '3ccd4411-08eb-4325-a46e-816ce64f7071',
};

describe('Services Component', () => {
  const setup = async () => {
    const renderResult = render(<Services operator={mockOperator} />);

    const servicesSection = await screen.findByTestId(
      'profile-servicesSection'
    );
    const privacyLinkMandatory = await screen.findByTestId(
      'privacyLinkMandatory'
    );
    const privacyLinkOptional = await screen.findByTestId(
      'privacyLinkOptional'
    );
    const dpaLink = await screen.findByTestId('dpaLink');
    const tomsLink = await screen.findByTestId('tomsLink');
    const downloadIcon = await screen.getAllByTestId('downloadIcon');

    const termsLink = await screen.findByTestId('termsLink');
    const externalLinksIcon = await screen.findByTestId('externalLinksIcon');

    return {
      renderResult,
      servicesSection,
      privacyLinkMandatory,
      privacyLinkOptional,
      dpaLink,
      tomsLink,
      termsLink,
      downloadIcon,
      externalLinksIcon,
    };
  };

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });

  test('Check if download links contain correct href and download attribute values', async () => {
    const {
      privacyLinkMandatory,
      privacyLinkOptional,
      dpaLink,
      tomsLink,
    } = await setup();

    expect(privacyLinkMandatory).toHaveAttribute(
      'download',
      'downloadFile.profile.privacy'
    );
    expect(privacyLinkMandatory).toHaveAttribute('href', privacyMandatoryHref);

    expect(privacyLinkOptional).toHaveAttribute(
      'download',
      'downloadFile.profile.privacy'
    );
    expect(privacyLinkOptional).toHaveAttribute('href', privacyOptionalHref);

    expect(dpaLink).toHaveAttribute('download', 'downloadFile.profile.avv');
    expect(dpaLink).toHaveAttribute('href', avvHref);

    expect(tomsLink).toHaveAttribute('download', 'downloadFile.profile.toms');
    expect(tomsLink).toHaveAttribute('href', tomsHref);
  });

  test('Check if terms link contains correct href and target attribute values', async () => {
    const { termsLink } = await setup();

    expect(termsLink).toHaveAttribute('href', TERMS_CONDITIONS_LINK);
    expect(termsLink).toHaveAttribute('target', '_blank');
  });
});
