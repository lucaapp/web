import React from 'react';
import { useIntl } from 'react-intl';

// Components
import { useGetPaymentEnabled, useOperator } from 'components/hooks';
import { PersonalData } from './PersonalData';
import { BusinessData } from './BusinessData';
import { ProfileServices } from './ProfileServices';
import { AccountDeletion } from './AccountDeletion';

import { Wrapper, Header } from './ProfileContent.styled';

export const ProfileContent = () => {
  const intl = useIntl();
  const { data: operator } = useOperator();

  const accountDeletionInProgress = !!operator.deletedAt;

  const { error: paymentError, data: paymentEnabled } = useGetPaymentEnabled(
    operator.operatorId,
    {
      enabled: !accountDeletionInProgress,
    }
  );

  if (paymentError) {
    return null;
  }

  return (
    <Wrapper data-cy="profilePageWrapper">
      <Header data-cy="profilePageHeader">
        {intl.formatMessage({ id: 'profile.heading' })}
      </Header>
      {!accountDeletionInProgress && (
        <>
          <PersonalData />
          <BusinessData />
          <ProfileServices
            accountDeletionInProgress={accountDeletionInProgress}
          />
        </>
      )}
      <AccountDeletion isPaymentEnabled={paymentEnabled?.paymentEnabled} />
    </Wrapper>
  );
};
