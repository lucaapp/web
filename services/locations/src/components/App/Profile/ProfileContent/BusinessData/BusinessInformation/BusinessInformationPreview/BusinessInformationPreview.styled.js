import styled from 'styled-components';

export const ProfileContent = styled.div`
  padding: 24px 0;
  margin: 0 32px;
  background-color: white;
`;

export const ProfileTitle = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;

export const ProfileDescription = styled.div`
  color: rgba(0, 0, 0);
  font-size: 16px;
  font-weight: 500;
  font-family: Montserrat-Medium, sans-serif;
  margin-bottom: ${({ marginBottom }) => marginBottom || ''};
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;
