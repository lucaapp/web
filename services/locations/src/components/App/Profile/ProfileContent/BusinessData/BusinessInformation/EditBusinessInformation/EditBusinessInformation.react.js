import React from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { Form } from 'antd';

import { BusinessAddress } from 'components/Authentication/Register/steps/BusinessInfoStep/BusinessAddress';

import { updateOperator } from 'network/api';
import {
  useNameValidator,
  QUERY_KEYS,
  useNotifications,
  useOperator,
} from 'components/hooks';

import { ButtonRow } from './ButtonRow';

import { shouldUpdateOperatorBusinessInfo } from './EditBusinessInformation.helper';
import {
  ProfileContent,
  Heading,
  Wrapper,
  StyledInput,
} from './EditBusinessInformation.styled';

export const EditBusinessInformation = ({ setIsEditable }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const queryClient = useQueryClient();
  const { data: operator } = useOperator();
  const { successMessage, errorMessage } = useNotifications();

  const businessNameValidator = useNameValidator('businessEntityName');

  const getInitialValues = () => {
    const {
      businessEntityCity,
      businessEntityName,
      businessEntityStreetName,
      businessEntityStreetNumber,
      businessEntityZipCode,
    } = operator;

    return {
      businessEntityCity,
      businessEntityName,
      businessEntityStreetName,
      businessEntityStreetNumber,
      businessEntityZipCode,
    };
  };

  const updateOperatorInfo = infoData => {
    updateOperator(infoData)
      .then(response => {
        if (response.status !== 204) {
          errorMessage('notification.profile.updateUser.error');
          return;
        }
        queryClient.invalidateQueries(QUERY_KEYS.ME);
        setIsEditable(false);
        successMessage('notification.profile.updateUser.success');
      })
      .catch(() => errorMessage('notification.profile.updateUser.error'));
  };

  const onFinish = values => {
    const {
      businessEntityCity,
      businessEntityName,
      businessEntityStreetName,
      businessEntityStreetNumber,
      businessEntityZipCode,
    } = values;

    if (shouldUpdateOperatorBusinessInfo(values, operator)) {
      const infoData = {
        businessEntityCity,
        businessEntityName,
        businessEntityStreetName,
        businessEntityStreetNumber,
        businessEntityZipCode,
      };

      return updateOperatorInfo(infoData);
    }

    return setIsEditable(false);
  };

  const onCancel = () => {
    form.resetFields();
    setIsEditable(false);
  };

  return (
    <ProfileContent data-cy="profile-editBusinessDataForm">
      <Form onFinish={onFinish} form={form} initialValues={getInitialValues()}>
        <Heading>
          {intl.formatMessage({ id: 'profile.businessInfo.title' })}
        </Heading>
        <Wrapper>
          <Form.Item
            data-cy="businessEntityName"
            colon={false}
            name="businessEntityName"
            label={intl.formatMessage({
              id: 'register.businessEntityName',
            })}
            rules={businessNameValidator}
          >
            <StyledInput data-cy="businessNameInputField" />
          </Form.Item>
          <BusinessAddress />
        </Wrapper>
      </Form>
      <ButtonRow form={form} onCancel={onCancel} />
    </ProfileContent>
  );
};
