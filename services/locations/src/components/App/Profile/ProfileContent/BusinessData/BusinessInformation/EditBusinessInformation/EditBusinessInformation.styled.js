import styled, { css } from 'styled-components';
import { Media } from 'utils/media';
import { Input } from 'antd';

export const ProfileContent = styled.div`
  padding: 24px 32px;
  background-color: white;
  border-bottom: 1px solid rgb(151, 151, 151);
`;

export const Heading = styled.div`
  color: rgba(0, 0, 0);
  font-size: 16px;
  font-weight: 600;
  font-family: Montserrat-Bold, sans-serif;
  margin-bottom: 16px;
`;

export const Wrapper = styled.div`
  width: 700px;
`;

const sharedInputStyle = css`
  border: 1px solid #696969 !important;
  background-color: transparent !important;

  &:hover,
  &:active,
  &:focus {
    border: 1px solid #696969 !important;
    background-color: transparent !important;
  }

  ${Media.mobile`
    border-radius: 0px !important;
    border: 0.5px solid rgb(0, 0, 0) !important;
    line-height: 48px !important;
    height: 48px !important;
  `}
`;
export const StyledInput = styled(Input)`
  ${sharedInputStyle}
`;
