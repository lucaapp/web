import React, { useState } from 'react';
import { BusinessInformationPreview } from './BusinessInformationPreview';
import { EditBusinessInformation } from './EditBusinessInformation';

export const BusinessInformation = () => {
  const [isEditable, setIsEditable] = useState(false);

  return (
    <>
      {!isEditable ? (
        <BusinessInformationPreview
          data-cy="profileBusinessPreview"
          setIsEditable={setIsEditable}
        />
      ) : (
        <EditBusinessInformation setIsEditable={setIsEditable} />
      )}
    </>
  );
};
