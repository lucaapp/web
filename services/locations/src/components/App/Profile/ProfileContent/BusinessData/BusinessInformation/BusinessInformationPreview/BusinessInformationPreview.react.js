import React from 'react';
import { useIntl } from 'react-intl';

import { PrimaryButton } from 'components/general';
import { useOperator } from 'components/hooks';
import {
  ProfileContent,
  ButtonWrapper,
  ProfileTitle,
  ProfileDescription,
} from './BusinessInformationPreview.styled';

export const BusinessInformationPreview = ({ setIsEditable }) => {
  const intl = useIntl();
  const { data: operator } = useOperator();

  const {
    businessEntityName,
    businessEntityCity,
    businessEntityStreetName,
    businessEntityStreetNumber,
    businessEntityZipCode,
  } = operator;

  return (
    <ProfileContent data-cy="profile-businessDataSection">
      {businessEntityName && (
        <>
          <ProfileTitle data-cy="profile-businessNameTitle">
            {intl.formatMessage({ id: 'profile.businessInfo.name' })}
          </ProfileTitle>
          <ProfileDescription
            marginBottom="16px"
            data-cy="profile-businessName"
          >
            {businessEntityName}
          </ProfileDescription>
        </>
      )}
      {businessEntityStreetName &&
        businessEntityStreetNumber &&
        businessEntityZipCode &&
        businessEntityCity && (
          <>
            <ProfileTitle data-cy="profile-businessAddressTitle">
              {intl.formatMessage({ id: 'profile.businessInfo.address' })}
            </ProfileTitle>
            <ProfileDescription data-cy="profile-businessAddressStreetAndNumber">{`${businessEntityStreetName} ${businessEntityStreetNumber}`}</ProfileDescription>
            <ProfileDescription
              marginBottom="16px"
              data-cy="profile-businessAddressZipAndCity"
            >{`${businessEntityZipCode} ${businessEntityCity}`}</ProfileDescription>
          </>
        )}
      <ButtonWrapper>
        <PrimaryButton
          data-cy="profile-editBusinessDataButton"
          onClick={() => {
            setIsEditable(true);
          }}
        >
          {intl.formatMessage({ id: 'location.edit' })}
        </PrimaryButton>
      </ButtonWrapper>
    </ProfileContent>
  );
};
