import _ from 'lodash';

const getBusinessInfo = value => {
  const {
    businessEntityName,
    businessEntityCity,
    businessEntityStreetName,
    businessEntityStreetNumber,
    businessEntityZipCode,
  } = value;
  return [
    businessEntityName,
    businessEntityCity,
    businessEntityStreetName,
    businessEntityStreetNumber,
    businessEntityZipCode,
  ];
};

export const shouldUpdateOperatorBusinessInfo = (values, operator) => {
  return !_.isEqual(getBusinessInfo(values), getBusinessInfo(operator));
};
