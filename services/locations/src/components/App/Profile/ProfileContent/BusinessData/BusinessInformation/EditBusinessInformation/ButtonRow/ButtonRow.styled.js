import styled from 'styled-components';
import { SecondaryButton } from 'components/general';

export const StyledSecondaryButton = styled(SecondaryButton)`
  margin-right: 16px;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;
