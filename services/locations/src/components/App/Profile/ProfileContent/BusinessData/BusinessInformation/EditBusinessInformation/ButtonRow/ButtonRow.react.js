import React from 'react';
import { useIntl } from 'react-intl';

import { PrimaryButton } from 'components/general';
import { ButtonWrapper, StyledSecondaryButton } from './ButtonRow.styled';

export const ButtonRow = ({ form, onCancel }) => {
  const intl = useIntl();

  return (
    <ButtonWrapper>
      <StyledSecondaryButton onClick={onCancel}>
        {intl.formatMessage({ id: 'generic.cancel' })}
      </StyledSecondaryButton>
      <PrimaryButton
        data-cy="profile-saveBusinessDataButton"
        onClick={() => form.submit()}
      >
        {intl.formatMessage({ id: 'profile.overview.submit' })}
      </PrimaryButton>
    </ButtonWrapper>
  );
};
