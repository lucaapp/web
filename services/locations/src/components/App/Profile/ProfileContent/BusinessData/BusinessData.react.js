import React from 'react';
import { useIntl } from 'react-intl';

// Components
import { LocationCard } from 'components/general';
import { BusinessInformation } from './BusinessInformation';

export const BusinessData = () => {
  const intl = useIntl();

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'profile.businessData' })}
      testId="businessData"
    >
      <BusinessInformation />
    </LocationCard>
  );
};
