import React from 'react';
import { Popconfirm, message } from 'antd';
import { useQueryClient } from 'react-query';
import { useIntl } from 'react-intl';

import { requestAccountDeletion } from 'network/api';

import { QUERY_KEYS } from 'components/hooks';
import { DangerButton } from 'components/general';

import { waitingPeriodDays } from '../AccountDeletion.helper';
import { Text, Wrapper, ButtonWrapper } from '../AccountDeletion.styled';

export const DeleteAccount = ({ isPaymentEnabled }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();

  const onSuccess = () => {
    queryClient.invalidateQueries(QUERY_KEYS.ME);
    message.success(
      intl.formatMessage({ id: 'account.delete.deletion.success' })
    );
  };
  const onError = () => {
    message.error(intl.formatMessage({ id: 'error.headline' }));
  };

  const accountDeleteConfirmPromptId = isPaymentEnabled
    ? 'account.delete.confirm.promptPaymentEnabled'
    : 'account.delete.confirm.prompt';
  const accountDeleteConfirmId = isPaymentEnabled
    ? 'account.delete.confirmPaymentEnabled'
    : 'account.delete.confirm';

  const deleteAccount = () => {
    requestAccountDeletion()
      .then(response => {
        if (response.status !== 200) {
          onError();
          return;
        }
        onSuccess();
      })
      .catch(() => onError());
  };

  return (
    <Wrapper data-cy="deleteAccount">
      <Text>
        {isPaymentEnabled
          ? intl.formatMessage(
              { id: 'account.delete.infoPaymentEnabled' },
              { br: <br /> }
            )
          : intl.formatMessage(
              { id: 'account.delete.info' },
              { days: waitingPeriodDays }
            )}
      </Text>
      <ButtonWrapper>
        <Popconfirm
          placement="topRight"
          disabled={false}
          onConfirm={deleteAccount}
          title={intl.formatMessage({ id: accountDeleteConfirmPromptId })}
          okText={intl.formatMessage({ id: accountDeleteConfirmId })}
          cancelText={intl.formatMessage({ id: 'generic.cancel' })}
        >
          <DangerButton data-cy="deleteAccount">
            {intl.formatMessage({ id: 'account.delete.confirm' })}
          </DangerButton>
        </Popconfirm>
      </ButtonWrapper>
    </Wrapper>
  );
};
