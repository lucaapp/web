import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { AccountDeletion } from './AccountDeletion.react';

const mockIsPaymentEnabled = true;

describe('AccountDeletion', () => {
  const setup = async isPaymentEnabled => {
    const renderResult = render(
      <AccountDeletion isPaymentEnabled={isPaymentEnabled} />
    );

    const accountDeletioncard = await screen.findByTestId(
      'locationCard-accountDeletion'
    );

    return {
      renderResult,
      accountDeletioncard,
    };
  };
  describe('Non deleted operator', () => {
    test('Check if component renders correctly', async () => {
      testAllDefined(await setup(mockIsPaymentEnabled));
      const deleteAccount = screen.queryByTestId('deleteAccount');
      const restoreAccount = screen.queryByTestId('restoreAccount');
      expect(restoreAccount).not.toBeInTheDocument();
      expect(deleteAccount).toBeDefined();
    });
  });
  describe('Deleted operator', () => {
    test('Check if component renders correctly', async () => {
      testAllDefined(await setup(mockIsPaymentEnabled));
      const deleteAccount = screen.queryByTestId('deleteAccount');
      const restoreAccount = screen.queryByTestId('restoreAccount');
      expect(deleteAccount).not.toBeInTheDocument();
      expect(restoreAccount).toBeDefined();
    });
  });
});
