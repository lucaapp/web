import React from 'react';
import { useQueryClient } from 'react-query';
import { useIntl } from 'react-intl';
import { message } from 'antd';

import { QUERY_KEYS, useOperator } from 'components/hooks';
import { PrimaryButton } from 'components/general';
import { undoAccountDeletion } from 'network/api';
import { calculateDaysRemaining } from '../AccountDeletion.helper';

import { Text, Wrapper, ButtonWrapper } from '../AccountDeletion.styled';

export const RestoreAccount = () => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const { data: operator } = useOperator();

  const daysRemaining = calculateDaysRemaining(operator);

  const onSuccess = () => {
    queryClient.invalidateQueries(QUERY_KEYS.ME);
    message.success(
      intl.formatMessage({ id: 'account.delete.reactivation.success' })
    );
  };
  const onError = () => {
    message.error(intl.formatMessage({ id: 'error.headline' }));
  };

  const restoreAccount = () => {
    undoAccountDeletion()
      .then(response => {
        if (response.status !== 204) {
          onError();
          return;
        }
        onSuccess();
      })
      .catch(() => onError());
  };

  return (
    <Wrapper data-cy="restoreAccount">
      <Text>
        {intl.formatMessage(
          { id: 'account.delete.info.inProgress' },
          { days: daysRemaining }
        )}
      </Text>
      <ButtonWrapper>
        <PrimaryButton onClick={restoreAccount}>
          {intl.formatMessage({ id: 'account.delete.reactivate' })}
        </PrimaryButton>
      </ButtonWrapper>
    </Wrapper>
  );
};
