import React from 'react';
import { useIntl } from 'react-intl';

import { LocationCard } from 'components/general';
import { useOperator } from 'components/hooks';
import { DeleteAccount } from './DeleteAccount';
import { RestoreAccount } from './RestoreAccount';

export const AccountDeletion = ({ isPaymentEnabled }) => {
  const intl = useIntl();
  const { data: operator } = useOperator();
  const deletionInProgress = !!operator.deletedAt;

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({
        id: `account.delete.heading${deletionInProgress ? '.inProgress' : ''}`,
      })}
      testId="accountDeletion"
    >
      {deletionInProgress ? (
        <RestoreAccount />
      ) : (
        <DeleteAccount isPaymentEnabled={isPaymentEnabled} />
      )}
    </LocationCard>
  );
};
