import styled from 'styled-components';
import { SecondaryButton } from 'components/general';

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const StyledSecondaryButton = styled(SecondaryButton)`
  margin-right: 16px;
`;
