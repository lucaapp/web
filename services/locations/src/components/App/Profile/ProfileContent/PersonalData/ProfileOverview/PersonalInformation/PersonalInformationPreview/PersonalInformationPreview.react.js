import React from 'react';
import { useIntl } from 'react-intl';

import { PrimaryButton } from 'components/general';
import { useOperator } from 'components/hooks';
import { EmailChangeAlert } from '../EmailChangeAlert';
import {
  ProfileContent,
  ButtonWrapper,
  ProfileTitle,
  ProfileDescription,
} from './PersonalInformationPreview.styled';

export const PersonalInformationPreview = ({ setIsEditable }) => {
  const intl = useIntl();
  const { data: operator } = useOperator();

  const { firstName, lastName, email, phone } = operator;

  return (
    <ProfileContent data-cy="profile-personalDataSection">
      {firstName && lastName && (
        <>
          <ProfileTitle>
            {intl.formatMessage({ id: 'contactForm.modal.name' })}
          </ProfileTitle>
          <ProfileDescription marginBottom="16px">{`${firstName} ${lastName}`}</ProfileDescription>
        </>
      )}
      <>
        <ProfileTitle>
          {intl.formatMessage({ id: 'settings.location.phone' })}
        </ProfileTitle>
        {phone ? (
          <ProfileDescription marginBottom="16px">{phone}</ProfileDescription>
        ) : (
          <ProfileDescription marginBottom="16px">
            {intl.formatMessage({ id: 'profile.personalData.noPhone' })}
          </ProfileDescription>
        )}
      </>

      {email && (
        <>
          <ProfileTitle>
            {intl.formatMessage({ id: 'profile.email' })}
          </ProfileTitle>
          <ProfileDescription marginBottom="16px">{email}</ProfileDescription>
        </>
      )}
      <EmailChangeAlert />
      <ButtonWrapper>
        <PrimaryButton
          data-cy="editPersonalProfileInfo"
          onClick={() => {
            setIsEditable(true);
          }}
        >
          {intl.formatMessage({ id: 'location.edit' })}
        </PrimaryButton>
      </ButtonWrapper>
    </ProfileContent>
  );
};
