import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { Form } from 'antd';

import {
  useEmailValidator,
  usePersonNameValidator,
  QUERY_KEYS,
  useNotifications,
  useOperator,
  useMobilePhoneValidator,
} from 'components/hooks';

import { updateOperator, updateEmail } from 'network/api';
import { PasswordModal } from 'components/App/modals/PasswordModal';

import { EmailChangeAlert } from '../EmailChangeAlert';
import { ButtonRow } from './ButtonRow';
import {
  shouldUpdateOperatorEmail,
  shouldUpdateOperatorPersonalInfo,
  getInitialValues,
} from './EditPersonalInformation.helper';

import {
  ProfileContent,
  StyledInput,
  StyledForm,
  StyledFormItem,
  ItemsWrapper,
} from './EditPersonalInformation.styled';

export const EditPersonalInformation = ({ setIsEditable }) => {
  const intl = useIntl();
  const [isPasswordModalOpen, setIsPasswordModalOpen] = useState(false);
  const [newMail, setNewMail] = useState(null);
  const queryClient = useQueryClient();
  const [form] = Form.useForm();
  const { data: operator } = useOperator();
  const { successMessage, errorMessage } = useNotifications();

  const firstNameValidator = usePersonNameValidator('firstName');
  const lastNameValidator = usePersonNameValidator('lastName');
  const emailValidator = useEmailValidator();
  const phoneValidator = useMobilePhoneValidator();

  const updateUserErrorMessage = 'notification.profile.updateUser.error';

  const handleUpdateEmailError = errorString => {
    setNewMail(null);
    errorMessage(errorString);
    form.resetFields();
  };

  const resetForm = () => {
    setNewMail(null);
    setIsPasswordModalOpen(false);
    setIsEditable(false);
    form.resetFields();
  };

  const onChangeEmail = password => {
    updateEmail({ email: newMail, lang: intl.locale, password })
      .then(response => {
        if (response.status === 204) {
          queryClient.invalidateQueries(
            QUERY_KEYS.IS_MAIL_CHALLENGE_IN_PROGRESS
          );
          successMessage('notification.profile.updateUser.success');
          resetForm();
        } else if (response.status === 403) {
          resetForm();
          handleUpdateEmailError(
            'notification.profile.updateEmail.error.incorrectPassword'
          );
        } else {
          resetForm();
          handleUpdateEmailError('notification.profile.updateEmail.error');
        }
      })
      .catch(() => {
        resetForm();
        handleUpdateEmailError('notification.profile.updateEmail.error');
      });
  };

  const updateOperatorInfo = async infoData => {
    const response = await updateOperator(infoData);

    if (response.status !== 204) {
      errorMessage(updateUserErrorMessage);
      return;
    }

    queryClient.invalidateQueries(QUERY_KEYS.ME);
  };

  const onFinish = values => {
    const { firstName, lastName, email, phone } = values;
    const infoData = {
      firstName,
      lastName,
      phone,
    };

    if (
      !shouldUpdateOperatorPersonalInfo(values, operator) &&
      !shouldUpdateOperatorEmail(values, operator)
    ) {
      resetForm();
      return;
    }

    if (
      shouldUpdateOperatorPersonalInfo(values, operator) &&
      !shouldUpdateOperatorEmail(values, operator)
    ) {
      try {
        updateOperatorInfo(infoData);
        successMessage('notification.profile.updateUser.success');
        setIsEditable(false);
      } catch {
        errorMessage(updateUserErrorMessage);
      }
      return;
    }

    if (
      shouldUpdateOperatorEmail(values, operator) &&
      !shouldUpdateOperatorPersonalInfo(values, operator)
    ) {
      setIsPasswordModalOpen(true);
      setNewMail(email);
      return;
    }

    if (
      shouldUpdateOperatorPersonalInfo(values, operator) &&
      shouldUpdateOperatorEmail(values, operator)
    ) {
      try {
        updateOperatorInfo(infoData);
      } catch {
        errorMessage(updateUserErrorMessage);
      } finally {
        setIsPasswordModalOpen(true);
        setNewMail(email);
      }
    }
  };

  const onCancel = () => {
    form.resetFields();
    setIsEditable(false);
  };

  return (
    <>
      <ProfileContent data-cy="profilePersonalEdit">
        <StyledForm
          onFinish={onFinish}
          form={form}
          initialValues={getInitialValues(operator)}
        >
          <ItemsWrapper>
            <StyledFormItem
              colon={false}
              label={intl.formatMessage({
                id: 'generic.firstName',
              })}
              name="firstName"
              rules={firstNameValidator}
            >
              <StyledInput />
            </StyledFormItem>
            <StyledFormItem
              colon={false}
              label={intl.formatMessage({
                id: 'generic.lastName',
              })}
              name="lastName"
              rules={lastNameValidator}
            >
              <StyledInput />
            </StyledFormItem>
          </ItemsWrapper>
          <ItemsWrapper>
            <StyledFormItem
              colon={false}
              label={intl.formatMessage({
                id: 'settings.location.phone',
              })}
              name="phone"
              rules={phoneValidator}
            >
              <StyledInput />
            </StyledFormItem>
            <StyledFormItem
              colon={false}
              label={intl.formatMessage({
                id: 'registration.form.email',
              })}
              name="email"
              rules={emailValidator}
            >
              <StyledInput data-cy="profile-emailInputField" />
            </StyledFormItem>
          </ItemsWrapper>
        </StyledForm>
        <EmailChangeAlert />
        <ButtonRow form={form} onCancel={onCancel} />
      </ProfileContent>
      {isPasswordModalOpen && (
        <PasswordModal callback={onChangeEmail} onClose={resetForm} />
      )}
    </>
  );
};
