import { Form } from 'antd';
import styled from 'styled-components';

import { SecondaryButton } from 'components/general';

export const ProfileContent = styled.div`
  padding: 24px 32px;
  background-color: white;
  border-radius: 0 0 12px 12px;
`;

export const Heading = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-weight: 600;
  font-family: Montserrat-Bold, sans-serif;
  margin-bottom: 16px;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const StyledSecondaryButton = styled(SecondaryButton)`
  margin-right: 16px;
`;

export const StyledForm = styled(Form)`
  width: 100%;
  display: flex;
  flex-direction: column;

  @media (max-width: 1024px) {
    flex-wrap: wrap;
  }
`;

export const StyledFormItem = styled(Form.Item)`
  width: 420px;
  margin-right: 40px;
  @media (max-width: 1024px) {
    max-width: 100%;
  }
`;

export const CurrentPasswordWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 16px;
`;

export const NewPasswordWrapper = styled.div`
  display: flex;
  align-items: flex-start;

  @media (max-width: 1024px) {
    flex-direction: column;
    max-width: 100%;
  }
`;

export const NewPasswordAndStrengthMeterWrapper = styled.div`
  display: flex;
  flex-direction: column;

  @media (max-width: 1024px) {
    width: 100%;
  }
`;
