import React from 'react';
import { PersonalInformation } from './PersonalInformation';

export const ProfileOverview = () => <PersonalInformation />;
