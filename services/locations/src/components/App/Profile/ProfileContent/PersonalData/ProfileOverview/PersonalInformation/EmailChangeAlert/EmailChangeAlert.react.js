import React from 'react';
import { useIntl } from 'react-intl';

import { useQueryMailChange } from 'components/hooks';
import { StyledAlert } from './EmailChangeAlert.styled';

export const EmailChangeAlert = () => {
  const intl = useIntl();

  const { data: emailChangeIsActive } = useQueryMailChange();

  return (
    <>
      {emailChangeIsActive && (
        <StyledAlert
          data-cy="activeEmailChange"
          type="info"
          message={intl.formatMessage({
            id: 'profile.changeEmailProgress',
          })}
        />
      )}
    </>
  );
};
