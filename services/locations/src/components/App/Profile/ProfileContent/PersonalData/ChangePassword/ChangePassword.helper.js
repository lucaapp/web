import { notification } from 'antd';

export const handleResponse = (response, intl, form, errorMessage) => {
  switch (response.status) {
    case 204: {
      notification.success({
        message: intl.formatMessage({
          id: 'profile.changePassword.success',
        }),
        className: 'notificationChangePasswordSuccess',
      });
      form.resetFields();

      break;
    }
    case 400: {
      errorMessage('profile.changePassword.error.weakPassword');

      break;
    }
    case 403: {
      errorMessage('profile.changePassword.error.wrongPassword');

      break;
    }
    case 500: {
      errorMessage('profile.changePassword.error.wrongUser');

      break;
    }
    default:
  }
};
