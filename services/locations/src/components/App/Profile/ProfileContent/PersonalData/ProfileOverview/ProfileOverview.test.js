import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { ProfileOverview } from './ProfileOverview.react';

const mockOperator = {
  operatorId: '3ccd4411-08eb-4325-a46e-816ce64f7071',
};

describe('ProfileOverview', () => {
  const setup = async () => {
    const renderResult = render(<ProfileOverview operator={mockOperator} />);

    const personalInformationPreview = await screen.findByTestId(
      'profile-personalDataSection'
    );

    return {
      renderResult,
      personalInformationPreview,
    };
  };
  test('Check if component renders correctly', async () => {
    testAllDefined(await setup());
  });
});
