import React, { useState } from 'react';
import { PersonalInformationPreview } from './PersonalInformationPreview';
import { EditPersonalInformation } from './EditPersonalInformation';

export const PersonalInformation = () => {
  const [isEditable, setIsEditable] = useState(false);

  return (
    <>
      {!isEditable ? (
        <PersonalInformationPreview setIsEditable={setIsEditable} />
      ) : (
        <EditPersonalInformation setIsEditable={setIsEditable} />
      )}
    </>
  );
};
