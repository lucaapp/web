import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Input, Form } from 'antd';
import { zxcvbnWithLanguageOption } from 'config.zxcvbn';

import { getLanguage } from 'services';
import { PrimaryButton } from 'components/general';
import { useNotifications, useOperator } from 'components/hooks';
import { changePassword } from 'network/api';
import { StrengthMeter } from 'components/general/StrengthMeter';
import { MIN_STRENGTH_SCORE } from 'constants/general';
import { handleResponse } from './ChangePassword.helper';
import {
  ButtonWrapper,
  Heading,
  NewPasswordAndStrengthMeterWrapper,
  NewPasswordWrapper,
  ProfileContent,
  StyledForm,
  StyledFormItem,
  StyledSecondaryButton,
} from './ChangePassword.styled';

export const ChangePassword = () => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const { errorMessage } = useNotifications();
  const { data: operator } = useOperator();

  const [strengthScore, setStrengthScore] = useState(0);
  const [warningsList, setWarningsList] = useState([]);
  const [displayStrengthMeter, setDisplayStrengthMeter] = useState(false);

  const { firstName, lastName, email } = operator;

  const onCancel = () => {
    form.resetFields();
    setDisplayStrengthMeter(false);
    setStrengthScore(0);
  };

  const onChange = values => {
    const { newPassword } = values;

    if (newPassword === '') {
      setDisplayStrengthMeter(false);
      setStrengthScore(0);
    }

    if (newPassword) {
      setDisplayStrengthMeter(true);
      const clientLang = getLanguage();

      const passwordStrength = zxcvbnWithLanguageOption(
        newPassword,
        [firstName, lastName, email],
        clientLang
      );

      setStrengthScore(passwordStrength.score);
      setWarningsList([
        passwordStrength?.feedback?.warning,
        ...passwordStrength?.feedback?.suggestions,
      ]);
    }
  };

  const onFinish = values => {
    const { currentPassword, newPassword } = values;

    changePassword({ currentPassword, newPassword })
      .then(response => {
        setDisplayStrengthMeter(false);
        setStrengthScore(0);

        handleResponse(response, intl, form, errorMessage);
      })
      .catch(() =>
        errorMessage(
          'registration.server.error.desc',
          'registration.server.error.msg'
        )
      );
  };

  const submitForm = () => {
    form.submit();
  };

  return (
    <ProfileContent data-cy="profile-changePasswordSection">
      <Heading data-cy="changePasswordSectionTitle">
        {intl.formatMessage({ id: 'profile.changePassword' })}
      </Heading>
      <StyledForm
        form={form}
        onFinish={onFinish}
        onValuesChange={onChange}
        autoComplete="off"
        data-cy="changePasswordForm"
      >
        <StyledFormItem
          colon={false}
          data-cy="currentPasswordFormItem"
          name="currentPassword"
          label={intl.formatMessage({
            id: 'profile.changePassword.oldPassword',
          })}
          rules={[
            {
              required: true,
              message: (
                <div data-cy="errorProvidePassword">
                  {intl.formatMessage({
                    id: 'error.password',
                  })}
                </div>
              ),
            },
          ]}
        >
          <Input.Password data-cy="currentPasswordInputField" />
        </StyledFormItem>

        <NewPasswordWrapper>
          <NewPasswordAndStrengthMeterWrapper>
            <StyledFormItem
              colon={false}
              data-cy="newPasswordFormItem"
              name="newPassword"
              hasFeedback
              label={intl.formatMessage({
                id: 'profile.changePassword.newPassword',
              })}
              rules={[
                {
                  required: true,
                  message: (
                    <div data-cy="errorProvidePassword">
                      {intl.formatMessage({
                        id: 'error.password',
                      })}
                    </div>
                  ),
                },
                () => ({
                  validator() {
                    if (
                      displayStrengthMeter &&
                      strengthScore < MIN_STRENGTH_SCORE
                    ) {
                      return Promise.reject(warningsList);
                    }

                    return Promise.resolve();
                  },
                }),
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (getFieldValue('currentPassword') === value)
                      return Promise.reject(
                        <div data-cy="errorPasswordSame">
                          {intl.formatMessage({ id: 'error.password.same' })}
                        </div>
                      );

                    return Promise.resolve();
                  },
                }),
              ]}
            >
              <Input.Password data-cy="newPasswordInputField" />
            </StyledFormItem>
            {displayStrengthMeter && (
              <StrengthMeter strengthScore={strengthScore} lightVariation />
            )}
          </NewPasswordAndStrengthMeterWrapper>

          <StyledFormItem
            colon={false}
            data-cy="newPasswordConfirmFormItem"
            name="newPasswordConfirm"
            label={intl.formatMessage({
              id: 'profile.changePassword.newPasswordRepeat',
            })}
            hasFeedback
            dependencies={['newPassword']}
            rules={[
              {
                required: true,
                message: intl.formatMessage({
                  id: 'error.passwordConfirm',
                }),
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue('newPassword') === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    <div data-cy="errorPasswordMismatch">
                      {intl.formatMessage({ id: 'error.passwordConfirm' })}
                    </div>
                  );
                },
              }),
            ]}
          >
            <Input.Password data-cy="newPasswordConfirmInputField" />
          </StyledFormItem>
        </NewPasswordWrapper>
      </StyledForm>
      <ButtonWrapper>
        <StyledSecondaryButton onClick={onCancel}>
          {intl.formatMessage({ id: 'generic.cancel' })}
        </StyledSecondaryButton>
        <PrimaryButton data-cy="changePasswordButton" onClick={submitForm}>
          {intl.formatMessage({ id: 'profile.overview.submit' })}
        </PrimaryButton>
      </ButtonWrapper>
    </ProfileContent>
  );
};
