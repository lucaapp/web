import _ from 'lodash';

const getPersonalInfo = value => {
  const { firstName, lastName, phone } = value;
  return [firstName, lastName, phone];
};

export const shouldUpdateOperatorEmail = (values, operator) =>
  values.email !== operator.email;

export const shouldUpdateOperatorPersonalInfo = (values, operator) => {
  return !_.isEqual(getPersonalInfo(values), getPersonalInfo(operator));
};

export const getInitialValues = operator => {
  const { firstName, lastName, email, phone } = operator;

  return {
    firstName,
    lastName,
    email,
    phone,
  };
};
