import styled, { css } from 'styled-components';
import { Input, Form } from 'antd';
import { Media } from 'utils/media';

export const ProfileContent = styled.div`
  padding: 24px 0;
  margin: 0 32px;
  background-color: white;
  border-bottom: 1px solid rgb(151, 151, 151);
`;

const sharedInputStyle = css`
  border: 1px solid #696969 !important;
  background-color: transparent !important;

  &:hover,
  &:active,
  &:focus {
    border: 1px solid #696969 !important;
    background-color: transparent !important;
  }

  ${Media.mobile`
    border-radius: 0px !important;
    border: 0.5px solid rgb(0, 0, 0) !important;
    line-height: 48px !important;
    height: 48px !important;
  `}
`;

export const StyledInput = styled(Input)`
  ${sharedInputStyle}
`;

export const StyledForm = styled(Form)`
  display: flex;
  flex-direction: column;
`;

export const StyledFormItem = styled(Form.Item)`
  width: 420px;
  margin-right: 16px;
`;

export const ItemsWrapper = styled.div`
  display: flex;
`;
