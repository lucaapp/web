import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { PersonalData } from './PersonalData.react';

const mockOperator = {
  operatorId: '3ccd4411-08eb-4325-a46e-816ce64f7071',
};

describe('PersonalData', () => {
  test('Check if component renders correctly', async () => {
    const renderResult = await render(<PersonalData operator={mockOperator} />);

    const personalDataCard = await screen.findByTestId(
      'locationCard-personalData'
    );

    await testAllDefined({
      renderResult,
      personalDataCard,
    });
  });
});
