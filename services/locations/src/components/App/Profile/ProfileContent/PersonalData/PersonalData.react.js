import React from 'react';
import { useIntl } from 'react-intl';

// Components
import { LocationCard } from 'components/general';
import { ProfileOverview } from './ProfileOverview';
import { ChangePassword } from './ChangePassword';

export const PersonalData = () => {
  const intl = useIntl();

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'profile.personalData' })}
      testId="personalData"
    >
      <ProfileOverview />
      <ChangePassword />
    </LocationCard>
  );
};
