import React from 'react';
import { useIntl } from 'react-intl';

// Components
import { LocationCard } from 'components/general';
import { Services } from './Services';
import { ResetKey } from './ResetKey';

export const ProfileServices = ({ accountDeletionInProgress }) => {
  const intl = useIntl();

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'profile.services.overview' })}
      testId="profileServices"
    >
      <Services />
      {!accountDeletionInProgress && <ResetKey />}
    </LocationCard>
  );
};
