import styled from 'styled-components';

export const ResetKeyContent = styled.div`
  padding: 24px 32px;
  background-color: white;
  border-radius: 0 0 12px 12px;
`;

export const Heading = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const PrivateKeyInfo = styled.div``;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 32px;
`;
