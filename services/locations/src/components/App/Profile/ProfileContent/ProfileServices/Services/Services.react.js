import React from 'react';
import { useIntl } from 'react-intl';

import { useGetPaymentOperator, useOperator } from 'components/hooks';
import { PayServices } from 'components/App/Profile/ProfileContent/ProfileServices/Services/PaymentServices';
import { getDownloadLinks, getExternalLinks } from './Services.helper';
import {
  Content,
  Wrapper,
  Link,
  StyledIcon,
  Description,
} from './Services.styled';

export const Services = () => {
  const intl = useIntl();

  const { data: operator } = useOperator();
  const downloadLinks = getDownloadLinks(intl);
  const externalLinks = getExternalLinks(intl);

  const {
    isLoading: isPayOperatorLoading,
    error: payOperatorError,
    data: paymentOperator,
  } = useGetPaymentOperator(operator.operatorId);

  if (payOperatorError || isPayOperatorLoading) {
    return null;
  }

  return (
    <Content data-cy="profile-servicesSection">
      <Description>
        {intl.formatMessage({ id: 'profile.services.description' })}
      </Description>
      <Wrapper>
        {downloadLinks.map(({ intlId, dataCy, download, href, icon }) => (
          <Link key={intlId} data-cy={dataCy} download={download} href={href}>
            {intlId}
            <StyledIcon data-cy="downloadIcon" component={icon.component} />
          </Link>
        ))}
        {externalLinks.map(({ intlId, dataCy, href, icon }) => (
          <Link
            key={intlId}
            data-cy={dataCy}
            target="_blank"
            rel="noopener noreferrer"
            href={href}
          >
            {intlId}
            <StyledIcon
              data-cy="externalLinksIcon"
              component={icon.component}
            />
          </Link>
        ))}
        {paymentOperator?.payTermsAccepted && <PayServices />}
      </Wrapper>
    </Content>
  );
};
