import React from 'react';
import { useIntl } from 'react-intl';
import {
  Link,
  Wrapper,
  StyledIcon,
} from 'components/App/Profile/ProfileContent/ProfileServices/Services/Services.styled';
import {
  getLucaPayDpa,
  getLucaPayTermsAndConditions,
} from './PayServices.helper';

export const PayServices = () => {
  const intl = useIntl();

  const lucaPayDpa = getLucaPayDpa(intl);
  const lucaPayTermsAndConditions = getLucaPayTermsAndConditions(intl);

  return (
    <Wrapper>
      {lucaPayDpa.map(({ intlId, dataCy, download, href, icon }) => (
        <Link key={intlId} data-cy={dataCy} download={download} href={href}>
          {intlId}
          <StyledIcon component={icon.component} />
        </Link>
      ))}
      {lucaPayTermsAndConditions.map(({ intlId, dataCy, href, icon }) => (
        <Link
          key={intlId}
          data-cy={dataCy}
          target="_blank"
          rel="noopener noreferrer"
          href={href}
        >
          {intlId}
          <StyledIcon component={icon.component} />
        </Link>
      ))}
    </Wrapper>
  );
};
