import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const Content = styled.div`
  padding: 24px 0 0 0;
  margin: 0 32px;
  background-color: white;
  border-bottom: 0.5px solid rgb(197, 195, 190);
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 24px;
`;

export const Heading = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 16px;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Link = styled.a`
  color: rgb(80, 102, 124);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 24px;
  text-decoration: none;

  .anticon {
    margin-left: 8px;
  }

  :hover {
    color: rgb(80, 102, 124);
  }
`;

export const StyledIcon = styled(Icon)`
  font-size: 16px;
`;
