import React from 'react';
import { Modal, PrimaryButton } from 'components/general';
import { useIntl } from 'react-intl';
import { Spin } from 'antd';
import { ResetKey as ResetKeyLoader } from 'components/PrivateKeyLoader/ResetKey';
import { useGetPrivateKeySecret } from 'components/hooks';
import {
  ButtonWrapper,
  ResetKeyContent,
  Heading,
  PrivateKeyInfo,
} from './ResetKey.styled';

export const ResetKey = () => {
  const intl = useIntl();
  const {
    data: privateKeySecret,
    isLoading: isPrivateKeySecretLoading,
  } = useGetPrivateKeySecret();

  const openModalButton = (
    <ButtonWrapper>
      <PrimaryButton data-cy="resetPrivateKeyButton">
        {intl.formatMessage({ id: 'profile.resetKey' })}
      </PrimaryButton>
    </ButtonWrapper>
  );

  const modalContent = () => (
    <ResetKeyLoader privateKeySecret={privateKeySecret} />
  );

  if (isPrivateKeySecretLoading) return <Spin />;

  return (
    <ResetKeyContent data-cy="profile-resetPrivateKeySection">
      <Heading> {intl.formatMessage({ id: 'profile.resetKey' })}</Heading>
      <PrivateKeyInfo>
        {intl.formatMessage({ id: 'privateKey.modal.info' })}
      </PrivateKeyInfo>
      <Modal content={modalContent} openButton={openModalButton} />
    </ResetKeyContent>
  );
};
