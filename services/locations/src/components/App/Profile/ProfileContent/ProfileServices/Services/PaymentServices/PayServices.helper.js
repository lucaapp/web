import {
  PAYMENT_DPA_LINK,
  PAYMENT_TERMS_CONDITIONS_LINK,
} from 'constants/links';

import { ExternalLinkIcon } from 'assets/icons';

export const getLucaPayDpa = intl => [
  {
    dataCy: 'lucaPayDpaLink',
    href: PAYMENT_DPA_LINK,
    icon: {
      component: ExternalLinkIcon,
    },
    intlId: intl.formatMessage({ id: 'profile.services.lucapay.download.avv' }),
  },
];

export const getLucaPayTermsAndConditions = intl => [
  {
    dataCy: 'LucaPayTermsLink',
    href: PAYMENT_TERMS_CONDITIONS_LINK,
    icon: {
      component: ExternalLinkIcon,
    },
    intlId: intl.formatMessage({ id: 'profile.services.lucapay.agb' }),
  },
];
