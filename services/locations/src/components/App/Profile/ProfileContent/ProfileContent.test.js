import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { operator } from '__mocks__/storeSelectors';
import { ProfileContent } from './ProfileContent.react';

const mockPayEnabled = { paymenEnabled: false };

jest.mock('components/hooks/useQueries', () => ({
  useGetPaymentEnabled: () => ({
    data: mockPayEnabled,
    isError: false,
    isLoading: false,
  }),
}));

const privacyMandatoryHref = '/static/media/DSE_Luca_mandatory.pdf';
const privacyOptionalHref = '/static/media/DSE_Luca_optional.pdf';
const avvHref = '/static/media/AVV_Luca.pdf';
const tomsHref = '/static/media/TOMS_Luca.pdf';

jest.mock(
  'assets/documents/DSE_Luca_mandatory.pdf',
  () => privacyMandatoryHref
);

jest.mock('assets/documents/DSE_Luca_optional.pdf', () => privacyOptionalHref);
jest.mock('assets/documents/AVV_Luca.pdf', () => avvHref);
jest.mock('assets/documents/TOMS_Luca.pdf', () => tomsHref);

describe('Profile Content Component', () => {
  const setup = async () => {
    const renderResult = render(<ProfileContent />);

    const wrapper = await screen.findByTestId('profilePageWrapper');
    const header = await screen.findByTestId('profilePageHeader');

    const accountDeletioncard = await screen.findByTestId(
      'locationCard-accountDeletion'
    );

    return {
      renderResult,
      wrapper,
      header,
      accountDeletioncard,
    };
  };

  afterEach(() => {
    delete operator.data.deletedAt;
  });

  describe('Non deleted operator', () => {
    beforeEach(() => {
      operator.data.deletedAt = null;
    });

    test('Check if component renders correctly', async () => {
      const result = await setup();

      const personalData = screen.getByTestId('locationCard-personalData');
      const businessData = screen.getByTestId('locationCard-businessData');
      const profileServicesCard = screen.getByTestId(
        'locationCard-profileServices'
      );
      testAllDefined({
        ...result,
        businessData,
        personalData,
        profileServicesCard,
      });
    });
  });

  describe('Deleted operator', () => {
    beforeEach(() => {
      operator.data.deletedAt = Date.now();
    });

    test('Check if component renders correctly', async () => {
      testAllDefined(await setup());

      const personalData = screen.queryByTestId('locationCard-personalData');
      const businessData = screen.queryByTestId('locationCard-businessData');
      const profileServicesCard = screen.queryByTestId(
        'locationCard-profileServices'
      );
      expect(personalData).not.toBeInTheDocument();
      expect(businessData).not.toBeInTheDocument();
      expect(profileServicesCard).not.toBeInTheDocument();
    });
  });
});
