import React from 'react';
import { Layout } from 'antd';

import { Content, NavigationButton, Sider } from 'components/general';

import { ProfileContent } from './ProfileContent';

export const Profile = () => {
  return (
    <Layout>
      <Sider>
        <NavigationButton />
      </Sider>
      <Content>
        <ProfileContent />
      </Content>
    </Layout>
  );
};
