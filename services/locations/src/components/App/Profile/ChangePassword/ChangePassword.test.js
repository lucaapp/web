import React from 'react';
import { render, testAllDefined, screen } from 'utils/testing';
import { fireEvent } from '@testing-library/dom';
import { act } from 'react-dom/test-utils';
import * as api from 'network/api';
import { ChangePassword } from '../ProfileContent/PersonalData/ChangePassword';

const mockedOperator = {
  firstName: 'Some',
  lastName: 'Dude',
  email: 'some@dud.es',
};

const mockedPasswords = {
  empty: '',
  veryWeak: 'a',
  weak: 'a123',
  medium: 'a123qwertz',
  strong: 'a123qwertz!',
  veryStrong: '*#*]^k8Uyp{u7HK',
};

describe('ChangePasswordComponent', () => {
  const setup = async () => {
    const renderResult = render(<ChangePassword operator={mockedOperator} />);

    const profileChangePasswordSection = await screen.findByTestId(
      'profile-changePasswordSection'
    );
    const changePasswordSectionTitle = await screen.findByTestId(
      'changePasswordSectionTitle'
    );
    const changePasswordForm = await screen.findByTestId('changePasswordForm');
    const currentPasswordFormItem = await screen.findByTestId(
      'currentPasswordFormItem'
    );
    const currentPasswordInputField = await screen.findByTestId(
      'currentPasswordInputField'
    );
    const newPasswordFormItem = await screen.findByTestId(
      'newPasswordFormItem'
    );
    const newPasswordInputField = await screen.findByTestId(
      'newPasswordInputField'
    );
    const newPasswordConfirmFormItem = await screen.findByTestId(
      'newPasswordConfirmFormItem'
    );
    const newPasswordConfirmInputField = await screen.findByTestId(
      'newPasswordConfirmInputField'
    );
    const changePasswordButton = await screen.findByTestId(
      'changePasswordButton'
    );

    return {
      renderResult,
      profileChangePasswordSection,
      changePasswordButton,
      changePasswordSectionTitle,
      changePasswordForm,
      currentPasswordFormItem,
      currentPasswordInputField,
      newPasswordFormItem,
      newPasswordInputField,
      newPasswordConfirmFormItem,
      newPasswordConfirmInputField,
    };
  };

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });

  test('Check password criteria', async () => {
    const { renderResult, newPasswordInputField } = await setup();
    expect(renderResult).toBeDefined();

    const checkPasswordLevel = async (key, value) => {
      fireEvent.change(newPasswordInputField, {
        target: { value },
      });
      const element = await screen.findByTestId(key);
      expect(element).toBeDefined();
    };

    await checkPasswordLevel(
      'descriptionPasswordVeryWeak',
      mockedPasswords.veryWeak
    );
    await checkPasswordLevel('descriptionPasswordWeak', mockedPasswords.weak);
    await checkPasswordLevel(
      'descriptionPasswordMedium',
      mockedPasswords.medium
    );
    await checkPasswordLevel(
      'descriptionPasswordStrong',
      mockedPasswords.strong
    );
    await checkPasswordLevel(
      'descriptionPasswordVeryStrong',
      mockedPasswords.veryStrong
    );
  });

  test('Check different passwords warning', async () => {
    const {
      renderResult,
      newPasswordInputField,
      newPasswordConfirmInputField,
    } = await setup();
    expect(renderResult).toBeDefined();

    fireEvent.change(newPasswordInputField, {
      target: { value: mockedPasswords.weak },
    });
    expect(newPasswordInputField.value).toEqual(mockedPasswords.weak);

    fireEvent.change(newPasswordConfirmInputField, {
      target: { value: mockedPasswords.veryWeak },
    });
    expect(newPasswordConfirmInputField.value).toEqual(
      mockedPasswords.veryWeak
    );

    const errorPasswordMismatch = await screen.findByTestId(
      'errorPasswordMismatch'
    );
    expect(errorPasswordMismatch).toBeDefined();
  });

  test('Check submit password change', async () => {
    const {
      renderResult,
      currentPasswordInputField,
      newPasswordInputField,
      newPasswordConfirmInputField,
      changePasswordButton,
    } = await setup();

    // eslint-disable-next-line require-await
    await act(async () => {
      fireEvent.change(currentPasswordInputField, {
        target: { value: mockedPasswords.medium },
      });
      fireEvent.change(newPasswordInputField, {
        target: { value: mockedPasswords.veryStrong },
      });
      fireEvent.change(newPasswordConfirmInputField, {
        target: { value: mockedPasswords.veryStrong },
      });
    });
    expect(currentPasswordInputField.value).toEqual(mockedPasswords.medium);
    expect(newPasswordInputField.value).toEqual(mockedPasswords.veryStrong);
    expect(newPasswordConfirmInputField.value).toEqual(
      mockedPasswords.veryStrong
    );

    const testResponseNotification = async (status, intlKey) => {
      // with spyOn we can specify the response of changePassword when called after submit
      jest.spyOn(api, 'changePassword').mockResolvedValue({
        status,
      });
      await act(async () => {
        await changePasswordButton.click();
        // we expect that response by the mocked status 403
        const result = await renderResult.findAllByText(intlKey);
        expect(result).toBeTruthy();
      });
    };
    await testResponseNotification(
      400,
      'profile.changePassword.error.weakPassword'
    );
    await testResponseNotification(
      403,
      'profile.changePassword.error.wrongPassword'
    );
    await testResponseNotification(
      500,
      'profile.changePassword.error.wrongUser'
    );
    await testResponseNotification(204, 'profile.changePassword.success');
  });
});
