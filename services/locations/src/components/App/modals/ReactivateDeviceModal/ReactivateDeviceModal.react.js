import React, { useEffect, useCallback, useState } from 'react';

import { notification } from 'antd';
import { useQueryClient } from 'react-query';
import { useIntl } from 'react-intl';

import { reactivateDevice } from 'network/api';

import { generatePIN, generateTransferQRCodeData } from 'utils/operatorDevice';

import { useChallenge, QUERY_KEYS, useNotifications } from 'components/hooks';

import { Steps } from 'components/general';
import { LoadingSpinner } from 'components/general/LoadingSpinner';
import { useModalContext } from 'components/general/Modal/useModalContext';

import { useActivateDeviceSteps } from '../CreateDeviceModal/CreateDeviceModal.helper';

export const ReactivateDeviceModal = ({ deviceId }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const { challenge, challengeState } = useChallenge();
  const { closeModal, isModalVisible } = useModalContext();
  const [currentStep, setCurrentStep] = useState(0);
  const { errorMessage } = useNotifications();

  const [authenticationPIN] = useState(generatePIN());
  const [authenticationQRCode, setAuthenticationQRCode] = useState(null);

  const onReactivate = useCallback(async () => {
    if (!challenge || authenticationQRCode) return;

    try {
      const device = await reactivateDevice(deviceId);

      const authenticationQRCodeData = await generateTransferQRCodeData(
        'AUTHENTICATION_TOKEN',
        device.refreshToken,
        authenticationPIN,
        challenge.uuid
      );

      setAuthenticationQRCode(authenticationQRCodeData);
    } catch {
      errorMessage('notification.device.error');

      closeModal();
    }
  }, [
    challenge,
    authenticationQRCode,
    deviceId,
    authenticationPIN,
    errorMessage,
    closeModal,
  ]);

  const steps = useActivateDeviceSteps(authenticationQRCode, authenticationPIN);

  const onDone = useCallback(() => {
    closeModal();
    queryClient.invalidateQueries(QUERY_KEYS.OPERATOR_DEVICES);
  }, [closeModal, queryClient]);

  useEffect(() => {
    onReactivate();
  }, [onReactivate]);

  useEffect(() => {
    if (challengeState.authPinRequired) {
      setCurrentStep(1);
      return;
    }

    if (challengeState.done) {
      notification.success({
        message: intl.formatMessage({ id: 'notification.device.success' }),
      });
      onDone();
      return;
    }

    if (challengeState.canceled) {
      onDone();
    }
  }, [challengeState, intl, isModalVisible, onDone]);

  if (!authenticationQRCode) {
    return <LoadingSpinner />;
  }

  return <Steps steps={steps} currentStep={currentStep} />;
};
