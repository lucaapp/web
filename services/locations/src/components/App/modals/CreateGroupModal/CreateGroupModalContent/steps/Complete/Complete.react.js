import React from 'react';
import { useIntl } from 'react-intl';

import { Wrapper } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { CompleteCard } from 'components/App/modals/generalOnboarding/common/CompleteCard';
import { StepProgress } from 'components/App/modals/generalOnboarding/StepProgress';
import { QRDownload } from './QRDownload';

export const Complete = ({ back, done, group, totalSteps, currentStep }) => {
  const intl = useIntl();
  if (!group) {
    back();
  }

  return (
    <Wrapper>
      <StepProgress currentStep={currentStep} totalSteps={totalSteps} />
      <CompleteCard
        title={intl.formatMessage({
          id: 'modal.createGroup.complete.title',
        })}
        description={intl.formatMessage({
          id: 'modal.createGroup.complete.description',
        })}
      >
        <QRDownload group={group} done={done} />
      </CompleteCard>
    </Wrapper>
  );
};
