import { Form } from 'antd';
import { v4 as uuidv4 } from 'uuid';
import { PlusCircleOutlined } from '@ant-design/icons';
import React from 'react';
import { useIntl } from 'react-intl';

import { useNameValidator } from 'components/hooks/useValidators';
import { BinIconSVG } from 'assets/icons';

import {
  StyledSelect,
  StyledInput,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { SelectInput } from 'components/App/modals/generalOnboarding/AreaDetails/common/SelectInput';
import { useOptions } from 'components/App/modals/generalOnboarding/AreaDetails/common/useOptions';
import {
  AddArea,
  AddAreaText,
  AddAreaWrapper,
  FormItemWrapper,
  InputContainer,
  RemoveButton,
  TrashIcon,
} from '../AreaDivision.styled';

export const AreaNameInput = ({ temporaryAreas, setTemporaryAreas, form }) => {
  const intl = useIntl();
  const locationNameValidator = useNameValidator('locationName');
  const { areaTypeOptions } = useOptions();

  const toggleIndoorOutdoor = (target, value) => {
    form.setFieldsValue({
      [target]: value,
    });
  };

  const onRemoveArea = index => {
    setTemporaryAreas([
      ...temporaryAreas.slice(0, index),
      ...temporaryAreas.slice(index + 1, temporaryAreas.length),
    ]);
  };

  const onAddArea = () => {
    setTemporaryAreas([...temporaryAreas, { id: uuidv4() }]);

    form.setFieldsValue({
      [`areaType${temporaryAreas.length}`]: null,
    });
  };

  return (
    <>
      {temporaryAreas.map((temporaryArea, index) => (
        <FormItemWrapper key={temporaryArea.id}>
          <InputContainer width="60%" marginRight="16px">
            <Form.Item
              label={intl.formatMessage({
                id: 'modal.createGroup.areaSelection.areaName',
              })}
              name={`areaName${index}`}
              rules={locationNameValidator}
            >
              <StyledInput autoFocus />
            </Form.Item>
          </InputContainer>
          <InputContainer width="40%">
            <StyledSelect>
              <SelectInput
                label={intl.formatMessage({
                  id: 'settings.location.indoorToggle.description.label',
                })}
                name={`areaType${index}`}
                dataCy={`areaType${index}`}
                handleSelect={value =>
                  toggleIndoorOutdoor(`areaType${index}`, value)
                }
                options={areaTypeOptions}
                required
              />
            </StyledSelect>
          </InputContainer>
          <RemoveButton
            onClick={() => onRemoveArea(index)}
            data-cy="removeAreaRow"
          >
            <TrashIcon src={BinIconSVG} />
          </RemoveButton>
        </FormItemWrapper>
      ))}
      <AddAreaWrapper>
        <AddArea onClick={onAddArea} data-cy="addMoreArea">
          <AddAreaText>
            {intl.formatMessage({ id: 'modal.createGroup.addArea' })}
          </AddAreaText>
          <PlusCircleOutlined style={{ fontSize: 20 }} />
        </AddArea>
      </AddAreaWrapper>
    </>
  );
};
