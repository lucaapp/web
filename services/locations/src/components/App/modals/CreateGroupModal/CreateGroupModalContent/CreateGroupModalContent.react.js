import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useQueryClient } from 'react-query';
import { Steps } from 'antd';
import { useMatomo } from '@jonkoops/matomo-tracker-react';

import { useModalContext } from 'components/general/Modal/useModalContext';
import { QUERY_KEYS } from 'components/hooks';
import {
  TableInput,
  PhoneInput,
  AreaDetails,
  GooglePlacesInput,
} from 'components/App/modals/generalOnboarding';
import { BASE_GROUP_ROUTE, BASE_LOCATION_ROUTE } from 'constants/routes';

import { SelectGroupType } from './steps/SelectGroupType';
import { AreaDivision } from './steps/AreaDivision';
import { Complete } from './steps/Complete';
import { NameInputGroup } from './steps/NameInputGroup';
import {
  SELECT_GROUP_STEP,
  NAME_INPUT_STEP,
  PHONE_INPUT_STEP,
  TABLE_INPUT_STEP,
  COMPLETE_STEP,
  AREA_SELECTION_STEP,
  getGroupPayload,
  AREA_DETAILS_STEP,
  GOOGLE_PLACES_OPT_IN_STEP,
  TOTAL_STEPS,
} from './CreateGroupModalContent.helper';

export const CreateGroupModalContent = () => {
  const history = useHistory();
  const queryClient = useQueryClient();
  const { trackEvent } = useMatomo();
  const { closeModal } = useModalContext();

  const [group, setGroup] = useState(null);
  const [currentStep, setCurrentStep] = useState(0);
  const [groupType, setGroupType] = useState(null);
  const [groupName, setGroupName] = useState(null);
  const [googlePlaces, setGooglePlaces] = useState(false);
  const [address, setAddress] = useState(null);
  const [phone, setPhone] = useState(null);
  const [areaDetails, setAreaDetails] = useState({
    isIndoor: true,
  });
  const [tableCount, setTableCount] = useState(null);
  const [coordinates, setCoordinates] = useState(null);

  const nextStep = () => setCurrentStep(currentStep + 1);
  const previousStep = () => setCurrentStep(currentStep - 1);

  const onDone = () => {
    history.push(
      `${BASE_GROUP_ROUTE}${group.groupId}${BASE_LOCATION_ROUTE}${group.location.locationId}`
    );

    trackEvent({
      category: 'location-group',
      action: 'created',
      name: groupName || '',
      value: group.groupId,
    });

    queryClient.invalidateQueries(QUERY_KEYS.GROUPS);
    closeModal();
  };

  const groupPayload = getGroupPayload({
    groupName,
    phone,
    address,
    tableCount,
    groupType,
    isIndoor: areaDetails.isIndoor,
    coordinates,
  });

  const steps = [
    {
      id: SELECT_GROUP_STEP,
      content: (
        <SelectGroupType
          setGroupType={setGroupType}
          next={nextStep}
          totalSteps={TOTAL_STEPS}
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: NAME_INPUT_STEP,
      content: (
        <NameInputGroup
          groupName={groupName}
          setGroupName={setGroupName}
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: PHONE_INPUT_STEP,
      content: (
        <PhoneInput
          phone={phone}
          setPhone={setPhone}
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
          isGroup
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: GOOGLE_PLACES_OPT_IN_STEP,
      content: (
        <GooglePlacesInput
          totalSteps={TOTAL_STEPS}
          setEnabled={setGooglePlaces}
          enabled={googlePlaces}
          address={address}
          setAddress={setAddress}
          coordinates={coordinates}
          setCoordinates={setCoordinates}
          googleEnabled={googlePlaces}
          next={nextStep}
          back={previousStep}
          isGroup
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: AREA_DETAILS_STEP,
      content: (
        <AreaDetails
          areaDetails={areaDetails}
          setAreaDetails={setAreaDetails}
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
          modalArea="createGroup"
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: TABLE_INPUT_STEP,
      content: (
        <TableInput
          tableCount={tableCount}
          setTableCount={setTableCount}
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: AREA_SELECTION_STEP,
      content: (
        <AreaDivision
          groupType={groupType}
          next={nextStep}
          back={previousStep}
          groupPayload={groupPayload}
          setGroup={setGroup}
          totalSteps={TOTAL_STEPS}
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: COMPLETE_STEP,
      content: (
        <Complete
          group={group}
          done={onDone}
          back={previousStep}
          totalSteps={TOTAL_STEPS}
          currentStep={currentStep + 1}
        />
      ),
    },
  ];

  return (
    <>
      <Steps
        progressDot={() => null}
        current={currentStep}
        style={{ margin: 0 }}
      >
        {steps.map(step => (
          <Steps.Step key={step.id} />
        ))}
      </Steps>
      {steps[currentStep].content}
    </>
  );
};
