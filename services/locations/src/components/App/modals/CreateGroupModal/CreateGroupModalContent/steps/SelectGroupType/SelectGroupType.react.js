import React from 'react';
import { useIntl } from 'react-intl';

import {
  Wrapper,
  Header,
  Description,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';

import { groupOptions } from 'components/general';

import { StepProgress } from 'components/App/modals/generalOnboarding/StepProgress';
import {
  Selection,
  TypeWrapper,
  StyledIcon,
  StyledTypeText,
} from './SelectGroupType.styled';

export const SelectGroupType = ({
  next,
  setGroupType,
  totalSteps,
  currentStep,
}) => {
  const intl = useIntl();

  const select = type => {
    setGroupType(type);
    next();
  };

  return (
    <Wrapper data-cy="selectGroupType">
      <StepProgress currentStep={currentStep} totalSteps={totalSteps} />
      <Header>
        {intl.formatMessage({ id: 'modal.createGroup.selectType.title' })}
      </Header>
      <Description>
        {intl.formatMessage({
          id: 'modal.createGroup.selectType.description',
        })}
      </Description>
      <Selection data-cy="selectGroupTypeSelection">
        {groupOptions.map(option => (
          <TypeWrapper
            data-cy={option.type}
            key={option.type}
            onClick={() => select(option.type)}
          >
            <StyledIcon component={option.icon} />
            <StyledTypeText>
              {intl.formatMessage({
                id: option.intlId,
              })}
            </StyledTypeText>
          </TypeWrapper>
        ))}
      </Selection>
    </Wrapper>
  );
};
