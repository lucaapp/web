export const findDuplicates = array => {
  return array.find((item, index) => array.indexOf(item) !== index);
};
