import React, { useState } from 'react';
import { useIntl } from 'react-intl';

import { DEFAULT_CWA_VALUE } from 'constants/cwaQrCodeValue';

// Worker
import { downloadPDF } from 'utils/downloadPDF';
import { getPDFWorker } from 'utils/workers';

import {
  ButtonWrapper,
  QRCodeDownloadWrapper,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { QRDownloadCard } from 'components/App/modals/generalOnboarding/common/QRDownloadCard';
import { PrimaryButton, SuccessButton } from 'components/general';
import {
  useGetLocationById,
  useGetLocationTables,
  useWorker,
} from 'components/hooks';

export const QRDownload = ({ done, group }) => {
  const intl = useIntl();
  const [isDownloading, setIsDownloading] = useState(false);
  const { data: location, isLoading, error } = useGetLocationById(
    group.location.locationId
  );

  const {
    error: tablesError,
    data: locationTables,
    isLoading: tablesIsLoading,
  } = useGetLocationTables(group.location.locationId);

  const pdfWorkerApiReference = useWorker(getPDFWorker());

  if (isLoading || error || tablesIsLoading || tablesError) return null;

  const downloadOptions = {
    setIsDownloading,
    pdfWorkerApiReference,
    location: { ...location, tables: locationTables },
    intl,
    isTableQRCodeEnabled: locationTables?.length > 0,
    isCWAEventEnabled: DEFAULT_CWA_VALUE,
  };
  const triggerDownload = () => downloadPDF(downloadOptions);

  const suffix = locationTables.length > 0 ? 'table' : 'entire';

  return (
    <QRDownloadCard
      title={intl.formatMessage({
        id: `modal.createGroup.QRDownload.title.${suffix}`,
      })}
      description={intl.formatMessage({
        id: `modal.createGroup.QRDownload.description.${suffix}`,
      })}
    >
      <QRCodeDownloadWrapper>
        <SuccessButton
          loading={isDownloading}
          disabled={isDownloading}
          onClick={triggerDownload}
          data-cy="downloadQRCode"
        >
          {intl.formatMessage({
            id: `qrCodesDownload.${suffix}`,
          })}
        </SuccessButton>
      </QRCodeDownloadWrapper>

      <ButtonWrapper>
        <PrimaryButton onClick={done} data-cy="done">
          {intl.formatMessage({
            id: 'done',
          })}
        </PrimaryButton>
      </ButtonWrapper>
    </QRDownloadCard>
  );
};
