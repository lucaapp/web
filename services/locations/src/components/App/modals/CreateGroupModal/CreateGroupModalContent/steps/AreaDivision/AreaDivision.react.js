import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { v4 as uuidv4 } from 'uuid';

import { Form } from 'antd';

import { Switch } from 'components/general';
import { useNotifications } from 'components/hooks';
import { createGroup as createGroupRequest } from 'network/api';
import {
  Header,
  Wrapper,
  Description,
  SwitchWrapper,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { StepProgress } from 'components/App/modals/generalOnboarding/StepProgress';
import { ButtonRow } from 'components/App/modals/generalOnboarding/common/ButtonRow';
import { useModalContext } from 'components/general/Modal/useModalContext';
import { findDuplicates } from './AreaDivision.helper';
import { AreaNameInput } from './AreaNameInput';

export const AreaDivision = ({
  setGroup,
  next,
  back,
  groupPayload,
  totalSteps,
  currentStep,
}) => {
  const intl = useIntl();
  const { successMessage, errorMessage } = useNotifications();
  const [isAreaSelection, setIsAreaSelection] = useState(false);
  const [isFormSubmitting, setIsFormSubmitting] = useState(false);
  const [temporaryAreas, setTemporaryAreas] = useState([]);
  const [form] = Form.useForm();
  const { closeModal } = useModalContext();

  const onChange = () => {
    setIsAreaSelection(!isAreaSelection);
  };

  useEffect(() => {
    if (isAreaSelection) {
      setTemporaryAreas([{ id: uuidv4() }]);

      form.setFieldsValue({
        [`areaType${temporaryAreas.length}`]: null,
      });
    } else setTemporaryAreas([]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAreaSelection]);

  const handleServerError = () => {
    errorMessage('notification.createGroup.error');
    setIsFormSubmitting(false);
    closeModal();
  };

  const handleResponse = response => {
    if (!response) {
      handleServerError();
      return;
    }
    successMessage('notification.createGroup.success');
    setGroup(response);
    setIsFormSubmitting(false);
    next();
  };

  const onFinish = async filedsValues => {
    setIsFormSubmitting(true);

    const areaNames = Object.values(filedsValues).filter(
      name => typeof name !== 'boolean'
    );

    if (findDuplicates(areaNames)) {
      errorMessage(
        'notification.createGroup.areaDivision.error.areaNameExists'
      );
      return;
    }

    const addedAreas = temporaryAreas
      .map((area, index) => ({
        name: filedsValues[`areaName${index}`],
        isIndoor: filedsValues[`areaType${index}`],
      }))
      .filter(area => !!area.name);

    const createGroupPayload = { ...groupPayload, areas: addedAreas };

    try {
      const response = await createGroupRequest(createGroupPayload);

      handleResponse(response);
    } catch {
      handleServerError();
    }
  };

  return (
    <Wrapper>
      <StepProgress currentStep={currentStep} totalSteps={totalSteps} />
      <Header>
        {intl.formatMessage({
          id: `modal.createGroup.areaSelection.title`,
        })}
      </Header>
      <Description>
        {intl.formatMessage(
          {
            id: `modal.createGroup.areaSelection.description`,
          },
          { br: <br /> }
        )}
      </Description>
      <SwitchWrapper>
        <Switch
          onChange={onChange}
          checked={isAreaSelection}
          data-cy="toggleAreaDivision"
        />
      </SwitchWrapper>

      <Form onFinish={onFinish} form={form}>
        {isAreaSelection && (
          <AreaNameInput
            temporaryAreas={temporaryAreas}
            setTemporaryAreas={setTemporaryAreas}
            next={next}
            back={back}
            form={form}
          />
        )}
        <ButtonRow
          confirmButtonLabel={intl.formatMessage({
            id: 'createGroup.button.done',
          })}
          isFormSubmitting={isFormSubmitting}
          onBack={back}
          htmlType="submit"
        />
      </Form>
    </Wrapper>
  );
};
