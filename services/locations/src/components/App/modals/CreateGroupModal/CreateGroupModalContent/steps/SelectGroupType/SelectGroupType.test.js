import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { act } from 'react-dom/test-utils';
import { SelectGroupType } from './SelectGroupType.react';

const mockNext = jest.fn();
const mockSetGroupType = jest.fn();

describe('SelectGroupType', () => {
  const setup = async () => {
    const renderResult = render(
      <SelectGroupType
        currentStep={1}
        totalSteps={2}
        setGroupType={mockSetGroupType}
        next={mockNext}
      />
    );

    const wrapper = await screen.findByTestId('selectGroupType');
    const selection = await screen.findByTestId('selectGroupTypeSelection');
    const restaurantOption = await screen.findByTestId('restaurant');
    const baseOption = await screen.findByTestId('base');
    const nursingHomeOption = await screen.findByTestId('nursing_home');
    const hotelOption = await screen.findByTestId('hotel');
    const storeOption = await screen.findByTestId('store');

    return {
      renderResult,
      wrapper,
      selection,
      restaurantOption,
      baseOption,
      nursingHomeOption,
      hotelOption,
      storeOption,
    };
  };
  afterEach(() => {
    jest.clearAllMocks();
  });

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });
  test('Selecting an option', async () => {
    const { restaurantOption } = await setup();

    await act(async () => {
      await restaurantOption.click();
    });
    expect(mockSetGroupType).toHaveBeenCalledWith('restaurant');
    expect(mockNext).toHaveBeenCalledTimes(1);
  });
});
