import React from 'react';
import { createGlobalStyle } from 'styled-components';

import { CreateGroupModalContent } from './CreateGroupModalContent';

const GlobalModalStyle = createGlobalStyle`
  .noHeader .ant-modal-title {
    display: none;
  }

  .noHeader .ant-steps {
    display: none;
  }
`;

export const CreateGroupModal = () => {
  return (
    <>
      <GlobalModalStyle />
      <CreateGroupModalContent />
    </>
  );
};
