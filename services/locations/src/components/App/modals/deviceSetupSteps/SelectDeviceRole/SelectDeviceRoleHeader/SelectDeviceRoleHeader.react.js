import React from 'react';

import { useIntl } from 'react-intl';
import { StepHeadline, StepDescription } from '../../CreateDeviceModal.styled';

export const SelectDeviceRoleHeader = () => {
  const intl = useIntl();

  return (
    <>
      <StepHeadline>
        {intl.formatMessage({ id: 'modal.createDevice.selectRole' })}
      </StepHeadline>
      <StepDescription>
        {intl.formatMessage({
          id: 'modal.createDevice.selectRole.description',
        })}
      </StepDescription>
      <StepDescription>
        {intl.formatMessage(
          {
            id: 'modal.createDevice.selectRole.warningText',
          },
          {
            warning: (
              <b>
                {intl.formatMessage({
                  id: 'modal.createDevice.selectRole.warning',
                })}
              </b>
            ),
          }
        )}
      </StepDescription>
    </>
  );
};
