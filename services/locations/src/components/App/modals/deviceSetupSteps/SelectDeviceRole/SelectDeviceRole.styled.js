import styled from 'styled-components';

export const DeviceAuthenticationWrapper = styled.div`
  flex: 1;
  display: flex;
  height: 400px;
  flex-direction: column;
`;
