import React, { useState } from 'react';
import { useQueryClient } from 'react-query';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import { PrimaryButton, SecondaryButton } from 'components/general';

import { SelectDeviceRoleHeader } from './SelectDeviceRoleHeader';
import { Roles } from './Roles';

import { DeviceAuthenticationWrapper } from './SelectDeviceRole.styled';

import {
  StepFooter,
  StepFooterLeft,
  StepFooterRight,
} from '../CreateDeviceModal.styled';

export const SelectDeviceRole = ({ onCancel, onCreate }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const [isLoading, setIsLoading] = useState(false);
  const [isRoleSelected, setIsRoleSelected] = useState(false);

  const onSubmit = ({ role }) => {
    queryClient.invalidateQueries('devices');
    setIsLoading(true);
    onCreate(role).catch(() => setIsLoading(false));
  };

  return (
    <Form
      onFinish={onSubmit}
      onValuesChange={({ role }) => role && setIsRoleSelected(true)}
    >
      <DeviceAuthenticationWrapper>
        <SelectDeviceRoleHeader />
        <Roles />
        <StepFooter>
          <StepFooterLeft>
            <SecondaryButton onClick={onCancel}>
              {intl.formatMessage({
                id: 'generic.cancel',
              })}
            </SecondaryButton>
          </StepFooterLeft>
          <StepFooterRight>
            <PrimaryButton
              htmlType="submit"
              loading={isLoading}
              disabled={!isRoleSelected}
            >
              {intl.formatMessage({
                id: 'modal.createDevice.selectRole.create',
              })}
            </PrimaryButton>
          </StepFooterRight>
        </StepFooter>
      </DeviceAuthenticationWrapper>
    </Form>
  );
};
