export const getRoles = (intl, dailyKey) => {
  const scanner = {
    value: 'scanner',
    name: intl.formatMessage({
      id: 'device.role.scanner',
    }),
    description: intl.formatMessage({
      id: 'modal.createDevice.selectRole.scanner',
    }),
  };

  const employee = {
    value: 'employee',
    name: intl.formatMessage({
      id: 'device.role.employee',
    }),
    description: intl.formatMessage({
      id: 'modal.createDevice.selectRole.employee',
    }),
    tooltip: intl.formatMessage({
      id: 'modal.createDevice.selectRole.tooltip',
    }),
  };

  const manager = {
    value: 'manager',
    name: intl.formatMessage({
      id: 'device.role.manager',
    }),
    description: intl.formatMessage({
      id: 'modal.createDevice.selectRole.manager',
    }),
    tooltip: intl.formatMessage({
      id: 'modal.createDevice.selectRole.tooltip',
    }),
  };

  if (dailyKey) return [scanner, employee, manager];

  return [scanner, manager];
};
