import styled from 'styled-components';

export const DeviceAuthenticationWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;
export const DeviceAuthenticationContent = styled.div`
  flex: 1;
  width: 100%;
  display: flex;
  padding: 32px 0;
  align-items: center;
  justify-content: center;
`;
export const QRCodeWrapper = styled.div`
  width: 210px;
  height: 210px;
  display: flex;
  background: #fff;
  border-radius: 4px;
  align-items: center;
  justify-content: center;
  box-shadow: 0px 2px 4px 0px rgba(143, 143, 143, 0.5);
`;
