import React from 'react';
import {
  CheckIconWrapper,
  Description,
  QRCodeHeader,
  StyledDivider,
  StyledCheckIcon,
  Wrapper,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';

export const QRDownloadCard = ({ title, description, children }) => {
  return (
    <>
      <CheckIconWrapper>
        <StyledCheckIcon />
      </CheckIconWrapper>
      <StyledDivider />
      <Wrapper>
        <QRCodeHeader>{title}</QRCodeHeader>
        <Description>{description}</Description>
        <div>{children}</div>
      </Wrapper>
    </>
  );
};
