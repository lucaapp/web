import React from 'react';
import { PrimaryButton, SecondaryButton } from 'components/general';
import { useIntl } from 'react-intl';
import { ButtonWrapper } from '../../Onboarding.styled';

export const ButtonRow = ({
  confirmButtonLabel,
  htmlType,
  isFormSubmitting = false,
  onBack,
  onConfirm,
}) => {
  const intl = useIntl();

  return (
    <ButtonWrapper multipleButtons>
      <SecondaryButton
        data-cy="previousStep"
        onClick={onBack}
        disabled={isFormSubmitting}
      >
        {intl.formatMessage({
          id: 'authentication.form.button.back',
        })}
      </SecondaryButton>
      <PrimaryButton
        data-cy="nextStep"
        onClick={onConfirm}
        htmlType={htmlType}
        disabled={isFormSubmitting}
      >
        {confirmButtonLabel}
      </PrimaryButton>
    </ButtonWrapper>
  );
};
