import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import { useMobilePhoneValidator } from 'components/hooks/useValidators';
import { getFormattedPhoneNumber } from 'utils/formatters';

import {
  Wrapper,
  Header,
  Description,
  StyledInput,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { useUser } from 'store-context/selectors';
import { StepProgress } from '../StepProgress';
import { ButtonRow } from '../common/ButtonRow';

export const PhoneInput = ({
  phone: currentPhone,
  setPhone,
  back,
  next,
  totalSteps,
  isGroup,
  currentStep,
}) => {
  const intl = useIntl();
  const phoneValidator = useMobilePhoneValidator();

  const { isLoading, error, data: operator } = useUser();

  const onFinish = ({ phone }) => {
    setPhone(getFormattedPhoneNumber(phone));
    next();
  };

  if (error || isLoading) return null;

  return (
    <Wrapper>
      <StepProgress currentStep={currentStep} totalSteps={totalSteps} />
      <Header>
        {intl.formatMessage({
          id: isGroup
            ? 'modal.createGroup.phoneInput.title'
            : 'modal.createLocation.phoneInput.title',
        })}
      </Header>
      <Description>
        {intl.formatMessage({
          id: isGroup
            ? 'modal.createGroup.phoneInput.description'
            : 'modal.createLocation.phoneInput.description',
        })}
      </Description>
      <Form
        onFinish={onFinish}
        initialValues={
          currentPhone ? { phone: currentPhone } : { phone: operator.phone }
        }
      >
        <Form.Item
          data-cy="phone"
          colon={false}
          label={intl.formatMessage({
            id: 'createGroup.phone',
          })}
          name="phone"
          rules={phoneValidator}
        >
          <StyledInput autoFocus />
        </Form.Item>
        <ButtonRow
          onBack={back}
          confirmButtonLabel={intl.formatMessage({
            id: 'authentication.form.button.next',
          })}
          htmlType="submit"
        />
      </Form>
    </Wrapper>
  );
};
