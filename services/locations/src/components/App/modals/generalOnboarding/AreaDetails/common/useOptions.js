import { useIntl } from 'react-intl';

export const useOptions = () => {
  const intl = useIntl();

  const defaultOption = {
    key: 'default',
    value: null,
    label: intl.formatMessage({
      id: 'modal.createGroup.select.defaultOption',
    }),
    dataCy: 'areaDetailsSelectionDefaultOption',
  };

  const roomDimensions = [
    {
      key: 'roomWidth',
      value: 'roomWidth',
      name: 'roomWidth',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.roomSize.width',
      }),
    },
    {
      key: 'roomDepth',
      value: 'roomDepth',
      name: 'roomDepth',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.roomSize.depth',
      }),
    },
    {
      key: 'roomHeight',
      value: 'roomHeight',
      name: 'roomHeight',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.roomSize.height',
      }),
    },
  ];

  const areaTypeOptions = [
    {
      key: 'indoor',
      value: true,
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.areaType.indoor',
      }),
      dataCy: 'areaTypeOptionIndoor',
    },
    {
      key: 'outdoor',
      value: false,
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.areaType.outdoor',
      }),
      dataCy: 'areaTypeOptionOutdoor',
    },
  ];

  const entryPolicyInfoOptions = [
    defaultOption,
    {
      key: '2G',
      value: '2G',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.entryPolicyInfo.2G',
      }),
      dataCy: 'entryPolicyInfoOption2G',
    },
    {
      key: '3G',
      value: '3G',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.entryPolicyInfo.3G',
      }),
      dataCy: 'entryPolicyInfoOption3G',
    },
    {
      key: 'none',
      value: 'none',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.entryPolicyInfo.none',
      }),
      dataCy: 'entryPolicyInfoOptionNone',
    },
  ];

  const medicalProtectionOptions = [
    defaultOption,
    {
      key: 'yes',
      value: 'yes',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.masks.yes',
      }),
      dataCy: 'medicalMasksOptionYes',
    },
    {
      key: 'partial',
      value: 'partial',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.masks.partial',
      }),
      dataCy: 'medicalMasksOptionPartial',
    },
    {
      key: 'no',
      value: 'no',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.masks.no',
      }),
      dataCy: 'medicalMasksOptionNo',
    },
  ];

  const ventilationOptions = [
    defaultOption,
    {
      key: 'electric',
      value: 'electric',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.selectVentilation.electric',
      }),
      dataCy: 'roomVentilationOptionElectric',
    },
    {
      key: 'window',
      value: 'window',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.selectVentilation.window',
      }),
      dataCy: 'roomVentilationOptionWindow',
    },
    {
      key: 'partial',
      value: 'partial',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.selectVentilation.partial',
      }),
      dataCy: 'roomVentilationOptionPartial',
    },
    {
      key: 'no',
      value: 'no',
      label: intl.formatMessage({
        id: 'modal.createGroup.areaDetails.selectVentilation.no',
      }),
      dataCy: 'roomVentilationOptionNo',
    },
  ];

  return {
    roomDimensions,
    areaTypeOptions,
    entryPolicyInfoOptions,
    ventilationOptions,
    medicalProtectionOptions,
  };
};
