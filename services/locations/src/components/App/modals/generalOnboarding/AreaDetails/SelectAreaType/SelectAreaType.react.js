import React from 'react';

import { StyledSelect } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { SelectInput } from '../common/SelectInput';
import { useOptions } from '../common/useOptions';

export const SelectAreaType = ({ areaDetails, setAreaDetails }) => {
  const { areaTypeOptions } = useOptions();

  const handleAreaTypeChange = value => {
    setAreaDetails({
      ...areaDetails,
      isIndoor: value,
    });
  };

  return (
    <StyledSelect>
      <SelectInput
        dataCy="selectAreaType"
        name="areaType"
        handleSelect={handleAreaTypeChange}
        options={areaTypeOptions}
        required
      />
    </StyledSelect>
  );
};
