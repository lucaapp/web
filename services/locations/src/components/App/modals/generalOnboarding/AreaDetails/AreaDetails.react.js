import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';
import {
  Wrapper,
  Header,
  Description,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { SelectAreaType } from './SelectAreaType';
import { ButtonRow } from '../common/ButtonRow';
import { StepProgress } from '../StepProgress';

export const AreaDetails = ({
  areaDetails,
  setAreaDetails,
  totalSteps,
  back,
  next,
  currentStep,
}) => {
  const intl = useIntl();
  return (
    <Wrapper>
      <StepProgress currentStep={currentStep} totalSteps={totalSteps} />
      <Header>
        {intl.formatMessage({ id: 'modal.createGroup.areaDetails.headline' })}
      </Header>
      <Description>
        {intl.formatMessage({
          id: 'modal.createGroup.areaDetails.description',
        })}
      </Description>
      <Form
        onFinish={next}
        initialValues={{
          areaType: [areaDetails.isIndoor],
        }}
      >
        <SelectAreaType
          areaDetails={areaDetails}
          setAreaDetails={setAreaDetails}
        />
        <ButtonRow
          onBack={back}
          confirmButtonLabel={intl.formatMessage({
            id: 'authentication.form.button.next',
          })}
          htmlType="submit"
        />
      </Form>
    </Wrapper>
  );
};
