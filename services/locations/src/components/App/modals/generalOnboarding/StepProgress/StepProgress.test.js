import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { StepProgress } from './StepProgress.react';

const currentStepCount = 1;
const totalStepsCount = 2;
describe('StepProgress', () => {
  const setup = async (currentStep, totalSteps) => {
    const renderResult = render(
      <StepProgress currentStep={currentStep} totalSteps={totalSteps} />
    );

    const stepDisplay = await screen.findByTestId('stepCount');

    return {
      renderResult,
      stepDisplay,
    };
  };

  test('Check if component renders', async () => {
    testAllDefined(await setup(currentStepCount, totalStepsCount));
  });
  test('Component displays the correct step count', async () => {
    const { stepDisplay } = await setup(currentStepCount, totalStepsCount);
    expect(stepDisplay).toHaveTextContent(
      `${currentStepCount} / ${totalStepsCount}`
    );
  });
});
