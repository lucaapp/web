import styled from 'styled-components';
import { Media } from 'utils/media';
import { TimePicker, Input, Divider } from 'antd';
import { CheckIcon } from 'assets/icons';

const font = 'Montserrat-Bold, sans-serif';

export const Wrapper = styled.div`
  ${Media.tablet`
    width: 80vw;
  `}
`;

export const ManualInputInfo = styled.div`
  font-size: 14px;
  font-weight: ${({ highlight }) => (highlight ? 600 : 500)};
  letter-spacing: 0;
  line-height: 20px;
  color: rgba(0, 0, 0, 0.87);
  font-family: ${({ highlight }) =>
      highlight ? 'Montserrat-Bold' : 'Montserrat-Medium'},
    sans-serif;
  margin-bottom: ${({ isLast }) => (isLast ? 8 : 0)}px;
  margin-top: ${({ isLast }) => (isLast ? 24 : 0)}px;
`;

export const InlineLink = styled.div`
  border: none;
  cursor: pointer;
  font-size: 14px;
  font-weight: 600;
  user-select: none;
  letter-spacing: 0;
  color: rgb(0, 0, 0);
  margin-bottom: 16px;
  text-decoration: underline;
  font-family: Montserrat-SemiBold, sans-serif;
`;

export const StepProgressWrapper = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 4px;
`;

export const Header = styled.div`
  color: rgb(0, 0, 0);
  font-family: ${font};
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 16px;
  word-break: normal;
`;

export const ToggleDescription = styled.span`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-right: 140px;
`;

export const StyledSwitchContainer = styled.div`
  margin-top: 8px;
  display: flex;
`;

export const CheckIconWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 16px;
`;

export const StyledCheckIcon = styled(CheckIcon)`
  margin-bottom: 40px;
  width: 120px;
  height: 120px;
`;

export const QRCodeHeader = styled.div`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
`;

export const QRCodeDownloadWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 40px 0;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: ${({ multipleButtons }) =>
    multipleButtons ? 'space-between' : 'flex-end'};
  margin-top: 40px;

  ${Media.mobile`
    margin-top: 40px;
    flex-direction: column;  
  `}
`;

export const StyledTimePicker = styled(TimePicker)`
  width: 200px;
`;

export const Expand = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  transition: all 1000ms;
  margin-top: ${({ open }) => (open ? '32px' : '')};
  overflow: hidden;
`;

export const SwitchWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 24px;
`;

export const GoogleMapSelectorWrapper = styled.div`
  height: 350px;
`;

export const StyledInput = styled(Input)`
  border-radius: 0;
  border: 0.5px solid rgb(0, 0, 0);
  height: 40px;

  :focus,
  :hover,
  :active {
    border-radius: 0;
    border: 0.5px solid rgb(0, 0, 0);
    height: 40px;
  }
`;

export const StyledSelect = styled.div`
  width: 100%;

  .ant-select {
    .ant-select-selector {
      border-radius: 0;
      border: 0.5px solid rgb(0, 0, 0) !important;
      height: 40px;
      display: flex;
      align-items: center;

      :focus,
      :hover,
      :active {
        border-radius: 0;
        border: 0.5px solid rgb(0, 0, 0);
        height: 40px;
        display: flex;
        align-items: center;
      }
    }
  }

  .ant-select-arrow {
    margin-right: 8px;
  }
`;

export const StyledDivider = styled(Divider)`
  border: 1px solid #d8d8d8;
  width: 814px;
  margin: 18px 0 18px -32px;
`;
