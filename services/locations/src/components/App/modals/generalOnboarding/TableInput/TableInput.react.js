import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';
import { Switch } from 'components/general';
import {
  useNotifications,
  QUERY_KEYS,
  useTableNumberValidator,
} from 'components/hooks';

import { MIN_TABLE_NUMBER, MAX_TABLE_NUMBER } from 'constants/tableNumber';
import { useQueryClient } from 'react-query';
import { createLocation as createLocationRequest } from 'network/api';

import { useModalContext } from 'components/general/Modal/useModalContext';
import { inputOnlyNumbersFilter } from 'utils/inputFilter';
import {
  Wrapper,
  Header,
  Description,
  SwitchWrapper,
} from '../Onboarding.styled';
import { StyledInputNumber } from './TableInput.styled';
import { StepProgress } from '../StepProgress';
import { ButtonRow } from '../common/ButtonRow';

export const TableInput = ({
  tableCount: currentTableCount,
  setTableCount,
  totalSteps,
  back,
  next,
  setLocation,
  setLocationId,
  locationPayload,
  isLocationModal,
  currentStep,
}) => {
  const intl = useIntl();
  const { successMessage, errorMessage } = useNotifications();
  const queryClient = useQueryClient();
  const { closeModal } = useModalContext();

  const [hasTables, setHasTables] = useState(false);
  const [isFormSubmitting, setIsFormSubmitting] = useState(false);
  const tableNumberValidator = useTableNumberValidator(true);

  const onFinishNext = ({ tableCount }) => {
    setTableCount(tableCount);
    next();
  };

  const handleServerError = () => {
    errorMessage('notification.createLocation.error');
    setIsFormSubmitting(false);
    closeModal();
  };

  const handleResponse = response => {
    if (!response) {
      handleServerError();
      return;
    }

    successMessage('notification.createLocation.success');
    setLocationId?.(response.uuid);
    setLocation(response);

    queryClient.invalidateQueries(QUERY_KEYS.GROUPS);
    setIsFormSubmitting(false);
    next();
  };

  const onFinishCreateLocation = ({ tableCount }) => {
    setIsFormSubmitting(true);

    const createLocationPayload = { ...locationPayload, tableCount };
    createLocationRequest(createLocationPayload)
      .then(response => {
        handleResponse(response);
      })
      .catch(() => {
        handleServerError();
      });
  };

  return (
    <Wrapper>
      <StepProgress currentStep={currentStep} totalSteps={totalSteps} />
      <Header>
        {intl.formatMessage({
          id: 'modal.createGroup.tableInput.title',
        })}
      </Header>
      <Description>
        {intl.formatMessage({
          id: 'modal.createGroup.tableInput.description',
        })}
      </Description>
      <SwitchWrapper>
        <Switch
          checked={hasTables}
          onChange={() => setHasTables(!hasTables)}
          data-cy="toggleTableInput"
        />
      </SwitchWrapper>
      <Form
        onFinish={isLocationModal ? onFinishCreateLocation : onFinishNext}
        initialValues={
          currentTableCount ? { tableCount: currentTableCount } : {}
        }
        style={{ marginTop: 40 }}
      >
        {hasTables && (
          <Form.Item
            colon={false}
            label={intl.formatMessage({
              id: 'createGroup.tableAmount',
            })}
            name="tableCount"
            rules={tableNumberValidator}
          >
            <StyledInputNumber
              onKeyDown={inputOnlyNumbersFilter}
              style={{ width: '100%' }}
              min={MIN_TABLE_NUMBER}
              max={MAX_TABLE_NUMBER}
              placeholder={intl.formatMessage({
                id: 'createGroup.tableCount.placeholder',
              })}
            />
          </Form.Item>
        )}
        <ButtonRow
          confirmButtonLabel={intl.formatMessage({
            id: isLocationModal
              ? 'createLocation.button.done'
              : 'authentication.form.button.next',
          })}
          isFormSubmitting={isFormSubmitting}
          onBack={back}
          htmlType="submit"
        />
      </Form>
    </Wrapper>
  );
};
