import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import { PrimaryButton, SecondaryButton } from 'components/general';
import {
  ButtonWrapper,
  Wrapper,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { StepProgress } from 'components/App/modals/generalOnboarding/StepProgress/StepProgress.react';
import { ExpandAddressFlow } from 'components/general/ExpandAddressFlow';
import { AddressHeading } from 'components/App/modals/EditAddressModal/AddressHeading';
import { useUser } from 'store-context/selectors';

export const GooglePlacesInput = ({
  totalSteps,
  enabled,
  setEnabled,
  back,
  next,
  address,
  setAddress,
  coordinates,
  setCoordinates,
  baseLocation,
  isGroup,
  currentStep,
}) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [displayManualAddress, setDisplayManualAddress] = useState(false);
  const [businessAddressEnabled, setBusinessAddressEnabled] = useState(false);
  const [temporaryAddress, setTemporaryAddress] = useState(address);
  const [isError, setIsError] = useState(false);
  const [filled, setFilled] = useState(false);
  const [hasPlace, setHasPlace] = useState(false);
  const [initialAddress] = useState({
    city: '',
    streetName: '',
    streetNr: '',
    zipCode: '',
  });

  const { data: operator } = useUser();

  useEffect(() => {
    if (businessAddressEnabled) {
      form.setFieldsValue({
        city: baseLocation?.city || operator?.businessEntityCity,
        streetName:
          baseLocation?.streetName || operator?.businessEntityStreetName,
        streetNr:
          baseLocation?.streetNr || operator?.businessEntityStreetNumber,
        zipCode: baseLocation?.zipCode || operator?.businessEntityZipCode,
      });
    } else {
      form.setFieldsValue(initialAddress);
    }
  }, [
    baseLocation?.city,
    baseLocation?.streetName,
    baseLocation?.streetNr,
    baseLocation?.zipCode,
    businessAddressEnabled,
    form,
    initialAddress,
    operator?.businessEntityCity,
    operator?.businessEntityStreetName,
    operator?.businessEntityStreetNumber,
    operator?.businessEntityZipCode,
    temporaryAddress,
  ]);

  const handleSwitch = () => {
    if (displayManualAddress) {
      return setBusinessAddressEnabled(!businessAddressEnabled);
    }

    setTemporaryAddress(initialAddress);
    setFilled(false);
    setHasPlace(false);
    return setEnabled(!enabled);
  };

  const onFinish = values => {
    setTemporaryAddress(values);
    setAddress(values);
    next();
  };

  const processContinue = () => {
    if (!enabled && !displayManualAddress) {
      return setDisplayManualAddress(true);
    }
    if (coordinates != null) {
      form.setFieldsValue({ ...coordinates });
    }

    return form.submit();
  };

  return (
    <Wrapper>
      <StepProgress currentStep={currentStep} totalSteps={totalSteps} />
      <AddressHeading
        displayManualAddress={displayManualAddress}
        isGroup={isGroup}
      />

      <ExpandAddressFlow
        handleSwitch={handleSwitch}
        enabled={enabled}
        form={form}
        handleSubmit={onFinish}
        initialValues={temporaryAddress}
        coordinates={coordinates}
        setCoordinates={setCoordinates}
        displayManualAddress={displayManualAddress}
        businessAddressEnabled={businessAddressEnabled}
        isError={isError}
        setIsError={setIsError}
        filled={filled}
        setFilled={setFilled}
        hasPlace={hasPlace}
        setHasPlace={setHasPlace}
        isGroup={isGroup}
      />

      <ButtonWrapper multipleButtons>
        <SecondaryButton onClick={back} data-cy="previousStep">
          {intl.formatMessage({
            id: 'authentication.form.button.back',
          })}
        </SecondaryButton>
        <div>
          <PrimaryButton
            data-cy="nextStep"
            onClick={processContinue}
            disabled={isError || (enabled && !hasPlace)}
          >
            {intl.formatMessage({
              id: 'modal.createGroup.googlePlacesInput.continue',
            })}
          </PrimaryButton>
        </div>
      </ButtonWrapper>
    </Wrapper>
  );
};
