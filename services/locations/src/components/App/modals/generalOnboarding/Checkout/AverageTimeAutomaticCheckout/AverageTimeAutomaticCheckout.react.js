import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';
import moment from 'moment';

import { Switch } from 'components/general';
import { setAverageCheckoutTime } from 'utils/formatters';
import {
  CheckoutSectionTile,
  StyledForm,
  CheckoutSectionDescription,
  StyledSwitchContainer,
  StyledTimePicker,
} from '../Checkout.styled';

export const AverageTimeAutomaticCheckout = ({
  averageCheckinTime: currentAverageCheckinTime,
  automaticCheckInTime,
  setAutomaticCheckInTime,
  checkinTimeForm,
  onFinishCheckinTime,
}) => {
  const intl = useIntl();
  const DEFAULT_CHECKIN_DURATION = moment('01:30', 'HH:mm');

  return (
    <>
      <CheckoutSectionTile>
        {intl.formatMessage({
          id: 'modal.createGroup.automaticCheckout.timeTitle',
        })}
      </CheckoutSectionTile>
      <CheckoutSectionDescription>
        {intl.formatMessage({
          id: 'modal.createGroup.automaticCheckout.timeInfo',
        })}
      </CheckoutSectionDescription>
      <StyledSwitchContainer>
        <Switch
          data-cy="toggleAutomaticCheckinTime"
          checked={automaticCheckInTime}
          onChange={() => {
            setAutomaticCheckInTime(!automaticCheckInTime);
          }}
        />
      </StyledSwitchContainer>

      <StyledForm
        form={checkinTimeForm}
        onFinish={onFinishCheckinTime}
        initialValues={{
          averageCheckinTime:
            currentAverageCheckinTime || DEFAULT_CHECKIN_DURATION,
        }}
        $display={automaticCheckInTime}
      >
        <Form.Item
          colon={false}
          label={intl.formatMessage({
            id: 'createGroup.automaticCheckout.averageCheckinTime',
          })}
          name="averageCheckinTime"
        >
          <StyledTimePicker
            showNow={false}
            format="HH:mm"
            minuteStep={15}
            onSelect={time => {
              setAverageCheckoutTime(time, checkinTimeForm);
            }}
            placeholder={intl.formatMessage({
              id: 'settings.location.checkout.average.placeholder',
            })}
          />
        </Form.Item>
      </StyledForm>
    </>
  );
};
