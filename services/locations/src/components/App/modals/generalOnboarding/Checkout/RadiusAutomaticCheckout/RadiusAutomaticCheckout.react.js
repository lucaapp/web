import React from 'react';
import { useIntl } from 'react-intl';

import { useCheckoutRadiusValidator } from 'components/hooks/useValidators';

import { Switch } from 'components/general';
import { DEFAULT_CHECKOUT_RADIUS } from 'constants/checkout';

import {
  GooglePlacesWrapper,
  GoogleMapSelectMarker,
} from 'components/general/GoogleService';

import {
  CheckoutSectionTile,
  StyledForm,
  StyledInputNumber,
  CheckoutSectionDescription,
  StyledSwitchContainer,
  StyledItem,
  GoogleMapSelectorWrapper,
} from '../Checkout.styled';

export const RadiusAutomaticCheckout = ({
  radius: currentRadius,
  setRadius,
  googleEnabled,
  coordinates,
  setCoordinates,
  automaticCheckout,
  setAutomaticCheckout,
  onFinishRadius,
  radiusForm,
  isGroup,
}) => {
  const intl = useIntl();
  const checkoutRadiusValidator = useCheckoutRadiusValidator();

  const suffix = !isGroup ? 'Area' : '';

  if (!currentRadius) {
    setRadius(DEFAULT_CHECKOUT_RADIUS);
  }

  return (
    <>
      <CheckoutSectionTile>
        {intl.formatMessage({
          id: `modal.createGroup.automaticCheckout.radiusTitle${suffix}`,
        })}
      </CheckoutSectionTile>
      <CheckoutSectionDescription $isError={!googleEnabled}>
        {intl.formatMessage({
          id: googleEnabled
            ? `modal.createGroup.automaticCheckout.radiusInfo${suffix}`
            : 'modal.createGroup.automaticCheckout.radiusInfoError',
        })}
      </CheckoutSectionDescription>
      <StyledSwitchContainer>
        <Switch
          data-cy="toggleAutomaticCheckout"
          disabled={!googleEnabled}
          checked={automaticCheckout}
          onChange={() => {
            setAutomaticCheckout(!automaticCheckout);
          }}
        />
      </StyledSwitchContainer>

      <GooglePlacesWrapper enabled>
        <StyledForm
          onFinish={onFinishRadius}
          form={radiusForm}
          initialValues={{
            radius: currentRadius || DEFAULT_CHECKOUT_RADIUS,
          }}
          $display={automaticCheckout}
        >
          <StyledItem
            colon={false}
            label={intl.formatMessage({
              id: 'createGroup.radiusLabel',
            })}
            name="radius"
            rules={checkoutRadiusValidator}
          >
            <StyledInputNumber onChange={setRadius} autoFocus />
          </StyledItem>
          <GoogleMapSelectorWrapper>
            <GoogleMapSelectMarker
              coordinates={coordinates}
              setCoordinates={setCoordinates}
              circleRadius={currentRadius || DEFAULT_CHECKOUT_RADIUS}
            />
          </GoogleMapSelectorWrapper>
        </StyledForm>
      </GooglePlacesWrapper>
    </>
  );
};
