import { getMinutesFromTimeString } from 'utils/formatters';

export const timeDiffValidator = (averageCheckinTime, errorMessage) => {
  if (
    averageCheckinTime !== null &&
    getMinutesFromTimeString(averageCheckinTime) < 15
  ) {
    return errorMessage('notification.updateAverageCheckinTime.TimeError');
  }

  return null;
};
