import styled from 'styled-components';
import { InputNumber, Form, Divider, TimePicker } from 'antd';

export const StyledInputNumber = styled(InputNumber)`
  width: 100%;
  border: 0.5px solid rgb(0, 0, 0);
  border-radius: 0;

  :focus,
  :hover,
  :active {
    border-radius: 0;
    border: 0.5px solid rgb(0, 0, 0);
  }
`;

export const StyledForm = styled(Form)`
  margin-top: 24px;
  display: ${({ $display }) => ($display ? 'unset' : 'none')};
`;

export const StyledItem = styled(Form.Item)`
  width: 348px;
`;

export const StyledDivider = styled(Divider)`
  border: 1px solid #d8d8d8;
  position: absolute;
  left: 0;
  top: 75px;
`;

export const StyledDividerTime = styled(Divider)`
  border: 1px solid #d8d8d8;
  width: 814px;
  margin: 40px 0 0 -32px;
`;

export const CheckoutSectionTile = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  padding-top: 16px;
`;

export const CheckoutSectionDescription = styled.div`
  color: ${({ $isError }) => ($isError ? 'rgb(241, 103, 4)' : 'rgb(0, 0, 0)')};
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  padding-top: 8px;
  padding-bottom: 24px;
`;

export const StyledSwitchContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export const GoogleMapSelectorWrapper = styled.div`
  height: 160px;
`;

export const StyledTimePicker = styled(TimePicker)`
  width: 348px;
  border: 0.5px solid rgb(0, 0, 0);
  border-radius: 0;

  :focus,
  :hover {
    border-radius: 0;
    border: 0.5px solid rgb(0, 0, 0);
  }
`;
