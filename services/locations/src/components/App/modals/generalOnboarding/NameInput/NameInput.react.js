import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import {
  Wrapper,
  Header,
  StyledInput,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { StepProgress } from 'components/App/modals/generalOnboarding/StepProgress';
import { ButtonRow } from '../common/ButtonRow';

export const NameInput = ({
  initialValues,
  modalArea,
  back,
  onFinish,
  onValueUpdate,
  totalSteps,
  rules,
  name,
  form,
  currentStep,
}) => {
  const intl = useIntl();

  return (
    <Wrapper>
      <StepProgress currentStep={currentStep} totalSteps={totalSteps} />
      <Header>
        {intl.formatMessage({
          id: `modal.${modalArea}.nameInput.title`,
        })}
      </Header>
      <Form
        onFinish={onFinish}
        form={form}
        initialValues={initialValues}
        onValuesChange={onValueUpdate}
      >
        <Form.Item
          data-cy={name}
          colon={false}
          label={intl.formatMessage({
            id: `modal.${modalArea}.nameInput.description`,
          })}
          name={name}
          rules={rules}
        >
          <StyledInput autoFocus />
        </Form.Item>
        <ButtonRow
          onBack={back}
          confirmButtonLabel={intl.formatMessage({
            id: 'authentication.form.button.next',
          })}
          htmlType="submit"
        />
      </Form>
    </Wrapper>
  );
};
