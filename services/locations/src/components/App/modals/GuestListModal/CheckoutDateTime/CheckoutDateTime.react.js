import React from 'react';
import { useIntl } from 'react-intl';
import { getFormattedDate, getFormattedTime } from 'utils/formatters';

export const CheckoutDateTime = ({ trace }) => {
  const intl = useIntl();

  return (
    <div>
      <span data-cy="checkoutDate">{getFormattedDate(trace.checkout)}</span>
      <span> - </span>
      <span data-cy="checkoutTime">{getFormattedTime(trace.checkout)}</span>
      {trace.checkout && (
        <span>{intl.formatMessage({ id: 'modal.guestList.time.clock' })}</span>
      )}
    </div>
  );
};
