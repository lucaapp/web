import React, { useRef } from 'react';
import { Select } from 'antd';

import { useIntl } from 'react-intl';

import {
  StyledContainer,
  StyledOptionTitle,
  StyledOptionContainer,
} from './DurationFilter.styled';

import {
  ALL_OPTION,
  LAST_WEEK_OPTION,
  TODAY_OPTION,
} from '../DurationFilter.helper';

const { Option } = Select;

export function DurationFilter({ duration, setActiveDuration }) {
  const intl = useIntl();
  const selectReference = useRef();

  const options = [
    {
      value: TODAY_OPTION,
      intl: intl.formatMessage({ id: 'duration.today' }),
    },
    {
      value: LAST_WEEK_OPTION,
      intl: intl.formatMessage({ id: 'duration.week' }),
    },
    {
      intl: intl.formatMessage({ id: 'duration.all' }),
      value: ALL_OPTION,
    },
  ];

  const handleChange = value => {
    setActiveDuration(value);
    selectReference.current.blur();
  };

  return (
    <StyledContainer>
      <Select
        showArrow
        value={duration}
        defaultValue={TODAY_OPTION}
        className="filter"
        onChange={handleChange}
        maxTagPlaceholder="..."
        dropdownClassName="filter"
        ref={selectReference}
      >
        {options.map(filterOption => {
          return (
            <Option value={filterOption.value} key={filterOption.value}>
              <StyledOptionContainer>
                <StyledOptionTitle>{filterOption.intl}</StyledOptionTitle>
              </StyledOptionContainer>
            </Option>
          );
        })}
      </Select>
    </StyledContainer>
  );
}
