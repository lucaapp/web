import React from 'react';

import { useIntl } from 'react-intl';

import { KeycardIcon, PenIcon, MobilePhoneIcon } from 'assets/icons';

import { StyledTooltip } from 'components/general';

import {
  DEVICE_TYPE_ANDROID,
  DEVICE_TYPE_IOS,
  DEVICE_TYPE_STATIC,
  DEVICE_TYPE_WEBAPP,
} from 'constants/deviceTypes';
import { StyledIcon, Wrapper } from './DeviceIcon.styled';

export const DeviceIcon = ({ deviceType }) => {
  const intl = useIntl();

  const getAttributesByDeviceType = type => {
    switch (type) {
      case DEVICE_TYPE_IOS:
      case DEVICE_TYPE_ANDROID:
      case DEVICE_TYPE_WEBAPP:
        return {
          icon: (
            <StyledIcon component={MobilePhoneIcon} data-cy="mobilePhoneIcon" />
          ),
          intlId: 'modal.guestList.deviceType.app',
        };
      case DEVICE_TYPE_STATIC:
        return {
          icon: <StyledIcon component={KeycardIcon} />,
          intlId: 'modal.guestList.deviceType.badge',
        };
      default:
        return {
          icon: <StyledIcon component={PenIcon} data-cy="penIcon" />,
          intlId: 'modal.guestList.deviceType.contactForm',
        };
    }
  };

  const device = getAttributesByDeviceType(deviceType);

  return (
    <Wrapper>
      <StyledTooltip
        placement="right"
        title={intl.formatMessage({
          id: device.intlId,
        })}
      >
        {device.icon}
      </StyledTooltip>
    </Wrapper>
  );
};
