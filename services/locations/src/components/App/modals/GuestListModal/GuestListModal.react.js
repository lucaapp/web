import React, { useState } from 'react';

import moment from 'moment';
import { useIntl } from 'react-intl';
import { Spin, notification, Table } from 'antd';
import { useQueryClient } from 'react-query';

// Utils
import { sortTraces } from 'utils/sort';
import { base64ToHex } from '@lucaapp/crypto';

// Api
import { forceCheckoutByOperator } from 'network/api';

// Components
import { useGetTraces, QUERY_KEYS, useNotifications } from 'components/hooks';
import { RefreshIcon } from 'assets/icons';
import { DurationFilter } from './DurationFilter';
import { DeviceIcon } from './DeviceIcon';
import { CheckoutButton } from './CheckoutButton';
import { CheckinDateTime } from './CheckinDateTime';
import { CheckoutDateTime } from './CheckoutDateTime';

import {
  Wrapper,
  DescriptionCount,
  DescriptionPeriod,
  Header,
  Loading,
  GuestListHeading,
  RefreshTime,
  IconStyled,
  CountValue,
  CountWrapper,
  TableWrapper,
  DurationWrapper,
} from './GuestListModal.styled';

import { ALL_OPTION, TODAY_OPTION } from './DurationFilter.helper';

export const GuestListModal = ({ location }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const { errorMessage } = useNotifications();

  const [duration, setActiveDuration] = useState(TODAY_OPTION);
  const [lastRefresh, setLastRefresh] = useState(moment());

  const { isLoading, error, data: traces } = useGetTraces(
    location.uuid,
    duration !== ALL_OPTION ? duration : null
  );

  const renderCheckoutError = () => errorMessage('notification.checkOut.error');

  const onCheckoutSingleGuest = traceId => {
    forceCheckoutByOperator(traceId)
      .then(response => {
        if (response.status === 204) {
          queryClient.invalidateQueries(`current/${location.scannerId}`);
          queryClient.invalidateQueries(`traces/${location.uuid}/${duration}`);
          notification.success({
            message: intl.formatMessage({
              id: 'notification.checkOut.success',
            }),
            className: 'successCheckout',
          });
        } else {
          renderCheckoutError();
        }
      })
      .catch(() => renderCheckoutError());
  };

  if (isLoading)
    return (
      <Loading>
        <Spin size="large" />
      </Loading>
    );
  if (error) return null;

  const columns = [
    {
      title: intl.formatMessage({ id: 'modal.guestList.type' }),
      key: 'deviceType',
      render: function renderDeviceType(trace) {
        return (
          <DeviceIcon data-cy="deviceIcon" deviceType={trace.deviceType} />
        );
      },
    },
    {
      title: intl.formatMessage({ id: 'modal.guestList.checkinDate' }),
      key: 'checkin',
      render: function renderCheckinDateTime(trace) {
        return <CheckinDateTime trace={trace} />;
      },
    },
    {
      title: intl.formatMessage({ id: 'modal.guestList.checkoutDate' }),
      key: 'checkout',
      render: function renderCheckoutDateTime(trace) {
        return <CheckoutDateTime trace={trace} />;
      },
    },
    {
      title: intl.formatMessage({ id: 'modal.guestList.guest' }),
      key: 'guest',
      render: function renderCheckin(trace) {
        return (
          <span data-cy="traceId">
            {base64ToHex(trace.traceId).slice(0, 7)}
          </span>
        );
      },
    },
    {
      title: intl.formatMessage({
        id: 'group.view.overview.checkout',
      }),
      key: 'checkout',
      render: function renderCheckout(trace) {
        return (
          <CheckoutButton
            trace={trace}
            onCheckoutSingleGuest={onCheckoutSingleGuest}
          />
        );
      },
    },
  ];

  const refresh = () => {
    setLastRefresh(moment());
    queryClient.invalidateQueries([QUERY_KEYS.TRACES, location.uuid]);
  };

  return (
    <Wrapper>
      <Header>
        <GuestListHeading>
          {intl.formatMessage({
            id: 'modal.guestList.title',
          })}
        </GuestListHeading>
        <RefreshTime data-cy="refreshTime">
          {`${intl.formatMessage({
            id: 'modal.tableAllocation.lastRefresh',
          })}: ${moment(lastRefresh).format('DD.MM.YYYY - HH:mm:ss')}`}
          {intl.formatMessage({ id: 'modal.guestList.time.clock' })}
        </RefreshTime>
        <IconStyled
          data-cy="refreshIcon"
          onClick={refresh}
          component={RefreshIcon}
        />
      </Header>

      <CountWrapper>
        <DescriptionCount>
          {intl.formatMessage({ id: 'modal.guestList.totalCount' })}
        </DescriptionCount>
        <CountValue data-cy="totalCheckinCount">{traces.length}</CountValue>
      </CountWrapper>

      <DurationWrapper>
        <DescriptionPeriod data-cy="descriptionPeriod">
          {intl.formatMessage({ id: 'modal.guestList.periodSelect' })}
        </DescriptionPeriod>
        <DurationFilter
          duration={duration}
          setActiveDuration={setActiveDuration}
        />
      </DurationWrapper>

      <TableWrapper data-cy="guestListTable">
        <Table
          columns={columns}
          dataSource={sortTraces(traces)}
          pagination={false}
          rowKey={record => record.traceId}
          locale={{
            emptyText: intl.formatMessage({ id: 'guestlist.table.empty' }),
          }}
        />
      </TableWrapper>
    </Wrapper>
  );
};
