import React from 'react';
import { getFormattedDate, getFormattedTime } from 'utils/formatters';

export const CheckinDateTime = ({ trace }) => {
  return (
    <div>
      <span data-cy="checkinDate">{getFormattedDate(trace.checkin)}</span>
      <span> - </span>
      <span data-cy="checkinTime">{getFormattedTime(trace.checkin)}</span>
    </div>
  );
};
