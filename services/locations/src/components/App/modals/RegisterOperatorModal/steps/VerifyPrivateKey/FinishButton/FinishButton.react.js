import React from 'react';
import { useIntl } from 'react-intl';
import { SecondaryButton } from 'components/general';
import { FinishButtonWrapper } from './FinishButton.styled';

export const FinishButton = ({ back }) => {
  const intl = useIntl();

  return (
    <FinishButtonWrapper align="space-between">
      <SecondaryButton onClick={back}>
        {intl.formatMessage({
          id: 'modal.registerOperator.keyTestGenerateKey',
        })}
      </SecondaryButton>
    </FinishButtonWrapper>
  );
};
