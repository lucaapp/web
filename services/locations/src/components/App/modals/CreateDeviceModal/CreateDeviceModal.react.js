import React, { useState, useEffect, useCallback } from 'react';

import { useIntl } from 'react-intl';
import { notification } from 'antd';
import { useQueryClient } from 'react-query';

import { createDevice } from 'network/api';
import { QUERY_KEYS, useChallenge, useNotifications } from 'components/hooks';
import { generatePIN, generateTransferQRCodeData } from 'utils/operatorDevice';

import { Steps } from 'components/general';

import { useModalContext } from 'components/general/Modal/useModalContext';
import {
  SELECT_DEVICE_ROLE_STEP,
  useActivateDeviceSteps,
} from './CreateDeviceModal.helper';

import { SelectDeviceRole } from '../deviceSetupSteps/SelectDeviceRole';

export const CreateDeviceModal = () => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const { isModalVisible, closeModal } = useModalContext();
  const { challengeId, challenge, challengeState } = useChallenge();
  const { errorMessage } = useNotifications();

  const [currentStep, setCurrentStep] = useState(0);
  const [authenticationPIN, setAuthenticationPIN] = useState('');
  const [finishedChallengeId, setFinishedChallengeId] = useState(null);
  const [authenticationQRCode, setAuthenticationQRCode] = useState(null);

  const onCancel = useCallback(() => {
    closeModal();
  }, [closeModal]);

  const onDone = useCallback(() => {
    closeModal();
    setFinishedChallengeId(challengeId);

    queryClient.invalidateQueries(QUERY_KEYS.OPERATOR_DEVICES);
  }, [challengeId, closeModal, queryClient]);

  useEffect(() => {
    if (finishedChallengeId && finishedChallengeId === challengeId) return;

    if (challengeState.authPinRequired) {
      setCurrentStep(2);
      return;
    }

    if (challengeState.done) {
      notification.success({
        message: intl.formatMessage({ id: 'notification.device.success' }),
      });

      onDone();
      return;
    }

    if (challengeState.canceled) {
      onCancel();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [challengeState, intl, onCancel, onDone]);

  const onCreate = useCallback(
    async role => {
      if (!challenge || authenticationQRCode) return;

      try {
        const authenticationEncryptionPIN = generatePIN();
        const device = await createDevice(role);

        setAuthenticationPIN(authenticationEncryptionPIN);

        const authenticationQRCodeData = await generateTransferQRCodeData(
          'AUTHENTICATION_TOKEN',
          device.refreshToken,
          authenticationEncryptionPIN,
          challenge.uuid
        );

        setAuthenticationQRCode(authenticationQRCodeData);
        setCurrentStep(1);
      } catch {
        errorMessage('notification.device.error');
      }
    },
    [authenticationQRCode, challenge, errorMessage]
  );

  const activateDeviceSteps = useActivateDeviceSteps(
    authenticationQRCode,
    authenticationPIN
  );

  const steps = [
    {
      id: SELECT_DEVICE_ROLE_STEP,
      content: <SelectDeviceRole onCreate={onCreate} onCancel={onCancel} />,
    },
    ...activateDeviceSteps,
  ];

  useEffect(() => {
    if (!isModalVisible) {
      setCurrentStep(0);
      setAuthenticationPIN('');
      setAuthenticationQRCode(null);
    }
  }, [isModalVisible]);

  if (!isModalVisible) {
    return null;
  }

  return <Steps steps={steps} currentStep={currentStep} />;
};
