import React from 'react';
import { DeviceAuthenticationPIN } from '../deviceSetupSteps/DeviceAuthenticationPIN';
import { DeviceAuthenticationQRCode } from '../deviceSetupSteps/DeviceAuthenticationQRCode';

export const SELECT_DEVICE_ROLE_STEP = 'SELECT_DEVICE_ROLE_STEP';
export const DEVICE_AUTHENTICATION_QR_CODE_STEP =
  'DEVICE_AUTHENTICATION_QR_CODE_STEP';
export const DEVICE_AUTHENTICATION_ENCRYPTION_PIN_STEP =
  'DEVICE_AUTHENTICATION_ENCRYPTION_PIN_STEP';

const NUMBER_OF_DEVICE_CREATION_STEPS = 2;

export const useActivateDeviceSteps = (
  authenticationQRCode,
  authenticationPIN
) => [
  {
    id: DEVICE_AUTHENTICATION_QR_CODE_STEP,
    content: (
      <DeviceAuthenticationQRCode
        currentStep={1}
        authenticationQRCode={authenticationQRCode}
        numberOfSteps={NUMBER_OF_DEVICE_CREATION_STEPS}
      />
    ),
  },
  {
    id: DEVICE_AUTHENTICATION_ENCRYPTION_PIN_STEP,
    content: (
      <DeviceAuthenticationPIN
        currentStep={2}
        authenticationPIN={authenticationPIN}
        numberOfSteps={NUMBER_OF_DEVICE_CREATION_STEPS}
      />
    ),
  },
];
