import styled from 'styled-components';

export const InnerWrapper = styled.div`
  padding: 0 8px;
`;

export const Headline = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  padding: 16px 0 23px 0;
`;
