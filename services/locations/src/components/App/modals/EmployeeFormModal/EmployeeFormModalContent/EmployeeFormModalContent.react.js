import React from 'react';
import { useIntl } from 'react-intl';
import { SubmitButton } from 'components/general/Form/AppForm/SubmitButton';
import { AppForm, FormInputType } from 'components/general/Form';
import { Row, Col } from 'antd';
import { useEmployeeNameSchema } from 'components/general/Form/hooks';

import { useQueryClient } from 'react-query';
import { createEmployee, updateEmployee } from 'network/api';
import { QUERY_KEYS, useNotifications } from 'components/hooks';

import { Headline, InnerWrapper } from './EmployeeFormModalContent.styled';

export const EmployeeFormModalContent = ({
  group,
  employee,
  isEdit,
  closeModal,
}) => {
  const intl = useIntl();
  const { errorMessage, successMessage } = useNotifications();
  const employeeNameSchema = useEmployeeNameSchema();
  const queryClient = useQueryClient();

  const initialValues = {
    firstName: employee?.firstName ?? '',
    lastName: employee?.lastName ?? '',
  };

  const handleCreateEmployee = ({ firstName, lastName }) =>
    createEmployee({
      locationGroupId: group.groupId,
      firstName,
      lastName,
    })
      .then(response => {
        if (response.status !== 204) {
          errorMessage('notification.createEmployee.error');
          return;
        }
        successMessage('notification.createEmployee.success');
        closeModal();
        queryClient.invalidateQueries(QUERY_KEYS.EMPLOYEES);
      })
      .catch(() => errorMessage('notification.createEmployee.error'));

  const handleUpdateEmployee = ({ firstName, lastName }) => {
    if (firstName === employee.firstName && lastName === employee.lastName) {
      return;
    }
    updateEmployee({
      employeeId: employee.uuid,
      data: { firstName, lastName },
    })
      .then(response => {
        if (response.status !== 204) {
          errorMessage('notification.editEmployee.error');
          return;
        }
        successMessage('notification.editEmployee.success');
        closeModal();
        queryClient.invalidateQueries(QUERY_KEYS.EMPLOYEES);
      })
      .catch(() => errorMessage('notification.editEmployee.error'));
  };

  return (
    <InnerWrapper>
      <Headline>
        {intl.formatMessage({
          id: isEdit
            ? 'groupProfile.employees.employeeFormModal.title.edit'
            : 'groupProfile.employees.employeeFormModal.title.create',
        })}
      </Headline>

      <AppForm
        onSubmit={isEdit ? handleUpdateEmployee : handleCreateEmployee}
        initialValues={initialValues}
        validationSchema={employeeNameSchema}
        enableReinitialize
      >
        <Row>
          <Col span={12}>
            <FormInputType.Input
              name="firstName"
              labelId="form.label.firstName"
              paddingRight="16px"
              autoFocus
            />
          </Col>
          <Col span={12}>
            <FormInputType.Input
              name="lastName"
              labelId="form.label.lastName"
            />
          </Col>
        </Row>

        <SubmitButton
          title={intl.formatMessage({ id: 'button.save' })}
          dataCy="submitButton"
        />
      </AppForm>
    </InnerWrapper>
  );
};
