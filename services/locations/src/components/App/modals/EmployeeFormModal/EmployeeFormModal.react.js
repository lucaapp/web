import React from 'react';
import { Modal } from 'antd';
import { EmployeeFormModalContent } from './EmployeeFormModalContent';

export const EmployeeFormModal = ({
  group,
  selectedEmployee,
  isVisible,
  closeModal,
  isEdit,
}) => {
  return (
    <Modal
      visible={isVisible}
      destroyOnClose
      footer={null}
      centered
      width={542}
      onCancel={closeModal}
      className="employeeFormModal"
    >
      <EmployeeFormModalContent
        group={group}
        employee={selectedEmployee}
        closeModal={closeModal}
        isEdit={isEdit}
      />
    </Modal>
  );
};
