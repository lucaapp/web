import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';

import { getRequiredRule } from 'utils/antFormRules';

import { SecondaryButton } from 'components/general';

import {
  Wrapper,
  Title,
  Description,
  ButtonWrapper,
  StyledPasswordInput,
} from './PasswordModalContent.styled';

export const PasswordModalContent = ({ callback }) => {
  const intl = useIntl();

  const onFinish = ({ password }) => callback(password);

  return (
    <Wrapper>
      <Title>{intl.formatMessage({ id: 'passwordModal.title' })}</Title>
      <Description>
        {intl.formatMessage({ id: 'passwordModal.description' })}
      </Description>
      <Form onFinish={onFinish}>
        <Form.Item
          colon={false}
          label={intl.formatMessage({
            id: 'registration.form.password',
          })}
          name="password"
          rules={[getRequiredRule(intl, 'password')]}
        >
          <StyledPasswordInput
            data-cy="passwordModal-passwordInputField"
            autoComplete="current-password"
            iconRender={visible =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
          />
        </Form.Item>
        <ButtonWrapper>
          <SecondaryButton htmlType="submit" data-cy="proceedButton">
            {intl.formatMessage({ id: 'passwordModal.button' })}
          </SecondaryButton>
        </ButtonWrapper>
      </Form>
    </Wrapper>
  );
};
