import React from 'react';
import { createGlobalStyle } from 'styled-components';
import { MediumModal } from 'components/general/Modal/Modal.styled';

// Constants
import { zIndex } from 'constants/layout';

import { PasswordModalContent } from './PasswordModalContent';

export const PasswordModal = ({ callback, onClose }) => {
  const GlobalModalStyle = createGlobalStyle`
  .noHeader .ant-modal-title {
    display: none;
  }

  .noHeader .ant-steps {
    display: none;
  }
`;

  return (
    <>
      <GlobalModalStyle />
      <MediumModal
        className="noHeader"
        visible
        zIndex={zIndex.modalArea}
        onCancel={onClose}
        centered
        footer={null}
      >
        <PasswordModalContent callback={callback} />
      </MediumModal>
    </>
  );
};
