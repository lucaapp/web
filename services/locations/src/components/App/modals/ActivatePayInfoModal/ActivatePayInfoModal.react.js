import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { PrimaryButton } from 'components/general';
import { patchOperator } from 'network/payment';
import { useGetDisplayPaymentModal } from 'components/hooks/useQueries';
import { StyledModal, Text, Title } from './ActivatePayInfoModal.styled';

export const ActivatePayInfoModal = ({ operator }) => {
  const intl = useIntl();
  const [visible, setVisible] = useState(true);

  const {
    isLoading,
    error,
    data: displayPaymentModal,
  } = useGetDisplayPaymentModal(operator.operatorId, {
    enabled: !!operator,
  });

  if (!operator || isLoading || error) return null;

  const onClose = async () => {
    // patch pay operator
    await patchOperator(operator.operatorId, {
      hasSeenPaymentInfoModal: true,
    });
    setVisible(false);
  };

  return (
    <StyledModal
      title={intl.formatMessage({ id: 'modal.completePayActivation.title' })}
      visible={visible && displayPaymentModal}
      onCancel={onClose}
      centered
      footer={
        <PrimaryButton onClick={onClose}>
          {intl.formatMessage({ id: 'modal.completePayActivation.close' })}
        </PrimaryButton>
      }
    >
      {intl.formatMessage(
        { id: 'modal.completePayActivation.content' },
        {
          // eslint-disable-next-line react/display-name
          text: (...chunks) => <Text>{chunks}</Text>,
          // eslint-disable-next-line react/display-name
          title: (...chunks) => <Title>{chunks}</Title>,
        }
      )}
    </StyledModal>
  );
};
