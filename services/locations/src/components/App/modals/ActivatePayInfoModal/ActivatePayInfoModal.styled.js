import styled from 'styled-components';
import { Modal } from 'antd';

export const StyledModal = styled(Modal)`
  max-width: 542px;

  .ant-modal-footer {
    border: none !important;
  }

  .ant-modal-title {
    color: rgb(0, 0, 0);
    font-size: 16px;
    font-weight: bold;
    height: 24px;
    letter-spacing: 0;
    line-height: 24px;
  }
`;

export const Text = styled.p`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
  padding-bottom: 16px;
`;

export const Title = styled.h2`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 20px;
  padding: 8px 0;
`;
