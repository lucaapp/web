import React, { useState } from 'react';
import { useIntl } from 'react-intl';

import { CONSECUTIVE_NUMBERING, CUSTOM_NUMBERING } from 'constants/tableNumber';

import { TableNumberInput } from './TableNumberInput';
import { NumberingSelection } from './NumberingSelection';
import { TableConfiguration } from './TableConfiguration';
import { Description, Title } from './EditTablesModalContent.styled';

export const EditTablesModalContent = ({
  locationId,
  closeModal,
  tableCount: count,
  numberedTables,
}) => {
  const intl = useIntl();
  const [isEditable, setIsEditable] = useState(false);
  const [radioValue, setRadioValue] = useState(
    numberedTables.some(table => table.key !== table.name)
      ? CUSTOM_NUMBERING
      : CONSECUTIVE_NUMBERING
  );

  return (
    <>
      <Title>{intl.formatMessage({ id: 'editTables.title' })}</Title>
      <Description>
        {intl.formatMessage({ id: 'editTables.description' })}
      </Description>
      <TableNumberInput
        tableCount={count}
        isEditable={isEditable}
        setIsEditable={setIsEditable}
        locationId={locationId}
      />
      <NumberingSelection
        radioValue={radioValue}
        setRadioValue={setRadioValue}
      />
      <TableConfiguration
        tableCount={count}
        closeModal={closeModal}
        radioValue={radioValue}
        locationId={locationId}
        isNumberInputActive={isEditable}
      />
    </>
  );
};
