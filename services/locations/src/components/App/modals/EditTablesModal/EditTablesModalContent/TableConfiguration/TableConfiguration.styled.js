import styled from 'styled-components';
import { Form, Input } from 'antd';
import { Media } from 'utils/media';

export const TableNamesWrapper = styled.div`
  background: rgb(245, 245, 244);
  border-radius: 4px;
  display: flex;
  flex-wrap: wrap;
  overflow: auto;
  padding: 24px 48px 32px 24px;
  column-gap: 40px;
  row-gap: 16px;
  min-height: 30px;
  max-height: 232px;
`;
export const Label = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  height: 20px;
  letter-spacing: 0;
  line-height: 20px;
  margin-right: 16px;
`;

export const FormItemWrapper = styled.div`
  display: flex;
  align-items: center;
  height: 30px;
`;

export const StyledFormItem = styled(Form.Item)`
  margin-bottom: 0;
`;
export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: ${({ multipleButtons }) =>
    multipleButtons ? 'space-between' : 'flex-end'};
  margin-top: 24px;

  ${Media.mobile`
    margin-top: 40px;
    flex-direction: column;
  `}
`;

export const StyledInput = styled(Input)`
  width: 64px;

  .ant-input {
    height: 26px;

    :hover,
    :focus,
    :active {
      height: 26px;
    }
  }
`;
