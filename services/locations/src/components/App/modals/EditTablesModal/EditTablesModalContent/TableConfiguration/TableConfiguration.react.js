import React, { useEffect, useState } from 'react';
import { useQueryClient } from 'react-query';

import { useIntl } from 'react-intl';
import { Form } from 'antd';

import {
  QUERY_KEYS,
  useGetLocationTables,
  useNotifications,
} from 'components/hooks';
import { PrimaryButton, SecondaryButton } from 'components/general';

import { CUSTOM_NUMBERING } from 'constants/tableNumber';
import { updateLocationTable } from 'network/api';
import {
  ButtonWrapper,
  TableNamesWrapper,
  Label,
  FormItemWrapper,
  StyledFormItem,
  StyledInput,
} from './TableConfiguration.styled';

import {
  getCurrentValues,
  getLocationTablesToUpdate,
} from './TableConfiguration.helper';

export const TableConfiguration = ({
  tableCount,
  closeModal,
  radioValue,
  locationId,
  isNumberInputActive,
}) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const { errorMessage } = useNotifications();
  const queryClient = useQueryClient();

  const [saveDisabled, setSaveDisabled] = useState(false);
  const [isCustomNumbering, setIsCustomNumbering] = useState(null);

  const { error, data: locationTables, isLoading } = useGetLocationTables(
    locationId
  );

  const onSubmit = async values => {
    const updatePromises = [];
    getLocationTablesToUpdate(locationTables, values);
    if (isCustomNumbering) {
      const tablesToUpdate = getLocationTablesToUpdate(locationTables, values);
      tablesToUpdate.forEach(table => {
        updatePromises.push(
          updateLocationTable(locationId, table.internalNumber, {
            customName: `${table.customName}`,
          })
        );
      });
    } else {
      locationTables.forEach(table => {
        if (table.customName) {
          updatePromises.push(
            updateLocationTable(locationId, table.internalNumber, {
              customName: null,
            })
          );
        }
      });
    }

    try {
      await Promise.all(updatePromises);
      queryClient.invalidateQueries([QUERY_KEYS.LOCATION_TABLES, locationId]);
      closeModal();
    } catch {
      errorMessage('notification.updateLocationTables.error');
    }
  };

  const initialValues = getCurrentValues(locationTables, isCustomNumbering);

  useEffect(() => {
    if (radioValue === CUSTOM_NUMBERING) {
      const newValues = getCurrentValues(locationTables, true);
      form.setFieldsValue(newValues);
      setSaveDisabled(true);
      return setIsCustomNumbering(true);
    }
    const consecutiveValues = getCurrentValues(locationTables, false);
    form.setFieldsValue(consecutiveValues);
    setSaveDisabled(false);
    return setIsCustomNumbering(false);
  }, [radioValue, locationTables, form]);

  const checkDisabled = () => {
    const fieldsValues = form.getFieldsValue();
    const hasIncompleteFields = Object.keys(fieldsValues).some(
      field => fieldsValues[field].length === 0
    );
    return setSaveDisabled(hasIncompleteFields);
  };

  if (error || isLoading) return null;

  return (
    <Form
      form={form}
      onFinish={onSubmit}
      initialValues={initialValues}
      onFieldsChange={checkDisabled}
    >
      {tableCount > 0 && (
        <TableNamesWrapper>
          {locationTables.map(table => (
            <FormItemWrapper key={table.internalNumber} data-cy="tableItem">
              <Label>{intl.formatMessage({ id: 'table' })}</Label>
              <StyledFormItem name={`table-${table.internalNumber}`}>
                <StyledInput
                  disabled={!isCustomNumbering}
                  maxLength={100}
                  data-cy={`table-${table.internalNumber}`}
                />
              </StyledFormItem>
            </FormItemWrapper>
          ))}
        </TableNamesWrapper>
      )}
      <ButtonWrapper multipleButtons>
        <SecondaryButton onClick={closeModal} data-cy="cancelButton">
          {intl.formatMessage({
            id: 'generic.cancel',
          })}
        </SecondaryButton>
        <PrimaryButton
          htmlType="submit"
          disabled={saveDisabled || isNumberInputActive}
          data-cy="saveTableConfigurationButton"
        >
          {intl.formatMessage({
            id: 'location.save',
          })}
        </PrimaryButton>
      </ButtonWrapper>
    </Form>
  );
};
