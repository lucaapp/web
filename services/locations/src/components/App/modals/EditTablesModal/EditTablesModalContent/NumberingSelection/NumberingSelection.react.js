import React from 'react';
import { useIntl } from 'react-intl';
import { Radio, Space } from 'antd';

import { CONSECUTIVE_NUMBERING, CUSTOM_NUMBERING } from 'constants/tableNumber';

import { Description, Title } from '../EditTablesModalContent.styled';
import { StyledRadioGroup, StyledDivider } from './NumberingSelection.styled';

export const NumberingSelection = ({ setRadioValue, radioValue }) => {
  const intl = useIntl();
  const handleChangeNumbering = values => {
    setRadioValue(values.target.value);
  };

  return (
    <>
      <>
        <StyledDivider />
        <Title>{intl.formatMessage({ id: 'editTables.chooseName' })}</Title>
        <Description>
          {intl.formatMessage({ id: 'editTables.numberingDescription' })}
        </Description>
      </>
      <StyledRadioGroup
        onChange={handleChangeNumbering}
        name="numbering"
        defaultValue={radioValue}
      >
        <Space direction="vertical">
          <Radio
            value={CONSECUTIVE_NUMBERING}
            defaultChecked
            data-cy="consecutiveNumberingRadio"
          >
            {intl.formatMessage({ id: 'editTables.optionConsecutive' })}
          </Radio>
          <Radio value={CUSTOM_NUMBERING} data-cy="customNumberingRadio">
            {intl.formatMessage({ id: 'editTables.optionOwnNaming' })}
          </Radio>
        </Space>
      </StyledRadioGroup>
    </>
  );
};
