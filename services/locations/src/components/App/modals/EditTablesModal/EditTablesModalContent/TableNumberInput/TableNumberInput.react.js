import React, { useCallback } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';
import { useQueryClient } from 'react-query';
import { createLocationTable, deleteLocationTable } from 'network/api';
import { useTableNumberValidator } from 'components/hooks/useValidators';
import { PrimaryButton } from 'components/general';
import { MIN_TABLE_NUMBER, MAX_TABLE_NUMBER } from 'constants/tableNumber';
import { inputOnlyNumbersFilter } from 'utils/inputFilter';
import {
  QUERY_KEYS,
  useGetLocationTables,
  useNotifications,
} from 'components/hooks';
import {
  StyledInputNumber,
  ButtonWrapper,
  Wrapper,
  Description,
} from './TableNumberInput.styled';

export const TableNumberInput = ({
  isEditable,
  setIsEditable,
  tableCount,
  locationId,
}) => {
  const intl = useIntl();
  const validateNumberOfTables = useTableNumberValidator(true);
  const queryClient = useQueryClient();
  const { errorMessage } = useNotifications();

  const { error, data: locationTables, isLoading } = useGetLocationTables(
    locationId
  );

  const deleteLocationTables = useCallback(
    async (numberOfTablesToKeep = null) => {
      const promises = [];
      if (!numberOfTablesToKeep) {
        locationTables.map(table =>
          promises.push(deleteLocationTable(locationId, table.internalNumber))
        );
      } else {
        locationTables.map(table =>
          table.internalNumber > numberOfTablesToKeep
            ? promises.push(
                deleteLocationTable(locationId, table.internalNumber)
              )
            : null
        );
      }
      await Promise.all(promises);
      queryClient.invalidateQueries([QUERY_KEYS.LOCATION_TABLES, locationId]);
    },
    [locationId, queryClient, locationTables]
  );

  const createLocationTables = useCallback(
    async (numberOfTablesToCreate = null) => {
      if (numberOfTablesToCreate <= 0) return;

      const tables = Array.from(Array(numberOfTablesToCreate).keys());
      const apiTables = [];

      tables.forEach(() => {
        apiTables.push({ customName: '' });
      });

      try {
        await createLocationTable(locationId, { tables: apiTables });
        queryClient.invalidateQueries([QUERY_KEYS.LOCATION_TABLES, locationId]);
      } catch {
        errorMessage('notification.updateLocationTables.error');
      }
    },
    [queryClient, locationId, errorMessage]
  );

  const changeNumberOfTables = values => {
    const { nrOfTables } = values;

    if (!nrOfTables) return;

    if (nrOfTables > tableCount) {
      const tablesToCreate = nrOfTables - tableCount;
      createLocationTables(tablesToCreate);
    } else {
      const numberOfTablesToKeep = nrOfTables;
      deleteLocationTables(numberOfTablesToKeep);
    }
  };

  const onSubmit = values => {
    changeNumberOfTables(values);
    setIsEditable(false);
  };

  if (isLoading || error) return null;

  return (
    <>
      {!isEditable ? (
        <Wrapper>
          <Description data-cy="tableCount">{`${tableCount}`}</Description>
          <ButtonWrapper>
            <PrimaryButton
              data-cy="editTablesNumberButton"
              onClick={() => {
                setIsEditable(true);
              }}
            >
              {intl.formatMessage({ id: 'location.edit' })}
            </PrimaryButton>
          </ButtonWrapper>
        </Wrapper>
      ) : (
        <Form onFinish={onSubmit} initialValues={{ nrOfTables: tableCount }}>
          <Wrapper>
            <Form.Item name="nrOfTables" rules={validateNumberOfTables}>
              <StyledInputNumber
                onKeyDown={inputOnlyNumbersFilter}
                type="number"
                data-cy="tableNumberInput"
                min={MIN_TABLE_NUMBER}
                max={MAX_TABLE_NUMBER}
              />
            </Form.Item>
            <ButtonWrapper>
              <PrimaryButton htmlType="submit" data-cy="saveTablesNumberButton">
                {intl.formatMessage({
                  id: 'location.save',
                })}
              </PrimaryButton>
            </ButtonWrapper>
          </Wrapper>
        </Form>
      )}
    </>
  );
};
