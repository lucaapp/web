import styled from 'styled-components';
import { InputNumber } from 'antd';
import { Media } from 'utils/media';

export const StyledInputNumber = styled(InputNumber)`
  width: 99%;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: ${({ multipleButtons }) =>
    multipleButtons ? 'space-between' : 'flex-end'};
  margin-top: 24px;

  ${Media.mobile`
    margin-top: 40px;
    flex-direction: column;
  `}
`;

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 16px;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
  margin-bottom: 24px;
`;
