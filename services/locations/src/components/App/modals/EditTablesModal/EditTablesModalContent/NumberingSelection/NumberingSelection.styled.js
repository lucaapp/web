import styled from 'styled-components';
import { Divider, Radio } from 'antd';

export const StyledDivider = styled(Divider)`
  border: 1px solid rgb(224 224 224);
  height: 1px;
  width: 760px;
  margin: 40px 0 40px -32px;
`;
export const StyledRadioGroup = styled(Radio.Group)`
  margin-top: 26px;
  margin-bottom: 20px;

  .ant-radio-inner::after {
    background-color: rgb(0, 52, 255);
  }
`;
