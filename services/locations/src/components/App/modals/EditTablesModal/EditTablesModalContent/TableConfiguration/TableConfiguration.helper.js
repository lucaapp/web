export const getCurrentValues = (locationTables, isCustomNumbering) => {
  const initialValuesList = isCustomNumbering
    ? locationTables.map(table => ({
        [`table-${table.internalNumber}`]: table.customName
          ? table.customName
          : '',
      }))
    : locationTables.map(table => ({
        [`table-${table.internalNumber}`]: table.internalNumber,
      }));

  return Object.assign({}, ...initialValuesList);
};

const getTablesByListKey = locationTables =>
  locationTables.map(table => ({
    [`table-${table.internalNumber}`]: table.customName ? table.customName : '',
    internalNumber: table.internalNumber,
  }));

export const getLocationTablesToUpdate = (locationTables, newValues) => {
  const tablesToUpdate = [];
  const tablesByListKey = getTablesByListKey(locationTables);
  tablesByListKey.map(table => {
    const key = Object.keys(table)[0];
    const value = Object.values(table)[0];
    const correspondingValue = newValues[key];
    if (correspondingValue !== value) {
      tablesToUpdate.push({
        internalNumber: table.internalNumber,
        customName: correspondingValue,
      });
    }
    return correspondingValue !== value;
  });
  return tablesToUpdate;
};
