import React from 'react';
import { Modal } from 'antd';
import { createGlobalStyle } from 'styled-components';

// Constants
import { zIndex } from 'constants/layout';

import { EditTablesModalContent } from './EditTablesModalContent';

export const EditTablesModal = ({
  onClose,
  locationId,
  tableCount,
  numberedTables,
}) => {
  const GlobalModalStyle = createGlobalStyle`
  .noHeader .ant-modal-title {
    display: none;
  }

  .noHeader .ant-steps {
    display: none;
  }
`;

  return (
    <>
      <GlobalModalStyle />
      <Modal
        className="noHeader"
        visible
        zIndex={zIndex.modalArea}
        onCancel={onClose}
        centered
        width="760px"
        footer={null}
      >
        <EditTablesModalContent
          locationId={locationId}
          closeModal={onClose}
          tableCount={tableCount}
          numberedTables={numberedTables}
        />
      </Modal>
    </>
  );
};
