import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';
import { useQueryClient } from 'react-query';

// Components
import { BusinessAddress } from 'components/Authentication/Register/steps/BusinessInfoStep/BusinessAddress';
import {
  ButtonWrapper,
  CardTitle,
  StyledInput,
} from 'components/Authentication/Authentication.styled';
import { PrimaryButton } from 'components/general';

// Actions
import { useNameValidator } from 'components/hooks/useValidators';
import { updateOperator } from 'network/api';

import { Description } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { QUERY_KEYS, useNotifications } from 'components/hooks';
import { useModalContext } from 'components/general/Modal/useModalContext';

export const BusinessAddressModal = ({
  operator: {
    businessEntityName,
    businessEntityStreetName,
    businessEntityStreetNumber,
    businessEntityZipCode,
    businessEntityCity,
  },
}) => {
  const intl = useIntl();
  const businessNameValidator = useNameValidator('businessEntityName');
  const queryClient = useQueryClient();
  const { errorMessage, successMessage } = useNotifications();
  const { closeModal } = useModalContext();

  const onFinish = values => {
    updateOperator(values)
      .then(response => {
        if (response.status !== 204) {
          errorMessage('notification.profile.updateUser.error');
          return;
        }

        successMessage('notification.profile.updateUser.success');
        queryClient.invalidateQueries(QUERY_KEYS.ME);
        closeModal();
      })
      .catch(() => {
        errorMessage('notification.profile.updateUser.error');
      });
  };

  const initialValues = {
    businessEntityName,
    businessEntityStreetName,
    businessEntityStreetNumber,
    businessEntityZipCode,
    businessEntityCity,
  };

  return (
    <Form onFinish={onFinish} initialValues={initialValues}>
      <CardTitle data-cy="modalBusinessAddressTitle">
        {intl.formatMessage({
          id: 'modal.businessAddressTitle',
        })}
      </CardTitle>
      <Description>
        {intl.formatMessage({
          id: 'modal.businessAddressDescription',
        })}
      </Description>
      <Form.Item
        data-cy="businessEntityName"
        colon={false}
        name="businessEntityName"
        label={intl.formatMessage({
          id: 'register.businessEntityName',
        })}
        rules={businessNameValidator}
      >
        <StyledInput autoFocus />
      </Form.Item>

      <BusinessAddress />

      <ButtonWrapper>
        <PrimaryButton htmlType="submit" data-cy="confirmNameButton">
          {intl.formatMessage({
            id: 'location.save',
          })}
        </PrimaryButton>
      </ButtonWrapper>
    </Form>
  );
};
