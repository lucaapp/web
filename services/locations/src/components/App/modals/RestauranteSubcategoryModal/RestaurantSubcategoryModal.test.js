import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { act } from 'react-dom/test-utils';
import * as api from 'network/api';
import { Modal } from 'components/general';
import { RestauranteSubcategoryModal } from './RestaurantSubcategoryModal.react';

const mockedLocation = {
  uuid: '849f47df-9d80-473d-a9d4-1d73941b03d7',
  groupName: 'HILTON',
};

const mockedUpdateLocation = jest.spyOn(api, 'updateLocation');
mockedUpdateLocation.mockImplementation();

describe('RestaurantSubcategoryModal', () => {
  const setup = async () => {
    const renderResult = render(
      <Modal
        content={<RestauranteSubcategoryModal location={mockedLocation} />}
        visible
      />
    );

    const subcategoryHeader = await screen.findByTestId('subcategoryHeader');
    const subcategoryDescription = await screen.findByTestId(
      'subcategoryDescription'
    );
    const radioRestaurantField = await screen.findByTestId('radio-restaurant');
    const radioCafeField = await screen.findByTestId('radio-cafe');
    const radioClubField = await screen.findByTestId('radio-club');
    const radioBarField = await screen.findByTestId('radio-bar');
    const radioCafeteriaField = await screen.findByTestId('radio-cafeteria');
    const saveButton = await screen.findByTestId('subcategoryBtnSave');
    const closeButton = await screen.findByTestId('subcategoryBtnClose');

    return {
      renderResult,
      subcategoryHeader,
      subcategoryDescription,
      radioRestaurantField,
      radioCafeField,
      radioClubField,
      radioBarField,
      radioCafeteriaField,
      saveButton,
      closeButton,
    };
  };

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });

  // Check if save button is disabled
  test('Subtype not selected', async () => {
    const {
      saveButton,
      radioRestaurantField,
      radioCafeField,
      radioClubField,
      radioBarField,
      radioCafeteriaField,
    } = await setup();
    expect(radioRestaurantField.checked).toEqual(false);
    expect(radioCafeField.checked).toEqual(false);
    expect(radioClubField.checked).toEqual(false);
    expect(radioBarField.checked).toEqual(false);
    expect(radioCafeteriaField.checked).toEqual(false);

    expect(saveButton).toHaveAttribute('disabled');
  });

  test('Successful subtype selection', async () => {
    mockedUpdateLocation.mockResolvedValue({ status: 204 });
    const { saveButton, radioRestaurantField } = await setup();

    await act(async () => {
      await radioRestaurantField.click();
    });
    await act(async () => {
      await saveButton.click();
    });

    expect(mockedUpdateLocation).toHaveBeenCalled();
  });
});
