import React, { useState } from 'react';

import { useModalContext } from 'components/general/Modal/useModalContext';
import { useNotifications } from 'components/hooks';
import { updateLocation } from 'network/api';

import {
  StyledRadioGroup,
  StyledSpace,
} from './RestauranteSubcategoryRadioGroup.styled';

import { allowedRestauranteSubcategories } from './RestauranteSubcategoryRadioGroup.helper';

import { SubcategoryRadio } from './SubcategoryRadio';
import { RestauranteSubcategoryButtons } from './RestauranteSubcategoryButtons';

export const RestauranteSubcategoryRadioGroup = ({ location }) => {
  const { closeModal } = useModalContext();
  const { errorMessage } = useNotifications();
  const [radioValue, setRadioValue] = useState('');

  const handleChangeSubcategory = values => setRadioValue(values.target.value);

  const updateLocationSubcategory = () => {
    updateLocation({
      locationId: location?.uuid,
      data: { subtype: radioValue },
    })
      .then(() => {
        closeModal();
      })
      .catch(() => errorMessage('notification.updateLocation.error'));
  };

  return (
    <>
      <StyledRadioGroup
        onChange={handleChangeSubcategory}
        name="subcategory"
        defaultValue={radioValue}
      >
        <StyledSpace direction="vertical">
          {Object.keys(allowedRestauranteSubcategories).map(subcategory => {
            return (
              <SubcategoryRadio
                key={subcategory}
                radioValue={radioValue}
                subcategory={subcategory}
              />
            );
          })}
        </StyledSpace>
      </StyledRadioGroup>
      <RestauranteSubcategoryButtons
        radioValue={radioValue}
        updateLocationSubcategory={updateLocationSubcategory}
        closeModal={closeModal}
      />
    </>
  );
};
