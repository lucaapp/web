export const allowedRestauranteSubcategories = {
  restaurant: 'modal.subcategory.restaurant',
  cafe: 'modal.subcategory.cafe',
  club: 'modal.subcategory.club',
  bar: 'modal.subcategory.bar',
  cafeteria: 'modal.subcategory.cafeteria',
};
