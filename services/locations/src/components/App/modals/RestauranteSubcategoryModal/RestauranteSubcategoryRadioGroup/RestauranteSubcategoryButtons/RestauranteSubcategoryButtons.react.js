import React from 'react';
import { useIntl } from 'react-intl';

import { PrimaryButton, SecondaryButton } from 'components/general';
import { ButtonWrapper } from 'components/Authentication/Authentication.styled';

export const RestauranteSubcategoryButtons = ({
  radioValue,
  updateLocationSubcategory,
  closeModal,
}) => {
  const intl = useIntl();

  return (
    <ButtonWrapper multipleButtons>
      <SecondaryButton onClick={closeModal} data-cy="subcategoryBtnClose">
        {intl.formatMessage({
          id: 'button.close',
        })}
      </SecondaryButton>
      <PrimaryButton
        disabled={!radioValue}
        onClick={updateLocationSubcategory}
        data-cy="subcategoryBtnSave"
      >
        {intl.formatMessage({
          id: 'button.save',
        })}
      </PrimaryButton>
    </ButtonWrapper>
  );
};
