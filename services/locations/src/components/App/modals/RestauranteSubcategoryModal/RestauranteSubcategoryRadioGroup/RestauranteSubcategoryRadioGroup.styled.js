import styled from 'styled-components';
import { Radio, Space } from 'antd';

export const StyledRadioGroup = styled(Radio.Group)`
  margin-bottom: 24px;
  width: 100%;

  .ant-radio-inner::after {
    background-color: rgb(48, 61, 75);
  }
  .ant-space {
    width: 100%;
  }
`;

export const StyledSpace = styled(Space)`
  .ant-space-item:last-child {
    .subcategoryRadio {
      border-bottom: none;
    }
  }
`;
