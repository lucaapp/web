import React from 'react';

import { Radio } from 'antd';
import { useIntl } from 'react-intl';
import { Label, Wrapper } from './SubcategoryRadio.styled';
import { allowedRestauranteSubcategories } from '../RestauranteSubcategoryRadioGroup.helper';

export const SubcategoryRadio = ({ radioValue, subcategory }) => {
  const intl = useIntl();

  return (
    <Wrapper className="subcategoryRadio">
      <Label $isChecked={radioValue === subcategory}>
        {intl.formatMessage({
          id: allowedRestauranteSubcategories[subcategory],
        })}
      </Label>
      <Radio value={subcategory} data-cy={`radio-${subcategory}`} />
    </Wrapper>
  );
};
