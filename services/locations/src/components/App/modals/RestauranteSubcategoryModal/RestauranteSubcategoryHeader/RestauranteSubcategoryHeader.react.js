import React from 'react';
import { useIntl } from 'react-intl';

import {
  SubcategoryDescription,
  SubcategoryHeader,
} from './RestauranteSubcategoryHeader.styled';

export const RestauranteSubcategoryHeader = ({ location }) => {
  const intl = useIntl();

  const groupName = location?.groupName;

  return (
    <>
      <SubcategoryHeader data-cy="subcategoryHeader">
        {`${groupName} - ${intl.formatMessage({
          id: 'modal.subcategory.title',
        })}`}
      </SubcategoryHeader>
      <SubcategoryDescription data-cy="subcategoryDescription">
        {intl.formatMessage({
          id: 'modal.subcategory.description',
        })}
      </SubcategoryDescription>
    </>
  );
};
