import styled from 'styled-components';

export const SubcategoryHeader = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-weight: bold;
`;

export const SubcategoryDescription = styled.div`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  margin-top: 16px;
  margin-bottom: 8px;
`;
