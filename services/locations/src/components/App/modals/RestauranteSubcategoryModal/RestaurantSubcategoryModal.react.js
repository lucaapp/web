import React from 'react';

import { RestauranteSubcategoryRadioGroup } from './RestauranteSubcategoryRadioGroup';
import { RestauranteSubcategoryHeader } from './RestauranteSubcategoryHeader';

export const RestauranteSubcategoryModal = ({ location }) => {
  return (
    <>
      <RestauranteSubcategoryHeader location={location} />
      <RestauranteSubcategoryRadioGroup location={location} />
    </>
  );
};
