import React, { useMemo, useState } from 'react';
import moment from 'moment';
import { Spin, Popconfirm, notification } from 'antd';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { QuestionCircleOutlined } from '@ant-design/icons';

// Api
import { forceCheckoutByOperator, forceCheckoutUsers } from 'network/api';

// Components
import { QUERY_KEYS, useGetTraces, useNotifications } from 'components/hooks';
import { PrimaryButton } from 'components/general';
import { TableHeader } from './TableHeader';
import { Headline } from './Headline';
import { ModalHeadline } from './ModalHeadline';
import {
  Entry,
  Loading,
  Wrapper,
  GuestTable,
  TableRow,
} from './TableAllocationModal.styled';
import { extractTableNumbers } from './TableAllocationModal.helper';

export const TableAllocationModal = ({ privateKey, location }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const { errorMessage } = useNotifications();
  const [lastRefresh, setLastRefresh] = useState(moment());

  const { isLoading, error: fetchError, data: traces } = useGetTraces(
    location.uuid
  );

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const activeTables = useMemo(() => extractTableNumbers(traces, privateKey), [
    traces,
    privateKey,
  ]);

  const refresh = () => {
    setLastRefresh(moment());
    queryClient.invalidateQueries([QUERY_KEYS.TRACES, location.uuid]);
  };

  const handleError = () => {
    errorMessage('notification.checkOut.error');
  };

  const onCheckoutTable = traceIds => {
    Promise.all(traceIds.map(traceId => forceCheckoutByOperator(traceId)))
      .then(responses => {
        if (responses.some(response => response.status !== 204)) {
          throw new Error('checkout failed');
        }

        queryClient.invalidateQueries([QUERY_KEYS.TRACES, location.uuid]);
        queryClient.invalidateQueries([
          QUERY_KEYS.CURRENT_COUNT,
          location.scannerAccessId,
        ]);

        notification.success({
          message: intl.formatMessage({
            id: 'notification.checkOut.success',
          }),
          className: 'successCheckout',
        });
      })
      .catch(() => {
        handleError();
      });
  };

  const checkoutAll = () => {
    forceCheckoutUsers(location.uuid)
      .then(response => {
        if (response.status === 204) {
          queryClient.invalidateQueries(QUERY_KEYS.GROUPS);
          queryClient.invalidateQueries([
            QUERY_KEYS.CURRENT_COUNT,
            location.scannerAccessId,
          ]);

          refresh();

          notification.success({
            message: intl.formatMessage({
              id: 'notification.checkOut.success',
            }),
            className: 'successCheckout',
          });
        }
      })
      .catch(() => {
        handleError();
      });
  };

  if (isLoading)
    return (
      <Loading>
        <Spin size="large" />
      </Loading>
    );
  if (fetchError) return null;

  return (
    <Wrapper>
      <ModalHeadline location={location} />
      {!!Object.keys(activeTables).length && (
        <Headline
          activeTables={activeTables}
          callback={checkoutAll}
          lastRefresh={lastRefresh}
        />
      )}
      <GuestTable>
        <TableHeader activeTables={activeTables} />
        <tbody>
          {Object.keys(activeTables).map(table => (
            <TableRow headline="true" key={`table_${table}`}>
              <Entry>{table}</Entry>
              <Entry>{activeTables[table]?.length}</Entry>
              <Entry>
                {activeTables[table]?.length > 0 && (
                  <Popconfirm
                    placement="topLeft"
                    onConfirm={() => onCheckoutTable(activeTables[table])}
                    title={intl.formatMessage({
                      id: 'location.checkout.confirmTableCheckoutText',
                    })}
                    okText={intl.formatMessage({
                      id: 'location.checkout.confirmButton',
                    })}
                    okButtonProps={{ 'data-cy': 'checkoutButton' }}
                    cancelText={intl.formatMessage({
                      id: 'generic.cancel',
                    })}
                    cancelButtonProps={{ 'data-cy': 'cancelButton' }}
                    icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
                  >
                    <PrimaryButton data-cy="openCheckoutConfirmationButton">
                      {intl.formatMessage({
                        id: 'group.view.overview.tableAllocationCheckout',
                      })}
                    </PrimaryButton>
                  </Popconfirm>
                )}
              </Entry>
            </TableRow>
          ))}
        </tbody>
      </GuestTable>
    </Wrapper>
  );
};
