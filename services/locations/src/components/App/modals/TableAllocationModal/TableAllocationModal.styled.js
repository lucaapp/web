import styled from 'styled-components';

const getFontFamily = () => 'font-family: Montserrat-SemiBold, sans-serif';

const getMargin = () => 'margin-bottom: 16px';

const getBorder = hasBorder =>
  hasBorder ? 'border-bottom: 1px solid rgb(151, 151, 151)' : '';

const getTableHeader = isTableHeader =>
  isTableHeader ? 'font-weight: bold' : '';

export const Wrapper = styled.div`
  display: flex;
  width: 700px;
  flex-direction: column;
`;

export const Loading = styled.div`
  font-size: 24px;
  text-align: center;
  margin-top: 24px;
`;

export const HeaderRow = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 40px;
  margin-top: 30px;
`;

export const Entry = styled.td`
  ${getFontFamily()};
  ${getMargin()};
  ${({ tableHeader }) => getTableHeader(tableHeader)};
  padding: 10px 10px;
  text-align: left;
`;

export const ActiveTableCount = styled.div`
  font-size: 16px;
  font-weight: 500;
`;

export const GuestTable = styled.table`
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
`;

export const TableRow = styled.tr`
  padding: 5px 0;
  text-align: left;
  ${({ borderBottom }) => getBorder(borderBottom)};
`;
