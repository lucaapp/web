import React from 'react';
import { useIntl } from 'react-intl';
import { Popconfirm } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';

// Components
import { PrimaryButton } from 'components/general';
import { ActiveTableCount, HeaderRow } from '../TableAllocationModal.styled';

export const Headline = ({ activeTables, callback }) => {
  const intl = useIntl();

  return (
    <>
      <HeaderRow>
        <ActiveTableCount data-cy="allocatedTableCount">
          {`${intl.formatMessage({
            id: 'modal.tableAllocation.activeTableCount',
          })}: `}
          <b>{Object.keys(activeTables).length}</b>
        </ActiveTableCount>
        <Popconfirm
          placement="topLeft"
          onConfirm={callback}
          title={intl.formatMessage({
            id: 'location.checkout.confirmAllTableCheckoutText',
          })}
          okText={intl.formatMessage({
            id: 'group.view.overview.tableAllocationCheckoutAll',
          })}
          okButtonProps={{ 'data-cy': 'checkoutAllTablesButton' }}
          cancelText={intl.formatMessage({
            id: 'generic.cancel',
          })}
          cancelButtonProps={{ 'data-cy': 'cancelCheckoutAllTablesButton' }}
          icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
        >
          <PrimaryButton data-cy="openCheckoutAllTablesConfirmation">
            {intl.formatMessage({
              id: 'group.view.overview.tableAllocationCheckoutAll',
            })}
          </PrimaryButton>
        </Popconfirm>
      </HeaderRow>
    </>
  );
};
