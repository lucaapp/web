import React, { useState } from 'react';
import moment from 'moment';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';

// Components
import { QUERY_KEYS } from 'components/hooks';
import { RefreshIcon } from 'assets/icons';
import {
  AllocationHeading,
  RefreshTime,
  WrapperHeading,
  IconStyled,
} from './ModalHeadline.styled';

export const ModalHeadline = ({ location }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const [lastRefresh, setLastRefresh] = useState(moment());

  const refresh = () => {
    setLastRefresh(moment());
    queryClient.invalidateQueries([QUERY_KEYS.TRACES, location.uuid]);
  };

  return (
    <WrapperHeading>
      <AllocationHeading>
        {intl.formatMessage({
          id: 'modal.tableAllocation.title',
        })}
      </AllocationHeading>
      <RefreshTime data-cy="refreshTime">
        {`${intl.formatMessage({
          id: 'modal.tableAllocation.lastRefresh',
        })}: ${moment(lastRefresh).format('DD.MM.YYYY - HH:mm:ss')}`}
      </RefreshTime>
      <IconStyled
        onClick={refresh}
        component={RefreshIcon}
        data-cy="refreshButton"
      />
    </WrapperHeading>
  );
};
