import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const AllocationHeading = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-right: 16px;
`;

export const RefreshTime = styled.div`
  font-size: 14px;
  font-weight: 500;
  color: #000;
  font-family: Montserrat-Medium, sans-serif;
`;

export const WrapperHeading = styled.div`
  display: flex;
  align-items: baseline;
`;

export const IconStyled = styled(Icon)`
  margin-left: 16px;
  cursor: pointer;

  polyline {
    stroke: rgb(80, 102, 124);
  }
  path {
    fill: rgb(80, 102, 124);
  }
`;
