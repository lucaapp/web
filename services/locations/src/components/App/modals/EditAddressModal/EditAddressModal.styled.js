import styled from 'styled-components';

export const Header = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 24px;
`;

export const StyledSwitchContainer = styled.div`
  margin-top: 8px;
  display: flex;
`;

export const StyledButtonWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 40px 0 20px 0;
`;
