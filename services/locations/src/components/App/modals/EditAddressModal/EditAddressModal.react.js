import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { useQuery, useQueryClient } from 'react-query';
import { Form, notification } from 'antd';
import { Wrapper } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { getGroup, updateAddress } from 'network/api';
import { QUERY_KEYS } from 'components/hooks/useQueries';
import { useModalContext } from 'components/general/Modal/useModalContext';
import { ExpandAddressFlow } from 'components/general/ExpandAddressFlow';
import { useUser } from 'store-context/selectors';
import { useNotifications } from 'components/hooks';
import { AddressHeading } from './AddressHeading';
import { ButtonWrapper } from './ButtonWrapper';
import {
  getLocationAddress,
  getOperatorBusinessAddress,
} from './EditAddressModal.helper';

export const EditAddressModal = ({ location, isGroup }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const { closeModal } = useModalContext();
  const [form] = Form.useForm();
  const [displayManualAddress, setDisplayManualAddress] = useState(false);
  const [businessAddressEnabled, setBusinessAddressEnabled] = useState(false);
  const [isError, setIsError] = useState(false);
  const [filled, setFilled] = useState(false);
  const [hasPlace, setHasPlace] = useState(false);
  const [enableGoogleService, setEnableGoogleService] = useState(false);
  const [coordinates, setCoordinates] = useState(null);
  const { errorMessage } = useNotifications();

  const { data: operator } = useUser();

  const isBaseLocation = location.name === null;

  const { data: group, isLoading, error } = useQuery(
    [QUERY_KEYS.GROUP, location.groupId],
    () => getGroup(location.groupId),
    { enabled: isBaseLocation }
  );

  const baseLocation = group?.locations?.find(loc => loc.name === null);

  const baseLocationAddress = getLocationAddress(baseLocation);
  const initialValues = getLocationAddress(location);

  useEffect(() => {
    setEnableGoogleService(location.lat && location.lng);
  }, [form, location.lat, location.lng]);

  useEffect(() => {
    if (displayManualAddress && !businessAddressEnabled) {
      form.setFieldsValue(initialValues);
    }

    if (displayManualAddress && businessAddressEnabled && isBaseLocation) {
      form.setFieldsValue(getOperatorBusinessAddress(operator));
    }
    if (displayManualAddress && businessAddressEnabled && !isBaseLocation) {
      form.setFieldsValue(baseLocationAddress);
    }
  }, [
    baseLocationAddress,
    businessAddressEnabled,
    displayManualAddress,
    form,
    initialValues,
    isBaseLocation,
    operator,
  ]);

  const handleSwitch = () => {
    if (displayManualAddress) {
      return setBusinessAddressEnabled(!businessAddressEnabled);
    }
    setFilled(false);
    setHasPlace(false);

    return setEnableGoogleService(!enableGoogleService);
  };

  const handleError = () =>
    errorMessage(
      isGroup
        ? 'notification.updateGroup.error'
        : 'notification.updateArea.error'
    );

  const invalidateQueries = () => {
    queryClient.invalidateQueries([QUERY_KEYS.GROUP]);
    queryClient.invalidateQueries([QUERY_KEYS.LOCATION]);
  };

  const handleSubmit = async values => {
    const addressPayload = {
      streetName: values.streetName,
      streetNr: values.streetNr,
      zipCode: values.zipCode,
      city: values.city,
      state: values.state ?? null,
      lat: values.lat ?? null,
      lng: values.lng ?? null,
    };

    try {
      const response = await updateAddress({
        locationId: location.uuid,
        data: addressPayload,
      });

      if (response.status !== 204) {
        handleError();
        invalidateQueries();
        return;
      }

      invalidateQueries();
      notification.success({
        message: intl.formatMessage({
          id: isGroup
            ? 'notification.updateGroup.success'
            : 'notification.updateArea.success',
        }),
        className: 'editLocationSuccess',
      });

      closeModal();
    } catch {
      handleError();
    }
  };

  const processContinue = () => {
    if (!enableGoogleService && !displayManualAddress) {
      return setDisplayManualAddress(true);
    }
    if (coordinates !== null) {
      form.setFieldsValue({ ...coordinates });
    }

    return form.submit();
  };

  if (isLoading || error) return null;

  return (
    <Wrapper>
      <AddressHeading
        displayManualAddress={displayManualAddress}
        isGroup={isGroup}
      />

      <ExpandAddressFlow
        handleSwitch={handleSwitch}
        enabled={enableGoogleService}
        form={form}
        handleSubmit={handleSubmit}
        initialValues={initialValues}
        coordinates={coordinates}
        setCoordinates={setCoordinates}
        displayManualAddress={displayManualAddress}
        businessAddressEnabled={businessAddressEnabled}
        isError={isError}
        setIsError={setIsError}
        filled={filled}
        setFilled={setFilled}
        hasPlace={hasPlace}
        setHasPlace={setHasPlace}
        isBaseLocation={isBaseLocation}
      />

      <ButtonWrapper
        displayManualAddress={displayManualAddress}
        enableGoogleService={enableGoogleService}
        hasPlace={hasPlace}
        onClick={processContinue}
        isError={isError}
      />
    </Wrapper>
  );
};
