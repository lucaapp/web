export const getLocationAddress = location => ({
  city: location?.city,
  streetName: location?.streetName,
  streetNr: location?.streetNr,
  zipCode: location?.zipCode,
});

export const getOperatorBusinessAddress = operator => ({
  city: operator?.businessEntityCity,
  streetName: operator?.businessEntityStreetName,
  streetNr: operator?.businessEntityStreetNumber,
  zipCode: operator?.businessEntityZipCode,
});
