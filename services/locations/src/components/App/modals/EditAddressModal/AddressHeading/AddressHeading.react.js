import React from 'react';
import { useIntl } from 'react-intl';
import { Description } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { Header } from '../EditAddressModal.styled';

export const AddressHeading = ({ displayManualAddress, isGroup }) => {
  const intl = useIntl();

  const suffix = !isGroup ? 'Area' : '';

  return (
    <>
      <Header>
        {intl.formatMessage({
          id: displayManualAddress
            ? `modal.createGroup.googlePlacesInput.headingManualAddress${suffix}`
            : 'modal.createGroup.googlePlacesInput.heading',
        })}
      </Header>
      <Description>
        {intl.formatMessage({
          id: `modal.createGroup.googlePlacesInput.firstDescriptionManualAddress${suffix}`,
        })}
      </Description>

      {!displayManualAddress && (
        <Description>
          {intl.formatMessage(
            {
              id: `modal.createGroup.googlePlacesInput.secondDescription${suffix}`,
            },
            // eslint-disable-next-line react/display-name
            { br: <br />, b: (...chunks) => <b>{chunks}</b> }
          )}
        </Description>
      )}
    </>
  );
};
