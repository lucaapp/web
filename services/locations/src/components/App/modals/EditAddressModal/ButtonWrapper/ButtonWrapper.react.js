import React from 'react';
import { useIntl } from 'react-intl';
import { PrimaryButton, SecondaryButton } from 'components/general';
import { useModalContext } from 'components/general/Modal/useModalContext';
import { StyledButtonWrapper } from '../EditAddressModal.styled';

export const ButtonWrapper = ({
  displayManualAddress,
  hasPlace,
  enableGoogleService,
  isError,
  onClick,
}) => {
  const intl = useIntl();
  const { closeModal } = useModalContext();

  return (
    <StyledButtonWrapper>
      <SecondaryButton onClick={closeModal}>
        {intl.formatMessage({
          id: 'generic.cancel',
        })}
      </SecondaryButton>

      <PrimaryButton
        data-cy={displayManualAddress || hasPlace ? 'saveAddress' : 'nextStep'}
        onClick={onClick}
        disabled={isError || (enableGoogleService && !hasPlace)}
      >
        {intl.formatMessage({
          id:
            displayManualAddress || hasPlace
              ? 'button.save'
              : 'modal.createGroup.googlePlacesInput.continue',
        })}
      </PrimaryButton>
    </StyledButtonWrapper>
  );
};
