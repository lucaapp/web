import React from 'react';
import { useIntl } from 'react-intl';
import { Wrapper } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { CompleteCard } from 'components/App/modals/generalOnboarding/common/CompleteCard';
import { StepProgress } from 'components/App/modals/generalOnboarding/StepProgress';
import { QRDownload } from './QRDownload';

export const Complete = ({ location, group, done, back, totalSteps }) => {
  const intl = useIntl();

  if (!location) {
    back();
  }

  return (
    <Wrapper>
      <StepProgress currentStep={7} totalSteps={totalSteps} />
      <CompleteCard
        title={intl.formatMessage({
          id: 'modal.createLocation.complete.title',
        })}
        description={intl.formatMessage({
          id: 'modal.createLocation.complete.description',
        })}
      >
        <QRDownload location={location} group={group} done={done} />
      </CompleteCard>
    </Wrapper>
  );
};
