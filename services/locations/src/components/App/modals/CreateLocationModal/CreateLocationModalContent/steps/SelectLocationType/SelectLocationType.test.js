import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { act } from 'react-dom/test-utils';
import { SelectLocationType } from './SelectLocationType.react';

const mockNext = jest.fn();
const mockSetLocationType = jest.fn();

describe('SelectLocationType', () => {
  const setup = async () => {
    const renderResult = render(
      <SelectLocationType
        currentStep={1}
        totalSteps={2}
        setLocationType={mockSetLocationType}
        next={mockNext}
      />
    );

    const wrapper = await screen.findByTestId('selectLocationType');
    const selection = await screen.findByTestId('selectLocationTypeSelection');
    const restaurantOption = await screen.findByTestId('restaurant');
    const baseOption = await screen.findByTestId('base');
    const roomOption = await screen.findByTestId('room');
    const buildingOption = await screen.findByTestId('building');

    return {
      renderResult,
      wrapper,
      selection,
      restaurantOption,
      baseOption,
      roomOption,
      buildingOption,
    };
  };
  afterEach(() => {
    jest.clearAllMocks();
  });

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });
  test('Selecting an option', async () => {
    const { restaurantOption } = await setup();

    await act(async () => {
      await restaurantOption.click();
    });
    expect(mockSetLocationType).toHaveBeenCalledWith('restaurant');
    expect(mockNext).toHaveBeenCalledTimes(1);
  });
});
