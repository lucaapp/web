import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import { useNameValidator } from 'components/hooks/useValidators';

import { getDefaultNameRule, getUniqueNameRule } from 'utils/antFormRules';

import { useGetGroup } from 'components/hooks';
import { NameInput } from 'components/App/modals/generalOnboarding/NameInput';

export const NameInputLocations = ({
  locationName: currentLocationName,
  setLocationName,
  back,
  next,
  groupId,
  totalSteps,
  currentStep,
}) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [isLocationNameTaken, setIsLocationNameTaken] = useState(false);
  const locationNameValidator = useNameValidator('locationName');
  const { isLoading, error, data: group } = useGetGroup(groupId);

  const onFinish = ({ locationName }) => {
    const isExistingLocationName = group.locations.some(
      location => location.name === locationName.trim()
    );
    if (isExistingLocationName) {
      setIsLocationNameTaken(true);
      form.validateFields(['locationName']);
    } else {
      setIsLocationNameTaken(false);
      setLocationName(locationName);
      next();
    }
  };

  const onValueUpdate = () => setIsLocationNameTaken(false);

  const locationNameRules = [
    getDefaultNameRule(intl),
    getUniqueNameRule(intl, isLocationNameTaken),
    ...locationNameValidator,
  ];

  if (isLoading || error) return null;

  return (
    <NameInput
      initialValues={{ locationName: currentLocationName ?? '' }}
      modalArea="createLocation"
      back={back}
      onFinish={onFinish}
      onValueUpdate={onValueUpdate}
      totalSteps={totalSteps}
      currentStep={currentStep}
      rules={locationNameRules}
      form={form}
      name="locationName"
    />
  );
};
