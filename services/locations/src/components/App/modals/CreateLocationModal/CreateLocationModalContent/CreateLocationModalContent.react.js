import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Steps } from 'antd';
import { getBaseLocationFromGroup } from 'utils/group';
import { useGetGroup } from 'components/hooks';
import { useModalContext } from 'components/general/Modal/useModalContext';
import {
  TableInput,
  PhoneInput,
  AreaDetails,
  GooglePlacesInput,
} from 'components/App/modals/generalOnboarding';
import { BASE_GROUP_ROUTE, BASE_LOCATION_ROUTE } from 'constants/routes';
import { useMatomo } from '@jonkoops/matomo-tracker-react';
import {
  COMPLETE_STEP,
  NAME_INPUT_STEP,
  PHONE_INPUT_STEP,
  SELECT_LOCATION_STEP,
  TABLE_INPUT_STEP,
  getLocationPayload,
  AREA_DETAILS_STEP,
  TOTAL_STEPS,
} from './CreateLocationModalContent.helper';

import { SelectLocationType } from './steps/SelectLocationType';
import { NameInputLocations } from './steps/NameInputLocations';
import { Complete } from './steps/Complete';
import { GOOGLE_PLACES_OPT_IN_STEP } from '../../CreateGroupModal/CreateGroupModalContent/CreateGroupModalContent.helper';

export const CreateLocationModalContent = ({ groupId, setLocationId }) => {
  const history = useHistory();
  const { trackEvent } = useMatomo();
  const [currentStep, setCurrentStep] = useState(0);
  const [locationType, setLocationType] = useState(null);
  const [locationName, setLocationName] = useState(null);
  const [address, setAddress] = useState(null);
  const [phone, setPhone] = useState(null);
  const [areaDetails, setAreaDetails] = useState({
    isIndoor: true,
  });
  const [tableCount, setTableCount] = useState(null);
  const [location, setLocation] = useState(null);
  const [baseLocation, setBaseLocation] = useState(null);
  const [coordinates, setCoordinates] = useState(null);
  const [googlePlaces, setGooglePlaces] = useState(false);

  const { isLoading, error, data: group } = useGetGroup(groupId);
  const { closeModal } = useModalContext();

  const nextStep = () => setCurrentStep(currentStep + 1);
  const previousStep = () => setCurrentStep(currentStep - 1);

  const onDone = () => {
    history.push(
      `${BASE_GROUP_ROUTE}${groupId}${BASE_LOCATION_ROUTE}${location.uuid}`
    );

    trackEvent({
      category: 'location',
      action: 'created',
      name: location.name || '',
      value: location.uuid,
    });

    closeModal();
  };

  const locationPayload = getLocationPayload({
    groupId,
    locationName,
    phone,
    address,
    baseLocation,
    tableCount,
    locationType,
    isIndoor: areaDetails.isIndoor,
    coordinates,
  });

  const steps = [
    {
      id: SELECT_LOCATION_STEP,
      content: (
        <SelectLocationType
          setLocationType={setLocationType}
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: NAME_INPUT_STEP,
      content: (
        <NameInputLocations
          locationType={locationType}
          locationName={locationName}
          setLocationName={setLocationName}
          next={nextStep}
          back={previousStep}
          groupId={groupId}
          totalSteps={TOTAL_STEPS}
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: PHONE_INPUT_STEP,
      content: (
        <PhoneInput
          phone={phone}
          setPhone={setPhone}
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: GOOGLE_PLACES_OPT_IN_STEP,
      content: (
        <GooglePlacesInput
          baseLocation={baseLocation}
          totalSteps={TOTAL_STEPS}
          setEnabled={setGooglePlaces}
          enabled={googlePlaces}
          address={address}
          setAddress={setAddress}
          coordinates={coordinates}
          setCoordinates={setCoordinates}
          googleEnabled={googlePlaces}
          next={nextStep}
          back={previousStep}
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: AREA_DETAILS_STEP,
      content: (
        <AreaDetails
          areaDetails={areaDetails}
          setAreaDetails={setAreaDetails}
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
          modalArea="createLocation"
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: TABLE_INPUT_STEP,
      content: (
        <TableInput
          tableCount={tableCount}
          setTableCount={setTableCount}
          next={nextStep}
          back={previousStep}
          totalSteps={TOTAL_STEPS}
          setLocationId={setLocationId}
          setLocation={setLocation}
          locationPayload={locationPayload}
          isLocationModal
          currentStep={currentStep + 1}
        />
      ),
    },
    {
      id: COMPLETE_STEP,
      content: (
        <Complete
          location={location}
          group={group}
          done={onDone}
          totalSteps={TOTAL_STEPS}
          currentStep={currentStep + 1}
        />
      ),
    },
  ];

  useEffect(() => {
    if (isLoading || error || baseLocation || !group) {
      return;
    }

    setBaseLocation(getBaseLocationFromGroup(group));
  }, [isLoading, error, baseLocation, group]);

  if (isLoading || error) {
    return null;
  }

  return (
    <>
      <Steps
        progressDot={() => null}
        current={currentStep}
        style={{ margin: 0 }}
      >
        {steps.map(step => (
          <Steps.Step key={step.id} />
        ))}
      </Steps>
      {steps[currentStep].content}
    </>
  );
};
