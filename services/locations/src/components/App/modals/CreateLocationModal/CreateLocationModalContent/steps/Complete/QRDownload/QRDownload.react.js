import React, { useState } from 'react';
import { useIntl } from 'react-intl';

import { DEFAULT_CWA_VALUE } from 'constants/cwaQrCodeValue';
import { PrimaryButton, SuccessButton } from 'components/general';

// Worker
import { useGetLocationTables, useWorker } from 'components/hooks';
import { downloadPDF } from 'utils/downloadPDF';
import { getPDFWorker } from 'utils/workers';

import {
  ButtonWrapper,
  QRCodeDownloadWrapper,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { QRDownloadCard } from 'components/App/modals/generalOnboarding/common/QRDownloadCard';

export const QRDownload = ({ done, location, group }) => {
  const intl = useIntl();
  const [isDownloading, setIsDownloading] = useState(false);

  const pdfWorkerApiReference = useWorker(getPDFWorker());

  const { error, data: locationTables, isLoading } = useGetLocationTables(
    location.uuid
  );

  const downloadOptions = {
    setIsDownloading,
    pdfWorkerApiReference,
    location: {
      ...location,
      groupName: group?.name,
      tables: locationTables,
    },
    intl,
    isTableQRCodeEnabled: locationTables?.length > 0,
    isCWAEventEnabled: DEFAULT_CWA_VALUE,
  };
  const triggerDownload = () => downloadPDF(downloadOptions);

  if (error || isLoading) return null;

  const suffix = locationTables.length > 0 ? 'table' : 'entire';

  return (
    <QRDownloadCard
      title={intl.formatMessage({
        id: `modal.createLocation.QRDownload.title.${suffix}`,
      })}
      description={intl.formatMessage({
        id: `modal.createLocation.QRDownload.description.${suffix}`,
      })}
    >
      <QRCodeDownloadWrapper>
        <SuccessButton
          data-cy="downloadQRCode"
          onClick={triggerDownload}
          loading={isDownloading}
          disabled={isDownloading}
        >
          {intl.formatMessage({
            id: `qrCodesDownload.${suffix}`,
          })}
        </SuccessButton>
      </QRCodeDownloadWrapper>

      <ButtonWrapper>
        <PrimaryButton onClick={done} data-cy="done">
          {intl.formatMessage({
            id: 'done',
          })}
        </PrimaryButton>
      </ButtonWrapper>
    </QRDownloadCard>
  );
};
