import React from 'react';
import { createGlobalStyle } from 'styled-components';

import { CreateLocationModalContent } from './CreateLocationModalContent';

const GlobalModalStyle = createGlobalStyle`
  .noHeader .ant-modal-title {
    display: none;
  }

  .noHeader .ant-steps {
    display: none;
  }
`;

export const CreateLocationModal = ({ groupId }) => {
  return (
    <>
      <GlobalModalStyle />
      <CreateLocationModalContent groupId={groupId} />
    </>
  );
};
