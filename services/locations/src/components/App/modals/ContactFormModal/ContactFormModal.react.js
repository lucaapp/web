import React, { useState } from 'react';
import { Steps } from 'antd';

import { useModalContext } from 'components/general/Modal/useModalContext';
import { REQUEST_STEP, FINISH_STEP } from './ContactFormModal.helper';

import { RequestStep } from './steps/RequestStep';
import { FinishStep } from './steps/FinishStep';

export const ContactFormModal = () => {
  const { closeModal } = useModalContext();
  const [currentStep, setCurrentStep] = useState(0);

  const nextStep = () => setCurrentStep(currentStep + 1);

  const steps = [
    {
      id: REQUEST_STEP,
      content: <RequestStep next={nextStep} closeModal={closeModal} />,
    },
    {
      id: FINISH_STEP,
      content: <FinishStep done={closeModal} />,
    },
  ];

  return (
    <>
      <Steps
        progressDot={() => null}
        current={currentStep}
        style={{ display: 'none' }}
      >
        {steps.map(step => (
          <Steps.Step key={step.id} />
        ))}
      </Steps>
      {steps[currentStep].content}
    </>
  );
};
