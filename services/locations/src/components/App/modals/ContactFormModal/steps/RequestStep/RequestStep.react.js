import React from 'react';
import { useIntl } from 'react-intl';
import { Form, Input } from 'antd';

import { PrimaryButton, SecondaryButton } from 'components/general';

import { sendSupportMail } from 'network/api';

import { useTextAreaValidator } from 'components/hooks/useValidators';
import { getFormattedPhoneNumber } from 'utils/formatters';

import { getPhoneRule } from 'utils/antFormRules';
import { useNotifications, useOperator } from 'components/hooks';
import {
  Wrapper,
  Text,
  ContactDataWrapper,
  Name,
  Value,
  ButtonWrapper,
  Title,
  StyledFormItem,
} from './RequestStep.styled';

export const RequestStep = ({ next, closeModal }) => {
  const intl = useIntl();
  const { data: operator } = useOperator();
  const { errorMessage } = useNotifications();

  const requestTextValidator = useTextAreaValidator('requestText', 3000);

  const contactData = [
    {
      intlId: 'contactForm.modal.supportCode',
      value: operator.supportCode,
      testId: 'contactFormOperatorSupportCode',
    },
    {
      intlId: 'contactForm.modal.name',
      value: `${operator.firstName} ${operator.lastName}`,
      testId: 'contactFormOperatorName',
    },
    {
      intlId: 'contactForm.modal.email',
      value: operator.email,
      testId: 'contactFormOperatorEmail',
    },
  ];

  const onFinish = async values => {
    const { phone, requestText } = values;
    const formattedPhone = phone ? getFormattedPhoneNumber(phone) : undefined;
    const formattedRequestText = requestText?.split('\n').join(' ').trim();

    try {
      const response = await sendSupportMail({
        phone: formattedPhone,
        requestText: formattedRequestText,
      });

      if (response.status !== 204) return;

      next();
    } catch {
      errorMessage('contactForm.modal.notification.error');
    }
  };

  return (
    <Wrapper data-cy="contactFormModal">
      <Title>
        {intl.formatMessage({
          id: 'contactForm.modal.title',
        })}
      </Title>
      <Text>{intl.formatMessage({ id: 'contactForm.modal.text' })}</Text>
      {contactData.map(entry => (
        <ContactDataWrapper key={entry.intlId}>
          <Name>{`${intl.formatMessage({ id: entry.intlId })}:`}</Name>
          <Value data-cy={entry.testId}>{entry.value}</Value>
        </ContactDataWrapper>
      ))}
      <Form onFinish={onFinish}>
        <StyledFormItem
          colon={false}
          rules={[getPhoneRule(intl)]}
          label={intl.formatMessage({
            id: 'contactForm.modal.phone',
          })}
          name="phone"
        >
          <Input data-cy="contactFormPhoneNumber" />
        </StyledFormItem>
        <Form.Item
          colon={false}
          label={intl.formatMessage({
            id: 'contactForm.modal.request',
          })}
          name="requestText"
          rules={requestTextValidator}
          required
        >
          <Input.TextArea
            maxLength={3000}
            showCount
            data-cy="contactFormMessage"
            autoSize={{ minRows: 2, maxRows: 6 }}
          />
        </Form.Item>
        <ButtonWrapper multipleButtons>
          <SecondaryButton onClick={closeModal}>
            {intl.formatMessage({
              id: 'generic.cancel',
            })}
          </SecondaryButton>
          <PrimaryButton htmlType="submit" data-cy="contactFormSendButton">
            {intl.formatMessage({
              id: 'contactForm.modal.submit',
            })}
          </PrimaryButton>
        </ButtonWrapper>
      </Form>
    </Wrapper>
  );
};
