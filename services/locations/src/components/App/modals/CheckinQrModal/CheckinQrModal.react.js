import React from 'react';

import { useIntl } from 'react-intl';

import { LucaLogoPaddingSVG } from 'assets/icons';

import {
  Wrapper,
  Description,
  QrCodeWrapper,
  StyledQRCode,
} from './CheckinQrModal.styled';

const imageSettings = {
  src: LucaLogoPaddingSVG,
  x: null,
  y: null,
  height: 40,
  width: 80,
  excavate: true,
};

export const CheckinQrModal = ({ service }) => {
  const intl = useIntl();

  return (
    <Wrapper id="test">
      <Description>
        {intl.formatMessage(
          { id: 'modal.checkInQrModal.description' },
          { divider: <br /> }
        )}
      </Description>
      <QrCodeWrapper>
        <StyledQRCode
          value={service.url}
          size={200}
          imageSettings={imageSettings}
          level="M"
        />
      </QrCodeWrapper>
    </Wrapper>
  );
};
