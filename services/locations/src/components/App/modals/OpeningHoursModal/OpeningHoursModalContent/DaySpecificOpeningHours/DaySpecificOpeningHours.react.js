import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import { WithHover } from 'components/general/WithHover';
import {
  Description,
  StyledCheckbox,
  StyledCol,
  StyledRow,
  StyledTimeRangePicker,
  StyledColCheckbox,
} from './DaySpecificOpeningHours.styled';
import {
  formatTime,
  getTimePlaceholder,
  showTimeByLang,
  transformOpeningHoursOfDaysToInitialValues,
} from './DaySpecificOpeningHours.helper';
import { ActionItems } from './ActionItems';

export const DaySpecificOpeningHours = ({
  openingHours,
  weekday,
  onRangeChange,
}) => {
  const intl = useIntl();
  const currentLanguage = intl.locale;

  const initialFormOpenHours = transformOpeningHoursOfDaysToInitialValues(
    openingHours,
    currentLanguage
  );

  const showTime = showTimeByLang(currentLanguage);

  const format = formatTime(currentLanguage);
  const placeholder = getTimePlaceholder(currentLanguage);

  return (
    <Form.List key={weekday} name={weekday} initialValue={initialFormOpenHours}>
      {(fields, { add, remove }) => {
        return (
          <>
            {fields.map((field, index_) => (
              <WithHover key={field.fieldKey}>
                {(isHovered, onPointerEnter, onPointerLeave) => (
                  <StyledRow
                    onPointerEnter={onPointerEnter}
                    onPointerLeave={onPointerLeave}
                  >
                    <StyledColCheckbox span={8} $isVisible={index_ === 0}>
                      <Form.Item
                        noStyle
                        valuePropName="checked"
                        name={[field.name, 'included']}
                        fieldKey={[field.fieldKey, 'included']}
                        initialValue={index_ === 0 ? !!openingHours : true}
                      >
                        <StyledCheckbox />
                      </Form.Item>
                      <Description>
                        {intl.formatMessage({
                          id: `openingHours.weekday.${weekday}`,
                        })}
                      </Description>
                    </StyledColCheckbox>
                    <StyledCol span={12}>
                      <Form.Item
                        noStyle
                        name={[field.fieldKey, 'openingHours']}
                      >
                        <StyledTimeRangePicker
                          showNow={false}
                          showTime={showTime}
                          allowClear={false}
                          format={format}
                          minuteStep={30}
                          placeholder={placeholder}
                          onChange={values => {
                            onRangeChange(values, weekday, index_);
                          }}
                        />
                      </Form.Item>
                    </StyledCol>
                    <StyledCol span={4}>
                      <ActionItems
                        add={add}
                        remove={remove}
                        index={index_}
                        isHovered={isHovered}
                      />
                    </StyledCol>
                  </StyledRow>
                )}
              </WithHover>
            ))}
          </>
        );
      }}
    </Form.List>
  );
};
