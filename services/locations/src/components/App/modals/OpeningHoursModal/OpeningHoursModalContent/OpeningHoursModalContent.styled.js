import { Form } from 'antd';
import styled from 'styled-components';

export const Wrapper = styled.div`
  max-height: calc(100vh - 200px);
  overflow: auto;
  background-color: #f3f5f7;
  padding: 30px 24px;
`;

export const Headline = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 24px;
`;

export const StyledForm = styled(Form)`
  margin-top: 16px;
`;
