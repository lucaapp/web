import moment from 'moment';

const FORMAT_24_HOUR = 'hh:mm';
const FORMAT_12_HOUR_MERIDIAN = 'HH:mm A';
const FORMAT_12_HOUR = 'HH:mm';
const FORMAT_24_HOUR_MERIDIAN = 'hh:mm A';

const PLACEHOLDER_24HOURS = ['10:00', '18:00'];
const PLACEHOLDER_12HOURS = ['10:00 AM', '06:00 PM'];

const formatTimeBasedOnLocales = (currentLanguage, time) => {
  switch (currentLanguage) {
    case 'de':
      return moment(time, 'HH:mm');
    case 'en':
      return moment(time, 'hh:mm A');
    default:
      return moment(time, 'hh:mm A');
  }
};

export const showTimeByLang = currentLanguage => {
  switch (currentLanguage) {
    case 'de':
      return {
        format: FORMAT_24_HOUR,
        use12Hours: false,
      };
    default:
      return {
        format: FORMAT_12_HOUR_MERIDIAN,
        use12Hours: true,
      };
  }
};

export const formatTime = currentLanguage => {
  switch (currentLanguage) {
    case 'de':
      return FORMAT_12_HOUR;
    default:
      return FORMAT_24_HOUR_MERIDIAN;
  }
};

export const getTimePlaceholder = currentLanguage => {
  switch (currentLanguage) {
    case 'de':
      return PLACEHOLDER_24HOURS;
    default:
      return PLACEHOLDER_12HOURS;
  }
};

export const transformOpeningHoursOfDaysToInitialValues = (
  openingHours,
  currentLanguage
) => {
  return !openingHours || openingHours.length === 0
    ? [
        {
          openingHours: [],
          included: false,
        },
      ]
    : openingHours.map(openingHour => {
        return {
          openingHours: openingHour.openingHours.map(time => {
            return formatTimeBasedOnLocales(currentLanguage, time);
          }),
          included: openingHour.included,
        };
      });
};
