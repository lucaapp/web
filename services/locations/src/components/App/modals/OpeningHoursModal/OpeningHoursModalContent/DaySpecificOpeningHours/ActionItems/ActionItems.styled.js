import BaseIcon from '@ant-design/icons';
import styled, { css } from 'styled-components';

const sharedIconStyle = css`
  visibility: ${({ isVisible }) => (isVisible ? 'visible' : 'hidden')};
  margin-right: 16px;
  cursor: pointer;
`;
export const StyledRemoveIcon = styled(BaseIcon)`
  ${sharedIconStyle};
  font-size: 16px;
`;
export const StyledAddIcon = styled(BaseIcon)`
  ${sharedIconStyle};
  font-size: 24px;
`;
