import moment from 'moment';

export const initialOpeningHours = {
  mon: null,
  tue: null,
  wed: null,
  thu: null,
  fri: null,
  sat: null,
  sun: null,
};
export const defaultOpeningHours = [
  moment('10:00', 'HH:mm').format('HH:mm'),
  moment('18:00', 'HH:mm').format('HH:mm'),
];

export const weekdays = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

export const cleanseOpeningHoursFormValues = formValues => {
  const cleansedOpeningHours = {};

  for (const [weekday, values] of Object.entries(formValues)) {
    const cleansedFormValues = values.flatMap(openingHour => {
      return {
        openingHours:
          openingHour.openingHours.length > 0
            ? openingHour.openingHours.map(time =>
                moment(time, 'HH:mm').format('HH:mm')
              )
            : defaultOpeningHours,
        included: openingHour.included,
      };
    });
    cleansedOpeningHours[weekday] = cleansedFormValues;
  }
  return cleansedOpeningHours;
};
