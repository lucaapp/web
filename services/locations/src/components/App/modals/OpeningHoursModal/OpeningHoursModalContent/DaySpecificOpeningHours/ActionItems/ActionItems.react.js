import React from 'react';
import { useIntl } from 'react-intl';
import { CrossIcon, PlusInCircleIcon } from 'assets/icons';

import { StyledRemoveIcon, StyledAddIcon } from './ActionItems.styled';

export const ActionItems = ({ add, remove, index, isHovered }) => {
  const intl = useIntl();

  return (
    <>
      <StyledRemoveIcon
        isVisible={index !== 0 && isHovered}
        title={intl.formatMessage({
          id: 'openingHours.weekday.remove',
        })}
        component={CrossIcon}
        onClick={() => remove(index)}
      />
      <StyledAddIcon
        isVisible={isHovered}
        title={intl.formatMessage({
          id: 'openingHours.weekday.add',
        })}
        component={PlusInCircleIcon}
        onClick={() =>
          add({
            openingHours: [],
            included: true,
          })
        }
      />
    </>
  );
};
