import { Checkbox, Col, Row, TimePicker } from 'antd';
import styled from 'styled-components';

export const Description = styled.span`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;

export const StyledCheckbox = styled(Checkbox)`
  margin: 0 12px 0 16px;

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: #50667c !important;
    border-color: #50667c !important;
  }
`;

export const StyledCol = styled(Col)`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const StyledColCheckbox = styled(Col)`
  visibility: ${({ $isVisible }) => ($isVisible ? 'visible' : 'hidden')};
`;

export const StyledRow = styled(Row)`
  align-items: center;
  padding: 12px 0;

  &:hover {
    background-color: white;
  }
`;
export const StyledTimeRangePicker = styled(TimePicker.RangePicker)`
  .ant-picker-input {
    input::placeholder {
      color: #000;
      opacity: 1;
    }
  }
`;
