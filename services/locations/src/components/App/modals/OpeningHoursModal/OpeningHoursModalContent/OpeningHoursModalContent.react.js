import React, { useCallback } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';
import isEqual from 'lodash/isEqual';
import { useQueryClient } from 'react-query';

import { updateOpeningHours } from 'network/api';

import { QUERY_KEYS, useNotifications } from 'components/hooks';
import { PrimaryButton } from 'components/general';
import {
  ButtonWrapper,
  Headline,
  Wrapper,
  StyledForm,
} from './OpeningHoursModalContent.styled';
import { DaySpecificOpeningHours } from './DaySpecificOpeningHours';
import {
  cleanseOpeningHoursFormValues,
  initialOpeningHours,
  weekdays,
} from './OpeningHoursModalContent.helper';

export const OpeningHoursModalContent = ({ location, onClose }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const queryClient = useQueryClient();
  const { errorMessage } = useNotifications();

  const openingHours = location.openingHours
    ? location.openingHours
    : initialOpeningHours;

  const errorNotification = useCallback(
    () => errorMessage('notification.updateOpeningHours.error'),
    [errorMessage]
  );

  const onFinish = async values => {
    const cleansedOpeningHours = cleanseOpeningHoursFormValues(values);
    const openingHoursChanged = !isEqual(openingHours, cleansedOpeningHours);

    if (openingHoursChanged) {
      try {
        await updateOpeningHours(location.uuid, {
          openingHours: cleansedOpeningHours,
        });

        queryClient.invalidateQueries([QUERY_KEYS.LOCATION, location.uuid]);
      } catch {
        errorNotification();
      }
    }

    onClose();
  };

  const onRangeChange = (newValues, weekday, index) => {
    const formValues = form.getFieldsValue();
    const valuesWeekdayUpdated = formValues[weekday];

    valuesWeekdayUpdated[index] = {
      included: true,
      openingHours: newValues,
    };

    form.setFieldsValue({
      [weekday]: valuesWeekdayUpdated,
    });
  };

  return (
    <>
      <Headline data-cy="openingHoursModalHeadline">
        {intl.formatMessage({ id: 'openingHours.modal.headline' })}
      </Headline>
      <StyledForm form={form} onFinish={onFinish}>
        <Wrapper>
          {weekdays.map(weekday => (
            <DaySpecificOpeningHours
              key={weekday}
              openingHours={openingHours[weekday]}
              weekday={weekday}
              onRangeChange={onRangeChange}
            />
          ))}
        </Wrapper>
        <ButtonWrapper>
          <PrimaryButton htmlType="submit">
            {intl.formatMessage({ id: 'openingHours.modal.button.save' })}
          </PrimaryButton>
        </ButtonWrapper>
      </StyledForm>
    </>
  );
};
