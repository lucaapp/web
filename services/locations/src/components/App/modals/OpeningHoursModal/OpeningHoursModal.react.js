import React from 'react';
import { Modal } from 'antd';
import { createGlobalStyle } from 'styled-components';

// Constants
import { zIndex } from 'constants/layout';

import { OpeningHoursModalContent } from './OpeningHoursModalContent';

export const OpeningHoursModal = ({ onClose, location }) => {
  const GlobalModalStyle = createGlobalStyle`
  .noHeader .ant-modal-title {
    display: none;
  }

  .noHeader .ant-steps {
    display: none;
  }
`;

  return (
    <>
      <GlobalModalStyle />
      <Modal
        className="noHeader"
        visible
        zIndex={zIndex.modalArea}
        onCancel={onClose}
        centered
        width="600px"
        footer={null}
      >
        <OpeningHoursModalContent location={location} onClose={onClose} />
      </Modal>
    </>
  );
};
