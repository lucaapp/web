import React from 'react';
import { useIntl } from 'react-intl';
import { CheckIcon } from 'assets/icons';
import { PrimaryButton } from 'components/general';
import { useModalContext } from 'components/general/Modal/useModalContext';
import { Confirmation, Wrapper } from './ConfirmationPrivateKey.styled';

export const ConfirmationPrivateKey = () => {
  const intl = useIntl();
  const { closeModal } = useModalContext();

  return (
    <Wrapper>
      <CheckIcon />
      <Confirmation data-cy="notification-privateKeyUploadSuccess">
        {intl.formatMessage({
          id: 'confirmationPrivateKey',
        })}
      </Confirmation>
      <PrimaryButton onClick={closeModal} data-cy="finish">
        {intl.formatMessage({
          id: 'modal.registerOperator.finish',
        })}
      </PrimaryButton>
    </Wrapper>
  );
};
