import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 0 32px 32px 32px;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 32px;
`;
