import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { notification } from 'antd';
import { updateLocation } from 'network/api';

import { getFormattedPhoneNumber } from 'utils/formatters';
import { useEditProfileSchema } from 'components/general/Form/hooks';
import { StyledDivider } from 'components/App/GroupSettings/GroupProfile/SettingsOverview/SettingsOverview.styled';
import {
  ProfileSection,
  useGetDisplayFields,
} from 'components/general/EditLocationsProfile/ProfileSection';
import { LocationCard } from 'components/general';
import { AddressSection } from 'components/general/EditLocationsProfile/AddressSection';
import { useQueryClient } from 'react-query';
import { QUERY_KEYS, useNotifications } from 'components/hooks';
import { Wrapper } from './SettingsOverview.styled';

export const SettingsOverview = ({ location }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const { errorMessage } = useNotifications();
  const [isEditProfileMode, setIsEditProfileMode] = useState(false);
  const [isLocationNameDisabled] = useState(!location.name);
  const { locationSchema } = useEditProfileSchema(isLocationNameDisabled);

  const profileFields = useGetDisplayFields(null, location, 'location');

  const handleServerError = () => {
    errorMessage('notification.updateLocation.error');
  };

  const handleResponse = response => {
    switch (response.status) {
      case 200:
        notification.success({
          message: (
            <div data-cy="notification-updateLocationSuccess">
              {intl.formatMessage({
                id: 'notification.updateLocation.success',
              })}
            </div>
          ),
          className: 'editLocationSuccess',
        });
        queryClient.invalidateQueries(QUERY_KEYS.GROUPS);
        break;
      case 409:
        errorMessage('error.locationName.exist');
        break;
      default:
        handleServerError();
        break;
    }
  };

  const handleSubmit = ({ name, phone }) => {
    const formattedPhoneNumber = getFormattedPhoneNumber(phone);
    const formattedLocationName = name?.trim();

    if (
      formattedLocationName === location.name &&
      formattedPhoneNumber === location.phone
    ) {
      setIsEditProfileMode(false);
      return;
    }

    updateLocation({
      locationId: location.uuid,
      data: {
        phone: formattedPhoneNumber,
        locationName:
          location.name === null ? undefined : formattedLocationName,
      },
    })
      .then(response => {
        queryClient.invalidateQueries([QUERY_KEYS.LOCATION, location.uuid]);
        handleResponse(response);
        setIsEditProfileMode(false);
      })
      .catch(() => {
        handleServerError();
      });
  };

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'profile.overview' })}
    >
      <Wrapper>
        <ProfileSection
          location={location}
          isEditProfileMode={isEditProfileMode}
          setIsEditProfileMode={setIsEditProfileMode}
          onSubmit={handleSubmit}
          validationSchema={locationSchema}
          profileFields={profileFields}
          disabled={isLocationNameDisabled}
          placeholder={
            !location?.name
              ? intl.formatMessage({ id: 'location.defaultName' })
              : ''
          }
          dataCy="editLocation"
        />
        <StyledDivider />
        <AddressSection location={location} dataCy="editAddress" />
      </Wrapper>
    </LocationCard>
  );
};
