import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { notification, Popconfirm } from 'antd';
import { DangerButton, LocationCard } from 'components/general';
import { useQueryClient } from 'react-query';
import { useHistory } from 'react-router';
import { QuestionCircleOutlined } from '@ant-design/icons';

import { deleteLocation } from 'network/api';

import { BASE_GROUP_ROUTE, BASE_LOCATION_ROUTE } from 'constants/routes';

import {
  QUERY_KEYS,
  useGetGroup,
  useGetPaymentActive,
  useNotifications,
} from 'components/hooks';
import { PasswordModal } from 'components/App/modals/PasswordModal';

import { Wrapper, ButtonWrapper, Info } from './DeleteLocation.styled';

export const DeleteLocation = ({ location }) => {
  const intl = useIntl();
  const history = useHistory();
  const queryClient = useQueryClient();
  const { errorMessage } = useNotifications();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const { isLoading, error, data: group } = useGetGroup(location.groupId);
  const { data: paymentStatus } = useGetPaymentActive(location.uuid);

  const handleError = errorString => {
    errorMessage(errorString);
  };

  const onDelete = password => {
    deleteLocation(location.uuid, { password })
      .then(response => {
        if (response.status === 204) {
          setIsModalOpen(false);
          notification.success({
            message: intl.formatMessage({
              id: 'notification.deleteLocation.success',
            }),
            className: 'successDeletedNotification',
          });
          queryClient.invalidateQueries(QUERY_KEYS.GROUPS);
          history.push(
            `${BASE_GROUP_ROUTE}${location.groupId}${BASE_LOCATION_ROUTE}${
              group.locations.find(entry => !entry.name).uuid
            }`
          );
        } else if (response.status === 403) {
          handleError('notification.deleteLocation.error.incorrectPassword');
        } else {
          handleError('notification.deleteLocation.error');
          setIsModalOpen(false);
        }
      })
      .catch(() => {
        setIsModalOpen(false);
        handleError('notification.deleteLocation.error');
      });
  };

  if (error || isLoading) return null;

  const paymentActive = !!paymentStatus?.paymentActive;

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({
        id: paymentActive
          ? 'settings.location.delete.paymentEnabled'
          : 'settings.location.delete',
      })}
    >
      <Wrapper>
        <Info>
          {intl.formatMessage({
            id: paymentActive
              ? 'settings.location.delete.info.paymentEnabled'
              : 'settings.location.delete.info',
          })}
        </Info>
        <ButtonWrapper>
          <Popconfirm
            placement="topLeft"
            onConfirm={() => setIsModalOpen(true)}
            title={intl.formatMessage({
              id: paymentActive
                ? 'location.delete.confirmText.paymentEnabled'
                : 'location.delete.confirmText',
            })}
            okText={intl.formatMessage({
              id: 'location.delete.confirmButton',
            })}
            okButtonProps={{ 'data-cy': 'deleteLocationConfirmButton' }}
            cancelText={intl.formatMessage({
              id: 'generic.cancel',
            })}
            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
          >
            <DangerButton data-cy="deleteLocation">
              {intl.formatMessage({ id: 'settings.location.delete.submit' })}
            </DangerButton>
          </Popconfirm>
        </ButtonWrapper>
      </Wrapper>
      {isModalOpen && (
        <PasswordModal
          callback={onDelete}
          onClose={() => setIsModalOpen(false)}
        />
      )}
    </LocationCard>
  );
};
