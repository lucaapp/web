import React from 'react';
import { useParams } from 'react-router-dom';

import { OpeningHours } from 'components/App/shared';
import { useGetLocationById } from 'components/hooks';
import { SettingsOverview } from './SettingsOverview';
import { DeleteLocation } from './DeleteLocation';
import { Wrapper } from './LocationSettings.styled';

export const LocationSettings = () => {
  const { locationId } = useParams();

  const { isError, isLoading, data: location } = useGetLocationById(locationId);

  if (isLoading || isError) return null;

  return (
    <Wrapper data-cy="areaProfile">
      <SettingsOverview location={location} />
      <OpeningHours locationId={location.uuid} />
      {location.name !== null && <DeleteLocation location={location} />}
    </Wrapper>
  );
};
