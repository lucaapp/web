import React from 'react';
import { useIntl } from 'react-intl';
import { Layout } from 'antd';

import { Content, NavigationButton, Sider } from 'components/general';

import { Links } from './Links';
import { ContactSection } from './ContactSection';

import { Wrapper, Header, CardWrapper } from './HelpCenter.styled';

export const HelpCenter = () => {
  const intl = useIntl();

  return (
    <Layout>
      <Sider>
        <NavigationButton />
      </Sider>
      <Content>
        <Wrapper data-cy="helpCenterPageWrapper">
          <Header data-cy="helpCenterTitle">
            {intl.formatMessage({ id: 'helpCenter.title' })}
          </Header>
          <CardWrapper>
            <Links />
            <ContactSection />
          </CardWrapper>
        </Wrapper>
      </Content>
    </Layout>
  );
};
