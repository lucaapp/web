import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { FAQ_LINK, VIDEOS_LINK } from 'constants/links';
import { LICENSES_ROUTE } from 'constants/routes';
import { Links } from './Links.react';

describe('Links Component', () => {
  const setup = async () => {
    const renderResult = render(<Links />);

    const linksSection = await screen.findByTestId('helpCenter-linksSection');
    const supportVideoLink = await screen.findByTestId('supportVideoLink');
    const faqLink = await screen.findByTestId('faqLink');
    const licenseLink = await screen.findByTestId('licenseLink');

    return {
      renderResult,
      linksSection,
      supportVideoLink,
      faqLink,
      licenseLink,
    };
  };

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });

  test('Check if links contain correct href and target attribute values', async () => {
    const {
      supportVideoLink,
      faqLink,
      toolkitLink,
      licenseLink,
    } = await setup();

    await (async () => {
      await supportVideoLink;
      await faqLink;
      await toolkitLink;
      await licenseLink;

      expect(supportVideoLink).toHaveAttribute('href', VIDEOS_LINK);
      expect(supportVideoLink).toHaveAttribute('target', '_blank');

      expect(faqLink).toHaveAttribute('href', FAQ_LINK);
      expect(faqLink).toHaveAttribute('target', '_blank');

      expect(licenseLink).toHaveAttribute('href', LICENSES_ROUTE);
      expect(licenseLink).toHaveAttribute('target', '_blank');
    });
  });
});
