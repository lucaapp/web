import React from 'react';
import { useIntl } from 'react-intl';
import { LICENSES_ROUTE } from 'constants/routes';
import { FAQ_LINK, VIDEOS_LINK } from 'constants/links';

import { ExternalIcon } from 'assets/icons';
import { Wrapper, LinkWrapper, LinkText, StyledIcon } from './Links.styled';

export const Links = () => {
  const intl = useIntl();
  const links = [
    {
      url: VIDEOS_LINK,
      intlId: 'profile.services.videos',
      testId: 'supportVideoLink',
    },
    { url: FAQ_LINK, intlId: 'profile.services.faq', testId: 'faqLink' },
    { url: LICENSES_ROUTE, intlId: 'license.license', testId: 'licenseLink' },
  ];

  return (
    <Wrapper data-cy="helpCenter-linksSection">
      {links.map(link => (
        <LinkWrapper key={link.intlId}>
          <LinkText
            href={link.url}
            target="_blank"
            rel="noopener noreferrer"
            data-cy={link.testId}
          >
            {intl.formatMessage({ id: link.intlId })}
            <StyledIcon component={ExternalIcon} />
          </LinkText>
        </LinkWrapper>
      ))}
    </Wrapper>
  );
};
