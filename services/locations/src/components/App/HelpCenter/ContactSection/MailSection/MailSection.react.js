import React from 'react';
import { useIntl } from 'react-intl';

import { ContactFormModal } from 'components/App/modals/ContactFormModal';
import { Modal, PrimaryButton } from 'components/general';
import { ArrowRightOutlined } from '@ant-design/icons';

import { LARGE_MODAL } from 'components/general/Modal';
import {
  Wrapper,
  Heading,
  Text,
  StyledMailOutlined,
} from './MailSection.styled';

export const MailSection = () => {
  const intl = useIntl();

  const modalOpenButton = (
    <PrimaryButton data-cy="helpCenter-openContactFormButton">
      {intl.formatMessage({ id: 'helpCenter.mail.buttonText' })}
      <ArrowRightOutlined />
    </PrimaryButton>
  );

  const modalContent = () => <ContactFormModal />;

  return (
    <Wrapper data-cy="helpCenter-mailSection">
      <StyledMailOutlined data-cy="helpCenter-mailIcon" />
      <Heading data-cy="helpCenter-mailSectionHeading">
        {intl.formatMessage({ id: 'helpCenter.mail.heading' })}
      </Heading>
      <Text data-cy="helpCenter-mailSectionText">
        {intl.formatMessage({ id: 'helpCenter.mail.text' })}
      </Text>
      <Modal
        content={modalContent}
        openButton={modalOpenButton}
        size={LARGE_MODAL}
      />
    </Wrapper>
  );
};
