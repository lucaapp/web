import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { act } from 'react-dom/test-utils';
import { MailSection } from './MailSection.react';

const mockOperator = {
  firstName: 'Some',
  lastName: 'Dude',
  email: 'some@dud.es',
  supportCode: 'WY7GK9THNFLM',
};

describe('Mail Section Component', () => {
  const setup = async () => {
    const renderResult = render(<MailSection />);

    const mailSection = await screen.findByTestId('helpCenter-mailSection');
    const mailIcon = await screen.findByTestId('helpCenter-mailIcon');
    const heading = await screen.findByTestId('helpCenter-mailSectionHeading');
    const headingText = screen.getByText('helpCenter.mail.heading');
    const text = await screen.findByTestId('helpCenter-mailSectionText');
    const mailSectionText = screen.getByText('helpCenter.mail.text');
    const openContactFormButton = await screen.findByTestId(
      'helpCenter-openContactFormButton'
    );

    return {
      renderResult,
      mailIcon,
      mailSection,
      heading,
      headingText,
      text,
      mailSectionText,
      openContactFormButton,
    };
  };

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });

  test('Check if operator object is passed to ContactFormModal', async () => {
    const { openContactFormButton } = await setup();

    await act(async () => {
      await openContactFormButton.click();
    });

    const contactFormOperatorName = screen.getByTestId(
      'contactFormOperatorName'
    );

    const contactFormOperatorEmail = screen.getByTestId(
      'contactFormOperatorEmail'
    );

    expect(contactFormOperatorName).toBeDefined();
    expect(contactFormOperatorName.textContent).toBe(
      `${mockOperator.firstName} ${mockOperator.lastName}`
    );

    expect(contactFormOperatorEmail).toBeDefined();
    expect(contactFormOperatorEmail.textContent).toBe(`${mockOperator.email}`);
  });
});
