import React, { useCallback, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { useHistory, useParams } from 'react-router';
import {
  useGetGroups,
  useGetPaymentEnabled,
  useOperator,
} from 'components/hooks';

import { WomanOpenArmsIcon } from 'assets/icons';

import {
  BASE_LOCATION_ROUTE,
  BASE_GROUP_ROUTE,
  BASE_GROUP_SETTINGS_ROUTE,
} from 'constants/routes';

import { ONBOARDING_STATUS } from 'constants/paymentOnboarding';
import { getOnboardingStatus } from 'network/payment';
import {
  Wrapper,
  EmptyGroupTitle,
  EmptyGroupSubTitle,
  StyledEmptyGroupIcon,
} from './EmptyGroup.styled';
import { CreateGroup } from './CreateGroup';

export const EmptyGroup = () => {
  const intl = useIntl();
  const history = useHistory();
  const { groupId } = useParams();

  const { data: operator } = useOperator();
  const { isLoading, error, data: groups } = useGetGroups();

  const {
    isLoading: isPaymentLoading,
    error: paymentError,
    data: paymentEnabled,
  } = useGetPaymentEnabled(operator.operatorId, {
    enabled: !!operator,
  });

  const navigateToBase = () => history.push(BASE_GROUP_ROUTE);

  const navigateToGeneralLocation = () =>
    history.push(
      `${BASE_GROUP_ROUTE}${groups[0].groupId}${BASE_LOCATION_ROUTE}${
        groups[0].locations.find(location => !location.name).uuid
      }`
    );

  const navigateToGroupSetting = () =>
    history.push(`${BASE_GROUP_SETTINGS_ROUTE}${groups[0].groupId}`);

  const redirectForGroupIdInParameters = () => {
    const baseLocationId = groups
      ?.find(group => group.groupId === groupId)
      ?.locations.find(location => !location.name)?.uuid;

    if (baseLocationId) {
      history.push(
        `${BASE_GROUP_ROUTE}${groupId}${BASE_LOCATION_ROUTE}${baseLocationId}`
      );
    }
  };

  const redirectsForNonPayOperators = () => {
    if (!groups.length) {
      navigateToBase();
      return;
    }
    if (groupId) {
      redirectForGroupIdInParameters();
      return;
    }
    navigateToGeneralLocation();
  };

  const redirectsForPayOperators = async () => {
    const groupOnboardingStatus = await getOnboardingStatus(
      groupId || groups[0].groupId
    );
    const groupHasActivatedPay =
      groupOnboardingStatus.status === ONBOARDING_STATUS.DONE;

    if (!groups.length) {
      navigateToBase();
      return;
    }
    if (groups.length && !groupHasActivatedPay) {
      navigateToGroupSetting();
      return;
    }
    if (groups.length && groupHasActivatedPay) {
      navigateToGeneralLocation();
      return;
    }
    if (groupId) {
      redirectForGroupIdInParameters();
      return;
    }
    navigateToGeneralLocation();
  };

  // eslint-disable-next-line complexity
  const checkForRedirect = useCallback(() => {
    const isPaymentForOperatorEnabled =
      paymentEnabled && paymentEnabled.paymentEnabled;

    if (!isPaymentForOperatorEnabled) {
      redirectsForNonPayOperators();
      return;
    }

    if (isPaymentForOperatorEnabled) {
      redirectsForPayOperators().catch(() => console.error);
      return;
    }

    navigateToGeneralLocation();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [groupId, groups, paymentEnabled]);

  useEffect(() => {
    if (!groups?.length) {
      return;
    }
    if (isLoading || error || isPaymentLoading || paymentError) {
      return;
    }

    checkForRedirect();
  }, [
    checkForRedirect,
    error,
    groups?.length,
    isLoading,
    isPaymentLoading,
    paymentError,
  ]);

  if (isLoading || error || isPaymentLoading || paymentError || groups.length)
    return null;

  return (
    <Wrapper>
      <EmptyGroupTitle>
        {intl.formatMessage({ id: 'emptyGroup.title' })}
      </EmptyGroupTitle>
      <EmptyGroupSubTitle>
        {intl.formatMessage({ id: 'emptyGroup.subtitle' })}
      </EmptyGroupSubTitle>
      <CreateGroup />
      <StyledEmptyGroupIcon component={WomanOpenArmsIcon} />
    </Wrapper>
  );
};
