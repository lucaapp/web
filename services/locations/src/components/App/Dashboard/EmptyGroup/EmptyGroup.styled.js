import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const Wrapper = styled.div`
  margin: 40px 44px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
`;

export const EmptyGroupTitle = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 34px;
  font-weight: 500;
  margin-bottom: 8px;
`;

export const EmptyGroupSubTitle = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 32px;
`;

export const StyledEmptyGroupIcon = styled(Icon)`
  display: flex;
  justify-content: center;
  font-size: 250px;
  margin: 80px 0 40px 0;
`;
