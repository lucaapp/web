import React, { useState } from 'react';
import { useIntl } from 'react-intl';

import { Modal, PrimaryButton } from 'components/general';
import { CreateGroupModal } from 'components/App/modals/CreateGroupModal';
import { ButtonWrapper } from './CreateGroup.styled';

export const CreateGroup = () => {
  const intl = useIntl();
  const [resetStepsTrigger, setResetStepsTrigger] = useState(false);

  const openModalButton = (
    <ButtonWrapper>
      <PrimaryButton data-cy="createGroup">
        {intl.formatMessage({ id: 'groupList.createGroup' })}
      </PrimaryButton>
    </ButtonWrapper>
  );

  const modalContent = () => (
    <CreateGroupModal resetStepsTrigger={resetStepsTrigger} />
  );

  return (
    <Modal
      content={modalContent}
      openButton={openModalButton}
      afterClose={() => setResetStepsTrigger(true)}
    />
  );
};
