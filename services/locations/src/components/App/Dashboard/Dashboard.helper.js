import { RESTAURANT_TYPE } from 'constants/locations';

export const getIsBusinessEmpty = operator =>
  !operator.businessEntityName ||
  !operator.businessEntityStreetName ||
  !operator.businessEntityStreetNumber ||
  !operator.businessEntityZipCode ||
  !operator.businessEntityCity;

export const findAnyRestaurantWithoutSubtype = groups => {
  const restaurantLocations = groups
    ?.map(group => {
      const restaurantType = group.locations.find(
        location => location.type === RESTAURANT_TYPE
      );

      if (restaurantType) {
        return {
          ...restaurantType,
          groupName: group.name,
        };
      }

      return null;
    })
    .filter(element => element);

  if (!restaurantLocations) return null;

  return restaurantLocations.find(
    restaurantLocation => !restaurantLocation.subtype
  );
};
