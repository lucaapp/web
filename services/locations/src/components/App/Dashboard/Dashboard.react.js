import React, { useCallback, useEffect, useState } from 'react';
import { Redirect, Route, Switch, useHistory } from 'react-router';
import { Layout } from 'antd';

import {
  LOGIN_ROUTE,
  BASE_GROUP_ROUTE,
  GROUP_ROUTE,
  LOCATION_ROUTE,
  MAGIC_LOGIN_ROUTE,
} from 'constants/routes';
import {
  useGetGroups,
  useGetPrivateKeySecret,
  useOperator,
} from 'components/hooks';
import {
  getIsOperatorFromMagicLogin,
  hasSeenPrivateKeyModal,
  hasSeenSubcategoryModal,
  setHasSeenPrivateKeyModal,
  setHasSeenSubcategoryModal,
  setIsOperatorFromMagicLogin,
} from 'utils/storage';
import { usePrivateKey, getRemainingDaysToUploadKey } from 'utils/privateKey';

import { PrivateKeyLoader } from 'components/PrivateKeyLoader';
import { GroupList } from 'components/App/GroupList';
import { RegisterOperatorModal } from 'components/App/modals/RegisterOperatorModal';
import { BusinessAddressModal } from 'components/App/modals/BusinessAddressModal';
import { Content, Modal, Sider } from 'components/general';

import { Location } from './Location';
import { EmptyGroup } from './EmptyGroup';
import { AccountDeletedView } from './AccountDeletedView';
import { SkipButton } from './Location/LocationOverview/TableAllocation/SkipButton.react';
import {
  getIsBusinessEmpty,
  findAnyRestaurantWithoutSubtype,
} from './Dashboard.helper';

import { RestauranteSubcategoryModal } from '../modals/RestauranteSubcategoryModal';

const KEY_UPLOAD_THRESHOLD = 4;

export const Dashboard = () => {
  const history = useHistory();
  const { data: operator } = useOperator();

  const [modalContent, setModalContent] = useState(null);
  const [isModalClosable, setIsModalClosable] = useState(true);
  const [
    handleLocalStorageKey,
    setHandleLocalStorageKey,
  ] = useState(() => () => {});

  const {
    data: groups,
    error: groupsError,
    isLoading: isGroupsLoading,
  } = useGetGroups();

  const {
    data: privateKeySecret,
    isLoading: isPrivateKeyLoading,
    error: privateKeyError,
  } = useGetPrivateKeySecret();

  const [privateKey] = usePrivateKey(privateKeySecret);

  const { isTrusted: isOperatorTrusted, lastSeenPrivateKey } = operator;

  const remainingDaysToUploadKey = getRemainingDaysToUploadKey(
    lastSeenPrivateKey
  );

  const shouldOpenKeyLoader =
    (!privateKey && !isPrivateKeyLoading) ||
    remainingDaysToUploadKey > KEY_UPLOAD_THRESHOLD;

  const hasPublicKey =
    !!operator?.publicKey ||
    (!privateKeySecret && !isPrivateKeyLoading) ||
    privateKeyError;

  const openRegisterOperatorModal = useCallback(() => {
    setIsModalClosable(true);
    setModalContent(() => (
      <RegisterOperatorModal privateKeySecret={privateKeySecret} />
    ));

    setHandleLocalStorageKey(() => () => setHasSeenPrivateKeyModal(true));
  }, [privateKeySecret]);

  const openPrivateKeyModal = useCallback(() => {
    setIsModalClosable(true);
    setModalContent(() => (
      <PrivateKeyLoader
        showSuccessScreen
        footerItem={remainingDaysToUploadKey > 0 && <SkipButton />}
        remainingDaysToUploadKey={remainingDaysToUploadKey}
        privateKeySecret={privateKeySecret}
      />
    ));

    setHandleLocalStorageKey(() => () => setHasSeenPrivateKeyModal(true));
  }, [privateKeySecret, remainingDaysToUploadKey]);

  const openBusinessFormModal = useCallback(oper => {
    setIsModalClosable(false);
    setModalContent(() => <BusinessAddressModal operator={oper} />);
  }, []);

  const openRestaurantSubCategoryModal = useCallback(restaurant => {
    setIsModalClosable(true);
    setModalContent(() => (
      <RestauranteSubcategoryModal location={restaurant} />
    ));

    setHandleLocalStorageKey(() => () => setHasSeenSubcategoryModal(true));
  }, []);

  // eslint-disable-next-line complexity
  useEffect(() => {
    if (privateKeyError || isPrivateKeyLoading || isGroupsLoading) {
      return;
    }

    if (!operator) {
      history.push(LOGIN_ROUTE);
    }

    if (!groups || groupsError) {
      history.push(BASE_GROUP_ROUTE);
    }

    const isOperatorFromMagicLoginRoute = document.referrer.includes(
      MAGIC_LOGIN_ROUTE
    );

    if (isOperatorFromMagicLoginRoute) {
      setIsOperatorFromMagicLogin(true);
    }

    if (
      !hasPublicKey &&
      !getIsOperatorFromMagicLogin() &&
      !hasSeenPrivateKeyModal()
    ) {
      openRegisterOperatorModal();
    }

    if (
      shouldOpenKeyLoader &&
      hasPublicKey &&
      !getIsOperatorFromMagicLogin() &&
      !hasSeenPrivateKeyModal()
    ) {
      openPrivateKeyModal();
    }

    if (getIsBusinessEmpty(operator)) {
      openBusinessFormModal(operator);
    }

    if (findAnyRestaurantWithoutSubtype(groups) && !hasSeenSubcategoryModal()) {
      openRestaurantSubCategoryModal(findAnyRestaurantWithoutSubtype(groups));
    }
  }, [
    groups,
    groupsError,
    hasPublicKey,
    history,
    isGroupsLoading,
    isPrivateKeyLoading,
    openBusinessFormModal,
    openPrivateKeyModal,
    openRegisterOperatorModal,
    openRestaurantSubCategoryModal,
    operator,
    privateKeyError,
    shouldOpenKeyLoader,
  ]);

  if (operator.deletedAt) {
    return <AccountDeletedView />;
  }

  return (
    <Layout>
      {modalContent && (
        <Modal
          content={modalContent}
          visible
          closable={isModalClosable}
          afterClose={() => handleLocalStorageKey()}
        />
      )}
      <Sider>
        <GroupList />
      </Sider>
      <Content>
        <Switch>
          <Route path={LOCATION_ROUTE}>
            <Location isOperatorTrusted={isOperatorTrusted} />
          </Route>
          <Route path={GROUP_ROUTE}>
            <EmptyGroup />
          </Route>
          <Route path={BASE_GROUP_ROUTE}>
            <EmptyGroup />
          </Route>
          <Redirect to={BASE_GROUP_ROUTE} />
        </Switch>
      </Content>
    </Layout>
  );
};
