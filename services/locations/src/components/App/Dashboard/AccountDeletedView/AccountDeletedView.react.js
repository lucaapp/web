import React from 'react';
import { Layout } from 'antd';

import { Content, Sider } from 'components/general';
import { AccountReactivation } from './AccountReactivation';

export const AccountDeletedView = () => {
  return (
    <Layout>
      <Sider />
      <Content>
        <AccountReactivation />
      </Content>
    </Layout>
  );
};
