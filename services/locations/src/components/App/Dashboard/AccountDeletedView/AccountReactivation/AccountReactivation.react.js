import React from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { message } from 'antd';

import { undoAccountDeletion } from 'network/api';

import { PrimaryButton } from 'components/general';

import { QUERY_KEYS, useOperator } from 'components/hooks';
import { calculateDaysRemaining } from 'components/App/Profile/ProfileContent/AccountDeletion/AccountDeletion.helper';
import { ContentWrapper, Heading } from './AccountReactivation.styled';

export const AccountReactivation = () => {
  const { data: operator } = useOperator();

  const daysRemaining = calculateDaysRemaining(operator);

  const intl = useIntl();
  const queryClient = useQueryClient();

  const restoreAccount = () => {
    undoAccountDeletion()
      .then(() => {
        queryClient.invalidateQueries(QUERY_KEYS.ME);
        message.success(
          intl.formatMessage({ id: 'account.delete.reactivation.success' })
        );
      })
      .catch(() => message.error(intl.formatMessage({ id: 'error.headline' })));
  };

  return (
    <ContentWrapper data-cy="deletionRequested">
      <Heading>
        {intl.formatMessage({ id: 'account.delete.heading.inProgress2' })}
      </Heading>
      <p>
        {intl.formatMessage(
          { id: 'account.delete.info.inProgress' },
          { days: <strong>{daysRemaining}</strong> }
        )}
      </p>
      <PrimaryButton onClick={restoreAccount} data-cy="restoreAccount">
        {intl.formatMessage({ id: 'account.delete.reactivate' })}
      </PrimaryButton>
    </ContentWrapper>
  );
};
