import React, { useCallback, useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import Icon from '@ant-design/icons';

import {
  createAdditionalData,
  deleteAdditionalData,
  updateAdditionalData,
} from 'network/api';
import {
  useGetAdditionalData,
  QUERY_KEYS,
  useNotifications,
} from 'components/hooks';
import { isValidCharacter } from 'utils/validators';
import { AddCircleBlueIcon } from 'assets/icons';
import {
  CardSection,
  CardSectionDescription,
  LocationCard,
} from 'components/general/LocationCard';
import {
  StyledFooter,
  StyledIconButton,
  StyledIconButtonText,
} from './CheckInQuery.styled';
import { AdditionalData } from './AdditionalData';

export const CheckInQuery = ({ location }) => {
  const intl = useIntl();
  const { errorMessage } = useNotifications();
  const queryClient = useQueryClient();
  const [additionalData, setAdditionalData] = useState([]);
  const { isLoading, error, data } = useGetAdditionalData(location.uuid);

  const invalidateQueries = useCallback(
    () => queryClient.invalidateQueries(QUERY_KEYS.ADDITIONAL_DATA),
    [queryClient]
  );

  const addAdditionalData = useCallback(() => {
    createAdditionalData(location.uuid, {
      key: '',
    })
      .then(response => {
        if (response.status === 403) {
          errorMessage(
            'settings.location.checkin.additionalData.notification.maxReachedError.desc'
          );
        }
        invalidateQueries();
      })
      .catch(invalidateQueries);
  }, [location.uuid, invalidateQueries, errorMessage]);

  const removeAdditionalData = useCallback(
    id => {
      deleteAdditionalData(id).then(invalidateQueries).catch(invalidateQueries);
    },
    [invalidateQueries]
  );

  useEffect(() => {
    if (isLoading || error) return;

    setAdditionalData(data.additionalData);
  }, [isLoading, data, error, additionalData.length]);

  const handleUpdateAdditionalData = (additionalDataId, value) => {
    updateAdditionalData(additionalDataId, {
      key: value,
      isRequired: true,
    })
      .then(() => {
        const newStateAdditionalInfo = additionalData.map(information =>
          information.uuid === additionalDataId
            ? { ...information, key: value }
            : information
        );

        setAdditionalData(newStateAdditionalInfo);
      })
      .catch(invalidateQueries);
  };

  const onBlur = (event, additionalDataId) => {
    const { value } = event.target;
    const keyAlreadyExists = additionalData.some(
      additionalInfo => additionalInfo.key === value
    );

    if (value.length === 0) {
      removeAdditionalData(additionalDataId);
      return;
    }

    if (
      value.startsWith('_') ||
      !value.trim() ||
      !isValidCharacter(value.trim()) ||
      keyAlreadyExists
    ) {
      return;
    }

    handleUpdateAdditionalData(additionalDataId, value);
  };

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'settings.location.checkin.headline' })}
      testId="additionalData"
    >
      <CardSection isLast>
        <CardSectionDescription>
          {intl.formatMessage({
            id: 'settings.location.checkin.additionalData.description',
          })}
        </CardSectionDescription>
        {additionalData.map((information, index) => (
          <AdditionalData
            dataCy={`checkInQuestionInputField-${index + 1}`}
            key={information.uuid}
            information={information}
            additionalData={additionalData}
            removeAdditionalData={removeAdditionalData}
            onBlur={onBlur}
          />
        ))}
        <StyledFooter>
          <StyledIconButton
            data-cy="addCheckInQuestionButton"
            onClick={addAdditionalData}
          >
            <Icon component={AddCircleBlueIcon} style={{ fontSize: 16 }} />
            <StyledIconButtonText>
              {intl.formatMessage({
                id: 'settings.location.checkin.additionalData.add',
              })}
            </StyledIconButtonText>
          </StyledIconButton>
        </StyledFooter>
      </CardSection>
    </LocationCard>
  );
};
