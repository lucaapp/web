import isFinite from 'lodash/isFinite';

import { MIN_TABLE_NUMBER, MAX_TABLE_NUMBER } from 'constants/tableNumber';

export const translate = (key, intl) =>
  intl.formatMessage({
    id: `settings.location.tables.${key}`,
  });

export const isValidNumberOfTables = numberOfTables => {
  return (
    isFinite(numberOfTables) &&
    numberOfTables >= MIN_TABLE_NUMBER &&
    numberOfTables <= MAX_TABLE_NUMBER
  );
};
