import styled from 'styled-components';
import { Form, InputNumber } from 'antd';

export const StyledForm = styled(Form)`
  width: 100%;
  margin-top: 16px;
`;

export const StyledFormItem = styled(Form.Item)`
  width: 50%;
`;

export const StyledInputNumber = styled(InputNumber)`
  width: 99%;
`;
export const ButtonWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;
