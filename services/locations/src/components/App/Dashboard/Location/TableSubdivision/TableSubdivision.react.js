import React, { useCallback, useState } from 'react';
import { useIntl } from 'react-intl';

import { useQueryClient } from 'react-query';
import { PrimaryButton, Switch } from 'components/general';
import {
  QUERY_KEYS,
  useGetLocationTables,
  useNotifications,
} from 'components/hooks';
import { MIN_TABLE_NUMBER, MAX_TABLE_NUMBER } from 'constants/tableNumber';
import { createLocationTable, deleteLocationTable } from 'network/api';
import { useTableNumberValidator } from 'components/hooks/useValidators';
import {
  CardSection,
  CardSectionTitle,
  LocationCard,
  TitleContainer,
} from 'components/general/LocationCard';
import { inputOnlyNumbersFilter } from 'utils/inputFilter';
import { StyledSwitchContainer } from '../GenerateQRCodes/GenerateQRCodes.styled';
import { translate, isValidNumberOfTables } from './TableSubdivision.helper';
import {
  StyledForm,
  StyledFormItem,
  StyledInputNumber,
  ButtonWrapper,
} from './TableSubdivision.styled';

export const TableSubdivision = ({ location }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const { errorMessage } = useNotifications();
  const [changeInProgress, setChangeInProgress] = useState(false);
  const validateNumberOfTables = useTableNumberValidator(true);

  const { error, data: locationTables, isLoading } = useGetLocationTables(
    location.uuid
  );

  const createLocationTables = useCallback(
    async (locationId, tableCount = null) => {
      if (tableCount <= 0) return;
      const tables = Array.from(Array(tableCount).keys());
      const apiTables = [];
      tables.forEach(() => {
        apiTables.push({ customName: '' });
      });
      try {
        await createLocationTable(locationId, { tables: apiTables });
        queryClient.invalidateQueries([QUERY_KEYS.LOCATION_TABLES, locationId]);
      } catch {
        errorMessage('settings.location.tables.save.error');
      }
    },
    [queryClient, errorMessage]
  );

  const deleteLocationTables = useCallback(
    async (numberOfTablesToKeep = null) => {
      const promises = [];
      if (!numberOfTablesToKeep) {
        locationTables.map(table =>
          promises.push(
            deleteLocationTable(location.uuid, table.internalNumber)
          )
        );
      } else {
        locationTables.map(table =>
          table.internalNumber > numberOfTablesToKeep
            ? promises.push(
                deleteLocationTable(location.uuid, table.internalNumber)
              )
            : null
        );
      }
      await Promise.all(promises);
      queryClient.invalidateQueries([
        QUERY_KEYS.LOCATION_TABLES,
        location.uuid,
      ]);
    },
    [location, queryClient, locationTables]
  );

  const updateCurrentLocationTablesCount = useCallback(
    async numberOfTables => {
      if (numberOfTables > locationTables.length) {
        const tablesToCreate = numberOfTables - locationTables.length;
        await createLocationTables(location.uuid, tablesToCreate);
      } else {
        const numberOfTablesToKeep = numberOfTables;
        await deleteLocationTables(numberOfTablesToKeep);
      }
    },
    [locationTables, location, createLocationTables, deleteLocationTables]
  );

  const handleNumberOfTables = useCallback(
    async ({ tableCount }) => {
      setChangeInProgress(true);
      if (!isValidNumberOfTables(tableCount)) return;
      await updateCurrentLocationTablesCount(Number(tableCount));
      setChangeInProgress(false);
    },
    [updateCurrentLocationTablesCount]
  );

  const toggleForm = () => {
    if (locationTables.length <= 0) {
      createLocationTables(location.uuid, 10);
    } else {
      deleteLocationTables();
    }
  };

  if (error || isLoading) return null;

  return (
    <LocationCard title={translate('headline', intl)} testId="tableSubdivision">
      <CardSection isLast>
        <CardSectionTitle>
          <TitleContainer data-cy="tableSubdivisionDescription">
            {translate('description', intl)}
          </TitleContainer>
          <StyledSwitchContainer>
            <Switch
              data-cy="tableSubdivisionToggle"
              checked={locationTables.length > 0}
              onChange={toggleForm}
            />
          </StyledSwitchContainer>
        </CardSectionTitle>
        {locationTables.length > 0 && (
          <StyledForm
            initialValues={{ tableCount: locationTables.length }}
            onFinish={handleNumberOfTables}
          >
            <StyledFormItem
              name="tableCount"
              label={translate('input', intl)}
              rules={validateNumberOfTables}
            >
              <StyledInputNumber
                onKeyDown={inputOnlyNumbersFilter}
                data-cy="dashboard-tableCountInputField"
                min={MIN_TABLE_NUMBER}
                max={MAX_TABLE_NUMBER}
                placeholder={translate('input.placeholder', intl)}
              />
            </StyledFormItem>
            <ButtonWrapper>
              <PrimaryButton
                htmlType="submit"
                data-cy="saveTablesNumberButton"
                loading={changeInProgress}
              >
                {intl.formatMessage({
                  id: 'location.save',
                })}
              </PrimaryButton>
            </ButtonWrapper>
          </StyledForm>
        )}
      </CardSection>
    </LocationCard>
  );
};
