import React from 'react';
import { useIntl } from 'react-intl';
import { InformationIcon, StyledTooltip } from 'components/general';
import {
  CardSection,
  CardSectionDescription,
  LocationCard,
} from 'components/general/LocationCard';
import { LocationPreview } from './LocationPreview';
import { ExternalLinks } from './ExternalLinks';
import { StyledFooter, StyledTitle } from './ExternalLinksSection.styled';

export const ExternalLinksSection = ({ location }) => {
  const intl = useIntl();

  return (
    <LocationCard
      testId="websiteMenuAndLinks"
      title={
        <StyledTitle>
          {intl.formatMessage({
            id: 'settings.location.externalLinks.headline',
          })}
          <StyledTooltip
            title={intl.formatMessage({
              id: 'settings.location.externalLinks.previewTooltip',
            })}
          />
        </StyledTitle>
      }
    >
      <CardSection isLast>
        <CardSectionDescription>
          {intl.formatMessage({
            id: 'settings.location.externalLinks.description',
          })}
          {intl.formatMessage({
            id: 'externalLinks.preview.drawer.subtitle',
          })}
          <StyledTooltip
            title={intl.formatMessage({
              id: 'settings.location.externalLinks.tooltip',
            })}
          >
            <InformationIcon data-cy="linkContentInformationIcon" />
          </StyledTooltip>
        </CardSectionDescription>
        <StyledFooter>
          <ExternalLinks location={location} />
          <LocationPreview location={location} />
        </StyledFooter>
      </CardSection>
    </LocationCard>
  );
};
