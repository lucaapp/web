import styled from 'styled-components';

export const Wrapper = styled.div`
  flex: 1;
  width: 100%;
  height: 100%;
  padding: 32px;
  display: flex;
  align-items: center;
  box-sizing: border-box;
  flex-direction: column;
`;

export const PreviewIFrame = styled.iframe`
  zoom: 0.1;
  width: 133%;
  height: 133%;
  border: none;
  transform: scale(0.75);
  transform-origin: top left;
`;

export const ScreenWrapper = styled.div`
  position: relative;
`;

export const SpinWrapper = styled.div`
  display: flex;
  justify-content: center;
  height: 100%;
  align-items: center;
`;
