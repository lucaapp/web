export const getFormFields = () => [
  {
    id: 'menu',
    label: 'externalLinks.menu',
    placeholder: 'externalLinks.menu.placeholder',
  },
  {
    id: 'website',
    label: 'externalLinks.website',
    placeholder: 'externalLinks.website.placeholder',
  },
  {
    id: 'schedule',
    label: 'externalLinks.schedule',
    placeholder: 'externalLinks.schedule.placeholder',
  },
  {
    id: 'map',
    label: 'externalLinks.map',
    placeholder: 'externalLinks.map.placeholder',
  },
  {
    id: 'general',
    label: 'externalLinks.general',
    placeholder: 'externalLinks.general.placeholder',
  },
];

export const getFieldsValaueChanged = (currentValues, initialValues) => {
  const currentValuesKeys = Object.keys(currentValues);

  const changes = currentValuesKeys.map(currentValuesKey => {
    if (currentValues[currentValuesKey] === '') {
      return initialValues[currentValuesKey] !== null;
    }

    return initialValues[currentValuesKey] !== currentValues[currentValuesKey];
  });

  return changes.some(change => !!change);
};
