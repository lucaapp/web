import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';
import { useQueryClient } from 'react-query';

import { updateLocationLink } from 'network/api';
import { PrimaryButton } from 'components/general';
import {
  useGetLocationLinks,
  useNotifications,
  QUERY_KEYS,
  useHTTPSDomainValidator,
} from 'components/hooks';
import { getFormFields, getFieldsValaueChanged } from './ExternalLinks.helper';
import {
  StyledForm,
  StyledInput,
  ButtonWrapper,
  Wrapper,
} from './ExternalLinks.styled';

export const ExternalLinks = ({ location }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const { errorMessage } = useNotifications();
  const [form] = Form.useForm();
  const httpsDomainValidator = useHTTPSDomainValidator();

  const [isButtonDisabled, setIsButtonDisabled] = useState(false);

  const fields = getFormFields();

  useEffect(() => {
    const formValues = form.getFieldsValue();
    const hasValues = Object.entries(formValues).filter(([, value]) => value);

    const formIsDirty = hasValues.length === 0 || !form.isFieldTouched(true);

    setIsButtonDisabled(formIsDirty);
  }, [form]);

  const { isLoading, error: linksError, data: links } = useGetLocationLinks(
    location.uuid
  );

  const validateFields = async () => {
    const valuesWasChanged = getFieldsValaueChanged(
      form.getFieldsValue(),
      links
    );

    try {
      await form.validateFields();

      setIsButtonDisabled(!valuesWasChanged);
    } catch {
      setIsButtonDisabled(true);
    }
  };

  const onFinish = values => {
    Object.entries(values).forEach(async ([type, value]) => {
      const url = !value ? null : value;
      if (links[type] === url) return;

      try {
        const data = { type, url };

        await updateLocationLink(location.uuid, data);
        queryClient.invalidateQueries(QUERY_KEYS.LOCATION_LINKS);
        setIsButtonDisabled(true);
      } catch {
        errorMessage(`notification.externalLinks.${type}.error`);
      }
    });
  };

  if (isLoading || linksError) return null;

  return (
    <Wrapper data-cy="websiteMenuLinksSection">
      <StyledForm
        form={form}
        onChange={validateFields}
        onFinish={onFinish}
        initialValues={links}
      >
        {fields.map(field => (
          <Form.Item
            key={field.id}
            colon={false}
            label={intl.formatMessage({
              id: field.label,
            })}
            name={field.id}
            rules={httpsDomainValidator}
          >
            <StyledInput
              data-cy={`linkInput-${field.id}`}
              placeholder={intl.formatMessage({ id: field.placeholder })}
            />
          </Form.Item>
        ))}
        <ButtonWrapper>
          <PrimaryButton
            data-cy="websiteLinksSaveChangesButton"
            htmlType="submit"
            disabled={isButtonDisabled}
          >
            {intl.formatMessage({ id: 'externalLinks.save' })}
          </PrimaryButton>
        </ButtonWrapper>
      </StyledForm>
    </Wrapper>
  );
};
