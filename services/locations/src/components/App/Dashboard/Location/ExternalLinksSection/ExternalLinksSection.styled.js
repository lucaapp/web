import styled from 'styled-components';

export const StyledFooter = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: left;
`;

export const StyledTitle = styled.div`
  display: flex;
  align-items: center;
`;

export const StyledSwitchContainer = styled.div`
  flex: 1;
  display: flex;
  justify-content: flex-end;
`;
