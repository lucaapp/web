import React, { useState, useEffect } from 'react';
import { Spin } from 'antd';

// eslint-disable-next-line import/no-unresolved
import 'html5-device-mockups/dist/device-mockups.min.css';

import { useGetLocationLinks } from 'components/hooks';
import {
  Wrapper,
  ScreenWrapper,
  PreviewIFrame,
  SpinWrapper,
} from './LocationPreview.styled';

export const LocationPreview = ({ location }) => {
  const [isLoading, setIsLoading] = useState(true);
  const [indexKey, setIndexKey] = useState(0);
  const [currentLinks, setCurrentLinks] = useState(null);
  const { data: links } = useGetLocationLinks(location.uuid);

  useEffect(() => {
    if (links && links !== currentLinks) {
      setCurrentLinks(links);
      setIsLoading(true);
      setIndexKey(indexKey + 1);
    }
  }, [
    links,
    setCurrentLinks,
    setIndexKey,
    setIsLoading,
    currentLinks,
    indexKey,
  ]);

  const toggleLoading = () => setIsLoading(!isLoading);

  return (
    <Wrapper>
      <div className="device-wrapper">
        <div
          className="device"
          data-color="black"
          data-device="iPhone7"
          data-orientation="portrait"
        >
          <ScreenWrapper className="screen">
            {isLoading && (
              <SpinWrapper>
                <Spin />
              </SpinWrapper>
            )}
            <PreviewIFrame
              key={indexKey}
              onLoad={toggleLoading}
              data-preview={JSON.stringify(currentLinks)}
              src={`/webapp/${location.scannerId}#${true}`}
            />
          </ScreenWrapper>
          <div className="button" />
        </div>
      </div>
    </Wrapper>
  );
};
