import React from 'react';
import { PaymentHistory } from 'components/general/PaymentHistory';
import { KYB_STATUS } from 'constants/paymentOnboarding';
import { queryDoNotRetryOnCode } from 'network/utils';
import { PoweredByRapyd } from 'components/general/PoweredByRapyd';
import { useGetPaymentLocationGroup } from 'components/hooks/useQueries';
import { AreaInactivePayment } from './AreaInactivePayment';
import { EnablePayment } from './EnablePayment';
import { PaymentWrapper } from './Payment.styled';
import { GenerateQRCodes } from '../GenerateQRCodes';

export const Payment = ({ location }) => {
  const {
    isLoading,
    error,
    data: paymentLocationGroup,
  } = useGetPaymentLocationGroup(location.groupId, {
    retry: queryDoNotRetryOnCode([401, 404]),
  });

  if (isLoading || error) return null;

  if (
    paymentLocationGroup?.kybStatus === KYB_STATUS.COMPLETED ||
    paymentLocationGroup?.paymentAllowed
  ) {
    return (
      <PaymentWrapper>
        <PoweredByRapyd />
        <EnablePayment location={location} />
        <GenerateQRCodes location={location} />
        <PaymentHistory
          locationGroupId={location.groupId}
          locationId={location.uuid}
        />
      </PaymentWrapper>
    );
  }
  return (
    <>
      <PoweredByRapyd />
      <AreaInactivePayment location={location} />
    </>
  );
};
