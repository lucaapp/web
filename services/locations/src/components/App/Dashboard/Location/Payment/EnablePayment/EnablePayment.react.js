import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { patchLocation } from 'network/payment';
import { useQueryClient } from 'react-query';
import { LocationCard, Switch } from 'components/general';
import {
  QUERY_KEYS,
  useGetPaymentActive,
  useGetLocationTables,
  useGetGroup,
  useNotifications,
} from 'components/hooks';
import { SmallModal } from 'components/general/Modal/Modal.styled';
import { Title, Wrapper } from './EnablePayment.styled';
import { DownloadQrCodes } from './DownloadQrCodes';

export const EnablePayment = ({ location }) => {
  const intl = useIntl();
  const { errorMessage } = useNotifications();
  const queryClient = useQueryClient();
  const [isModalVisible, setIsModalVisible] = useState(false);

  const { data: group } = useGetGroup(location.groupId);

  const { isLoading, data: locationPaymentStatus } = useGetPaymentActive(
    location.uuid
  );

  const {
    data: locationTables,
    error: isTablesError,
    isLoading: isTablesLoading,
  } = useGetLocationTables(location.uuid);

  if (
    isLoading ||
    !location ||
    !location.uuid ||
    isTablesError ||
    isTablesLoading ||
    !group
  ) {
    return null;
  }

  const closeModal = () => setIsModalVisible(false);

  const showErrorNotification = () =>
    errorMessage('payment.location.enablePayment.error');

  const toggleOnEnablePayment = () => {
    patchLocation(location.uuid, !locationPaymentStatus.paymentActive)
      .then(response => {
        if (response.status !== 202) {
          showErrorNotification();
          return;
        }
        if (!locationPaymentStatus.paymentActive) {
          setIsModalVisible(true);
        }
        queryClient.invalidateQueries(QUERY_KEYS.LOCATION_PAYMENT_ACTIVE);
      })
      .catch(() => {
        showErrorNotification();
      });
  };

  return (
    <LocationCard>
      <Wrapper>
        <Title>
          {intl.formatMessage({ id: 'groupSettings.tabs.payment' })}
        </Title>
        <Switch
          checked={locationPaymentStatus.paymentActive}
          onChange={toggleOnEnablePayment}
        />
        <SmallModal
          visible={isModalVisible}
          footer={null}
          onCancel={closeModal}
        >
          <DownloadQrCodes
            location={location}
            locationTables={locationTables}
            done={closeModal}
            group={group}
          />
        </SmallModal>
      </Wrapper>
    </LocationCard>
  );
};
