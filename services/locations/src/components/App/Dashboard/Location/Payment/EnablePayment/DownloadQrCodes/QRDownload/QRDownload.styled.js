import styled from 'styled-components';

import { QRCodeIcon } from 'assets/icons';

export const IconWrapper = styled.div`
  margin: 40px 0 50px 0;
`;

export const StyledQRCodeIcon = styled(QRCodeIcon)`
  width: 146px;
  height: 135px;
`;

export const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  width: 100%;
  flex-wrap: wrap;
`;

export const QRDownloadCardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
