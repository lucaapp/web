import React, { useState } from 'react';
import { useIntl } from 'react-intl';

import { DEFAULT_CWA_VALUE } from 'constants/cwaQrCodeValue';
import { useWorker } from 'components/hooks/useWorker';
import { getPDFWorker } from 'utils/workers';
import { downloadPDF } from 'utils/downloadPDF';
import { SecondaryButton, SuccessButton } from 'components/general';
import {
  IconWrapper,
  StyledQRCodeIcon,
  ButtonsWrapper,
  QRDownloadCardWrapper,
} from './QRDownload.styled';

export const QRDownload = ({
  done,
  location,
  group,
  locationTables,
  hasTables,
}) => {
  const intl = useIntl();
  const [isDownloading, setIsDownloading] = useState(false);

  const pdfWorkerApiReference = useWorker(getPDFWorker());

  const downloadOptions = {
    setIsDownloading,
    pdfWorkerApiReference,
    location: {
      ...location,
      groupName: group?.name,
      tables: locationTables,
    },
    intl,
    isTableQRCodeEnabled: hasTables,
    isCWAEventEnabled: DEFAULT_CWA_VALUE,
  };
  const triggerDownload = () => downloadPDF(downloadOptions);

  return (
    <QRDownloadCardWrapper>
      <IconWrapper>
        <StyledQRCodeIcon />
      </IconWrapper>
      <ButtonsWrapper>
        <SecondaryButton onClick={done}>
          {intl.formatMessage({
            id: 'button.close',
          })}
        </SecondaryButton>
        <SuccessButton
          onClick={triggerDownload}
          loading={isDownloading}
          disabled={isDownloading}
        >
          {intl.formatMessage({
            id: `qrCodesDownload.${hasTables ? 'table' : 'entire'}`,
          })}
        </SuccessButton>
      </ButtonsWrapper>
    </QRDownloadCardWrapper>
  );
};
