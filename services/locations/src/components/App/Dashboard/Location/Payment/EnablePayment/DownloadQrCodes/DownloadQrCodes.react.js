import React from 'react';
import { useIntl } from 'react-intl';

import { Wrapper } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { CompleteCard } from 'components/App/modals/generalOnboarding/common/CompleteCard';
import { QRDownload } from './QRDownload';

export const DownloadQrCodes = ({ location, locationTables, done, group }) => {
  const intl = useIntl();
  const hasTables = locationTables?.length > 0;

  return (
    <Wrapper>
      <CompleteCard
        title={intl.formatMessage({
          id: `modal.activatedPayment.title.${hasTables ? 'table' : 'entire'}`,
        })}
        description={intl.formatMessage({
          id: `modal.activatedPayment.description.${
            hasTables ? 'table' : 'entire'
          }`,
        })}
      >
        <QRDownload
          group={group}
          done={done}
          location={location}
          locationTables={locationTables}
          hasTables={hasTables}
        />
      </CompleteCard>
    </Wrapper>
  );
};
