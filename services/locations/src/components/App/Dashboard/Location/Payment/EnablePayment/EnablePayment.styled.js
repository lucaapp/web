import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: -8px;
`;

export const Title = styled.div`
  font-family: Montserrat-Bold, sans-serif;
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 0;
  padding: 4px 16px;
`;
