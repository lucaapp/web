import styled from 'styled-components';
import { RightCircleFilled } from '@ant-design/icons';

export const Wrapper = styled.div`
  padding: 16px;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  margin: 0 0 16px 0;
`;

export const SetupPaymentTitle = styled.div`
  color: rgb(80, 102, 124);
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;
  margin-right: 8px;
`;

export const SetupWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 18px;
  cursor: pointer;
`;

export const StyledRightCircleFilled = styled(RightCircleFilled)`
  font-size: 24px;
  color: rgb(195, 206, 217);
`;
