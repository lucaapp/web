import styled from 'styled-components';
import { Tabs } from 'antd';

const { TabPane } = Tabs;

export const StyledTabs = styled(Tabs)`
  .ant-tabs-tab.ant-tabs-tab-active > .ant-tabs-tab-btn {
    color: rgb(0, 0, 0);
    font-size: 16px;
  }
  .ant-tabs-tab {
    color: rgb(0, 0, 0);
    font-family: Montserrat-Medium, sans-serif;
    font-size: 16px;
    font-weight: 500;
    margin-left: 39px;
  }
  .ant-tabs-nav {
    border-bottom: 0.5px solid #9b9b9b;
  }
  .ant-tabs-ink-bar {
    background: #f1743c;
    border: 2px solid rgb(241, 116, 60);
    border-radius: 4px;
  }
  .ant-tabs-tab + .ant-tabs-tab {
    margin: 0 0 0 80px;
  }
  .ant-tabs-tab:first-child {
    margin-left: 16px;
  }
`;

export const StyledTabPane = styled(TabPane)`
  padding: 4px 4px;
`;

export const NoHeadlineSpacer = styled.div`
  height: 32px;
  width: 100%;
`;

export const Wrapper = styled.div`
  margin: 40px;
  display: flex;
  flex-direction: column;
`;

export const HeaderWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Header = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 34px;
  font-weight: 500;
  margin-bottom: 24px;
`;

export const GroupName = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 48px 0 0 0;
`;

export const HiddenImage = styled.img`
  display: none;
`;

export const SectionHeader = styled.p`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 24px;
  padding: 16px 3px;
  margin-bottom: 0;
`;
