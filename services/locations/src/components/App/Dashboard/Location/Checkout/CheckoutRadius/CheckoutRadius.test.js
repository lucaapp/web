import { render, screen, testAllDefined } from 'utils/testing';
import React from 'react';
import { fireEvent } from '@testing-library/dom';
import { act } from 'react-dom/test-utils';
import * as api from 'network/api';
import { CheckoutRadius } from './CheckoutRadius.react';

const mockedUpdateLocation = jest.spyOn(api, 'updateLocation');
mockedUpdateLocation.mockImplementation();

const mockedCheckout = {
  DEFAULT_CHECKOUT_RADIUS: 25,
  MAX_CHECKOUT_RADIUS: 1000,
};

jest.mock('constants/checkout', () => ({
  DEFAULT_CHECKOUT_RADIUS: 25,
  MAX_CHECKOUT_RADIUS: 1000,
}));

const mockedLocation = {
  uuid: '4babc731-2076-4005-ad0e-e681fe901337',
  radius: 50,
  lat: 52,
  lng: 20,
};

describe('CheckoutRadiusComponent', () => {
  const setup = async () => {
    const renderResult = render(<CheckoutRadius location={mockedLocation} />);

    const activateCheckoutRadius = await screen.findByTestId(
      'activateCheckoutRadius'
    );

    return {
      renderResult,
      activateCheckoutRadius,
    };
  };

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });

  test('Test if input is visible', async () => {
    await render(
      <CheckoutRadius location={{ ...mockedLocation, radius: 50 }} />
    );
    const checkoutRadiusInput = await screen.findByTestId(
      'checkoutRadiusInput'
    );
    expect(await screen.findByText('switch.active')).toBeDefined();

    expect(checkoutRadiusInput).toBeDefined();
  });

  test('Test if input is invisible for radius 0', async () => {
    await render(
      <CheckoutRadius location={{ ...mockedLocation, radius: 0 }} />
    );

    expect(screen.queryByText('switch.active')).toBeNull();
    expect(screen.queryByTestId('checkoutRadiusInput')).toBeNull();
  });

  test('Test if minimum radius works', async () => {
    await render(
      <CheckoutRadius location={{ ...mockedLocation, radius: 50 }} />
    );
    const checkoutRadiusInput = await screen.findByTestId(
      'checkoutRadiusInput'
    );
    await act(async () => {
      fireEvent.change(checkoutRadiusInput, {
        target: { value: 10 },
      });
      await checkoutRadiusInput.focus();
      await checkoutRadiusInput.blur();
    });
    expect(checkoutRadiusInput.value).toBe(
      mockedCheckout.DEFAULT_CHECKOUT_RADIUS.toString()
    );
    expect(mockedUpdateLocation).toHaveBeenCalled();
  });

  test('Test if maximum radius works', async () => {
    await render(
      <CheckoutRadius location={{ ...mockedLocation, radius: 50 }} />
    );
    const checkoutRadiusInput = await screen.findByTestId(
      'checkoutRadiusInput'
    );
    await act(async () => {
      fireEvent.change(checkoutRadiusInput, {
        target: { value: 9999 },
      });
      await checkoutRadiusInput.focus();
      await checkoutRadiusInput.blur();
    });
    expect(checkoutRadiusInput.value).toBe(
      mockedCheckout.MAX_CHECKOUT_RADIUS.toString()
    );
    expect(mockedUpdateLocation).toHaveBeenCalled();
  });

  test('Test value change', async () => {
    await render(
      <CheckoutRadius location={{ ...mockedLocation, radius: 50 }} />
    );
    const checkoutRadiusInput = await screen.findByTestId(
      'checkoutRadiusInput'
    );
    await act(async () => {
      fireEvent.change(checkoutRadiusInput, {
        target: { value: 150 },
      });
      await checkoutRadiusInput.focus();
      await checkoutRadiusInput.blur();
    });
    expect(checkoutRadiusInput.value).toBe('150');
    expect(mockedUpdateLocation).toHaveBeenCalled();
  });
});
