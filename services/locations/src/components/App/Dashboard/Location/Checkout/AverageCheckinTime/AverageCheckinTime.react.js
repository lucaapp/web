import React, { useState, useCallback } from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { Form } from 'antd';

import { updateLocation } from 'network/api';
import { InformationIcon, StyledTooltip, Switch } from 'components/general';
import { QUERY_KEYS, useNotifications } from 'components/hooks';

import { DEFAULT_AVERAGE_CHECKIN_TIME } from 'constants/checkout';
import { setAverageCheckoutTime } from 'utils/formatters';

import {
  CardSectionDescription,
  CardSectionTitle,
} from 'components/general/LocationCard';

import { StyledSwitchContainer } from '../../GenerateQRCodes/GenerateQRCodes.styled';
import {
  Wrapper,
  PickerWrapper,
  StyledTimePicker,
  StyledTitle,
} from './AverageCheckinTime.styled';
import {
  getTimeStringFromMinutes,
  onChangeAverageCheckinTime,
} from './AverageCheckinTime.helper';

export const AverageCheckinTime = ({ location }) => {
  const intl = useIntl();
  const { errorMessage } = useNotifications();
  const [form] = Form.useForm();
  const queryClient = useQueryClient();
  const [isAverageTimeActive, setIsAverageTimeActive] = useState(
    !!location.averageCheckinTime
  );

  const invalidateQueries = useCallback(() => {
    queryClient.invalidateQueries([QUERY_KEYS.LOCATION, location.uuid]);
  }, [location, queryClient]);

  const updateAverageCheckinTime = averageCheckinTime => {
    updateLocation({
      locationId: location.uuid,
      data: { averageCheckinTime, radius: location.radius },
    })
      .then(invalidateQueries)
      .catch(() => {
        errorMessage('notification.updateAverageCheckinTime.error');
        invalidateQueries();
      });
  };

  const toggleAverageTime = () => {
    setIsAverageTimeActive(!isAverageTimeActive);
    if (isAverageTimeActive) {
      updateAverageCheckinTime(null);
    } else {
      updateAverageCheckinTime(DEFAULT_AVERAGE_CHECKIN_TIME);
    }
  };

  const changeAverageTime = time => {
    setAverageCheckoutTime(time, form);
    onChangeAverageCheckinTime(time, errorMessage, updateAverageCheckinTime);
  };

  return (
    <Wrapper>
      <CardSectionTitle>
        <StyledTitle>
          {intl.formatMessage({
            id: 'settings.location.checkout.average.title',
          })}
          <StyledTooltip
            title={intl.formatMessage({
              id: 'settings.location.checkoutByTime.tooltip',
            })}
          >
            <InformationIcon />
          </StyledTooltip>
        </StyledTitle>
        <StyledSwitchContainer>
          <Switch
            checked={isAverageTimeActive}
            onChange={toggleAverageTime}
            data-cy="activateAutomaticCheckinTime"
          />
        </StyledSwitchContainer>
      </CardSectionTitle>
      <CardSectionDescription>
        {intl.formatMessage(
          {
            id: 'settings.location.checkout.average.description',
          },
          { br: <br /> }
        )}
      </CardSectionDescription>
      {isAverageTimeActive && (
        <PickerWrapper>
          <Form
            form={form}
            initialValues={{
              averageCheckinTime: getTimeStringFromMinutes(location) || null,
            }}
          >
            <Form.Item name="averageCheckinTime">
              <StyledTimePicker
                showNow={false}
                format="HH:mm"
                minuteStep={15}
                onSelect={changeAverageTime}
                placeholder={intl.formatMessage({
                  id: 'settings.location.checkout.average.placeholder',
                })}
              />
            </Form.Item>
          </Form>
        </PickerWrapper>
      )}
    </Wrapper>
  );
};
