import styled from 'styled-components';
import { TimePicker } from 'antd';

export const Wrapper = styled.div`
  border-top: 1px solid #d8d8d8;
  padding: 24px 32px 0 32px;
`;

export const PickerWrapper = styled.div``;

export const StyledTimePicker = styled(TimePicker)`
  width: 50%;
`;

export const StyledTitle = styled.div`
  display: flex;
  align-items: center;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 8px;
`;
