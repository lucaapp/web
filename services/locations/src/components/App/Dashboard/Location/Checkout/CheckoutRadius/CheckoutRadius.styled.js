import { Form } from 'antd';
import styled from 'styled-components';

export const StyledForm = styled(Form)`
  width: 100%;
`;

export const StyledFormItem = styled(Form.Item)`
  width: 100%;

  .ant-input-number-group-wrapper {
    width: 50%;
  }
`;

export const StyledTitle = styled.div`
  display: flex;
  align-items: center;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 8px;
`;
