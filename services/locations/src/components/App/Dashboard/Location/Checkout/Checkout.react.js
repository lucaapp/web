import { useIntl } from 'react-intl';
import React from 'react';

import { LocationCard } from 'components/general';
import { AverageCheckinTime } from './AverageCheckinTime';
import { CheckoutRadius } from './CheckoutRadius';

export const Checkout = ({ location }) => {
  const intl = useIntl();

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'settings.location.checkout.headline' })}
      testId="checkoutRadius"
    >
      <CheckoutRadius location={location} />
      <AverageCheckinTime location={location} />
    </LocationCard>
  );
};
