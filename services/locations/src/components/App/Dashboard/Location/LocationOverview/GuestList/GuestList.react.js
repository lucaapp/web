import React from 'react';
import { useIntl } from 'react-intl';

import { GuestListModal } from 'components/App/modals/GuestListModal';
import { Modal } from 'components/general';
import { GuestListComp } from './GuestList.styled';

export const GuestList = ({ location }) => {
  const intl = useIntl();

  const modalOpenButton = (
    <GuestListComp data-cy="showGuestList">
      {intl.formatMessage({ id: 'group.view.overview.guestList' })}
    </GuestListComp>
  );

  const modalContent = () => <GuestListModal location={location} />;

  return <Modal content={modalContent} openButton={modalOpenButton} />;
};
