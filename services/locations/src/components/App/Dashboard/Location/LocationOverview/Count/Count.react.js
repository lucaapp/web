import React from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';

import { RefreshIcon } from 'assets/icons';

import { useGetCurrentCount, QUERY_KEYS } from 'components/hooks';

import { Counter, Wrapper, Refresh, StyledIcon } from './Count.styled';

export const Count = ({ location }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();

  const {
    data: currentCount,
    isLoading: isCurrentLoading,
    isError: isCurrentError,
  } = useGetCurrentCount(location.scannerAccessId);

  const getCount = () => {
    if (isCurrentLoading) {
      return 0;
    }
    if (isCurrentError) {
      return intl.formatMessage({ id: 'location.count.error' });
    }
    return currentCount;
  };

  const refresh = () =>
    queryClient.invalidateQueries([
      QUERY_KEYS.CURRENT_COUNT,
      location.scannerAccessId,
    ]);

  return (
    <Wrapper>
      <Counter data-cy="guestCount">{getCount()}</Counter>
      <Refresh
        title={intl.formatMessage({ id: 'location.count.refresh' })}
        onClick={refresh}
      >
        <StyledIcon component={RefreshIcon} />
      </Refresh>
    </Wrapper>
  );
};
