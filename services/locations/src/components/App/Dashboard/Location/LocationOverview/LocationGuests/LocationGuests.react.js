import React from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { Popconfirm } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { PrimaryButton, LocationCard } from 'components/general';

import { forceCheckoutUsers } from 'network/api';

import {
  QUERY_KEYS,
  useGetCurrentCount,
  useNotifications,
} from 'components/hooks';
import { GuestList } from '../GuestList';
import { TableAllocation } from '../TableAllocation';
import { Count } from '../Count';

import {
  GuestWrapper,
  Info,
  InfoWrapper,
  LinkWrapper,
} from './LocationGuests.styled';

export const LocationGuests = ({ location }) => {
  const intl = useIntl();
  const { successMessage, errorMessage } = useNotifications();
  const queryClient = useQueryClient();

  const {
    data: currentCount,
    isLoading: isCurrentLoading,
  } = useGetCurrentCount(location.scannerAccessId);

  const checkoutDisabled = !isCurrentLoading && currentCount === 0;

  const checkoutGuestIntl =
    currentCount > 1
      ? 'group.view.overview.checkout.all'
      : 'group.view.overview.checkout';

  const showError = () => errorMessage('notification.checkOut.error');

  const invalidateQueries = () => {
    queryClient.invalidateQueries(QUERY_KEYS.GROUPS);
    queryClient.invalidateQueries([
      QUERY_KEYS.CURRENT_COUNT,
      location.scannerAccessId,
    ]);
  };

  const onCheckout = () => {
    forceCheckoutUsers(location.uuid)
      .then(response => {
        if (response.status !== 204) {
          showError();
          return;
        }
        invalidateQueries();
        successMessage('notification.checkOut.success');
      })
      .catch(() => {
        showError();
      });
  };

  return (
    <LocationCard
      title={intl.formatMessage({ id: 'group.view.overview.guests' })}
    >
      <GuestWrapper>
        <InfoWrapper>
          <Info>
            <Count location={location} />
            <LinkWrapper>
              <GuestList location={location} />
              <TableAllocation location={location} />
            </LinkWrapper>
          </Info>
          <div>
            <Popconfirm
              placement="topLeft"
              onConfirm={onCheckout}
              disabled={checkoutDisabled}
              title={intl.formatMessage({
                id: 'location.checkout.confirmText',
              })}
              okText={intl.formatMessage({
                id: 'location.checkout.confirmButton',
              })}
              cancelText={intl.formatMessage({
                id: 'generic.cancel',
              })}
              icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
            >
              <PrimaryButton
                data-cy="checkoutGuest"
                disabled={checkoutDisabled}
              >
                {intl.formatMessage({ id: checkoutGuestIntl })}
              </PrimaryButton>
            </Popconfirm>
          </div>
        </InfoWrapper>
      </GuestWrapper>
    </LocationCard>
  );
};
