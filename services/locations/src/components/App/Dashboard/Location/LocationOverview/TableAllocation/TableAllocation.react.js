import React from 'react';
import { useIntl } from 'react-intl';

// Utils
import { getRemainingDaysToUploadKey, usePrivateKey } from 'utils/privateKey';

// Components
import { useGetPrivateKeySecret, useOperator } from 'components/hooks';
import { PrivateKeyLoader } from 'components/PrivateKeyLoader';
import { TableAllocationModal } from 'components/App/modals/TableAllocationModal';
import { Spin } from 'antd';
import { Modal } from 'components/general';
import { TableAllocationWrapper } from './TableAllocation.styled';
import { SkipButton } from './SkipButton.react';

export const TableAllocation = ({ location }) => {
  const intl = useIntl();
  const operator = useOperator();

  const {
    data: privateKeySecret,
    isLoading: isPrivateKeySecretLoading,
  } = useGetPrivateKeySecret();

  const [privateKey] = usePrivateKey(privateKeySecret);

  const remainingDaysToUploadKey = getRemainingDaysToUploadKey(
    operator.data.lastSeenPrivateKey
  );

  const openTableAllocation = selectedPrivateKey => {
    return (
      <TableAllocationModal
        location={location}
        privateKey={selectedPrivateKey}
      />
    );
  };

  if (operator.isLoading() || isPrivateKeySecretLoading) return <Spin />;

  if (!location.tableCount || location.tableCount === 0) return null;

  const modalOpenButton = (
    <TableAllocationWrapper data-cy="showTableAllocation">
      {intl.formatMessage({ id: 'group.view.overview.tableAllocation' })}
    </TableAllocationWrapper>
  );

  const modalContent = privateKey ? (
    <TableAllocationModal location={location} privateKey={privateKey} />
  ) : (
    <PrivateKeyLoader
      onSuccess={openTableAllocation}
      infoTextId="privateKey.tableAllocation.modal.info"
      footerItem={remainingDaysToUploadKey > 0 && <SkipButton />}
      remainingDaysToUploadKey={remainingDaysToUploadKey}
      privateKeySecret={privateKeySecret}
    />
  );

  return (
    <Modal
      closable={remainingDaysToUploadKey > 0}
      content={modalContent}
      openButton={modalOpenButton}
    />
  );
};
