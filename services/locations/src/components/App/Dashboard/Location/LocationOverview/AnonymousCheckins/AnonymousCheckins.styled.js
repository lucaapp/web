import styled from 'styled-components';

export const Wrapper = styled.div`
  margin-top: 24px;
  padding: 0 16px 0;
  border-top: 1px solid rgba(0, 0, 0, 0.87);
`;

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Header = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 16px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  margin-top: 32px;
`;

export const StyledSwitchContainer = styled.div`
  margin-top: 32px;
`;

export const Description = styled.div`
  margin-top: 16px;
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;
