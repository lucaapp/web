import React, { useState } from 'react';
import { useIntl } from 'react-intl';

import { updateLocation } from 'network/api';

import { useNotifications } from 'components/hooks';
import { Switch } from 'components/general';
import {
  Wrapper,
  Header,
  HeaderWrapper,
  StyledSwitchContainer,
  Description,
} from './AnonymousCheckins.styled';

export const AnonymousCheckins = ({ location }) => {
  const intl = useIntl();
  const { errorMessage } = useNotifications();
  const [isContactDataMandatory, setIsContactDataMandatory] = useState(
    location.isContactDataMandatory
  );

  const toggleMandatoryCheckin = () => {
    updateLocation({
      locationId: location.uuid,
      data: { isContactDataMandatory: !isContactDataMandatory },
    })
      .then(() => {
        setIsContactDataMandatory(!isContactDataMandatory);
      })
      .catch(() => errorMessage('notification.updateLocation.error'));
  };

  return (
    <Wrapper>
      <HeaderWrapper>
        <Header>
          {intl.formatMessage({
            id: 'group.view.overview.anonymousCheckins.title',
          })}
        </Header>
        <StyledSwitchContainer>
          <Switch
            checked={isContactDataMandatory}
            onChange={toggleMandatoryCheckin}
            customCheckedLabel={intl.formatMessage({ id: 'switch.mandatory' })}
          />
        </StyledSwitchContainer>
      </HeaderWrapper>
      <Description>
        {intl.formatMessage({
          id: 'group.view.overview.anonymousCheckins.description',
        })}
      </Description>
    </Wrapper>
  );
};
