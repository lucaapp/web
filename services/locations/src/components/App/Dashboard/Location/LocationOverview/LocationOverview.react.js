import React from 'react';
import { LocationCard } from 'components/general';
import { useFeatureFlags } from 'components/hooks';

import { AnonymousCheckins } from './AnonymousCheckins';
import { EntryPolicy } from './EntryPolicy';

export const LocationOverview = ({ location }) => {
  const featureFlags = useFeatureFlags();
  return (
    <LocationCard>
      <EntryPolicy location={location} />
      {featureFlags.anonymous_checkins && (
        <AnonymousCheckins location={location} />
      )}
    </LocationCard>
  );
};
