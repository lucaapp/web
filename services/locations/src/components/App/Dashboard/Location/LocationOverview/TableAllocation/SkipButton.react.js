import React from 'react';

import { useIntl } from 'react-intl';

import { SecondaryButton } from 'components/general';
import { useModalContext } from 'components/general/Modal/useModalContext';

export const SkipButton = () => {
  const intl = useIntl();
  const { closeModal } = useModalContext();

  return (
    <SecondaryButton
      type="link"
      onClick={closeModal}
      data-cy="skipPrivateKeyUpload"
    >
      {intl.formatMessage({ id: 'privateKey.modal.skip' })}
    </SecondaryButton>
  );
};
