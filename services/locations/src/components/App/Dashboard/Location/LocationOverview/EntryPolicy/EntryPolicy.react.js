import React, { useState } from 'react';
import { useIntl } from 'react-intl';

import { updateLocation } from 'network/api';
import {
  getEntryPolicyOptions,
  LocationEntryPolicyTypes,
} from 'utils/entryPolicy';
import { Switch, InformationIcon, StyledTooltip } from 'components/general';

import { useFeatureFlags, useNotifications } from 'components/hooks';
import {
  Wrapper,
  Header,
  HeaderWrapper,
  StyledSwitchContainer,
  Description,
  StyledSelect,
  StyledOption,
} from './EntryPolicy.styled';

export const EntryPolicy = ({ location }) => {
  const intl = useIntl();
  const { errorMessage } = useNotifications();
  const [isEntryPolicyActive, setIsEntryPolicyActive] = useState(
    !!location.entryPolicy
  );

  const featureFlags = useFeatureFlags();
  const entryPolicies = getEntryPolicyOptions(featureFlags?.enable_two_g_plus);
  const defaultValue =
    location.entryPolicy ??
    entryPolicies?.[0].value ??
    LocationEntryPolicyTypes.TWO_G;

  const showError = () => errorMessage('notification.updateLocation.error');

  const toggleEntryPolicy = () => {
    updateLocation({
      locationId: location.uuid,
      data: {
        entryPolicy: isEntryPolicyActive ? null : defaultValue,
      },
    })
      .then(() => {
        setIsEntryPolicyActive(!isEntryPolicyActive);
      })
      .catch(showError);
  };

  const handlePolicyChange = value => {
    updateLocation({
      locationId: location.uuid,
      data: {
        entryPolicy: value,
      },
    }).catch(showError);
  };

  return (
    <Wrapper data-cy="locationCardCheckInEntryPolicy">
      <HeaderWrapper>
        <Header data-cy="checkInEntryPolicyCardTitle">
          {intl.formatMessage({
            id: 'group.view.overview.entryPolicy.title',
          })}
        </Header>
        <StyledSwitchContainer>
          <Switch
            checked={isEntryPolicyActive}
            onChange={toggleEntryPolicy}
            data-cy="activateCheckInEntryPolicyToggle"
          />
        </StyledSwitchContainer>
      </HeaderWrapper>
      <Description data-cy="checkInEntryPolicyCardDescription">
        {intl.formatMessage({
          id: 'group.view.overview.entryPolicy.info',
        })}
        <StyledTooltip
          title={intl.formatMessage(
            {
              id: 'entryPolicy.tooltip',
            },
            { br: <br /> }
          )}
        >
          <InformationIcon data-cy="checkInEntryPolicyInformationIcon" />
        </StyledTooltip>
      </Description>
      {isEntryPolicyActive && (
        <StyledSelect
          defaultValue={defaultValue}
          onChange={handlePolicyChange}
          data-cy="checkInRequirementsDropdownMenu"
        >
          {entryPolicies.map(policy => (
            <StyledOption
              key={policy.id}
              value={policy.value}
              data-cy={policy.dataCy}
            >
              {intl.formatMessage({ id: policy.locale })}
            </StyledOption>
          ))}
        </StyledSelect>
      )}
    </Wrapper>
  );
};
