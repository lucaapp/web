import React from 'react';

import { AreaSection } from 'components/App/Dashboard/Location/AreaDetails/common';
import { IMMUNIZATION_TYPE } from 'components/App/Dashboard/Location/AreaDetails/common/form/selectOptions.helper';
import { useOptions } from 'components/App/modals/generalOnboarding/AreaDetails/common/useOptions';
import { SelectInput } from 'components/App/modals/generalOnboarding/AreaDetails/common/SelectInput';
import {
  useAreaDetails,
  useTranslation,
} from 'components/App/Dashboard/Location/AreaDetails/hooks';
import { Form } from 'antd';

export const SelectEntryPolicyInfo = () => {
  const { form, area, updateAreaDetails } = useAreaDetails();
  const translate = useTranslation(IMMUNIZATION_TYPE);

  const handleAreaMedicalProtectionChange = value => {
    const data = {
      entryPolicyInfo: value,
    };

    updateAreaDetails(data);
  };

  const { entryPolicyInfoOptions } = useOptions();

  return (
    <Form
      form={form}
      style={{ width: '100%' }}
      initialValues={{
        entryPolicyInfo:
          area.entryPolicyInfo ?? entryPolicyInfoOptions[0].value,
      }}
      data-cy="dashboard-entryPolicyInfoSection"
    >
      <AreaSection
        description={translate('description')}
        title={translate('title')}
        dataCy="dashboard-entryPolicyInfoDescription"
      >
        <SelectInput
          dataCy="dashboard-entryPolicyInfoSelection"
          name="entryPolicyInfo"
          handleSelect={handleAreaMedicalProtectionChange}
          options={entryPolicyInfoOptions}
        />
      </AreaSection>
    </Form>
  );
};
