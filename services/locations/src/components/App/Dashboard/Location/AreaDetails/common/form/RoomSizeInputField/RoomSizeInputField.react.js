import React, { useEffect } from 'react';
import { Col } from 'antd';
import { useFormikContext } from 'formik';

import { useAreaDetails } from 'components/App/Dashboard/Location/AreaDetails/hooks';
import { FormInputType } from 'components/general/Form';
import { StyledInput } from './StyledInput.styled';

export const RoomSizeInputField = ({ labelId, name, placeholder, dataCy }) => {
  const { area } = useAreaDetails();
  const { resetForm } = useFormikContext();

  const isOutdoor = !area.isIndoor;

  useEffect(() => {
    if (!isOutdoor) return;

    resetForm();
  }, [isOutdoor, resetForm]);

  return (
    <Col flex="1">
      <StyledInput isOutdoor={isOutdoor}>
        <FormInputType.Input
          dataCy={dataCy}
          disabled={isOutdoor}
          name={name}
          placeholder={placeholder}
          suffix="m"
          labelId={labelId}
        />
      </StyledInput>
    </Col>
  );
};
