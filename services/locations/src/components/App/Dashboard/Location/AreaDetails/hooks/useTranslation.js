import { useIntl } from 'react-intl';

export const useTranslation = section => {
  const intl = useIntl();

  return key => {
    const values = {
      id: `settings.location.areaDetails.${section}.${key}`,
    };

    return intl.formatMessage(values);
  };
};
