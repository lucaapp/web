import styled from 'styled-components';

import { CardSection } from 'components/general/LocationCard';

export const StyledFooter = styled.div`
  width: 100%;
  display: flex;
  align-items: baseline;
  justify-content: left;
  gap: 10px;

  span,
  input[disabled] {
    background-color: #fff !important;
  }
`;

export const AreaDetailsCardSection = styled(CardSection)`
  padding: 0;
  border: none;
`;
