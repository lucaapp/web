import styled from 'styled-components';

export const StyledInput = styled.div`
  input::placeholder {
    color: ${({ isOutdoor }) => (isOutdoor ? '#ccc' : '#000')};
  }
`;
