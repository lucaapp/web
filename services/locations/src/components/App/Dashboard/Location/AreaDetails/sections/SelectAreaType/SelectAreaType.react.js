import React from 'react';

import { AREA_TYPE } from 'components/App/Dashboard/Location/AreaDetails/common/form/selectOptions.helper';
import { AreaSection } from 'components/App/Dashboard/Location/AreaDetails/common';
import { useOptions } from 'components/App/modals/generalOnboarding/AreaDetails/common/useOptions';
import { SelectInput } from 'components/App/modals/generalOnboarding/AreaDetails/common/SelectInput';

import {
  useAreaDetails,
  useTranslation,
} from 'components/App/Dashboard/Location/AreaDetails/hooks';
import { Form } from 'antd';

export const SelectAreaType = () => {
  const { form, area, updateAreaDetails } = useAreaDetails();
  const translate = useTranslation(AREA_TYPE);

  const handleAreaTypeChange = value => {
    const data = {
      isIndoor: value,
    };

    if (value === false) {
      data.ventilation = null;
      data.roomWidth = null;
      data.roomDepth = null;
      data.roomHeight = null;
      data.isIndoor = value;
    }

    updateAreaDetails(data);
  };
  const { areaTypeOptions } = useOptions();

  return (
    <Form
      form={form}
      style={{ width: '100%' }}
      initialValues={{ areaType: area.isIndoor ?? areaTypeOptions[0].value }}
      data-cy="dashboard-areaTypeSection"
    >
      <AreaSection
        description={translate('description')}
        title={translate('title')}
        tooltip={translate('tooltip')}
        dataCy="dashboard-areaTypeDescription"
      >
        <SelectInput
          dataCy="dashboard-areaTypeSelection"
          name="areaType"
          handleSelect={handleAreaTypeChange}
          options={areaTypeOptions}
        />
      </AreaSection>
    </Form>
  );
};
