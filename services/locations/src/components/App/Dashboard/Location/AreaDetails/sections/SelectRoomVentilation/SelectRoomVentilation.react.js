import React, { useEffect } from 'react';
import { Form } from 'antd';

import { AreaSection } from 'components/App/Dashboard/Location/AreaDetails/common';

import {
  useAreaDetails,
  useTranslation,
} from 'components/App/Dashboard/Location/AreaDetails/hooks';
import { useOptions } from 'components/App/modals/generalOnboarding/AreaDetails/common/useOptions';
import { SelectInput } from 'components/App/modals/generalOnboarding/AreaDetails/common/SelectInput';

export const SelectRoomVentilation = () => {
  const { area, form, updateAreaDetails } = useAreaDetails();
  const translate = useTranslation('roomVentilation');

  const handleRoomVentilationChange = value => {
    const data = {
      ventilation: value,
    };

    updateAreaDetails(data);
  };

  const { ventilationOptions, areaTypeOptions } = useOptions();

  // If the area is outdoor, Airing (Ventilation) will be disabled and populated with "Outdoor".
  const roomVentilationOptions = !area.isIndoor
    ? [...ventilationOptions, areaTypeOptions[1]]
    : ventilationOptions;

  const getInitialValueVentilation = () => {
    if (!area.ventilation && area.isIndoor) {
      return roomVentilationOptions[0].value;
    }

    if (!area.ventilation && !area.isIndoor) {
      return roomVentilationOptions[5].value;
    }

    return area.ventilation;
  };

  useEffect(() => {
    return form.setFieldsValue({
      ventilation: getInitialValueVentilation(),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [area.isIndoor]);

  return (
    <Form
      form={form}
      style={{ width: '100%' }}
      initialValues={{
        ventilation: getInitialValueVentilation(),
      }}
      data-cy="dashboard-roomVentilationSection"
    >
      <AreaSection
        description={translate('description')}
        title={translate('title')}
        dataCy="dashboard-roomVentilationDescription"
      >
        <SelectInput
          dataCy="dashboard-roomVentilationSelection"
          name="ventilation"
          handleSelect={handleRoomVentilationChange}
          options={roomVentilationOptions}
          disabled={!area.isIndoor}
        />
      </AreaSection>
    </Form>
  );
};
