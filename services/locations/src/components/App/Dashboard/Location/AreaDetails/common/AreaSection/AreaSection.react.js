import React from 'react';

import {
  CardSectionDescription,
  StyledCardSectionTitle,
  StyledTitleContainer,
} from 'components/general/LocationCard';
import { InformationIcon, StyledTooltip } from 'components/general';
import { StyledAreaSection } from './AreaSection.styled';
import { StyledFooter } from '../../AreaDetails.styled';

export const AreaSection = ({
  children,
  description,
  title,
  tooltip,
  dataCy,
}) => {
  return (
    <StyledAreaSection>
      <StyledCardSectionTitle>
        <StyledTitleContainer>{title}</StyledTitleContainer>
        {tooltip && (
          <StyledTooltip title={tooltip}>
            <InformationIcon />
          </StyledTooltip>
        )}
      </StyledCardSectionTitle>
      <CardSectionDescription data-cy={dataCy}>
        {description}
      </CardSectionDescription>
      <StyledFooter>{children}</StyledFooter>
    </StyledAreaSection>
  );
};
