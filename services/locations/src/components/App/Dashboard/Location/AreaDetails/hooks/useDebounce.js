import { useCallback, useEffect } from 'react';
import debounce from 'lodash/debounce';

export const useDebounce = (callback, seconds) => {
  const milliseconds = seconds * 1000;
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const memoizedCallback = useCallback(
    debounce(() => callback(), milliseconds),
    [milliseconds, callback]
  );

  useEffect(() => {
    return () => {
      memoizedCallback.cancel();
    };
  }, [memoizedCallback]);

  return memoizedCallback;
};
