import { useContext } from 'react';

import { AreaDetailsContext } from '../context/AreaDetailsContext.react';

export const useAreaDetails = () => {
  return useContext(AreaDetailsContext);
};
