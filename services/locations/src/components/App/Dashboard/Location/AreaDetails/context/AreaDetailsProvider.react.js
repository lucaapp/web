import React, { useState } from 'react';
import { updateLocation } from 'network/api';
import { Form } from 'antd';
import isEqual from 'lodash/isEqual';

import { useNotifications } from 'components/hooks';
import { AreaDetailsContext } from './AreaDetailsContext.react';
import { useSelectOptionsFromJSON } from '../hooks';

export const AreaDetailsProvider = ({ children, location }) => {
  const [form] = Form.useForm();
  const options = useSelectOptionsFromJSON();
  const { errorMessage } = useNotifications();

  const [area, setArea] = useState(location);

  const dataAlreadyExists = data => {
    const existingObject = Object.entries(data).reduce((accumulator, [key]) => {
      if (key in area) accumulator[key] = area[key];

      return accumulator;
    }, {});

    return isEqual(data, existingObject);
  };

  const updateAreaDetails = async data => {
    if (dataAlreadyExists(data)) return;

    try {
      const newData = {
        locationId: area.uuid,
        data,
      };

      const response = await updateLocation(newData);
      const body = await response.json();

      setArea(body);
      errorMessage('notification.updateLocation.success');
    } catch {
      errorMessage('notification.updateLocation.error');
    }
  };

  const values = {
    area,
    form,
    options,
    updateAreaDetails,
  };

  return (
    <AreaDetailsContext.Provider value={values}>
      {children}
    </AreaDetailsContext.Provider>
  );
};
