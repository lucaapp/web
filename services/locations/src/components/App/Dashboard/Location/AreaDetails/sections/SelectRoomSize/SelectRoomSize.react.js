import React from 'react';
import { transform } from 'lodash';

import { AreaSection } from 'components/App/Dashboard/Location/AreaDetails/common';
import { RoomSizeInputField } from 'components/App/Dashboard/Location/AreaDetails/common/form';
import {
  useAreaDetails,
  useTranslation,
} from 'components/App/Dashboard/Location/AreaDetails/hooks';
import { useRoomSizeValidationSchema } from 'components/App/Dashboard/Location/AreaDetails/hooks/formValidationSchemas';
import { AppForm, AutoSubmitForm } from 'components/general/Form';

export const SelectRoomSize = () => {
  const translate = useTranslation('roomSize');
  const validationSchema = useRoomSizeValidationSchema();
  const { area, form, updateAreaDetails } = useAreaDetails();

  const handleSubmit = data => {
    const castToNumber = (accumulator, value, key) => {
      accumulator[key] = Number(value);
    };

    const roomSize = transform(data, castToNumber, {});

    updateAreaDetails(roomSize);
  };

  const { roomWidth, roomHeight, roomDepth } = area;

  const initialRoomSize = {
    roomDepth: roomDepth ?? '',
    roomHeight: roomHeight ?? '',
    roomWidth: roomWidth ?? '',
  };

  return (
    <>
      <AppForm
        form={form}
        initialValues={initialRoomSize}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        enableReinitialize
        dataCy="dashboard-roomSizeSection"
      >
        <AreaSection
          description={translate('description')}
          title={translate('title')}
          dataCy="dashboard-roomSizeDescription"
        >
          <RoomSizeInputField
            labelId="width.label"
            name="roomWidth"
            placeholder="0.0"
            dataCy="roomWidthInputField"
          />
          <RoomSizeInputField
            labelId="depth.label"
            name="roomDepth"
            placeholder="0.0"
            dataCy="roomDepthInputField"
          />
          <RoomSizeInputField
            labelId="height.label"
            name="roomHeight"
            placeholder="0.0"
            dataCy="roomHeightInputField"
          />
          <AutoSubmitForm wait={0.5} resetErrors={!area.isIndoor} />
        </AreaSection>
      </AppForm>
    </>
  );
};
