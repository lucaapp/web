import styled from 'styled-components';

export const StyledAreaSection = styled.div`
  border-top: 1px solid #d8d8d8;
  padding: 24px 32px 40px 32px;
  width: 100%;

  &:last-child {
    padding-bottom: 10px;
  }
`;
