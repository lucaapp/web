import React from 'react';

import { LocationCard } from 'components/general';
import {
  SelectAreaMedicalProtection,
  SelectAreaType,
  SelectRoomSize,
  SelectEntryPolicyInfo,
  SelectRoomVentilation,
} from './sections';
import { AreaDetailsCardSection } from './AreaDetails.styled';
import { AreaDetailsProvider } from './context';
import { useTranslation } from './hooks';

export const AreaDetails = ({ location }) => {
  const translate = useTranslation('card');

  return (
    <LocationCard
      description={translate('description')}
      isCollapse
      title={translate('title')}
      testId="areaDetails"
    >
      <AreaDetailsCardSection>
        <AreaDetailsProvider location={location}>
          <SelectAreaType />
          <SelectAreaMedicalProtection />
          <SelectEntryPolicyInfo />
          <SelectRoomVentilation />
          <SelectRoomSize />
        </AreaDetailsProvider>
      </AreaDetailsCardSection>
    </LocationCard>
  );
};
