export const AIRING_VENTILATION = 'roomVentilation';
export const AREA_TYPE = 'areaType';
export const MEDICAL_GEAR = 'medicalGear';
export const IMMUNIZATION_TYPE = 'immunization';
export const MEDICAL_GEAR_MASKS = `${MEDICAL_GEAR}.masks`;
