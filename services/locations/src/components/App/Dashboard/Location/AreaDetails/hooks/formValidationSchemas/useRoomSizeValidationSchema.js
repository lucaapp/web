import * as Yup from 'yup';

import { useTranslation } from '../useTranslation';

export const useRoomSizeValidationSchema = () => {
  const translate = useTranslation('roomSize');

  const requiredError = translate('validation.required');
  const numberError = translate('validation.number');
  const numberNonNegativeError = translate('validation.numberNonNegative');

  return Yup.object().shape({
    roomWidth: Yup.number()
      .min(0, numberNonNegativeError)
      .typeError(numberError)
      .required(requiredError)
      .label(translate('width.label')),
    roomDepth: Yup.number()
      .min(0, numberNonNegativeError)
      .typeError(numberError)
      .required(requiredError)
      .label(translate('depth.label')),
    roomHeight: Yup.number()
      .min(0, numberNonNegativeError)
      .typeError(numberError)
      .required(requiredError)
      .label(translate('height.label')),
  });
};
