import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { useParams } from 'react-router-dom';

import { LucaLogoPaddingSVG } from 'assets/icons';

import { LocationSettings } from 'components/App/LocationSettings';
import {
  useGetLocationById,
  useDailyKey,
  useGetPaymentEnabled,
} from 'components/hooks/useQueries';

import { Checkout } from './Checkout';
import { CheckInQuery } from './CheckInQuery';
import { ScannerSelection } from './ScannerSelection';
import { AreaDetails } from './AreaDetails';
import { GenerateQRCodes } from './GenerateQRCodes';
import { TableSubdivision } from './TableSubdivision';
import { LocationOverview } from './LocationOverview';
import { ExternalLinksSection } from './ExternalLinksSection';
import { RegisterBadges } from './RegisterBadges';
import {
  Header,
  Wrapper,
  GroupName,
  StyledTabs,
  HiddenImage,
  ButtonWrapper,
  HeaderWrapper,
  StyledTabPane,
  SectionHeader,
  NoHeadlineSpacer,
} from './Location.styled';
import { LocationGuests } from './LocationOverview/LocationGuests';
import { Payment } from './Payment';

// eslint-disable-next-line complexity
export const Location = ({ isOperatorTrusted }) => {
  const intl = useIntl();
  const [disableCheckins, setDisableCheckins] = useState(false);
  const { locationId } = useParams();

  const { isLoading, error, data: location } = useGetLocationById(locationId);

  const {
    isLoading: isPaymentLoading,
    error: paymentError,
    data: paymentEnabled,
  } = useGetPaymentEnabled(location?.operator, {
    enabled: !!location,
  });

  const {
    isLoading: isLoadingDailyKey,
    error: errorDailyKey,
    data: dailyKey,
  } = useDailyKey();

  useEffect(() => {
    if (isLoadingDailyKey) return;
    setDisableCheckins(errorDailyKey || !dailyKey);
  }, [errorDailyKey, dailyKey, isLoadingDailyKey]);

  if (isLoading || error || isPaymentLoading || paymentError) {
    return null;
  }

  const isPaymentEnabled = paymentEnabled && paymentEnabled.paymentEnabled;

  return (
    <>
      <Wrapper>
        <HeaderWrapper>
          <GroupName data-cy="groupName">{location.groupName}</GroupName>
          <Header data-cy="locationName">
            {location.name ||
              intl.formatMessage({ id: 'location.defaultName' })}
          </Header>
        </HeaderWrapper>
        {isOperatorTrusted && (
          <ButtonWrapper>
            <RegisterBadges />
          </ButtonWrapper>
        )}
        <StyledTabs
          defaultActiveKey={
            isPaymentEnabled ? 'openPaymentSettings' : 'openAreaCheckin'
          }
        >
          <StyledTabPane
            tab={intl.formatMessage({ id: 'location.tabs.profile' })}
            key="openAreaSettings"
          >
            <NoHeadlineSpacer />
            <LocationSettings />
          </StyledTabPane>
          <StyledTabPane
            key="openAreaCheckin"
            tab={intl.formatMessage({ id: 'location.tabs.checkin' })}
          >
            <SectionHeader />
            <GenerateQRCodes location={location} />
            <ExternalLinksSection location={location} />
            <TableSubdivision location={location} />
            {!disableCheckins && (
              <>
                <SectionHeader>
                  {intl.formatMessage({
                    id: 'dashboard.location.header.title.contactTracing',
                  })}
                </SectionHeader>
                <LocationGuests location={location} />
                <ScannerSelection
                  location={location}
                  isDisabled={disableCheckins}
                />
                <LocationOverview location={location} />
                <CheckInQuery location={location} />
                <Checkout location={location} />
                <AreaDetails location={location} />
              </>
            )}
            {/* We need to pre load the Luca logo for QR-Code generation */}
            <HiddenImage src={LucaLogoPaddingSVG} />
          </StyledTabPane>
          {isPaymentEnabled && (
            <StyledTabPane
              tab={intl.formatMessage({ id: 'location.tabs.payment' })}
              key="openPaymentSettings"
            >
              <Payment location={location} />
            </StyledTabPane>
          )}
        </StyledTabs>
      </Wrapper>
    </>
  );
};
