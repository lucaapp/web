import React from 'react';
import { useIntl } from 'react-intl';

import { Switch } from 'components/general';

import {
  StyledSwitchContainer,
  StyledCardSection,
  StyledCardSectionTitle,
} from '../GenerateQRCodes.styled';

export const QrCodeTable = ({
  tableCount,
  isTableQRCodeEnabled,
  setIsLocationQRCodeEnabled,
  setIsTableQRCodeEnabled,
}) => {
  const intl = useIntl();

  const switchQRCodeSettings = () => {
    setIsTableQRCodeEnabled(!isTableQRCodeEnabled);
    setIsLocationQRCodeEnabled(isTableQRCodeEnabled);
  };

  return (
    <StyledCardSection data-cy="qrCodesForTablesSection">
      <StyledCardSectionTitle>
        {intl.formatMessage({
          id: 'settings.location.qrcode.tables.headline',
        })}
        <StyledSwitchContainer>
          <Switch
            checked={isTableQRCodeEnabled}
            disabled={!tableCount}
            onChange={switchQRCodeSettings}
            data-cy="qrCodesForTablesToggle"
          />
        </StyledSwitchContainer>
      </StyledCardSectionTitle>
    </StyledCardSection>
  );
};
