import React from 'react';
import { useIntl } from 'react-intl';

import { Switch } from 'components/general';

import {
  StyledSwitchContainer,
  StyledCardSection,
  StyledCardSectionTitle,
} from '../GenerateQRCodes.styled';

export const QrCodeLocation = ({
  isLocationQRCodeEnabled,
  setIsTableQRCodeEnabled,
  setIsLocationQRCodeEnabled,
}) => {
  const intl = useIntl();

  const switchQRCodeSettings = () => {
    setIsLocationQRCodeEnabled(!isLocationQRCodeEnabled);
    setIsTableQRCodeEnabled(isLocationQRCodeEnabled);
  };

  return (
    <StyledCardSection data-cy="qrCodeForEntireAreaSection">
      <StyledCardSectionTitle>
        {intl.formatMessage({
          id: 'settings.location.qrcode.location.headline',
        })}
        <StyledSwitchContainer>
          <Switch
            checked={isLocationQRCodeEnabled}
            onChange={switchQRCodeSettings}
            data-cy="qrCodeForEntireAreaToggle"
          />
        </StyledSwitchContainer>
      </StyledCardSectionTitle>
    </StyledCardSection>
  );
};
