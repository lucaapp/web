import React, { useEffect, useState } from 'react';

import { useGetLocationTables } from 'components/hooks/useQueries';
import { BasicGenerateQRCodes } from 'components/general/BasicGenerateQRCodes';
import { QrCodeLocation } from './QrCodeLocation';
import { QrCodeTable } from './QrCodeTable';
import { QrPrint } from './QrPrint';

export const GenerateQRCodes = ({ location }) => {
  const [isLocationQRCodeEnabled, setIsLocationQRCodeEnabled] = useState(null);
  const [isTableQRCodeEnabled, setIsTableQRCodeEnabled] = useState(null);

  const { error, data: locationTables, isLoading } = useGetLocationTables(
    location.uuid
  );

  useEffect(() => {
    if (locationTables?.length > 0) {
      setIsTableQRCodeEnabled(true);
      setIsLocationQRCodeEnabled(false);
    }
    if (!locationTables?.length) {
      setIsLocationQRCodeEnabled(true);
      setIsTableQRCodeEnabled(false);
    }
  }, [locationTables]);

  if (error || isLoading) return null;

  const children = () => (
    <>
      <QrCodeLocation
        location={location}
        isLocationQRCodeEnabled={isLocationQRCodeEnabled}
        setIsLocationQRCodeEnabled={setIsLocationQRCodeEnabled}
        setIsTableQRCodeEnabled={setIsTableQRCodeEnabled}
      />
      {locationTables.length > 0 && (
        <QrCodeTable
          tableCount={locationTables.length}
          setIsLocationQRCodeEnabled={setIsLocationQRCodeEnabled}
          isTableQRCodeEnabled={isTableQRCodeEnabled}
          setIsTableQRCodeEnabled={setIsTableQRCodeEnabled}
        />
      )}
    </>
  );

  return (
    <BasicGenerateQRCodes
      location={location}
      isTableQRCodeEnabled={isTableQRCodeEnabled}
      isLocationQRCodeEnabled={isLocationQRCodeEnabled}
      locationTables={locationTables}
      children={children()}
      QRPrint={<QrPrint />}
    />
  );
};
