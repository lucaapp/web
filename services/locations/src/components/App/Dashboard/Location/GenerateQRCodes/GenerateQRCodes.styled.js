import styled from 'styled-components';
import { PrimaryButton } from 'components/general';
import { CardSection, CardSectionTitle } from 'components/general/LocationCard';

export const CollapsableWrapper = styled.div`
  padding: 0 32px;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;

export const StyledPrimaryButton = styled(PrimaryButton)`
  margin-bottom: 10px;
  max-width: 248px;
  align-self: flex-end;
`;

export const StyledSwitchContainer = styled.div`
  flex: 1;
  display: flex;
  justify-content: flex-end;
`;

export const StyledCardSection = styled(CardSection)`
  padding: 14px 0;
`;

export const StyledCardSectionTitle = styled(CardSectionTitle)`
  font-family: inherit;
  font-weight: 500;
  word-break: keep-all;
`;

export const buttonStyle = {
  fontFamily: 'Montserrat-Bold, sans-serif',
  fontSize: 14,
  fontWeight: 'bold',
  padding: '0 40px',
  background: 'transparent',
  border: '1px solid black',
  color: 'rgba(0, 0, 0, 0.87)',
  marginBottom: 16,
};
