import styled from 'styled-components';
import { QuestionCircleFilled } from '@ant-design/icons';

export const Description = styled.div`
  margin-bottom: 24px;
  display: flex;
  align-items: center;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;

export const StyledQuestionCircleFilled = styled(QuestionCircleFilled)`
  cursor: unset;
  font-size: 14px;
  margin-left: 8px;
  color: rgb(150, 145, 136);
`;
