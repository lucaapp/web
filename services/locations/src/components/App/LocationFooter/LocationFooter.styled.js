import styled from 'styled-components';

export const LinksWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`;

export const Wrapper = styled.footer`
  width: 100%;
  position: absolute;
  bottom: 0;
  display: flex;
  padding: 0 34px 32px 32px;
  flex-direction: row;
  justify-content: space-between;
`;

export const Link = styled.a`
  border: none;
  padding-left: 16px;
  background: transparent;
  color: ${({ color }) => color};
`;

export const Version = styled.span`
  color: ${({ color }) => color};
  word-break: keep-all;
`;
