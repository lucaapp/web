import React from 'react';
import { useIntl } from 'react-intl';

import { useRouteMatch } from 'react-router';

import {
  LOCATION_ROUTE,
  BASE_GROUP_ROUTE,
  GROUP_SETTINGS_ROUTE,
} from 'constants/routes';

// Assets
import { LucaLogoWhiteIconSVG } from 'assets/icons';

import { Devices } from './Devices';
import { HelpCenter } from './HelpCenter';
import { DetailsDropdown } from './DetailsDropdown';
import {
  Logo,
  SubTitle,
  MenuWrapper,
  HeaderWrapper,
  StyledLink,
} from './Header.styled';

export const Header = () => {
  const intl = useIntl();
  const locationsRoute = useRouteMatch(LOCATION_ROUTE);
  const groupSettingsRoute = useRouteMatch(GROUP_SETTINGS_ROUTE);

  const groupId =
    locationsRoute?.params?.groupId || groupSettingsRoute?.params?.groupId;

  return (
    <HeaderWrapper data-cy="header">
      <StyledLink
        to={groupId ? `${BASE_GROUP_ROUTE}${groupId}` : `${BASE_GROUP_ROUTE}`}
        style={{ lineHeight: 0 }}
      >
        <Logo src={LucaLogoWhiteIconSVG} data-cy="headerLogoHome" />
        <SubTitle>
          {intl.formatMessage({
            id: 'header.subtitle',
          })}
        </SubTitle>
      </StyledLink>
      <MenuWrapper>
        <Devices />
        <DetailsDropdown />
        <HelpCenter />
      </MenuWrapper>
    </HeaderWrapper>
  );
};
