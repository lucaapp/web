import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { Header } from './Header.react';

jest.mock('store-context/selectors', () => ({
  useUser: () => ({
    data: {
      lastVersionSeen: '2.12.0',
    },
    isError: false,
    isLoading: false,
  }),
  useLanguage: () => ({
    changeLanguage: () => {},
  }),
}));

describe('HeaderComponent', () => {
  const setup = async () => {
    const renderResult = render(<Header />);

    const header = await screen.findByTestId('header');
    const headerLogoHome = await screen.findByTestId('headerLogoHome');

    return {
      renderResult,
      header,
      headerLogoHome,
    };
  };

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });
});
