import React from 'react';
import { Menu } from 'antd';
import { useQueryClient } from 'react-query';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router';

import {
  PROFILE_ROUTE,
  ARCHIVE_ROUTE,
  BASE_DATA_TRANSFER_ROUTE,
} from 'constants/routes';

import { getIncompleteTransfers } from 'utils/shareData';
import { usePrivateKey } from 'utils/privateKey';
import { clearHasSeenPrivateKeyModal } from 'utils/storage';
import { logout } from 'network/api';

import { useDailyKey, useLoginRoute, useNotifications } from 'components/hooks';
import { clearJwt } from 'utils/jwtStorage';
import { StyledMenuItem, StyledBadge } from './MenuEntries.styled';

export const MenuEntries = ({ onAction, transfers, ...properties }) => {
  const intl = useIntl();
  const history = useHistory();
  const loginRoute = useLoginRoute();
  const queryClient = useQueryClient();
  const { errorMessage } = useNotifications();
  const [, clearPrivateKey] = usePrivateKey(null);

  const { isLoading, error, data: dailyKey } = useDailyKey();

  const openProfile = () => {
    history.push(PROFILE_ROUTE);
    onAction();
  };

  const openArchive = () => {
    history.push(ARCHIVE_ROUTE);
    onAction();
  };

  const openDataRequests = () => {
    history.push(BASE_DATA_TRANSFER_ROUTE);
    onAction();
  };

  const handleLogout = () => {
    logout()
      .then(response => {
        if (response.status >= 400) {
          errorMessage('notification.logout.error');
          return;
        }

        queryClient.clear();

        clearPrivateKey(null);
        clearJwt();
        clearHasSeenPrivateKeyModal();

        window.location = loginRoute;
      })
      .catch(() => errorMessage('notification.logout.error'));
  };

  return (
    <Menu {...properties}>
      <StyledMenuItem key="0" data-cy="menuItemProfile" onClick={openProfile}>
        {intl.formatMessage({
          id: 'header.profile',
        })}
      </StyledMenuItem>
      <StyledMenuItem key="1" data-cy="menuItemArchive" onClick={openArchive}>
        {intl.formatMessage({
          id: 'header.archive',
        })}
      </StyledMenuItem>

      {dailyKey &&
        !isLoading &&
        !error(
          <StyledMenuItem
            key="2"
            data-cy="menuItemDataRequests"
            onClick={openDataRequests}
          >
            <StyledBadge count={getIncompleteTransfers(transfers).length} />
            {intl.formatMessage({ id: 'shareData.title' })}
          </StyledMenuItem>
        )}

      <Menu.Divider />
      <StyledMenuItem key="3" data-cy="menuItemLogout" onClick={handleLogout}>
        {intl.formatMessage({
          id: 'header.logout',
        })}
      </StyledMenuItem>
    </Menu>
  );
};
