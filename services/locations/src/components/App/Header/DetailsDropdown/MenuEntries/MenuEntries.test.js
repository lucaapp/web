import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { act } from 'react-dom/test-utils';
import * as api from 'network/api';
import * as jwtStorage from 'utils/jwtStorage';
import { waitFor } from '@testing-library/dom';
import { MenuEntries } from './MenuEntries.react';
import 'jest-location-mock';

jest.mock('components/hooks/useLoginRoute', () => ({
  useLoginRoute: () => 'LOGIN_ROUTE',
}));

describe('MenuEntriesComponent', () => {
  const setup = async () => {
    const renderResult = render(
      <MenuEntries onAction={() => {}} transfers={[]} />
    );

    const menuItemProfile = await screen.findByTestId('menuItemProfile');
    const menuItemArchive = await screen.findByTestId('menuItemArchive');
    const menuItemLogout = await screen.findByTestId('menuItemLogout');

    return {
      renderResult,
      menuItemProfile,
      menuItemArchive,
      menuItemLogout,
    };
  };

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });

  test('Are translations available', async () => {
    const { renderResult } = await setup();
    expect(await renderResult.getByText('header.profile')).toBeDefined();
    expect(await renderResult.getByText('header.archive')).toBeDefined();
    expect(await renderResult.findByText('header.logout')).toBeDefined();
  });

  // test logout
  test('Check logout working', async () => {
    const { menuItemLogout } = await setup();
    const mockedClearJwt = jest.spyOn(jwtStorage, 'clearJwt');
    mockedClearJwt.mockImplementation();
    jest.spyOn(api, 'logout').mockResolvedValue({ status: 400 });

    await act(async () => {
      await menuItemLogout.click();
    });
    expect(mockedClearJwt).toHaveBeenCalledTimes(0);
    jest.spyOn(api, 'logout').mockResolvedValue({ status: 204 });

    await act(async () => {
      await menuItemLogout.click();
    });

    await waitFor(() => expect(window.location).toBe('LOGIN_ROUTE'));
  });
});
