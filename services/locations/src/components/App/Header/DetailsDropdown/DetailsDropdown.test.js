import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { DetailsDropdown } from './DetailsDropdown.react';

jest.mock('components/hooks/useQueries', () => ({
  useGetTransfers: () => ({
    data: [],
    isError: false,
    isLoading: false,
  }),
}));

describe('DetailsDropdownComponent', () => {
  const setup = async () => {
    const renderResult = render(<DetailsDropdown />);

    const dropdownWrapper = await screen.findByTestId('dropdownWrapper');
    const dropdownMenuTrigger = await screen.findByTestId(
      'dropdownMenuTrigger'
    );

    return {
      renderResult,
      dropdownWrapper,
      dropdownMenuTrigger,
    };
  };

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });

  test('Is title available', () => {
    expect(screen.findByText('menu.title')).toBeDefined();
  });
});
