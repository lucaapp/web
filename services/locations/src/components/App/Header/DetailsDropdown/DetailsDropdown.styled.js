import styled from 'styled-components';
import { Badge } from 'antd';
import Icon from '@ant-design/icons';

export const StyledIcon = styled(Icon)`
  font-size: 32px;
`;

export const StyledBadge = styled(Badge)`
  top: -8px;
  left: 24px;
  line-height: 0 !important;
  position: absolute;
`;

export const Wrapper = styled.div`
  margin: 0 64px 0 44px;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  justify-content: center;
  position: relative;
`;

export const IconText = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 12px;
  font-weight: 500;
  margin-top: 10px;
  margin-bottom: 10px;
`;
