import styled from 'styled-components';

export const HelpCenterComp = styled.div`
  cursor: pointer;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const IconText = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 12px;
  font-weight: 500;
  margin-top: 10px;
  margin-bottom: 10px;
`;
