import styled from 'styled-components';
import { Layout } from 'antd';

export const AppWrapper = styled(Layout)`
  width: 100%;
  min-height: 100vh;
  background-color: #f3f5f7;
  position: relative;
`;
