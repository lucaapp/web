import React, { useMemo } from 'react';

import { useGetGroups } from 'components/hooks';

import { Wrapper } from './GroupList.styled';
import { sortGroups } from './GroupList.helper';
import { GroupsList } from './GroupsList';
import { CreateGroup } from './CreateGroup';

export const GroupList = () => {
  const { isLoading, error, data = [] } = useGetGroups();

  const groups = useMemo(() => sortGroups(data), [data]);

  if (isLoading || error) return null;

  return (
    <Wrapper id="groupList">
      <GroupsList groups={groups} />
      <CreateGroup />
    </Wrapper>
  );
};
