import { useLocation } from 'react-router';

const GROUP = 'group';
const LOCATION = 'location';
const SETTINGS = 'settings';
const TABLES = 'tables';
export const sortLocations = locations => {
  // Base location should be first
  // Other locations after base sorted alphabetically
  const baseLocation = locations.find(location => !location.name);
  const rest = locations.filter(location => location.name);
  const sortedRest = rest.sort((a, b) => a.name.localeCompare(b.name));

  return [baseLocation, ...sortedRest];
};

export const sortGroups = groups => {
  // Sort groups alphabetically
  return groups.sort((a, b) => a.name.localeCompare(b.name));
};

export const useGetLocationsRoute = () => {
  const location = useLocation();
  const urlArray = location.pathname.split('/');

  let groupId;
  let locationId;
  let isTables = false;

  const tablesPosition = urlArray.indexOf(TABLES);
  const groupPosition = urlArray.indexOf(GROUP);
  const locationPosition = urlArray.indexOf(LOCATION);

  groupId = urlArray[groupPosition + 2];
  locationId = urlArray[locationPosition + 2];

  if (urlArray[groupPosition + 1] !== SETTINGS) {
    groupId = urlArray[groupPosition + 1];
  }

  if (urlArray[locationPosition + 1] !== SETTINGS) {
    locationId = urlArray[locationPosition + 1];
  }

  if (tablesPosition >= 0) {
    locationId = urlArray[tablesPosition + 1];
    isTables = true;
  }

  return {
    groupId,
    locationId,
    isTables,
  };
};
