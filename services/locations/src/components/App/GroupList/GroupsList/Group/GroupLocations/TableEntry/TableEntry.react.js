import React from 'react';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router';
import { useGetLocationTables } from 'components/hooks';

import { BASE_GROUP_ROUTE, BASE_TABLES_ROUTE } from 'constants/routes';

import { useGroupsNavigation } from 'components/App/GroupList/hooks';

import { TableEntryCom, StyledTableIcon } from './TableEntry.styled';

export const TableEntry = ({ location }) => {
  const intl = useIntl();
  const history = useHistory();
  const { group: activeGroup } = useGroupsNavigation();

  const { error, data: locationTables, isLoading } = useGetLocationTables(
    location.uuid
  );

  const handleClick = () => {
    const route = `${BASE_GROUP_ROUTE}${location.groupId}${BASE_TABLES_ROUTE}${location.uuid}`;
    history.push(route);
  };

  if (error || isLoading || !locationTables.length) return null;

  return (
    <TableEntryCom
      data-cy={`tables-${location.uuid}`}
      isActiveTable={
        activeGroup.locationId === location.uuid && activeGroup.isTables
      }
      onClick={handleClick}
    >
      <StyledTableIcon />
      {intl.formatMessage({ id: 'sidebar.tables' })}
    </TableEntryCom>
  );
};
