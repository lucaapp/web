import React from 'react';
import { useIntl } from 'react-intl';
import Icon from '@ant-design/icons';

import { AddCircleBlueIcon } from 'assets/icons';
import { CreateLocationModal } from 'components/App/modals/CreateLocationModal';
import { Modal } from 'components/general';
import { Wrapper, CreateText } from './CreateLocation.styled';

export const CreateLocation = ({ groupId }) => {
  const intl = useIntl();

  const modalOpenButton = (
    <Wrapper data-cy={`createLocation-${groupId}`}>
      <Icon component={AddCircleBlueIcon} style={{ fontSize: 14 }} />
      <CreateText>
        {intl.formatMessage({ id: 'groupList.createLocation' })}
      </CreateText>
    </Wrapper>
  );

  const modalContent = () => <CreateLocationModal groupId={groupId} />;

  return <Modal openButton={modalOpenButton} content={modalContent} />;
};
