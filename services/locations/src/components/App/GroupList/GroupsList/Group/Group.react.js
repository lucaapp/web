import React from 'react';

import { ArrowDownBlueIcon } from 'assets/icons';
import {
  BASE_GROUP_SETTINGS_ROUTE,
  BASE_GROUP_ROUTE,
  BASE_LOCATION_ROUTE,
} from 'constants/routes';

import { useMatomo } from '@jonkoops/matomo-tracker-react';
import { useIntl } from 'react-intl';
import { StyledIcon } from '../../GroupList.styled';
import { useGroupsNavigation } from '../../hooks';
import { GroupLocations } from './GroupLocations';
import {
  GroupName,
  GroupNameWrapper,
  StyledGroupIndicatorDarkIcon,
  StyledGroupIndicatorMidIcon,
  StyledGroupIndicatorLightIcon,
} from './Group.styled';

const ArrowIcon = ({ isActive }) => (
  <StyledIcon component={ArrowDownBlueIcon} rotate={isActive ? 0 : -90} />
);

export const Group = ({
  group: { locations, groupId, name },
  mappingIndex,
}) => {
  const intl = useIntl();
  const { trackEvent } = useMatomo();
  const { open, group: activeGroup } = useGroupsNavigation();

  const openGroup = () => {
    trackEvent({
      category: 'location-group',
      action: 'changed',
      name,
      value: groupId,
    });

    open(`${BASE_GROUP_SETTINGS_ROUTE}${groupId}`);
  };

  const openLocation = location => {
    const { uuid } = location;

    trackEvent({
      category: 'location',
      action: 'changed',
      name: location.name || intl.formatMessage({ id: 'location.defaultName' }),
      value: location.uuid,
    });

    open(`${BASE_GROUP_ROUTE}${groupId}${BASE_LOCATION_ROUTE}${uuid}`);
  };

  const getGroupIcon = index => {
    switch (index % 3) {
      case 0:
        return <StyledGroupIndicatorDarkIcon />;
      case 1:
        return <StyledGroupIndicatorMidIcon />;
      default:
        return <StyledGroupIndicatorLightIcon />;
    }
  };

  return (
    <>
      <GroupNameWrapper
        onClick={openGroup}
        isActiveGroup={
          groupId === activeGroup.groupId &&
          !activeGroup.locationId &&
          !activeGroup.isTables
        }
        data-cy={`groupItem-${groupId}`}
      >
        {getGroupIcon(mappingIndex)}
        <ArrowIcon isActive={groupId === activeGroup.groupId} />
        <GroupName data-cy={`location-${name}`}>{name}</GroupName>
      </GroupNameWrapper>
      <GroupLocations
        openLocation={openLocation}
        groupId={groupId}
        locations={locations}
      />
    </>
  );
};
