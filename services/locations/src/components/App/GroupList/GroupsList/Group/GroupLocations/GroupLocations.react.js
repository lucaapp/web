import React, { useMemo } from 'react';

import { useIntl } from 'react-intl';

import { useGroupsNavigation } from 'components/App/GroupList/hooks';

import { CreateLocation } from './CreateLocation';
import { TableEntry } from './TableEntry';
import { sortLocations } from './GroupLocations.helper';
import { Location } from './GroupLocations.styled';

export const GroupLocations = ({ locations, groupId, openLocation }) => {
  const intl = useIntl();

  const { group } = useGroupsNavigation();

  const memoLocations = useMemo(() => sortLocations(locations), [locations]);

  if (groupId !== group.groupId) return null;

  return (
    <>
      {memoLocations.map(location => (
        <div key={location.uuid}>
          <Location
            data-cy={`location-${location.name || 'General'}`}
            onClick={() => openLocation(location)}
            isActiveLocation={
              location.uuid === group.locationId && !group.isTables
            }
          >
            {location.name ||
              intl.formatMessage({ id: 'location.defaultName' })}
          </Location>
          <TableEntry
            location={location}
            isActiveLocation={location.uuid === group.locationId}
          />
        </div>
      ))}
      <CreateLocation groupId={groupId} />
    </>
  );
};
