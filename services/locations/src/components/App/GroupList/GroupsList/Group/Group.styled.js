import styled from 'styled-components';
import {
  GroupIndicatorDarkIcon,
  GroupIndicatorMidIcon,
  GroupIndicatorLightIcon,
} from 'assets/icons';

export const GroupName = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  margin-left: 4px;
`;

export const GroupNameWrapper = styled.div`
  display: flex;
  justify-content: left;
  padding: 15px 0 15px 30px;
  background: ${({ isActiveGroup }) =>
    isActiveGroup ? 'rgb(243, 245, 247)' : ''};
  font-weight: ${({ isActiveGroup }) => (isActiveGroup ? '600' : '500')};
  font-family: ${({ isActiveGroup }) =>
    isActiveGroup ? 'Montserrat-Bold, sans-serif' : 'Montserrat, sans-serif'};

  &:active,
  &:hover {
    background: rgb(243, 245, 247);
    font-family: Montserrat-Bold, sans-serif;
    font-weight: 600;
  }
`;

export const StyledGroupIndicatorDarkIcon = styled(GroupIndicatorDarkIcon)`
  margin-right: 8px;
`;
export const StyledGroupIndicatorMidIcon = styled(GroupIndicatorMidIcon)`
  margin-right: 8px;
`;
export const StyledGroupIndicatorLightIcon = styled(GroupIndicatorLightIcon)`
  margin-right: 8px;
`;
