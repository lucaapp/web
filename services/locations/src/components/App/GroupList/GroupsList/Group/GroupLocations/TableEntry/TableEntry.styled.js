import styled from 'styled-components';
import { TableBlackIcon } from 'assets/icons';

export const TableEntryCom = styled.div`
  cursor: pointer;
  overflow: hidden;
  text-overflow: ellipsis;
  background: ${({ isActiveTable }) =>
    isActiveTable ? 'rgb(243, 245, 247)' : 'rgba(243, 245, 247, 0.45)'};
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  border-bottom: 1px solid rgb(248, 251, 255);
  padding: 15px 0 15px 70px;
  display: flex;
  align-items: center;
`;

export const StyledTableIcon = styled(TableBlackIcon)`
  margin-right: 8px;
`;
