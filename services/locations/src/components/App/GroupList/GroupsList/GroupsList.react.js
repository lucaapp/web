import React from 'react';

import { Group } from './Group';
import { GroupComp, GroupContainer, ListWrapper } from './GroupsList.styled';

export const GroupsList = ({ groups }) => {
  return (
    <ListWrapper>
      {groups.map((group, index) => {
        return (
          <GroupContainer key={group.groupId}>
            <GroupComp data-cy={group.name}>
              <Group group={group} mappingIndex={index} />
            </GroupComp>
          </GroupContainer>
        );
      })}
    </ListWrapper>
  );
};
