import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 40px;
`;

export const StyledIcon = styled(Icon)`
  font-size: 16px;
  color: rgb(80, 102, 124);
  margin-top: 3px;
`;
