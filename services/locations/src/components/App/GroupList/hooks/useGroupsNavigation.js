import { useHistory } from 'react-router';

import { useGetLocationsRoute } from '../GroupList.helper';

export const useGroupsNavigation = () => {
  const history = useHistory();
  const group = useGetLocationsRoute();

  const open = location => history.push(location);

  return { group, open };
};
