import styled from 'styled-components';

export const Wrapper = styled.div`
  position: sticky;
  bottom: 0;
  display: flex;
  padding: 15px 0 15px 32px;
  cursor: pointer;
  background-color: #dae0e7;
  align-items: center;
`;

export const CreateText = styled.div`
  color: rgb(80, 102, 124);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  margin-left: 8px;
`;
