import React from 'react';
import { useIntl } from 'react-intl';
import Icon from '@ant-design/icons';

import { AddCircleBlueIcon } from 'assets/icons';

import { CreateGroupModal } from 'components/App/modals/CreateGroupModal';

import { Modal } from 'components/general';
import { Wrapper, CreateText } from './CreateGroup.styled';

export const CreateGroup = () => {
  const intl = useIntl();

  const modelOpenButton = (
    <Wrapper data-cy="createGroup">
      <Icon component={AddCircleBlueIcon} style={{ fontSize: 14 }} />
      <CreateText>
        {intl.formatMessage({ id: 'groupList.createGroup' })}
      </CreateText>
    </Wrapper>
  );

  const modalContent = () => <CreateGroupModal />;

  return <Modal content={modalContent} openButton={modelOpenButton} />;
};
