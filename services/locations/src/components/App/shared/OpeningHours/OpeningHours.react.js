import React from 'react';
import { useIntl } from 'react-intl';

import { useGetLocationById } from 'components/hooks';

import { LocationCard } from 'components/general';

import { EmptyHoursView } from './EmptyHoursView';
import { HoursView } from './HoursView';

import { Wrapper } from './OpeningHours.styled';

export const OpeningHours = ({
  group = null,
  locationId: passedLocationId,
}) => {
  const intl = useIntl();
  const locationId = group
    ? group?.locations?.find(location => !location.name).uuid
    : passedLocationId;

  const { isLoading, error, data: location } = useGetLocationById(locationId);

  if (isLoading || error) return null;

  const isOpeningHoursEmpty =
    !location.openingHours ||
    Object.values(location.openingHours).every(
      openingHour => !openingHour || !openingHour[0]?.included
    );

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'openingHours.card.title' })}
      testId="openingHours"
    >
      <Wrapper>
        {isOpeningHoursEmpty ? (
          <EmptyHoursView location={location} />
        ) : (
          <HoursView location={location} />
        )}
      </Wrapper>
    </LocationCard>
  );
};
