import React, { useState } from 'react';
import { useIntl } from 'react-intl';

import { DoorsignIcon } from 'assets/icons';
import { PrimaryButton } from 'components/general';

import { OpeningHoursModal } from 'components/App/modals/OpeningHoursModal';

import {
  ContentWrapper,
  EmptyViewHeadline,
  ButtonWrapper,
  EmptyViewDescription,
} from './EmptyHoursView.styled';

export const EmptyHoursView = ({ location }) => {
  const intl = useIntl();
  const [isOpeningHoursModalOpen, setIsOpeningHoursModalOpen] = useState(false);

  const toggleOpeningHoursModal = () =>
    setIsOpeningHoursModalOpen(!isOpeningHoursModalOpen);

  return (
    <>
      <ContentWrapper data-cy="emptyOpeningHoursWrapper">
        <EmptyViewHeadline data-cy="emptyOpeningHoursHeadline">
          {intl.formatMessage({ id: 'openingHours.card.empty.heading' })}
        </EmptyViewHeadline>
        <EmptyViewDescription data-cy="emptyOpeningHoursDescription">
          {intl.formatMessage({ id: 'openingHours.card.empty.description' })}
        </EmptyViewDescription>
        <DoorsignIcon data-cy="emptyOpeningHoursIcon" />
      </ContentWrapper>
      <ButtonWrapper>
        <PrimaryButton
          onClick={toggleOpeningHoursModal}
          data-cy="emptyOpeningHoursButton"
        >
          {intl.formatMessage({ id: 'openingHours.card.empty.createButton' })}
        </PrimaryButton>
      </ButtonWrapper>
      {isOpeningHoursModalOpen && (
        <OpeningHoursModal
          location={location}
          onClose={toggleOpeningHoursModal}
        />
      )}
    </>
  );
};
