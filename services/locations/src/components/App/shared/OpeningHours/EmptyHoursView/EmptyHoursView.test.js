import React from 'react';
import { render, screen, testAllDefined } from 'utils/testing';
import { act } from 'react-dom/test-utils';
import { EmptyHoursView } from './EmptyHoursView.react';

const mockLocation = {};

describe('EmptyHoursView Component', () => {
  const setup = async () => {
    const renderResult = render(<EmptyHoursView location={mockLocation} />);

    const wrapper = await screen.findByTestId('emptyOpeningHoursWrapper');
    const headline = await screen.findByTestId('emptyOpeningHoursHeadline');
    const description = await screen.findByTestId(
      'emptyOpeningHoursDescription'
    );
    const icon = await screen.findByTestId('emptyOpeningHoursIcon');
    const button = await screen.findByTestId('emptyOpeningHoursButton');

    return {
      renderResult,
      wrapper,
      headline,
      description,
      icon,
      button,
    };
  };

  test('Check if component renders', async () => {
    testAllDefined(await setup());
  });

  test('Check if OpeningHoursModal is opened on click', async () => {
    const { button } = await setup();

    await act(async () => {
      await button.click();
    });

    const openingHoursModalHeadline = screen.getByTestId(
      'openingHoursModalHeadline'
    );

    expect(openingHoursModalHeadline).toBeDefined();
  });
});
