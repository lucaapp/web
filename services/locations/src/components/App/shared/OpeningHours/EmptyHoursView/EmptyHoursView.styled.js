import styled from 'styled-components';

export const ContentWrapper = styled.div`
  border: 0.5px solid rgb(217, 215, 212);
  padding: 32px 0;
  text-align: center;
  margin-bottom: 24px;
`;

export const EmptyViewHeadline = styled.div`
  color: rgb(129, 129, 129);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const EmptyViewDescription = styled.div`
  color: rgb(129, 129, 129);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  height: 48px;
  line-height: 24px;
  text-align: center;
  width: 500px;
  margin: 0 auto 16px auto;
`;
