const getDayString = (day, intl) =>
  intl.formatMessage({ id: `openingHours.weekday.${day}` });

export const getOpeningHoursList = (location, intl) => {
  const dayKeys = Object.keys(location.openingHours);

  const openingHoursPerDay = dayKeys.map((dayKey, counter) => {
    const hoursList = location.openingHours[dayKey];
    return Array.isArray(hoursList)
      ? hoursList.map((_, index) => {
          return {
            id: `${counter}${index}`,
            day: index === 0 ? `${getDayString(dayKey, intl)}:` : '',
            openingHour: location.openingHours[dayKey][index].openingHours[0],
            closingHour: location.openingHours[dayKey][index].openingHours[1],
            included: location.openingHours[dayKey][index].included,
          };
        })
      : [];
  });

  return openingHoursPerDay.reduce((flatten, array) => [...flatten, ...array]);
};
