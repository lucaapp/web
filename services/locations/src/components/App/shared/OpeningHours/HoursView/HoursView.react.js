import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import moment from 'moment';

import { OpeningHoursModal } from 'components/App/modals/OpeningHoursModal';
import { PrimaryButton } from 'components/general';
import { useLanguage } from 'store-context/selectors';
import { getOpeningHoursList } from './HoursView.helper';
import {
  Wrapper,
  HoursList,
  ButtonWrapper,
  DayWrapper,
  Day,
  Time,
} from './HoursView.styled';

export const HoursView = ({ location }) => {
  const intl = useIntl();
  const { currentLanguage } = useLanguage();
  const [isOpeningHoursModalOpen, setIsOpeningHoursModalOpen] = useState(false);

  const toggleOpeningHoursModal = () =>
    setIsOpeningHoursModalOpen(!isOpeningHoursModalOpen);

  const openingHoursList = getOpeningHoursList(location, intl);

  const formatHourByLanguage = hour => {
    const selectedHour = hour.split(':')[0];
    const selectedMinutes = hour.split(':')[1];
    const date = new Date().setHours(selectedHour, selectedMinutes);
    const formattedHour = moment(date).format('hh:mm A');

    return currentLanguage === 'de' ? hour : formattedHour;
  };

  return (
    <>
      <Wrapper>
        <HoursList>
          {openingHoursList.map(entry => (
            <DayWrapper key={`${entry.id}`}>
              <Day>{`${entry.day}`}</Day>
              <Time>
                {entry.included
                  ? `${formatHourByLanguage(
                      entry.openingHour
                    )} - ${formatHourByLanguage(entry.closingHour)}`
                  : intl.formatMessage({ id: 'openingHours.weekday.closed' })}
              </Time>
            </DayWrapper>
          ))}
        </HoursList>
        <ButtonWrapper>
          <PrimaryButton onClick={toggleOpeningHoursModal}>
            {intl.formatMessage({ id: 'openingHours.card.edit' })}
          </PrimaryButton>
        </ButtonWrapper>
      </Wrapper>
      {isOpeningHoursModalOpen && (
        <OpeningHoursModal
          location={location}
          onClose={toggleOpeningHoursModal}
        />
      )}
    </>
  );
};
