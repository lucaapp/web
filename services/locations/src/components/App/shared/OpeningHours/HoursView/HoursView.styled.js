import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 24px;
`;

export const HoursList = styled.div`
  width: 250px;
  display: flex;
  flex-direction: column;
`;

export const ButtonWrapper = styled.div`
  width: 50%;
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
`;

export const DayWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 8px;
`;

export const Day = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;

export const Time = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;
