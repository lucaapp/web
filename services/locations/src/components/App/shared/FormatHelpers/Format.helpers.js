export const formatCurrency = (intl, number, currency = 'EUR') =>
  intl.formatNumber(number, { style: 'currency', currency });
