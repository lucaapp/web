import React from 'react';
import { Popconfirm } from 'antd';
import { useIntl } from 'react-intl';

export const RefundPopConfirm = ({ onConfirm, children }) => {
  const intl = useIntl();

  return (
    <Popconfirm
      placement="topLeft"
      onConfirm={onConfirm}
      title={intl.formatMessage({
        id: 'payment.location.paymentHistory.refund.confirm.title',
      })}
      okText={intl.formatMessage({
        id: 'payment.location.paymentHistory.refund.confirm.okText',
      })}
      cancelText={intl.formatMessage({
        id: 'generic.cancel',
      })}
      zIndex={300000}
    >
      {children}
    </Popconfirm>
  );
};
