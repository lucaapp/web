import React from 'react';
import { useIntl } from 'react-intl';
import Icon from '@ant-design/icons';

// Assets
import { WarningIcon } from 'assets/icons';

import { StyledTooltip } from 'components/general';

import { IconWrapper } from './RequestWarning.styled';

const RequestWarningIcon = () => (
  <Icon component={WarningIcon} style={{ fontSize: 16 }} />
);

export const RequestWarning = () => {
  const intl = useIntl();

  return (
    <StyledTooltip title={intl.formatMessage({ id: 'requestWarning.info' })}>
      <IconWrapper>
        <RequestWarningIcon />
      </IconWrapper>
    </StyledTooltip>
  );
};
