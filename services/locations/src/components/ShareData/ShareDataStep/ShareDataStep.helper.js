import {
  decryptTrace,
  reencryptAdditionalData,
  reencryptWithHDEKP,
} from 'utils/crypto';

/**
 * This function decrypts the outer encryption layer of the traces requested by
 * the health department for the corresponding locationTransfer process. Any
 * additional data will be re-encrypted with the public key of the responsible
 * health department. The remaining data is still encrypted with the daily key.
 *
 * @see https://www.luca-app.de/securityoverview/processes/tracing_find_contacts.html#process
 *
 * @param transfer
 * @param trace
 * @param privateKey
 */
export const decryptTraceByOperator = (transfer, trace, privateKey) => {
  const decryptedTrace = decryptTrace(trace, privateKey);
  const hdEncryptedTraceData = reencryptWithHDEKP(
    decryptedTrace.data,
    transfer.department.publicHDEKP
  );
  const reencryptedAdditionalData = reencryptAdditionalData(
    trace.additionalData,
    privateKey,
    transfer.department.publicHDEKP
  );

  return {
    traceId: trace.traceId,
    version: decryptedTrace.version,
    keyId: decryptedTrace.keyId,
    publicKey: decryptedTrace.publicKey,
    verification: decryptedTrace.verification,
    data: hdEncryptedTraceData,
    additionalData: reencryptedAdditionalData,
    isContactDataMandatory: trace.isContactDataMandatory,
  };
};
