import React from 'react';
import { useIntl } from 'react-intl';
import { uniqueId } from 'lodash';

import { RequestContent } from '../../ShareData.styled';
import {
  StyledLabel,
  StyledValue,
  StyledHealthDepartment,
} from '../ShareDataStep.styled';
import { VerificationTag } from './VerificationTag';

export const HealthDepartmentInfo = ({ transfers }) => {
  const intl = useIntl();

  return (
    <RequestContent>
      <StyledLabel>
        {intl.formatMessage({ id: 'shareData.inquiringHD' })}
      </StyledLabel>
      {transfers.map(({ department }) => (
        <StyledValue key={uniqueId('id_')}>
          <StyledHealthDepartment>
            {department.name}
            <VerificationTag healthDepartment={department} />
          </StyledHealthDepartment>
          <StyledHealthDepartment>{department.phone}</StyledHealthDepartment>
          <StyledHealthDepartment>{department.email}</StyledHealthDepartment>
        </StyledValue>
      ))}
    </RequestContent>
  );
};
