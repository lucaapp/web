import styled from 'styled-components';

export const StatusWrapper = styled.div`
  display: flex;
  margin: 24px 0;
  justify-content: space-between;
`;

export const TransferIndex = styled.div`
  color: #000000;
  font-size: 16px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  width: 50%;
`;

export const StatusInformationWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const FeedbackMessage = styled.div`
  color: ${({ $isShareDataSuccess }) =>
    $isShareDataSuccess ? '#000000' : '#FF0000'};
  font-size: 14px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  line-height: 24px;
`;

export const ErrorInformation = styled.div`
  color: #000000;
  font-size: 14px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  line-height: 20px;
`;
