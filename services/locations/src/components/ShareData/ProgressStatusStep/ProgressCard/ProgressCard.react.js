import React from 'react';
import { Progress } from 'antd';
import { useIntl } from 'react-intl';
import { HD_SUPPORT_EMAIL } from 'constants/links';
import {
  ErrorInformation,
  FeedbackMessage,
  StatusInformationWrapper,
  StatusWrapper,
  TransferIndex,
} from './ProgressCard.styled';

export const ProgressCard = ({
  index,
  progressStatus,
  statusFeedback,
  showRequestTooBigError,
  enrichedTransfer,
}) => {
  const intl = useIntl();
  const emailLink = `mailto:${HD_SUPPORT_EMAIL}`;

  return (
    <>
      <StatusWrapper>
        <TransferIndex>
          {intl.formatMessage(
            {
              id: 'shareData.progress.transferName',
            },
            {
              index: index + 1,
            }
          )}
        </TransferIndex>
        <Progress
          percent={progressStatus[enrichedTransfer.uuid]}
          status={statusFeedback}
        />
      </StatusWrapper>
      {statusFeedback && (
        <StatusInformationWrapper>
          <FeedbackMessage
            $isShareDataSuccess={statusFeedback === 'success'}
            data-cy="shareDataFeedback"
          >
            {intl.formatMessage({
              id: `shareData.progress.${statusFeedback}`,
            })}
          </FeedbackMessage>
          {statusFeedback === 'exception' && (
            <ErrorInformation data-cy="shareDataErrorMessage">
              {intl.formatMessage(
                {
                  id: showRequestTooBigError
                    ? 'shareData.progress.errorRequestTooBig'
                    : 'shareData.progress.error.message',
                },
                {
                  // eslint-disable-next-line react/display-name
                  a: (...chunks) => (
                    <a href={emailLink} rel="noopener noreferrer">
                      {chunks}
                    </a>
                  ),
                  // eslint-disable-next-line react/display-name
                  b: (...chunks) => <b>{chunks}</b>,
                  email: HD_SUPPORT_EMAIL,
                  transferId: enrichedTransfer.uuid.slice(0, 8),
                }
              )}
            </ErrorInformation>
          )}
        </StatusInformationWrapper>
      )}
    </>
  );
};
