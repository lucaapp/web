import {
  END_OF_LAST_TRANSFER,
  END_OF_CURRENT_TRANSFER,
} from 'constants/tracesIterator';
import { calculateProgress, getConditions } from './tracesIterator';

describe('Calculate progress percentage', () => {
  it('Calculates progress percentage', () => {
    expect(calculateProgress(0, 0)).toBe(100);
    expect(calculateProgress(0, 100)).toBe(100);
    expect(calculateProgress(0, -1)).toBe(0);
    expect(calculateProgress(1, -1)).toBe(0);
    expect(calculateProgress(-1, 1)).toBe(0);
    expect(calculateProgress(10, 9)).toBe(100);
    expect(calculateProgress(10, 11)).toBe(100);
    expect(calculateProgress(15, 7)).toBe(54);
    expect(calculateProgress(3, 0)).toBe(34);
  });
});

describe('Conditions based on the index position within an array', () => {
  // 10 transfers and current transfer has 10 traces
  it('returns end of last transfer', () => {
    expect(getConditions(9, 8, 9, 9, 1)).toEqual(END_OF_LAST_TRANSFER);
    expect(getConditions(9, 9, 9, 9, 1)).toEqual(END_OF_LAST_TRANSFER);
  });
  it('returns end of current transfer', () => {
    expect(getConditions(5, 9, 9, 9, 1)).toEqual(END_OF_CURRENT_TRANSFER);
    expect(getConditions(8, 9, 9, 9, 1)).toEqual(END_OF_CURRENT_TRANSFER);
  });
  it('returns empty', () => {
    expect(getConditions(5, 8, 9, 9, 1)).toEqual('');
    expect(getConditions(9, 7, 9, 9, 1)).toEqual('');
    expect(getConditions(-1, 0, 9, 9, 1)).toEqual('');
    expect(getConditions(0, 2, 9, 9, 1)).toEqual('');
  });
});
