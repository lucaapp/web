import React, { useEffect, useMemo, useState } from 'react';
import { useEnrichedTransfers } from 'components/hooks';
import { shareData } from 'network/api';
import { useIntl } from 'react-intl';
import { Divider } from 'antd';
import { Empty } from 'components/general/Empty';
import { Headline, Wrapper } from './ProgressStatusStep.styled';
import { getDecryptor } from './decryptor';
import { ProgressCard } from './ProgressCard';

export const ProgressStatusStep = ({ transfers, privateKey }) => {
  const intl = useIntl();
  const enrichedTransfers = useEnrichedTransfers(transfers);

  const decryptor = useMemo(
    () => getDecryptor(enrichedTransfers, privateKey),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const [progressStatus, setProgressStatus] = useState(decryptor.progress);
  const [counter, setCounter] = useState(0);
  const [isAllTracesProcessed, setIsAllTracesProcessed] = useState(false);
  const [showRequestTooBigError, setShowRequestTooBigError] = useState(false);
  const [statusFeedback, setStatusFeedback] = useState('');

  const processTraces = () => {
    if (!decryptor.isAllTransfersFinished) {
      decryptor.decryptChunk();
      // Give ant design progress bar UI enough time to update with the count
      setTimeout(() => {
        setProgressStatus(decryptor.progress);
        setCounter(counter + 1);
      }, 2000);
    } else {
      setIsAllTracesProcessed(true);
    }
  };

  const handleShareData = () => {
    for (const [key, value] of Object.entries(decryptor.validTraces)) {
      shareData({
        traces: {
          traces: value,
        },
        locationTransferId: key,
      })
        .then(({ status }) => {
          if (status === 204) {
            setStatusFeedback('success');
          } else if (status === 413) {
            setStatusFeedback('exception');
            setShowRequestTooBigError(true);
          } else {
            setStatusFeedback('exception');
          }
        })
        .catch(error => {
          setStatusFeedback('exception');
          console.error(error);
        });
    }
  };

  useEffect(
    () => (!isAllTracesProcessed ? processTraces() : handleShareData()),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [counter, isAllTracesProcessed]
  );

  if (!enrichedTransfers.length)
    return (
      <Empty
        description={intl.formatMessage({ id: 'shareData.progress.empty' })}
      />
    );

  return (
    <>
      <Headline>
        {intl.formatMessage({ id: 'shareData.progress.headline' })}
      </Headline>
      {enrichedTransfers.map((enrichedTransfer, index) => (
        <Wrapper key={enrichedTransfer.uuid}>
          <ProgressCard
            index={index}
            progressStatus={progressStatus}
            statusFeedback={statusFeedback}
            showRequestTooBigError={showRequestTooBigError}
            enrichedTransfer={enrichedTransfer}
          />
          {index < enrichedTransfers.length - 1 && <Divider />}
        </Wrapper>
      ))}
    </>
  );
};
