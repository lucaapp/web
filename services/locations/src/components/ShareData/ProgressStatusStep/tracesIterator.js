import {
  CHUNK_SIZE_PERCENTAGE,
  END_OF_CURRENT_TRANSFER,
  END_OF_LAST_TRANSFER,
} from 'constants/tracesIterator';

export const calculateProgress = (length, index) => {
  if (length < 0 || index < 0) return 0;
  if (length === 0 || index > length) return 100;
  return Math.ceil((100 * (index + 1)) / length);
};

/**
 * This function returns scenario based on the position within an array.
 * @returns {string}
 */
export const getConditions = (
  currentTransferIndex,
  currentTraceIndex,
  transfersEnd,
  tracesEnd,
  chunkSize
) => {
  if (
    currentTransferIndex >= transfersEnd &&
    currentTraceIndex >= tracesEnd - chunkSize
  ) {
    return END_OF_LAST_TRANSFER;
  }
  if (currentTransferIndex < transfersEnd && currentTraceIndex >= tracesEnd) {
    return END_OF_CURRENT_TRANSFER;
  }
  return '';
};

/**
 * This function returns an iterator to loop over the traces in chunks.
 * @param transferStart index of the first transfer
 * @param traceStart index of the first trace of the transfer
 * @param enrichedTransfers
 * @returns {} object containing relevant information about the chunk
 */
export const tracesIterator = (
  transferStart = 0,
  traceStart = -1,
  enrichedTransfers
) => {
  let currentTransferIndex = transferStart;
  let currentTraceIndex = traceStart;
  const transfersEnd = enrichedTransfers.length - 1;
  let currentTransfer = enrichedTransfers[transferStart];
  let tracesEnd = currentTransfer.traces.length - 1;

  // Computed as a fixed percentage of the transfer traces length
  let chunkSize = Math.round(
    CHUNK_SIZE_PERCENTAGE * currentTransfer.traces.length
  );

  return {
    next() {
      const tracesOfChunk = [];
      const start = currentTraceIndex + 1;
      let done = false;

      const conditions = getConditions(
        currentTransferIndex,
        currentTraceIndex,
        transfersEnd,
        tracesEnd,
        chunkSize
      );

      switch (conditions) {
        case END_OF_LAST_TRANSFER:
          // Math.min() to prevent overshoot of index
          currentTraceIndex = Math.min(
            currentTraceIndex + chunkSize,
            tracesEnd
          );
          done = true;
          break;
        case END_OF_CURRENT_TRANSFER:
          currentTransferIndex += 1;
          currentTransfer = enrichedTransfers[currentTransferIndex];
          tracesEnd = currentTransfer.traces.length - 1;
          chunkSize = Math.round(
            CHUNK_SIZE_PERCENTAGE * currentTransfer.traces.length
          );
          currentTraceIndex = chunkSize + 1;
          break;

        default:
          currentTraceIndex = Math.min(
            currentTraceIndex + 1 + chunkSize,
            tracesEnd
          );
      }

      for (let index = start; index <= currentTraceIndex; index++) {
        tracesOfChunk.push(currentTransfer.traces[index]);
      }

      return {
        indices: [currentTransferIndex, currentTraceIndex],
        traces: tracesOfChunk,
        progress: calculateProgress(
          currentTransfer.traces.length,
          currentTraceIndex
        ),
        currentTransfer,
        done,
      };
    },
  };
};
