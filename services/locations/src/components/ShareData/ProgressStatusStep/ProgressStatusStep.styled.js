import styled from 'styled-components';

export const Headline = styled.div`
  color: #000000;
  font-size: 20px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  line-height: 30px;
`;

export const Wrapper = styled.div``;
