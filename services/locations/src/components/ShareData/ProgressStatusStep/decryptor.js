import { TraceSchema } from 'utils/traceSchema';
import { decryptTraceByOperator } from '../ShareDataStep/ShareDataStep.helper';
import { tracesIterator } from './tracesIterator';

/**
 * This function provides object for chunk-wise trace decryption
 * @param enrichedTransfers
 * @param privateKey
 * @returns {}
 */
export const getDecryptor = (enrichedTransfers, privateKey) => {
  const decryptor = {
    enrichedTransfers,
    privateKey,
    isAllTransfersFinished: false,
    iterator: tracesIterator(0, -1, enrichedTransfers),
    progress: {},
    validTraces: {},

    decryptChunk() {
      // Get next chunk from the iterator
      const { traces, progress, currentTransfer, done } = this.iterator.next();
      // Decrypt chunk, parse and store valid traces
      traces.forEach(trace => {
        const newValue = decryptTraceByOperator(
          currentTransfer,
          trace,
          this.privateKey
        );
        const validatedTrace = TraceSchema.parse(newValue);
        this.validTraces[currentTransfer.uuid].push(validatedTrace);
      });
      // Update progress when finished
      this.isAllTransfersFinished = done;
      this.progress[currentTransfer.uuid] = progress;
    },
  };

  enrichedTransfers.forEach(transfer => {
    decryptor.progress[transfer.uuid] = 0;
    decryptor.validTraces[transfer.uuid] = [];
  });

  return decryptor;
};
