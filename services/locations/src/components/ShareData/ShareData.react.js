/* eslint-disable complexity */
import React, { useEffect, useState } from 'react';

import { Alert, Spin } from 'antd';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';
import { useParams, useHistory } from 'react-router-dom';

import { usePrivateKey } from 'utils/privateKey';
import { getSignatureValidityForAllTransfers } from 'utils/signatures';
import {
  getAllUncompletedTransfers,
  getLocationTransfer,
  getLastKeyUpdate,
} from 'network/api';
import { useGetPrivateKeySecret } from 'components/hooks';

import { Header } from 'components/Header';

import { LOGIN_ROUTE, BASE_DATA_TRANSFER_ROUTE } from 'constants/routes';
import { Content, SiteHeader } from 'components/general';
import {
  // Content,
  Main,
  RequestWrapper,
  StyledSecondaryButton,
} from './ShareData.styled';
import { PrivateKeyStep } from './PrivateKeyStep';
import { ShareDataStep } from './ShareDataStep';
import { ProgressStatusStep } from './ProgressStatusStep';

export const ShareData = () => {
  const intl = useIntl();
  const history = useHistory();
  const { transferId } = useParams();
  const [currentStep, setCurrentStep] = useState(0);
  const [privateKey, setPrivateKey] = useState();
  const [isPrivateKeyPreloaded, setIsPrivateKeyPreloaded] = useState(false);

  const {
    error: privateKeyError,
    data: privateKeySecret,
  } = useGetPrivateKeySecret();
  if (privateKeyError) {
    history.push(LOGIN_ROUTE);
  }
  const [existingPrivateKey] = usePrivateKey(privateKeySecret);
  const { isLoading: isKeyUpdateLoading, data: lastKeyUpdate } = useQuery(
    'lastKeyUpdate',
    getLastKeyUpdate
  );
  const { isLoading: isTransfersLoading, error, data: transfers } = useQuery(
    ['uncompletedTransfersWithValidity', transferId],
    async () => {
      const uncompletedTransfersWithValidity = transferId
        ? await getLocationTransfer(transferId).then(response => {
            return [response];
          })
        : await getAllUncompletedTransfers();

      return getSignatureValidityForAllTransfers(
        uncompletedTransfersWithValidity
      );
    }
  );

  useEffect(() => {
    if (existingPrivateKey) {
      if (!privateKey) {
        setCurrentStep(1);
        setIsPrivateKeyPreloaded(true);
      }
      setPrivateKey(existingPrivateKey);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [existingPrivateKey]);

  const progressStep = () => setCurrentStep(currentStep + 1);

  const setKey = key => setPrivateKey(key);

  if (isTransfersLoading || isKeyUpdateLoading) return <Spin />;

  const steps = [
    {
      id: '0',
      content: (
        <PrivateKeyStep
          next={progressStep}
          setPrivateKey={setKey}
          privateKeySecret={privateKeySecret}
        />
      ),
    },
    {
      id: '1',
      content: (
        <ShareDataStep
          next={progressStep}
          transfers={transfers}
          privateKey={privateKey}
          lastKeyUpdate={lastKeyUpdate}
          showStepLabel={!isPrivateKeyPreloaded}
          title={intl.formatMessage({ id: 'shareData.shareDataStep.title' })}
        />
      ),
    },
    {
      id: '2',
      content: (
        <ProgressStatusStep
          transfers={transfers}
          privateKey={privateKey}
          showStepLabel={!isPrivateKeyPreloaded}
          title={intl.formatMessage({ id: 'shareData.shareDataStep.title' })}
        />
      ),
    },
  ];

  const backToLocations = () => history.push(BASE_DATA_TRANSFER_ROUTE);

  const getHeaderActions = () => (
    <StyledSecondaryButton onClick={backToLocations}>
      {intl.formatMessage({ id: 'shareData.backToLocations' })}
    </StyledSecondaryButton>
  );

  return (
    <>
      <SiteHeader
        title={intl.formatMessage({ id: 'shareData.site.title' })}
        meta={intl.formatMessage({ id: 'shareData.site.meta' })}
      />
      <Main style={{ backgroundColor: 'black' }}>
        <Header
          title={intl.formatMessage({ id: 'shareData.header.title' })}
          actions={getHeaderActions()}
        />
        {error?.status === 410 && (
          <Alert
            style={{ textAlign: 'center', marginTop: 48 }}
            type="success"
            message={intl.formatMessage({ id: 'shareData.completed' })}
          />
        )}
        {error?.status > 400 && error?.status !== 410 && (
          <Alert
            style={{ textAlign: 'center', marginTop: 48 }}
            type="error"
            message={intl.formatMessage({ id: 'shareData.noData' })}
          />
        )}
        <Content>
          {!error && transfers && (
            <RequestWrapper>
              <>{steps[currentStep].content}</>
            </RequestWrapper>
          )}
        </Content>
      </Main>
    </>
  );
};
