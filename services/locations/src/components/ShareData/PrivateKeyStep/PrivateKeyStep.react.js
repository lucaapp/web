import React from 'react';

import { useHistory, useLocation } from 'react-router';

import { BASE_ROUTE } from 'constants/routes';
import { PrivateKeyLoader } from 'components/PrivateKeyLoader';
import { RequestContent, StepLabel } from '../ShareData.styled';

export const PrivateKeyStep = ({ next, setPrivateKey, privateKeySecret }) => {
  const history = useHistory();
  const location = useLocation();

  const handleError = () => {
    history.push(`${BASE_ROUTE}?redirect=${location.pathname}`);
  };

  return (
    <RequestContent>
      <StepLabel>1/2</StepLabel>
      <PrivateKeyLoader
        onSuccess={privateKey => {
          setPrivateKey(privateKey);
          setTimeout(next, 300);
        }}
        onError={handleError}
        infoTextId="shareData.privateKeyStep.info"
        privateKeySecret={privateKeySecret}
      />
    </RequestContent>
  );
};
