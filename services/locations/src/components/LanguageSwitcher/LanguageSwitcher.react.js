import React, { useMemo } from 'react';
import { DownOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import { useIntl } from 'react-intl';

import { excludeCurrentLanguageFromLocales } from 'utils/language';
import { useLanguage } from 'store-context/selectors';
import { StyledButton, StyledDropdown } from './LanguageSwitcher.styled';

export const LanguageSwitcher = () => {
  const { locale } = useIntl();
  const { changeLanguage } = useLanguage();

  const languages = useMemo(() => excludeCurrentLanguageFromLocales(locale), [
    locale,
  ]);

  const handleChange = ({ key: language }) => {
    changeLanguage(language);
  };

  const menu = (
    <Menu onClick={handleChange}>
      {languages.map(language => (
        <Menu.Item key={language}>{language.toUpperCase()}</Menu.Item>
      ))}
    </Menu>
  );

  return (
    <StyledDropdown overlay={menu}>
      <StyledButton>
        {locale.toUpperCase()} <DownOutlined />
      </StyledButton>
    </StyledDropdown>
  );
};
