import styled from 'styled-components';

import { Button, Dropdown } from 'antd';

export const StyledButton = styled(Button)`
  background: none;
  border: 0;
  box-shadow: none;
  font-weight: bold;
  padding: 0;

  &:hover,
  &:focus {
    color: #000;
    background: none;
    border: none;
  }
`;

export const StyledDropdown = styled(Dropdown)`
  width: auto;
`;
