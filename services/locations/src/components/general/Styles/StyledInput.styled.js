import styled from 'styled-components';
import { Form, Input } from 'antd';
import { Media } from 'utils/media';

export const StyledInput = styled(Input)`
  border-radius: 0;
  border: 0.5px solid rgb(0, 0, 0);
  height: 40px;

  :focus,
  :hover,
  :active {
    border-radius: 0;
    border: 0.5px solid rgb(0, 0, 0);
    height: 40px;
  }
`;

export const StyledFormItem = styled(Form.Item)`
  width: ${({ width }) => (width ? `${width}` : '100%')};
  padding-right: ${({ $paddingRight }) =>
    $paddingRight ? `${$paddingRight}` : '0'};

  ${Media.mobile`
    width: 100%;
    padding-right: 0;
  `}
`;
