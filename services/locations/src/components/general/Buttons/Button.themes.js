export const defaultPrimaryButtonTheme = {
  main: 'rgb(195, 206, 217)',
  focus: 'rgb(155, 173, 191)',
  hover: 'rgb(155, 173, 191)',
  disabled: 'rgb(218, 224, 231)',
};

const paymentButtonColor = 'rgb(255, 220, 95)';

export const paymentPrimaryButtonTheme = {
  main: paymentButtonColor,
  focus: paymentButtonColor,
  hover: paymentButtonColor,
  disabled: paymentButtonColor,
};
