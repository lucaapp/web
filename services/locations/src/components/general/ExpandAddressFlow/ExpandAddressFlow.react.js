import React from 'react';
import { Form } from 'antd';
import { useIntl } from 'react-intl';
import {
  Expand,
  StyledSwitchContainer,
  ToggleDescription,
  Wrapper,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import {
  FormFields,
  GooglePlacesWrapper,
  GoogleAddress,
} from 'components/general/GoogleService';
import { Switch } from 'components/general/Switch';

export const ExpandAddressFlow = ({
  handleSwitch,
  enabled,
  form,
  handleSubmit,
  initialValues,
  coordinates,
  setCoordinates,
  displayManualAddress,
  businessAddressEnabled,
  isError,
  setIsError,
  filled,
  setFilled,
  hasPlace,
  setHasPlace,
  isBaseLocation,
  isGroup,
}) => {
  const intl = useIntl();

  const getDescription = () => {
    if (displayManualAddress && (isBaseLocation || isGroup))
      return 'modal.editAddress.manualInput.useBusinessAddress';
    if (displayManualAddress && !isBaseLocation)
      return 'modal.editAddress.manualInput.useGroupAddress';

    return 'modal.editAddress.activateGoogleService';
  };

  return (
    <>
      <StyledSwitchContainer>
        <ToggleDescription>
          {intl.formatMessage({
            id: getDescription(),
          })}
        </ToggleDescription>
        <Switch
          data-cy={
            displayManualAddress
              ? 'toggleUseBusinessAddress'
              : 'toggleGoogleService'
          }
          checked={displayManualAddress ? businessAddressEnabled : enabled}
          onChange={handleSwitch}
        />
      </StyledSwitchContainer>

      <Expand open={displayManualAddress || enabled}>
        {displayManualAddress && (
          <Wrapper>
            <Form
              form={form}
              onFinish={handleSubmit}
              initialValues={initialValues}
            >
              <FormFields visible />
            </Form>
          </Wrapper>
        )}
        {enabled && (
          <Wrapper>
            <GooglePlacesWrapper enabled>
              <Form
                form={form}
                onFinish={handleSubmit}
                initialValues={initialValues}
              >
                <GoogleAddress
                  coordinates={coordinates}
                  setCoordinates={setCoordinates}
                  form={form}
                  setFilled={setFilled}
                  filled={filled}
                  setIsError={setIsError}
                  isError={isError}
                  hasPlace={hasPlace}
                  setHasPlace={setHasPlace}
                />
              </Form>
            </GooglePlacesWrapper>
          </Wrapper>
        )}
      </Expand>
    </>
  );
};
