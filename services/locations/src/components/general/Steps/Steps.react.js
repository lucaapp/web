import React from 'react';

import { Steps as AntdSteps } from 'antd';

import { Wrapper } from './Steps.styled';

export const Steps = ({ steps, currentStep }) => (
  <Wrapper>
    <AntdSteps current={currentStep} progressDot={() => null}>
      {steps.map(step => (
        <AntdSteps.Step key={step.id} />
      ))}
    </AntdSteps>
    {steps[currentStep].content}
  </Wrapper>
);
