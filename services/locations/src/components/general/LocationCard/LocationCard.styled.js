import styled from 'styled-components';
import { Collapse } from 'antd';
import Icon from '@ant-design/icons';

export const StyledIcon = styled(Icon)`
  font-size: 16px;
  color: rgb(80, 102, 124);
  margin-top: 9px;
`;

export const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;
  background: rgb(255, 255, 255);
  border-radius: 8px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.15);
  margin-top: 16px;
  padding: ${({ isCollapse }) => (isCollapse ? 'none' : '20px 16px 0 16px')};
`;

export const StyledTitle = styled.div`
  align-items: center;
  display: flex;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: ${({ isDeviceList }) => (isDeviceList ? '14px' : '16px')};
  font-weight: bold;
  padding: 4px 16px;
  width: 100%;
  white-space: nowrap;
  justify-content: ${({ isFlexEnd }) =>
    isFlexEnd ? 'flex-end' : 'flex-start'};
`;

export const StyledPlaceholder = styled.div`
  padding-bottom: 32px;
`;

export const CardSection = styled.div`
  width: 100%;
  display: flex;
  padding: 16px 16px 0 16px;
  flex-direction: column;
  align-items: ${({ direction }) =>
    direction === 'end' ? `flex-end` : `flex-start`};
  border-bottom: ${({ isLast }) => (isLast ? 0 : 1)}px solid rgb(151, 151, 151);
`;

export const CardSectionTitle = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

export const StyledCardSectionTitle = styled(CardSectionTitle)`
  align-items: center;
  margin-bottom: 5px;
  justify-content: unset;
`;

export const CardSectionTitleWrapper = styled.div`
  display: flex;
`;

export const TitleContainer = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-family: Montserrat-SemiBold, sans-serif;
  font-size: 14px;
  font-weight: 600;
  margin-bottom: 5px;
  word-break: keep-all;
`;

export const StyledTitleContainer = styled(TitleContainer)`
  margin-bottom: 0;
`;

export const CardSectionSubTitle = styled(TitleContainer)`
  font-weight: 400;
`;

export const CardSectionDescription = styled.div`
  width: 100%;
  font-size: 14px;
  margin-bottom: 24px;
  font-weight: 500;
  color: rgba(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
`;

export const StyledCollapse = styled(Collapse)`
  .ant-collapse-header {
    display: flex !important;
    justify-content: space-between !important;
    flex-direction: row-reverse !important;
  }
`;
