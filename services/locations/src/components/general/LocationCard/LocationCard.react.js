import React, { useState } from 'react';
import { InformationIcon } from 'components/general/InformationIcon';
import { ArrowIcon } from 'components/general/ArrowIcon';
import { StyledTooltip } from 'components/general/StyledTooltip';
import {
  StyledCollapse,
  StyledContainer,
  StyledPlaceholder,
  StyledTitle,
} from './LocationCard.styled';

const { Panel } = StyledCollapse;
const CONTENT_KEY = 'CONTENT';

export const LocationCard = ({
  title,
  testId,
  children,
  tooltip,
  description,
  open = false,
  isCollapse = false,
  isFlexEnd = false,
  isDeviceList = false,
}) => {
  const [isOpen, setIsOpen] = useState(open);

  if (!isCollapse) {
    return (
      <StyledContainer data-cy={`locationCard-${testId}`}>
        {title && (
          <StyledTitle
            showBorder
            isFlexEnd={isFlexEnd}
            isDeviceList={isDeviceList}
          >
            {title}
          </StyledTitle>
        )}
        {children}
        <StyledPlaceholder />
      </StyledContainer>
    );
  }

  const collapseClassName = isOpen ? ' locationCard-open' : '';
  const extendedClass =
    collapseClassName && description
      ? `${collapseClassName}-with-description`
      : collapseClassName;

  return (
    <StyledContainer data-cy={`locationCard-${testId || ''}`} isCollapse>
      <StyledCollapse
        bordered={false}
        className={`locationCard${extendedClass}`}
        defaultActiveKey={open ? [CONTENT_KEY] : []}
        expandIconPosition="right"
        style={{
          background: 'transparent',
        }}
        onChange={key => setIsOpen(key.length > 0)}
        expandIcon={({ isActive }) => <ArrowIcon isActive={isActive} />}
      >
        <Panel
          key={CONTENT_KEY}
          header={
            <StyledTitle data-cy={`${testId}Title`}>
              {title}
              {tooltip && (
                <StyledTooltip title={tooltip}>
                  <InformationIcon />
                </StyledTooltip>
              )}
            </StyledTitle>
          }
        >
          {description && (
            <div
              className="locationCard-panel-description"
              data-cy={`${testId}PanelDescription`}
            >
              {description}
            </div>
          )}
          {children}
        </Panel>
      </StyledCollapse>
    </StyledContainer>
  );
};
