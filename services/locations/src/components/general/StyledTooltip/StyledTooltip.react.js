/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Tooltip } from 'antd';

import { getOverlayInnerStyle } from './StyledTooltip.styled';

export const StyledTooltip = ({
  title,
  children,
  placement = 'top',
  trigger = 'hover',
  minWidth = 0,
  ...properties
}) => {
  return (
    <Tooltip
      placement={placement}
      title={title}
      trigger={trigger}
      overlayInnerStyle={getOverlayInnerStyle(minWidth)}
      {...properties}
    >
      {children}
    </Tooltip>
  );
};
