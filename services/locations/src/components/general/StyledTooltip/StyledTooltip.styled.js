export const getOverlayInnerStyle = (minWidth = 0) => ({
  color: 'rgb(0, 0, 0)',
  fontFamily: 'Montserrat-Medium, sans-serif',
  fontSize: 14,
  fontWeight: 500,
  background: 'rgb(218, 224, 231)',
  boxShadow: 'none',
  padding: '8px 16px',
  borderRadius: 8,
  minWidth,
});
