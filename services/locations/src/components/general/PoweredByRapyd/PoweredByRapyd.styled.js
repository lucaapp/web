import styled from 'styled-components';

export const PoweredByWrapper = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-MediumItalic, sans-serif;
  font-size: 14px;
  font-style: italic;
  font-weight: 500;
  text-align: right;
  padding: 10px 3px 0 0;
`;
