import React from 'react';
import { useIntl } from 'react-intl';

import { PoweredByWrapper } from './PoweredByRapyd.styled';

export const PoweredByRapyd = () => {
  const intl = useIntl();
  return (
    <PoweredByWrapper>
      {intl.formatMessage({
        id: 'payment.poweredBy',
      })}
    </PoweredByWrapper>
  );
};
