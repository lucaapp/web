import React from 'react';
import { Spin } from 'antd';
import { useGetLocationTableByTableId } from 'components/hooks/useQueries';

const LOCATION_TABLES_ID_BYTES = 16;
const LOCATION_TABLES_ID_LENGTH = Math.ceil((4 * LOCATION_TABLES_ID_BYTES) / 3);

export const TableName = ({ tableId }) => {
  // locationTable {id, customName, internalNumber, locationId}
  const {
    isLoading,
    error,
    data: locationTable,
  } = useGetLocationTableByTableId(tableId, {
    enabled: tableId?.length === LOCATION_TABLES_ID_LENGTH || false,
  });

  if (!tableId) return null;

  if (error || tableId.length !== LOCATION_TABLES_ID_LENGTH) return tableId;

  if (isLoading) {
    return <Spin />;
  }

  const getName = () =>
    locationTable?.customName || locationTable?.internalNumber || tableId;

  return <span>{getName()}</span>;
};
