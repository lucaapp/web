import React from 'react';
import { Checkbox, Form } from 'antd';
import { useIntl } from 'react-intl';

export const ConsentCheckbox = ({
  name,
  onChange,
  errorMessage,
  dataCy,
  label,
  link,
  download,
}) => {
  const intl = useIntl();

  return (
    <Form.Item
      name={name}
      valuePropName="checked"
      rules={[
        {
          validator: (_, value) =>
            value
              ? Promise.resolve()
              : Promise.reject(
                  intl.formatMessage({
                    id: errorMessage,
                  })
                ),
        },
      ]}
    >
      <Checkbox data-cy={dataCy} onChange={onChange}>
        {intl.formatMessage(
          { id: label },
          {
            // eslint-disable-next-line react/display-name
            a: (...chunks) => (
              <a
                href={link}
                download={download}
                target="_blank"
                rel="noopener noreferrer"
              >
                {chunks}
              </a>
            ),
          }
        )}
      </Checkbox>
    </Form.Item>
  );
};
