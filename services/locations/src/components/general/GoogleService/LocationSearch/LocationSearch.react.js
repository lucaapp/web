import React, { useState, useCallback } from 'react';
import { useIntl } from 'react-intl';
import { Autocomplete } from '@react-google-maps/api';

import { LensBlackIcon } from 'assets/icons';
import { useNotifications } from 'components/hooks';
import {
  StyledInput,
  StyledIcon,
  StyledItem,
  SearchWrapper,
} from './LocationSearch.styled';

export const LocationSearch = ({
  form,
  setCoordinates,
  setFilled,
  setIsError,
  isError,
  setHasPlace = () => {},
  setAddressFromPlace = () => {},
}) => {
  const intl = useIntl();
  const { errorMessage } = useNotifications();

  const [map, setMap] = useState(null);

  const onLoad = useCallback(mapInstance => {
    setMap(mapInstance);
  }, []);

  const autofillForm = place => {
    const address = {
      streetName: '',
      streetNr: '',
      zipCode: '',
      city: '',
      state: '',
    };
    place.address_components.forEach(addressComponent => {
      if (addressComponent.types.includes('street_number')) {
        address.streetNr = addressComponent.long_name;
      }
      if (addressComponent.types.includes('route')) {
        address.streetName = addressComponent.long_name;
      }
      if (addressComponent.types.includes('postal_code')) {
        address.zipCode = addressComponent.long_name;
      }
      if (addressComponent.types.includes('locality')) {
        address.city = addressComponent.long_name;
      }
      if (addressComponent.types.includes('administrative_area_level_1')) {
        address.state = addressComponent.long_name;
      }
    });

    if (isError) {
      setIsError(false);
    }

    if (!place.geometry.location.lat() || !place.geometry.location.lng()) {
      setIsError(true);

      errorMessage(
        'autocomplete.fill.notSupported.description',
        'autocomplete.fill.title'
      );
    }

    const autofill = {
      locationName: place.name,
      phone: place.formatted_phone_number,
      streetName: address.streetName,
      streetNr: address.streetNr,
      zipCode: address.zipCode,
      city: address.city,
      state: address.state,
    };
    form.setFieldsValue(autofill);
    setCoordinates({
      lat: place.geometry.location.lat(),
      lng: place.geometry.location.lng(),
    });

    setFilled(
      !!autofill?.streetName &&
        !!autofill?.streetNr &&
        !!autofill?.city &&
        !!autofill?.zipCode
    );
    setAddressFromPlace(autofill);
  };

  const onPlaceChanged = () => {
    const place = map.getPlace();

    if (!Object.keys(place).includes('address_components')) {
      errorMessage('autocomplete.fill.description', 'autocomplete.fill.title');

      return;
    }

    setHasPlace(true);
    autofillForm(place);
  };

  return (
    <SearchWrapper>
      <StyledIcon component={LensBlackIcon} />
      <Autocomplete onLoad={onLoad} onPlaceChanged={onPlaceChanged}>
        <StyledItem name="googleApi">
          <StyledInput
            id="locationSearch"
            placeholder={intl.formatMessage({ id: 'createLocation.form.find' })}
            autoFocus
          />
        </StyledItem>
      </Autocomplete>
    </SearchWrapper>
  );
};
