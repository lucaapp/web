import React from 'react';

import { useIntl } from 'react-intl';
import {
  useCityValidator,
  useHouseNoValidator,
  useStreetValidator,
  useZipCodeValidator,
} from 'components/hooks/useValidators';
import { StyledInput } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { HiddenInputs } from './HiddenInputs';
import {
  RowWrapper,
  StyledFormItemLarge,
  StyledFormItemSmall,
} from './FormFields.styled';

export const FormFields = ({ visible }) => {
  const intl = useIntl();
  const streetValidator = useStreetValidator('streetName');
  const houseNoValidator = useHouseNoValidator('streetNr');
  const zipCodValidator = useZipCodeValidator('zipCode');
  const cityValidator = useCityValidator('city');

  return (
    <div>
      <RowWrapper>
        <StyledFormItemLarge
          $visible={visible}
          rules={streetValidator}
          key="streetName"
          name="streetName"
          label={intl.formatMessage({
            id: 'card.address.streetName',
          })}
        >
          <StyledInput />
        </StyledFormItemLarge>
        <StyledFormItemSmall
          $visible={visible}
          rules={houseNoValidator}
          key="streetNr"
          name="streetNr"
          label={intl.formatMessage({
            id: 'card.address.streetNr',
          })}
        >
          <StyledInput />
        </StyledFormItemSmall>
      </RowWrapper>

      <RowWrapper>
        <StyledFormItemSmall
          $visible={visible}
          rules={zipCodValidator}
          key="zipCode"
          name="zipCode"
          label={intl.formatMessage({
            id: 'card.address.zipCode',
          })}
        >
          <StyledInput />
        </StyledFormItemSmall>
        <StyledFormItemLarge
          $visible={visible}
          rules={cityValidator}
          key="city"
          name="city"
          label={intl.formatMessage({
            id: 'card.address.city',
          })}
        >
          <StyledInput />
        </StyledFormItemLarge>
      </RowWrapper>

      <HiddenInputs />
    </div>
  );
};
