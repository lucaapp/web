import React from 'react';
import { StyledSticker } from './OptionalSticker.styled';

export const OptionalSticker = ({ dataCy }) => {
  return (
    <StyledSticker data-cy={`optionalSticker-${dataCy}`}>
      OPTIONAL
    </StyledSticker>
  );
};
