import React from 'react';
import { useIntl } from 'react-intl';

import { InformationIcon, StyledTooltip, Switch } from 'components/general';

import {
  StyledCardSection,
  StyledCardSectionTitle,
  StyledSwitchContainer,
} from 'components/App/Dashboard/Location/GenerateQRCodes/GenerateQRCodes.styled';

export const QrCwaCompatible = ({
  setIsCWAEventEnabled,
  isCWAEventEnabled,
}) => {
  const intl = useIntl();

  return (
    <StyledCardSection data-cy="qrCodeCompatabilitySection" isLast>
      <StyledCardSectionTitle>
        {intl.formatMessage({ id: 'location.setting.qr.compatibility' })}
        <StyledTooltip
          title={intl.formatMessage({
            id: 'settings.location.qrcode.cwaInfoText',
          })}
        >
          <InformationIcon data-cy="qrCodeCompatabilityInfoIcon" />
        </StyledTooltip>
        <StyledSwitchContainer>
          <Switch
            data-cy="qrCodeCompatabilityToggle"
            checked={isCWAEventEnabled}
            onChange={() => setIsCWAEventEnabled(!isCWAEventEnabled)}
          />
        </StyledSwitchContainer>
      </StyledCardSectionTitle>
    </StyledCardSection>
  );
};
