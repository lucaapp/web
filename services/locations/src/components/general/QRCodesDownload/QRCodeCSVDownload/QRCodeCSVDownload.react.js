import React from 'react';
import { useIntl } from 'react-intl';
import { CSVLink } from 'react-csv';
import {
  getCWAFragment,
  getLocationName,
  getQRCodeFilename,
} from 'utils/qrCodeData';

import { sanitizeForCSV } from 'utils/sanitizer';
import { bytesToBase64Url } from 'utils/encodings';
import { WEB_APP_BASE_PATH } from 'constants/links';
import { DisabledLink, linkButtonStyle } from './QRCodeCSVDownload.styled';

const generateLocationQrCode = (
  location,
  sharedContentPart,
  isCWAEventEnabled
) => {
  const qrCodeContent = `${sharedContentPart}#${bytesToBase64Url(
    JSON.stringify({})
  )}${getCWAFragment(location, isCWAEventEnabled)}`;
  const row = [sanitizeForCSV(getLocationName(location)), qrCodeContent];
  return [row];
};

const generateTableQrCodes = (
  location,
  locationTables,
  sharedContentPart,
  isCWAEventEnabled,
  intl
) => {
  const contentList = [];

  for (const locationTable of locationTables) {
    const additionalData = bytesToBase64Url(
      JSON.stringify({
        tableId: locationTable.id,
      })
    );

    const qrCodeContent = `${sharedContentPart}#${additionalData}${getCWAFragment(
      location,
      isCWAEventEnabled
    )}`;

    const tableKey = locationTable.customName || locationTable.internalNumber;

    const rowKey = `${intl.formatMessage({
      id: 'modal.qrCodeDocument.table',
    })} ${tableKey}`;

    const row = [rowKey, qrCodeContent];

    contentList.push(row);
  }
  return contentList;
};

const getCSVFileContentFromLocation = (
  location,
  locationTables,
  isTableQRCodeEnabled,
  isCWAEventEnabled,
  intl
) => {
  const sharedContentPart = `${WEB_APP_BASE_PATH}${location.scannerId}`;
  const contentRows = !isTableQRCodeEnabled
    ? generateLocationQrCode(location, sharedContentPart, isCWAEventEnabled)
    : generateTableQrCodes(
        location,
        locationTables,
        sharedContentPart,
        isCWAEventEnabled,
        intl
      );

  return [
    [
      intl.formatMessage({ id: 'location.csv.name' }),
      intl.formatMessage({ id: 'location.csv.content' }),
    ],
    ...contentRows,
  ];
};

export const QRCodeCSVDownload = ({
  location,
  locationTables,
  isCWAEventEnabled,
  isTableQRCodeEnabled,
  disabled,
}) => {
  const intl = useIntl();
  const filename = getQRCodeFilename(
    location,
    isTableQRCodeEnabled,
    intl,
    'csv'
  );

  return disabled ? (
    <DisabledLink>
      {intl.formatMessage({
        id: 'settings.location.qrcode.generateCsv',
      })}
    </DisabledLink>
  ) : (
    <CSVLink
      data-cy="qrCodeDownloadCsv"
      style={linkButtonStyle}
      data={getCSVFileContentFromLocation(
        location,
        locationTables,
        isTableQRCodeEnabled,
        isCWAEventEnabled,
        intl
      )}
      filename={filename}
    >
      {intl.formatMessage({
        id: 'settings.location.qrcode.generateCsv',
      })}
    </CSVLink>
  );
};
