import styled from 'styled-components';
import { buttonStyle } from 'components/App/Dashboard/Location/GenerateQRCodes/GenerateQRCodes.styled';

export const linkButtonStyle = {
  ...buttonStyle,
  marginBottom: 0,
  color: 'rgb(80, 102, 124)',
  padding: '0 16px 0 0',
  textDecoration: 'none',
  alignSelf: 'center',
  border: 'none',
  boxShadow: 'none',
};

export const DisabledLink = styled.span`
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  color: rgb(211, 211, 211);
  padding-right: 16px;
`;
