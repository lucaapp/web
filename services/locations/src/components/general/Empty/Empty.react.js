import React from 'react';

export const Empty = ({ children, image, description }) => {
  return (
    <Empty image={image} description={description}>
      {children}
    </Empty>
  );
};
