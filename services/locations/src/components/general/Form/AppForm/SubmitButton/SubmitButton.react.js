import React from 'react';
import { useFormikContext } from 'formik';
import { ThemeProvider } from 'styled-components';

import { defaultPrimaryButtonTheme, PrimaryButton } from 'components/general';
import { ButtonWrapper } from './SubmitButton.styled';

export const SubmitButton = ({
  title,
  dataCy,
  loading,
  theme = defaultPrimaryButtonTheme,
  disableWhenInvalid = false,
}) => {
  const { handleSubmit, isValid } = useFormikContext();
  let validForm = true;

  if (disableWhenInvalid) {
    validForm = isValid;
  }

  return (
    <ButtonWrapper>
      <ThemeProvider theme={theme}>
        <PrimaryButton
          data-cy={dataCy}
          onClick={handleSubmit}
          loading={loading}
          disabled={!validForm}
        >
          {title}
        </PrimaryButton>
      </ThemeProvider>
    </ButtonWrapper>
  );
};
