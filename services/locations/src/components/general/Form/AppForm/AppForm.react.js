import React from 'react';
import { Formik } from 'formik';

import { WithTranslateFormErrors } from '../hoc';
import { StyledForm } from './AppForm.styled';

export const AppForm = ({
  children,
  form,
  initialValues,
  onSubmit,
  validationSchema,
  enableReinitialize = false,
  dataCy,
  onValuesChange,
}) => {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
      enableReinitialize={enableReinitialize}
    >
      <WithTranslateFormErrors>
        <StyledForm
          form={form}
          onValuesChange={onValuesChange}
          data-cy={dataCy}
        >
          {() => <>{children}</>}
        </StyledForm>
      </WithTranslateFormErrors>
    </Formik>
  );
};
