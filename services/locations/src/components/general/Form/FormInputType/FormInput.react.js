import React from 'react';
import { useIntl } from 'react-intl';
import { StyledInput, StyledFormItem } from 'components/general/Styles';
import { InputType } from './InputType';

export const FormInput = ({
  StyledInputComponent = StyledInput,
  name,
  placeholder,
  disabled,
  suffix,
  autoFocus,
  dataCy,
  labelId,
  type = 'text',
  required,
  width,
  paddingRight,
}) => {
  const intl = useIntl();

  return (
    <StyledFormItem
      colon={false}
      label={intl.formatMessage({
        id: labelId,
      })}
      required={required}
      $width={width}
      $paddingRight={paddingRight}
    >
      <InputType
        component={StyledInputComponent}
        autoComplete="off"
        name={name}
        placeholder={placeholder}
        disabled={disabled}
        suffix={suffix}
        autoFocus={autoFocus}
        data-cy={dataCy}
        type={type}
      />
    </StyledFormItem>
  );
};
