import React from 'react';
import { useFormikContext } from 'formik';
import { ErrorMessage } from 'components/general/Form/index';

export const InputType = ({
  component: Component,
  name,
  label,
  ...properties
}) => {
  const {
    setFieldTouched,
    handleChange,
    errors,
    touched,
    values,
  } = useFormikContext();

  return (
    <>
      <Component
        name={name}
        id={name}
        onBlur={() => setFieldTouched(name)}
        onChange={handleChange(name)}
        value={values[name]}
        {...properties}
      >
        {label}
      </Component>
      <ErrorMessage
        error={errors[name]}
        visible={touched[name]}
        dataCy={`error-${name}`}
      />
    </>
  );
};
