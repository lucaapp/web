import { FormInput } from './FormInput.react';
import { FormCheckbox } from './FormCheckbox.react';

const FormInputType = { Checkbox: FormCheckbox, Input: FormInput };

export { FormInputType };
