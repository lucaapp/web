import { useIntl } from 'react-intl';
import { MAX_NAME_LENGTH } from 'constants/valueLength';
import { Yup } from './yupConfig';

export const useEmployeeNameSchema = () => {
  const intl = useIntl();

  return Yup.object().shape({
    firstName: Yup.string()
      .strict(false)
      .trim()
      .required(intl.formatMessage({ id: 'error.firstName' }))
      .max(MAX_NAME_LENGTH)
      .safeString(intl.formatMessage({ id: 'error.firstName.invalid' })),
    lastName: Yup.string()
      .strict(false)
      .trim()
      .required(intl.formatMessage({ id: 'error.lastName' }))
      .max(MAX_NAME_LENGTH)
      .safeString(intl.formatMessage({ id: 'error.lastName.invalid' })),
  });
};
