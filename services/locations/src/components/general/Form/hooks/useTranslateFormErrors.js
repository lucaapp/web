import { useEffect } from 'react';
import { useFormikContext } from 'formik';

import { useLanguage } from 'store-context/selectors';

export const useTranslateFormErrors = () => {
  const { setFieldTouched, errors, touched } = useFormikContext();

  const { currentLanguage } = useLanguage();

  useEffect(() => {
    Object.keys(errors).forEach(fieldName => {
      if (Object.keys(touched).includes(fieldName)) {
        setFieldTouched(fieldName);
      }
    });
  }, [currentLanguage, errors, touched, setFieldTouched]);
};
