import * as Yup from 'yup';
import validator from 'validator';
import {
  isValidMobilePhoneNumber,
  isValidPhoneNumber,
  isValidCharacter,
  isNotDefaultLocationName,
  validateVatNumber,
} from 'utils/validators';

Yup.addMethod(Yup.string, 'phoneNumber', function handleError(errorMessage) {
  return this.test(
    'test-phone-number',
    errorMessage,
    value => value && isValidPhoneNumber(value)
  );
});

Yup.addMethod(Yup.string, 'mobilePhoneNumber', function handleError(intl) {
  return this.test(
    'test-mobile-phone-number',
    intl.formatMessage({ id: 'error.phone.invalid' }),
    value => value && isValidMobilePhoneNumber(value)
  );
});

Yup.addMethod(Yup.string, 'safeString', function handleError(errorMessage) {
  return this.test('test-safe-string', errorMessage, value => {
    if (value === undefined) return true;
    return isValidCharacter(value);
  });
});

Yup.addMethod(
  Yup.string,
  'defaultLocationName',
  function handleError(errorMessage) {
    return this.test('test-default-locationName', errorMessage, value => {
      if (value === undefined) return true;
      return isNotDefaultLocationName(value);
    });
  }
);

Yup.addMethod(Yup.string, 'zipCode', function handleError(errorMessage) {
  return this.test('test-zip-code', errorMessage, value => {
    if (value === undefined) return true;
    return validator.isPostalCode(value, 'DE');
  });
});

Yup.addMethod(Yup.string, 'vatNumber', function handleError(errorMessage) {
  return this.test('test-vat-number', errorMessage, value => {
    if (value === undefined) return true;
    return validateVatNumber(value);
  });
});

export * as Yup from 'yup';
