import { useIntl } from 'react-intl';
import { MAX_LOCATION_NAME_LENGTH } from 'constants/valueLength';
import { Yup } from './yupConfig';

export const useEditProfileSchema = isLocationNameDisabled => {
  const intl = useIntl();

  const groupSchema = Yup.object().shape({
    name: Yup.string()
      .strict(false)
      .trim()
      .required(intl.formatMessage({ id: 'error.groupName' }))
      .max(MAX_LOCATION_NAME_LENGTH)
      .safeString(intl.formatMessage({ id: 'error.groupName.invalid' })),
    phone: Yup.string()
      .required(intl.formatMessage({ id: 'error.phone' }))
      .mobilePhoneNumber(intl),
  });

  const locationSchema = Yup.object().shape({
    name: Yup.string()
      .when([], {
        is: () => !isLocationNameDisabled,
        then: Yup.string().required(
          intl.formatMessage({ id: 'error.locationName' })
        ),
        otherwise: Yup.string().notRequired(),
      })
      .max(MAX_LOCATION_NAME_LENGTH)
      .safeString(intl.formatMessage({ id: 'error.locationName.invalid' }))
      .defaultLocationName(
        intl.formatMessage({ id: 'error.locationName.notDefault' })
      ),
    phone: Yup.string()
      .required(intl.formatMessage({ id: 'error.phone' }))
      .mobilePhoneNumber(intl),
  });

  return { groupSchema, locationSchema };
};
