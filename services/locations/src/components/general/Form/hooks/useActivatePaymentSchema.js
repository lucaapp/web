import * as Yup from 'yup';
import { useIntl } from 'react-intl';

import { useInvoiceDataSchema } from './useInvoiceDataSchema';

export const useActivatePaymentSchema = () => {
  const intl = useIntl();

  const errorDPA = intl.formatMessage({
    id: 'error.dpa',
  });

  const errorTermsAndConditions = intl.formatMessage({
    id: 'error.termsAndConditions',
  });

  const invoiceDataSchema = useInvoiceDataSchema();

  return invoiceDataSchema.concat(
    Yup.object().shape({
      phone: Yup.string()
        .required(intl.formatMessage({ id: 'error.phone' }))
        .mobilePhoneNumber(intl),
      dpa: Yup.bool().oneOf([true], errorDPA),
      termsAndConditions: Yup.bool().oneOf([true], errorTermsAndConditions),
    })
  );
};
