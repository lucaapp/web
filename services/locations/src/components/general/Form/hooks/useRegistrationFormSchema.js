import * as Yup from 'yup';
import { useIntl } from 'react-intl';
import { MAX_EMAIL_LENGTH } from 'constants/valueLength';

export const useRegistrationValidationSchema = () => {
  const intl = useIntl();

  const emailValidationSchema = Yup.object().shape({
    email: Yup.string()
      .trim()
      .email(intl.formatMessage({ id: 'error.email' }))
      .required(
        intl.formatMessage({
          id: 'error.email',
        })
      )
      .max(
        MAX_EMAIL_LENGTH,
        intl.formatMessage({
          id: 'error.length',
        })
      ),
  });

  return { emailValidationSchema };
};
