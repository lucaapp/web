import { useIntl } from 'react-intl';
import {
  MAX_CITY_LENGTH,
  MAX_NAME_LENGTH,
  MAX_POSTAL_CODE_LENGTH,
  MAX_STREET_LENGTH,
  MAX_VAT,
} from 'constants/valueLength';
import { Yup } from './yupConfig';

export const useInvoiceDataSchema = () => {
  const intl = useIntl();

  const errorMissingFiled = intl.formatMessage({
    id: 'createGroup.errorMissingField',
  });
  const errorEmail = intl.formatMessage({
    id: 'error.email',
  });

  const errorMissingTaxId = intl.formatMessage({
    id: 'createGroup.errorMissingTaxId',
  });

  const errorInvalid = intl.formatMessage({ id: 'formField.invalid' });

  return Yup.object().shape({
    companyName: Yup.string()
      .required(errorMissingFiled)
      .max(MAX_NAME_LENGTH)
      .safeString(errorInvalid),
    businessFirstName: Yup.string()
      .required(errorMissingFiled)
      .max(MAX_NAME_LENGTH)
      .safeString(errorInvalid),
    businessLastName: Yup.string()
      .required(errorMissingFiled)
      .max(MAX_NAME_LENGTH)
      .safeString(errorInvalid),
    street: Yup.string()
      .required(errorMissingFiled)
      .max(MAX_STREET_LENGTH)
      .safeString(errorInvalid),
    streetNumber: Yup.string()
      .required(errorMissingFiled)
      .max(MAX_CITY_LENGTH)
      .safeString(errorInvalid),
    zipCode: Yup.string()
      .required(errorMissingFiled)
      .max(MAX_POSTAL_CODE_LENGTH)
      .zipCode(errorInvalid),
    city: Yup.string()
      .required(errorMissingFiled)
      .max(MAX_CITY_LENGTH)
      .safeString(errorInvalid),
    invoiceEmail: Yup.string().email(errorEmail).required(errorEmail),
    vatNumber: Yup.string().max(MAX_VAT).vatNumber(errorMissingTaxId),
  });
};
