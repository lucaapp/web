import { useIntl } from 'react-intl';
import { Yup } from './yupConfig';

export const useMobilePhoneSchema = () => {
  const intl = useIntl();

  return Yup.object().shape({
    phone: Yup.string()
      .required(intl.formatMessage({ id: 'error.phone' }))
      .mobilePhoneNumber(intl),
  });
};
