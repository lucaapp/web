import React from 'react';

import { useTranslateFormErrors } from '../hooks';

export const WithTranslateFormErrors = ({ children }) => {
  useTranslateFormErrors();

  return <>{children}</>;
};
