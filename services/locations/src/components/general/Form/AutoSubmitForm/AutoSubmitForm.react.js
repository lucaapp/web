import { useEffect } from 'react';
import { useFormikContext } from 'formik';
import { isEmpty } from 'lodash';

import { useDebounce } from 'components/App/Dashboard/Location/AreaDetails/hooks';

export const AutoSubmitForm = ({ wait, resetErrors }) => {
  const {
    dirty,
    errors,
    isValid,
    setErrors,
    submitForm,
    values,
  } = useFormikContext();

  const debouncedSubmitForm = useDebounce(submitForm, wait);

  useEffect(() => {
    const formIsDirty = !isEmpty(errors) || !(isValid && dirty);
    if (formIsDirty) return;

    const filterFields = Object.entries(values).filter(([, value]) => !value);

    const formIsInvalid = filterFields.length !== 0;
    if (formIsInvalid) return;

    debouncedSubmitForm();
  }, [debouncedSubmitForm, values, errors, isValid, dirty]);

  useEffect(() => {
    if (!resetErrors) return;

    setErrors({});
  }, [resetErrors, setErrors]);

  return null;
};
