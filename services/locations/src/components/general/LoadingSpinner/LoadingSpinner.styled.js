import styled from 'styled-components';
import { LoadingOutlined } from '@ant-design/icons';

export const SpinWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const StyledSpinIcon = styled(LoadingOutlined)`
  font-size: 24px;
`;
