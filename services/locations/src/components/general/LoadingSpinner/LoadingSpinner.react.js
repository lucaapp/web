import React from 'react';
import { Spin } from 'antd';
import { SpinWrapper, StyledSpinIcon } from './LoadingSpinner.styled';

export const LoadingSpinner = () => (
  <SpinWrapper>
    <Spin indicator={<StyledSpinIcon spin />} />
  </SpinWrapper>
);
