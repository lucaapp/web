import styled from 'styled-components';
import { InformationIcon } from 'components/general';
import { CollapsableWrapper } from '../PayoutHistory/PayoutHistory.styled';

export const StyledInformationIcon = styled(InformationIcon)`
  align-self: center;
`;

export const ScrollableCollapsableWrapper = styled(CollapsableWrapper)`
  overflow: auto;
  padding: 0;
  margin: 0 32px 32px 32px;
`;

export const StyledDownloadWrapper = styled.div`
  display: flex;
  margin-bottom: 24px;
  justify-content: flex-end;
`;
