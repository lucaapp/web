import styled from 'styled-components';
import { Title } from 'components/general/DrawerRight';
import { DangerButton, SecondaryButton } from 'components/general';

export const StyledTitle = styled(Title)`
  font-size: 24px;
`;

export const TitleWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: ${({ $isMultiple }) =>
    $isMultiple ? 'space-between' : 'flex-end'};
  margin-top: 40px;
  margin-bottom: 80px;
`;

export const StyledDangerButton = styled(DangerButton)`
  padding: 0 20px;
`;

export const StyledSecondaryButton = styled(SecondaryButton)`
  padding: 0 20px;
  min-width: ${({ $isMultiple }) => ($isMultiple ? 'auto' : '152px')};
`;

export const SubRowWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const StyledRefundedText = styled.div`
  color: rgb(241, 103, 4);
  font-size: 16px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  text-align: right;
`;
