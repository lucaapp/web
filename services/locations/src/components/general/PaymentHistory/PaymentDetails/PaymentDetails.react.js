import React from 'react';
import { useIntl } from 'react-intl';
import { Space } from 'antd';
import { ReturnIcon } from 'assets/icons';
import { DataRowFactory, ROW_TYPE } from 'components/general/Factory';
import { DrawerRight } from 'components/general/DrawerRight';
import { RefundPopConfirm } from 'components/App/shared';
import { PAYMENT_STATUS } from 'constants/paymentOnboarding';
import { HELP_CENTER_ROUTE } from 'constants/routes';
import {
  ButtonWrapper,
  StyledDangerButton,
  StyledRefundedText,
  StyledSecondaryButton,
  StyledTitle,
  SubRowWrapper,
  TitleWrapper,
} from './PaymentDetails.styled';
import { usePaymentDetailsRows } from './PaymentDetails.helper';

export const PaymentDetails = ({
  visible,
  paymentDetail,
  onClose,
  handleRefundPayment,
  locationNameMap,
}) => {
  const intl = useIntl();

  const paymentDetailsRows = usePaymentDetailsRows(
    paymentDetail,
    locationNameMap
  );
  const isPayCompleted = paymentDetail.status === PAYMENT_STATUS.CLOSED;
  const isPayPendingOrRefunded = !isPayCompleted || paymentDetail.refunded;

  const renderTitle = () => (
    <TitleWrapper>
      <StyledTitle>
        {intl.formatMessage({
          id: 'payment.paymentDetails.drawer.title',
        })}
      </StyledTitle>
      {paymentDetail.refunded && (
        <StyledRefundedText>
          {intl.formatMessage({
            id: 'payment.paymentDetails.drawer.refunded',
          })}
        </StyledRefundedText>
      )}
    </TitleWrapper>
  );

  const navigateToHelpCenterUrl = () => {
    window.location.href = HELP_CENTER_ROUTE;
  };

  return (
    <DrawerRight title={renderTitle()} visible={visible} onClose={onClose}>
      <>
        {paymentDetailsRows.map(row => (
          <div key={row.key}>
            <DataRowFactory
              type={ROW_TYPE.MAIN_ROW}
              label={row.title}
              value={row.value}
              icon={row.icon}
              shouldLineThrough={
                row.key.toLowerCase().includes('amount') &&
                isPayPendingOrRefunded
              }
            />
            {row.subRows && (
              <SubRowWrapper>
                {row.subRows.map(subRow => (
                  <DataRowFactory
                    type={ROW_TYPE.SUB_ROW}
                    label={subRow.subTitle}
                    value={subRow.subValue}
                    key={subRow.subKey}
                    shouldLineThrough={isPayPendingOrRefunded}
                  />
                ))}
              </SubRowWrapper>
            )}
          </div>
        ))}

        <ButtonWrapper $isMultiple={!isPayPendingOrRefunded}>
          {!isPayPendingOrRefunded && (
            <RefundPopConfirm
              onConfirm={() => handleRefundPayment(paymentDetail.id)}
            >
              <StyledDangerButton>
                <Space>
                  <ReturnIcon />
                  {intl.formatMessage({
                    id: 'payment.button.refund',
                  })}
                </Space>
              </StyledDangerButton>
            </RefundPopConfirm>
          )}

          <StyledSecondaryButton
            $isMultiple={!isPayPendingOrRefunded}
            onClick={navigateToHelpCenterUrl}
          >
            {intl.formatMessage({
              id: 'payment.button.support',
            })}
          </StyledSecondaryButton>
        </ButtonWrapper>
      </>
    </DrawerRight>
  );
};
