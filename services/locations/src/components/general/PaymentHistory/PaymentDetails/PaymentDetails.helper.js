import React from 'react';
import { useIntl } from 'react-intl';
import {
  getFormattedShortDateFromDateTime,
  getFormattedTimeFromDateTime,
} from 'utils/formatters';
import { PAYMENT_STATUS } from 'constants/paymentOnboarding';
import { YellowStarIcon } from 'assets/icons';
import { TableName } from 'components/general/TableName';
import { useFormatCurrencyByLang } from 'components/hooks';
import { getAmount, getTotalAmount, getTotalFees } from 'utils/paymentAmount';

export const usePaymentDetailsRows = (paymentDetail, locationNameMap) => {
  const intl = useIntl();
  const { formatCurrencyByLang } = useFormatCurrencyByLang();

  const {
    invoiceAmount: paidByGuest,
    tipAmount,
    lucaDiscount,
    lucaTipTopUp,
    dateTime,
    createdDateTime,
    status,
    locationId,
    table,
    paymentVerifier,
  } = paymentDetail;

  const paidByLuca = lucaDiscount?.discountAmount || 0;

  const isPayCompleted = status === PAYMENT_STATUS.CLOSED;

  return [
    {
      title: intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.date',
      }),
      key: 'date',
      value: getFormattedShortDateFromDateTime(
        isPayCompleted ? dateTime : createdDateTime
      ),
    },
    {
      title: intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.time',
      }),
      key: 'time',
      value: getFormattedTimeFromDateTime(
        isPayCompleted ? dateTime : createdDateTime
      ),
    },
    {
      title: intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.table',
      }),
      key: 'table',
      value: <TableName locationId={locationId} tableId={table} />,
    },
    {
      title: intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.area',
      }),
      key: 'locationId',
      value:
        locationNameMap.get(locationId) ||
        intl.formatMessage({ id: 'location.defaultName' }),
    },
    {
      title: intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.code',
      }),
      key: 'paymentVerifier',
      value: paymentVerifier,
    },
    {
      title: intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.invoiceAmount',
      }),
      key: 'amount',
      value: formatCurrencyByLang(getAmount(paymentDetail)),
      icon: lucaDiscount ? YellowStarIcon : null,
      subRows: lucaDiscount
        ? [
            {
              subTitle: intl.formatMessage({
                id: 'payment.paymentDetails.invoiceAmount.paidByGuest',
              }),
              subValue: formatCurrencyByLang(paidByGuest),
              subKey: 'invoiceAmountPaidByGuest',
            },
            {
              subTitle: intl.formatMessage({
                id: 'payment.paymentDetails.invoiceAmount.paidByLuca',
              }),
              subValue: formatCurrencyByLang(paidByLuca),
              subKey: 'invoiceAmountPaidByLuca',
            },
          ]
        : null,
    },
    {
      title: intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.tipAmount',
      }),
      key: 'tipAmount',
      value: formatCurrencyByLang(tipAmount),
      icon: lucaDiscount ? YellowStarIcon : null,
      subRows: lucaTipTopUp
        ? [
            {
              subTitle: intl.formatMessage({
                id: 'payment.paymentDetails.invoiceAmount.paidByGuest',
              }),
              subValue: formatCurrencyByLang(lucaTipTopUp.originalTipAmount),
              subKey: 'invoiceAmountPaidByGuest',
            },
            {
              subTitle: intl.formatMessage({
                id: 'payment.paymentDetails.invoiceAmount.paidByLuca',
              }),
              subValue: formatCurrencyByLang(lucaTipTopUp.tipTopUpAmount),
              subKey: 'invoiceAmountPaidByLuca',
            },
          ]
        : null,
    },
    {
      title: intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.total',
      }),
      key: 'totalAmount',
      value: formatCurrencyByLang(getTotalAmount(paymentDetail)),
    },
    {
      title: intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.fees',
      }),
      key: '',
      value: formatCurrencyByLang(getTotalFees(paymentDetail)),
    },
  ];
};
