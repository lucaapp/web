import { PAYMENT_STATUS } from 'constants/paymentOnboarding';
import { Badge, Space } from 'antd';
import React from 'react';
import { useIntl } from 'react-intl';
import { StyledTooltip } from 'components/general';
import { useFormatCurrencyByLang } from 'components/hooks/useFormatCurrency';
import { RefundPopConfirm } from 'components/App/shared';
import { getTotalAmount } from 'utils/paymentAmount';
import { StyledRefundIcon } from './PaymentsTable.styled';
import { getCorrespondingErrorMessage } from './PaymentTableStatusCell.helper';

export const PaymentTableStatusCell = ({ record, onRefundPayment }) => {
  const intl = useIntl();
  const { formatCurrencyByLang } = useFormatCurrencyByLang();

  if (record.refunded) {
    return (
      <StyledTooltip
        placement="top"
        title={intl.formatMessage(
          {
            id: 'payment.location.paymentHistory.refunded',
          },
          {
            refundedAmount: formatCurrencyByLang(getTotalAmount(record)),
          }
        )}
      >
        <Badge status="error" text="" />
      </StyledTooltip>
    );
  }

  const RefundConfirm = ({ paymentId }) => (
    <RefundPopConfirm onConfirm={() => onRefundPayment(paymentId)}>
      <StyledTooltip
        placement="top"
        title={intl.formatMessage({
          id: 'payment.location.paymentHistory.refund',
        })}
      >
        <StyledRefundIcon />
      </StyledTooltip>
    </RefundPopConfirm>
  );

  switch (record.status) {
    case PAYMENT_STATUS.ACTIVE:
      return (
        <StyledTooltip
          placement="top"
          title={intl.formatMessage({
            id: 'payment.historyTable.paymentStatus.tooltip.title.active',
          })}
        >
          <Badge status="processing" text="" />
        </StyledTooltip>
      );
    case PAYMENT_STATUS.CANCELED:
      return (
        <StyledTooltip
          placement="top"
          title={intl.formatMessage({
            id: 'payment.historyTable.paymentStatus.tooltip.title.canceled',
          })}
        >
          <Badge status="error" text="" />
        </StyledTooltip>
      );
    case PAYMENT_STATUS.CLOSED:
      return (
        <Space size="middle">
          <RefundConfirm paymentId={record.id} />
          <StyledTooltip
            placement="top"
            title={intl.formatMessage({
              id: 'payment.historyTable.paymentStatus.tooltip.title.closed',
            })}
          >
            <Badge status="success" text="" />
          </StyledTooltip>
        </Space>
      );
    case PAYMENT_STATUS.ERROR:
      // eslint-disable-next-line no-case-declarations
      const errorMessage = getCorrespondingErrorMessage(record, intl);
      return (
        <StyledTooltip placement="top" title={errorMessage}>
          <Badge status="error" text="" />
        </StyledTooltip>
      );
    case PAYMENT_STATUS.EXPIRED:
      return (
        <StyledTooltip
          placement="top"
          title={intl.formatMessage({
            id: 'payment.historyTable.paymentStatus.tooltip.title.expired',
          })}
        >
          <Badge status="warning" text="" />
        </StyledTooltip>
      );
    case PAYMENT_STATUS.NEW:
      return (
        <StyledTooltip
          placement="top"
          title={intl.formatMessage({
            id: 'payment.historyTable.paymentStatus.tooltip.title.new',
          })}
        >
          <Badge status="processing" text="" />
        </StyledTooltip>
      );
    case PAYMENT_STATUS.REVERSED:
      return (
        <StyledTooltip
          placement="top"
          title={intl.formatMessage({
            id: 'payment.historyTable.paymentStatus.tooltip.title.reversed',
          })}
        >
          <Badge status="warning" text="" />
        </StyledTooltip>
      );

    default:
      return <Badge status="default" />;
  }
};
