const ERROR_PROCESSING_CARD_N7_RAPYD_ERROR = 'ERROR_PROCESSING_CARD - [N7]';
const ERROR_PROCESSING_CARD_83_RAPYD_ERROR = 'ERROR_PROCESSING_CARD - [83]';
const ERROR_PROCESSING_CARD_82_RAPYD_ERROR = 'ERROR_PROCESSING_CARD - [82]';
const ERROR_PROCESSING_CARD_78_RAPYD_ERROR = 'ERROR_PROCESSING_CARD - [78]';
const ERROR_PROCESSING_CARD_61_RAPYD_ERROR = 'ERROR_PROCESSING_CARD - [61]';
const ERROR_PROCESSING_CARD_59_RAPYD_ERROR = 'ERROR_PROCESSING_CARD - [59]';
const ERROR_PROCESSING_CARD_51_RAPYD_ERROR = 'ERROR_PROCESSING_CARD - [51]';
const ERROR_PROCESSING_CARD_30_RAPYD_ERROR = 'ERROR_PROCESSING_CARD - [30]';
const ERROR_PROCESSING_CARD_1A_RAPYD_ERROR = 'ERROR_PROCESSING_CARD - [1A]';
const ERROR_PROCESSING_CARD_05_RAPYD_ERROR = 'ERROR_PROCESSING_CARD - [05]';
const ERROR_PROCESSING_CARD_04_RAPYD_ERROR = 'ERROR_PROCESSING_CARD - [04]';
const ERROR_COMPLETE_PAYMENT_RAPYD_ERROR = 'ERROR_COMPLETE_PAYMENT';
const ERROR_CARD_AUTHENTICATION_FAILURE_RAPYD_ERROR =
  'ERROR_CARD_AUTHENTICATION_FAILURE';
const ERROR_3DS_AUTHENTICATION_FAILURE_RAPYD_ERROR =
  'ERROR_3DS_AUTHENTICATION_FAILURE';
const ERROR_3DS_FAILURE_RAPYD_ERROR = 'ERROR_3DS_FAILURE';
const SOME_RAPYD_ERROR = '909';

const possibleErrorCodes = [
  ERROR_PROCESSING_CARD_N7_RAPYD_ERROR,
  ERROR_PROCESSING_CARD_83_RAPYD_ERROR,
  ERROR_PROCESSING_CARD_82_RAPYD_ERROR,
  ERROR_PROCESSING_CARD_78_RAPYD_ERROR,
  ERROR_PROCESSING_CARD_61_RAPYD_ERROR,
  ERROR_PROCESSING_CARD_59_RAPYD_ERROR,
  ERROR_PROCESSING_CARD_51_RAPYD_ERROR,
  ERROR_PROCESSING_CARD_30_RAPYD_ERROR,
  ERROR_PROCESSING_CARD_1A_RAPYD_ERROR,
  ERROR_PROCESSING_CARD_05_RAPYD_ERROR,
  ERROR_PROCESSING_CARD_04_RAPYD_ERROR,
  ERROR_COMPLETE_PAYMENT_RAPYD_ERROR,
  ERROR_CARD_AUTHENTICATION_FAILURE_RAPYD_ERROR,
  ERROR_3DS_AUTHENTICATION_FAILURE_RAPYD_ERROR,
  ERROR_3DS_FAILURE_RAPYD_ERROR,
  SOME_RAPYD_ERROR,
];

export const getCorrespondingErrorMessage = (record, intl) => {
  if (
    possibleErrorCodes.includes(errorCode => errorCode === record.errorCode)
  ) {
    return intl.formatMessage({
      id: record.errorCode,
    });
  }
  return intl.formatMessage(
    {
      id: 'payment.historyTable.paymentStatus.tooltip.title.errorFallback',
    },
    { error: record.errorMessage }
  );
};
