import React from 'react';
import { PAYMENT_STATUS } from 'constants/paymentOnboarding';
import {
  getFormattedShortDateFromDateTime,
  getFormattedTimeFromDateTime,
} from 'utils/formatters';
import { TableName } from 'components/general/TableName';

import { YellowStarIcon } from 'assets/icons';
import { StyledIcon } from 'components/general/DataRow/DataRow.styled';
import { getAmount, getTotalAmount } from 'utils/paymentAmount';
import {
  AmountCell,
  CrossedTableCell,
  TableCell,
} from './PaymentsTable.styled';
import { PaymentTableStatusCell } from './PaymentTableStatusCell.react';

export const getLocationNameMap = groups => {
  return new Map(groups.flatMap(g => g.locations.map(l => [l.uuid, l.name])));
};

const isLineCrossed = record =>
  record.status !== PAYMENT_STATUS.CLOSED || record.refunded;

export const columnConfig = (
  intl,
  locationId,
  locationNameMap,
  onRefundPayment,
  formatCurrencyByLang
) => [
  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.date',
    }),
    dataIndex: 'date',
    align: 'left',
    render(string, record) {
      return (
        <TableCell>
          {getFormattedShortDateFromDateTime(
            record.status === PAYMENT_STATUS.CLOSED
              ? record.dateTime
              : record.createdDateTime
          )}
        </TableCell>
      );
    },
  },
  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.time',
    }),
    dataIndex: 'time',
    align: 'left',
    render(string, record) {
      return (
        <TableCell>
          {getFormattedTimeFromDateTime(
            record.status === PAYMENT_STATUS.CLOSED
              ? record.dateTime
              : record.createdDateTime
          )}
        </TableCell>
      );
    },
  },
  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.table',
    }),
    dataIndex: 'table',
    align: 'left',
    render(_, record) {
      return (
        <TableCell>
          <TableName locationId={record.locationId} tableId={record.table} />
        </TableCell>
      );
    },
  },
  !locationId
    ? {
        title: intl.formatMessage({
          id: 'payment.paymentHistoryTable.column.area',
        }),
        dataIndex: 'locationId',
        align: 'left',
        render(string, record) {
          const name =
            locationNameMap.get(record.locationId) ||
            intl.formatMessage({ id: 'location.defaultName' });

          return <TableCell>{name}</TableCell>;
        },
      }
    : {
        className: 'hidden',
      },
  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.code',
    }),
    dataIndex: 'paymentVerifier',
    align: 'center',
    render(string) {
      return <TableCell>{string}</TableCell>;
    },
  },
  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.invoiceAmount',
    }),
    dataIndex: 'invoiceAmount',
    align: 'right',
    render(string, record) {
      if (isLineCrossed(record)) {
        return (
          <CrossedTableCell>
            {formatCurrencyByLang(getAmount(record))}
          </CrossedTableCell>
        );
      }

      return <TableCell>{formatCurrencyByLang(getAmount(record))}</TableCell>;
    },
  },
  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.tipAmount',
    }),
    dataIndex: 'tipAmount',
    align: 'right',
    render(string, record) {
      if (!string) {
        return <TableCell>-</TableCell>;
      }

      if (isLineCrossed(record)) {
        return (
          <CrossedTableCell>
            {formatCurrencyByLang(record.tipAmount)}
          </CrossedTableCell>
        );
      }

      return <TableCell>{formatCurrencyByLang(record.tipAmount)}</TableCell>;
    },
  },

  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.total',
    }),
    dataIndex: 'amount',
    align: 'right',
    render(string, record) {
      if (isLineCrossed(record)) {
        return (
          <CrossedTableCell>
            {formatCurrencyByLang(getTotalAmount(record))}
            {record.lucaDiscount && <StyledIcon component={YellowStarIcon} />}
          </CrossedTableCell>
        );
      }

      if (record.status === PAYMENT_STATUS.CLOSED) {
        return (
          <AmountCell>
            {formatCurrencyByLang(getTotalAmount(record))}
            {record.lucaDiscount && <StyledIcon component={YellowStarIcon} />}
          </AmountCell>
        );
      }
      return <TableCell>-</TableCell>;
    },
  },
  {
    title: '',
    align: 'right',
    render(string, record) {
      return (
        <PaymentTableStatusCell
          record={record}
          onRefundPayment={() => onRefundPayment(record.id)}
        />
      );
    },
  },
];
