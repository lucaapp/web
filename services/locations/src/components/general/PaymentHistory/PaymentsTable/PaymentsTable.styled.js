import styled from 'styled-components';
import { ReturnIcon } from 'assets/icons';

export const TableCell = styled.div`
  font-size: 14px;
  font-weight: 500;
  height: 20px;
  letter-spacing: 0;
  line-height: 20px;
`;

export const CrossedTableCell = styled(TableCell)`
  text-decoration: line-through;
`;

export const AmountCell = styled(TableCell)`
  font-size: 16px;
  font-weight: bold;
  text-align: right;
`;

export const RowNumberCell = styled(TableCell)`
  color: rgb(129, 129, 129);
`;

export const StyledRefundIcon = styled(ReturnIcon)`
  cursor: pointer;
  color: rgb(80, 102, 124);
  font-size: 12px;
  font-weight: bold;
  text-align: center;

  &:hover {
    color: rgb(129, 129, 129);
  }
`;
