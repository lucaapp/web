import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { notification, Table } from 'antd';

import { triggerRefund } from 'network/payment';
import { useFormatCurrencyByLang } from 'components/hooks/useFormatCurrency';

import { QUERY_KEYS, useGetGroups, useNotifications } from 'components/hooks';
import { RapydApiError } from 'network/utils';
import {
  PaginationWrapper,
  StyledArrowRightIcon,
  StyledButton,
  TableWrapper,
} from './RapydTable.styled';

import { columnConfig, getLocationNameMap } from './PaymentsTable.helper';
import { PaymentDetails } from '../PaymentDetails';

const PAGE_SIZE = 10;

export const PaymentsTable = ({
  locationGroupId,
  locationId,
  payments,
  setCursorPayment,
  cursorPayment,
  isLoading,
}) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const { formatCurrencyByLang } = useFormatCurrencyByLang();
  const [isDetailsDrawerVisible, setIsDetailsDrawerVisible] = useState(false);
  const [selectedRow, setSelectedRow] = useState({});
  const [pageCount, setPageCount] = useState(0);
  const { errorMessage } = useNotifications();

  const { data: groups } = useGetGroups(locationGroupId);

  if (!locationGroupId || !groups) return null;

  const dataSource = payments?.payments.map((entry, index) => ({
    ...entry,
    key: index + (1 + PAGE_SIZE * pageCount),
  }));

  const onNext = () => {
    setCursorPayment(payments.cursor ? payments.cursor : '');
    setPageCount(pageCount + 1);
  };
  const onReset = () => {
    setCursorPayment('');
    setPageCount(0);
  };

  const onRefundPayment = paymentId => {
    const onSuccess = async () => {
      await Promise.all([
        queryClient.invalidateQueries(QUERY_KEYS.PAYMENTS),
        queryClient.invalidateQueries(QUERY_KEYS.PAYOUTS),
        queryClient.invalidateQueries(QUERY_KEYS.BALANCE),
      ]).then(() => {
        notification.success({
          message: intl.formatMessage({
            id: 'payment.location.paymentHistory.refund.success',
          }),
        });
      });
    };

    triggerRefund(locationGroupId, paymentId)
      .then(response => {
        if (!response) {
          errorMessage('payment.location.paymentHistory.refund.error');
          return;
        }

        if (response instanceof RapydApiError) {
          errorMessage('payment.location.paymentHistory.refund.error.rapyd');
          return;
        }

        onSuccess();
        setIsDetailsDrawerVisible(false);
      })
      .catch(() =>
        errorMessage('payment.location.paymentHistory.refund.error')
      );
  };

  const locationNameMap = getLocationNameMap(groups);

  const onSelectRow = record => {
    setIsDetailsDrawerVisible(true);
    setSelectedRow(record);
  };

  return (
    <>
      <TableWrapper>
        <Table
          columns={columnConfig(
            intl,
            locationId,
            locationNameMap,
            onRefundPayment,
            formatCurrencyByLang
          )}
          dataSource={dataSource || []}
          loading={isLoading}
          locale={{
            emptyText: intl.formatMessage({
              id: 'payment.paymentHistoryTable.emptyState',
            }),
          }}
          scroll={{ x: 'max-content' }}
          onRow={record => {
            return {
              onClick: () => onSelectRow(record),
              onDoubleClick: () => onSelectRow(record),
              onContextMenu: () => onSelectRow(record),
            };
          }}
        />
      </TableWrapper>
      <PaginationWrapper>
        {cursorPayment !== '' ? (
          <StyledButton onClick={() => onReset()}>
            {intl.formatMessage({ id: 'payment.historyTable.previous' })}
          </StyledButton>
        ) : (
          <div />
        )}
        {!!payments?.cursor && (
          <StyledButton onClick={() => onNext()}>
            {intl.formatMessage({ id: 'payment.historyTable.next' })}
            <StyledArrowRightIcon />
          </StyledButton>
        )}
      </PaginationWrapper>
      <PaymentDetails
        visible={isDetailsDrawerVisible}
        paymentDetail={selectedRow}
        onClose={() => setIsDetailsDrawerVisible(false)}
        handleRefundPayment={onRefundPayment}
        locationNameMap={locationNameMap}
      />
    </>
  );
};
