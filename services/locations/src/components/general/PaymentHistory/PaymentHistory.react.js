import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';

import {
  LocationCard,
  StyledTooltip,
  InformationIcon,
} from 'components/general';
import { PaymentsTable } from 'components/general/PaymentHistory/PaymentsTable';
import { QUERY_KEYS } from 'components/hooks';
import {
  getLocationGroupPayments,
  getLocationPayments,
  queryDoNotRetryOnCode,
} from 'network';

import { PaymentsCSVDownload } from './PaymentsCSVDownload';

import {
  StyledInformationIcon,
  StyledDownloadWrapper,
  ScrollableCollapsableWrapper,
} from './PaymentHistory.styled';

const REFETCH_INTERVAL = 30 * 1000; // 30 seconds

export const PaymentHistory = ({ locationGroupId, locationId }) => {
  const intl = useIntl();

  const tooltipId = locationId
    ? 'payment.paymentHistory.location.tooltip'
    : 'payment.paymentHistory.locationGroup.tooltip';

  const [cursorPayment, setCursorPayment] = useState('');

  const { error, isLoading, data: payments } = useQuery(
    [QUERY_KEYS.PAYMENTS, cursorPayment, locationGroupId, locationId],
    () =>
      locationId
        ? getLocationPayments(locationId, cursorPayment)
        : getLocationGroupPayments(locationGroupId, cursorPayment),
    {
      refetchInterval: REFETCH_INTERVAL,
      retry: queryDoNotRetryOnCode([404, 403]),
    }
  );

  if (isLoading) return null;

  const isOpen = payments.payments.length > 0;

  const PaymentHistoryCardTitle = () => (
    <>
      {intl.formatMessage({ id: 'payment.areaPaymentHistoryHeadline' })}
      <StyledTooltip
        placement="bottom"
        title={intl.formatMessage({
          id: tooltipId,
        })}
      >
        <StyledInformationIcon />
      </StyledTooltip>
    </>
  );

  return (
    <LocationCard isCollapse open={isOpen} title={<PaymentHistoryCardTitle />}>
      <ScrollableCollapsableWrapper>
        {!error && (
          <>
            <StyledDownloadWrapper>
              <PaymentsCSVDownload
                locationId={locationId}
                locationGroupId={locationGroupId}
                payments={payments}
              />
              <StyledTooltip
                title={intl.formatMessage({
                  id: 'payment.download.infoText',
                })}
              >
                <InformationIcon data-cy="paymentCsvInfoIcon" />
              </StyledTooltip>
            </StyledDownloadWrapper>
            <PaymentsTable
              locationId={locationId}
              locationGroupId={locationGroupId}
              payments={payments}
              setCursorPayment={setCursorPayment}
              cursorPayment={cursorPayment}
              isLoading={isLoading}
            />
          </>
        )}
      </ScrollableCollapsableWrapper>
    </LocationCard>
  );
};
