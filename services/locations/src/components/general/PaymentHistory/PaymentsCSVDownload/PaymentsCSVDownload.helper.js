import sanitize from 'sanitize-filename';

import { PAYMENT_STATUS } from 'constants/paymentOnboarding';
import { getAmount, getTotalAmount } from 'utils/paymentAmount';
import {
  getFormattedTimeFromDateTime,
  getFormattedShortDateFromDateTime,
} from 'utils/formatters';
import { getLocationGroupPayments, getLocationPayments } from 'network/payment';
import { useIntl } from 'react-intl';
import { useGetGroup, useGetLocationById } from 'components/hooks';

const getLocationNameMap = group =>
  new Map(group.locations.map(l => [l.uuid, l.name]));

const getDataForLocation = async locationId => {
  const payments = [];
  const initialData = await getLocationPayments(locationId, '');
  let { cursor } = initialData;
  payments.push(...initialData.payments);
  while (cursor) {
    // eslint-disable-next-line no-await-in-loop
    const data = await getLocationPayments(locationId, cursor);
    payments.push(...data.payments);
    cursor = data.cursor;
  }
  return payments;
};

const getDataForLocationGroup = async locationGroupId => {
  const payments = [];
  const initialData = await getLocationGroupPayments(locationGroupId, '');
  let { cursor } = initialData;
  payments.push(...initialData.payments);
  while (cursor) {
    // eslint-disable-next-line no-await-in-loop
    const data = await getLocationGroupPayments(locationGroupId, cursor);
    payments.push(...data.payments);
    cursor = data.cursor;
  }
  return payments;
};

const generateContentRows = payments => {
  const contentList = [];

  for (const payment of payments) {
    const row = Object.values(payment);
    contentList.push(row);
  }

  return contentList;
};

const getPaymentStatus = (payment, intl) => {
  if (payment.status === PAYMENT_STATUS.CLOSED) {
    if (payment.refunded) {
      return intl.formatMessage({ id: 'paymentStatus.refunded' });
    }
    return intl.formatMessage({ id: 'paymentStatus.closed' });
  }
  return payment.status;
};

const getFormattedPayments = (payments, intl, group, location) => {
  const formattedPayments = [];

  const locationNameMap = getLocationNameMap(group);

  for (const payment of payments) {
    const { tipAmount } = payment;
    const date = getFormattedShortDateFromDateTime(
      payment.status === PAYMENT_STATUS.CLOSED
        ? payment.dateTime
        : payment.createdDateTime
    );
    const time = getFormattedTimeFromDateTime(
      payment.status === PAYMENT_STATUS.CLOSED
        ? payment.dateTime
        : payment.createdDateTime
    );

    const name = !location
      ? locationNameMap.get(payment.locationId) ||
        intl.formatMessage({ id: 'location.defaultName' })
      : location.name || intl.formatMessage({ id: 'location.defaultName' });

    const paymentStatus = getPaymentStatus(payment, intl);

    const formattedPayment = {
      [intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.date',
      })]: date,
      [intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.time',
      })]: time,
      [intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.code',
      })]: payment.paymentVerifier,
      [intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.invoiceAmount',
      })]: parseFloat(getAmount(payment)).toFixed(2),
      [intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.tipAmount',
      })]: tipAmount,
      [intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.total',
      })]: parseFloat(getTotalAmount(payment)).toFixed(2),
      [intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.area',
      })]: name,
      [intl.formatMessage({
        id: 'payment.paymentHistoryTable.column.status',
      })]: paymentStatus,
    };

    formattedPayments.push(formattedPayment);
  }
  return formattedPayments;
};

const getPaymentsHistoryDownloadFilename = (intl, location, locationGroup) => {
  const fileNameLocale = 'downloadFile.locations.paymentHistory';
  if (!location)
    return sanitize(
      intl.formatMessage(
        { id: fileNameLocale },
        {
          fileName: `${locationGroup.name.replaceAll(' ', '_')}`,
          format: 'csv',
        }
      )
    );

  const locationName = `_${location.name}`;
  const fileName = `${location.groupName.replaceAll(' ', '_')}${
    location.name ? locationName.replace(new RegExp(' ', 'g'), '_') : ''
  }`;

  return sanitize(
    intl.formatMessage({ id: fileNameLocale }, { fileName, format: 'csv' })
  );
};

export const useCSVDownload = ({ locationId, locationGroupId }) => {
  const intl = useIntl();

  const { data: location } = useGetLocationById(locationId, {
    enabled: !!locationId,
  });
  const { data: locationGroup } = useGetGroup(locationGroupId);

  if (!locationGroup || (!!locationId && !location))
    return {
      downloadCSVContent: () => {},
    };

  const fileName = getPaymentsHistoryDownloadFilename(
    intl,
    location,
    locationGroup
  );

  const downloadCSVContent = async () => {
    const payments = locationId
      ? await getDataForLocation(locationId)
      : await getDataForLocationGroup(locationGroupId);

    const formattedPayments = getFormattedPayments(
      payments,
      intl,
      locationGroup,
      location
    );

    if (formattedPayments.length === 0) {
      return {
        formattedPayments,
        fileName,
      };
    }
    const header = Object.keys(formattedPayments[0]);
    const contentRows = generateContentRows(formattedPayments);
    return {
      formattedPayments: [header, ...contentRows],
      fileName,
    };
  };

  return { downloadCSVContent };
};
