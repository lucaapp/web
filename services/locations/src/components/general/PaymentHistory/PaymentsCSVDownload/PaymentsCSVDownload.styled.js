import styled from 'styled-components';

export const DisabledLink = styled.span`
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  color: rgb(211, 211, 211);
  padding-right: 16px;
  cursor: not-allowed;
`;

export const StyledTrigger = styled.div`
  margin-bottom: 0;
  padding: 0 16px 0 0;
  text-decoration: none;
  align-self: center;
  border: none;
  box-shadow: none;
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  color: rgb(80, 102, 124);
  cursor: pointer;
`;
