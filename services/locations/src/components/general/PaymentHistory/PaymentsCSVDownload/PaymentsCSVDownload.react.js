import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useIntl } from 'react-intl';
import { CSVLink } from 'react-csv';

import {
  hideProgressBar,
  showProgressBar,
  DOWNLOAD_CSV_PAYMENT_LOADING_MESSAGE,
} from 'utils/progressBar';

import { useCSVDownload } from './PaymentsCSVDownload.helper';

import { DisabledLink, StyledTrigger } from './PaymentsCSVDownload.styled';

export const PaymentsCSVDownload = ({
  locationId,
  locationGroupId,
  payments,
}) => {
  const intl = useIntl();
  const csvInstance = useRef();

  const [isLoading, setIsLoading] = useState(false);
  const [data, setData] = useState(false);
  const [filename, setFilename] = useState('');

  const { downloadCSVContent } = useCSVDownload({
    locationId,
    locationGroupId,
  });
  const downloadData = useCallback(async () => {
    setIsLoading(true);

    const { formattedPayments, fileName } = await downloadCSVContent();
    await setData(formattedPayments);
    await setFilename(fileName);
  }, [downloadCSVContent]);

  useEffect(() => {
    if (isLoading) {
      const messageText = intl.formatMessage({
        id: 'message.generatingPaymentCsv',
      });
      showProgressBar(null, messageText, DOWNLOAD_CSV_PAYMENT_LOADING_MESSAGE);
    }
  }, [isLoading, intl]);

  useEffect(() => {
    if (data && csvInstance.current && csvInstance.current.link) {
      setTimeout(() => {
        csvInstance.current.link.click();
        setData(false);
        hideProgressBar(DOWNLOAD_CSV_PAYMENT_LOADING_MESSAGE);
        setIsLoading(false);
      });
    }
  }, [data]);

  return payments.length === 0 ? (
    <DisabledLink>
      {intl.formatMessage({
        id: 'settings.location.qrcode.generateCsv',
      })}
    </DisabledLink>
  ) : (
    <>
      <StyledTrigger onClick={downloadData}>
        {intl.formatMessage({
          id: 'settings.location.qrcode.generateCsv',
        })}
      </StyledTrigger>
      {data && <CSVLink filename={filename} data={data} ref={csvInstance} />}
    </>
  );
};
