import React from 'react';
import { useIntl } from 'react-intl';

import { LucaLogoWhiteIconSVG } from 'assets/icons';
import { HeaderWrapper, Logo, SubTitle } from './LogoBackground.styled';

export const LogoBackground = () => {
  const intl = useIntl();

  return (
    <HeaderWrapper>
      <Logo src={LucaLogoWhiteIconSVG} />
      <SubTitle>
        {intl.formatMessage({
          id: 'header.subtitle',
        })}
      </SubTitle>
    </HeaderWrapper>
  );
};
