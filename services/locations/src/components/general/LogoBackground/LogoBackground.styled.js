import styled from 'styled-components';

export const Logo = styled.img`
  margin-left: 40px;
  height: 37px;
`;

export const SubTitle = styled.div`
  font-family: Montserrat-SemiBold, sans-serif;
  font-size: 10px;
  font-weight: 600;
  color: white;
  display: flex;
  align-items: flex-end;
  margin-left: 11px;
  margin-top: 26px;

  @media (max-width: 900px) and (orientation: landscape) {
    display: none;
  }
`;

export const HeaderWrapper = styled.div`
  display: flex;
  align-items: center;
  height: 138px;
  background-color: black;
  padding-bottom: 25px;
`;
