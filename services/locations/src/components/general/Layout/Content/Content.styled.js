import styled from 'styled-components';
import { Layout } from 'antd';

export const contentStyles = {
  backgroundColor: '#f3f5f7',
};

export const LayoutContent = styled(Layout.Content)`
  padding-bottom: 64px;
`;
