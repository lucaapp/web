import React from 'react';

import { LayoutContent, contentStyles } from './Content.styled';

export const Content = ({ children }) => (
  <LayoutContent style={contentStyles}>{children}</LayoutContent>
);
