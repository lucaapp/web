import React from 'react';
import { Layout } from 'antd';

import { siderStyles } from './Sider.styled';

export const Sider = ({ children, width = 300 }) => {
  return (
    <Layout.Sider width={width} style={siderStyles}>
      {children}
    </Layout.Sider>
  );
};
