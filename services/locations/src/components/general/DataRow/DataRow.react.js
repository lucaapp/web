import React from 'react';
import { StyledIcon } from './DataRow.styled';

export const DataRow = ({ label, value, icon, style, shouldLineThrough }) => {
  const {
    labelComponent: LabelComponent,
    valueComponent: ValueComponent,
    rowComponent: RowComponent,
  } = style;

  return (
    <>
      <RowComponent>
        <LabelComponent>
          {label}
          {icon && <StyledIcon component={icon} />}
        </LabelComponent>
        <ValueComponent $shouldLineThrough={shouldLineThrough}>
          {value}
        </ValueComponent>
      </RowComponent>
    </>
  );
};
