import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const StyledIcon = styled(Icon)`
  margin-left: 8px;
`;
