import React from 'react';

import { useHoverState } from '../hooks/useHoverState';

export const WithHover = ({ children }) => {
  const { isHovered, onPointerEnter, onPointerLeave } = useHoverState();

  return <>{children(isHovered, onPointerEnter, onPointerLeave)}</>;
};
