import styled from 'styled-components';

export const StyledLabel = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  text-align: left;
`;

export const StyledValue = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  text-align: right;
  text-decoration: ${({ $shouldLineThrough }) =>
    $shouldLineThrough ? 'line-through' : 'none'};
`;

export const StyledRow = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 16px;
`;

export const StyledSubRow = styled(StyledRow)`
  padding-left: 16px;
  padding-bottom: 8px;

  &:not(:last-child) {
    margin-bottom: 0;
  }
`;

export const StyledSubLabel = styled(StyledLabel)`
  font-size: 14px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
`;

export const StyledSubValue = styled(StyledValue)`
  font-size: 14px;
`;
