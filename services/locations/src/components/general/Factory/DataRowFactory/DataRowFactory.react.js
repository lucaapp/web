import React from 'react';
import { DataRow } from 'components/general/DataRow';
import {
  StyledLabel,
  StyledRow,
  StyledSubLabel,
  StyledSubRow,
  StyledSubValue,
  StyledValue,
} from './DataRowFactory.styled';

export const ROW_TYPE = {
  MAIN_ROW: 'MAIN_ROW',
  SUB_ROW: 'SUB_ROW',
};

const MAIN_STYLE = {
  labelComponent: StyledLabel,
  valueComponent: StyledValue,
  rowComponent: StyledRow,
};

const SUB_STYLE = {
  labelComponent: StyledSubLabel,
  valueComponent: StyledSubValue,
  rowComponent: StyledSubRow,
};

const setStyle = type => {
  switch (type) {
    case ROW_TYPE.MAIN_ROW:
      return MAIN_STYLE;
    case ROW_TYPE.SUB_ROW:
      return SUB_STYLE;
    default:
      return MAIN_STYLE;
  }
};
export const DataRowFactory = ({
  type,
  label,
  value,
  icon,
  shouldLineThrough,
}) => {
  const style = setStyle(type);
  return (
    <DataRow
      style={style}
      label={label}
      value={value}
      icon={icon}
      shouldLineThrough={shouldLineThrough}
    />
  );
};
