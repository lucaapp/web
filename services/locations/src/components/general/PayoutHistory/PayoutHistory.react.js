import React, { useState } from 'react';
import { queryDoNotRetryOnCode } from 'network/utils';
import { Table } from 'antd';
import { getFormattedDateTime } from 'utils/formatters';
import { useIntl } from 'react-intl';
import { QUERY_KEYS } from 'components/hooks';
import { useQuery } from 'react-query';
import { getPayouts } from 'network/payment';
import { useFormatCurrencyByLang } from 'components/hooks/useFormatCurrency';

import {
  CollapsableWrapper,
  RowNumberCell,
  TableCell,
} from './PayoutHistory.styled';
import {
  PaginationWrapper,
  StyledArrowRightIcon,
  StyledButton,
  TableWrapper,
} from '../PaymentHistory/PaymentsTable/RapydTable.styled';
import { LocationCard } from '../LocationCard';

const REFETCH_INTERVAL = 60 * 1000; // Makes 60.000ms which is 60s, which we call a minute
const PAGE_SIZE = 10;

export const PayoutHistory = ({ locationGroupId }) => {
  const intl = useIntl();
  const [cursorPayouts, setCursorPayouts] = useState('');
  const [pageCount, setPageCount] = useState(0);
  const { formatCurrencyByLang } = useFormatCurrencyByLang();

  const { error, isFetching, data: payoutHistory } = useQuery(
    [QUERY_KEYS.PAYOUTS, cursorPayouts],
    () => getPayouts(locationGroupId, cursorPayouts),
    {
      enabled: !!locationGroupId,
      retry: queryDoNotRetryOnCode([401, 403, 404]),
      refetchInterval: REFETCH_INTERVAL,
    }
  );

  const columns = [
    {
      title: '',
      dataIndex: 'row',
      align: 'left',
      render(string, record) {
        return <RowNumberCell>{record.key}</RowNumberCell>;
      },
    },
    {
      title: intl.formatMessage({ id: 'payment.location.payoutHistory.date' }),
      dataIndex: 'dateTime',
      align: 'left',
      render(string) {
        return (
          <TableCell>{getFormattedDateTime(parseInt(string, 10))}</TableCell>
        );
      },
    },
    {
      title: intl.formatMessage({
        id: 'payment.location.payoutHistory.amount',
      }),
      dataIndex: 'amount',
      align: 'right',
      render(string) {
        return <TableCell>{formatCurrencyByLang(string)}</TableCell>;
      },
    },
  ];

  if (!locationGroupId || error) {
    return null;
  }

  const onNext = () => {
    setCursorPayouts(payoutHistory.cursor ? payoutHistory.cursor : '');
    setPageCount(pageCount + 1);
  };

  const onReset = () => {
    setCursorPayouts('');
    setPageCount(0);
  };

  return (
    <LocationCard
      isCollapse
      open={!!payoutHistory?.payouts.length}
      title={intl.formatMessage({ id: 'payment.location.payoutHistory.title' })}
    >
      <CollapsableWrapper>
        <TableWrapper>
          <Table
            columns={columns}
            dataSource={
              payoutHistory
                ? payoutHistory.payouts.map((entry, index) => ({
                    ...entry,
                    key: index + (1 + PAGE_SIZE * pageCount),
                  }))
                : []
            }
            loading={isFetching}
            locale={{
              emptyText: intl.formatMessage({
                id: 'payment.location.payoutHistory.empty',
              }),
            }}
          />
        </TableWrapper>
        {!!payoutHistory?.cursor && (
          <PaginationWrapper>
            <StyledButton onClick={onReset}>
              {intl.formatMessage({ id: 'payment.historyTable.previous' })}
            </StyledButton>
            <StyledButton onClick={onNext}>
              {intl.formatMessage({ id: 'payment.historyTable.next' })}
              <StyledArrowRightIcon />
            </StyledButton>
          </PaginationWrapper>
        )}
      </CollapsableWrapper>
    </LocationCard>
  );
};
