import styled from 'styled-components';

export const CollapsableWrapper = styled.div`
  padding: 0 32px 32px 32px;
  display: flex;
  flex-direction: column;
`;

export const TableCell = styled.div`
  font-size: 14px;
  font-weight: 500;
  height: 20px;
  letter-spacing: 0;
  line-height: 20px;
`;

export const RowNumberCell = styled(TableCell)`
  color: rgb(129, 129, 129);
`;
