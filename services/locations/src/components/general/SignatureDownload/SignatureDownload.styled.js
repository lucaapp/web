import styled from 'styled-components';

export const StyledLink = styled.a`
  text-decoration: none;
  color: rgb(80, 102, 124);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;

export const StyledTextSmall = styled.div`
  margin-top: 14px;
  margin-bottom: 8px;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;
