import React from 'react';
import { useIntl } from 'react-intl';

import {
  mapStrengthScore,
  mapStrengthScoreDataCy,
  mapStyleToScore,
  MEDIUM_PASSWORD,
  STRONG_PASSWORD,
  VERY_STRONG_PASSWORD,
  WEAK_PASSWORD,
} from 'utils/strengthMeter';
import {
  Container,
  Wrapper,
  StrengthDescription,
  StrengthCell,
} from './StrengthMeter.styled';

export const StrengthMeter = ({ strengthScore, lightVariation = false }) => {
  const intl = useIntl();

  return (
    <Container>
      <Wrapper lightVariation={lightVariation} data-cy="passwordStrengthMeter">
        <StrengthCell isStartCell style={mapStyleToScore[strengthScore]} />
        <StrengthCell
          style={
            strengthScore >= WEAK_PASSWORD ? mapStyleToScore[strengthScore] : {}
          }
        />
        <StrengthCell
          style={
            strengthScore >= MEDIUM_PASSWORD
              ? mapStyleToScore[strengthScore]
              : {}
          }
        />
        <StrengthCell
          style={
            strengthScore >= STRONG_PASSWORD
              ? mapStyleToScore[strengthScore]
              : {}
          }
        />
        <StrengthCell
          isEndCell
          style={
            strengthScore >= VERY_STRONG_PASSWORD
              ? mapStyleToScore[strengthScore]
              : {}
          }
        />
      </Wrapper>
      <StrengthDescription data-cy={mapStrengthScoreDataCy[strengthScore]}>
        {intl.formatMessage({
          id: mapStrengthScore[strengthScore],
        })}
      </StrengthDescription>
    </Container>
  );
};
