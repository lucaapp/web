/*
Drawer component

General informations:
- always opens from right
- max-width of 490px
- starts under the Header

Properties:
- heading: small text over the title (string - optional)
- title: title text (string - optional)
- visible: determines if the drawer is open or not (boolean)
- onClose: close callback (function)
- children: content of the drawer
*/

import React from 'react';

import { StyledDrawer, Heading, Header, Title } from './DrawerRight.styled';

export const DrawerRight = ({ heading, title, onClose, visible, children }) => {
  return (
    <StyledDrawer
      placement="right"
      onClose={onClose}
      visible={visible}
      mask={false}
    >
      <>
        <Header>
          {heading && <Heading>{heading}</Heading>}
          {title && <Title>{title}</Title>}
        </Header>
        {children}
      </>
    </StyledDrawer>
  );
};
