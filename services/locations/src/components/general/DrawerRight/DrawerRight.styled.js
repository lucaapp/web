import styled from 'styled-components';
import { Drawer } from 'antd';

export const StyledDrawer = styled(Drawer)`
  max-width: 490px;
  top: 72px;
  z-index: 90000;

  > .ant-drawer-content-wrapper {
    box-shadow: none !important;
  }

  > .ant-drawer-content-wrapper
    > .ant-drawer-content
    > .ant-drawer-wrapper-body {
    padding: 0 32px;
  }

  > .ant-drawer-content-wrapper
    > .ant-drawer-content
    > .ant-drawer-wrapper-body
    > .ant-drawer-body {
    padding: 0;
  }

  > .ant-drawer-content-wrapper
    > .ant-drawer-content
    > .ant-drawer-wrapper-body
    > .ant-drawer-header-close-only
    > .ant-drawer-header-title {
    justify-content: flex-end;
  }

  > .ant-drawer-content-wrapper
    > .ant-drawer-content
    > .ant-drawer-wrapper-body
    > .ant-drawer-header-close-only {
    padding-right: 0;
  }

  > .ant-drawer-content-wrapper
    > .ant-drawer-content
    > .ant-drawer-wrapper-body
    > .ant-drawer-header-close-only
    > .ant-drawer-header-title
    > button {
    color: rgb(27, 28, 30);
    margin-right: 0;
  }
`;

export const Heading = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 8px;
`;

export const Title = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 34px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 42px;
`;

export const Header = styled.div`
  margin: 32px 0;
  display: flex;
  flex-direction: column;
`;
