import React from 'react';

import { InformationIcon } from 'components/general/InformationIcon';
import { StyledTooltip } from 'components/general/StyledTooltip';
import {
  CardSectionTitle,
  StyledTitleContainer,
} from 'components/general/LocationCard';

export const CardSection = ({ title, tooltip, children }) => {
  return (
    <CardSectionTitle>
      <StyledTitleContainer>{title}</StyledTitleContainer>
      {tooltip && (
        <StyledTooltip title={tooltip}>
          <InformationIcon />
        </StyledTooltip>
      )}
      {children}
    </CardSectionTitle>
  );
};
