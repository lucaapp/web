import React from 'react';

import { Helmet } from 'react-helmet-async';

export const SiteHeader = ({ title, meta }) => {
  return (
    <Helmet>
      <title>{title}</title>
      <meta name="description" content={meta} />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"
      />
    </Helmet>
  );
};
