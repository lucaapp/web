import React from 'react';
import { ArrowDownBlueIcon } from 'assets/icons';
import { StyledIcon } from '../LocationCard/LocationCard.styled';

const ROTATE_DEGREES_INITIAL = 0;
const ROTATE_DEGREES_ACTIVE = 180;

export const ArrowIcon = ({ isActive }) => (
  <StyledIcon
    component={ArrowDownBlueIcon}
    rotate={isActive ? ROTATE_DEGREES_ACTIVE : ROTATE_DEGREES_INITIAL}
  />
);
