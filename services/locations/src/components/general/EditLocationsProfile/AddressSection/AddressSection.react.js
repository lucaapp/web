import React from 'react';
import { useIntl } from 'react-intl';
import {
  StyledButtonWrapper,
  Wrapper,
} from 'components/general/EditLocationsProfile/EditLocationsProfile.styled';
import { PrimaryButton } from 'components/general';
import { EditAddressModal } from 'components/App/modals/EditAddressModal';
import { DisplayAddress } from 'components/general/EditLocationsProfile/AddressSection/DisplayAddress';
import { Modal } from 'components/general/Modal';

export const AddressSection = ({ location, isGroup, dataCy }) => {
  const intl = useIntl();

  const modalOpenButton = (
    <StyledButtonWrapper>
      <PrimaryButton data-cy={dataCy}>
        {intl.formatMessage({
          id: 'profile.overview.edit',
        })}
      </PrimaryButton>
    </StyledButtonWrapper>
  );

  const modalContent = () => (
    <EditAddressModal location={location} isGroup={isGroup} forceRender />
  );

  return (
    <Wrapper>
      <DisplayAddress location={location} />
      <Modal content={modalContent} openButton={modalOpenButton} />
    </Wrapper>
  );
};
