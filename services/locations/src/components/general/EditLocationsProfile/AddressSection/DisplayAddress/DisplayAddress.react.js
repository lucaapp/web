import React from 'react';
import { useIntl } from 'react-intl';

import { AddressRow, AddressHeader } from './DisplayAddress.styled';

export const DisplayAddress = ({ location }) => {
  const intl = useIntl();
  const { streetName, streetNr, zipCode, city } = location;

  return (
    <>
      <AddressHeader>
        {intl.formatMessage({ id: 'settings.location.address' })}
      </AddressHeader>

      {streetName && streetNr && (
        <AddressRow data-cy="addressRowStreetNameAndStreetNr">{`${
          streetName || ''
        } ${streetNr || ''}`}</AddressRow>
      )}
      {zipCode && city && (
        <AddressRow data-cy="addressRowZipCodeAndCity">{`${zipCode || ''} ${
          city || ''
        }`}</AddressRow>
      )}
    </>
  );
};
