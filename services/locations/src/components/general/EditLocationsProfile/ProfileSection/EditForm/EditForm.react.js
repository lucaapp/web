import React from 'react';
import { useIntl } from 'react-intl';
import { AppForm, FormInputType } from 'components/general/Form';
import { SubmitButton } from 'components/general/Form/AppForm/SubmitButton';
import { defaultPrimaryButtonTheme } from 'components/general';

export const EditForm = ({
  group,
  location,
  onSubmit,
  validationSchema,
  disabled,
  placeholder,
}) => {
  const intl = useIntl();

  return (
    <AppForm
      onSubmit={onSubmit}
      initialValues={{
        name: group?.name ?? location?.name ?? '',
        phone: location.phone,
      }}
      validationSchema={validationSchema}
    >
      <FormInputType.Input
        name="name"
        labelId={group ? 'settings.group.name' : 'settings.location.name'}
        placeholder={placeholder}
        autoFocus
        disabled={disabled}
      />
      <FormInputType.Input name="phone" labelId="settings.location.phone" />

      <SubmitButton
        title={intl.formatMessage({ id: 'profile.overview.submit' })}
        dataCy="submitButton"
        theme={defaultPrimaryButtonTheme}
      />
    </AppForm>
  );
};
