import React from 'react';
import { useIntl } from 'react-intl';
import { EditForm } from 'components/general/EditLocationsProfile/ProfileSection/EditForm';
import {
  StyledButtonWrapper,
  Wrapper,
} from 'components/general/EditLocationsProfile/EditLocationsProfile.styled';
import { PrimaryButton } from 'components/general/index';
import { DisplaySection } from './DisplaySection';

export const ProfileSection = ({
  group,
  location,
  isEditProfileMode,
  setIsEditProfileMode,
  onSubmit,
  validationSchema,
  profileFields,
  dataCy,
  disabled,
  placeholder,
}) => {
  const intl = useIntl();

  return (
    <>
      {!isEditProfileMode ? (
        <Wrapper>
          {profileFields.map(field => (
            <div key={field.name}>
              <DisplaySection name={field.name} value={field.value} />
            </div>
          ))}
          <StyledButtonWrapper>
            <PrimaryButton
              data-cy={dataCy}
              onClick={() => setIsEditProfileMode(true)}
            >
              {intl.formatMessage({
                id: 'profile.overview.edit',
              })}
            </PrimaryButton>
          </StyledButtonWrapper>
        </Wrapper>
      ) : (
        <EditForm
          group={group}
          location={location}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
          disabled={disabled}
          placeholder={placeholder}
        />
      )}
    </>
  );
};
