import styled from 'styled-components';

export const OverviewHeading = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 4px;
`;

export const OverviewValue = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;

export const Wrapper = styled.div`
  margin-bottom: 16px;
`;
