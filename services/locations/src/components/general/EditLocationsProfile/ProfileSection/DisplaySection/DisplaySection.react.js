import React from 'react';
import {
  OverviewHeading,
  OverviewValue,
  Wrapper,
} from './DisplaySection.styled';

export const DisplaySection = ({ name, value }) => {
  return (
    <Wrapper>
      <OverviewHeading data-cy={name}>{name}</OverviewHeading>
      <OverviewValue data-cy={value}>{value}</OverviewValue>
    </Wrapper>
  );
};
