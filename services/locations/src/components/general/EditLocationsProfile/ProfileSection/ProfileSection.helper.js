import { useIntl } from 'react-intl';

export const useGetDisplayFields = (group, location, local) => {
  const intl = useIntl();

  return [
    {
      name: intl.formatMessage({
        id: `settings.${local}.name`,
      }),
      value:
        group?.name ??
        location?.name ??
        intl.formatMessage({ id: 'location.defaultName' }),
    },
    {
      name: intl.formatMessage({
        id: `settings.location.phone`,
      }),
      value: location?.phone,
    },
  ];
};
