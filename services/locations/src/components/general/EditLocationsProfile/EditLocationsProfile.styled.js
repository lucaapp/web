import styled from 'styled-components';
import { ButtonWrapper } from 'components/App/LocationSettings/SettingsOverview/SettingsOverview.styled';

export const StyledButtonWrapper = styled(ButtonWrapper)`
  position: absolute;
  right: 0;
  bottom: 0;
  margin-bottom: 0;
`;

export const Wrapper = styled.div`
  position: relative;
`;
