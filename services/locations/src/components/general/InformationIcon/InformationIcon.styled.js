import styled from 'styled-components';
import { QuestionmarkBlueIcon } from 'assets/icons';

export const InformationIcon = styled(QuestionmarkBlueIcon)`
  font-size: 16px;
  margin-left: 8px;
  cursor: pointer;
  align-self: center;
`;
