import React from 'react';

import { ModalContext } from './ModalContext';

export const useModalContext = () => React.useContext(ModalContext);
