import React, { cloneElement, useState, useEffect } from 'react';

import isFunction from 'lodash/isFunction';

import { zIndex } from 'constants/layout';
import { AppModal } from './AppModal.react';
import { MEDIUM_MODAL } from './Modal.constants';
import { ModalContext } from './ModalContext';

export const Modal = ({
  openButton,
  content,
  afterClose = () => null,
  className = 'noHeader',
  closable = true,
  footer = null,
  index = zIndex.modalArea,
  size = MEDIUM_MODAL,
  title = '',
  visible = false,
}) => {
  const [isModalVisible, setIsModalVisible] = useState(visible);

  useEffect(() => {
    if (!visible) return;

    setIsModalVisible(true);
  }, [visible]);

  const openModal = () => {
    setIsModalVisible(true);
  };

  const closeModal = () => {
    return setIsModalVisible(false);
  };

  const values = {
    afterClose,
    className,
    closable,
    closeModal,
    footer,
    index,
    isModalVisible,
    openModal,
    size,
    title,
  };

  const cloneReactElement = (element, properties = null) =>
    element && cloneElement(element, { ...properties });

  const openModalButton = () =>
    cloneReactElement(openButton, { onClick: openModal });

  const modalContent = () => {
    // This is useful when a React.Element have sideEffects.
    const element = isFunction(content) ? content() : content;
    return <AppModal>{cloneReactElement(element)}</AppModal>;
  };

  return (
    <ModalContext.Provider value={values}>
      {openModalButton()}
      {modalContent()}
    </ModalContext.Provider>
  );
};
