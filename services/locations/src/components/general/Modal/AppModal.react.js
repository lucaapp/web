import React from 'react';

import { upperCaseFirstLetter } from 'utils/strings';
import { SmallModal, MediumModal, LargeModal } from './Modal.styled';
import { useModalContext } from './useModalContext';

const modalTypes = {
  SmallModal,
  MediumModal,
  LargeModal,
};

export const AppModal = ({ children }) => {
  const {
    afterClose,
    className,
    closable,
    footer,
    closeModal,
    index,
    isModalVisible,
    size,
    title,
  } = useModalContext();

  const modal = `${upperCaseFirstLetter(size)}Modal`;
  if (!(modal in modalTypes)) {
    return null;
  }

  const ModalType = modalTypes[modal];

  return (
    <ModalType
      afterClose={afterClose}
      centered
      className={className}
      closable={closable}
      footer={footer}
      onCancel={closeModal}
      title={title}
      visible={isModalVisible}
      zIndex={index}
      maskClosable={false}
      destroyOnClose
    >
      {children}
    </ModalType>
  );
};
