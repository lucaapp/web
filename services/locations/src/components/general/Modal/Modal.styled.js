import { Modal } from 'antd';
import styled from 'styled-components';

export const SmallModal = styled(Modal)`
  width: 542px !important;
`;

export const MediumModal = styled(Modal)`
  width: 814px !important;
`;

export const LargeModal = styled(Modal)`
  width: 1036px !important;
`;
