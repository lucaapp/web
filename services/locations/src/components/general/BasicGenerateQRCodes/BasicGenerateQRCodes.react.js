import React, { useCallback, useRef, useState } from 'react';
import { useIntl } from 'react-intl';

import { DEFAULT_CWA_VALUE } from 'constants/cwaQrCodeValue';
import { useWorker } from 'components/hooks/useWorker';
import { downloadPDF } from 'utils/downloadPDF';
import { getPDFWorker } from 'utils/workers';
import {
  hideProgressBar,
  DOWNLOAD_QR_CODE_LOADING_MESSAGE,
} from 'utils/progressBar';

import {
  InformationIcon,
  LocationCard,
  StyledTooltip,
} from 'components/general';

import {
  QRCodeCSVDownload,
  QrCwaCompatible,
} from 'components/general/QRCodesDownload';
import {
  ButtonWrapper,
  CollapsableWrapper,
  StyledButtonSection,
  StyledCSVWrapper,
  StyledPrimaryButton,
} from './BasicGenerateQRCodes.styled';

export const BasicGenerateQRCodes = ({
  location,
  isLocationQRCodeEnabled,
  isTableQRCodeEnabled,
  locationTables,
  children,
  QRPrint,
}) => {
  const intl = useIntl();
  const [isDownloading, setIsDownloading] = useState(false);
  const [isCWAEventEnabled, setIsCWAEventEnabled] = useState(DEFAULT_CWA_VALUE);

  const worker = useRef(getPDFWorker());
  const cleanup = useCallback(() => {
    hideProgressBar(DOWNLOAD_QR_CODE_LOADING_MESSAGE);
  }, []);
  const pdfWorkerApiReference = useWorker(worker.current, cleanup);

  const downloadOptions = {
    setIsDownloading,
    pdfWorkerApiReference,
    location: { ...location, tables: locationTables },
    intl,
    isTableQRCodeEnabled,
    isCWAEventEnabled,
  };
  const triggerDownload = () => downloadPDF(downloadOptions);

  const isDownloadDisabled =
    !isLocationQRCodeEnabled && !locationTables?.length;

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'settings.location.qrcode.headline' })}
      testId="generateQRCodes"
    >
      <CollapsableWrapper>
        {children}
        <QrCwaCompatible
          setIsCWAEventEnabled={setIsCWAEventEnabled}
          isCWAEventEnabled={isCWAEventEnabled}
        />
      </CollapsableWrapper>
      <StyledButtonSection direction="end" isLast>
        <ButtonWrapper>
          <StyledPrimaryButton
            loading={isDownloading}
            onClick={triggerDownload}
            disabled={isDownloadDisabled}
            data-cy="qrCodeDownload"
          >
            {intl.formatMessage({ id: 'settings.location.qrcode.generate' })}
          </StyledPrimaryButton>
          <StyledCSVWrapper>
            <QRCodeCSVDownload
              location={location}
              isCWAEventEnabled={isCWAEventEnabled}
              isTableQRCodeEnabled={isTableQRCodeEnabled}
              disabled={isDownloadDisabled}
              locationTables={locationTables}
            />
            <StyledTooltip
              title={intl.formatMessage({
                id: 'settings.location.qrcode.infoText',
              })}
            >
              <InformationIcon data-cy="qrCodeCsvInfoIcon" />
            </StyledTooltip>
          </StyledCSVWrapper>
        </ButtonWrapper>
      </StyledButtonSection>
      {QRPrint}
    </LocationCard>
  );
};
