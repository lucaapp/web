import {
  GastroBlackIcon,
  OtherBlackIcon,
  RoomBlackIcon,
  HouseBlackIcon,
  StoreBlackIcon,
  NursingBlackIcon,
  HotelBlackIcon,
} from 'assets/icons';

export const groupEnumTypes = {
  RESTAURANT_TYPE: 'restaurant',
  BASE_TYPE: 'base',
  NURSING_HOME_TYPE: 'nursing_home',
  HOTEL_TYPE: 'hotel',
  STORE_TYPE: 'store',
};

const locationEnumTypes = {
  RESTAURANT_TYPE: 'restaurant',
  ROOM_TYPE: 'room',
  BUILDING_TYPE: 'building',
  BASE_TYPE: 'base',
};

export const locationOptions = [
  {
    type: locationEnumTypes.RESTAURANT_TYPE,
    intlId: 'modal.createGroup.selectType.restaurant',
    icon: GastroBlackIcon,
  },
  {
    type: locationEnumTypes.ROOM_TYPE,
    intlId: 'modal.createLocation.selectType.room',
    icon: RoomBlackIcon,
  },
  {
    type: locationEnumTypes.BUILDING_TYPE,
    intlId: 'modal.createLocation.selectType.building',
    icon: HouseBlackIcon,
  },
  {
    type: locationEnumTypes.BASE_TYPE,
    intlId: 'modal.createGroup.selectType.base',
    icon: OtherBlackIcon,
  },
];

export const groupOptions = [
  {
    type: groupEnumTypes.RESTAURANT_TYPE,
    intlId: 'modal.createGroup.selectType.restaurant',
    icon: GastroBlackIcon,
  },
  {
    type: groupEnumTypes.STORE_TYPE,
    intlId: 'modal.createGroup.selectType.store',
    icon: StoreBlackIcon,
  },
  {
    type: groupEnumTypes.HOTEL_TYPE,
    intlId: 'modal.createGroup.selectType.hotel',
    icon: HotelBlackIcon,
  },
  {
    type: groupEnumTypes.NURSING_HOME_TYPE,
    intlId: 'modal.createGroup.selectType.nursing_home',
    icon: NursingBlackIcon,
  },
  {
    type: groupEnumTypes.BASE_TYPE,
    intlId: 'modal.createGroup.selectType.base',
    icon: OtherBlackIcon,
  },
];
