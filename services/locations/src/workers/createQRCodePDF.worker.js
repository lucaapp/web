import { expose } from 'comlink';
import { jsPDF } from 'jspdf';
import qrcode from 'yaqrcode';
import { LUCA_SVG_BASE_64 } from 'constants/base64Logo';
import { generateQRData } from 'utils/qrCodeData';

const ROWS = 3;
const COLUMNS = 4;

const drawCuttedText = (text, length, pdf, x, y) => {
  const parts = pdf.splitTextToSize(text, length);
  const isTextFitting = parts.length === 1;
  pdf.text(`${parts[0]}${isTextFitting ? '' : '...'}`, x, y, {
    align: 'center',
  });
};

const drawQRCode = ({
  pdf,
  x,
  y,
  base64QR,
  areaName = '',
  tableLabel = '',
  locationName,
}) => {
  const maxTextLength = 40;
  if (areaName) {
    pdf.setFontSize(9);
    pdf.setFont(undefined, 'bold');
    const yOffset = tableLabel ? 10 : 15;
    drawCuttedText(areaName, maxTextLength, pdf, 35 + x, yOffset + y);
  }
  if (tableLabel) {
    pdf.setFontSize(8);
    pdf.setFont(undefined, 'normal');
    drawCuttedText(tableLabel, maxTextLength, pdf, 35 + x, 15 + y);
  }
  pdf.rect(0 + x, 0 + y, 70, 74.25);
  pdf.addImage(base64QR, 'png', 15 + x, 17 + y, 40, 40);
  pdf.setFillColor('#FFFFFF');
  pdf.rect(29 + x, 33 + y, 12, 8, 'F');
  pdf.addImage(LUCA_SVG_BASE_64, 'png', 31 + x, 35 + y, 8, 4);
  pdf.setFontSize(9);
  pdf.setFont(undefined, 'bold');
  drawCuttedText(locationName, maxTextLength, pdf, 35 + x, 62 + y);
};

const PDFCreator = {
  getPDF(
    location,
    isTableQRCodeEnabled,
    isCWAEventEnabled,
    path,
    tablePrefix,
    additionalData,
    progressCallback
  ) {
    const pdf = new jsPDF();

    const qrCodeData = generateQRData(
      location,
      path,
      isTableQRCodeEnabled,
      isCWAEventEnabled,
      additionalData
    );

    const pages = Math.ceil(qrCodeData.length / 12);
    const tables = location.tables || [];

    if (isTableQRCodeEnabled) {
      let id = 0;
      for (let page = 0; page < pages; page++) {
        for (let column = 0; column < COLUMNS; column++) {
          for (let row = 0; row < ROWS; row++) {
            if (id >= qrCodeData.length) break;
            const base64QR = qrcode(qrCodeData[id], {
              size: 800,
            });
            drawQRCode({
              pdf,
              x: row * 70,
              y: column * 74.25,
              base64QR,
              areaName: location.name,
              tableLabel: `${tablePrefix} ${
                tables[id]?.customName || tables[id]?.internalNumber
              }`,
              locationName: location.groupName,
            });
            const percentage = Math.round((id / qrCodeData.length) * 100);
            progressCallback(percentage);
            id += 1;
          }
        }
        pdf.addPage();
      }
      pdf.deletePage(pages + 1);
    } else {
      const base64QR = qrcode(qrCodeData[0], {
        size: 800,
      });
      drawQRCode({
        pdf,
        x: 0,
        y: 0,
        base64QR,
        areaName: location.name,
        locationName: location.groupName,
      });
    }
    return pdf.output('blob');
  },
};

expose(PDFCreator);
