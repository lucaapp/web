import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { HelmetProvider } from 'react-helmet-async';
import { IntercomProvider } from 'react-use-intercom';

import moment from 'moment';
import 'moment/locale/de';

import { MainRouting } from 'MainRouting.react';

import { INTERCOM_APP_ID, INTERCOM_APP_API_BASE } from 'constants/intercom';

import { getLanguage } from 'utils/language';
import { ErrorWrapper } from 'components/ErrorWrapper';

import { InternationalizeProvider } from 'components/context';

import { createInstance, MatomoProvider } from '@jonkoops/matomo-tracker-react';
import { MATOMO_BASE_URL, MATOMO_SITE_ID } from 'constants/matomo';
import { StoreContextProvider } from './store-context/StoreContextProvider';

moment.locale(getLanguage());
document.documentElement.lang = getLanguage();

const queryClient = new QueryClient();

const matomoTrackerInstance = createInstance({
  urlBase: MATOMO_BASE_URL,
  siteId: MATOMO_SITE_ID,
  disabled: process.env.NODE_ENV !== 'production',
  heartBeat: {
    active: true,
    seconds: 10,
  },
  linkTracking: true,
  configurations: {
    disableCookies: true,
    setSecureCookie: true,
    setRequestMethod: 'POST',
  },
});

export const Main = () => {
  return (
    <MatomoProvider value={matomoTrackerInstance}>
      <HelmetProvider>
        <QueryClientProvider client={queryClient}>
          <StoreContextProvider>
            <InternationalizeProvider>
              <BrowserRouter>
                <ErrorWrapper>
                  <IntercomProvider
                    appId={INTERCOM_APP_ID}
                    apiBase={INTERCOM_APP_API_BASE}
                  >
                    <MainRouting />
                  </IntercomProvider>
                </ErrorWrapper>
              </BrowserRouter>
            </InternationalizeProvider>
          </StoreContextProvider>
        </QueryClientProvider>
      </HelmetProvider>
    </MatomoProvider>
  );
};
