import { useContextSelector } from 'use-context-selector';

import { StoreContext } from './StoreContext';

export const useUser = () =>
  useContextSelector(StoreContext, state => state.user);

export const useLanguage = () =>
  useContextSelector(StoreContext, state => state.language);
