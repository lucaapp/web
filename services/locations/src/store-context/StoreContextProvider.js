import React from 'react';
import { useQuery } from 'react-query';

import { QUERY_KEYS, useLanguage } from 'components/hooks';
import { getMe } from 'network/api';
import { StoreContext } from './StoreContext';

const useGetMe = () => {
  const options = {
    cacheTime: 0,
    retry: false,
  };

  return useQuery(QUERY_KEYS.ME, getMe, options);
};

const useStore = () => {
  const user = useGetMe();
  const language = useLanguage(user?.data?.languageOverwrite);

  return { language, user };
};

export const StoreContextProvider = ({ children }) => (
  <StoreContext.Provider value={useStore()}>{children}</StoreContext.Provider>
);
