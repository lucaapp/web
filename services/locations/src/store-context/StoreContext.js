import { createContext } from 'use-context-selector';

export const StoreContext = createContext(null);
