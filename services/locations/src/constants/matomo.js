const matomoBaseUrl = 'https://lucaapp.matomo.cloud/';
const matomoSiteId = 1;

export const MATOMO_BASE_URL = process.env.MATOMO_BASE_URL || matomoBaseUrl;
export const MATOMO_SITE_ID = process.env.MATOMO_SITE_ID || matomoSiteId;
