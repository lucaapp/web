import moment from 'moment';

export const COUNTER_REFETCH_INTERVAL_MS = moment
  .duration('5', 'minutes')
  .as('ms');
