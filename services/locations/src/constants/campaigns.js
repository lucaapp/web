export const CAMPAIGNS = {
  PAY_RAFFLE_AND_TIPTOPUP: 'PayRaffleAndTipTopUp',
  LUCA_DISCOUNT_50: 'luca-discount-50',
  LUCA_DISCOUNT: 'luca-discount',
  LUCA_TIP_TOP_UP: 'luca-tipTopUp',
  LUCA_TIP_TOP_UP_10: 'luca-tipTopUp-10',
};
