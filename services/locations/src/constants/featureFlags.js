export const defaultFeatureFlags = {
  anonymous_checkins: false,
  enable_two_g_plus: false,
  enable_staff_members: false,
  pos_integration: false,
};
