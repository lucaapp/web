export const PRIVATE_KEY_MODAL_SEEN_STORAGE_KEY = 'PRIVATE_KEY_MODAL_SEEN';
export const ENCRYPTED_PRIVATE_KEY_SESSION_KEY = 'ENCRYPTED_PRIVATE_KEY';
export const BACKEND_JWT = 'BACKEND_JWT';
export const DAILY_KEY_BANNER_SEEN_STORAGE_KEY =
  'DAILY_KEY_BANNER_SEEN_STORAGE_KEY';
export const IS_OPERATOR_FROM_MAGIC_LOGIN_KEY =
  'IS_OPERATOR_FROM_MAGIC_LINK_KEY';
export const SUBCATEGORY_MODAL_SEEN_STORAGE_KEY =
  'SUBCATEGORY_MODAL_SEEN_STORAGE_KEY';
