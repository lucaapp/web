export const DEFAULT_CHECKOUT_RADIUS = 50;
export const MAX_CHECKOUT_RADIUS = 5000;
export const DEFAULT_AVERAGE_CHECKIN_TIME = 90;
export const MAX_AVERAGE_CHECKIN_TIME = 1440;
export const MIN_AVERAGE_CHECKIN_TIME = 15;
export const MIN_VAT = 3;
export const MAX_VAT = 12;
