import { verifySignedLocationTransfer } from '@lucaapp/crypto';
import { getRootCA, getBasicCA } from 'network/static';
import { getIssuer } from 'network/api';

export const getSignatureValidityForTransfer = (
  transfer,
  ...certificateChain
) =>
  getIssuer(transfer.departmentId)
    .then(issuer => {
      const decodedLocationTransfer = verifySignedLocationTransfer({
        certificateChain,
        signedLocationTransfer: transfer.signedLocationTransfer,
        issuer,
      });

      return { ...transfer, time: decodedLocationTransfer.time };
    })
    .catch(error => console.error(error));

export const getSignatureValidityForAllTransfers = transfers => {
  const pems = [getRootCA(), getBasicCA()];

  return Promise.all(pems)
    .then(certificateChain =>
      Promise.all(
        transfers.map(transfer =>
          getSignatureValidityForTransfer(transfer, ...certificateChain)
        )
      )
    )
    .then(checkedTransfers =>
      checkedTransfers.filter(checkedTransfer => !!checkedTransfer)
    );
};
