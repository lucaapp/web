import React from 'react';

// eslint-disable-next-line import/no-extraneous-dependencies
import '@testing-library/jest-dom';
// eslint-disable-next-line import/no-extraneous-dependencies
import { render, queries, configure } from '@testing-library/react';

// Providers
import { QueryClient, QueryClientProvider } from 'react-query';
import { IntlProvider } from 'react-intl';
import { BrowserRouter as Router } from 'react-router-dom';

const queryClient = new QueryClient();

const customRender = ui => {
  // We overwrite the onError method to be able to check for translation keys rather than actual translations
  return render(
    <Router>
      <IntlProvider locale="in" onError={() => {}}>
        <QueryClientProvider client={queryClient}>{ui}</QueryClientProvider>
      </IntlProvider>
    </Router>,
    {
      queries: {
        ...queries,
      },
    }
  );
};

// this is needed to use matchMedia function in jest which is used by some ant packages
global.matchMedia =
  global.matchMedia ||
  // eslint-disable-next-line func-names
  function () {
    return {
      matches: false,
      addListener() {},
      removeListener() {},
    };
  };

// re-export everything
// eslint-disable-next-line import/no-extraneous-dependencies
export * from '@testing-library/react';

// override render method
export { customRender as render };

export function delay(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
export async function waitToBeCalled(mockFunction, maxChecks = 50) {
  for (let check = 0; check < maxChecks; check++) {
    // eslint-disable-next-line no-await-in-loop
    await delay(200);
    if (mockFunction.mock.calls.length > 0) {
      return;
    }
  }
}

// check if all selected elements by key are available after rendering
export function testAllDefined(rendered) {
  for (const key of Object.keys(rendered)) {
    expect(rendered[key]).toBeDefined();
  }
}

configure({ testIdAttribute: 'data-cy' });
