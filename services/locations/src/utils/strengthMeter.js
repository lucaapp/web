import {
  mediumPassword,
  strongPassword,
  veryStrongPassword,
  veryWeakPassword,
  weakPassword,
} from 'components/general/StrengthMeter/StrengthMeter.styled';

export const VERY_WEAK_PASSWORD = 0;
export const WEAK_PASSWORD = 1;
export const MEDIUM_PASSWORD = 2;
export const STRONG_PASSWORD = 3;
export const VERY_STRONG_PASSWORD = 4;

export const mapStrengthScore = {
  [VERY_WEAK_PASSWORD]: 'authentication.setPassword.passwordVeryWeak',
  [WEAK_PASSWORD]: 'authentication.setPassword.passwordWeak',
  [MEDIUM_PASSWORD]: 'authentication.setPassword.passwordMedium',
  [STRONG_PASSWORD]: 'authentication.setPassword.passwordStrong',
  [VERY_STRONG_PASSWORD]: 'authentication.setPassword.passwordVeryStrong',
};

export const mapStrengthScoreDataCy = {
  [VERY_WEAK_PASSWORD]: 'descriptionPasswordVeryWeak',
  [WEAK_PASSWORD]: 'descriptionPasswordWeak',
  [MEDIUM_PASSWORD]: 'descriptionPasswordMedium',
  [STRONG_PASSWORD]: 'descriptionPasswordStrong',
  [VERY_STRONG_PASSWORD]: 'descriptionPasswordVeryStrong',
};

export const mapStyleToScore = {
  [VERY_WEAK_PASSWORD]: veryWeakPassword,
  [WEAK_PASSWORD]: weakPassword,
  [MEDIUM_PASSWORD]: mediumPassword,
  [STRONG_PASSWORD]: strongPassword,
  [VERY_STRONG_PASSWORD]: veryStrongPassword,
};
