export const upperCaseFirstLetter = subject => {
  if (typeof subject !== 'string' || !subject) return '';

  return subject.replace(/^[a-z]/i, value => value.toUpperCase());
};

export const extractFirstLetterUppercase = string => {
  if (typeof string !== 'string' || !string) return '';

  return string.charAt(0).toUpperCase();
};

export const getNameInitials = (firstName, lastName) => {
  return `${extractFirstLetterUppercase(
    firstName
  )}${extractFirstLetterUppercase(lastName)}`;
};
