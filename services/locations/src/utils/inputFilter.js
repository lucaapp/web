// To be used as onKeyDown event, will prevent anything but numbers to be entered
export const inputOnlyNumbersFilter = event_ => {
  // allow only numbers
  if (event_.key.match(/\d/)) {
    return;
  }

  // All allowed special keys
  if (['Backspace', 'Delete', 'Enter'].includes(event_.key)) {
    return;
  }

  event_.preventDefault(); // if none of the previous matched suppress key press event
};
