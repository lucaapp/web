/**
 * Get amount of invoice amount and luca discount amount if applicable
 * @param payment
 * @returns number
 */
export const getAmount = payment =>
  payment.lucaDiscount
    ? payment.lucaDiscount.originalInvoiceAmount
    : payment.invoiceAmount;

/**
 * Get total amount including invoice amount, tip amount and
 * luca discount amount if applicable
 * @param payment
 * @returns number
 */
export const getTotalAmount = payment => {
  const { tipAmount } = payment;
  const amountPayed = getAmount(payment);
  return amountPayed + tipAmount;
};

export const getTotalFees = payment => payment.fixedFee + payment.variableFee;
