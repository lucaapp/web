import { z as zod } from 'zod';
import * as crypto from '@lucaapp/crypto';
import validator from 'validator';

export const z = {
  ...zod,

  base64: ({ min, max, length, rawLength }) =>
    zod
      .string()
      .min(min)
      .max(max)
      .length(length)
      .refine(value => {
        if (!validator.isBase64(value)) return false;
        if (!rawLength) return true;

        return crypto.base64ToBytes(value).length === rawLength;
      }),

  ecPublicKey: () => z.base64({ length: 88, rawLength: 65 }),

  ecCompressedPublicKey: () => z.base64({ length: 44, rawLength: 33 }),

  ecSignature: () => z.base64({ max: 120 }),

  iv: () => z.base64({ length: 24, rawLength: 16 }),

  mac: () => z.base64({ length: 44, rawLength: 32 }),

  traceId: () => z.base64({ length: 24, rawLength: 16 }),

  dailyKeyId: () => zod.number().int().min(0).max(34),
};
