import {
  PRIVATE_KEY_MODAL_SEEN_STORAGE_KEY,
  IS_OPERATOR_FROM_MAGIC_LOGIN_KEY,
  SUBCATEGORY_MODAL_SEEN_STORAGE_KEY,
} from 'constants/storage';

export const setHasSeenPrivateKeyModal = value =>
  localStorage.setItem(PRIVATE_KEY_MODAL_SEEN_STORAGE_KEY, value.toString());

export const hasSeenPrivateKeyModal = () =>
  localStorage.getItem(PRIVATE_KEY_MODAL_SEEN_STORAGE_KEY);

export const clearHasSeenPrivateKeyModal = () =>
  sessionStorage.removeItem(PRIVATE_KEY_MODAL_SEEN_STORAGE_KEY);

// Magic login
export const setIsOperatorFromMagicLogin = value =>
  sessionStorage.setItem(IS_OPERATOR_FROM_MAGIC_LOGIN_KEY, value.toString());

export const getIsOperatorFromMagicLogin = () =>
  sessionStorage.getItem(IS_OPERATOR_FROM_MAGIC_LOGIN_KEY);

export const setHasSeenSubcategoryModal = value =>
  localStorage.setItem(SUBCATEGORY_MODAL_SEEN_STORAGE_KEY, value.toString());

export const hasSeenSubcategoryModal = () =>
  localStorage.getItem(SUBCATEGORY_MODAL_SEEN_STORAGE_KEY);
