export function sortTraces(traces) {
  return traces.sort((traceA, traceB) => {
    if (!traceA.checkout) {
      return -1;
    }

    if (!traceB.checkout) {
      return 1;
    }

    return traceB.checkout - traceA.checkout;
  });
}

export const sortByName = (a, b) => {
  if (a.firstName !== b.firstName) {
    return a.firstName.localeCompare(b.firstName, 'de', {
      sensitivity: 'base',
    });
  }
  return a.lastName.localeCompare(b.lastName, 'de', {
    sensitivity: 'base',
  });
};
