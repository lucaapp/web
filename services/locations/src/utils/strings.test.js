import { upperCaseFirstLetter } from './strings';

describe('Strings /', () => {
  describe('upperCaseFirstLetter /', () => {
    it('should return an empry string if argument is falsy', () => {
      expect(upperCaseFirstLetter()).toBeFalsy();
    });

    it('should return fist letter as upperCase', () => {
      expect(upperCaseFirstLetter('a')).toBe('A');
    });
  });
});
