import { useMemo } from 'react';

import { isValidUrlParameter } from 'utils/validators';

export const getCampaignParameters = queryParameters => {
  const urlParameters = new URLSearchParams(queryParameters);
  const utmMedium = urlParameters.get('utm_medium');
  const utmSource = urlParameters.get('utm_source');

  if (
    !(utmMedium && isValidUrlParameter(utmMedium)) ||
    !(utmSource && isValidUrlParameter(utmSource))
  ) {
    return {
      utm_medium: 'direct',
      utm_source: 'direct',
    };
  }
  return {
    utm_medium: utmMedium,
    utm_source: utmSource,
  };
};

export function useCampaign(queryParameters) {
  return useMemo(() => {
    return getCampaignParameters(queryParameters);
  }, [queryParameters]);
}
