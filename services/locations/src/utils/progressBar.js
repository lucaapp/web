import React from 'react';
import styled from 'styled-components';
import { message, Progress } from 'antd';

const MessageProgressWrapper = styled.div`
  display: inline-block;
  width: 600px;
`;

export const DOWNLOAD_QR_CODE_LOADING_MESSAGE = 'downloadQrCodeLoadingMessage';
export const DOWNLOAD_CSV_PAYMENT_LOADING_MESSAGE =
  'downloadCsvPaymentLoadingMessage';

export const showProgressBar = (progress, messageText, key) => {
  return message.loading({
    content: (
      <MessageProgressWrapper>
        {messageText}
        {progress && <Progress percent={progress} strokeColor="#303d4b" />}
      </MessageProgressWrapper>
    ),
    duration: 0,
    key,
  });
};

export const hideProgressBar = messageKey => message.destroy(messageKey);
