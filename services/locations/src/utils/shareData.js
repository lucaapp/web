export const getIncompleteTransfers = transfers =>
  transfers.filter(transfer => !!transfer.contactedAt && !transfer.isCompleted);

export const getCompleteTransfers = transfers =>
  transfers.filter(transfer => transfer.isCompleted);
