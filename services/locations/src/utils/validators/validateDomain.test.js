/* eslint-disable sonarjs/no-duplicate-string */
import {
  isValidDomain,
  isHTTPValidDomain,
  isHTTPSValidDomain,
} from './validateDomain';

describe('Validate Domain /', () => {
  const http = 'http://';
  const https = 'https://';

  const domain = 'nexenio.com';

  describe('Is Valid Domain', () => {
    describe('should return true', () => {
      it('if domain is nexenio.com', () => {
        const result = isValidDomain(domain);

        expect(result).toBe(true);
      });

      it('if domain is http://nexenio.com', () => {
        const result = isValidDomain(http + domain);

        expect(result).toBe(true);
      });

      it('if domain is https://nexenio.com', () => {
        const result = isValidDomain(https + domain);

        expect(result).toBe(true);
      });
    });

    describe('should return false', () => {
      it('if domain is nexenio', () => {
        const result = isValidDomain('nexenio');

        expect(result).toBe(false);
      });

      it('if domain is nexenio.', () => {
        const result = isValidDomain('nexenio.');

        expect(result).toBe(false);
      });

      it('if domain is nexenio.c', () => {
        const result = isValidDomain('nexenio.c');

        expect(result).toBe(false);
      });
    });
  });

  describe('Is HTTP Valid Domain', () => {
    describe('should return true', () => {
      it('if domain is http://nexenio.com', () => {
        const result = isHTTPValidDomain(http + domain);

        expect(result).toBe(true);
      });
    });

    describe('should return false', () => {
      it('if domain is nexenio.com', () => {
        const result = isHTTPValidDomain(domain);

        expect(result).toBe(false);
      });

      it('if domain is https://nexenio.com', () => {
        const result = isHTTPValidDomain(https + domain);

        expect(result).toBe(false);
      });
    });
  });

  describe('Is HTTPS Valid Domain', () => {
    describe('should return true', () => {
      it('if domain is https://nexenio.com', () => {
        const result = isHTTPSValidDomain(https + domain);

        expect(result).toBe(true);
      });
    });

    describe('should return false', () => {
      it('if domain is nexenio.com', () => {
        const result = isHTTPSValidDomain(domain);

        expect(result).toBe(false);
      });

      it('if domain is http://nexenio.com', () => {
        const result = isHTTPSValidDomain(http + domain);

        expect(result).toBe(false);
      });
    });
  });
});
