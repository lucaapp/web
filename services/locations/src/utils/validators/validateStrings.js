import {
  DEFAULT_LOCATION_NAME_DE,
  DEFAULT_LOCATION_NAME_EN,
} from 'constants/locations';

const SAFE_CHARACTERS_REGEX = /^[\w !&()+,./:@`|£À-ÿāăąćĉċčđēėęěĝğģĥħĩīįİıĵķĸĺļłńņōőœŗřśŝşšţŦũūŭůűųŵŷźżžơưếệ–-]*$/i;
const NO_HTTP_REGEX = /^((?!http).)*$/i;
const NO_FTP_REGEX = /^((?!ftp).)*$/i;

const ALPHANUMERIC_REGEX = /^[\da-z]+$/i;

export const isValidCharacter = value =>
  SAFE_CHARACTERS_REGEX.test(value) &&
  NO_HTTP_REGEX.test(value) &&
  NO_FTP_REGEX.test(value);

export const isValidTextCharacter = value =>
  NO_HTTP_REGEX.test(value) && NO_FTP_REGEX.test(value);

export const isNotDefaultLocationName = value =>
  value?.toLowerCase() !== DEFAULT_LOCATION_NAME_EN &&
  value?.toLowerCase() !== DEFAULT_LOCATION_NAME_DE;

export const isValidUrlParameter = value => ALPHANUMERIC_REGEX.test(value);
