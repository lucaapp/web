import {
  isValidPhoneNumber,
  isValidMobilePhoneNumber,
} from './validatePhoneNumber';

describe('Validate phone number', () => {
  it('accepts only valid number formats', () => {
    expect(isValidPhoneNumber('1232421')).toEqual(false);
    expect(isValidPhoneNumber('')).toEqual(false);
    expect(isValidPhoneNumber('030901820')).toEqual(false);
    expect(isValidPhoneNumber('+49 69 1234 5678')).toEqual(true);
  });

  it('accepts only mobile number', () => {
    expect(isValidMobilePhoneNumber('+49 69 1234 5678')).toEqual(false);
    expect(isValidMobilePhoneNumber('+49 180 1234 5678')).toEqual(false);
    expect(isValidMobilePhoneNumber('+49 211 1234567')).toEqual(false);
    expect(isValidMobilePhoneNumber('0 1511-1234124')).toEqual(false);
    expect(isValidMobilePhoneNumber('+49 1511-1234124')).toEqual(true);
    expect(isValidMobilePhoneNumber('+49 179 123 4567')).toEqual(true);
  });
});
