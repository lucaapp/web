const VAT_NUMBER_REGEX = /(DE)\d{9}/g;

export const validateVatNumber = value => {
  if (typeof value !== 'string') return false;

  return value.match(VAT_NUMBER_REGEX) !== null;
};
