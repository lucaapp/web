import { validateVatNumber } from './validateVatNumber';

describe('Vat number validation', () => {
  it('accepts valid vat numbers', () => {
    const validVatNumbers = ['DE123456789', 'DE9876544557'];
    validVatNumbers.map(vatNumber =>
      expect(validateVatNumber(vatNumber)).toBe(true)
    );
  });

  it('rejects invalid vat numbers', () => {
    const inValidVatNumbers = [
      '0',
      '',
      'EN123456789',
      'DE12345678',
      'CN918282888888',
      1010188,
      undefined,
    ];
    inValidVatNumbers.map(vatNumber =>
      expect(validateVatNumber(vatNumber)).toBe(false)
    );
  });
});
