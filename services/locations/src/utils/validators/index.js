export * from './validatePhoneNumber';
export * from './validateVatNumber';
export * from './validateStrings';
export * from './validateDataFormat';
export * from './validateDomain';
