import parsePhoneNumber from 'libphonenumber-js/max';

export const isValidPhoneNumber = (value, country = 'DE') => {
  const parsedNumber = parsePhoneNumber(value, {
    extract: false,
  });
  return !!parsedNumber?.isValid() && parsedNumber?.country === country;
};

export const isValidMobilePhoneNumber = (value, country = 'DE') => {
  const parsedNumber = parsePhoneNumber(value, {
    extract: false,
  });
  return (
    !!parsedNumber?.isValid() &&
    parsedNumber?.getType() === 'MOBILE' &&
    parsedNumber?.country === country
  );
};
