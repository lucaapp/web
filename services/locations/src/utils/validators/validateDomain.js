const patterns = {
  domainRegex: /^(?!:\/\/)([\w-]+\.)*[\dA-Za-z][\w-]+\.[A-Za-z]{2,11}?$/,
  httpRegex: /^((http):\/\/)[\da-z]+([.-][\da-z]+)*\.[a-z]{2,6}(:\d{1,5})?(\/.*)?$/i,
  httpsRegex: /^((https):\/\/)[\da-z]+([.-][\da-z]+)*\.[a-z]{2,6}(:\d{1,5})?(\/.*)?$/i,
};

const validateSinglePattern = value => pattern => patterns[pattern].test(value);

const domainRules = [
  patterns.domainRegex,
  patterns.httpRegex,
  patterns.httpsRegex,
];

const validatiMultiPatterns = value =>
  domainRules.some(regexRule => regexRule.test(value));

const validateDomain = value => pattern => {
  const trimValue = value.trim();

  return pattern
    ? validateSinglePattern(trimValue)(pattern)
    : validatiMultiPatterns(trimValue);
};

export const isValidDomain = value => validateDomain(value)();
export const isHTTPValidDomain = value => validateDomain(value)('httpRegex');
export const isHTTPSValidDomain = value => validateDomain(value)('httpsRegex');
