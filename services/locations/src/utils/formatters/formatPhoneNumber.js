import parsePhoneNumber from 'libphonenumber-js/max';

export const getFormattedPhoneNumber = (phone, country = 'DE') =>
  parsePhoneNumber(phone, country)?.format('E.164');
