import { getFormattedPhoneNumber } from './formatPhoneNumber';

describe('Format phone number', () => {
  it('formats valid phone numbers to E164', () => {
    expect(getFormattedPhoneNumber('+49 170 123-4567')).toEqual(
      '+491701234567'
    );
  });
});
