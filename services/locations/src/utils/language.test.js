import {
  getLanguage,
  isSupportedLanguage,
  excludeCurrentLanguageFromLocales,
} from './language';

describe('Language Service /', () => {
  describe('Is Supported Language /', () => {
    it('should return true if the language is supported', () => {
      const en = isSupportedLanguage('en');
      const de = isSupportedLanguage('de');

      expect(en).toBe(true);
      expect(de).toBe(true);
    });

    it('should return false if the language is not supported', () => {
      const result = isSupportedLanguage('a');

      expect(result).toBe(false);
    });
  });

  describe('Remove Language /', () => {
    it('should remove the selected language from the list', () => {
      const en = excludeCurrentLanguageFromLocales('en');
      const de = excludeCurrentLanguageFromLocales('de');

      expect(['en']).toEqual(expect.not.arrayContaining(en));
      expect(['de']).toEqual(expect.not.arrayContaining(de));
    });
  });

  describe('Get Navigator Language /', () => {
    let languageGetter;

    beforeEach(() => {
      languageGetter = jest.spyOn(window.navigator, 'language', 'get');
    });

    it('should return current browser language if is supported', () => {
      languageGetter.mockReturnValue('de');

      const language = getLanguage();

      expect(language).toEqual('de');
    });

    it('should return default language if is not supported', () => {
      languageGetter.mockReturnValue('ro');

      const language = getLanguage();

      expect(language).toEqual('en');
    });
  });
});
