import React from 'react';
import { render } from 'utils/testing';
import isArray from 'lodash/isArray';

export const setupCustomHooks = hooks => {
  if (!isArray(hooks)) {
    throw new Error('Hoooks must be an array');
  }

  let returnHooks = {};

  function TestComponent() {
    returnHooks = hooks.reduce(
      (customHooks, hook) => ({
        ...customHooks,
        [hook.fn.name]: hook.fn(...(hook.args ?? [])),
      }),
      {}
    );

    return null;
  }

  render(<TestComponent />);

  return returnHooks;
};
