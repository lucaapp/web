import { BACKEND_JWT } from 'constants/storage';
import { getBackendJwt } from 'network/api';

export const getJwt = async () => {
  let jwt = sessionStorage.getItem(BACKEND_JWT);
  let refresh = true;
  if (jwt) {
    try {
      const token = JSON.parse(atob(jwt.split('.')[1]));
      const isExpired = new Date(parseInt(token.exp, 10) * 1000) < new Date();
      if (!isExpired) {
        refresh = false;
      }
    } catch {
      refresh = true;
    }
  }

  if (refresh) {
    jwt = await getBackendJwt();
    sessionStorage.setItem(BACKEND_JWT, jwt.toString());
  }

  return jwt;
};

export const clearJwt = () => {
  sessionStorage.removeItem(BACKEND_JWT);
};
