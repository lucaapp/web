import { proxy } from 'comlink';
import FileSaver from 'file-saver';
import {
  hideProgressBar,
  showProgressBar,
  DOWNLOAD_QR_CODE_LOADING_MESSAGE,
} from 'utils/progressBar';
import { WEB_APP_BASE_PATH } from 'constants/links';
import { getQRCodeFilename } from './qrCodeData';

export const downloadPDF = async ({
  setIsDownloading,
  pdfWorkerApiReference,
  location,
  intl,
  isTableQRCodeEnabled,
  isCWAEventEnabled,
}) => {
  const messageText = intl.formatMessage({ id: 'message.generatingPDF' });
  const showLoadingProgress = percentage =>
    showProgressBar(percentage, messageText, DOWNLOAD_QR_CODE_LOADING_MESSAGE);
  setIsDownloading(true);
  showLoadingProgress(0);
  const { getPDF } = pdfWorkerApiReference.current;
  const pdf = await getPDF(
    location,
    isTableQRCodeEnabled,
    isCWAEventEnabled,
    WEB_APP_BASE_PATH,
    intl.formatMessage({
      id: 'modal.qrCodeDocument.table',
    }),
    {},
    proxy(showLoadingProgress)
  );
  showLoadingProgress(100);

  const filename = getQRCodeFilename(
    location,
    isTableQRCodeEnabled,
    intl,
    'pdf'
  );

  FileSaver.saveAs(pdf, filename);
  setIsDownloading(false);
  hideProgressBar(DOWNLOAD_QR_CODE_LOADING_MESSAGE);
};
