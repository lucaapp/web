import { generateQRPayload } from '@lucaapp/cwa-event';
import sanitize from 'sanitize-filename';
import { groupEnumTypes } from 'components/general/locationGroupOptions';
import { CWA } from 'constants/cwaQrCodeValue';
import { bytesToBase64Url } from './encodings';

const getLocationAddress = location =>
  `${location.streetName || ''} ${location.streetNr || ''}, ${
    location.zipCode || ''
  } ${location.city || ''}`;

export const getLocationName = location =>
  location.name === null || location.name === undefined
    ? `${location.groupName}`
    : `${location.groupName} ${location.name}`;

const generateLocationCWAContentPart = location => {
  const address = getLocationAddress(location).slice(0, CWA.MAX_ADDRESS_LENGTH);
  const description = getLocationName(location).slice(
    0,
    CWA.MAX_DESCRIPTION_LENGTH
  );

  const qrCodeContent = {
    description,
    address,
    defaultcheckinlengthMinutes: location.averageCheckinTime
      ? location.averageCheckinTime
      : CWA.DEFAULT_CHECKIN_MINUTES,
    locationType:
      location.type === groupEnumTypes.RESTAURANT_TYPE
        ? CWA.LOCATION_TYPE_PERMANENT_FOOD_SERVICE
        : CWA.LOCATION_TYPE_PERMANENT_OTHER,
  };
  return generateQRPayload(qrCodeContent);
};

export const getCWAFragment = (location, isCWAEventEnabled) =>
  isCWAEventEnabled ? `/CWA1/${generateLocationCWAContentPart(location)}` : '';

export const getQRCodeFilename = (
  location,
  isTableQRCodeEnabled,
  intl,
  format
) => {
  const locationName = `_${location.name}`;
  const fileName = `${location.groupName.replace(' ', '_')}${
    location.name ? locationName.replace(new RegExp(' ', 'g'), '_') : ''
  }`;

  const fileNameLocale = isTableQRCodeEnabled
    ? 'downloadFile.locations.tableQrCodes'
    : 'downloadFile.locations.generalQrCode';

  return sanitize(
    intl.formatMessage({ id: fileNameLocale }, { fileName, format })
  );
};

export const createAdditionalDataObjectPerTable = (
  location,
  additionalDataPerLocationObject
) => {
  const tables = location.tables || [];

  return tables.map(table => ({
    ...additionalDataPerLocationObject,
    tableId: table.id || '',
  }));
};

export const generateQrCodeContent = (
  path,
  location,
  additionalDataObject,
  isCWAEventEnabled
) => {
  const sharedContentPart = `${path}${location.scannerId}`;
  return `${sharedContentPart}#${bytesToBase64Url(
    JSON.stringify(additionalDataObject)
  )}${getCWAFragment(location, isCWAEventEnabled)}`;
};

export const generateQRData = (
  location,
  path,
  isTableQRCodeEnabled,
  isCWAEventEnabled,
  additionalData
) => {
  if (isTableQRCodeEnabled && location.tables?.length) {
    return createAdditionalDataObjectPerTable(
      location,
      additionalData
    ).map(additionalDataPerTableObject =>
      generateQrCodeContent(
        path,
        location,
        additionalDataPerTableObject,
        isCWAEventEnabled
      )
    );
  }
  return [
    generateQrCodeContent(path, location, additionalData, isCWAEventEnabled),
  ];
};
