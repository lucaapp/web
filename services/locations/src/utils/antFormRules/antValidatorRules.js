import {
  DEFAULT_CHECKOUT_RADIUS,
  MAX_CHECKOUT_RADIUS,
} from 'constants/checkout';
import { upperCaseFirstLetter } from 'utils/strings';
import { MAX_TABLE_NUMBER, MIN_TABLE_NUMBER } from 'constants/tableNumber';
import {
  validateDefaultName,
  checkExistingLocation,
  validatePhoneNumber,
  validateSafeString,
  validateTextSafeString,
  validateZipCode,
  validateEmail,
  validateNoNumeric,
  validateDomain,
  validateAdditionalData,
  validateAlreadyExistingData,
  validateVATNumber,
  validateHTTPSDomain,
  validateMobilePhoneNumber,
} from './antValidatorRules.helper';

export const getRequiredRule = (intl, fieldName) => ({
  required: true,
  whitespace: true,
  message: (
    // eslint-disable-next-line react/react-in-jsx-scope
    <div data-cy={`error-provide${upperCaseFirstLetter(fieldName)}`}>
      {intl.formatMessage({
        id: `error.${fieldName}`,
      })}
    </div>
  ),
});

export const getSafeStringRule = (intl, fieldName) => ({
  validator: validateSafeString,
  message: (
    // eslint-disable-next-line react/react-in-jsx-scope
    <div data-cy={`error-invalid${upperCaseFirstLetter(fieldName)}`}>
      {intl.formatMessage({
        id: `error.${fieldName}.invalid`,
      })}
    </div>
  ),
});

export const getValidDomainRule = intl => ({
  validator: validateDomain,
  message: intl.formatMessage({ id: 'error.domain.invalid' }),
});

export const getHTTPSDomainRule = intl => ({
  validator: validateHTTPSDomain,
  message: intl.formatMessage({ id: 'error.https.domain' }),
});

export const getTextSafeStringRule = (intl, fieldName) => ({
  validator: validateTextSafeString,
  message: intl.formatMessage({ id: `error.${fieldName}.invalid` }),
});

export const getNoNumericRule = (intl, fieldName) => ({
  validator: validateNoNumeric,
  message: (
    // eslint-disable-next-line react/react-in-jsx-scope
    <div data-cy={`error-invalid${upperCaseFirstLetter(fieldName)}`}>
      {intl.formatMessage({
        id: `error.${fieldName}.invalid`,
      })}
    </div>
  ),
});

export const getMaxLengthRule = (intl, max) => ({
  max,
  message: (
    // eslint-disable-next-line react/react-in-jsx-scope
    <div data-cy="error-inputMaxLength">
      {intl.formatMessage({
        id: 'error.length',
      })}
    </div>
  ),
});

export const getPhoneRule = intl => ({
  validator: validatePhoneNumber,
  message: intl.formatMessage({ id: 'error.phone.invalid' }),
});

export const getMobilePhoneRule = intl => ({
  validator: validateMobilePhoneNumber,
  message: intl.formatMessage({ id: 'error.phone.invalid' }),
});

export const getDefaultNameRule = intl => ({
  validator: validateDefaultName,
  message: intl.formatMessage({ id: 'error.locationName.notDefault' }),
});

export const getUniqueNameRule = (intl, isLocationNameTaken) => ({
  required: isLocationNameTaken,
  validator: checkExistingLocation(isLocationNameTaken),
  message: intl.formatMessage({ id: 'error.locationName.exist' }),
});

export const getZipCodeRule = intl => ({
  validator: validateZipCode,
  // message: intl.formatMessage({ id: 'error.zipCode.invalid' }),
  message: (
    // eslint-disable-next-line react/react-in-jsx-scope
    <div data-cy="error-zipCodeDigits">
      {intl.formatMessage({
        id: 'error.zipCode.invalid',
      })}
    </div>
  ),
});

export const getEmailRule = intl => ({
  validator: validateEmail,
  message: intl.formatMessage({ id: 'error.email' }),
});

export const getTableNumberRule = (intl, isRequired = false) => ({
  pattern: /^\d*$/,
  required: isRequired,
  min: MIN_TABLE_NUMBER,
  max: MAX_TABLE_NUMBER,
  message: (
    // eslint-disable-next-line react/react-in-jsx-scope
    <div data-cy="error-tableCount">
      {intl.formatMessage({
        id: 'error.tableCountNumber.invalid',
      })}
    </div>
  ),
});

export const getCheckoutRadiusRule = intl => ({
  type: 'number',
  required: true,
  min: DEFAULT_CHECKOUT_RADIUS,
  max: MAX_CHECKOUT_RADIUS,
  message: intl.formatMessage({
    id: 'settings.location.checkout.automatic.range',
  }),
});

export const getVATRule = intl => ({
  validator: validateVATNumber,
  message: intl.formatMessage({
    id: 'error.vatNumber.invalid',
  }),
});

export const getCheckAdditionalDataRule = intl => ({
  validator: validateAdditionalData,
  message: intl.formatMessage({
    id: 'settings.location.checkin.additionalData.input.error',
  }),
});

export const getAlreadyExistingDataRule = (intl, data) => ({
  validator: (_, value) => validateAlreadyExistingData(value, data),
  message: intl.formatMessage({
    id: 'notification.location.checkin.additionalData.keyAlreadyExists',
  }),
});

export const getCheckboxRule = message => ({
  validator: (_, value) =>
    value ? Promise.resolve() : Promise.reject(message),
});

export const getSelectRequiredRule = (intl, fieldName) => ({
  required: true,
  message: intl.formatMessage({ id: `error.${fieldName}` }),
});
