import validator from 'validator';

import {
  DEFAULT_LOCATION_NAME_DE,
  DEFAULT_LOCATION_NAME_EN,
} from 'constants/locations';

import { MAX_VAT, MIN_VAT } from 'constants/checkout';
import {
  isValidMobilePhoneNumber,
  isValidPhoneNumber,
  isValidCharacter,
  isValidTextCharacter,
  isHTTPSValidDomain,
} from 'utils/validators';

export const validateSafeString = (rule, value) => {
  if (!isValidCharacter(value?.trim())) {
    return Promise.reject();
  }
  return Promise.resolve();
};

export const validateDomain = (rule, value) => {
  if (!value) {
    return Promise.resolve();
  }
  if (!isHTTPSValidDomain(value?.trim())) {
    return Promise.reject();
  }
  return Promise.resolve();
};

export const validateVATNumber = (rule, value) => {
  if (!value) return Promise.resolve();

  const valueLength = value?.trim().length;
  if (valueLength < MIN_VAT || valueLength > MAX_VAT) {
    return Promise.reject();
  }
  return Promise.resolve();
};

export const validateHTTPSDomain = (rule, value) => {
  if (!value) {
    return Promise.resolve();
  }

  if (!isHTTPSValidDomain(value)) {
    return Promise.reject();
  }

  return Promise.resolve();
};

export const validateTextSafeString = (rule, value) => {
  if (!isValidTextCharacter(value?.split('\n').join(' ').trim())) {
    return Promise.reject();
  }
  return Promise.resolve();
};

export const validateNoNumeric = (rule, value) => {
  if (value?.trim() && validator.isNumeric(value.replace(/\s/g, ''))) {
    return Promise.reject();
  }
  return Promise.resolve();
};

export const validatePhoneNumber = (rule, value) => {
  if (value?.trim() && !isValidPhoneNumber(value)) {
    return Promise.reject();
  }
  return Promise.resolve();
};

export const validateMobilePhoneNumber = (rule, value) => {
  if (value?.trim() && !isValidMobilePhoneNumber(value)) {
    return Promise.reject();
  }
  return Promise.resolve();
};

export const validateDefaultName = (rule, value) => {
  if (
    value?.toLowerCase().trim() === DEFAULT_LOCATION_NAME_EN ||
    value?.toLowerCase().trim() === DEFAULT_LOCATION_NAME_DE
  ) {
    return Promise.reject();
  }
  return Promise.resolve();
};

export const checkExistingLocation = isLocationNameTaken => {
  return () => {
    if (isLocationNameTaken) {
      return Promise.reject();
    }
    return Promise.resolve();
  };
};

export const validateZipCode = (rule, value) => {
  if (value?.trim() && !validator.isPostalCode(value, 'DE')) {
    return Promise.reject();
  }
  return Promise.resolve();
};

export const validateEmail = (rule, value) => {
  if (
    value?.trim() &&
    !validator.isEmail(value, {
      allow_display_name: false,
      require_display_name: false,
      allow_utf8_local_part: true,
      require_tld: true,
      allow_ip_domain: false,
      blacklisted_chars: "=',\\\\",
    })
  ) {
    return Promise.reject();
  }
  return Promise.resolve();
};

export const validateAdditionalData = (rule, value) => {
  if (value.startsWith('_')) {
    return Promise.reject();
  }
  return Promise.resolve();
};

export const validateAlreadyExistingData = (value, data) => {
  const keyAlreadyExists = data.some(
    additionalInfo => additionalInfo.key === value
  );

  if (keyAlreadyExists) {
    return Promise.reject();
  }
  return Promise.resolve();
};
