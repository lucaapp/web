import { CAMPAIGNS } from 'constants/campaigns';

export const getTipTopUpAmount = (campaigns = [], tipAmount) => {
  const tipTopUpCampaign = campaigns?.find(
    payCampaign => payCampaign.campaign === CAMPAIGNS.PAY_RAFFLE_AND_TIPTOPUP
  );
  const { discountPercentage, discountMaxAmount } =
    tipTopUpCampaign?.paymentCampaign || {};

  const tipAmountPaidByLuca =
    (tipAmount * discountPercentage) / 100 > discountMaxAmount
      ? discountMaxAmount
      : (tipAmount * discountPercentage) / 100;
  const tipAmountPaidByGuest = tipAmount - tipAmountPaidByLuca;

  return { tipTopUpCampaign, tipAmountPaidByLuca, tipAmountPaidByGuest };
};
