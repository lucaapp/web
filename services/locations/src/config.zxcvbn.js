import { ZxcvbnOptions, zxcvbn } from '@zxcvbn-ts/core';
import zxcvbnCommonPackage from '@zxcvbn-ts/language-common';
import zxcvbnDePackage from '@zxcvbn-ts/language-de';
import zxcvbnEnPackage from '@zxcvbn-ts/language-en';

let options = {
  graphs: zxcvbnCommonPackage.adjacencyGraphs,
  dictionary: {
    ...zxcvbnCommonPackage.dictionary,
    ...zxcvbnDePackage.dictionary,
    ...zxcvbnEnPackage.dictionary,
  },
};

const zxcvbnWithLanguageOption = (password, userInputs, lang) => {
  options =
    lang === 'de'
      ? {
          ...options,
          translations: zxcvbnDePackage.translations,
        }
      : {
          ...options,
          translations: zxcvbnEnPackage.translations,
        };

  ZxcvbnOptions.setOptions(options);

  return zxcvbn(password, userInputs);
};

export { zxcvbnWithLanguageOption };
