const operator = {
  data: {
    firstName: 'Some',
    lastName: 'Dude',
    email: 'some@dud.es',
    supportCode: 'WY7GK9THNFLM',
  },
  error: false,
  isLoading: false,
};

jest.mock('store-context/selectors', () => ({
  useLanguage: () => ({
    currentLanguage: jest.fn(),
  }),
  useUser: () => operator,
}));

module.exports = { operator };
