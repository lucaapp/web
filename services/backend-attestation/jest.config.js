/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  rootDir: '.',
  roots: ['<rootDir>/src'],
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: ['<rootDir>/src/**/*.test.ts'],
  moduleNameMapper: {
    '^utils/(.*)$': '<rootDir>/src/utils/$1',
    '^constants/(.*)$': '<rootDir>/src/constants/$1',
    '^database/(.*)$': '<rootDir>/src/database/$1',
    '^database$': '<rootDir>/src/database/index',
    '^middlewares/(.*)$': '<rootDir>/src/middlewares/$1',
    '^passport/(.*)$': '<rootDir>/src/passport/$1',
    '^routes/(.*)$': '<rootDir>/src/routes/$1',
    '^testing/(.*)$': '<rootDir>/src/testing/$1',
  },
  collectCoverageFrom: [
    'src/**/*.{js,ts}',
    '!<rootDir>/node_modules',
    '!<rootDir>/src/index.js',
    '!<rootDir>/src/database/config.js',
    '!<rootDir>/src/database/migrations/',
    '!<rootDir>/src/database/seeds/',
    '!<rootDir>/**/*.test.ts',
  ],
  coverageReporters: ['lcov', 'text', 'cobertura'],
  coverageThreshold: {
    global: {
      branches: 25,
      functions: 16,
      lines: 32,
      statements: 33,
    },
    './src/utils/': {
      statements: 80,
      branches: 54,
      lines: 81,
      functions: 67,
    },
  },
  globals: { 'ts-jest': { tsconfig: 'tsconfig.test.json' } },
  reporters: [
    'default',
    [
      'jest-junit',
      {
        classNameTemplate: '{suitename}',
        titleTemplate: '{classname} {title}',
        addFileAttribute: 'true',
        filePathPrefix: 'services/backend-attestation',
      },
    ],
  ],
};
