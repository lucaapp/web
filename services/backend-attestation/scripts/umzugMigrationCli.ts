import { database } from 'database';
import { createUmzug } from './umzug';

export const umzug = createUmzug({
  directory: './src/database/migrations',
  tableName: '_Migrations',
});

(async () => {
  await umzug.runAsCLI();
  await database.close();
})();
