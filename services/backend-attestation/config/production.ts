import { PartialConfig } from './schema';

const config: PartialConfig = {
  debug: false,
  loglevel: 'info',
  allowRateLimitBypass: false,
  shutdownDelay: 15,
  jwt: {
    expiration: '10m',
  },
  jwk: {
    privateKey: null,
    publicKey: null,
  },
  allowAttestationVerificationsBypass: false,
};

// We cannot `export default` until this is fixed: https://github.com/node-config/node-config/issues/521
module.exports = config;
