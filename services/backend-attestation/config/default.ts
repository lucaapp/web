import { Config } from './schema';

const moment = require('moment');

const config: Config = {
  debug: true,
  loglevel: 'debug',
  allowRateLimitBypass: true,
  shutdownDelay: 0,
  port: 8080,
  healthCheckPort: 8040,
  version: {
    commit: 'dev',
    version: 'dev',
  },
  db: {
    host: 'database',
    host_read1: 'database',
    host_read2: 'database',
    host_read3: 'database',
    username: 'luca',
    password: 'lcadmin',
    database: 'luca-backend',
  },
  redis: {
    hostname: 'redis',
    database: 0,
    password:
      'ConqsCqWd]eaR82wv%C.iDdRybor8Ms2bM*h=m?V3@x2w^UxKA9pEjMjHn^y7?78', // DEV ONLY TOKEN
  },
  proxy: {
    http: null,
    https: null,
  },
  jwt: {
    expiration: '24h',
  },
  jwk: {
    privateKey: `-----BEGIN PRIVATE KEY-----
    MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgI7q6QaG05iPp1OHW
    +/2ddrplARnbz1CJpbLgrKX6152hRANCAAQcYXG/5yNjLbDypoYiwZqodXBeAwAX
    CBNLVhUDhO4yn74hGd1pMvZCZYeXzS8HU4/lFPD0jAysxzlaJ3GQWLvM
    -----END PRIVATE KEY-----`,
    publicKey: `-----BEGIN PUBLIC KEY-----
    MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEHGFxv+cjYy2w8qaGIsGaqHVwXgMA
    FwgTS1YVA4TuMp++IRndaTL2QmWHl80vB1OP5RTw9IwMrMc5WidxkFi7zA==
    -----END PUBLIC KEY-----`,
  },
  nonce: {
    maxAgeMs: moment.duration(1, 'minutes').asMilliseconds(),
  },
  allowAttestationVerificationsBypass: true,
  iosAttestation: {
    allowedAppIds: {
      consumerApp: '74643229Y4.de.culture4life.luca.debug',
      operatorApp: '',
    },
    allowedAaguidType: 'development', // "development" or "production"
  },
  androidAttestation: {
    allowedAPKPackageNames: {
      consumerApp: 'de.culture4life.luca.dev',
      operatorApp: '',
    },
    allowedAPKCertificateHashes: {
      // all hex (case-insensitive, seperating colons optional)
      consumerApp:
        '9C:1D:DE:02:07:A8:78:5A:06:B0:41:C8:41:30:D2:06:61:A3:50:76:39:DE:4E:FD:FC:9B:F9:7B:D6:95:3F:9B',
      operatorApp:
        '85:41:22:7B:6B:A0:C1:8D:02:17:DC:69:2D:A6:71:51:63:D4:7F:E9:22:D4:5F:F9:96:86:52:7F:8D:BA:89:E2,FA:C6:17:45:DC:09:03:78:6F:B9:ED:E6:2A:96:2B:39:9F:73:48:F0:BB:6F:89:9B:83:32:66:75:91:03:3B:9C',
    },
  },
};

// We cannot `export default` until this is fixed: https://github.com/node-config/node-config/issues/521
module.exports = config;
