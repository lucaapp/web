import { z } from 'utils/validation';
import type { infer as ZodInfer } from 'zod';

const pinoLogLevelSchema = z.union([
  z.literal('debug'),
  z.literal('error'),
  z.literal('info'),
  z.literal('trace'),
  z.literal('fatal'),
  z.literal('warn'),
]);

// Spent some time on this and am pretty sure now that this is a false positive
// eslint-disable-next-line security/detect-unsafe-regex
const FINGERPRINT_REG_EXP = /^(|([\dA-F]{2}(:[\dA-F]{2})*)(,[\dA-F]{2}(:[\dA-F]{2})*)*)$/;

const configSchema = z.object({
  debug: z.boolean(),
  loglevel: z.union([z.literal('silent'), pinoLogLevelSchema]),
  allowRateLimitBypass: z.boolean(),
  shutdownDelay: z.number().int().min(0),
  port: z.number().int().min(0).max(65536),
  healthCheckPort: z.number().int().min(0).max(65536),
  version: z.object({
    commit: z.string(),
    version: z.string(),
  }),
  db: z.object({
    host: z.string(),
    host_read1: z.string(),
    host_read2: z.string(),
    host_read3: z.string(),
    username: z.string(),
    password: z.string(),
    database: z.string(),
  }),
  redis: z.object({
    hostname: z.string(),
    database: z.number().int().min(0),
    password: z.string(),
  }),
  proxy: z.object({
    http: z.string().nullable(),
    https: z.string().nullable(),
  }),
  jwt: z.object({
    expiration: z.string(),
  }),
  jwk: z.object({
    privateKey: z.string().nullable(),
    publicKey: z.string().nullable(),
  }),
  nonce: z.object({
    maxAgeMs: z.number().int().min(0),
  }),
  allowAttestationVerificationsBypass: z.boolean(),
  iosAttestation: z.object({
    allowedAppIds: z.object({
      consumerApp: z.string(),
      operatorApp: z.string(),
    }),
    allowedAaguidType: z.union([
      z.literal('development'),
      z.literal('production'),
    ]),
  }),
  androidAttestation: z.object({
    allowedAPKPackageNames: z.object({
      consumerApp: z.string(),
      operatorApp: z.string(),
    }),
    allowedAPKCertificateHashes: z.object({
      consumerApp: z.string().regex(FINGERPRINT_REG_EXP),
      operatorApp: z.string().regex(FINGERPRINT_REG_EXP),
    }),
  }),
});

export default configSchema;

type DeepPartial<T> = T extends Record<string, unknown>
  ? {
      [P in keyof T]?: DeepPartial<T[P]>;
    }
  : T;
export type Config = ZodInfer<typeof configSchema>;
export type PartialConfig = DeepPartial<Config>;
