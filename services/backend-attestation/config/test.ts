import { PartialConfig } from './schema';

const config: PartialConfig = {
  loglevel: 'silent',
  iosAttestation: {
    allowedAppIds: {
      consumerApp: '74643229Y4.de.culture4life.luca.p2',
      operatorApp: '',
    },
    allowedAaguidType: 'development',
  },
  androidAttestation: {
    allowedAPKPackageNames: {
      consumerApp: 'de.culture4life.luca.dev',
      operatorApp: '',
    },
    allowedAPKCertificateHashes: {
      consumerApp:
        '9C:1D:DE:02:07:A8:78:5A:06:B0:41:C8:41:30:D2:06:61:A3:50:76:39:DE:4E:FD:FC:9B:F9:7B:D6:95:3F:9B',
      operatorApp:
        '85:41:22:7B:6B:A0:C1:8D:02:17:DC:69:2D:A6:71:51:63:D4:7F:E9:22:D4:5F:F9:96:86:52:7F:8D:BA:89:E2,FA:C6:17:45:DC:09:03:78:6F:B9:ED:E6:2A:96:2B:39:9F:73:48:F0:BB:6F:89:9B:83:32:66:75:91:03:3B:9C',
    },
  },
};

// We cannot `export default` until this is fixed: https://github.com/node-config/node-config/issues/521
module.exports = config;
