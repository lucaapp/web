import type { EnvironmentMapping } from 'config';
import type { Config } from './schema';

const config: EnvironmentMapping<Config> = {
  port: {
    __name: 'PORT',
    __format: 'number',
  },
  loglevel: 'LOGLEVEL',
  debug: {
    __name: 'DEBUG',
    __format: 'boolean',
  },
  allowRateLimitBypass: {
    __name: 'ALLOW_RATE_LIMIT_BYPASS',
    __format: 'boolean',
  },
  version: {
    commit: 'GIT_COMMIT',
    version: 'GIT_VERSION',
  },
  db: {
    host: 'DB_HOSTNAME',
    host_read1: 'DB_HOSTNAME_READ1',
    host_read2: 'DB_HOSTNAME_READ2',
    host_read3: 'DB_HOSTNAME_READ3',
    username: 'DB_USERNAME',
    password: 'DB_PASSWORD',
    database: 'DB_DATABASE',
  },
  redis: {
    hostname: 'REDIS_HOSTNAME',
    password: 'REDIS_PASSWORD',
    database: { __name: 'REDIS_DATABASE', __format: 'number' },
  },
  proxy: {
    http: 'http_proxy',
    https: 'http_proxy',
  },
  jwk: {
    privateKey: 'JWK_PRIVATE_KEY',
    publicKey: 'JWK_PUBLIC_KEY',
  },
  nonce: {
    maxAgeMs: {
      __name: 'NONCE_MAX_AGE',
      __format: 'number',
    },
  },
  allowAttestationVerificationsBypass: {
    __name: 'ALLOW_ATTESTATION_VERIFICATIONS_BYPASS',
    __format: 'boolean',
  },
  iosAttestation: {
    allowedAppIds: {
      consumerApp: 'IOS_ATTESTATION_APP_ID_CONSUMER_APP',
      operatorApp: 'IOS_ATTESTATION_APP_ID_OPERATOR_APP',
    },
    allowedAaguidType: 'IOS_ATTESTATION_ALLOWED_AAGUID_TYPE',
  },
  androidAttestation: {
    allowedAPKPackageNames: {
      consumerApp: 'ANDROID_ATTESTATION_ALLOWED_PKG_NAME_CONSUMER_APP',
      operatorApp: 'ANDROID_ATTESTATION_ALLOWED_PKG_NAME_OPERATOR_APP',
    },
    allowedAPKCertificateHashes: {
      consumerApp: 'ANDROID_ATTESTATION_ALLOWED_CERT_HASHES_CONSUMER_APP',
      operatorApp: 'ANDROID_ATTESTATION_ALLOWED_CERT_HASHES_OPERATOR_APP',
    },
  },
};

module.exports = config;
