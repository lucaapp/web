/* eslint-disable @typescript-eslint/no-explicit-any */

// isomorphic-webcrypto doesn't define the WebCrypto types itself but relies on the WebCrypto
// global being present. In our case, it isn't because we only include the Node.js types/
// Ideally, we would point to the browser's WebCrypto types here. Unfortunately, TypeScript
// doesn't allow us to obtain these types from any any lib (in this case: "dom") without pulling
// in all the globals. This would cause globals such as `window` to be available in our Node.js
// code which is error-prone.

interface WebCrypto extends Crypto {
  ensureSecure(): Promise<any>;
  getRandomValues: any;
  subtle: any;
}

declare let crypto: WebCrypto;

declare module 'isomorphic-webcrypto' {
  export = crypto;
  export default crypto;
}
