declare namespace Express {
  export interface User {
    deviceId: string;
    deviceOS: string;
  }
}
