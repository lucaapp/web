import type { RequestHandler, ErrorRequestHandler } from 'express';
import config from 'config';
import * as boom from '@hapi/boom';

export const handleNotFound: RequestHandler = () => {
  throw boom.notFound();
};

export const handleError: ErrorRequestHandler = (
  error,
  request,
  response,
  _next
) => {
  response.err = error;
  if (!boom.isBoom(error)) {
    boom.boomify(error);
  }
  const payload = { ...error.output.payload };
  if (error.data) {
    payload.data = error.data;
  }

  if (config.get('debug')) {
    payload.stack = error.stack;
  }

  return response.status(error.output.statusCode).send(payload);
};
