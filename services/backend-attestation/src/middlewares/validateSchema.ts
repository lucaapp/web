import express, { RequestHandler } from 'express';
import { ZodSchema, ZodError } from 'zod';
import { badRequest } from '@hapi/boom';
import { waitForMiddleware } from '../utils/middlewares';

const defaultJsonMiddleware = express.json();

type Literal = boolean | null | number | string;
type Json = Literal | { [key: string]: Json } | Json[];

export const validateSchema =
  // Generic type argument enables succeeding handlers to have `request.body` being the schema's
  // output type.
  <TZodSchemaOutput extends Json>(
    schema: ZodSchema<TZodSchemaOutput>,
    limit?: string
  ): RequestHandler<Record<string, string>, unknown, TZodSchemaOutput> => {
    const jsonMiddleware = limit
      ? express.json({ limit })
      : defaultJsonMiddleware;

    return async (request, response, next) => {
      await waitForMiddleware(jsonMiddleware, request, response);

      try {
        request.body = schema.parse(request.body);
        return next();
      } catch (error) {
        if (error instanceof ZodError) {
          throw badRequest('Request body validation failed.', error.issues);
        }
        throw error;
      }
    };
  };

export const validateQuerySchema = (
  schema: ZodSchema<Record<string, string | string[]>>
): RequestHandler => async (request, response, next) => {
  try {
    request.query = schema.parse(request.query);
    return next();
  } catch (error) {
    if (error instanceof ZodError) {
      throw badRequest('Query validation failed.', error.issues);
    }
    throw error;
  }
};

export const validateParametersSchema = (
  schema: ZodSchema<Record<string, string>>
): RequestHandler => async (request, response, next) => {
  try {
    request.params = schema.parse(request.params);
    return next();
  } catch (error) {
    if (error instanceof ZodError) {
      throw badRequest('Parameters validation failed.', error.issues);
    }
    throw error;
  }
};
