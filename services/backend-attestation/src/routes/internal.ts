import { Router } from 'express';
import passport from 'passport';
import { requireInternalIp } from 'middlewares/requireInternalIp';

import metricsRouter from './internal/metrics';
import jobsRouter from './internal/jobs';
import assertionRouter from './internal/assertion';
import androidKeyAttestationRouter from './internal/androidKeyAttestation';

const router = Router();

router.use(requireInternalIp);
router.use('/metrics', metricsRouter);
router.use(passport.authenticate('bearer-internalAccess', { session: false }));

router.use('/jobs', jobsRouter);
router.use('/assertion', assertionRouter);
router.use('/android-key-attestation', androidKeyAttestationRouter);

export default router;
