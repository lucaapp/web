import { Router } from 'express';
import swaggerRouter from './v1/swagger';
import nonceRouter from './v1/nonce';
import devicesRouter from './v1/devices';
import jwksRouter from './v1/jwks';

const router = Router();

router.use('/devices', devicesRouter);

router.use('/swagger', swaggerRouter);
router.use('/nonce', nonceRouter);
router.use('/devices', devicesRouter);
router.use('/jwks', jwksRouter);

export default router;
