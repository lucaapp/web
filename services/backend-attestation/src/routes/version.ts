import { Router } from 'express';
import config from 'config';

const router = Router();

router.get('/', (request, response) => {
  response.send(config.get('version'));
});

export default router;
