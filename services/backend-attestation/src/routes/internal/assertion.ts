import { boomify } from '@hapi/boom';
import status from 'http-status';
import { DeviceAttestationPlatform } from 'database/models/deviceAttestation';
import { Router } from 'express';
import { validateSchema } from 'middlewares/validateSchema';
import { verifyAndroidAssertionRequest } from 'routes/v1/devices/android';
import { AndroidAssertionError } from 'utils/attestation/android/errors';
import { IOSAssertionError } from 'utils/attestation/ios/errors';
import { z } from 'utils/validation';
import { verifyIOSAssertionRequest } from 'routes/v1/devices/ios';

const assertionSchema = z.union([
  z.object({
    platform: z.literal(DeviceAttestationPlatform.IOS),
    deviceId: z.uuid(),
    nonce: z.string(),
    clientDataBase64: z.base64(),
    assertion: z.string(),
  }),
  z.object({
    platform: z.literal(DeviceAttestationPlatform.ANDROID),
    deviceId: z.uuid(),
    nonce: z.string(),
    clientDataBase64: z.base64(),
    signature: z.ecSignature(),
  }),
]);

const router = Router();

router.post('/', validateSchema(assertionSchema), async (request, response) => {
  const { deviceId, platform, nonce, clientDataBase64 } = request.body;

  try {
    if (platform === DeviceAttestationPlatform.ANDROID) {
      const { signature } = request.body;
      await verifyAndroidAssertionRequest({
        requestHeaders: request.headers,
        deviceId,
        nonce,
        clientData: Buffer.from(clientDataBase64, 'base64'),
        signatureBase64: signature,
        logger: request.log,
      });
    } else {
      const { assertion } = request.body;
      await verifyIOSAssertionRequest({
        requestHeaders: request.headers,
        deviceId,
        nonce,
        clientData: Buffer.from(clientDataBase64, 'base64'),
        assertionBase64: assertion,
        logger: request.log,
      });
    }
  } catch (error) {
    if (
      error instanceof AndroidAssertionError ||
      error instanceof IOSAssertionError
    ) {
      throw boomify(error as Error, {
        statusCode: status.BAD_REQUEST,
        data: { result: 'FAILED' },
      });
    }

    throw error;
  }
  response.status(200).send({ result: 'PASSED' });
});

export default router;
