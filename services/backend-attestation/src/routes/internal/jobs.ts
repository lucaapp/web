import { Router } from 'express';
import { performance } from 'perf_hooks';
import { Nonce } from 'database';
import config from 'config';
import { Op } from 'sequelize';
import moment from 'moment';

const router = Router();

const NONCE_MAX_AGE = config.get('nonce.maxAgeMs');

router.post('/deleteExpiredNonce', async (_, response) => {
  const t0 = performance.now();
  const affectedRows = await Nonce.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(NONCE_MAX_AGE).toDate(),
      },
    },
  });
  const responseData = { affectedRows, time: performance.now() - t0 };
  response.send(responseData);
});

export default router;
