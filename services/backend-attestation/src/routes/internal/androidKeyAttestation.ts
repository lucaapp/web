import { boomify } from '@hapi/boom';
import status from 'http-status';
import config from 'config';
import { Router } from 'express';
import { validateSchema } from 'middlewares/validateSchema';
import { z } from 'utils/validation';
import {
  AndroidAttestationError,
  verifyAndroidKeyAttestation,
} from 'utils/attestation/android';
import { parsePKIJSCertificateFromPEM } from 'utils/certificates';

const androidKeyAttestationSchema = z.object({
  nonce: z.string(),
  publicKey: z.ecPublicKey().or(z.rsaPublicKey()),
  certificates: z.array(z.string()),
});

const router = Router();

router.post(
  '/',
  validateSchema(androidKeyAttestationSchema),
  async (request, response) => {
    const { nonce, publicKey, certificates } = request.body;

    const shouldBypassVerification =
      config.get('allowAttestationVerificationsBypass') &&
      request.headers['x-bypass-attestation'];

    if (!shouldBypassVerification) {
      const certificateTuples = certificates.map(certificate =>
        parsePKIJSCertificateFromPEM(certificate)
      );

      try {
        await verifyAndroidKeyAttestation(
          Buffer.from(publicKey, 'base64'),
          certificateTuples,
          Buffer.from(nonce, 'utf-8'),
          request.log
        );
      } catch (error) {
        if (error instanceof AndroidAttestationError) {
          throw boomify(error as Error, {
            statusCode: status.BAD_REQUEST,
            data: { result: 'FAILED' },
          });
        }

        throw error;
      }
    }

    response.status(200).send({ result: 'PASSED' });
  }
);

export default router;
