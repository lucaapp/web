import express from 'express';
import 'express-async-errors';
import superTestRequest from 'supertest';
import {
  AndroidAssertionError,
  AndroidAssertionErrorReason,
} from '../../utils/attestation/android/errors';
import {
  IOSAssertionError,
  IOSAssertionErrorReason,
} from '../../utils/attestation/ios/errors';
import assertionRouter from './assertion';
import { handleError } from '../../middlewares/error';

type VerifyAndroidAssertionRequest = typeof import('../v1/devices/android')['verifyAndroidAssertionRequest'];
type VerifyIOSAssertionRequest = typeof import('../v1/devices/ios')['verifyIOSAssertionRequest'];
let mockedVerifyAndroidAssertionRequest = jest.fn(
  (async _ => undefined) as VerifyAndroidAssertionRequest
);
let mockedVerifyIOSAssertionRequest = jest.fn(
  (async _ => undefined) as VerifyIOSAssertionRequest
);

jest.mock('routes/v1/devices/ios', (): typeof import('../v1/devices/ios') => {
  const actual = jest.requireActual('routes/v1/devices/ios');
  return {
    __esModule: true,
    ...actual,
    verifyIOSAssertionRequest: (...arguments_) =>
      mockedVerifyIOSAssertionRequest(...arguments_),
  };
});

jest.mock(
  'routes/v1/devices/android',
  (): typeof import('../v1/devices/android') => {
    const actual = jest.requireActual('routes/v1/devices/android');
    return {
      __esModule: true,
      ...actual,
      verifyAndroidAssertionRequest: (...arguments_) =>
        mockedVerifyAndroidAssertionRequest(...arguments_),
    };
  }
);

const getRequest = (): superTestRequest.SuperTest<superTestRequest.Test> =>
  superTestRequest(
    express().use(handleError).use(assertionRouter).use(handleError)
  );

const mockVerifyAndroidAssertionRequest = (
  mockedImplementation: VerifyAndroidAssertionRequest
) => {
  mockedVerifyAndroidAssertionRequest = jest.fn(mockedImplementation);
};

const mockVerifyIOSAssertionRequest = (
  mockedImplementation: VerifyIOSAssertionRequest
) => {
  mockedVerifyIOSAssertionRequest = jest.fn(mockedImplementation);
};

describe('POST /internal/assertion', () => {
  beforeEach(() => {
    mockedVerifyAndroidAssertionRequest = jest.fn(
      (async () => undefined) as VerifyAndroidAssertionRequest
    );
    mockedVerifyIOSAssertionRequest = jest.fn(
      (async () => undefined) as VerifyIOSAssertionRequest
    );
  });

  describe('when passing valid request body for Android and assertion passes', () => {
    it('responds 200 with result = PASSED', async () => {
      const response = await getRequest()
        .post('/')
        .send({
          platform: 'android',
          nonce: 'some_nonce',
          deviceId: '96326c0d-2a21-4582-a300-f5433056b6d7',
          signature: Buffer.from('some-signature').toString('base64'),
          clientDataBase64: '',
        });

      expect(response.status).toEqual(200);
      expect(response.body).toMatchObject({ result: 'PASSED' });
    });

    it('passes nonce + clientData as challenge to verifyAndroidAssertionRequest()', async () => {
      await getRequest()
        .post('/')
        .send({
          platform: 'android',
          nonce: 'some_nonce',
          deviceId: '96326c0d-2a21-4582-a300-f5433056b6d7',
          clientDataBase64: Buffer.from([0, 1, 2, 3]).toString('base64'),
          signature: Buffer.from('some-signature').toString('base64'),
        });

      expect(
        mockedVerifyAndroidAssertionRequest.mock.calls[0][0]
      ).toMatchObject({
        nonce: 'some_nonce',
        clientData: Buffer.from([0, 1, 2, 3]),
      });
    });
  });

  describe('when passing valid request body for iOS and assertion passes', () => {
    it('responds 200 with result = PASSED', async () => {
      const response = await getRequest()
        .post('/')
        .send({
          platform: 'ios',
          nonce: 'some_nonce',
          deviceId: '96326c0d-2a21-4582-a300-f5433056b6d7',
          assertion: Buffer.from('some-assertion-cbor').toString('base64'),
          clientDataBase64: '',
        });

      expect(response.status).toEqual(200);
      expect(response.body).toMatchObject({ result: 'PASSED' });
    });
  });

  describe('when passing valid request body for Android and assertion fails', () => {
    it('responds 400 with data.result = FAILED', async () => {
      mockVerifyAndroidAssertionRequest(async () => {
        throw new AndroidAssertionError(
          AndroidAssertionErrorReason.SIGNATURE_INVALID
        );
      });
      const response = await getRequest()
        .post('/')
        .send({
          platform: 'android',
          nonce: 'some_nonce',
          deviceId: '96326c0d-2a21-4582-a300-f5433056b6d7',
          signature: Buffer.from('some-signature').toString('base64'),
          clientDataBase64: '',
        });

      expect(response.status).toEqual(400);
      expect(response.body).toMatchObject({ data: { result: 'FAILED' } });
    });
  });

  describe('when passing valid request body for iOS and assertion fails', () => {
    it('responds 400 with data.result = FAILED', async () => {
      mockVerifyIOSAssertionRequest(async () => {
        throw new IOSAssertionError(IOSAssertionErrorReason.SIGNATURE_INVALID);
      });
      const response = await getRequest()
        .post('/')
        .send({
          platform: 'ios',
          nonce: 'some_nonce',
          deviceId: '96326c0d-2a21-4582-a300-f5433056b6d7',
          assertion: Buffer.from('some-assertion-cbor').toString('base64'),
          clientDataBase64: '',
        });

      expect(response.status).toEqual(400);
      expect(response.body).toMatchObject({ data: { result: 'FAILED' } });
    });
  });
});
