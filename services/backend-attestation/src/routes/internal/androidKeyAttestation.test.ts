import express from 'express';
import 'express-async-errors';
import superTestRequest from 'supertest';
import androidKeyAttestationROuter from './androidKeyAttestation';
import { handleError } from '../../middlewares/error';
import {
  AndroidAttestationError,
  AndroidAttestationErrorReason,
} from '../../utils/attestation/android';

type VerifyAndroidAssertionRequest = typeof import('../../utils/attestation/android')['verifyAndroidKeyAttestation'];
let mockedVerifyAndroidAssertionRequest: VerifyAndroidAssertionRequest = async _ =>
  undefined;

jest.mock(
  'utils/attestation/android',
  (): typeof import('../../utils/attestation/android') => {
    const actual = jest.requireActual('utils/attestation/android');
    return {
      __esModule: true,
      ...actual,
      verifyAndroidKeyAttestation: (...arguments_) =>
        mockedVerifyAndroidAssertionRequest(...arguments_),
    };
  }
);

const MOCK_PUBLIC_KEY_X963 = `MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEiMA0oZCqfbxaBhUBxlQoA5QlghmLPxzFRnPKO5rSC0FSgmelT1/boEafr7RrtpkKOWvwT5SknUMgyBx6skCjmA==`;
const MOCK_CERTIFICATE = `-----BEGIN CERTIFICATE-----
MIIF3DCCBMSgAwIBAgIQDK+Oqsay+j9ysFneQ1N7djANBgkqhkiG9w0BAQsFADBG
MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRUwEwYDVQQLEwxTZXJ2ZXIg
Q0EgMUIxDzANBgNVBAMTBkFtYXpvbjAeFw0yMTA4MzAwMDAwMDBaFw0yMjA5Mjgy
MzU5NTlaMB4xHDAaBgNVBAMTE3d3dy5zdWVkZGV1dHNjaGUuZGUwggEiMA0GCSqG
SIb3DQEBAQUAA4IBDwAwggEKAoIBAQCoksTAOTrZET3wu5TrADBzCJ3akGbIZsvH
P4lQ9TECGUO1ufjMXnoKMHXMXKG113/ZqsOUUnH0HZx0FDjBqHGEbDPkNsqAbdrW
M4h0ksKZ3FxW4m8se22XMwMrnFJCV0hlc9/CO36KHv2S2pRxyYcA1jZeb10tpGdO
x4BEdgY1rtu+qwEJLQdLDVnzGQbnOeW11P0uyPFgdv0A2bEUTGKex+j0+xwDZWH4
hy509Gx6u+txfLSzwhTdaU41nQCbTYgKOR5jNzkX+bjZ8K7GyMtxwYFpLO64oNiB
PQ6k0+UrPZTlCfqjKbws0CXE7+6kIOTG7AOHH8PFCJNnswHS1ZNtAgMBAAGjggLs
MIIC6DAfBgNVHSMEGDAWgBRZpGYGUqB7lZI8o5QHJ5Z0W/k90DAdBgNVHQ4EFgQU
W5rznzmOOMzfc0SxDrbTLQU0MTYwHgYDVR0RBBcwFYITd3d3LnN1ZWRkZXV0c2No
ZS5kZTAOBgNVHQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUF
BwMCMDsGA1UdHwQ0MDIwMKAuoCyGKmh0dHA6Ly9jcmwuc2NhMWIuYW1hem9udHJ1
c3QuY29tL3NjYTFiLmNybDATBgNVHSAEDDAKMAgGBmeBDAECATB1BggrBgEFBQcB
AQRpMGcwLQYIKwYBBQUHMAGGIWh0dHA6Ly9vY3NwLnNjYTFiLmFtYXpvbnRydXN0
LmNvbTA2BggrBgEFBQcwAoYqaHR0cDovL2NydC5zY2ExYi5hbWF6b250cnVzdC5j
b20vc2NhMWIuY3J0MAwGA1UdEwEB/wQCMAAwggF+BgorBgEEAdZ5AgQCBIIBbgSC
AWoBaAB2ACl5vvCeOTkh8FZzn2Old+W+V32cYAr4+U1dJlwlXceEAAABe5UI4ycA
AAQDAEcwRQIhAJkxBkppaiCb37xdGWi2FYO/sAeDdI/bvvNmcn+ZFuLkAiA2kqDQ
TMJzTRzshPVkYprvkurrcFSj8KEK6kCilryZMwB3AFGjsPX9AXmcVm24N3iPDKR6
zBsny/eeiEKaDf7UiwXlAAABe5UI44AAAAQDAEgwRgIhALrORnzWb0Z2XC1xMHCA
bfla4R2R7x+Hv3VYiBoH15t7AiEAkx5blBCLNtks/+vs8VW000pfbxXKQJ2Eysuc
yteOgLcAdQBByMqx3yJGShDGoToJQodeTjGLGwPr60vHaPCQYpYG9gAAAXuVCOMM
AAAEAwBGMEQCIAWO6FB1Enab8EwEItMV1ki0K1xPTAAq9vXXPlJbFl8nAiBKR57S
UNDWph0wQLPFNOGKXWDubfKbI4bmz7EZzU2QATANBgkqhkiG9w0BAQsFAAOCAQEA
XjCiyyi6dFJ140xTtF1Ptm7lG5A3jdNEQmyOCeR8yeOD32g9cICRjXLYLqpVw3Ih
Q+k4hfqoYqqoM6ep98d1Ai83/Y6My0uLk0cHk1ginNpBJpM8beUCKV6K4dLyCqML
z/v2UmAyZWJUXLZcDeeiatA7J7/BOqRmgh3vZvjL1qfGEiG7gn46x/ZXb1dLC2Y1
qQYzKJ89jHWLnYHsW/JNAhKa1BHPTVApI+cgtwuaLDKIEGOBdBEmGSlAzoEsfv/Z
cr5Q8JB23nyqCgIX8J1b6uQ1k23uhh5/Hr6wNJuDV+vePR6oiUSgfCzENdFkfrtr
jvIpvxqZj69IbyM200u/SQ==
-----END CERTIFICATE-----
`;

const getRequest = (): superTestRequest.SuperTest<superTestRequest.Test> =>
  superTestRequest(
    express().use(handleError).use(androidKeyAttestationROuter).use(handleError)
  );

describe('POST /internal/android-key-attestation', () => {
  beforeEach(() => {
    mockedVerifyAndroidAssertionRequest = async () => undefined;
  });

  describe('when passing valid request body for Android and attestation passes', () => {
    it('responds 200 with result = PASSED', async () => {
      const response = await getRequest()
        .post('/')
        .send({
          nonce: 'some_nonce',
          publicKey: MOCK_PUBLIC_KEY_X963,
          certificates: [MOCK_CERTIFICATE],
        });

      expect(response.status).toEqual(200);
      expect(response.body).toMatchObject({ result: 'PASSED' });
    });
  });

  describe('when passing valid request body for Android and attestation fails', () => {
    it('responds 400 with data.result = FAILED', async () => {
      mockedVerifyAndroidAssertionRequest = async () => {
        throw new AndroidAttestationError(
          AndroidAttestationErrorReason.KEY_ATTESTATION_INSUFFICIENT_SECURITY_LEVEL
        );
      };
      const response = await getRequest()
        .post('/')
        .send({
          nonce: 'some_nonce',
          publicKey: MOCK_PUBLIC_KEY_X963,
          certificates: [MOCK_CERTIFICATE],
        });

      expect(response.status).toEqual(400);
      expect(response.body).toMatchObject({ data: { result: 'FAILED' } });
    });
  });
});
