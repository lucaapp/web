/* eslint-disable unicorn/consistent-function-scoping */
/* eslint-disable @typescript-eslint/no-explicit-any */
import express from 'express';
import request from 'supertest';
import moment from 'moment';
import 'express-async-errors';
import { Nonce } from 'database';
import config from 'config';
import { setupDBRedisAndApp } from 'testing/utils';
import jobsRouter from './jobs';

const app = express();
app.use('', jobsRouter);

const NONCE_MAX_AGE_DOUBLE = config.get('nonce.maxAgeMs') * 2;

const createNonce = async (nonce: string) => Nonce.create({ nonce });

const createExpiredNonce = async (nonce: string) => {
  const savedNonce = await Nonce.create({
    nonce,
  });
  savedNonce.createdAt = moment(savedNonce.createdAt)
    .subtract(NONCE_MAX_AGE_DOUBLE)
    .toDate();
  return savedNonce?.save();
};

describe('Interal jobs router', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
  });
  afterEach(() => {
    Nonce.truncate();
  });

  describe('Nonce', () => {
    it('should cleanup expired nonce', async () => {
      const expiredNonce = 'EXP+ZFyHGaJN/z+gH+f+JgnzNEi+7LIUEtO1YMkuomma4X0=';
      await createExpiredNonce(expiredNonce);
      const expiredNonceInDatabase = await Nonce.findOne({
        where: { nonce: expiredNonce },
      });
      expect(expiredNonceInDatabase).not.toBeNull();

      await request(app)
        .post('/deleteExpiredNonce')
        .expect('Content-Type', /json/)
        .expect(200);

      const expiredNonceNotInDatabase = await Nonce.findOne({
        where: { nonce: expiredNonce },
      });
      expect(expiredNonceNotInDatabase).toBeNull();
    });
    it('should not cleanup non expired nonce', async () => {
      const validNonce = 'ZFyHGaJN/z+gH+f+JgnzNEi+7LIUEtO1YMkuomma4X0=';
      await createNonce(validNonce);
      let validNonceInDatabase = await Nonce.findOne({
        where: { nonce: validNonce },
      });
      expect(validNonceInDatabase).not.toBeNull();

      await request(app)
        .post('/deleteExpiredNonce')
        .expect('Content-Type', /json/)
        .expect(200);

      validNonceInDatabase = await Nonce.findOne({
        where: { nonce: validNonce },
      });
      expect(validNonceInDatabase).not.toBeNull();
    });
  });
});
