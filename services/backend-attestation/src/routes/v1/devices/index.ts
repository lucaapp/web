import { Router } from 'express';
import iosRouter from './ios';
import androidRouter from './android';

const devicesRouter = Router();

devicesRouter.use('/ios', iosRouter);
devicesRouter.use('/android', androidRouter);

export default devicesRouter;
