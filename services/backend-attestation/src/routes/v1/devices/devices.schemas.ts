import { z } from 'utils/validation';

export const registerIOSSchema = z.object({
  nonce: z.string(),
  attestation: z.string(),
  keyIdentifier: z.string(),
});

export const registerAndroidSchema = z.object({
  nonce: z.string(),
  keyAttestationPublicKey: z.ecPublicKey().or(z.rsaPublicKey()),
  keyAttestationCertificates: z.array(z.string()),
  safetyNetAttestationJws: z.compactJWS(),
});

export const assertIOSSchema = z.object({
  deviceId: z.uuid(),
  nonce: z.string(),
  assertion: z.string(),
});

export const assertAndroidSchema = z.object({
  deviceId: z.uuid(),
  nonce: z.string(),
  signature: z.ecSignature(),
});
