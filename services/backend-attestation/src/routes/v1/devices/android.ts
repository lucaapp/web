import config from 'config';
import moment from 'moment';
import status from 'http-status';
import { Router } from 'express';
import { validateSchema } from 'middlewares/validateSchema';
import { rateLimitRoute } from 'middlewares/rateLimit';
import { badRequest, boomify, notFound } from '@hapi/boom';

import { DeviceAttestation } from 'database';
import { DeviceAttestationPlatform } from 'database/models/deviceAttestation';
import { verifyAndroidAttestation } from 'utils/attestation/android';
import {
  convertDERBase64CertificateToPEM,
  parsePKIJSCertificateFromBER,
} from 'utils/certificates';
import { verifyAndroidAssertion } from 'utils/attestation/android/assertion';
import { checkAndInvalidateNonce } from 'utils/nonceUtils';
import { IncomingHttpHeaders } from 'http';
import { Logger } from 'pino';
import { generateDeviceAssertionJWT } from 'utils/attestation';
import { registerAndroidSchema, assertAndroidSchema } from './devices.schemas';

const router = Router();

router.post(
  '/register',
  rateLimitRoute({
    points: 5000,
    duration: moment.duration(1, 'hour').asSeconds(),
    blockDuration: moment.duration(1, 'day').asSeconds(),
  }),
  validateSchema(registerAndroidSchema),
  async (request, response) => {
    request.log.debug(request.body, 'req body');

    const publicKeyBuffer = Buffer.from(
      request.body.keyAttestationPublicKey,
      'base64'
    );

    try {
      await verifyAndroidAttestation(
        publicKeyBuffer,
        request.body.keyAttestationCertificates.map(certificate =>
          parsePKIJSCertificateFromBER(Buffer.from(certificate, 'base64'))
        ),
        request.body.nonce,
        request.body.safetyNetAttestationJws,
        checkAndInvalidateNonce,
        request.log.child({ attestationId: Date.now() })
      );
    } catch (error) {
      throw boomify(error as Error, { statusCode: status.BAD_REQUEST });
    }

    const deviceAttestation = await DeviceAttestation.create({
      publicKey: publicKeyBuffer,
      platform: DeviceAttestationPlatform.ANDROID,
      androidKeyAttestationCertificateChainPEM: request.body.keyAttestationCertificates
        .map(certificateDERBase64 =>
          convertDERBase64CertificateToPEM(certificateDERBase64)
        )
        .join('\n\n'),
      androidSafetyNetJWS: request.body.safetyNetAttestationJws,
    });

    response.send({
      deviceId: deviceAttestation.deviceId,
    });
  }
);

export const verifyAndroidAssertionRequest = async ({
  requestHeaders,
  deviceId,
  nonce,
  clientData,
  signatureBase64,
  logger,
}: {
  requestHeaders: IncomingHttpHeaders;
  deviceId: string;
  nonce: string;
  clientData: Buffer;
  signatureBase64: string;
  logger: Logger;
}): Promise<void> => {
  const shouldBypassVerification =
    config.get('allowAttestationVerificationsBypass') &&
    requestHeaders['x-bypass-attestation'];

  if (shouldBypassVerification) {
    return;
  }

  const deviceAttestation = await DeviceAttestation.findOne({
    where: { deviceId, platform: DeviceAttestationPlatform.ANDROID },
  });

  if (!deviceAttestation) {
    throw notFound(`Did not find existing device attestation for ${deviceId}`);
  }

  try {
    await verifyAndroidAssertion({
      publicKey: deviceAttestation.publicKey,
      nonce,
      signature: Buffer.from(signatureBase64, 'base64'),
      ensureValidNonceAndInvalidate: checkAndInvalidateNonce,
      logger,
      clientData,
    });
  } catch (error) {
    if (error instanceof Error) {
      throw boomify(error, {
        statusCode: status.BAD_REQUEST,
      });
    }
    throw badRequest('Unknown error', { error });
  }
};

router.post(
  '/assert',
  rateLimitRoute({
    points: 5000,
    duration: moment.duration(1, 'hour').asSeconds(),
    blockDuration: moment.duration(1, 'day').asSeconds(),
  }),
  validateSchema(assertAndroidSchema),
  async (request, response) => {
    request.log.debug(request.body, 'req body android');
    const { deviceId, nonce, signature } = request.body;

    await verifyAndroidAssertionRequest({
      requestHeaders: request.headers,
      deviceId,
      signatureBase64: signature,
      nonce,
      clientData: Buffer.alloc(0),
      logger: request.log,
    });

    const jwt = await generateDeviceAssertionJWT({
      platform: DeviceAttestationPlatform.ANDROID,
      deviceId,
    });

    response.send({
      jwt,
    });
  }
);

export default router;
