import config from 'config';
import moment from 'moment';
import status from 'http-status';
import { Router } from 'express';
import { validateSchema } from 'middlewares/validateSchema';
import { rateLimitRoute } from 'middlewares/rateLimit';
import {
  parseIOSAttestation,
  verifyIOSAttestation,
  deconstructAuthData,
} from 'utils/attestation/ios';
import { boomify, notFound } from '@hapi/boom';

import { DeviceAttestation } from 'database';
import { DeviceAttestationPlatform } from 'database/models/deviceAttestation';
import { checkAndInvalidateNonce } from 'utils/nonceUtils';
import {
  parseIOSAssertion,
  verifyIOSAssertion,
} from 'utils/attestation/ios/assertion';
import { IncomingHttpHeaders } from 'http';
import { Logger } from 'pino';
import { generateDeviceAssertionJWT } from 'utils/attestation';
import { registerIOSSchema, assertIOSSchema } from './devices.schemas';

const router = Router();

router.post(
  '/register',
  rateLimitRoute({
    points: 5000,
    duration: moment.duration(1, 'hour').asSeconds(),
    blockDuration: moment.duration(1, 'day').asSeconds(),
  }),
  validateSchema(registerIOSSchema),
  async (request, response) => {
    request.log.debug(request.body, 'req body');
    const attestation = Buffer.from(request.body.attestation, 'base64');
    const keyIdentifier = Buffer.from(request.body.keyIdentifier, 'base64');
    try {
      const parsedAttestation = await parseIOSAttestation(attestation);
      const authData = await deconstructAuthData(parsedAttestation.authData);
      const { spkiPublicKey, receipt } = await verifyIOSAttestation(
        parsedAttestation,
        authData,
        request.body.nonce,
        keyIdentifier,
        checkAndInvalidateNonce,
        request.log
      );

      const deviceAttestation = await DeviceAttestation.create({
        publicKey: spkiPublicKey,
        platform: DeviceAttestationPlatform.IOS,
        iosReceipt: receipt,
        iosSignCount: 0,
        iosAttestationCBOR: attestation,
      });

      request.log.debug(
        { spkiPublicKey, receipt },
        'attestation verification result'
      );
      response.send({
        deviceId: deviceAttestation.deviceId,
      });
    } catch (error) {
      throw boomify(error as Error, { statusCode: status.FORBIDDEN });
    }
  }
);

export const verifyIOSAssertionRequest = async ({
  deviceId,
  nonce,
  clientData,
  assertionBase64,
  requestHeaders,
  logger,
}: {
  deviceId: string;
  nonce: string;
  clientData: Buffer;
  assertionBase64: string;
  requestHeaders: IncomingHttpHeaders;
  logger: Logger;
}): Promise<void> => {
  const deviceAttestation = await DeviceAttestation.findOne({
    where: { deviceId, platform: DeviceAttestationPlatform.IOS },
  });

  if (!deviceAttestation) {
    throw notFound(`Did not find existing device attestation for ${deviceId}`);
  }

  const shouldBypassVerification =
    config.get('allowAttestationVerificationsBypass') &&
    requestHeaders['x-bypass-attestation'];

  if (shouldBypassVerification) {
    return;
  }

  try {
    const parsedIOSAssertion = await parseIOSAssertion(
      Buffer.from(assertionBase64, 'base64')
    );
    const { newCounter } = await verifyIOSAssertion(
      parsedIOSAssertion,
      deviceAttestation.publicKey,
      nonce,
      clientData,
      deviceAttestation.iosSignCount ?? null,
      checkAndInvalidateNonce,
      logger
    );
    await deviceAttestation.update({ iosSignCount: newCounter });
  } catch (error) {
    throw boomify(error as Error, { statusCode: status.BAD_REQUEST });
  }
};

router.post(
  '/assert',
  rateLimitRoute({
    points: 5000,
    duration: moment.duration(1, 'hour').asSeconds(),
    blockDuration: moment.duration(1, 'day').asSeconds(),
  }),
  validateSchema(assertIOSSchema),
  async (request, response) => {
    request.log.debug(request.body, 'req body ios');
    const { assertion, deviceId, nonce } = request.body;

    await verifyIOSAssertionRequest({
      deviceId,
      assertionBase64: assertion,
      nonce,
      clientData: Buffer.alloc(0),
      requestHeaders: request.headers,
      logger: request.log,
    });

    const jwt = await generateDeviceAssertionJWT({
      platform: DeviceAttestationPlatform.IOS,
      deviceId,
    });

    response.send({ jwt });
  }
);
export default router;
