import { Router } from 'express';

import * as jwkStore from 'utils/jwkStore';

const router = Router();

router.get('/', async (request, response) => {
  const jwks = await jwkStore.toJSON();

  request.log.info(
    {
      keyIdentifiers: jwks.keys.map(({ kid }) => `'${kid}'`),
    },
    `Sending JWKS`
  );
  response.send(jwks);
});

export default router;
