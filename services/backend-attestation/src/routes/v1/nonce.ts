import moment from 'moment';
import { Router } from 'express';
import { rateLimitRoute } from 'middlewares/rateLimit';
import { createAndGetNewNonce } from '../../utils/nonceUtils';

const router = Router();

router.get(
  '/request',
  rateLimitRoute({
    points: 10000,
    duration: moment.duration(1, 'hour').asSeconds(),
    blockDuration: moment.duration(1, 'day').asSeconds(),
  }),
  async (request, response) => {
    const nonce = await createAndGetNewNonce();

    response.send({
      nonce,
    });
  }
);

export default router;
