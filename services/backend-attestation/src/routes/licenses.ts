import fs from 'fs';
import { Router } from 'express';

const router = Router();

let licenses: string;
try {
  licenses = fs.readFileSync('./licenses.txt').toString();
} catch {
  licenses = 'Licenses not found.';
}

router.get('/', (request, response) => {
  response.type('text/plain');
  response.send(licenses);
});

export default router;
