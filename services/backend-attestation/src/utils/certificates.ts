import { fromBER, LocalBaseBlock, PrintableString } from 'asn1js';
import AttributeTypeAndValue from 'pkijs/build/AttributeTypeAndValue';
import Certificate from 'pkijs/build/Certificate';
import CertificateChainValidationEngine from 'pkijs/build/CertificateChainValidationEngine';
import { rootCertificates } from 'tls';
import { typedArrayToBuffer } from './bigEndian';
import './initPkijs';

export const OID_COMMON_NAME = '2.5.4.3';

// eslint-disable-next-line unicorn/better-regex
const PEM_HEADER_FOOTER_REG_EXP = /^-----(BEGIN|END) (CERTIFICATE|PUBLIC KEY)-----$/gm;

/**
 * node-forge offers certificate chain verification but the entire package is incompatible with EC
 * keys.
 *
 * @param certificates
 * @returns
 */
export const verifyCertificateChain = async ({
  leafCertificate,
  intermediateCertificates,
  trustedCertificates,
}: {
  leafCertificate: Certificate;
  intermediateCertificates: Certificate[];
  trustedCertificates: Certificate[];
}): Promise<void> => {
  const certificateVerificationEngine = new CertificateChainValidationEngine({
    certs: [...intermediateCertificates, leafCertificate],
    trustedCerts: trustedCertificates,
  });
  const {
    result,
    resultMessage,
  } = await certificateVerificationEngine.verify();

  if (!result) {
    throw new Error(`Certificate chain validation failed: ${resultMessage}`);
  }
};

export const extractBERFromPEM = (pem: string): Buffer => {
  return Buffer.from(pem.replace(PEM_HEADER_FOOTER_REG_EXP, ''), 'base64');
};

export type PKIJSCertificateTuple = {
  certificate: Certificate;
  asn: LocalBaseBlock;
};
export const parsePKIJSCertificateFromBER = (
  berBuffer: Buffer
): PKIJSCertificateTuple => {
  const asn = fromBER(typedArrayToBuffer(berBuffer));

  return {
    certificate: new Certificate({
      schema: asn.result,
    }),
    asn: asn.result,
  };
};

export const parsePKIJSCertificateFromPEM = (
  pem: string
): PKIJSCertificateTuple => {
  return parsePKIJSCertificateFromBER(extractBERFromPEM(pem));
};

export const findAttributeValue = (
  typesAndValues: AttributeTypeAndValue[],
  oid: string
): Buffer | null => {
  for (const typeAndValue of typesAndValues) {
    if (
      // @ts-ignore Type def is just wrong here. The ObjectIdentifier is actually jsut a string here
      (typeAndValue.type as string) === oid
    ) {
      if (typeAndValue.value instanceof PrintableString) {
        return Buffer.from(typeAndValue.value.valueBlock.valueHex);
      }
      throw new Error(
        `Expected LocalSimpleLocalSimpleStringValueBlock as value of attribute ${oid}`
      );
    }
  }
  return null;
};

export const filterRevokedCertificates = (
  certificates: Certificate[],
  revokedCertificateSerialNumbers: Buffer[]
): Certificate[] => {
  return certificates.filter(certificate => {
    const currentCertificateSerialNumber = Buffer.from(
      certificate.serialNumber.valueBlock.valueHex
    );
    return revokedCertificateSerialNumbers.some(revokedSerialNumber =>
      revokedSerialNumber.equals(currentCertificateSerialNumber)
    );
  });
};

let systemTrustedRootCertificates: Certificate[] | null = null;

export const getSystemTrustedRootCertificates = (): Certificate[] => {
  if (systemTrustedRootCertificates) {
    return systemTrustedRootCertificates;
  }

  systemTrustedRootCertificates = rootCertificates.map(
    rootCertificatePEM =>
      parsePKIJSCertificateFromPEM(rootCertificatePEM).certificate
  );

  return systemTrustedRootCertificates;
};

export const convertDERBase64CertificateToPEM = (derBase64: string): string => {
  return `-----BEGIN CERTIFICATE-----
${derBase64}
-----END CERTIFICATE-----`;
};
