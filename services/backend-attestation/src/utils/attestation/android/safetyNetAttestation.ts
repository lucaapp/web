import { z } from 'zod';
import {
  compactVerify,
  CompactVerifyResult,
  decodeProtectedHeader,
} from 'jose';
import { createPublicKey } from 'crypto';
import logger from 'utils/logger';
import {
  findAttributeValue,
  getSystemTrustedRootCertificates,
  OID_COMMON_NAME,
  parsePKIJSCertificateFromBER,
  verifyCertificateChain,
} from '../../certificates';
import {
  allowedAPKCertificateHashes,
  getAllowedAPKPackageNames,
  SafetyNetEvaluationType,
  SAFETY_NET_EXPECTED_LEAF_CERTIFICATE_HOST_NAME,
  SAFETY_NET_JWS_MAX_AGE_MS,
} from './config';

import {
  AndroidAttestationError,
  AndroidAttestationErrorReason,
} from './errors';

export const safetyNetJwsHeaderSchema = z.object({
  alg: z.union([
    z.literal('RS256'),
    z.literal('RS384'),
    z.literal('RS512'),
    z.literal('PS256'),
    z.literal('PS384'),
    z.literal('PS512'),
    z.literal('ES256'),
    z.literal('ES384'),
    z.literal('ES512'),
  ]),
  x5c: z.array(z.string()),
});

export type TSafetyNetJWSMessageHeader = ReturnType<
  typeof safetyNetJwsHeaderSchema['parse']
>;

export const safetyNetJwsMessagePayloadSchema = z.object({
  nonce: z.string(),
  timestampMs: z.number(),
  apkPackageName: z.string(),
  apkDigestSha256: z.string(),
  ctsProfileMatch: z.boolean(),
  apkCertificateDigestSha256: z.array(z.string()),
  basicIntegrity: z.boolean(),
  evaluationType: z.string(),
});

export type TSafetyNetJWSMessagePayload = ReturnType<
  typeof safetyNetJwsMessagePayloadSchema['parse']
>;

const verifySafetyNetJwsPayloadFields = async (
  payload: ReturnType<typeof safetyNetJwsMessagePayloadSchema['parse']>,
  expectedSafetyNetNonce: Buffer
): Promise<void> => {
  // Verify JWS age

  const jwsAge = Date.now() - payload.timestampMs;

  if (jwsAge >= SAFETY_NET_JWS_MAX_AGE_MS || jwsAge < 0) {
    const error = new AndroidAttestationError(
      AndroidAttestationErrorReason.SAFETY_NET_JWS_AGE_INVALID
    );
    logger.error(
      error,
      `JWS date is ${new Date(payload.timestampMs).toString()}`
    );
    throw error;
  }

  // Verify nonce

  const parsedNonceFromJWSPayload = Buffer.from(payload.nonce, 'base64');
  if (!parsedNonceFromJWSPayload.equals(expectedSafetyNetNonce)) {
    const error = new AndroidAttestationError(
      AndroidAttestationErrorReason.SAFETY_NET_NONCE_NOT_MATCHING
    );
    logger.error(error, `Nonce was payload.nonce`);
    throw error;
  }

  // Verify package name
  const allowedAPKPackageNames = getAllowedAPKPackageNames();

  if (
    payload.apkPackageName !== allowedAPKPackageNames.consumerApp &&
    payload.apkPackageName !== allowedAPKPackageNames.operatorApp
  ) {
    const error = new AndroidAttestationError(
      AndroidAttestationErrorReason.SAFETY_NET_PACKAGE_NAME_NOT_ALLOWED
    );

    logger.error(
      error,
      `JWS apkPackageName was ${payload.apkPackageName} but only ${allowedAPKPackageNames.consumerApp} or ${allowedAPKPackageNames.operatorApp} are allowed`
    );
    throw error;
  }

  // Verify signing certififcate hash

  if (payload.apkCertificateDigestSha256.length === 0) {
    throw new AndroidAttestationError(
      AndroidAttestationErrorReason.SAFETY_NET_NO_APK_CERTIFICATE_HASHES_PRESENT
    );
  }

  // apkCertificateDigestSha256 might contain multiple certificate hashes if multiple signing
  // certificates were used. This could be the case for publishing to the official Google Play store
  // because there is an upload certificate and a Google Play Sign certificate. We don't really
  // care as long as one of our known certificates signed the app code.
  const isAnyAPKCertificateHasheMatching = payload.apkCertificateDigestSha256.some(
    hashBase64 => {
      const expectedHashBuffer = Buffer.from(hashBase64, 'base64');

      return allowedAPKCertificateHashes.some(allowedAPKCertificateHash =>
        expectedHashBuffer.equals(allowedAPKCertificateHash)
      );
    }
  );
  if (!isAnyAPKCertificateHasheMatching) {
    const error = new AndroidAttestationError(
      AndroidAttestationErrorReason.SAFETY_NET_APK_CERTIFICATE_HASHES_NOT_ALLOWED
    );
    logger.error(
      error,
      `Allowed certificate hashes were ${allowedAPKCertificateHashes
        .map(hash => hash.toString('hex'))
        .join(
          ', '
        )} but received hashes ${payload.apkCertificateDigestSha256.join(', ')}`
    );
    throw error;
  }

  // Verify ctsProfileMatch

  if (!payload.ctsProfileMatch) {
    throw new AndroidAttestationError(
      AndroidAttestationErrorReason.SAFETY_NET_CTS_PROFILE_MATCH_FALSE
    );
  }

  // Verify basicIntegrity

  if (!payload.basicIntegrity) {
    throw new AndroidAttestationError(
      AndroidAttestationErrorReason.SAFETY_NET_BASIC_INTEGRITY_FALSE
    );
  }

  // Verify evaluationType
  const evaluationTypes = payload.evaluationType.split(',');
  if (!evaluationTypes.includes(SafetyNetEvaluationType.HARDWARE_BACKED)) {
    const error = new AndroidAttestationError(
      AndroidAttestationErrorReason.SAFETY_NET_EVALUATION_TYPE_INSUFFICIENT
    );
    logger.error(error, `Evaluation type was ${payload.evaluationType}`);
    throw error;
  }
};

const verifSafetyNetJWSSignature = async (
  encodedJWS: string,
  decodedJwsHeader: TSafetyNetJWSMessageHeader
): Promise<CompactVerifyResult> => {
  try {
    return await compactVerify(
      encodedJWS,
      createPublicKey({
        key: `-----BEGIN CERTIFICATE-----\n${decodedJwsHeader.x5c[0]}\n-----END CERTIFICATE-----`,
        format: 'pem',
        type: 'spki',
      })
    );
  } catch (joseError) {
    const mappedError = new AndroidAttestationError(
      AndroidAttestationErrorReason.SAFETY_NET_JWS_SIGNATURE_INVALID
    );
    logger.error(mappedError);
    if (joseError instanceof Error) {
      logger.error(joseError);
    }
    throw mappedError;
  }
};

export const verifySafetyNetAttestation = async (
  safetyNetAttestationJws: string,
  safetyNetNonce: Buffer
): Promise<void> => {
  const decodedJwsHeader = safetyNetJwsHeaderSchema.parse(
    decodeProtectedHeader(safetyNetAttestationJws)
  );

  // Verify certificate chain

  const [
    leafCertificate,
    ...intermediateCertificates
  ] = decodedJwsHeader.x5c.map(
    jwsCertificateBase64 =>
      parsePKIJSCertificateFromBER(Buffer.from(jwsCertificateBase64, 'base64'))
        .certificate
  );

  try {
    await verifyCertificateChain({
      leafCertificate,
      intermediateCertificates,
      trustedCertificates: getSystemTrustedRootCertificates(),
    });
  } catch (pkijsError) {
    const mappedError = new AndroidAttestationError(
      AndroidAttestationErrorReason.SAFETY_NET_CERTIFICATE_CHAIN_VERIFICATION
    );
    logger.error(mappedError);
    if (pkijsError instanceof Error) {
      logger.info(pkijsError);
    }
    throw mappedError;
  }

  // Verify matching SSL hostname
  const commonName = findAttributeValue(
    leafCertificate.subject.typesAndValues,
    OID_COMMON_NAME
  )?.toString('utf-8');
  if (commonName !== SAFETY_NET_EXPECTED_LEAF_CERTIFICATE_HOST_NAME) {
    const error = new AndroidAttestationError(
      AndroidAttestationErrorReason.SAFETY_NET_LEAF_CERTIFICATE_HOST_NAME_NOT_MATCHING
    );
    logger.error(
      error,
      `Expected ${SAFETY_NET_EXPECTED_LEAF_CERTIFICATE_HOST_NAME} but common name was ${commonName}`
    );
    throw error;
  }

  // Verify JWS signature
  const jwsVerifyResult = await verifSafetyNetJWSSignature(
    safetyNetAttestationJws,
    decodedJwsHeader
  );
  // Verify additional JWS fields
  const parsedJwsPayload = safetyNetJwsMessagePayloadSchema.parse(
    JSON.parse(Buffer.from(jwsVerifyResult.payload).toString('utf-8'))
  );
  await verifySafetyNetJwsPayloadFields(parsedJwsPayload, safetyNetNonce);
};
