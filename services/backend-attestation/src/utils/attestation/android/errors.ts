/* eslint-disable max-classes-per-file */

export enum AndroidAttestationErrorReason {
  KEY_ATTESTATION_CERTIFICATE_CHAIN_VERIFICATION = 'KEY_ATTESTATION_CERTIFICATE_CHAIN_VERIFICATION',
  UNKNOWN_NONCE = 'UNKNOWN_NONCE',
  KEY_ATTESTATION_CERTIFICATE_REVOKED = 'KEY_ATTESTATION_CERTIFICATE_REVOKED',
  KEY_ATTESTATION_ATTESTED_PUBLIC_KEY_NOT_MATCHING_CERT = 'KEY_ATTESTATION_ATTESTED_PUBLIC_KEY_NOT_MATCHING_CERT',
  KEY_ATTESTATION_MALFORMED_EXTENSIONS = 'KEY_ATTESTATION_MALFORMED_EXTENSIONS',
  KEY_ATTESTATION_INSUFFICIENT_SECURITY_LEVEL = 'KEY_ATTESTATION_INSUFFICIENT_SECURITY_LEVEL',
  KEY_ATTESTATION_CHALLENGE_EXTENSION_NOT_MATCHING_NONCE = 'KEY_ATTESTATION_CHALLENGE_EXTENSION_NOT_MATCHING_NONCE',
  SAFETY_NET_CERTIFICATE_CHAIN_VERIFICATION = 'SAFETY_NET_CERTIFICATE_CHAIN_VERIFICATION',
  SAFETY_NET_LEAF_CERTIFICATE_HOST_NAME_NOT_MATCHING = 'SAFETY_NET_LEAF_CERTIFICATE_HOST_NAME_NOT_MATCHING',
  SAFETY_NET_JWS_SIGNATURE_INVALID = 'SAFETY_NET_JWS_SIGNATURE_INVALID',
  SAFETY_NET_JWS_AGE_INVALID = 'SAFETY_NET_JWS_AGE_INVALID',
  SAFETY_NET_NONCE_NOT_MATCHING = 'SAFETY_NET_NONCE_NOT_MATCHING',
  SAFETY_NET_PACKAGE_NAME_NOT_ALLOWED = 'SAFETY_NET_PACKAGE_NAME_NOT_ALLOWED',
  SAFETY_NET_NO_APK_CERTIFICATE_HASHES_PRESENT = 'SAFETY_NET_NO_APK_CERTIFICATE_HASHES_PRESENT',
  SAFETY_NET_APK_CERTIFICATE_HASHES_NOT_ALLOWED = 'SAFETY_NET_APK_CERTIFICATE_HASHES_NOT_ALLOWED',
  SAFETY_NET_CTS_PROFILE_MATCH_FALSE = 'SAFETY_NET_CTS_PROFILE_MATCH_FALSE',
  SAFETY_NET_BASIC_INTEGRITY_FALSE = 'SAFETY_NET_BASIC_INTEGRITY_FALSE',
  SAFETY_NET_EVALUATION_TYPE_INSUFFICIENT = 'SAFETY_NET_EVALUATION_TYPE_INSUFFICIENT',
}

export class AndroidAttestationError extends Error {
  reason: AndroidAttestationErrorReason;

  constructor(reason: AndroidAttestationErrorReason) {
    super(`Android device attestation failed. Reason: ${reason}`);
    this.reason = reason;
  }
}

export enum AndroidAssertionErrorReason {
  UNKNOWN_NONCE = 'UNKNOWN_NONCE',
  SIGNATURE_INVALID = 'SIGNATURE_INVALID',
}

export class AndroidAssertionError extends Error {
  reason: AndroidAssertionErrorReason;

  constructor(reason: AndroidAssertionErrorReason) {
    super(`Android device assertion failed. Reason: ${reason}`);
    this.reason = reason;
  }
}
