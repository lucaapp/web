import {
  Constructed,
  Enumerated,
  fromBER,
  OctetString,
  Sequence,
  LocalBaseBlock,
} from 'asn1js';
import crypto from 'isomorphic-webcrypto';
import { Logger } from 'pino';
import { z } from 'zod';
import { assertASN1Type } from '../../asn1js';
import {
  filterRevokedCertificates,
  verifyCertificateChain,
  PKIJSCertificateTuple,
} from '../../certificates';
import { proxyClient } from '../../proxy';
import {
  AndroidKeyAttestationSecurityLevel,
  getGoogleHardwareAttestationRootCertificatesPKIJS,
  GOOGLE_CRL_URL,
  OID_ANDROID_KEY_ATTESTATION_EXTENSIONS,
} from './config';

import {
  AndroidAttestationError,
  AndroidAttestationErrorReason,
} from './errors';

const androidAttestationCertificateRevocationSchema = z.object({
  entries: z.record(
    z.object({
      status: z.union([z.literal('REVOKED'), z.literal('SUSPENDED')]),
    })
  ),
});

const X962_EC_PUBLIC_KEY_PREFIX = Buffer.from(
  '3059301306072A8648CE3D020106082A8648CE3D030107034200',
  'hex'
);
const getECPublicKeyBytesAsX962 = (publicKeyBytes: Buffer): Buffer => {
  // Check if given public key bytes indicate an X9.63 EC public key
  if (publicKeyBytes.byteLength === 65 && publicKeyBytes[0] === 0x04) {
    return Buffer.concat([X962_EC_PUBLIC_KEY_PREFIX, publicKeyBytes]);
  }
  return publicKeyBytes;
};
/**
 * Fetches the revocation entries for the attestation certificates. Documentation can be found here:
 * https://developer.android.com/training/articles/security-key-attestation#certificate_status
 *
 * @param spkiPublicKey
 * @param certificatesSPKI
 * @param nonce
 * @returns
 */
const fetchRevokedCertificateSerialNumbers = async (): Promise<Buffer[]> => {
  const response = await proxyClient.get(GOOGLE_CRL_URL);

  const parsedRevocationsResponse = androidAttestationCertificateRevocationSchema.parse(
    response.data
  );
  return Object.keys(
    parsedRevocationsResponse.entries
  ).map(revokedSerialNumber => Buffer.from(revokedSerialNumber, 'hex'));
};

/**
 * For unknown reason, PKI.js fails to find the extension for certificates of some Android devices.
 * As a fallback, we are navigating the ASN.1 structure manually.
 */
const fallbackFindExtensionDataInASN1 = (
  asn1Data: LocalBaseBlock,
  logger: Logger
): ArrayBuffer | null => {
  logger.warn(
    {
      asn1Data: Buffer.from(asn1Data.valueBeforeDecode).toString('base64'),
    },
    `Did not find Android key attestation certificate extension data via OID ${OID_ANDROID_KEY_ATTESTATION_EXTENSIONS}, trying fallback lookup now`
  );
  try {
    assertASN1Type(Sequence, asn1Data);
    const sequenceLevel1 = asn1Data.valueBlock.value[0];
    assertASN1Type(Sequence, sequenceLevel1);
    const constructedLevel2 = sequenceLevel1.valueBlock.value[7];
    assertASN1Type(Constructed, constructedLevel2);
    const sequenceLevel3 = constructedLevel2.valueBlock.value[0];
    assertASN1Type(Sequence, sequenceLevel3);
    const sequenceLevel3Blocks = sequenceLevel3.valueBlock.value;

    const extensionSequenceLevel4 =
      // This sequence contains the OID in question as the first item and the octet string as its
      // seconds item. However, sometimes (e.g. for the Samsung S10 sample), these two items
      // are reversed. This is NOT the case when decoding via https://lapo.it/asn1js/ so it seems
      // to be an issue in the asn1js library. Since the octet string, if even remotely valid,
      // is larger than the OID, we use this block length to patch the order.
      sequenceLevel3Blocks[0].blockLength > sequenceLevel3Blocks[1].blockLength
        ? sequenceLevel3Blocks[0]
        : sequenceLevel3Blocks[1];
    assertASN1Type(Sequence, extensionSequenceLevel4);
    const extensionValueOctetString =
      extensionSequenceLevel4.valueBlock.value[1];
    assertASN1Type(OctetString, extensionValueOctetString);

    return extensionValueOctetString.valueBlock.valueHex;
  } catch (error) {
    logger.error(
      error as Error,
      'Failed fallback extraction of Android key attestation certificate extension'
    );
    return null;
  }
};

const extractExtensionDataFromKeyAttestationCertificate = (
  certificateTuple: PKIJSCertificateTuple,
  logger: Logger
): Sequence => {
  const keyAttestationExtensionBuffer =
    (certificateTuple.certificate.extensions ?? []).find(
      extension => extension.extnID === OID_ANDROID_KEY_ATTESTATION_EXTENSIONS
    )?.extnValue.valueBlock.valueHex ??
    fallbackFindExtensionDataInASN1(certificateTuple.asn, logger);

  if (!keyAttestationExtensionBuffer) {
    throw new AndroidAttestationError(
      AndroidAttestationErrorReason.KEY_ATTESTATION_MALFORMED_EXTENSIONS
    );
  }

  const keyAttestationExtensionData = fromBER(keyAttestationExtensionBuffer)
    .result;

  assertASN1Type(Sequence, keyAttestationExtensionData);

  return keyAttestationExtensionData;
};

const verifyKeyAttestationCertificateExtensionData = (
  certificateTuple: PKIJSCertificateTuple,
  nonce: Buffer,
  logger: Logger
): void => {
  const keyAttestationExtensionData = extractExtensionDataFromKeyAttestationCertificate(
    certificateTuple,
    logger
  );

  // Check attestation security level

  const securityLevelBlock = keyAttestationExtensionData.valueBlock.value[1];
  assertASN1Type(Enumerated, securityLevelBlock);
  if (
    securityLevelBlock.valueBlock.valueDec <
    AndroidKeyAttestationSecurityLevel.TrustedEnvironment
  ) {
    const error = new AndroidAttestationError(
      AndroidAttestationErrorReason.KEY_ATTESTATION_INSUFFICIENT_SECURITY_LEVEL
    );
    logger.error(
      error,
      `Security level was ${securityLevelBlock.valueBlock.valueDec}`
    );
    throw error;
  }

  // Check keymaster security level

  const keymasterSecurityLevelBlock =
    keyAttestationExtensionData.valueBlock.value[3];
  assertASN1Type(Enumerated, keymasterSecurityLevelBlock);
  if (
    keymasterSecurityLevelBlock.valueBlock.valueDec <
    AndroidKeyAttestationSecurityLevel.TrustedEnvironment
  ) {
    const error = new AndroidAttestationError(
      AndroidAttestationErrorReason.KEY_ATTESTATION_INSUFFICIENT_SECURITY_LEVEL
    );
    logger.error(
      error,
      `Keymaster security level was ${securityLevelBlock.valueBlock.valueDec}`
    );
    throw error;
  }

  // Check attestation challenge

  const attestationChallengeBlock =
    keyAttestationExtensionData.valueBlock.value[4];
  assertASN1Type(OctetString, attestationChallengeBlock);

  if (
    !Buffer.from(attestationChallengeBlock.valueBlock.valueHex).equals(nonce)
  ) {
    throw new AndroidAttestationError(
      AndroidAttestationErrorReason.KEY_ATTESTATION_CHALLENGE_EXTENSION_NOT_MATCHING_NONCE
    );
  }
};

export const verifyAndroidKeyAttestation = async (
  attestedPublicKeySPKI: Buffer,
  [
    leafCertificateTuple,
    ...intermediateCertificateTuples
  ]: PKIJSCertificateTuple[],
  nonce: Buffer,
  logger: Logger
): Promise<void> => {
  // Verify certificate chain

  const leafCertificate = leafCertificateTuple.certificate;
  const intermediateCertificates = intermediateCertificateTuples.map(
    ({ certificate }) => certificate
  );
  try {
    await verifyCertificateChain({
      leafCertificate,
      intermediateCertificates,
      trustedCertificates: getGoogleHardwareAttestationRootCertificatesPKIJS(),
    });
  } catch (validationError) {
    const mappedError = new AndroidAttestationError(
      AndroidAttestationErrorReason.KEY_ATTESTATION_CERTIFICATE_CHAIN_VERIFICATION
    );

    logger.error(mappedError);
    if (validationError instanceof Error) {
      logger.error(validationError);
    }

    throw mappedError;
  }

  const revokedCertificateSerialNumbers = await fetchRevokedCertificateSerialNumbers();

  // Check for revoked certificates

  const revokedCertificates = filterRevokedCertificates(
    [leafCertificate, ...intermediateCertificates],
    revokedCertificateSerialNumbers
  );
  if (revokedCertificates.length > 0) {
    throw new AndroidAttestationError(
      AndroidAttestationErrorReason.KEY_ATTESTATION_CERTIFICATE_REVOKED
    );
  }

  // Check if leaf certificate matches attested public key

  const publicKeyFromCertificate = Buffer.from(
    await crypto.subtle.exportKey('spki', await leafCertificate.getPublicKey())
  );

  const attestedPublicKeyBytes = getECPublicKeyBytesAsX962(
    attestedPublicKeySPKI
  );

  if (!publicKeyFromCertificate.equals(attestedPublicKeyBytes)) {
    throw new AndroidAttestationError(
      AndroidAttestationErrorReason.KEY_ATTESTATION_ATTESTED_PUBLIC_KEY_NOT_MATCHING_CERT
    );
  }

  // Verify certificate extensions
  await verifyKeyAttestationCertificateExtensionData(
    leafCertificateTuple,
    nonce,
    logger
  );
};
