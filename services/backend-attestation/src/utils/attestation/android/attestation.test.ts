/* eslint-disable no-param-reassign */
import { useFakeTimers } from 'sinon';
import AxiosMockAdapter from 'axios-mock-adapter';
import jose from 'jose';
import { rootCertificates } from 'tls';
import { readFileSync } from 'fs';
import zod from 'zod';
import path from 'path';
import { Sequence } from 'asn1js';
import { z } from '../../validation';
import { verifyAndroidAttestation } from './attestation';
import {
  safetyNetJwsHeaderSchema,
  TSafetyNetJWSMessageHeader,
  TSafetyNetJWSMessagePayload,
} from './safetyNetAttestation';
import {
  extractBERFromPEM,
  parsePKIJSCertificateFromBER,
  PKIJSCertificateTuple,
} from '../../certificates';
import { proxyClient } from '../../proxy';
import { GOOGLE_CRL_URL } from './config';

let mockJWSModifyHeader:
  | null
  | ((decodedHeader: TSafetyNetJWSMessageHeader) => void) = null;
let mockJWSModifyPayload:
  | null
  | ((decodedAndParsedPayload: TSafetyNetJWSMessagePayload) => void) = null;

jest.mock('jose', (): typeof jose => {
  // eslint-disable-next-line global-require
  const {
    compactVerify,
    decodeProtectedHeader,
    ...realJose
  } = jest.requireActual('jose') as typeof jose;

  return {
    ...realJose,
    compactVerify: async (
      jws,
      keyLike,
      options
    ): ReturnType<typeof compactVerify> => {
      const decoded = await compactVerify(
        jws,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        keyLike as any,
        options
      );
      const parsedPayload = JSON.parse(
        Buffer.from(decoded.payload).toString('utf-8')
      );

      if (mockJWSModifyPayload) {
        mockJWSModifyPayload(parsedPayload);
      }
      decoded.payload = Buffer.from(JSON.stringify(parsedPayload), 'utf-8');

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      return decoded as any;
    },
    decodeProtectedHeader: jws => {
      const header = decodeProtectedHeader(jws);
      const parsedHeader = safetyNetJwsHeaderSchema.parse(header);

      if (mockJWSModifyHeader) {
        mockJWSModifyHeader(parsedHeader);
      }

      return parsedHeader;
    },
  };
});

let mockedAllowedAPKPackageName: string | null = null;
jest.mock('./config', () => {
  const actual = jest.requireActual('./config');
  return {
    ...actual,
    getAllowedAPKPackageNames: () => {
      const allowedAPKPackageNames = actual.getAllowedAPKPackageNames();
      return {
        ...allowedAPKPackageNames,
        consumerApp:
          mockedAllowedAPKPackageName ?? allowedAPKPackageNames.consumerApp,
      };
    },
  };
});

const UNRELATED_PUBLIC_KEY_BASE64 = `MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEAj2+7ATk/S+qvC2AlYAe7X1MPykmWpUxYN66hi7KRLCAguSyPZ2+HAo+RSnW0snbfUSjjM1yfL0qTYmCME/LNg==`;

const sampleData = {
  getKeyAttestationCertificates: (): PKIJSCertificateTuple[] =>
    [
      'MIIC3DCCAoKgAwIBAgIBATAKBggqhkjOPQQDAjA5MQwwCgYDVQQMDANURUUxKTAnBgNVBAUTIGZjNWEzN2M2ZTRlNzUwNmRhM2I4YTM1MjIwYTEzMWRlMB4XDTcwMDEwMTAwMDAwMFoXDTQ4MDEwMTAwMDAwMFowHzEdMBsGA1UEAxMUQW5kcm9pZCBLZXlzdG9yZSBLZXkwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAARlVfQlHe1PY+MCayY3IL4v4rvgyg2v+1azcbZazpfQ7YBen6ME4wHfqioqMSU94ms89DpUMPNWQqs9sHzVKF2Ro4IBkzCCAY8wDgYDVR0PAQH/BAQDAgeAMIIBewYKKwYBBAHWeQIBEQSCAWswggFnAgFkCgEBAgFkCgEBBCBNrVDYJYXMbgPiNN1MLKNHimrPivV1mRlWEkNvSFML3QQAME2/hUVJBEcwRTEfMB0EGGRlLmN1bHR1cmU0bGlmZS5sdWNhLmRldgIBXDEiBCCcHd4CB6h4WgawQchBMNIGYaNQdjneTv38m/l71pU/mzCB5aEFMQMCAQKiAwIBA6MEAgIBAKQFMQMCAQGlBTEDAgEEqgMCAQG/g3cCBQC/hT4DAgEAv4VATDBKBCAPbnXIAYO13sB0sAVNQnHpk4nr5LE2sIGd4fFQug/51wEB/woBAAQgIJG+QoQAVKJhqOUCvsrL655hSl8psZSBmZHQHTyhVbG/hUEFAgMB1MC/hUIFAgMDFdq/hUYIBAZnb29nbGW/hUcIBAZvcmlvbGW/hUgIBAZvcmlvbGW/hUwIBAZHb29nbGW/hU0JBAdQaXhlbCA2v4VOBgIEATSJLb+FTwYCBAE0iS0wCgYIKoZIzj0EAwIDSAAwRQIhAOnHbtcw5AgWI9L1edbpti6fkPLFwDO/ImR+h/gOU3ZIAiBV7H6QASI1wuIdmKLou6b8lzGtpBHvm6XFGR/uYC++xg==',
      'MIIB9TCCAXqgAwIBAgIRAKZjq2EOaddxacazrkpx++wwCgYIKoZIzj0EAwIwOTEMMAoGA1UEDAwDVEVFMSkwJwYDVQQFEyBiNzAxYmRlYmFhMGUxNjNjNDk4M2VmNDQ5Y2YyMjczYTAeFw0yMTAxMTMyMTEyMTJaFw0zMTAxMTEyMTEyMTJaMDkxDDAKBgNVBAwMA1RFRTEpMCcGA1UEBRMgZmM1YTM3YzZlNGU3NTA2ZGEzYjhhMzUyMjBhMTMxZGUwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAATkQ82uz1ny7WhizyaCgh2TnXiDpunNmeUGvIIeSNR9ClPLcQwr3O9rsYgKFtFSGetiOle/WlbHv7PbSywnBhwwo2MwYTAdBgNVHQ4EFgQU8LZpVwLlKuteY/lZWjAnkcyKyFYwHwYDVR0jBBgwFoAUgsAFCf3Gj3EYKjzaIEx2/7vO+h0wDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAgQwCgYIKoZIzj0EAwIDaQAwZgIxANDIh2UTqgytVuDWI7L5OAFs3pFthM+D9PaYJh17zUi+pdMqWzoHYypnTdcAfkPmkwIxAJcKhT92iVPBEXGMx5OABFDsOvPfLDKdmNd5fjNMuHFtXanXZ+/M4O2rAXaByM7YzA==',
      'MIIDlDCCAXygAwIBAgIRAMCYDGn+bv4sULuIPvxLWIQwDQYJKoZIhvcNAQELBQAwGzEZMBcGA1UEBRMQZjkyMDA5ZTg1M2I2YjA0NTAeFw0yMTAxMTMyMTA5MjdaFw0zMTAxMTEyMTA5MjdaMDkxDDAKBgNVBAwMA1RFRTEpMCcGA1UEBRMgYjcwMWJkZWJhYTBlMTYzYzQ5ODNlZjQ0OWNmMjI3M2EwdjAQBgcqhkjOPQIBBgUrgQQAIgNiAAS04MJay37J4sxw7bsoKUL1AjhT8SNSFCC9s3tsvLd1rv57tvWLOq8w7N71ttWq6EJNhw+JEjHLFnXhoiKjRIlyjcdeoj0PddtmW/ZoFuzTBcPfzPBQpl03v43ZfGWk+UijYzBhMB0GA1UdDgQWBBSCwAUJ/caPcRgqPNogTHb/u876HTAfBgNVHSMEGDAWgBQ2YeEAfIgFCVGLRGxH/xpMyepPEjAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwICBDANBgkqhkiG9w0BAQsFAAOCAgEAMuU8AneoBiXrItzXOKFS9dB3fsBcWvVdhtnhDC1CaJ1469bla71nVJ8lZazEaBhBwU5207Gb5elK2phdN88uO6raPyZHinEEA2FS1WcM5o4bsp8RrZLn/cfWjc0vD3zXSamFQec8btU4KJtjWSdB3NDgZX3TLpL18NLd2tnvm4K6me8sC7N/yexMKa/MSjtER6EAGWRDpW7AkAk3c4OixLF0Im/uZTAP8qn1aEiHq22zHMWRAQ+83oO8rffza1Yipv3DdamGzGXTOd2J3fedn0LbBVpFJw8N97PT87zI/wYp886+BXZCmUkkjtl5zFDVJH3gJSmuZGdSVvNY+UcqYufmW9vBQaN69IuRmlSD8Vvgz9WzvPkompcOQrTXZYCRL3NGx6oVXRQiJGDwrzru5IHYaSRCnpTDFJDf4+9ND5Q0IBbpdcRJ1KAmiMa+0QwTQtDVeevT7ycm38svLR7EJMe4M16cxrYamkiZweeGeJyCte4N0eoHTDVGsnPf8pwYBw3hwPmb3WpN1rRKju4+TXf7495aFX8jfT3i7DigqF6U9na1yL2SPJinAxrlNH/b91YIOResBzErUwKKcNDe/PnKes5/TMO6ZbjPuzTu8aeHAFSVHA1Sq5T0gT+h45Sj/ICxFHZjva8ED5jc5aCOJwt81q40cZIrYT8DjPCSYAE=',
      'MIIFHDCCAwSgAwIBAgIJANUP8luj8tazMA0GCSqGSIb3DQEBCwUAMBsxGTAXBgNVBAUTEGY5MjAwOWU4NTNiNmIwNDUwHhcNMTkxMTIyMjAzNzU4WhcNMzQxMTE4MjAzNzU4WjAbMRkwFwYDVQQFExBmOTIwMDllODUzYjZiMDQ1MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAr7bHgiuxpwHsK7Qui8xUFmOr75gvMsd/dTEDDJdSSxtf6An7xyqpRR90PL2abxM1dEqlXnf2tqw1Ne4Xwl5jlRfdnJLmN0pTy/4lj4/7tv0Sk3iiKkypnEUtR6WfMgH0QZfKHM1+di+y9TFRtv6y//0rb+T+W8a9nsNL/ggjnar86461qO0rOs2cXjp3kOG1FEJ5MVmFmBGtnrKpa73XpXyTqRxB/M0n1n/W9nGqC4FSYa04T6N5RIZGBN2z2MT5IKGbFlbC8UrW0DxW7AYImQQcHtGl/m00QLVWutHQoVJYnFPlXTcHYvASLu+RhhsbDmxMgJJ0mcDpvsC4PjvB+TxywElgS70vE0XmLD+OJtvsBslHZvPBKCOdT0MS+tgSOIfga+z1Z1g7+DVagf7quvmag8jfPioyKvxnK/EgsTUVi2ghzq8wm27ud/mIM7AY2qEORR8Go3TVB4HzWQgpZrt3i5MIlCaY504LzSRiigHCzAPlHws+W0rB5N+er5/2pJKnfBSDiCiFAVtCLOZ7gLiMm0jhO2B6tUXHI/+MRPjy02i59lINMRRev56GKtcd9qO/0kUJWdZTdA2XoS82ixPvZtXQpUpuL12ab+9EaDK8Z4RHJYYfCT3Q5vNAXaiWQ+8PTWm2QgBR/bkwSWc+NpUFgNPN9PvQi8WEg5UmAGMCAwEAAaNjMGEwHQYDVR0OBBYEFDZh4QB8iAUJUYtEbEf/GkzJ6k8SMB8GA1UdIwQYMBaAFDZh4QB8iAUJUYtEbEf/GkzJ6k8SMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgIEMA0GCSqGSIb3DQEBCwUAA4ICAQBOMaBc8oumXb2voc7XCWnuXKhBBK3e2KMGz39t7lA3XXRe2ZLLAkLM5y3J7tURkf5a1SutfdOyXAmeE6SRo83Uh6WszodmMkxK5GM4JGrnt4pBisu5igXEydaW7qq2CdC6DOGjG+mEkN8/TA6p3cnoL/sPyz6evdjLlSeJ8rFBH6xWyIZCbrcpYEJzXaUOEaxxXxgYz5/cTiVKN2M1G2okQBUIYSY6bjEL4aUN5cfo7ogP3UvliEo3Eo0YgwuzR2v0KR6C1cZqZJSTnghIC/vAD32KdNQ+c3N+vl2OTsUVMC1GiWkngNx1OO1+kXW+YTnnTUOtOIswUP/Vqd5SYgAImMAfY8U9/iIgkQj6T2W6FsScy94IN9fFhE1UtzmLoBIuUFsVXJMTz+Jucth+IqoWFua9v1R93/k98p41pjtFX+H8DslVgfP097vju4KDlqN64xV1grw3ZLl4CiOe/A91oeLm2UHOq6wn3esB4r2EIQKb6jTVGu5sYCcdWpXr0AUVqcABPdgL+H7qJguBw09ojm6xNIrw2OocrDKsudk/okr/AwqEyPKw9WnMlQgLIKw1rODG2NvU9oR3GVGdMkUBZutL8VuFkERQGt6vQ2OCw0sV47VMkuYbacK/xyZFiRcrPJPb41zgbQj9XAEyLKCHex0SdDrx+tWUDqG8At2JHA==',
    ].map(certificateHex =>
      parsePKIJSCertificateFromBER(Buffer.from(certificateHex, 'base64'))
    ),
  keyAttestationPublicKey:
    'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEZVX0JR3tT2PjAmsmNyC+L+K74MoNr/tWs3G2Ws6X0O2AXp+jBOMB36oqKjElPeJrPPQ6VDDzVkKrPbB81ShdkQ==',
  nonce: 'YWr0DmBHu++0IMt3hO8R7PPTGbGTDYFS3ddFsa1QyKY=',
  safetyNetAttestationJws:
    'eyJhbGciOiJSUzI1NiIsIng1YyI6WyJNSUlGWURDQ0JFaWdBd0lCQWdJUkFOaGNHbDcwQjVhSUNRQUFBQUVCbi9Fd0RRWUpLb1pJaHZjTkFRRUxCUUF3UmpFTE1Ba0dBMVVFQmhNQ1ZWTXhJakFnQmdOVkJBb1RHVWR2YjJkc1pTQlVjblZ6ZENCVFpYSjJhV05sY3lCTVRFTXhFekFSQmdOVkJBTVRDa2RVVXlCRFFTQXhSRFF3SGhjTk1qSXdNVEkxTVRBd01ETTBXaGNOTWpJd05ESTFNVEF3TURNeldqQWRNUnN3R1FZRFZRUURFeEpoZEhSbGMzUXVZVzVrY205cFpDNWpiMjB3Z2dFaU1BMEdDU3FHU0liM0RRRUJBUVVBQTRJQkR3QXdnZ0VLQW9JQkFRQ1k1bHpGY0hsZTFETGx0TkpobFNjbnFWUnNYQ1d6NjFGby9GR0tsYm00bGI5YzdyWXpZTm9MTWxUWGtaaUs0R1JFdnZqZ3dMd2M3TEM4TTZ6b3JGcWE5ajN6NG0vTXVkQ2FGVnR3MEFVbmVqalZSaFRiWkVKaWs4UUViaHg1YXpCTlNwM2grRzg2NUxaK3lnRGRkMFZaS2RxNTNLQjlqMEY4eWJrZHZVY1NzL20zR01qV0VBaXA0V25yRFk5RkxaZngrcENwQU5PQWJUTnZjaWlLQXdPa1FHREVJMUZxVEN1SW5aaUhSdm1pZk9Rc09uU0V4SXUzc1c3dlFjRXRUYkYrVVp4aGpiSDVFdmJkb0VuYUxNNlRCSnl1bDd0eld1ajRZNFhUY2t2ZFNDbnJBU3dzZ3lROXVOOXdoUHZBVm54R1ZCWElFVEV0VUE4bXlQNDNUS3NKQWdNQkFBR2pnZ0p3TUlJQ2JEQU9CZ05WSFE4QkFmOEVCQU1DQmFBd0V3WURWUjBsQkF3d0NnWUlLd1lCQlFVSEF3RXdEQVlEVlIwVEFRSC9CQUl3QURBZEJnTlZIUTRFRmdRVXFWTTJVTVpWQUs1Q3lRWTZGR3J0U0k3MXMyb3dId1lEVlIwakJCZ3dGb0FVSmVJWURySlhrWlFxNWRSZGhwQ0QzbE96dUpJd2JRWUlLd1lCQlFVSEFRRUVZVEJmTUNvR0NDc0dBUVVGQnpBQmhoNW9kSFJ3T2k4dmIyTnpjQzV3YTJrdVoyOXZaeTluZEhNeFpEUnBiblF3TVFZSUt3WUJCUVVITUFLR0pXaDBkSEE2THk5d2Eya3VaMjl2Wnk5eVpYQnZMMk5sY25SekwyZDBjekZrTkM1a1pYSXdIUVlEVlIwUkJCWXdGSUlTWVhSMFpYTjBMbUZ1WkhKdmFXUXVZMjl0TUNFR0ExVWRJQVFhTUJnd0NBWUdaNEVNQVFJQk1Bd0dDaXNHQVFRQjFua0NCUU13UHdZRFZSMGZCRGd3TmpBMG9ES2dNSVl1YUhSMGNEb3ZMMk55YkhNdWNHdHBMbWR2YjJjdlozUnpNV1EwYVc1MEwxSTNPR1kxZWpOcU4zbG5MbU55YkRDQ0FRTUdDaXNHQVFRQjFua0NCQUlFZ2ZRRWdmRUE3d0IxQUZHanNQWDlBWG1jVm0yNE4zaVBES1I2ekJzbnkvZWVpRUthRGY3VWl3WGxBQUFCZnBEbERBSUFBQVFEQUVZd1JBSWdJNDVsUHEwNVdWeEl6bzFVbGhoU0V2cklvQVY1RXF0MCtsVkVuaWxYcThVQ0lDV3BHRkg5RC9EeWZnYWdXMy8yZ0V1SFpaOEtHSzlCOUpaekJDSitCdlNlQUhZQUtYbSs4SjQ1T1NId1ZuT2ZZNlYzNWI1WGZaeGdDdmo1VFYwbVhDVmR4NFFBQUFGK2tPVUw0Z0FBQkFNQVJ6QkZBaUVBb2NtVmRjbENEMmJGUE9Ob1YyMXRiOEdzZVdkMkZtM1dTR3FXTTB3RDBCc0NJRWV0RHlwNXpjbjU4ajhoUkRSby9WVUd0ZzNtdjIrWTZKRjRqbnpCUktFUU1BMEdDU3FHU0liM0RRRUJDd1VBQTRJQkFRQUlubHhuSUl2Q0trVmlKZTVidEU2TVBZQWp4M0dIWjFLL3psdHBzZU1SUThiRlVLTUZMU1NxN3VORlBRcjdPVzNoQ2hnTENDVm9Fekc0YnFGdU14V2IrSHQ5UEh0RnhWWHpiZ0p5amJ2RDdIU09UcWs4QVkxYS9OUTV1anNDTFNKNERmNlJkaEgvT3ZwdGVQM05mbFVXTk1JQkV2MFV2MXR2TEVmUUdXMGhTYmc2TC9IR2dBY1d1TDdsNi9QWElFdTJlTDdrYUdGUmhJMmJqNEpOOVlFSEdudmhjR3A1NXlCMzdoSXgxbDhVNzVYOWhIMU82TU1tenZKMDVxdFhDc1RYUWllakQwVHR4VGpHVitWS3RwTFhJQ3BUZnhOc3BCekNMaDkxSUxtMnBHNFY5ZGttRVZvOTB0SnpKSS9BSzZhUGZvZ2NKb0JnbnBTOFVZd0FObVNDIiwiTUlJRmpEQ0NBM1NnQXdJQkFnSU5BZ0NPc2dJek5tV0xaTTNibXpBTkJna3Foa2lHOXcwQkFRc0ZBREJITVFzd0NRWURWUVFHRXdKVlV6RWlNQ0FHQTFVRUNoTVpSMjl2WjJ4bElGUnlkWE4wSUZObGNuWnBZMlZ6SUV4TVF6RVVNQklHQTFVRUF4TUxSMVJUSUZKdmIzUWdVakV3SGhjTk1qQXdPREV6TURBd01EUXlXaGNOTWpjd09UTXdNREF3TURReVdqQkdNUXN3Q1FZRFZRUUdFd0pWVXpFaU1DQUdBMVVFQ2hNWlIyOXZaMnhsSUZSeWRYTjBJRk5sY25acFkyVnpJRXhNUXpFVE1CRUdBMVVFQXhNS1IxUlRJRU5CSURGRU5EQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCQUt2QXFxUENFMjdsMHc5ekM4ZFRQSUU4OWJBK3hUbURhRzd5N1ZmUTRjK21PV2hsVWViVVFwSzB5djJyNjc4UkpFeEswSFdEamVxK25MSUhOMUVtNWo2ckFSWml4bXlSU2poSVIwS09RUEdCTVVsZHNhenRJSUo3TzBnLzgycWovdkdEbC8vM3Q0dFRxeGlSaExRblRMWEpkZUIrMkRoa2RVNklJZ3g2d043RTVOY1VIM1Jjc2VqY3FqOHA1U2oxOXZCbTZpMUZocUxHeW1oTUZyb1dWVUdPM3h0SUg5MWRzZ3k0ZUZLY2ZLVkxXSzNvMjE5MFEwTG0vU2lLbUxiUko1QXU0eTFldUZKbTJKTTllQjg0RmtxYTNpdnJYV1VlVnR5ZTBDUWRLdnNZMkZrYXp2eHR4dnVzTEp6TFdZSGs1NXpjUkFhY0RBMlNlRXRCYlFmRDFxc0NBd0VBQWFPQ0FYWXdnZ0Z5TUE0R0ExVWREd0VCL3dRRUF3SUJoakFkQmdOVkhTVUVGakFVQmdnckJnRUZCUWNEQVFZSUt3WUJCUVVIQXdJd0VnWURWUjBUQVFIL0JBZ3dCZ0VCL3dJQkFEQWRCZ05WSFE0RUZnUVVKZUlZRHJKWGtaUXE1ZFJkaHBDRDNsT3p1Skl3SHdZRFZSMGpCQmd3Rm9BVTVLOHJKbkVhSzBnbmhTOVNaaXp2OElrVGNUNHdhQVlJS3dZQkJRVUhBUUVFWERCYU1DWUdDQ3NHQVFVRkJ6QUJoaHBvZEhSd09pOHZiMk56Y0M1d2Eya3VaMjl2Wnk5bmRITnlNVEF3QmdnckJnRUZCUWN3QW9Za2FIUjBjRG92TDNCcmFTNW5iMjluTDNKbGNHOHZZMlZ5ZEhNdlozUnpjakV1WkdWeU1EUUdBMVVkSHdRdE1Dc3dLYUFub0NXR0kyaDBkSEE2THk5amNtd3VjR3RwTG1kdmIyY3ZaM1J6Y2pFdlozUnpjakV1WTNKc01FMEdBMVVkSUFSR01FUXdDQVlHWjRFTUFRSUJNRGdHQ2lzR0FRUUIxbmtDQlFNd0tqQW9CZ2dyQmdFRkJRY0NBUlljYUhSMGNITTZMeTl3YTJrdVoyOXZaeTl5WlhCdmMybDBiM0o1THpBTkJna3Foa2lHOXcwQkFRc0ZBQU9DQWdFQUlWVG95MjRqd1hVcjByQVBjOTI0dnVTVmJLUXVZdzNuTGZsTGZMaDVBWVdFZVZsL0R1MThRQVdVTWRjSjZvL3FGWmJoWGtCSDBQTmN3OTd0aGFmMkJlb0RZWTlDay9iK1VHbHVoeDA2emQ0RUJmN0g5UDg0bm5yd3BSKzRHQkRaSytYaDNJMHRxSnkycmdPcU5EZmxyNUlNUThaVFdBM3lsdGFrelNCS1o2WHBGMFBwcXlDUnZwL05DR3YyS1gyVHVQQ0p2c2NwMS9tMnBWVHR5QmpZUFJRK1F1Q1FHQUpLanRON1I1REZyZlRxTVd2WWdWbHBDSkJrd2x1Nys3S1kzY1RJZnpFN2NtQUxza01LTkx1RHorUnpDY3NZVHNWYVU3VnAzeEw2ME9ZaHFGa3VBT094RFo2cEhPajkrT0ptWWdQbU9UNFgzKzdMNTFmWEp5Ukg5S2ZMUlA2blQzMUQ1bm1zR0FPZ1oyNi84VDloc0JXMXVvOWp1NWZaTFpYVlZTNUgwSHlJQk1FS3lHTUlQaEZXcmx0L2hGUzI4TjF6YUtJMFpCR0QzZ1lnRExiaURUOWZHWHN0cGsrRm1jNG9sVmxXUHpYZTgxdmRvRW5GYnI1TTI3MkhkZ0pXbytXaFQ5QllNMEppK3dkVm1uUmZmWGdsb0VvbHVUTmNXemM0MWRGcGdKdThmRjNMRzBnbDJpYlNZaUNpOWE2aHZVMFRwcGpKeUlXWGhrSlRjTUpsUHJXeDFWeXRFVUdyWDJsMEpEd1JqVy82NTZyMEtWQjAyeEhSS3ZtMlpLSTAzVGdsTElwbVZDSzNrQktrS05wQk5rRnQ4cmhhZmNDS09iOUp4Lzl0cE5GbFFUbDdCMzlySmxKV2tSMTdRblpxVnB0RmVQRk9Sb1ptRnpNPSIsIk1JSUZZakNDQkVxZ0F3SUJBZ0lRZDcwTmJOczIrUnJxSVEvRThGalREVEFOQmdrcWhraUc5dzBCQVFzRkFEQlhNUXN3Q1FZRFZRUUdFd0pDUlRFWk1CY0dBMVVFQ2hNUVIyeHZZbUZzVTJsbmJpQnVkaTF6WVRFUU1BNEdBMVVFQ3hNSFVtOXZkQ0JEUVRFYk1Ca0dBMVVFQXhNU1IyeHZZbUZzVTJsbmJpQlNiMjkwSUVOQk1CNFhEVEl3TURZeE9UQXdNREEwTWxvWERUSTRNREV5T0RBd01EQTBNbG93UnpFTE1Ba0dBMVVFQmhNQ1ZWTXhJakFnQmdOVkJBb1RHVWR2YjJkc1pTQlVjblZ6ZENCVFpYSjJhV05sY3lCTVRFTXhGREFTQmdOVkJBTVRDMGRVVXlCU2IyOTBJRkl4TUlJQ0lqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FnOEFNSUlDQ2dLQ0FnRUF0aEVDaXg3am9YZWJPOXkvbEQ2M2xhZEFQS0g5Z3ZsOU1nYUNjZmIyakgvNzZOdThhaTZYbDZPTVMva3I5ckg1em9RZHNmbkZsOTd2dWZLajZid1NpVjZucWxLcitDTW55NlN4bkdQYjE1bCs4QXBlNjJpbTlNWmFSdzFORURQalRyRVRvOGdZYkV2cy9BbVEzNTFrS1NVakI2RzAwajB1WU9EUDBnbUh1ODFJOEUzQ3ducUlpcnU2ejFrWjFxK1BzQWV3bmpIeGdzSEEzeTZtYld3WkRyWFlmaVlhUlFNOXNIbWtsQ2l0RDM4bTVhZ0kvcGJvUEdpVVUrNkRPb2dyRlpZSnN1QjZqQzUxMXB6cnAxWmtqNVpQYUs0OWw4S0VqOEM4UU1BTFhMMzJoN00xYkt3WVVIK0U0RXpOa3RNZzZUTzhVcG12TXJVcHN5VXF0RWo1Y3VIS1pQZm1naENONkozQ2lvajZPR2FLL0dQNUFmbDQvWHRjZC9wMmgvcnMzN0VPZVpWWHRMMG03OVlCMGVzV0NydU9DN1hGeFlwVnE5T3M2cEZMS2N3WnBESWxUaXJ4WlVUUUFzNnF6a20wNnA5OGc3QkFlK2REcTZkc280OTlpWUg2VEtYLzFZN0R6a3ZndGRpemprWFBkc0R0UUN2OVV3K3dwOVU3RGJHS29nUGVNYTNNZCtwdmV6N1czNUVpRXVhKyt0Z3kvQkJqRkZGeTNsM1dGcE85S1dnejd6cG03QWVLSnQ4VDExZGxlQ2ZlWGtrVUFLSUFmNXFvSWJhcHNaV3dwYmtORmhIYXgyeElQRURnZmcxYXpWWTgwWmNGdWN0TDdUbExuTVEvMGxVVGJpU3cxbkg2OU1HNnpPMGI5ZjZCUWRnQW1EMDZ5SzU2bURjWUJaVUNBd0VBQWFPQ0FUZ3dnZ0UwTUE0R0ExVWREd0VCL3dRRUF3SUJoakFQQmdOVkhSTUJBZjhFQlRBREFRSC9NQjBHQTFVZERnUVdCQlRrcnlzbWNSb3JTQ2VGTDFKbUxPL3dpUk54UGpBZkJnTlZIU01FR0RBV2dCUmdlMllhUlEyWHlvbFFMMzBFelRTby8vejlTekJnQmdnckJnRUZCUWNCQVFSVU1GSXdKUVlJS3dZQkJRVUhNQUdHR1doMGRIQTZMeTl2WTNOd0xuQnJhUzVuYjI5bkwyZHpjakV3S1FZSUt3WUJCUVVITUFLR0hXaDBkSEE2THk5d2Eya3VaMjl2Wnk5bmMzSXhMMmR6Y2pFdVkzSjBNRElHQTFVZEh3UXJNQ2t3SjZBbG9DT0dJV2gwZEhBNkx5OWpjbXd1Y0d0cExtZHZiMmN2WjNOeU1TOW5jM0l4TG1OeWJEQTdCZ05WSFNBRU5EQXlNQWdHQm1lQkRBRUNBVEFJQmdabmdRd0JBZ0l3RFFZTEt3WUJCQUhXZVFJRkF3SXdEUVlMS3dZQkJBSFdlUUlGQXdNd0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFEU2tIckVvbzlDMGRoZW1NWG9oNmRGU1BzamJkQlpCaUxnOU5SM3Q1UCtUNFZ4ZnE3dnFmTS9iNUEzUmkxZnlKbTlidmhkR2FKUTNiMnQ2eU1BWU4vb2xVYXpzYUwreXlFbjlXcHJLQVNPc2hJQXJBb3labCt0SmFveDExOGZlc3NtWG4xaElWdzQxb2VRYTF2MXZnNEZ2NzR6UGw2L0FoU3J3OVU1cENaRXQ0V2k0d1N0ejZkVFovQ0xBTng4TFpoMUo3UUpWajJmaE10ZlRKcjl3NHozMFoyMDlmT1UwaU9NeStxZHVCbXB2dll1UjdoWkw2RHVwc3pmbncwU2tmdGhzMThkRzlaS2I1OVVodm1hU0daUlZiTlFwc2czQlpsdmlkMGxJS08yZDF4b3pjbE96Z2pYUFlvdkpKSXVsdHprTXUzNHFRYjlTei95aWxyYkNnajg9Il19.eyJub25jZSI6InhiRWlLWG9iMEZwRlhVN3h2dm0yVnVNNTVOYW8xN2Fzd3A0Y3pGZWRHOUE9IiwidGltZXN0YW1wTXMiOjE2NDYxNDIxNzI0NjksImFwa1BhY2thZ2VOYW1lIjoiZGUuY3VsdHVyZTRsaWZlLmx1Y2EuZGV2IiwiYXBrRGlnZXN0U2hhMjU2IjoidzNReDFSTUp5SEtBUkNYUkJ6S3l1dU1WTUpxMUZOVTZtSW9ueWp4M2hBbz0iLCJjdHNQcm9maWxlTWF0Y2giOnRydWUsImFwa0NlcnRpZmljYXRlRGlnZXN0U2hhMjU2IjpbIm5CM2VBZ2VvZUZvR3NFSElRVERTQm1HalVIWTUzazc5L0p2NWU5YVZQNXM9Il0sImJhc2ljSW50ZWdyaXR5Ijp0cnVlLCJldmFsdWF0aW9uVHlwZSI6IkJBU0lDLEhBUkRXQVJFX0JBQ0tFRCJ9.WmP_QRhnAJoWoDJ8bS_t8ofxlPUsR79O8eRhLBSYFonQW140yws5RIQ9ouK4h4ndGY2ZMil9CHm0f1JzuGmZis1QgJW4-eN-vlEZOYCNgdWOt4v-CY7CWrAB3xIavL1ecCpBeQcXOu-9O1Iik6VsASWWz3jzHbvvf9eJxrx679T86YQ8vh8OYXAtH-yPOgNUbyQ5FySQpu27pbCCdvzpVLNe8J9S_0tGsfNBPLwR6NH3jrrNIstYZSUghXYieuKewZ6j2-x7KASJnpX3BVyb9c6Tg1tPTC9GB6U46IOKzZS5TdPaJZh4jF7WLIT8fpNKBK6wyCd6TByKV-rpR8Rysw',
};

const axiosMockAdapter = new AxiosMockAdapter(proxyClient);
const CREATION_TIMESTAMP_JWS = 1646142172469;

const deviceSampleSchema = z.object({
  device: z.string(),
  createdAt: z.number(),
  expectSuccess: z.boolean(),
  payload: z.object({
    nonce: z.string(),
    keyAttestationPublicKey: z.string(),
    keyAttestationCertificates: z.array(z.string()),
    safetyNetAttestationJws: z.string(),
  }),
});

const deviceTestSamples = [
  './test-data/BlackBerry-Q10.json',
  './test-data/Google-Pixel-1.json',
  './test-data/Google-Pixel-2.json',
  './test-data/Google-Pixel-6.json',
  './test-data/HTC-U11.json',
  './test-data/Moto-G100.json',
  './test-data/Samsung-A3.json',
  './test-data/Samsung-S10.json',
].map(sample => {
  const parsedFile = deviceSampleSchema.parse(
    JSON.parse(readFileSync(path.join(__dirname, sample)).toString())
  ) as Required<zod.infer<typeof deviceSampleSchema>>;
  return {
    ...parsedFile,
    payload: parsedFile.payload as Required<typeof parsedFile.payload>,
    keyAttestationCertificatesPKIJS: parsedFile.payload.keyAttestationCertificates.map(
      certBase64 =>
        parsePKIJSCertificateFromBER(Buffer.from(certBase64, 'base64'))
    ),
  };
});

describe('verifyAndroidAttestation()', function () {
  jest.setTimeout(5_000);

  beforeEach(() => {
    mockJWSModifyHeader = null;
    mockJWSModifyPayload = null;
    axiosMockAdapter.onGet(GOOGLE_CRL_URL).reply(200, { entries: {} });
  });
  afterEach(() => {
    axiosMockAdapter.reset();
    mockedAllowedAPKPackageName = null;
  });

  describe('when providing valid key and SafetyNet attestation', function () {
    it('resolves successfully', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      await verifyAndroidAttestation(
        Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
        sampleData.getKeyAttestationCertificates(),
        sampleData.nonce,
        sampleData.safetyNetAttestationJws,
        async () => true
      );
    });
  });

  describe('when ensureValidNonceAndInvalidate() returns false', function () {
    it('throws UNKOWN_NONCE', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => false
        )
      ).rejects.toThrow('UNKNOWN_NONCE');
    });
  });

  describe('when SafetyNet attestation JWS certificate chain is invalid', function () {
    it('throws SAFETY_NET_CERTIFICATE_CHAIN_VERIFICATION', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      mockJWSModifyHeader = header => {
        header.x5c.splice(1);
      };

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('SAFETY_NET_CERTIFICATE_CHAIN_VERIFICATION');
    });
  });

  describe('when SafetyNet attestation JWS leaf certificate host name is not attest.android.com', function () {
    it('throws SAFETY_NET_LEAF_CERTIFICATE_HOST_NAME_NOT_MATCHING', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      mockJWSModifyHeader = header => {
        header.x5c = [
          // We fake the cert chain in the to just contain one of the system root certs. This way,
          // we get past the chain verification but the host name obviously won't match.
          extractBERFromPEM(rootCertificates[0]).toString('base64'),
        ];
      };

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('SAFETY_NET_LEAF_CERTIFICATE_HOST_NAME_NOT_MATCHING');
    });
  });

  describe('when SafetyNet attestation JWS signature cannot be verified', function () {
    const originalSafetyNetAttestationJws = sampleData.safetyNetAttestationJws;
    beforeAll(() => {
      sampleData.safetyNetAttestationJws += 'invalid_siganture_append';
    });
    afterAll(() => {
      sampleData.safetyNetAttestationJws = originalSafetyNetAttestationJws;
    });

    it('throws SAFETY_NET_JWS_SIGNATURE_INVALID', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('SAFETY_NET_JWS_SIGNATURE_INVALID');
    });
  });

  describe('when SafetyNet attestation JWS timestamp is in the future', function () {
    it('throws SAFETY_NET_JWS_AGE_INVALID', async function () {
      const now = new Date(CREATION_TIMESTAMP_JWS);
      useFakeTimers(now);

      mockJWSModifyPayload = payload => {
        payload.timestampMs = now.getTime() + 1;
      };

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('SAFETY_NET_JWS_AGE_INVALID');
    });
  });

  describe('when SafetyNet attestation JWS is too old', function () {
    it('throws SAFETY_NET_JWS_AGE_INVALID', async function () {
      const now = new Date(2022, 2, 1, 14, 42, 53);
      useFakeTimers(now);

      mockJWSModifyPayload = payload => {
        payload.timestampMs = now.getTime() - 5 * 60_000 - 1;
      };

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('SAFETY_NET_JWS_AGE_INVALID');
    });
  });

  describe('when SafetyNet attestation JWS nonce is not matching', function () {
    it('throws SAFETY_NET_NONCE_NOT_MATCHING', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      mockJWSModifyPayload = payload => {
        payload.nonce = 'wrong nonce';
      };

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('SAFETY_NET_NONCE_NOT_MATCHING');
    });
  });

  describe('when SafetyNet attestation JWS package name is not allowed', function () {
    it('throws SAFETY_NET_PACKAGE_NAME_NOT_ALLOWED', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      mockJWSModifyPayload = payload => {
        payload.apkPackageName = 'com.wrong.package';
      };

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('SAFETY_NET_PACKAGE_NAME_NOT_ALLOWED');
    });
  });

  describe('when SafetyNet attestation JWS APK certificate hashes are not allowed', function () {
    it('throws SAFETY_NET_APK_CERTIFICATE_HASHES_NOT_ALLOWED', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      mockJWSModifyPayload = payload => {
        payload.apkCertificateDigestSha256 = ['wrong123Hash456'];
      };

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('SAFETY_NET_APK_CERTIFICATE_HASHES_NOT_ALLOWED');
    });
  });

  describe('when SafetyNet attestation JWS does not list any APK certificate hashes', function () {
    it('throws SAFETY_NET_NO_APK_CERTIFICATE_HASHES_PRESENT', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      mockJWSModifyPayload = payload => {
        payload.apkCertificateDigestSha256 = [];
      };

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('SAFETY_NET_NO_APK_CERTIFICATE_HASHES_PRESENT');
    });
  });

  describe('when SafetyNet attestation JWS ctsProfileMatch is false', function () {
    it('throws SAFETY_NET_CTS_PROFILE_MATCH_FALSE', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      mockJWSModifyPayload = payload => {
        payload.ctsProfileMatch = false;
      };

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('SAFETY_NET_CTS_PROFILE_MATCH_FALSE');
    });
  });

  describe('when SafetyNet attestation JWS basicIntegrity is false', function () {
    it('throws SAFETY_NET_BASIC_INTEGRITY_FALSE', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      mockJWSModifyPayload = payload => {
        payload.basicIntegrity = false;
      };

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('SAFETY_NET_BASIC_INTEGRITY_FALSE');
    });
  });

  describe('when SafetyNet attestation JWS evaluationType is only BASIC', function () {
    it('throws SAFETY_NET_EVALUATION_TYPE_INSUFFICIENT', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      mockJWSModifyPayload = payload => {
        payload.evaluationType = 'BASIC';
      };

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('SAFETY_NET_EVALUATION_TYPE_INSUFFICIENT');
    });
  });

  describe('when key attestation certificate chain cannot be verified', function () {
    it('throws KEY_ATTESTATION_CERTIFICATE_CHAIN_VERIFICATION', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          // Chain without intermediates will make the chain validation fail
          [sampleData.getKeyAttestationCertificates()[0]],
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('KEY_ATTESTATION_CERTIFICATE_CHAIN_VERIFICATION');
    });
  });

  describe('when on of the key attestation certificates is revoked', function () {
    it('throws KEY_ATTESTATION_CERTIFICATE_REVOKED', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      axiosMockAdapter.onGet(GOOGLE_CRL_URL).reply(200, {
        entries: {
          '00a663ab610e69d77169c6b3ae4a71fbec': { status: 'REVOKED' },
        },
      });

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('KEY_ATTESTATION_CERTIFICATE_REVOKED');
    });
  });

  describe("when key attestation leaf certificate's public key is not the same as the public key to be attested", function () {
    it('throws KEY_ATTESTATION_ATTESTED_PUBLIC_KEY_NOT_MATCHING_CERT', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      await expect(
        verifyAndroidAttestation(
          Buffer.from(UNRELATED_PUBLIC_KEY_BASE64, 'base64'),
          sampleData.getKeyAttestationCertificates(),
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow(
        'KEY_ATTESTATION_ATTESTED_PUBLIC_KEY_NOT_MATCHING_CERT'
      );
    });
  });

  describe('when key attestation leaf certificate is missing extension data (custom OID attribute)', function () {
    beforeEach;
    it('throws KEY_ATTESTATION_MALFORMED_EXTENSIONS', async function () {
      useFakeTimers(new Date(CREATION_TIMESTAMP_JWS));

      const certificates = sampleData.getKeyAttestationCertificates();

      certificates[0].asn = new Sequence();
      certificates[0].certificate.extensions = [];

      await expect(
        verifyAndroidAttestation(
          Buffer.from(sampleData.keyAttestationPublicKey, 'base64'),
          certificates,
          sampleData.nonce,
          sampleData.safetyNetAttestationJws,
          async () => true
        )
      ).rejects.toThrow('KEY_ATTESTATION_MALFORMED_EXTENSIONS');
    });
  });

  describe("when verifying attestation for real devices' attestation samples", () => {
    deviceTestSamples.forEach(sample => {
      describe(`device ${sample.device}`, () => {
        beforeEach(() => {
          mockedAllowedAPKPackageName = 'de.culture4life.luca.p2';
        });

        // eslint-disable-next-line jest/valid-title
        it(sample.expectSuccess ? 'resolves' : 'rejects', async () => {
          useFakeTimers(new Date(sample.createdAt));

          const certificates = [...sample.keyAttestationCertificatesPKIJS];

          certificates[0].certificate.extensions = [];

          const promise = verifyAndroidAttestation(
            Buffer.from(sample.payload.keyAttestationPublicKey, 'base64'),
            certificates,
            sample.payload.nonce,
            sample.payload.safetyNetAttestationJws,
            async () => true
          );

          if (sample.expectSuccess) {
            // eslint-disable-next-line jest/no-conditional-expect
            return expect(promise).resolves.not.toThrow();
          }

          return expect(promise).rejects.toThrow();
        });
      });
    });
  });
});
