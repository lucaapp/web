import { createHash, createPublicKey, createVerify } from 'crypto';
import { Logger } from 'pino';
import defaulLogger from 'utils/logger';
import { AndroidAssertionError, AndroidAssertionErrorReason } from './errors';

/**
 * Asserts that the provided data proves that the sending device is one that already passed the
 * key attestation and SafetyNet attestation.
 *
 * @param param0.attestedSPKIPublicKey
 *    This should be a public key as stored in our DB after succeeding the key attestation and
 *    SafetyNet attestation.
 * @param param0.nonce
 * @param param0.clientData Additional data that was signed as well. It's expected that the signed
 *    payload was the bytes of the nonce + the bytes of the clientData.
 * @param param0.signature
 *    The signature as given by the asserting Android client. This will be checked to match the
 *    signature of the given nonce after hashing (!) using the attested public key from our DB.
 * @param param0.ensureValidNonceAndInvalidate
 */
export const verifyAndroidAssertion = async ({
  publicKey,
  nonce,
  clientData,
  signature,
  ensureValidNonceAndInvalidate,
  logger: passedLogger = defaulLogger,
}: {
  publicKey: Buffer;
  nonce: string;
  clientData: Buffer;
  signature: Buffer;
  ensureValidNonceAndInvalidate: (nonce: string) => Promise<boolean>;
  logger: Logger;
}): Promise<void> => {
  const logger = passedLogger.child({
    assertionData: {
      publicKey,
      nonce,
      signature,
    },
  });

  try {
    logger.info(`Verifying Android assertion`);

    // Verify nonce

    const wasNonceValid = await ensureValidNonceAndInvalidate(nonce);

    if (!wasNonceValid) {
      const error = new AndroidAssertionError(
        AndroidAssertionErrorReason.UNKNOWN_NONCE
      );
      logger.error(error, `Nonce was ${nonce}`);
      throw error;
    }

    // Verify signature

    const nodePublicKey = createPublicKey({
      type: 'spki',
      format: 'der',
      key: publicKey,
    });
    const hashedNonce = createHash('sha256')
      .update(nonce)
      .update(clientData)
      .digest();

    const verify = createVerify('SHA256');
    verify.update(hashedNonce);
    verify.end();
    const isSignatureValid = verify.verify({ key: nodePublicKey }, signature);

    if (!isSignatureValid) {
      const error = new AndroidAssertionError(
        AndroidAssertionErrorReason.SIGNATURE_INVALID
      );
      logger.error(
        error,
        `Signature was ${signature.toString(
          'hex'
        )}, public key was ${publicKey.toString('base64')}`
      );
      throw error;
    }
  } catch (error) {
    logger.error({
      err: error,
    });
    throw error;
  }
};
