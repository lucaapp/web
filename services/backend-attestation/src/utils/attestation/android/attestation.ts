import { createHash } from 'crypto';
import { Logger } from 'pino';
import { PKIJSCertificateTuple } from 'utils/certificates';
import defaultLogger from 'utils/logger';

import {
  AndroidAttestationError,
  AndroidAttestationErrorReason,
} from './errors';
import { verifyAndroidKeyAttestation } from './keyAttestation';
import { verifySafetyNetAttestation } from './safetyNetAttestation';

const buildNoncesFromBaseNonce = (
  baseNonce: string
): { keyAttestationNonce: Buffer; safetyNetNonce: Buffer } => {
  const baseNonceBuffer = Buffer.from(baseNonce);
  return {
    safetyNetNonce: createHash('sha256')
      .update(Buffer.concat([baseNonceBuffer, Buffer.from([1])]))
      .digest(),
    keyAttestationNonce: createHash('sha256')
      .update(Buffer.concat([baseNonceBuffer, Buffer.from([2])]))
      .digest(),
  };
};

/**
 * When this resolves without error, the passed public key is ensured to belong to an attested,
 * secure key pair (key attestation) performed by a device that is asserted via SafetyNet.
 *
 * @param keyAttestationSPKIPublicKey
 * @param keyAttestationCertificatesSPKI
 * @param baseNonce
 * @param safetyNetAttestationJws
 * @param ensureValidNonceAndInvalidate
 */
export const verifyAndroidAttestation = async (
  keyAttestationSPKIPublicKey: Buffer,
  keyAttestationCertificates: PKIJSCertificateTuple[],
  baseNonce: string,
  safetyNetAttestationJws: string,
  ensureValidNonceAndInvalidate: (nonce: string) => Promise<boolean>,
  passedLogger: Logger = defaultLogger
): Promise<void> => {
  const logger = passedLogger.child({
    attestationData: {
      keyAttestationSPKIPublicKey,
      keyAttestationCertificates: keyAttestationCertificates.map(a =>
        Buffer.from(a.asn.valueBeforeDecode).toString('base64')
      ),
      baseNonce,
      safetyNetAttestationJws,
    },
  });

  try {
    logger.info(`Verifying Android attestation`);

    const wasNonceValid = await ensureValidNonceAndInvalidate(baseNonce);

    if (!wasNonceValid) {
      const error = new AndroidAttestationError(
        AndroidAttestationErrorReason.UNKNOWN_NONCE
      );
      logger.error(error, `Nonce was ${baseNonce}`);
      throw error;
    }

    const { keyAttestationNonce, safetyNetNonce } = buildNoncesFromBaseNonce(
      baseNonce
    );

    // IMPORTANT: mind the order of the calls here!
    // We want to avoid handling data from the key attestation before we verified the SafetyNet
    // attestation and know thast we can trust the sending device/app.
    await verifySafetyNetAttestation(safetyNetAttestationJws, safetyNetNonce);

    await verifyAndroidKeyAttestation(
      keyAttestationSPKIPublicKey,
      keyAttestationCertificates,
      keyAttestationNonce,
      logger
    );
  } catch (error) {
    logger.error({
      err: error,
    });
    throw error;
  }
};
