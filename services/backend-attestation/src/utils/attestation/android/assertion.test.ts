import { useFakeTimers } from 'sinon';
import logger from '../../logger';
import { verifyAndroidAssertion } from './assertion';

const sampleData = {
  nonce: 'AeqMZ4+/MQFoNk9+hi3RfsimfrViC9OZ2CLvogpPEGo=',
  signature:
    'MEUCIFiOkX6r4Gb+WTOvbv6Aa5vv3GHtWn2qoLnWqvmPuwH/AiEAlIG4fj8GxWQFASM112XelpskP5KKXolrlb7WV5hjiPU=',
  attestedPublicKey:
    'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEmZ39tyxaCy3KVceO+EuC5zUjVOJrVGnQ4RPUF6PmlQ/hluxQKeIR183cdyVasQ9lHlPJg9BgLjy4OA5OPAHKwg==',
};

describe('verifyAndroidAssertion()', function () {
  jest.setTimeout(5_000);

  describe('when providing valid assertion, nonce and public key', function () {
    it('resolves successfully', async function () {
      useFakeTimers(new Date(2022, 2, 1, 13, 42, 53));

      await verifyAndroidAssertion({
        publicKey: Buffer.from(sampleData.attestedPublicKey, 'base64'),
        nonce: sampleData.nonce,
        clientData: Buffer.alloc(0),
        signature: Buffer.from(sampleData.signature, 'base64'),
        ensureValidNonceAndInvalidate: async () => true,
        logger,
      });
    });
  });

  describe('when nonce validation returns false', function () {
    it('throws with UNKNOWN_NONCE', async function () {
      useFakeTimers(new Date(2022, 2, 1, 13, 42, 53));

      await expect(
        verifyAndroidAssertion({
          publicKey: Buffer.from(sampleData.attestedPublicKey, 'base64'),
          nonce: `wrong ${sampleData.nonce}`,
          clientData: Buffer.alloc(0),
          signature: Buffer.from(sampleData.signature, 'base64'),
          ensureValidNonceAndInvalidate: async () => false,
          logger,
        })
      ).rejects.toThrow('UNKNOWN_NONCE');
    });
  });

  describe('when signature does not match', function () {
    it('throws with SIGNATURE_INVALID', async function () {
      useFakeTimers(new Date(2022, 2, 1, 13, 42, 53));

      await expect(
        verifyAndroidAssertion({
          publicKey: Buffer.from(sampleData.attestedPublicKey, 'base64'),
          nonce: sampleData.nonce,
          clientData: Buffer.alloc(0),
          signature: Buffer.concat([
            Buffer.from([0]),
            Buffer.from(sampleData.signature, 'base64').slice(1),
          ]),
          ensureValidNonceAndInvalidate: async () => true,
          logger,
        })
      ).rejects.toThrow('SIGNATURE_INVALID');
    });
  });

  describe("when passing additional client data which wasn't originally signed", function () {
    it('throws with SIGNATURE_INVALID', async function () {
      useFakeTimers(new Date(2022, 2, 1, 13, 42, 53));

      await expect(
        verifyAndroidAssertion({
          publicKey: Buffer.from(sampleData.attestedPublicKey, 'base64'),
          nonce: sampleData.nonce,
          clientData: Buffer.from([1]),
          signature: Buffer.from(sampleData.signature, 'base64'),
          ensureValidNonceAndInvalidate: async () => true,
          logger,
        })
      ).rejects.toThrow('SIGNATURE_INVALID');
    });
  });
});
