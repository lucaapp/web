import config from 'config';
import { DeviceAttestationPlatform } from 'database/models/deviceAttestation';
import { SignJWT } from 'jose';
import { getCurrentKeyPair } from 'utils/jwkStore';

const JWT_ISSUER = 'luca-attestation';
const JWT_EXPIRATION = config.get('jwt.expiration');

export const generateDeviceAssertionJWT = async ({
  platform,
  deviceId,
}: {
  platform: DeviceAttestationPlatform;
  deviceId: string;
}): Promise<string> => {
  const keyPair = await getCurrentKeyPair();

  return new SignJWT({ os: platform })
    .setProtectedHeader({
      alg: 'ES256',
      typ: 'JWT',
      kid: keyPair.keyIdentifier,
    })
    .setSubject(deviceId)
    .setIssuer(JWT_ISSUER)
    .setIssuedAt()
    .setNotBefore('0s')
    .setExpirationTime(JWT_EXPIRATION)
    .sign(keyPair.privateKey);
};
