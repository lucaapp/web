import { createHash } from 'crypto';
import { readFileSync } from 'fs';
import yamlJS from 'js-yaml';
import path from 'path';
import { useFakeTimers } from 'sinon';
import { z } from 'zod';
import { extractBERFromPEM } from '../../certificates';
import { parseIOSAssertion, verifyIOSAssertion } from './assertion';
import { IOS_ALLOWED_APP_ID_HASHES } from './config';

const devicecheckAppttestTestData = yamlJS.loadAll(
  readFileSync(
    path.join(__dirname, 'devicecheck-appattestios-14.4.yaml')
  ).toString()
);

const devicecheckAppttestTestAssertionSchema = z.object({
  assertionBase64: z.string(),
  bundleIdentifier: z.string(),
  clientDataBase64: z.string(),
  counter: z.number(),
  publicKey: z.string(),
  teamIdentifier: z.string(),
});

const devicecheckAppttestAssertion = devicecheckAppttestTestAssertionSchema.parse(
  devicecheckAppttestTestData[1]
);

const sampleData = {
  // Taken from https://github.com/veehaitch/devicecheck-appattest/blob/367cc186301350689d59c0ed41f095d01b36268f/src/test/resources/ios-14.4.yaml
  devicecheck_appatest_ios14_4: {
    clientDataBase64: devicecheckAppttestAssertion.clientDataBase64,
    assertionCBOR: devicecheckAppttestAssertion.assertionBase64,
    publicKeySPKIBase64: extractBERFromPEM(
      devicecheckAppttestAssertion.publicKey
    ).toString('base64'),
    appId: `${devicecheckAppttestAssertion.teamIdentifier}.${devicecheckAppttestAssertion.bundleIdentifier}`,
  },
  ownApp: {
    clientDataBase64: '7wIi29kgMQ0ttVGWPL3GnOZMJmoQEUHrBP5+UIOE1Ik=',
    assertionCBOR:
      'omlzaWduYXR1cmVYRjBEAiBpPXM5fk0AKD4JHV9hOaiImjXxbOB6tfUY0lQLjyhuqAIgQXG1ySk/oLGjem+tncYrkoecrmbALDmzKEpJu7dt2sZxYXV0aGVudGljYXRvckRhdGFYJe1fROqR+Y0hQ93GXwREXXBK5vUSWgvJsgWQONj3MINTQAAAAAE=',
    publicKeySPKIBase64:
      'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEAj2+7ATk/S+qvC2AlYAe7X1MPykmWpUxYN66hi7KRLCAguSyPZ2+HAo+RSnW0snbfUSjjM1yfL0qTYmCME/LNg==',
    appId: '74643229Y4.de.culture4life.luca.p2',
  },
};

const successfulEnsureValidNonceAndInvalidate = () => Promise.resolve(true);

function setIOSAllowedAppIds(allowedAppIds: string[]) {
  IOS_ALLOWED_APP_ID_HASHES.push(
    ...allowedAppIds.map(allowedAppId =>
      createHash('sha256').update(allowedAppId).digest()
    )
  );
}

describe('verifyIOSAssertion', function () {
  beforeEach(() => {
    IOS_ALLOWED_APP_ID_HASHES.splice(0);
  });

  describe('when providing valid sample from devicecheck-appattest for iOS 14.4', function () {
    it('resolves successfully', async function () {
      useFakeTimers(new Date(2022, 1, 17));
      const parsedAssertion = await parseIOSAssertion(
        Buffer.from(
          sampleData.devicecheck_appatest_ios14_4.assertionCBOR,
          'base64'
        )
      );
      setIOSAllowedAppIds([sampleData.devicecheck_appatest_ios14_4.appId]);

      await verifyIOSAssertion(
        parsedAssertion,
        Buffer.from(
          sampleData.devicecheck_appatest_ios14_4.publicKeySPKIBase64,
          'base64'
        ),
        '',
        Buffer.from(
          sampleData.devicecheck_appatest_ios14_4.clientDataBase64,
          'base64'
        ),
        0,
        successfulEnsureValidNonceAndInvalidate
      );
    });
  });

  describe('when providing valid sample from own iOS app', function () {
    it('resolves successfully', async function () {
      useFakeTimers(new Date(2022, 1, 17));
      const parsedAssertion = await parseIOSAssertion(
        Buffer.from(sampleData.ownApp.assertionCBOR, 'base64')
      );
      setIOSAllowedAppIds([sampleData.ownApp.appId]);

      await verifyIOSAssertion(
        parsedAssertion,
        Buffer.from(sampleData.ownApp.publicKeySPKIBase64, 'base64'),
        'abcde',
        Buffer.alloc(0),
        0,
        successfulEnsureValidNonceAndInvalidate
      );
    });
  });

  describe("when the assertion's signature cannot be verified", function () {
    it('throws SIGNATURE_INVALID', async function () {
      useFakeTimers(new Date(2022, 1, 17));
      const parsedAssertion = await parseIOSAssertion(
        Buffer.from(sampleData.ownApp.assertionCBOR, 'base64')
      );
      setIOSAllowedAppIds([sampleData.ownApp.appId]);

      parsedAssertion.signature[0] += 1;

      await expect(
        verifyIOSAssertion(
          parsedAssertion,
          Buffer.from(sampleData.ownApp.publicKeySPKIBase64, 'base64'),
          'abcde',
          Buffer.alloc(0),
          0,
          successfulEnsureValidNonceAndInvalidate
        )
      ).rejects.toThrowError('SIGNATURE_INVALID');
    });
  });

  describe("when the assertion's RP ID does not match our expected app ID", function () {
    it('throws APP_ID_HASH_NOT_MATCHING_RPID', async function () {
      useFakeTimers(new Date(2022, 1, 17));
      const parsedAssertion = await parseIOSAssertion(
        Buffer.from(sampleData.ownApp.assertionCBOR, 'base64')
      );

      setIOSAllowedAppIds(['TEAM123.different.app.id']);

      await expect(
        verifyIOSAssertion(
          parsedAssertion,
          Buffer.from(sampleData.ownApp.publicKeySPKIBase64, 'base64'),
          'abcde',
          Buffer.alloc(0),
          0,
          successfulEnsureValidNonceAndInvalidate
        )
      ).rejects.toThrowError('APP_ID_HASH_NOT_MATCHING_RPID');
    });
  });

  describe('when the attestion signCount is smaller than the last known signing counter', function () {
    it('throws INVALID_SIGN_COUNT', async function () {
      useFakeTimers(new Date(2022, 1, 17));
      const parsedAssertion = await parseIOSAssertion(
        Buffer.from(sampleData.ownApp.assertionCBOR, 'base64')
      );

      setIOSAllowedAppIds([sampleData.ownApp.appId]);

      await expect(
        verifyIOSAssertion(
          parsedAssertion,
          Buffer.from(sampleData.ownApp.publicKeySPKIBase64, 'base64'),
          'abcde',
          Buffer.alloc(0),
          2,
          successfulEnsureValidNonceAndInvalidate
        )
      ).rejects.toThrowError('INVALID_SIGN_COUNT');
    });
  });
});
