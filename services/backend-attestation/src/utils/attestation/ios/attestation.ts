import { decodeFirst } from 'cbor';
import { asn1, util } from 'node-forge';
import crypto from 'isomorphic-webcrypto';
import { z } from 'zod';
import { createHash } from 'crypto';

import defaultLogger from 'utils/logger';
import { Logger } from 'pino';
import {
  parsePKIJSCertificateFromBER,
  verifyCertificateChain,
} from '../../certificates';
import {
  findASN1NodeFollowingOID,
  findPublicKeyBitStringInX509ASN1,
  getOctetStringNodeBuffer,
} from '../../nodeForgeAsn1';
import {
  ALLOWED_AAGUID,
  IOS_ALLOWED_APP_ID_HASHES,
  IOS_ALLOWED_APP_IDS,
  getAppleAppAttestationRootCAPKIJS,
} from './config';
import { TIOSAttestationAuthData } from './authData';
import { IOSAttestationError, IOSAttestationErrorReason } from './errors';
import { verifyIOSAttestationReceipt } from './receipt';

// Note that this OID, while used by Apple, isn't officially documented. Examplary mention of this
// issues can be found here: https://bugs.webkit.org/show_bug.cgi?id=217935
const OID_APPLE_ATTESTATION_NONCE = '1.2.840.113635.100.8.2';

/**
 * This is the format of the CBOR-encoded attestation object
 */
const iosDeviceAttestationSchema = z.object({
  fmt: z.literal('apple-appattest'),
  attStmt: z.object({
    x5c: z.array(z.instanceof(Buffer)),
    receipt: z.instanceof(Buffer),
  }),
  authData: z.instanceof(Buffer),
});

export type TIOSAttestation = ReturnType<
  typeof iosDeviceAttestationSchema.parse
>;

export const parseIOSAttestation = async (
  attestation: Buffer
): Promise<TIOSAttestation> => {
  return iosDeviceAttestationSchema.parse(await decodeFirst(attestation));
};

const findAppleAttestationNonceInCredCert = (credCertASN1: asn1.Asn1) => {
  const oidValueAppleNonceASN1 = findASN1NodeFollowingOID(
    credCertASN1,
    OID_APPLE_ATTESTATION_NONCE
  );

  if (!oidValueAppleNonceASN1) {
    throw new Error(
      `Could not find value for OID ${OID_APPLE_ATTESTATION_NONCE} in attestation credCert`
    );
  }

  // The OID value we are interested in is a 32-byte octet string. However, the actual value in the
  // ASN.1 has the following structure: SEQUENCE(OCTETSTRING(SEQUENCE( ➡️ OCTETSTRING ⬅ )))
  // Unfortunately, for some reason, node-forge doesn't parse the tree any further.
  // Therefore, we just pick the last 32 bytes of the entire branch's buffer.
  const oidValueAppleNonceBuffer = getOctetStringNodeBuffer(
    oidValueAppleNonceASN1.value[0]
  );

  return oidValueAppleNonceBuffer.slice(-32, oidValueAppleNonceBuffer.length);
};

/** This process is based on the official docs from Apple to be found here: https://developer.apple.com/documentation/devicecheck/validating_apps_that_connect_to_your_server
 */
const rawVerifyIOSAttestation = async (
  parsedAttestation: TIOSAttestation,
  authData: TIOSAttestationAuthData,
  nonce: string,
  keyIdentifier: Buffer,
  ensureValidNonceAndInvalidate: (nonce: string) => Promise<boolean>,
  logger: Logger
): Promise<{ spkiPublicKey: Buffer; receipt: Buffer }> => {
  const wasNonceValid = await ensureValidNonceAndInvalidate(nonce);
  if (!wasNonceValid) {
    const error = new IOSAttestationError(
      IOSAttestationErrorReason.UNKNOWN_NONCE
    );
    logger.error(error, `Nonce was ${nonce}`);
    throw error;
  }

  const [credCert, ...intermediateCerts] = parsedAttestation.attStmt.x5c.map(
    certificateBuffer =>
      parsePKIJSCertificateFromBER(certificateBuffer).certificate
  );
  const credCertBuffer = parsedAttestation.attStmt.x5c[0];

  const { rpIdHash, signCount, attestedCredentialData } = authData;

  // Step 1 (according to Apple docs)
  try {
    await verifyCertificateChain({
      leafCertificate: credCert,
      intermediateCertificates: intermediateCerts,
      trustedCertificates: [getAppleAppAttestationRootCAPKIJS()],
    });
  } catch (pkijsError) {
    const mappedError = new IOSAttestationError(
      IOSAttestationErrorReason.CERTIFICATE_CHAIN_VERIFICATION
    );
    if (pkijsError instanceof Error) {
      logger.error(pkijsError);
    }
    logger.error(mappedError, `Nonce was ${nonce}`);
    throw mappedError;
  }

  // Step 2 (according to Apple docs)
  const clientDataHash = createHash('sha256').update(nonce).digest();
  const authAndNonceConcatResult = Buffer.concat([
    parsedAttestation.authData,
    clientDataHash,
  ]);

  // Step 3 (according to Apple docs)
  const authAndNonceHash = createHash('sha256')
    .update(authAndNonceConcatResult)
    .digest();

  // Step 4 (according to Apple docs)
  const credCertASN1 = asn1.fromDer(util.createBuffer(credCertBuffer));
  const authAndNonceHashFromCert = findAppleAttestationNonceInCredCert(
    credCertASN1
  );
  if (!authAndNonceHashFromCert.equals(authAndNonceHash)) {
    const error = new IOSAttestationError(
      IOSAttestationErrorReason.AUTH_DATE_NONCE_OID
    );
    logger.error(
      error,
      `Cred cert nonce was ${authAndNonceHashFromCert.toString(
        'hex'
      )} but expected was ${authAndNonceHash.toString('hex')}`
    );
    throw error;
  }

  // Step 5 (according to Apple docs)
  const publicKeyBytes = findPublicKeyBitStringInX509ASN1(credCertASN1);
  const publicKeyHash = createHash('sha256').update(publicKeyBytes).digest();
  if (!keyIdentifier.equals(publicKeyHash)) {
    const error = new IOSAttestationError(
      IOSAttestationErrorReason.KEY_IDENTIFIER_FINGERPRINT_EQUALITY
    );
    logger.error(
      error,
      `Expected public key hash ${publicKeyHash.toString(
        'base64'
      )} to match key identified ${keyIdentifier.toString('base64')}`
    );
    throw error;
  }

  // Step 6 (according to Apple docs)
  const anyAllowedAppIdHashMatches = IOS_ALLOWED_APP_ID_HASHES.some(
    allowedAppIDHash => allowedAppIDHash.equals(rpIdHash)
  );

  if (!anyAllowedAppIdHashMatches) {
    const error = new IOSAttestationError(
      IOSAttestationErrorReason.APP_ID_HASH_NOT_MATCHING_RPID
    );
    logger.error(
      error,
      `RP ID hash was ${rpIdHash} but allowed hashes were ${IOS_ALLOWED_APP_ID_HASHES}, based on allowed app IDs ${IOS_ALLOWED_APP_IDS}`
    );
    throw error;
  }

  // Step 7 (according to Apple docs)
  if (signCount > 0) {
    const error = new IOSAttestationError(
      IOSAttestationErrorReason.SIGN_COUNT_GREATER_ZERO
    );
    logger.error(error, `Expected sign count to be zero but was ${signCount}`);
    throw error;
  }

  // Step 8 (according to Apple docs)
  if (!attestedCredentialData.aaguid.equals(ALLOWED_AAGUID)) {
    const error = new IOSAttestationError(
      IOSAttestationErrorReason.AAGUID_FORBIDDEN_OR_INVALID
    );
    logger.error(
      error,
      `Expected aaguid to be ${ALLOWED_AAGUID.toString(
        'utf-8'
      )} but received ${attestedCredentialData.aaguid.toString('utf-8')}`
    );
    throw error;
  }

  // Step 9 (according to Apple docs)
  if (!keyIdentifier.equals(attestedCredentialData.credentialId)) {
    const error = new IOSAttestationError(
      IOSAttestationErrorReason.KEY_IDENTIFIER_NOT_MATCHING_CREDENTIAL_ID
    );
    logger.error(
      error,
      `Expected key identifier ${keyIdentifier.toString(
        'base64'
      )} to match ${attestedCredentialData.credentialId.toString('base64')}`
    );
    throw error;
  }

  await verifyIOSAttestationReceipt(
    parsedAttestation.attStmt.receipt,
    publicKeyBytes
  );

  logger.info('iOS device attestation successful');

  const spkiPublicKey = await crypto.subtle.exportKey(
    'spki',
    await credCert.getPublicKey()
  );
  return {
    receipt: parsedAttestation.attStmt.receipt,
    spkiPublicKey: Buffer.from(spkiPublicKey),
  };
};

export const verifyIOSAttestation = async (
  parsedAttestation: TIOSAttestation,
  authData: TIOSAttestationAuthData,
  nonce: string,
  keyIdentifier: Buffer,
  ensureValidNonceAndInvalidate: (nonce: string) => Promise<boolean>,
  passedLogger: Logger = defaultLogger
): Promise<{ spkiPublicKey: Buffer; receipt: Buffer }> => {
  const logger = passedLogger.child({
    attestationData: {
      parsedAttestation,
      authData,
      nonce,
      keyIdentifier,
    },
  });

  try {
    logger.info(`Verifying iOS attestation`);

    return await rawVerifyIOSAttestation(
      parsedAttestation,
      authData,
      nonce,
      keyIdentifier,
      ensureValidNonceAndInvalidate,
      logger
    );
  } catch (error) {
    logger.error({
      err: error,
    });
    throw error;
  }
};
