/* eslint-disable max-classes-per-file */

// Receipt errors

export enum IOSReceiptErrorReason {
  APP_ID_HASH_NOT_MATCHING = 'APP_ID_HASH_NOT_MATCHING',
  RECEIPT_TOO_OLD = 'RECEIPT_TOO_OLD',
  PUBLIC_KEY_NOT_MATCHING = 'PUBLIC_KEY_NOT_MATCHING',
}

// Attestation errors

export enum IOSAttestationErrorReason {
  UNKNOWN_NONCE = 'UNKNOWN_NONCE',
  CERTIFICATE_CHAIN_VERIFICATION = 'CERTIFICATE_CHAIN_VERIFICATION',
  AUTH_DATE_NONCE_OID = 'AUTH_DATE_NONCE_OID',
  KEY_IDENTIFIER_FINGERPRINT_EQUALITY = 'KEY_IDENTIFIER_FINGERPRINT_EQUALITY',
  ATTESTED_CREDENTIAL_DATA_MISSING_BY_FLAG = 'ATTESTED_CREDENTIAL_DATA_MISSING_BY_FLAG',
  ATTESTED_CREDENTIAL_DATA_MISSING_BY_PARSE = 'ATTESTED_CREDENTIAL_DATA_MISSING_BY_PARSE',
  EXTENSION_DATA_MISSING = 'EXTENSION_DATA_MISSING',
  KEY_IDENTIFIER_NOT_MATCHING_CREDENTIAL_ID = 'KEY_IDENTIFIER_NOT_MATCHING_CREDENTIAL_ID',
  SIGN_COUNT_GREATER_ZERO = 'SIGN_COUNT_GREATER_ZERO',
  AAGUID_FORBIDDEN_OR_INVALID = 'AAGUID_FORBIDDEN_OR_INVALID',
  APP_ID_HASH_NOT_MATCHING_RPID = 'APP_ID_HASH_NOT_MATCHING_RPID',
}

export class IOSAttestationError extends Error {
  reason: IOSAttestationErrorReason | IOSReceiptErrorReason;

  constructor(reason: IOSAttestationErrorReason | IOSReceiptErrorReason) {
    super(`iOS device attestation failed. Reason: ${reason}`);
    this.reason = reason;
  }
}

// Assertion errors

export enum IOSAssertionErrorReason {
  SIGNATURE_INVALID = 'SIGNATURE_INVALID',
  APP_ID_HASH_NOT_MATCHING_RPID = 'APP_ID_HASH_NOT_MATCHING_RPID',
  INVALID_SIGN_COUNT = 'INVALID_SIGN_COUNT',
  UNKNOWN_NONCE = 'UNKNOWN_NONCE',
}

export class IOSAssertionError extends Error {
  reason: IOSAssertionErrorReason;

  constructor(reason: IOSAssertionErrorReason) {
    super(`iOS device assertion failed. Reason: ${reason}`);
    this.reason = reason;
  }
}
