import config from 'config';
import { createHash } from 'crypto';
import Certificate from 'pkijs/build/Certificate';
import { parsePKIJSCertificateFromPEM } from '../../certificates';

export const IOS_ALLOWED_APP_IDS = [
  config.get('iosAttestation.allowedAppIds.consumerApp'),
  config.get('iosAttestation.allowedAppIds.operatorApp'),
];

export const IOS_ALLOWED_APP_ID_HASHES = IOS_ALLOWED_APP_IDS.map(allowedAppId =>
  createHash('sha256').update(allowedAppId).digest()
);

const ALLOWED_AAGUID_PRODUCTION = Buffer.concat([
  Buffer.from('appattest', 'utf-8'),
  Buffer.from([0, 0, 0, 0, 0, 0, 0]),
]);
const ALLOWED_AAGUID_DEVELOPMENT = Buffer.from('appattestdevelop', 'utf-8');
const attestationAllowedAaguidType = config.get(
  'iosAttestation.allowedAaguidType'
);
export const ALLOWED_AAGUID =
  attestationAllowedAaguidType === 'production'
    ? ALLOWED_AAGUID_PRODUCTION
    : ALLOWED_AAGUID_DEVELOPMENT;

export const APPLE_APP_ATTESTATION_ROOT_CA_PEM = `-----BEGIN CERTIFICATE-----
MIICITCCAaegAwIBAgIQC/O+DvHN0uD7jG5yH2IXmDAKBggqhkjOPQQDAzBSMSYw
JAYDVQQDDB1BcHBsZSBBcHAgQXR0ZXN0YXRpb24gUm9vdCBDQTETMBEGA1UECgwK
QXBwbGUgSW5jLjETMBEGA1UECAwKQ2FsaWZvcm5pYTAeFw0yMDAzMTgxODMyNTNa
Fw00NTAzMTUwMDAwMDBaMFIxJjAkBgNVBAMMHUFwcGxlIEFwcCBBdHRlc3RhdGlv
biBSb290IENBMRMwEQYDVQQKDApBcHBsZSBJbmMuMRMwEQYDVQQIDApDYWxpZm9y
bmlhMHYwEAYHKoZIzj0CAQYFK4EEACIDYgAERTHhmLW07ATaFQIEVwTtT4dyctdh
NbJhFs/Ii2FdCgAHGbpphY3+d8qjuDngIN3WVhQUBHAoMeQ/cLiP1sOUtgjqK9au
Yen1mMEvRq9Sk3Jm5X8U62H+xTD3FE9TgS41o0IwQDAPBgNVHRMBAf8EBTADAQH/
MB0GA1UdDgQWBBSskRBTM72+aEH/pwyp5frq5eWKoTAOBgNVHQ8BAf8EBAMCAQYw
CgYIKoZIzj0EAwMDaAAwZQIwQgFGnByvsiVbpTKwSga0kP0e8EeDS4+sQmTvb7vn
53O5+FRXgeLhpJ06ysC5PrOyAjEAp5U4xDgEgllF7En3VcE3iexZZtKeYnpqtijV
oyFraWVIyd/dganmrduC1bmTBGwD
-----END CERTIFICATE-----`;

let appleAppAttestationRootCAPKIJS: Certificate | null = null;
export const getAppleAppAttestationRootCAPKIJS = (): Certificate => {
  if (!appleAppAttestationRootCAPKIJS) {
    appleAppAttestationRootCAPKIJS = parsePKIJSCertificateFromPEM(
      APPLE_APP_ATTESTATION_ROOT_CA_PEM
    ).certificate;
  }

  return appleAppAttestationRootCAPKIJS;
};

const APPLE_PUBLIC_ROOT_CA_G3 = `-----BEGIN CERTIFICATE-----
MIICQzCCAcmgAwIBAgIILcX8iNLFS5UwCgYIKoZIzj0EAwMwZzEbMBkGA1UEAwwS
QXBwbGUgUm9vdCBDQSAtIEczMSYwJAYDVQQLDB1BcHBsZSBDZXJ0aWZpY2F0aW9u
IEF1dGhvcml0eTETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwHhcN
MTQwNDMwMTgxOTA2WhcNMzkwNDMwMTgxOTA2WjBnMRswGQYDVQQDDBJBcHBsZSBS
b290IENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9y
aXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzB2MBAGByqGSM49
AgEGBSuBBAAiA2IABJjpLz1AcqTtkyJygRMc3RCV8cWjTnHcFBbZDuWmBSp3ZHtf
TjjTuxxEtX/1H7YyYl3J6YRbTzBPEVoA/VhYDKX1DyxNB0cTddqXl5dvMVztK517
IDvYuVTZXpmkOlEKMaNCMEAwHQYDVR0OBBYEFLuw3qFYM4iapIqZ3r6966/ayySr
MA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgEGMAoGCCqGSM49BAMDA2gA
MGUCMQCD6cHEFl4aXTQY2e3v9GwOAEZLuN+yRhHFD/3meoyhpmvOwgPUnPWTxnS4
at+qIxUCMG1mihDK1A3UT82NQz60imOlM27jbdoXt2QfyFMm+YhidDkLF1vLUagM
6BgD56KyKA==
-----END CERTIFICATE-----
`;

let applePublicRootCAG3PKIJS: Certificate | null = null;
export const getApplePublicRootCAG3PKIJS = (): Certificate => {
  if (!applePublicRootCAG3PKIJS) {
    applePublicRootCAG3PKIJS = parsePKIJSCertificateFromPEM(
      APPLE_PUBLIC_ROOT_CA_G3
    ).certificate;
  }

  return applePublicRootCAG3PKIJS;
};
