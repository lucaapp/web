import SignedData from 'pkijs/build/SignedData';
import ContentInfo from 'pkijs/build/ContentInfo';
import {
  fromBER,
  Integer,
  LocalBaseBlock,
  OctetString,
  Sequence,
  Set,
} from 'asn1js';

import { createHash } from 'crypto';
import { asn1, util } from 'node-forge';
import { typedArrayToBuffer } from '../../bigEndian';
import {
  getApplePublicRootCAG3PKIJS,
  IOS_ALLOWED_APP_ID_HASHES,
  IOS_ALLOWED_APP_IDS,
} from './config';
import logger from '../../logger';
import { IOSAttestationError, IOSReceiptErrorReason } from './errors';
import { findPublicKeyBitStringInX509ASN1 } from '../../nodeForgeAsn1';
import { assertASN1Type } from '../../asn1js';

const receiptFieldParsers = {
  buffer: (valueItem: LocalBaseBlock) => {
    assertASN1Type(OctetString, valueItem);
    const { valueHex } = valueItem.valueBlock;
    return Buffer.from(valueHex);
  },
  utf8: (valueItem: LocalBaseBlock) => {
    assertASN1Type(OctetString, valueItem);
    const { valueHex } = valueItem.valueBlock;
    return Buffer.from(valueHex).toString('utf-8');
  },
  date: (valueItem: LocalBaseBlock) => {
    assertASN1Type(OctetString, valueItem);
    const { valueHex } = valueItem.valueBlock;
    return new Date(Buffer.from(valueHex).toString('utf-8'));
  },
  receiptType: (valueItem: LocalBaseBlock): 'RECEIPT' | 'ATTEST' => {
    assertASN1Type(OctetString, valueItem);
    const { valueHex } = valueItem.valueBlock;
    const rawReceiptType = Buffer.from(valueHex).toString('utf-8');

    if (rawReceiptType !== 'RECEIPT' && rawReceiptType !== 'ATTEST') {
      const error = new Error(`Unsupported receipt type ${rawReceiptType}`);

      logger.error(error, `Receipt type was ${rawReceiptType}`);
      throw error;
    }

    return rawReceiptType;
  },
};

function assertOctetString(
  asn1Block: LocalBaseBlock
): asserts asn1Block is OctetString {
  if (!(asn1Block instanceof OctetString)) {
    throw new TypeError(
      `Expected OCTET STRING but received different type of ASN.1 node`
    );
  }
}

// Shamelessly copied from: https://stackoverflow.com/a/50375286/973481
type UnionToIntersection<U> = (
  U extends unknown ? (k: U) => void : never
) extends (k: infer I) => void
  ? I
  : never;

type TReceiptField =
  | { appId: string }
  | { attestedPublicKey: Buffer }
  | { token: string }
  | { clientHash: Buffer }
  | { receiptType: 'ATTEST' | 'RECEIPT' }
  | { expirationTime: Date }
  | { creationTime: Date };

export type TReceiptFields = UnionToIntersection<TReceiptField>;

function assertReceiptFieldsComplete(
  partialReceiptFields: Partial<TReceiptFields>
): asserts partialReceiptFields is TReceiptFields {
  if (!partialReceiptFields.appId) {
    throw new Error(`Did not find appId in receipt fields`);
  }

  if (!partialReceiptFields.attestedPublicKey) {
    throw new Error(`Did not find attestedPublicKey in receipt fields`);
  }

  if (!partialReceiptFields.clientHash) {
    throw new Error(`Did not find clientHash in receipt fields`);
  }

  if (!partialReceiptFields.token) {
    throw new Error(`Did not find token in receipt fields`);
  }

  if (!partialReceiptFields.receiptType) {
    throw new Error(`Did not find receiptType in receipt fields`);
  }

  if (!partialReceiptFields.expirationTime) {
    throw new Error(`Did not find expirationTime in receipt fields`);
  }

  if (!partialReceiptFields.creationTime) {
    throw new Error(`Did not find creationTime in receipt fields`);
  }
}

function* iterateReceiptFields(
  receiptFieldsSequences: LocalBaseBlock[]
): Generator<TReceiptField> {
  for (const receiptFieldsSequence of receiptFieldsSequences) {
    assertASN1Type(Sequence, receiptFieldsSequence);
    const [
      typeItem,
      versionItem,
      valueItem,
    ] = receiptFieldsSequence.valueBlock.value;
    assertASN1Type(Integer, typeItem);
    assertASN1Type(Integer, versionItem);

    switch (typeItem.valueBlock.valueDec) {
      case 2:
        yield { appId: receiptFieldParsers.utf8(valueItem) };
        break;
      case 3:
        yield {
          attestedPublicKey: receiptFieldParsers.buffer(valueItem),
        };
        break;
      case 4:
        yield {
          clientHash: receiptFieldParsers.buffer(valueItem),
        };
        break;
      case 5:
        yield {
          token: receiptFieldParsers.utf8(valueItem),
        };
        break;
      case 6:
        yield {
          receiptType: receiptFieldParsers.receiptType(valueItem),
        };
        break;
      case 12:
        yield {
          creationTime: receiptFieldParsers.date(valueItem),
        };
        break;
      case 21:
        yield {
          expirationTime: receiptFieldParsers.date(valueItem),
        };
        break;
      default:
        logger.debug(
          `Ignoring receipt field of type ${typeItem.valueBlock.valueDec}`
        );
    }
  }
}

const buildReceiptFields = (
  receiptFieldsSequences: LocalBaseBlock[]
): TReceiptFields => {
  const result: Partial<TReceiptFields> = {};

  for (const receiptFieldSlice of iterateReceiptFields(
    receiptFieldsSequences
  )) {
    Object.assign(result, receiptFieldSlice);
  }

  assertReceiptFieldsComplete(result);

  return result;
};

/**
 * Verifies the receipt that's part of an Apple attestation object according to this official guide:
 * https://developer.apple.com/documentation/devicecheck/assessing_fraud_risk
 *
 * @param receiptBuffer
 */
export const verifyIOSAttestationReceipt = async (
  receiptBuffer: Buffer,
  expectedPublicKeyBytes: Buffer
): Promise<void> => {
  const receiptContentInfo = new ContentInfo({
    schema: fromBER(typedArrayToBuffer(receiptBuffer)).result,
  });

  const signedData = new SignedData({
    schema: receiptContentInfo.content,
  });

  signedData.encapContentInfo.eContent.valueBlock.value = [];
  signedData.encapContentInfo.eContent.valueBlock.valueHex = new ArrayBuffer(
    10
  );

  // Step 1-2 (according to Apple docs)
  await signedData.verify({
    signer: 0,
    checkChain: true,
    trustedCerts: [getApplePublicRootCAG3PKIJS()],
  });

  // Step 3 (according to Apple docs)

  // The asn1js library has trouble parsing the OCTET STRING that is the actually signed data in the
  // PKCS#7 receipt. While it parses it as an actual OCTET STRING, it icorrectly recognizes two
  // OCTET STRINGs as its descendants. Trying to decode either of these fails. Therefore, we concat
  // the descendants raw values ("valueBeforeDecode") and parse this as ASN.1. This yields the
  // desired SET.
  const intermediateSignedDataParsedASN1 = fromBER(
    signedData.encapContentInfo.eContent.valueBeforeDecode
  ).result;

  assertOctetString(intermediateSignedDataParsedASN1);
  assertOctetString(intermediateSignedDataParsedASN1.valueBlock.value[0]);
  assertOctetString(intermediateSignedDataParsedASN1.valueBlock.value[1]);

  const signedDataParsedASN1 = fromBER(
    typedArrayToBuffer(
      Buffer.concat(
        intermediateSignedDataParsedASN1.valueBlock.value.map(value => {
          assertASN1Type(OctetString, value);
          return Buffer.from(value.valueBlock.valueHex);
        })
      )
    )
  );

  assertASN1Type(Set, signedDataParsedASN1.result);

  const receiptFieldsSequences = signedDataParsedASN1.result.valueBlock.value;

  const receiptFields = buildReceiptFields(receiptFieldsSequences);

  // Step 4 (according to Apple docs)
  const receiptAppIDHash = createHash('sha256')
    .update(receiptFields.appId)
    .digest();
  const anyAllowedAppIdHashMatches = IOS_ALLOWED_APP_ID_HASHES.some(
    allowedAppIDHash => allowedAppIDHash.equals(receiptAppIDHash)
  );
  if (!anyAllowedAppIdHashMatches) {
    const error = new IOSAttestationError(
      IOSReceiptErrorReason.APP_ID_HASH_NOT_MATCHING
    );
    logger.error(
      error,
      `Receipt's app ID hash was ${receiptAppIDHash.toString(
        'hex'
      )} but allowed were [${IOS_ALLOWED_APP_ID_HASHES.join(
        ', '
      )}] based on allowed app IDs [${IOS_ALLOWED_APP_IDS.join(', ')}]`
    );
    throw error;
  }

  // Step 5 (according to Apple docs)
  const receiptAgeMS = Date.now() - receiptFields.creationTime.getTime();
  if (receiptAgeMS > 1000 * 60 * 5) {
    const error = new IOSAttestationError(
      IOSReceiptErrorReason.RECEIPT_TOO_OLD
    );
    logger.error(
      error,
      `Receipt's creation time was ${receiptFields.creationTime.toString()}`
    );
    throw error;
  }

  // Step 6 (according to Apple docs)
  const publicKeyBytes = findPublicKeyBitStringInX509ASN1(
    asn1.fromDer(util.createBuffer(receiptFields.attestedPublicKey))
  );
  if (!publicKeyBytes.equals(expectedPublicKeyBytes)) {
    const error = new IOSAttestationError(
      IOSReceiptErrorReason.PUBLIC_KEY_NOT_MATCHING
    );
    logger.error(
      error,
      `Expected receipt's attested public key to be ${expectedPublicKeyBytes.toString(
        'base64'
      )} but received ${publicKeyBytes.toString('base64')}`
    );
    throw error;
  }
};
