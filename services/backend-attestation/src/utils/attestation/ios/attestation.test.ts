import { useFakeTimers } from 'sinon';
import { extractBERFromPEM } from '../../certificates';
import { parseIOSAttestation, verifyIOSAttestation } from '.';
import { deconstructAttestationAuthData } from './authData';

const MOCK_NONCE = 'R/uyo36/JfBEgZdB458hM9bkIoiLEx3psu8EsccMZoo=';
const MOCK_ATTESTATION_CBOR_BASE64 =
  'o2NmbXRvYXBwbGUtYXBwYXR0ZXN0Z2F0dFN0bXSiY3g1Y4JZAuUwggLhMIICZ6ADAgECAgYBfwcA0LIwCgYIKoZIzj0EAwIwTzEjMCEGA1UEAwwaQXBwbGUgQXBwIEF0dGVzdGF0aW9uIENBIDExEzARBgNVBAoMCkFwcGxlIEluYy4xEzARBgNVBAgMCkNhbGlmb3JuaWEwHhcNMjIwMjE2MDkyNjA4WhcNMjIwMjE5MDkyNjA4WjCBkTFJMEcGA1UEAwxAOGFkMzIxYTRmOTYxZjAyM2IxMThiOTA3MDE3NWZkM2QxNmE2ZTgzOGE0NGFiODE0ZWNjZWJkZDg4YTkyOTJiYzEaMBgGA1UECwwRQUFBIENlcnRpZmljYXRpb24xEzARBgNVBAoMCkFwcGxlIEluYy4xEzARBgNVBAgMCkNhbGlmb3JuaWEwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAATCkV/0CVmOTxnJwPvnlJ0OjKk/aiSd+LqQfwxoZoxtLxTENLawgqrbPO43PNF0UClB3exeZ/xidR3n4/wwpxARo4HrMIHoMAwGA1UdEwEB/wQCMAAwDgYDVR0PAQH/BAQDAgTwMHwGCSqGSIb3Y2QIBQRvMG2kAwIBCr+JMAMCAQG/iTEDAgEAv4kyAwIBAb+JMwMCAQG/iTQkBCI3NDY0MzIyOVk0LmRlLmN1bHR1cmU0bGlmZS5sdWNhLnAypQYEBHNrcyC/iTYDAgEFv4k3AwIBAL+JOQMCAQC/iToDAgEAMBUGCSqGSIb3Y2QIBwQIMAa/ingCBAAwMwYJKoZIhvdjZAgCBCYwJKEiBCBePWY0g+iIhVYRS+0yzcDewJ55tKzLJ63tIOwcDqiwxzAKBggqhkjOPQQDAgNoADBlAjBWyexfn9uAwFYPq6Fvhr9bWOz+RDisedmpsC2dnO8A50q+DkqK3I8QTP+nH+hv4wYCMQCUeV6YsucA4mt58ZanHwLMmQGdkSFhNEVn96pa3v4BaO7ZJvzbKuw5F2gAw6vR8zNZAkcwggJDMIIByKADAgECAhAJusXhvEAa2dRTlbw4GghUMAoGCCqGSM49BAMDMFIxJjAkBgNVBAMMHUFwcGxlIEFwcCBBdHRlc3RhdGlvbiBSb290IENBMRMwEQYDVQQKDApBcHBsZSBJbmMuMRMwEQYDVQQIDApDYWxpZm9ybmlhMB4XDTIwMDMxODE4Mzk1NVoXDTMwMDMxMzAwMDAwMFowTzEjMCEGA1UEAwwaQXBwbGUgQXBwIEF0dGVzdGF0aW9uIENBIDExEzARBgNVBAoMCkFwcGxlIEluYy4xEzARBgNVBAgMCkNhbGlmb3JuaWEwdjAQBgcqhkjOPQIBBgUrgQQAIgNiAASuWzegd015sjWPQOfR8iYm8cJf7xeALeqzgmpZh0/40q0VJXiaomYEGRJItjy5ZwaemNNjvV43D7+gjjKegHOphed0bqNZovZvKdsyr0VeIRZY1WevniZ+smFNwhpmzpmjZjBkMBIGA1UdEwEB/wQIMAYBAf8CAQAwHwYDVR0jBBgwFoAUrJEQUzO9vmhB/6cMqeX66uXliqEwHQYDVR0OBBYEFD7jXRwEGanJtDH4hHTW4eFXcuObMA4GA1UdDwEB/wQEAwIBBjAKBggqhkjOPQQDAwNpADBmAjEAu76IjXONBQLPvP1mbQlXUDW81ocsP4QwSSYp7dH5FOh5mRya6LWu+NOoVDP3tg0GAjEAqzjt0MyB7QCkUsO6RPmTY2VT/swpfy60359evlpKyraZXEuCDfkEOG94B7tYlDm3Z3JlY2VpcHRZDlUwgAYJKoZIhvcNAQcCoIAwgAIBATEPMA0GCWCGSAFlAwQCAQUAMIAGCSqGSIb3DQEHAaCAJIAEggPoMYIEEDAqAgECAgEBBCI3NDY0MzIyOVk0LmRlLmN1bHR1cmU0bGlmZS5sdWNhLnAyMIIC7wIBAwIBAQSCAuUwggLhMIICZ6ADAgECAgYBfwcA0LIwCgYIKoZIzj0EAwIwTzEjMCEGA1UEAwwaQXBwbGUgQXBwIEF0dGVzdGF0aW9uIENBIDExEzARBgNVBAoMCkFwcGxlIEluYy4xEzARBgNVBAgMCkNhbGlmb3JuaWEwHhcNMjIwMjE2MDkyNjA4WhcNMjIwMjE5MDkyNjA4WjCBkTFJMEcGA1UEAwxAOGFkMzIxYTRmOTYxZjAyM2IxMThiOTA3MDE3NWZkM2QxNmE2ZTgzOGE0NGFiODE0ZWNjZWJkZDg4YTkyOTJiYzEaMBgGA1UECwwRQUFBIENlcnRpZmljYXRpb24xEzARBgNVBAoMCkFwcGxlIEluYy4xEzARBgNVBAgMCkNhbGlmb3JuaWEwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAATCkV/0CVmOTxnJwPvnlJ0OjKk/aiSd+LqQfwxoZoxtLxTENLawgqrbPO43PNF0UClB3exeZ/xidR3n4/wwpxARo4HrMIHoMAwGA1UdEwEB/wQCMAAwDgYDVR0PAQH/BAQDAgTwMHwGCSqGSIb3Y2QIBQRvMG2kAwIBCr+JMAMCAQG/iTEDAgEAv4kyAwIBAb+JMwMCAQG/iTQkBCI3NDY0MzIyOVk0LmRlLmN1bHR1cmU0bGlmZS5sdWNhLnAypQYEBHNrcyC/iTYDAgEFv4k3AwIBAL+JOQMCAQC/iToDAgEAMBUGCSqGSIb3Y2QIBwQIMAa/ingCBAAwMwYJKoZIhvdjZAgCBCYwJKEiBCBePWY0g+iIhVYRS+0yzcDewJ55tKzLJ63tIOwcDqiwxzAKBggqhkjOPQQDAgNoADBlAjBWyexfn9uAwFYPq6Fvhr9bWOz+RDisedmpsC2dnO8A50q+DkqK3I8QTP+nH+hv4wYCMQCUeV6YsucA4mt58ZanHwLMmQGdkSFhNEVn96pa3v4BaO7ZJvzbKuw5F2gAw6vR8zMwKAIBBAIBAQQgmW0nnYIVXq5pw6W+7lFDMRlX3NQkT4EVVo/6hTADnaEwYAIBBQIBAQRYRXM4b3NYRS9mVlBldExUOElFZVoraktxWVlrM2Q2Qlp0ak1hRnA1TmcxMnpzcmdUQmFSa3lzNWVDUDVCTmhnSTR3U21mTnFyL3hva2pMNXlsZTdWOFE9PTAOAgEGAgEBBAZBVFRFU1QwDwIBBwIBAQQHc2FuZGJveDAgAgEMAgEBBBgyMDIyLTAyLTE3VDA5OgQsMjY6MDguMzY4WjAgAgEVAgEBBBgyMDIyLTA1LTE4VDA5OjI2OjA4LjM2OFoAAAAAAACggDCCA64wggNUoAMCAQICEFpjJPW2ctrfH4W+ZDeqFOIwCgYIKoZIzj0EAwIwfDEwMC4GA1UEAwwnQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgNSAtIEcxMSYwJAYDVQQLDB1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwHhcNMjEwNTA1MDQwNzUyWhcNMjIwNjA0MDQwNzUxWjBaMTYwNAYDVQQDDC1BcHBsaWNhdGlvbiBBdHRlc3RhdGlvbiBGcmF1ZCBSZWNlaXB0IFNpZ25pbmcxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAELsXe1jucNyoc6PZZ1HC+gAoLvrp2OYRC2uUxlc0XTttpsRERdei9p2MvNc++eqh6L1DVluytBItt4JPr96ysEqOCAdgwggHUMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAU2Rf+S2eQOEuS9NvO1VeAFAuPPckwQwYIKwYBBQUHAQEENzA1MDMGCCsGAQUFBzABhidodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLWFhaWNhNWcxMDEwggEcBgNVHSAEggETMIIBDzCCAQsGCSqGSIb3Y2QFATCB/TCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA1BggrBgEFBQcCARYpaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkwHQYDVR0OBBYEFIGCBRw26M+diRwFHH9m3uETIOVTMA4GA1UdDwEB/wQEAwIHgDAPBgkqhkiG92NkDA8EAgUAMAoGCCqGSM49BAMCA0gAMEUCIEbl6FNbfgVKn3/xjyoz1uGyGSpRZBDXeykfBquci6kTAiEAuHeXtKhLMS55fYtQ4yjVQbYt4ZdBgvJH8JTG8orOCxYwggL5MIICf6ADAgECAhBW+4PUK/+NwzeZI7Varm69MAoGCCqGSM49BAMDMGcxGzAZBgNVBAMMEkFwcGxlIFJvb3QgQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTE5MDMyMjE3NTMzM1oXDTM0MDMyMjAwMDAwMFowfDEwMC4GA1UEAwwnQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgNSAtIEcxMSYwJAYDVQQLDB1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAASSzmO9fYaxqygKOxzhr/sElICRrPYx36bLKDVvREvhIeVX3RKNjbqCfJW+Sfq+M8quzQQZ8S9DJfr0vrPLg366o4H3MIH0MA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUu7DeoVgziJqkipnevr3rr9rLJKswRgYIKwYBBQUHAQEEOjA4MDYGCCsGAQUFBzABhipodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLWFwcGxlcm9vdGNhZzMwNwYDVR0fBDAwLjAsoCqgKIYmaHR0cDovL2NybC5hcHBsZS5jb20vYXBwbGVyb290Y2FnMy5jcmwwHQYDVR0OBBYEFNkX/ktnkDhLkvTbztVXgBQLjz3JMA4GA1UdDwEB/wQEAwIBBjAQBgoqhkiG92NkBgIDBAIFADAKBggqhkjOPQQDAwNoADBlAjEAjW+mn6Hg5OxbTnOKkn89eFOYj/TaH1gew3VK/jioTCqDGhqqDaZkbeG5k+jRVUztAjBnOyy04eg3B3fL1ex2qBo6VTs/NWrIxeaSsOFhvoBJaeRfK6ls4RECqsxh2Ti3c0owggJDMIIByaADAgECAggtxfyI0sVLlTAKBggqhkjOPQQDAzBnMRswGQYDVQQDDBJBcHBsZSBSb290IENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzAeFw0xNDA0MzAxODE5MDZaFw0zOTA0MzAxODE5MDZaMGcxGzAZBgNVBAMMEkFwcGxlIFJvb3QgQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMHYwEAYHKoZIzj0CAQYFK4EEACIDYgAEmOkvPUBypO2TInKBExzdEJXxxaNOcdwUFtkO5aYFKndke19OONO7HES1f/UftjJiXcnphFtPME8RWgD9WFgMpfUPLE0HRxN12peXl28xXO0rnXsgO9i5VNlemaQ6UQoxo0IwQDAdBgNVHQ4EFgQUu7DeoVgziJqkipnevr3rr9rLJKswDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYwCgYIKoZIzj0EAwMDaAAwZQIxAIPpwcQWXhpdNBjZ7e/0bA4ARku437JGEcUP/eZ6jKGma87CA9Sc9ZPGdLhq36ojFQIwbWaKEMrUDdRPzY1DPrSKY6UzbuNt2he3ZB/IUyb5iGJ0OQsXW8tRqAzoGAPnorIoAAAxgfwwgfkCAQEwgZAwfDEwMC4GA1UEAwwnQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgNSAtIEcxMSYwJAYDVQQLDB1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMCEFpjJPW2ctrfH4W+ZDeqFOIwDQYJYIZIAWUDBAIBBQAwCgYIKoZIzj0EAwIERjBEAiAjOrCDZl6+Wmu6CQWd+FflcNO8iCIyiGiCP4SiZL5ESAIgVjCyy/dYkxMI+CSR6EZW1rsmuDf/AjE5TjVp0PNqrN8AAAAAAABoYXV0aERhdGFYpO1fROqR+Y0hQ93GXwREXXBK5vUSWgvJsgWQONj3MINTQAAAAABhcHBhdHRlc3RkZXZlbG9wACCK0yGk+WHwI7EYuQcBdf09FqboOKRKuBTszr3YipKSvKUBAgMmIAEhWCDCkV/0CVmOTxnJwPvnlJ0OjKk/aiSd+LqQfwxoZoxtLyJYIBTENLawgqrbPO43PNF0UClB3exeZ/xidR3n4/wwpxAR';
const MOCK_KEY_IDENTIFIER_BASE64 =
  'itMhpPlh8COxGLkHAXX9PRam6DikSrgU7M692IqSkrw=';

const WRONG_CERTIFICATE_CA = `-----BEGIN CERTIFICATE-----
MIIF3DCCBMSgAwIBAgIQDK+Oqsay+j9ysFneQ1N7djANBgkqhkiG9w0BAQsFADBG
MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRUwEwYDVQQLEwxTZXJ2ZXIg
Q0EgMUIxDzANBgNVBAMTBkFtYXpvbjAeFw0yMTA4MzAwMDAwMDBaFw0yMjA5Mjgy
MzU5NTlaMB4xHDAaBgNVBAMTE3d3dy5zdWVkZGV1dHNjaGUuZGUwggEiMA0GCSqG
SIb3DQEBAQUAA4IBDwAwggEKAoIBAQCoksTAOTrZET3wu5TrADBzCJ3akGbIZsvH
P4lQ9TECGUO1ufjMXnoKMHXMXKG113/ZqsOUUnH0HZx0FDjBqHGEbDPkNsqAbdrW
M4h0ksKZ3FxW4m8se22XMwMrnFJCV0hlc9/CO36KHv2S2pRxyYcA1jZeb10tpGdO
x4BEdgY1rtu+qwEJLQdLDVnzGQbnOeW11P0uyPFgdv0A2bEUTGKex+j0+xwDZWH4
hy509Gx6u+txfLSzwhTdaU41nQCbTYgKOR5jNzkX+bjZ8K7GyMtxwYFpLO64oNiB
PQ6k0+UrPZTlCfqjKbws0CXE7+6kIOTG7AOHH8PFCJNnswHS1ZNtAgMBAAGjggLs
MIIC6DAfBgNVHSMEGDAWgBRZpGYGUqB7lZI8o5QHJ5Z0W/k90DAdBgNVHQ4EFgQU
W5rznzmOOMzfc0SxDrbTLQU0MTYwHgYDVR0RBBcwFYITd3d3LnN1ZWRkZXV0c2No
ZS5kZTAOBgNVHQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUF
BwMCMDsGA1UdHwQ0MDIwMKAuoCyGKmh0dHA6Ly9jcmwuc2NhMWIuYW1hem9udHJ1
c3QuY29tL3NjYTFiLmNybDATBgNVHSAEDDAKMAgGBmeBDAECATB1BggrBgEFBQcB
AQRpMGcwLQYIKwYBBQUHMAGGIWh0dHA6Ly9vY3NwLnNjYTFiLmFtYXpvbnRydXN0
LmNvbTA2BggrBgEFBQcwAoYqaHR0cDovL2NydC5zY2ExYi5hbWF6b250cnVzdC5j
b20vc2NhMWIuY3J0MAwGA1UdEwEB/wQCMAAwggF+BgorBgEEAdZ5AgQCBIIBbgSC
AWoBaAB2ACl5vvCeOTkh8FZzn2Old+W+V32cYAr4+U1dJlwlXceEAAABe5UI4ycA
AAQDAEcwRQIhAJkxBkppaiCb37xdGWi2FYO/sAeDdI/bvvNmcn+ZFuLkAiA2kqDQ
TMJzTRzshPVkYprvkurrcFSj8KEK6kCilryZMwB3AFGjsPX9AXmcVm24N3iPDKR6
zBsny/eeiEKaDf7UiwXlAAABe5UI44AAAAQDAEgwRgIhALrORnzWb0Z2XC1xMHCA
bfla4R2R7x+Hv3VYiBoH15t7AiEAkx5blBCLNtks/+vs8VW000pfbxXKQJ2Eysuc
yteOgLcAdQBByMqx3yJGShDGoToJQodeTjGLGwPr60vHaPCQYpYG9gAAAXuVCOMM
AAAEAwBGMEQCIAWO6FB1Enab8EwEItMV1ki0K1xPTAAq9vXXPlJbFl8nAiBKR57S
UNDWph0wQLPFNOGKXWDubfKbI4bmz7EZzU2QATANBgkqhkiG9w0BAQsFAAOCAQEA
XjCiyyi6dFJ140xTtF1Ptm7lG5A3jdNEQmyOCeR8yeOD32g9cICRjXLYLqpVw3Ih
Q+k4hfqoYqqoM6ep98d1Ai83/Y6My0uLk0cHk1ginNpBJpM8beUCKV6K4dLyCqML
z/v2UmAyZWJUXLZcDeeiatA7J7/BOqRmgh3vZvjL1qfGEiG7gn46x/ZXb1dLC2Y1
qQYzKJ89jHWLnYHsW/JNAhKa1BHPTVApI+cgtwuaLDKIEGOBdBEmGSlAzoEsfv/Z
cr5Q8JB23nyqCgIX8J1b6uQ1k23uhh5/Hr6wNJuDV+vePR6oiUSgfCzENdFkfrtr
jvIpvxqZj69IbyM200u/SQ==
-----END CERTIFICATE-----
`;

describe('verifyIOSAttestation', function () {
  describe('when providing valid attestation, nonce and key identifier', function () {
    it('resolves successfully', async function () {
      useFakeTimers(new Date(2022, 1, 17));
      const parsedAttestation = await parseIOSAttestation(
        Buffer.from(MOCK_ATTESTATION_CBOR_BASE64, 'base64')
      );
      const authData = await deconstructAttestationAuthData(
        parsedAttestation.authData
      );

      await verifyIOSAttestation(
        parsedAttestation,
        authData,
        MOCK_NONCE,
        Buffer.from(MOCK_KEY_IDENTIFIER_BASE64, 'base64'),
        () => Promise.resolve(true)
      );
    });
  });

  describe('when ensureValidNonceAndInvalidate() returns false', function () {
    it('rejects with UNKNOWN_NONCE', async function () {
      useFakeTimers(new Date(2022, 1, 18));

      const parsedAttestation = await parseIOSAttestation(
        Buffer.from(MOCK_ATTESTATION_CBOR_BASE64, 'base64')
      );
      const authData = await deconstructAttestationAuthData(
        parsedAttestation.authData
      );

      parsedAttestation.attStmt.x5c = [extractBERFromPEM(WRONG_CERTIFICATE_CA)];

      await expect(
        verifyIOSAttestation(
          parsedAttestation,
          authData,
          MOCK_NONCE,
          Buffer.from(MOCK_KEY_IDENTIFIER_BASE64, 'base64'),
          async () => false
        )
      ).rejects.toThrowError('UNKNOWN_NONCE');
    });
  });

  describe('when providing attestation with credCert from different CA', function () {
    it('rejects with CERTIFICATE_CHAIN_VERIFICATION', async function () {
      useFakeTimers(new Date(2022, 1, 18));

      const parsedAttestation = await parseIOSAttestation(
        Buffer.from(MOCK_ATTESTATION_CBOR_BASE64, 'base64')
      );
      const authData = await deconstructAttestationAuthData(
        parsedAttestation.authData
      );

      parsedAttestation.attStmt.x5c = [extractBERFromPEM(WRONG_CERTIFICATE_CA)];

      await expect(
        verifyIOSAttestation(
          parsedAttestation,
          authData,
          MOCK_NONCE,
          Buffer.from(MOCK_KEY_IDENTIFIER_BASE64, 'base64'),
          () => Promise.resolve(true)
        )
      ).rejects.toThrowError('CERTIFICATE_CHAIN_VERIFICATION');
    });
  });

  describe('when providing incorrect nonce', function () {
    it('rejects with AUTH_DATE_NONCE_OID', async function () {
      useFakeTimers(new Date(2022, 1, 17));
      const parsedAttestation = await parseIOSAttestation(
        Buffer.from(MOCK_ATTESTATION_CBOR_BASE64, 'base64')
      );
      const authData = await deconstructAttestationAuthData(
        parsedAttestation.authData
      );
      const nonce = `_${MOCK_NONCE.slice(1)}`;

      await expect(
        verifyIOSAttestation(
          parsedAttestation,
          authData,
          nonce,
          Buffer.from(MOCK_KEY_IDENTIFIER_BASE64, 'base64'),
          () => Promise.resolve(true)
        )
      ).rejects.toThrowError('AUTH_DATE_NONCE_OID');
    });
  });

  describe('when providing incorrect keyIdentifier', function () {
    it('rejects with AUTH_DATE_NONCE_OID', async function () {
      useFakeTimers(new Date(2022, 1, 17));
      const parsedAttestation = await parseIOSAttestation(
        Buffer.from(MOCK_ATTESTATION_CBOR_BASE64, 'base64')
      );
      const keyIdentifier = Buffer.from(MOCK_KEY_IDENTIFIER_BASE64, 'base64');
      keyIdentifier[0] += 1;
      const authData = await deconstructAttestationAuthData(
        parsedAttestation.authData
      );

      await expect(
        verifyIOSAttestation(
          parsedAttestation,
          authData,
          MOCK_NONCE,
          keyIdentifier,
          () => Promise.resolve(true)
        )
      ).rejects.toThrowError('KEY_IDENTIFIER_FINGERPRINT_EQUALITY');
    });
  });

  describe('when auth data signCount > 0', function () {
    it('rejects with SIGN_COUNT_GREATER_ZERO', async function () {
      useFakeTimers(new Date(2022, 1, 17));
      const parsedAttestation = await parseIOSAttestation(
        Buffer.from(MOCK_ATTESTATION_CBOR_BASE64, 'base64')
      );
      const authData = await deconstructAttestationAuthData(
        parsedAttestation.authData
      );
      authData.signCount += 1;

      await expect(
        verifyIOSAttestation(
          parsedAttestation,
          authData,
          MOCK_NONCE,
          Buffer.from(MOCK_KEY_IDENTIFIER_BASE64, 'base64'),
          () => Promise.resolve(true)
        )
      ).rejects.toThrowError('SIGN_COUNT_GREATER_ZERO');
    });
  });

  describe("when attested credential data's credentialId does not match passed key identifier", function () {
    it('rejects with SIGN_COUNT_GREATER_ZERO', async function () {
      useFakeTimers(new Date(2022, 1, 17));
      const parsedAttestation = await parseIOSAttestation(
        Buffer.from(MOCK_ATTESTATION_CBOR_BASE64, 'base64')
      );
      const authData = await deconstructAttestationAuthData(
        parsedAttestation.authData
      );

      // Buffer.from() copies the buffer which is necessary since there are multiple views on the
      // same buffer and without copying it we would actually run into the error AUTH_DATE_NONCE_OID
      // because that check happens earlier.
      authData.attestedCredentialData.credentialId = Buffer.from(
        authData.attestedCredentialData.credentialId
      );
      authData.attestedCredentialData.credentialId[0] += 1;

      await expect(
        verifyIOSAttestation(
          parsedAttestation,
          authData,
          MOCK_NONCE,
          Buffer.from(MOCK_KEY_IDENTIFIER_BASE64, 'base64'),
          () => Promise.resolve(true)
        )
      ).rejects.toThrowError('KEY_IDENTIFIER_NOT_MATCHING_CREDENTIAL_ID');
    });
  });

  describe('when providing valid attestation but receipt is too old', function () {
    it('rejects with RECEIPT_TOO_OLD', async function () {
      useFakeTimers(new Date(2022, 1, 18));

      const parsedAttestation = await parseIOSAttestation(
        Buffer.from(MOCK_ATTESTATION_CBOR_BASE64, 'base64')
      );
      const authData = await deconstructAttestationAuthData(
        parsedAttestation.authData
      );

      await expect(
        verifyIOSAttestation(
          parsedAttestation,
          authData,
          MOCK_NONCE,
          Buffer.from(MOCK_KEY_IDENTIFIER_BASE64, 'base64'),
          () => Promise.resolve(true)
        )
      ).rejects.toThrowError('RECEIPT_TOO_OLD');
    });
  });
});
