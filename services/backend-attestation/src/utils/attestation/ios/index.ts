export { parseIOSAttestation, verifyIOSAttestation } from './attestation';
export type { TIOSAttestation } from './attestation';
export { deconstructAttestationAuthData as deconstructAuthData } from './authData';
export type { TIOSAttestationAuthData } from './authData';
export type { TReceiptFields } from './receipt';
