import { decodeAll } from 'cbor';
import {
  convertBigEndian16BitBufferToNumber,
  convertBigEndian32BitBufferToNumber,
} from '../../bigEndian';
import { IOSAttestationError, IOSAttestationErrorReason } from './errors';

const AUTH_DATA_OFFSET_RP_ID_HASH = 0;
const AUTH_DATA_OFFSET_FLAGS = 32;
const AUTH_DATA_OFFSET_SIGN_COUNT = 33;
const AUTH_DATA_OFFSET_ATTESTED_CREDENTIAL_DATA = 37;

const ATTESTED_CREDENTIAL_DATA_OFFSETS = {
  aaguid: 0,
  credentialIdLength: 16,
  credentialId: 18,
};

type TIOSAttestationFlags = {
  userPresent: boolean;
  userVerified: boolean;
  attestedCredentialDataIncluded: boolean;
  extensionDataIncluded: boolean;
};

const IOS_ATTESTATION_FLAGS = {
  userVerified: 0b00000100,
  attestedCredentialDataIncluded: 0b01000000,
  extensionDataIncluded: 0b10000000,
  userPresent: 0b00000001,
};

/**
 * Extracts a dictionary of the values of the authentiation flags. See the table here:
 * https://www.w3.org/TR/webauthn/#sctn-authenticator-data
 *
 * @param flagsBuffer
 * @returns
 */
const deconstructFlags = ([flagsBits]: Buffer): TIOSAttestationFlags => {
  /* eslint-disable no-bitwise */
  return {
    userPresent: !!(flagsBits & IOS_ATTESTATION_FLAGS.userPresent),
    userVerified: !!(flagsBits & IOS_ATTESTATION_FLAGS.userVerified),
    attestedCredentialDataIncluded: !!(
      flagsBits & IOS_ATTESTATION_FLAGS.attestedCredentialDataIncluded
    ),
    extensionDataIncluded: !!(
      flagsBits & IOS_ATTESTATION_FLAGS.extensionDataIncluded
    ),
  };
  /* eslint-enable no-bitwise */
};

/**
 * Extracts dictionary of attested credential data from the original buffer. For details, see
 * https://www.w3.org/TR/webauthn/#attested-credential-data
 *
 * @param attestedCredentialData
 * @param flags
 * @returns
 */
const deconstructAttestedCredentialData = async (
  attestedCredentialData: Buffer,
  flags: TIOSAttestationFlags
): Promise<{
  aaguid: Buffer;
  credentialIdLength: Buffer;
  credentialId: Buffer;
  credentialPublicKeyBuffer: Buffer;
}> => {
  if (!flags.attestedCredentialDataIncluded) {
    // While we don't care if any extension data is present in the attestaion data, the attested
    // credential data is mandatory because we use it to verify authenticity.
    throw new IOSAttestationError(
      IOSAttestationErrorReason.ATTESTED_CREDENTIAL_DATA_MISSING_BY_FLAG
    );
  }

  const aaguid = attestedCredentialData.slice(
    ATTESTED_CREDENTIAL_DATA_OFFSETS.aaguid,
    ATTESTED_CREDENTIAL_DATA_OFFSETS.credentialIdLength
  );
  const credentialIdLength = attestedCredentialData.slice(
    ATTESTED_CREDENTIAL_DATA_OFFSETS.credentialIdLength,
    ATTESTED_CREDENTIAL_DATA_OFFSETS.credentialId
  );

  const credentialIdLengthNumber = convertBigEndian16BitBufferToNumber(
    credentialIdLength
  );

  const attestedCredentialDataOffset =
    ATTESTED_CREDENTIAL_DATA_OFFSETS.credentialId + credentialIdLengthNumber;

  const credentialId = attestedCredentialData.slice(
    ATTESTED_CREDENTIAL_DATA_OFFSETS.credentialId,
    attestedCredentialDataOffset
  );

  const [
    attestedCredentialDataCBOR,
    extensionDataCBOR,
  ] = await decodeAll(
    attestedCredentialData.slice(attestedCredentialDataOffset),
    { extendedResults: true }
  );

  if (attestedCredentialDataCBOR?.length <= 0) {
    throw new IOSAttestationError(
      IOSAttestationErrorReason.ATTESTED_CREDENTIAL_DATA_MISSING_BY_PARSE
    );
  }

  if (flags.extensionDataIncluded && extensionDataCBOR?.length <= 0) {
    throw new IOSAttestationError(
      IOSAttestationErrorReason.EXTENSION_DATA_MISSING
    );
  }

  return {
    aaguid,
    credentialIdLength,
    credentialId,
    credentialPublicKeyBuffer: attestedCredentialData.slice(
      attestedCredentialDataOffset,
      attestedCredentialDataOffset + attestedCredentialDataCBOR.length
    ),
  };
};

export type TIOSAssertionAuthData = {
  rpIdHash: Buffer;
  flags: TIOSAttestationFlags;
  signCount: number;
};

export const deconstructAssertionAuthData = async (
  authData: Buffer
): Promise<TIOSAssertionAuthData> => {
  const rpIdHash = authData.slice(
    AUTH_DATA_OFFSET_RP_ID_HASH,
    AUTH_DATA_OFFSET_FLAGS
  );

  const flags = deconstructFlags(
    authData.slice(AUTH_DATA_OFFSET_FLAGS, AUTH_DATA_OFFSET_SIGN_COUNT)
  );

  const signCount = authData.slice(
    AUTH_DATA_OFFSET_SIGN_COUNT,
    AUTH_DATA_OFFSET_ATTESTED_CREDENTIAL_DATA
  );

  return {
    rpIdHash,
    flags,
    signCount: convertBigEndian32BitBufferToNumber(signCount),
  };
};

export type TIOSAttestationAuthData = TIOSAssertionAuthData & {
  attestedCredentialData: Awaited<
    ReturnType<typeof deconstructAttestedCredentialData>
  >;
};
export const deconstructAttestationAuthData = async (
  authData: Buffer
): Promise<TIOSAttestationAuthData> => {
  const { rpIdHash, flags, signCount } = await deconstructAssertionAuthData(
    authData
  );

  return {
    rpIdHash,
    flags,
    signCount,
    attestedCredentialData: await deconstructAttestedCredentialData(
      authData.slice(AUTH_DATA_OFFSET_ATTESTED_CREDENTIAL_DATA),
      flags
    ),
  };
};
