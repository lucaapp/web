import { BaseBlock, LocalValueBlock } from 'asn1js';

export function assertASN1Type<TAsn1Type extends BaseBlock<LocalValueBlock>>(
  Type: { new (): TAsn1Type },
  asn1Block: unknown
): asserts asn1Block is TAsn1Type {
  if (!(asn1Block instanceof Type)) {
    throw new TypeError(
      `Expected ${Type.name} but received different type of ASN.1 node`
    );
  }
}
