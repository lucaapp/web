import axios from 'axios';
import config from 'config';
import HttpsProxyAgent from 'https-proxy-agent';
import { setupCache } from 'axios-cache-adapter';

const httpsProxy = config.get('proxy.https');

const axiosCache = setupCache({ readHeaders: true });

export const proxyAgent = httpsProxy
  ? // @ts-ignore HttpsProxyAgent is missing constructor type
    new HttpsProxyAgent(httpsProxy)
  : undefined;

export const proxyClient = axios.create({
  httpsAgent: proxyAgent,
  adapter: axiosCache.adapter,
  proxy: false,
});
