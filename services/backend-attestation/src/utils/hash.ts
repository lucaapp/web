import crypto from 'crypto';
import { promisify } from 'util';

const scrypt = promisify(crypto.scrypt);
const KEY_LENGTH = 64;

export const hashPassword = async (
  password: string,
  salt: string
): Promise<Buffer> => {
  const hash = await scrypt(password, salt, KEY_LENGTH);
  return hash as Buffer;
};

export const pseudoHashPassword = async (): Promise<Buffer> => {
  const hash = await scrypt('pseudo', 'pseudo', KEY_LENGTH);
  return hash as Buffer;
};

export const sha256 = (data: string): string => {
  const hash = crypto.createHash('sha256');
  hash.update(data);
  return hash.digest('hex');
};
