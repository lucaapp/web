import { util } from 'node-forge';

export const nodeForgeBufferToNodeBuffer = (
  nodeForgeBuffer: util.ByteBuffer
): Buffer => {
  const nativeBuffer = Buffer.alloc(nodeForgeBuffer.length());

  let offset = 0;
  for (const byte of nodeForgeBuffer.getBytes()) {
    nativeBuffer.set([byte.charCodeAt(0)], offset);
    offset += 1;
  }
  return nativeBuffer;
};
