const database = require('../database');

export const logJobCompletion = async (
  name: string,
  meta: Record<string, string>
): Promise<void> => {
  await database.JobCompletion.create({
    name,
    meta: JSON.stringify(meta),
  });
};
