import type { RequestHandler, Request, Response } from 'express';

export const waitForMiddleware = (
  middleware: RequestHandler,
  request: Request,
  response: Response
): Promise<void> =>
  new Promise((resolve, reject) => {
    try {
      middleware(request, response, () => {
        resolve();
      });
    } catch (error) {
      reject(error);
    }
  });

export const combineMiddlewares = (
  middlewares: RequestHandler[]
): RequestHandler => async (request, response, next) => {
  for (const middleware of middlewares) {
    await waitForMiddleware(middleware, request, response);
  }
  next();
};
