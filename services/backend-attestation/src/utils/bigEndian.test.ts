// eslint-disable-next-line node/no-unpublished-import
import {
  convertBigEndian16BitBufferToNumber,
  convertBigEndian32BitBufferToNumber,
} from './bigEndian';

describe('bigEndian', () => {
  describe('when converting big-endian-encoded 1000 in 16 bit buffer to number', () => {
    it('returns 1000', () => {
      expect(
        convertBigEndian16BitBufferToNumber(new Uint8Array([0x03, 0xe8]))
      ).toEqual(1000);
    });
  });

  describe('when converting big-endian-encoded 1000 in 32 bit buffer to number', () => {
    it('returns 1000', () => {
      expect(
        convertBigEndian32BitBufferToNumber(new Uint8Array([0, 0, 0x03, 0xe8]))
      ).toEqual(1000);
    });
  });
});
