import { Schema } from 'zod';
import { z } from './validation';

describe('validation', () => {
  describe('object', () => {
    it('strips unknown keys', async () => {
      const schema = z.object({ test: z.string() });
      const inputObject = { test: 'test', unknown: 'test' };
      const expectedOutput = { test: 'test' };
      expect(schema.parse(inputObject)).toMatchObject(expectedOutput);
    });
  });

  describe('uuid', () => {
    let schema: Schema<unknown>;
    beforeEach(() => {
      schema = z.uuid();
    });

    it('should accept v4 uuid', async () => {
      expect(() =>
        schema.parse('ebca725f-6c9c-4870-bcff-1fcb852a56b3')
      ).not.toThrow();
    });
    it('should not accept non-v4 uuid', async () => {
      expect(() =>
        schema.parse('ffa3bb2b-fd75-ffff-ffff-facab27ddcea')
      ).toThrow(z.ZodError);
    });
    it('should reject invalid uuids', async () => {
      expect(() =>
        schema.parse('ffa3bb2b-fd75-ffff-ffff-facab27ddcea/')
      ).toThrow(z.ZodError);
      expect(() => schema.parse('ffa3bb2bfd75fffffffffacab27ddcea')).toThrow(
        z.ZodError
      );
    });
  });

  describe('base64', () => {
    it('should accept valid base64', async () => {
      const schema = z.base64({ rawLength: 4 });
      expect(() => schema.parse('dGVzdA==')).not.toThrow();
    });

    it('should reject invalid padding', async () => {
      const schema = z.base64();
      expect(() => schema.parse('dGVzdA')).toThrow(z.ZodError);
    });

    it('should reject non-base64', async () => {
      const schema = z.base64();
      expect(() => schema.parse('caffee')).toThrow(z.ZodError);
    });
    it('should reject too small strings', async () => {
      const schema = z.base64({ min: 100 });
      expect(() => schema.parse('dGVzdA==')).toThrow(z.ZodError);
    });
    it('should reject too long strings', async () => {
      const schema = z.base64({ max: 3 });
      expect(() => schema.parse('dGVzdA==')).toThrow(z.ZodError);
    });

    it('should reject invalid raw lengths', async () => {
      const schema = z.base64({ rawLength: 5 });
      expect(() => schema.parse('dGVzdA==')).toThrow(z.ZodError);
    });
  });
});
