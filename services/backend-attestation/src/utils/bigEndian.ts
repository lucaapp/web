export function typedArrayToBuffer(array: Uint8Array): ArrayBuffer {
  return array.buffer.slice(
    array.byteOffset,
    array.byteLength + array.byteOffset
  );
}

export function convertBigEndian16BitBufferToNumber(
  typedArray: Uint8Array
): number {
  const buffer = typedArrayToBuffer(typedArray);
  const view = new DataView(buffer, 0);
  const value = view.getUint16(0);

  return Number(value);
}

export function convertBigEndian32BitBufferToNumber(
  typedArray: Uint8Array
): number {
  const buffer = typedArrayToBuffer(typedArray);
  const view = new DataView(buffer, 0);
  const value = view.getUint32(0);

  return Number(value);
}
