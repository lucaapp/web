import config from 'config';
import moment from 'moment';
import { randomBytes } from 'crypto';
import { database, Nonce } from 'database';

const NONCE_MAX_AGE = config.get('nonce.maxAgeMs');

export const checkAndInvalidateNonce = async (
  nonce: string
): Promise<boolean> => {
  return database.transaction(async transaction => {
    const nonceInstance = await Nonce.findOne({
      where: { nonce },
      transaction,
    });

    if (!nonceInstance) {
      return false;
    }

    await nonceInstance.destroy({ transaction });

    if (moment().diff(moment(nonceInstance.createdAt)) > NONCE_MAX_AGE) {
      return false;
    }

    return true;
  });
};

export const createAndGetNewNonce = async (): Promise<string> => {
  const utf8Nonce = randomBytes(32).toString('base64');

  await Nonce.create({ nonce: utf8Nonce });

  return utf8Nonce;
};
