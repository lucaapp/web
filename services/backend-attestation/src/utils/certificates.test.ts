// eslint-disable-next-line node/no-unpublished-import
import { useFakeTimers } from 'sinon';
import {
  parsePKIJSCertificateFromPEM,
  verifyCertificateChain,
} from './certificates';

const CERTIFICATE_CHAIN_0_LEAF = `-----BEGIN CERTIFICATE-----
MIIC4TCCAmegAwIBAgIGAX8C6LbAMAoGCCqGSM49BAMCME8xIzAhBgNVBAMMGkFw
cGxlIEFwcCBBdHRlc3RhdGlvbiBDQSAxMRMwEQYDVQQKDApBcHBsZSBJbmMuMRMw
EQYDVQQIDApDYWxpZm9ybmlhMB4XDTIyMDIxNTE0MjExOVoXDTIyMDIxODE0MjEx
OVowgZExSTBHBgNVBAMMQDdlMWQyMjRlZGMzODBmNzNiMzRlODRjMzMzOGZlNDIy
NzNiODE3NzBhZGUzOTcwNzc2MzFmMDA0NjVmM2EyZmYxGjAYBgNVBAsMEUFBQSBD
ZXJ0aWZpY2F0aW9uMRMwEQYDVQQKDApBcHBsZSBJbmMuMRMwEQYDVQQIDApDYWxp
Zm9ybmlhMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEfm+1SMspoUsPTx9D+hZr
3BJd1+/h38pdHUN/rf7CxKDFJL+6yNV27PU0u0LPqc4n7oGsaCOc9Lmfa4ea3JJm
wqOB6zCB6DAMBgNVHRMBAf8EAjAAMA4GA1UdDwEB/wQEAwIE8DB8BgkqhkiG92Nk
CAUEbzBtpAMCAQq/iTADAgEBv4kxAwIBAL+JMgMCAQG/iTMDAgEBv4k0JAQiNzQ2
NDMyMjlZNC5kZS5jdWx0dXJlNGxpZmUubHVjYS5wMqUGBARza3Mgv4k2AwIBBb+J
NwMCAQC/iTkDAgEAv4k6AwIBADAVBgkqhkiG92NkCAcECDAGv4p4AgQAMDMGCSqG
SIb3Y2QIAgQmMCShIgQgh2pB4IXr75bOQ+2zC10mQzM1cRECZdc55QeYU1zejn0w
CgYIKoZIzj0EAwIDaAAwZQIxAMNz5zOaTWP+3WNNoOL0gwMoEuQM2Xr280HF/XWg
sZ11JMF5IJd1xRIFfiaMII07sAIwWSmVmElfjMj6EugSp0cOqAQiYdhELIFYB6Y5
wvHDZRmq6p0maDaC3fsTfKFrpeMH
-----END CERTIFICATE-----`;
const CERTIFICATE_CHAIN_0_INTERMEDIATE = `-----BEGIN CERTIFICATE-----
MIICQzCCAcigAwIBAgIQCbrF4bxAGtnUU5W8OBoIVDAKBggqhkjOPQQDAzBSMSYw
JAYDVQQDDB1BcHBsZSBBcHAgQXR0ZXN0YXRpb24gUm9vdCBDQTETMBEGA1UECgwK
QXBwbGUgSW5jLjETMBEGA1UECAwKQ2FsaWZvcm5pYTAeFw0yMDAzMTgxODM5NTVa
Fw0zMDAzMTMwMDAwMDBaME8xIzAhBgNVBAMMGkFwcGxlIEFwcCBBdHRlc3RhdGlv
biBDQSAxMRMwEQYDVQQKDApBcHBsZSBJbmMuMRMwEQYDVQQIDApDYWxpZm9ybmlh
MHYwEAYHKoZIzj0CAQYFK4EEACIDYgAErls3oHdNebI1j0Dn0fImJvHCX+8XgC3q
s4JqWYdP+NKtFSV4mqJmBBkSSLY8uWcGnpjTY71eNw+/oI4ynoBzqYXndG6jWaL2
bynbMq9FXiEWWNVnr54mfrJhTcIaZs6Zo2YwZDASBgNVHRMBAf8ECDAGAQH/AgEA
MB8GA1UdIwQYMBaAFKyREFMzvb5oQf+nDKnl+url5YqhMB0GA1UdDgQWBBQ+410c
BBmpybQx+IR01uHhV3LjmzAOBgNVHQ8BAf8EBAMCAQYwCgYIKoZIzj0EAwMDaQAw
ZgIxALu+iI1zjQUCz7z9Zm0JV1A1vNaHLD+EMEkmKe3R+RToeZkcmui1rvjTqFQz
97YNBgIxAKs47dDMge0ApFLDukT5k2NlU/7MKX8utN+fXr5aSsq2mVxLgg35BDhv
eAe7WJQ5tw==
-----END CERTIFICATE-----`;
const CERTIFICATE_CHAIN_0_CA = `-----BEGIN CERTIFICATE-----
MIICITCCAaegAwIBAgIQC/O+DvHN0uD7jG5yH2IXmDAKBggqhkjOPQQDAzBSMSYw
JAYDVQQDDB1BcHBsZSBBcHAgQXR0ZXN0YXRpb24gUm9vdCBDQTETMBEGA1UECgwK
QXBwbGUgSW5jLjETMBEGA1UECAwKQ2FsaWZvcm5pYTAeFw0yMDAzMTgxODMyNTNa
Fw00NTAzMTUwMDAwMDBaMFIxJjAkBgNVBAMMHUFwcGxlIEFwcCBBdHRlc3RhdGlv
biBSb290IENBMRMwEQYDVQQKDApBcHBsZSBJbmMuMRMwEQYDVQQIDApDYWxpZm9y
bmlhMHYwEAYHKoZIzj0CAQYFK4EEACIDYgAERTHhmLW07ATaFQIEVwTtT4dyctdh
NbJhFs/Ii2FdCgAHGbpphY3+d8qjuDngIN3WVhQUBHAoMeQ/cLiP1sOUtgjqK9au
Yen1mMEvRq9Sk3Jm5X8U62H+xTD3FE9TgS41o0IwQDAPBgNVHRMBAf8EBTADAQH/
MB0GA1UdDgQWBBSskRBTM72+aEH/pwyp5frq5eWKoTAOBgNVHQ8BAf8EBAMCAQYw
CgYIKoZIzj0EAwMDaAAwZQIwQgFGnByvsiVbpTKwSga0kP0e8EeDS4+sQmTvb7vn
53O5+FRXgeLhpJ06ysC5PrOyAjEAp5U4xDgEgllF7En3VcE3iexZZtKeYnpqtijV
oyFraWVIyd/dganmrduC1bmTBGwD
-----END CERTIFICATE-----`;
const CERTIFICATE_CHAIN_1 = `-----BEGIN CERTIFICATE-----
MIIF3DCCBMSgAwIBAgIQDK+Oqsay+j9ysFneQ1N7djANBgkqhkiG9w0BAQsFADBG
MQswCQYDVQQGEwJVUzEPMA0GA1UEChMGQW1hem9uMRUwEwYDVQQLEwxTZXJ2ZXIg
Q0EgMUIxDzANBgNVBAMTBkFtYXpvbjAeFw0yMTA4MzAwMDAwMDBaFw0yMjA5Mjgy
MzU5NTlaMB4xHDAaBgNVBAMTE3d3dy5zdWVkZGV1dHNjaGUuZGUwggEiMA0GCSqG
SIb3DQEBAQUAA4IBDwAwggEKAoIBAQCoksTAOTrZET3wu5TrADBzCJ3akGbIZsvH
P4lQ9TECGUO1ufjMXnoKMHXMXKG113/ZqsOUUnH0HZx0FDjBqHGEbDPkNsqAbdrW
M4h0ksKZ3FxW4m8se22XMwMrnFJCV0hlc9/CO36KHv2S2pRxyYcA1jZeb10tpGdO
x4BEdgY1rtu+qwEJLQdLDVnzGQbnOeW11P0uyPFgdv0A2bEUTGKex+j0+xwDZWH4
hy509Gx6u+txfLSzwhTdaU41nQCbTYgKOR5jNzkX+bjZ8K7GyMtxwYFpLO64oNiB
PQ6k0+UrPZTlCfqjKbws0CXE7+6kIOTG7AOHH8PFCJNnswHS1ZNtAgMBAAGjggLs
MIIC6DAfBgNVHSMEGDAWgBRZpGYGUqB7lZI8o5QHJ5Z0W/k90DAdBgNVHQ4EFgQU
W5rznzmOOMzfc0SxDrbTLQU0MTYwHgYDVR0RBBcwFYITd3d3LnN1ZWRkZXV0c2No
ZS5kZTAOBgNVHQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUF
BwMCMDsGA1UdHwQ0MDIwMKAuoCyGKmh0dHA6Ly9jcmwuc2NhMWIuYW1hem9udHJ1
c3QuY29tL3NjYTFiLmNybDATBgNVHSAEDDAKMAgGBmeBDAECATB1BggrBgEFBQcB
AQRpMGcwLQYIKwYBBQUHMAGGIWh0dHA6Ly9vY3NwLnNjYTFiLmFtYXpvbnRydXN0
LmNvbTA2BggrBgEFBQcwAoYqaHR0cDovL2NydC5zY2ExYi5hbWF6b250cnVzdC5j
b20vc2NhMWIuY3J0MAwGA1UdEwEB/wQCMAAwggF+BgorBgEEAdZ5AgQCBIIBbgSC
AWoBaAB2ACl5vvCeOTkh8FZzn2Old+W+V32cYAr4+U1dJlwlXceEAAABe5UI4ycA
AAQDAEcwRQIhAJkxBkppaiCb37xdGWi2FYO/sAeDdI/bvvNmcn+ZFuLkAiA2kqDQ
TMJzTRzshPVkYprvkurrcFSj8KEK6kCilryZMwB3AFGjsPX9AXmcVm24N3iPDKR6
zBsny/eeiEKaDf7UiwXlAAABe5UI44AAAAQDAEgwRgIhALrORnzWb0Z2XC1xMHCA
bfla4R2R7x+Hv3VYiBoH15t7AiEAkx5blBCLNtks/+vs8VW000pfbxXKQJ2Eysuc
yteOgLcAdQBByMqx3yJGShDGoToJQodeTjGLGwPr60vHaPCQYpYG9gAAAXuVCOMM
AAAEAwBGMEQCIAWO6FB1Enab8EwEItMV1ki0K1xPTAAq9vXXPlJbFl8nAiBKR57S
UNDWph0wQLPFNOGKXWDubfKbI4bmz7EZzU2QATANBgkqhkiG9w0BAQsFAAOCAQEA
XjCiyyi6dFJ140xTtF1Ptm7lG5A3jdNEQmyOCeR8yeOD32g9cICRjXLYLqpVw3Ih
Q+k4hfqoYqqoM6ep98d1Ai83/Y6My0uLk0cHk1ginNpBJpM8beUCKV6K4dLyCqML
z/v2UmAyZWJUXLZcDeeiatA7J7/BOqRmgh3vZvjL1qfGEiG7gn46x/ZXb1dLC2Y1
qQYzKJ89jHWLnYHsW/JNAhKa1BHPTVApI+cgtwuaLDKIEGOBdBEmGSlAzoEsfv/Z
cr5Q8JB23nyqCgIX8J1b6uQ1k23uhh5/Hr6wNJuDV+vePR6oiUSgfCzENdFkfrtr
jvIpvxqZj69IbyM200u/SQ==
-----END CERTIFICATE-----`;

describe('verifyCertificateChain()', () => {
  describe('when calling with single certificate that is valid', () => {
    it('does not throw', async () => {
      useFakeTimers(new Date(2022, 1, 17));

      await expect(
        verifyCertificateChain({
          leafCertificate: parsePKIJSCertificateFromPEM(CERTIFICATE_CHAIN_0_CA)
            .certificate,
          intermediateCertificates: [],
          trustedCertificates: [
            parsePKIJSCertificateFromPEM(CERTIFICATE_CHAIN_0_CA).certificate,
          ],
        })
      ).resolves.toEqual(undefined);
    });
  });

  describe('when calling with correct chain of three certificates', () => {
    it('does not throw', async () => {
      useFakeTimers(new Date(2022, 1, 17));

      await verifyCertificateChain({
        leafCertificate: parsePKIJSCertificateFromPEM(CERTIFICATE_CHAIN_0_LEAF)
          .certificate,
        intermediateCertificates: [
          parsePKIJSCertificateFromPEM(CERTIFICATE_CHAIN_0_INTERMEDIATE)
            .certificate,
          parsePKIJSCertificateFromPEM(CERTIFICATE_CHAIN_0_CA).certificate,
        ],
        trustedCertificates: [
          parsePKIJSCertificateFromPEM(CERTIFICATE_CHAIN_0_CA).certificate,
        ],
      });
    });
  });

  describe('when calling with incorrect chain of two certificates', () => {
    it('throws', async () => {
      useFakeTimers(new Date(2022, 1, 17));

      await expect(
        verifyCertificateChain({
          leafCertificate: parsePKIJSCertificateFromPEM(
            CERTIFICATE_CHAIN_0_LEAF
          ).certificate,
          intermediateCertificates: [],
          trustedCertificates: [
            parsePKIJSCertificateFromPEM(CERTIFICATE_CHAIN_1).certificate,
          ],
        })
      ).rejects.toThrowError('No valid certificate paths found');
    });
  });

  describe('when calling with single leaf certificate, no intermediates and no trusted certificates', () => {
    it('throws', async () => {
      useFakeTimers(new Date(2022, 1, 17));

      await expect(
        verifyCertificateChain({
          leafCertificate: parsePKIJSCertificateFromPEM(
            CERTIFICATE_CHAIN_0_LEAF
          ).certificate,
          intermediateCertificates: [],
          trustedCertificates: [],
        })
      ).rejects.toThrowError('No valid certificate paths found');
    });
  });

  describe('when calling with leaf that is no longer valid', () => {
    it('throws', async () => {
      useFakeTimers(new Date(3000, 1, 1));

      // eslint-disable-next-line sonarjs/no-identical-functions
      await expect(
        verifyCertificateChain({
          leafCertificate: parsePKIJSCertificateFromPEM(CERTIFICATE_CHAIN_0_CA)
            .certificate,
          intermediateCertificates: [],
          trustedCertificates: [
            parsePKIJSCertificateFromPEM(CERTIFICATE_CHAIN_0_CA).certificate,
          ],
        })
      ).rejects.toThrowError('not yet valid or expired');
    });
  });

  describe('when calling with leaf that is not yet valid', () => {
    it('throws', async () => {
      useFakeTimers(new Date(2000, 1, 1));

      // eslint-disable-next-line sonarjs/no-identical-functions
      await expect(
        verifyCertificateChain({
          leafCertificate: parsePKIJSCertificateFromPEM(CERTIFICATE_CHAIN_0_CA)
            .certificate,
          intermediateCertificates: [],
          trustedCertificates: [
            parsePKIJSCertificateFromPEM(CERTIFICATE_CHAIN_0_CA).certificate,
          ],
        })
      ).rejects.toThrow('not yet valid or expired');
    });
  });

  const pixelCertificateSample = {
    leaf: parsePKIJSCertificateFromPEM(`-----BEGIN CERTIFICATE-----
    MIICnTCCAkGgAwIBAgIBATAMBggqhkjOPQQDAgUAMD8xEjAQBgNVBAwMCVN0cm9uZ0JveDEpMCcG
    A1UEBRMgMDY4NDJmODRiY2JhZGJkMTk2NDA1YmZkNmE2MzQ5ZWIwHhcNNzAwMTAxMDAwMDAwWhcN
    NDgwMTAxMDAwMDAwWjAfMR0wGwYDVQQDExRBbmRyb2lkIEtleXN0b3JlIEtleTBZMBMGByqGSM49
    AgEGCCqGSM49AwEHA0IABBHABnrmEWfbosmzbcMUAh/2NwAZedNNP5ACfwvHjX+rRMCkGeqgNm77
    rpyVsSxDAA9zE44u1343tlaBNipYrhKjggFKMIIBRjAOBgNVHQ8BAf8EBAMCB4AwggEyBgorBgEE
    AdZ5AgERBIIBIjCCAR4CAWQKAQICAWQKAQIED3NhbXBsZUNoYWxsZW5nZQQAMFi/hUVUBFIwUDEq
    MCgEI2Fpc2VjLmtleWF0dGVzdGF0aW9uc2FtcGxlY29sbGVjdG9yAgEBMSIEII74INRjXmHRlKBV
    E+oC4kluPLbuEq7tcQ1Eh4RNnzioMIGioQUxAwIBAqIDAgEDowQCAgEApQUxAwIBBKoDAgEBv4N4
    AwIBAr+FPgMCAQC/hUBMMEoEIA9udcgBg7XewHSwBU1CcemTievksTawgZ3h8VC6D/nXAQH/CgEA
    BCAQ8FuaGgNMaEMEqXMgVhd95hBhHYH7DapxdHHvLdRvYr+FQQUCAwHUwL+FQgUCAwMV2b+FTgYC
    BAE0iMm/hU8GAgQBNIjEMAwGCCqGSM49BAMCBQADSAAwRQIgI1WDEtiueZbjeEz8+mMdNWCQZe6X
    /TUqSUMWd+cyf1ACIQDZ++y5JJ3Ris6SHbXAXloRdcCIo5S1XuQP8O5+CICmUQ==
    -----END CERTIFICATE-----`).certificate,
    intermediate1Valid: parsePKIJSCertificateFromPEM(`-----BEGIN CERTIFICATE-----
    MIICADCCAYWgAwIBAgIQVpokAbqSODCb2sAGwqwlHTAKBggqhkjOPQQDAjA/MRIwEAYDVQQMDAlT
    dHJvbmdCb3gxKTAnBgNVBAUTIGYzZGYxOTdiMTQxYzkzNDdjN2RhZjAzNzVlYzBmOTQ5MB4XDTIw
    MDkxMTE4MDI0NFoXDTMwMDkwOTE4MDI0NFowPzESMBAGA1UEDAwJU3Ryb25nQm94MSkwJwYDVQQF
    EyAwNjg0MmY4NGJjYmFkYmQxOTY0MDViZmQ2YTYzNDllYjBZMBMGByqGSM49AgEGCCqGSM49AwEH
    A0IABDmYmvJ55PRAZABxF0WLxqMrTVYFZPBbqDomRAAEhhOTZTNx8rxaMPXVPpaLYvnNUE5pX/NS
    5NBsSlI8iORw/pujYzBhMB0GA1UdDgQWBBRRJG6wMccStm7GLMxYkLTeG74jSTAfBgNVHSMEGDAW
    gBRu5hHfcEbVuzRtjS2OBjcfUnGrTTAPBgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwICBDAK
    BggqhkjOPQQDAgNpADBmAjEA7t9apSnOE6WgDTWRmkbrzswZrmONiQfV6KKmOUNkB7Sz9Zkd6Nwl
    T5AJuq2Fno2HAjEAul3OMVL2XjXKqAoRHXpC3Xqg1sBGAfeBq+1iT0W6hBNjGpHj9rl9mQqwO6s5
    Km9j
    -----END CERTIFICATE-----`).certificate,
    intermediate1Invalid: parsePKIJSCertificateFromPEM(`-----BEGIN CERTIFICATE-----
    MIICADCCAYWgAwIBAgIQVpokAbqSODCb2sAGwqwlHTAKBggqhkjOPQQDAjA/MRIw
    EAYDVQQMDAlTdHJvbmdCb3gxKTAnBgNVBAUTIGYzZGYxOTdiMTQxYzkzNDdjN2Rh
    ZjAzNzVlYzBmOTQ5MB4XDTIwMDkxMTE4MDI0NFoXDTMwMDkwOTE4MDI0NFowPzES
    MBAGA1UEDAwJU3Ryb25nQm94MSkwJwYDVQQFEyAwNjg0MmY4NGJjYmFkYmQxOTY0
    MDViZmQ2YTYzNDllYjBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABDmYmvJ55PRA
    ZABxF0WLxqMrTVYFZPBbqDomRAAEhhOTZTNx8rxaMPXVPpaLYvnNUE5pX/NS5NBs
    SlI8iORw/pujYzBhMB0GA1UdDgQWBBRRJG6wMccStm7GLMxYkLTeG74jSTAfBgNV
    HSMEGDAWgBRy+KPj1VpLPCxn6xMH90MjCh216TAPBgNVHRMBAf8EBTADAQH/MA4G
    A1UdDwEB/wQEAwICBDAKBggqhkjOPQQDAgNpADBmAjEAlcqu72MMTb1nEM01FAUU
    IdMSPQnbrzhzHamygU/MensCxb2k6iGndbtLiA60spsAAjEA7SOdGvAIe5V8JA20
    2L0Gg/CDyQ5YYMTNYlsR1MgtvgkSRdwCRWKl+L0U85b7eNVN
    -----END CERTIFICATE-----`).certificate,
    intermediate2: parsePKIJSCertificateFromPEM(`-----BEGIN CERTIFICATE-----
    MIIDmTCCAYGgAwIBAgIQBg2Ja9xgpXallHvgiV9ZiTANBgkqhkiG9w0BAQsFADAbMRkwFwYDVQQF
    ExBmOTIwMDllODUzYjZiMDQ1MB4XDTIwMDkxMTE4MDIyMVoXDTMwMDkwOTE4MDIyMVowPzESMBAG
    A1UEDAwJU3Ryb25nQm94MSkwJwYDVQQFEyBmM2RmMTk3YjE0MWM5MzQ3YzdkYWYwMzc1ZWMwZjk0
    OTB2MBAGByqGSM49AgEGBSuBBAAiA2IABHJGYGgFBHogBxkYllZN3Ckx4N40qmD72LhOxrVE73Iq
    hDuP7naPKmEdfcF4U4lzb/FzFPZ/fsHmSE/DSwHoST3AxQwq9g0xx/m1p/aWPVq8Rco2uhSgsnLM
    bGz28V6jY6NjMGEwHQYDVR0OBBYEFG7mEd9wRtW7NG2NLY4GNx9ScatNMB8GA1UdIwQYMBaAFDZh
    4QB8iAUJUYtEbEf/GkzJ6k8SMA8GA1UdEwEB/wQFMAMBAf8wDgYDVR0PAQH/BAQDAgIEMA0GCSqG
    SIb3DQEBCwUAA4ICAQA4KNgGY+XZsN9BgD53T//kTlyasN6bBnyajVac3O9pvYgLarushTyz8xfb
    tio3WEy5484QiWKhXbVTGFjt9XeH0drHe2dYBKroYxWGsI0NdY3trA1uMx80oWhvEuFxvAQHV//U
    Hd0YQCVuXjDQTP8Fdr8n8eYPBd5fxQol+pWY0UjbPgpf3Pz5KkJBEp5aijoLurlANL11d1pX8fot
    CCLhh0/QArmFbAxTOQrCIRI+2yX0D1DwazeWrZLSD/hPkM1/uEmleE45XEUXy2MLdixtEiI6yy+A
    /wQMizMoVhVuXVUZZiDLHPtNTjxQs5ZXDyDyxpJa0X5WckbttqRuEgd6iMWVhhMatfV7e8jXmt72
    BJzSZWOAf2hU2Xes6oIGjFah6AIFcMoqKfBzClrEbkiES+qLSXuBjDNkI3jXcU775OI/Q+gK19yD
    tzR+wl1uP8ivGnRixTc96r/i4AwTyiijvWW6QQcKAwTJSniD5LS5V/jB5ml6oX26fFhQteRPAsLX
    QfD11adkh7suquWL3ZV1GNSH+89rbGtO1wSl98cHnUu30dXSxHBAPK+N3S7sz8YaM1CBpb4eUCj6
    ASR6Hr/UxW9GEnajLq2KPYf8MqG0ECgxQf9PDBBkTnZfFMRs4/FNtaUu10dOjj7ltEfrS0rHjgYE
    ykrM9WmORlkUk9NsoQ==
    -----END CERTIFICATE-----`).certificate,
    root: parsePKIJSCertificateFromPEM(`-----BEGIN CERTIFICATE-----
    MIIFYDCCA0igAwIBAgIJAOj6GWMU0voYMA0GCSqGSIb3DQEBCwUAMBsxGTAXBgNVBAUTEGY5MjAw
    OWU4NTNiNmIwNDUwHhcNMTYwNTI2MTYyODUyWhcNMjYwNTI0MTYyODUyWjAbMRkwFwYDVQQFExBm
    OTIwMDllODUzYjZiMDQ1MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAr7bHgiuxpwHs
    K7Qui8xUFmOr75gvMsd/dTEDDJdSSxtf6An7xyqpRR90PL2abxM1dEqlXnf2tqw1Ne4Xwl5jlRfd
    nJLmN0pTy/4lj4/7tv0Sk3iiKkypnEUtR6WfMgH0QZfKHM1+di+y9TFRtv6y//0rb+T+W8a9nsNL
    /ggjnar86461qO0rOs2cXjp3kOG1FEJ5MVmFmBGtnrKpa73XpXyTqRxB/M0n1n/W9nGqC4FSYa04
    T6N5RIZGBN2z2MT5IKGbFlbC8UrW0DxW7AYImQQcHtGl/m00QLVWutHQoVJYnFPlXTcHYvASLu+R
    hhsbDmxMgJJ0mcDpvsC4PjvB+TxywElgS70vE0XmLD+OJtvsBslHZvPBKCOdT0MS+tgSOIfga+z1
    Z1g7+DVagf7quvmag8jfPioyKvxnK/EgsTUVi2ghzq8wm27ud/mIM7AY2qEORR8Go3TVB4HzWQgp
    Zrt3i5MIlCaY504LzSRiigHCzAPlHws+W0rB5N+er5/2pJKnfBSDiCiFAVtCLOZ7gLiMm0jhO2B6
    tUXHI/+MRPjy02i59lINMRRev56GKtcd9qO/0kUJWdZTdA2XoS82ixPvZtXQpUpuL12ab+9EaDK8
    Z4RHJYYfCT3Q5vNAXaiWQ+8PTWm2QgBR/bkwSWc+NpUFgNPN9PvQi8WEg5UmAGMCAwEAAaOBpjCB
    ozAdBgNVHQ4EFgQUNmHhAHyIBQlRi0RsR/8aTMnqTxIwHwYDVR0jBBgwFoAUNmHhAHyIBQlRi0Rs
    R/8aTMnqTxIwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAYYwQAYDVR0fBDkwNzA1oDOg
    MYYvaHR0cHM6Ly9hbmRyb2lkLmdvb2dsZWFwaXMuY29tL2F0dGVzdGF0aW9uL2NybC8wDQYJKoZI
    hvcNAQELBQADggIBACDIw41L3KlXG0aMiS//cqrG+EShHUGo8HNsw30W1kJtjn6UBwRM6jnmiwfB
    Pb8VA91chb2vssAtX2zbTvqBJ9+LBPGCdw/E53Rbf86qhxKaiAHOjpvAy5Y3m00mqC0w/Zwvju1t
    wb4vhLaJ5NkUJYsUS7rmJKHHBnETLi8GFqiEsqTWpG/6ibYCv7rYDBJDcR9W62BW9jfIoBQcxUCU
    JouMPH25lLNcDc1ssqvC2v7iUgI9LeoM1sNovqPmQUiG9rHli1vXxzCyaMTjwftkJLkf6724DFhu
    Kug2jITV0QkXvaJWF4nUaHOTNA4uJU9WDvZLI1j83A+/xnAJUucIv/zGJ1AMH2boHqF8CY16LpsY
    gBt6tKxxWH00XcyDCdW2KlBCeqbQPcsFmWyWugxdcekhYsAWyoSf818NUsZdBWBaR/OukXrNLfkQ
    79IyZohZbvabO/X+MVT3rriAoKc8oE2Uws6DF+60PV7/WIPjNvXySdqspImSN78mflxDqwLqRBYk
    A3I75qppLGG9rp7UCdRjxMl8ZDBld+7yvHVgt1cVzJx9xnyGCC23UaicMDSXYrB4I4WHXPGjxhZu
    CuPBLTdOLU8YRvMYdEvYebWHMpvwGCF6bAx3JBpIeOQ1wDB5y0USicV3YgYGmi+NZfhA4URSh77Y
    d6uuJOJENRaNVTzk
    -----END CERTIFICATE-----`).certificate,
  };

  describe('when running the SSE/Pixel certificate sample cases', () => {
    describe('when passing valid leaf, intermediates in order 1 > 2 and trusted root', () => {
      it('does not throw', async () => {
        useFakeTimers(new Date(2022, 1, 17));

        await verifyCertificateChain({
          leafCertificate: pixelCertificateSample.leaf,
          intermediateCertificates: [
            pixelCertificateSample.intermediate1Valid,
            pixelCertificateSample.intermediate2,
          ],
          trustedCertificates: [pixelCertificateSample.root],
        });
      });
    });

    describe('when passing valid leaf, intermediates in order 2 > 1 and trusted root', () => {
      it('does not throw', async () => {
        useFakeTimers(new Date(2022, 1, 17));

        await verifyCertificateChain({
          leafCertificate: pixelCertificateSample.leaf,
          intermediateCertificates: [
            pixelCertificateSample.intermediate2,
            pixelCertificateSample.intermediate1Valid,
          ],
          trustedCertificates: [pixelCertificateSample.root],
        });
      });
    });

    describe('when intermediates contain an additional unrelated/unnecessary certificate', () => {
      it('does not throw', async () => {
        useFakeTimers(new Date(2022, 1, 17));

        await verifyCertificateChain({
          leafCertificate: pixelCertificateSample.leaf,
          intermediateCertificates: [
            pixelCertificateSample.intermediate2,
            pixelCertificateSample.intermediate1Valid,
            parsePKIJSCertificateFromPEM(CERTIFICATE_CHAIN_0_INTERMEDIATE)
              .certificate,
          ],
          trustedCertificates: [pixelCertificateSample.root],
        });
      });
    });

    describe('when trusted root is also contained in intermediates', () => {
      it('does not throw', async () => {
        useFakeTimers(new Date(2022, 1, 17));

        await verifyCertificateChain({
          leafCertificate: pixelCertificateSample.leaf,
          intermediateCertificates: [
            pixelCertificateSample.intermediate2,
            pixelCertificateSample.intermediate1Valid,
            pixelCertificateSample.root,
          ],
          trustedCertificates: [pixelCertificateSample.root],
        });
      });
    });

    describe('when intermediate 2 is passed as leaf cert (and leaf cert is passed as intermediate)', () => {
      it('does not throw (is still valid leaf to root)', async () => {
        useFakeTimers(new Date(2022, 1, 17));

        await verifyCertificateChain({
          leafCertificate: pixelCertificateSample.intermediate2,
          intermediateCertificates: [
            pixelCertificateSample.leaf,
            pixelCertificateSample.intermediate1Valid,
            pixelCertificateSample.root,
          ],
          trustedCertificates: [pixelCertificateSample.root],
        });
      });
    });

    describe('when intermediate 1 is swapped with a different, self-signed certificate', () => {
      it('throws', async () => {
        useFakeTimers(new Date(2022, 1, 17));

        await expect(
          verifyCertificateChain({
            leafCertificate: pixelCertificateSample.leaf,
            intermediateCertificates: [
              pixelCertificateSample.intermediate1Invalid,
              pixelCertificateSample.intermediate2,
              pixelCertificateSample.root,
            ],
            trustedCertificates: [pixelCertificateSample.root],
          })
        ).rejects.toThrow('No valid certificate paths found');
      });
    });
  });
});
