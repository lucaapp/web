const config = require('config');
const redis = require('redis');
const { promisify } = require('util');
const logger = require('./logger').default;
const lifecycle = require('./lifecycle');

const RETRY_INTERVAL_MS = 500;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const retryStrategy = (info: any) => {
  logger.warn(info, `retrying redis connection`);
  return RETRY_INTERVAL_MS;
};

type RedisClient = ReturnType<typeof redis.createClient>;

let client: RedisClient | null = null;

export const getClient = (): RedisClient => {
  if (client) {
    return client;
  }

  client = redis.createClient({
    host: config.get('redis.hostname'),
    password: config.get('redis.password'),
    enable_offline_queue: true,
    detect_buffers: true,
    db: config.get('redis.database'),
    retry_strategy: retryStrategy,
  });
  const promiseClient = {
    quit: promisify(client.quit).bind(client),
    set: promisify(client.set).bind(client),
    get: promisify(client.get).bind(client),
  };
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  client.on('error', (error: any) => {
    logger.error(error);
  });

  lifecycle.registerShutdownHandler(async () => {
    if (!client.connected) {
      return;
    }

    logger.info('closing redis connection');
    await promiseClient.quit();
    logger.info('redis connection closed');
  });

  return client;
};
