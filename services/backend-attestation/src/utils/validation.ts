/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { z as zod } from 'zod';
import validator from 'validator';

export const z = {
  ...zod,
  uuid: () =>
    zod
      .string()
      .length(36)
      .refine(value => validator.isUUID(value, 4)),
  unixTimestamp: () => zod.number().int().positive(),
  base64: ({
    min,
    max,
    length,
    rawLength,
  }: {
    min?: number;
    max?: number;
    length?: number;
    rawLength?: number;
  } = {}) =>
    zod
      .string()
      .min(min as number)
      .max(max as number)
      .length(length as number)
      .refine(value => {
        if (!validator.isBase64(value)) return false;
        if (!rawLength) return true;

        return Buffer.from(value, 'base64').length === rawLength;
      }),
  bigIntegerString: () =>
    zod
      .string()
      .max(2048)
      .refine(value =>
        validator.isInt(value, {
          allow_leading_zeroes: false,
        })
      ),
  ecPublicKey: () => z.base64({ length: 88, rawLength: 65 }),
  ecCompressedPublicKey: () => z.base64({ length: 44, rawLength: 33 }),
  ecSignature: () => z.base64({ max: 120 }),
  rsaPublicKey: () => z.base64({ max: 2048 }),
  compactJWS: () => z.string().refine(value => validator.isJWT(value)),
  iv: () => z.base64({ length: 24, rawLength: 16 }),
  mac: () => z.base64({ length: 44, rawLength: 32 }),
  powSolution: () =>
    z.object({
      id: z.uuid(),
      w: z.bigIntegerString(),
    }),
};
