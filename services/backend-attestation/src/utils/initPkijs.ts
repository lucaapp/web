import CryptoEngine from 'pkijs/build/CryptoEngine';
import webcrypto from 'isomorphic-webcrypto';
import { setEngine } from 'pkijs/build/common';

setEngine(
  'newEngine',
  webcrypto,
  // @ts-ignore PKIJS type defs are a pain
  new CryptoEngine({ name: '', crypto: webcrypto, subtle: webcrypto.subtle })
);
