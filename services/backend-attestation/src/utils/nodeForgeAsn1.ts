import { asn1, util } from 'node-forge';
import { nodeForgeBufferToNodeBuffer } from './nodeForgeCompatiblity';

const OID_EC_PUBLIC_KEY = '1.2.840.10045.2.1';

function getMaybeSequenceNodeChildren(asn1Data: asn1.Asn1): null | asn1.Asn1[] {
  if (asn1Data.type !== asn1.Type.SEQUENCE) {
    return null;
  }

  if (!Array.isArray(asn1Data.value)) {
    throw new TypeError(
      `Invalid ASN.1 data: discovered SEQUENCE but value was not array`
    );
  }

  return asn1Data.value;
}

function getOIDNodeValue(asn1Data: asn1.Asn1): string {
  if (typeof asn1Data.value !== 'string') {
    throw new TypeError(
      `Invalid ASN.1 data: discovered OID but value was not string`
    );
  }

  return asn1Data.value;
}

export const getOctetStringNodeBuffer = (asn1Data: asn1.Asn1): Buffer => {
  if (asn1Data.type !== asn1.Type.OCTETSTRING) {
    throw new TypeError(
      `Assertion failed: expected OCTETSTRING (${asn1.Type.OCTETSTRING}) in ASN.1 but got type index ${asn1Data.type}`
    );
  }

  if (typeof asn1Data.value !== 'string') {
    throw new TypeError(
      `Invalid ASN.1 data: discovered OCTETSTRING but value was not string`
    );
  }

  return nodeForgeBufferToNodeBuffer(util.createBuffer(asn1Data.value));
};

export const getSequenceNode = (asn1Data: asn1.Asn1): asn1.Asn1[] => {
  const nodes = getMaybeSequenceNodeChildren(asn1Data);

  if (!nodes) {
    throw new TypeError(
      `Invalid ASN.1 data: expected SEQUENCE but type index was `
    );
  }

  return nodes;
};

export const getBitstringNode = (asn1Data: asn1.Asn1): Buffer => {
  if (asn1Data.type !== asn1.Type.BITSTRING) {
    throw new TypeError(
      `Assertion failed: expected BITSTRING (${asn1.Type.BITSTRING}) in ASN.1 but got type index ${asn1Data.type}`
    );
  }

  const { value } = asn1Data;
  if (typeof value !== 'string') {
    throw new TypeError(
      `Invalid ASN.1 data: discovered BITSTRING but value was not string`
    );
  }
  return nodeForgeBufferToNodeBuffer(util.createBuffer(value));
};

/**
 * This traverses the entire ASN.1 tree and yields each node exactly once. It does this deeply,
 * meaning that it steps into SEQUENCEs and SETs and recursively traverses these, too.
 *
 * @param asn1Data
 * @returns
 */
export function* iterateASN1Nodes(asn1Data: asn1.Asn1): Generator<asn1.Asn1> {
  yield asn1Data;

  if (asn1Data.type === asn1.Type.SEQUENCE) {
    if (!Array.isArray(asn1Data.value)) {
      throw new TypeError(
        `Invalid ASN.1 data: discovered SEQUENCE but value was not array`
      );
    }

    for (const item of asn1Data.value) {
      yield* iterateASN1Nodes(item);
    }
  }

  if (asn1Data.type === asn1.Type.SET) {
    if (!Array.isArray(asn1Data.value)) {
      throw new TypeError(
        `Invalid ASN.1 data: discovered SET but value was not array`
      );
    }

    for (const item of asn1Data.value) {
      yield* iterateASN1Nodes(item);
    }
  }

  if (asn1Data.type === asn1.Type.BITSTRING && Array.isArray(asn1Data.value)) {
    yield* iterateASN1Nodes(asn1Data.value[0]);
  }

  return null;
}

/**
 * For each SEQUENCE that starts with an OID node, this will yield the succeeding item.
 * Example where this is necessary: Apple's device attestation nonce is identified by a non-standard
 * OID, preceeding the nonce. It looks like this: SEQUENCE(OID, OCTET_STRING)
 *
 * @param asn1Data
 */
export function* iterateAllSequenceItemsFollowingOIDs(
  asn1Data: asn1.Asn1
): Generator<{ oid: string; value: asn1.Asn1[] }> {
  for (const asn1Node of iterateASN1Nodes(asn1Data)) {
    const sequenceValues = getMaybeSequenceNodeChildren(asn1Node);
    if (sequenceValues && sequenceValues[0].type === asn1.Type.OID) {
      const oid = asn1.derToOid(
        util.createBuffer(getOIDNodeValue(sequenceValues[0]))
      );
      yield { oid, value: sequenceValues.slice(1) };
    }
  }
}

/**
 * This traverses the ASN.1 to look for nodes that look like this: SEQUENCE(OID, ...nodes). If it
 * finds such, it checks if the OID matches the passed OID argument. If it matches, it returns the
 * succeeding sequence's nodes.
 *
 * See iterateAllSequenceItemsFollowingOIDs() for where this is useful.
 *
 * @param asn1Data
 */
export const findASN1NodeFollowingOID = (
  asn1Data: asn1.Asn1,
  oid: string
): { oid: string; value: asn1.Asn1[] } | null => {
  for (const currentValue of iterateAllSequenceItemsFollowingOIDs(asn1Data)) {
    if (currentValue.oid === oid) {
      return currentValue;
    }
  }

  return null;
};

/**
 * This finds the BITSTRING containing the public key in an ASN.1 structure that encodes an X.509
 * certificate.
 *
 * @param x509ASN1Data
 */
export const findPublicKeyBitStringInX509ASN1 = (
  x509ASN1Data: asn1.Asn1
): Buffer => {
  const level0Sequence = getSequenceNode(x509ASN1Data);
  const level1Sequence = getSequenceNode(level0Sequence[0]);
  const level2Sequence = getSequenceNode(level1Sequence[6]);
  const algorithmIdentifierSequence = getSequenceNode(level2Sequence[0]);

  // Assert algorithm

  const algorithmIdentifierOID = asn1.derToOid(
    util.createBuffer(getOIDNodeValue(algorithmIdentifierSequence[0]))
  );

  if (algorithmIdentifierOID !== OID_EC_PUBLIC_KEY) {
    throw new Error(
      `Discovered unsupported algorithm identifier while decoding X.509: only ecdsaWithSHA256 (${OID_EC_PUBLIC_KEY}) is supported but got ${algorithmIdentifierOID}`
    );
  }

  // Extract public key

  const publicKeyNode = level2Sequence[1];

  if (publicKeyNode.type !== asn1.Type.BITSTRING) {
    throw new Error(
      `X.509 encoding error: expected BITSTRING containing public key but found ${publicKeyNode.type}`
    );
  }

  return (
    getBitstringNode(publicKeyNode)
      // The first byte just states the integer encoding and can be ignored for our case
      // See https://crypto.stackexchange.com/a/30616
      .slice(1)
  );
};
