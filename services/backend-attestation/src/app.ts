import express from 'express';
import 'express-async-errors';
import passport from 'passport';
import { httpLogger } from 'utils/logger';
import { noCache } from '@lucaapp/no-cache';
import { handleError, handleNotFound } from 'middlewares/error';
import requestMetricsMiddleware from 'middlewares/requestMetrics';
import bearerInternalAccessStrategy from 'passport/bearerInternalAccess';

// Routes
import versionRouter from 'routes/version';
import licensesRouter from 'routes/licenses';
import internalRouter from 'routes/internal';
import healthRouter from 'routes/health';
import v1Router from 'routes/v1';

let app: express.Express;

export const getApp = (): express.Express => {
  return app;
};

export const configureHealthChecks = (): express.Express => {
  const healthCheckApp = express();
  healthCheckApp.use(httpLogger);
  healthCheckApp.use(requestMetricsMiddleware);

  healthCheckApp.use('/health', healthRouter);
  healthCheckApp.use('/internal', internalRouter);

  return healthCheckApp;
};

export const configureApp = (): express.Express => {
  app = express();
  const router = express.Router();

  app.disable('x-powered-by');
  app.disable('etag');
  app.enable('strict routing');

  app.set('trust proxy', 2);

  // Passport Strategies
  passport.use('bearer-internalAccess', bearerInternalAccessStrategy);

  // Global Middlewares
  app.use(httpLogger);
  app.use(requestMetricsMiddleware);
  app.use(passport.initialize());
  app.use(noCache);

  app.use('/attestation/api', router);
  app.use(handleNotFound);
  app.use(handleError);

  // Routing
  router.use('/version', versionRouter);
  router.use('/licenses', licensesRouter);
  router.use('/internal', internalRouter);
  router.use('/health', healthRouter);
  router.use('/v1', v1Router);

  return app;
};
