import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'DeviceAttestation',
        'iosAttestationCBOR',
        {
          type: DataTypes.BLOB,
          allowNull: true,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'DeviceAttestation',
        'androidKeyAttestationCertificateChainPEM',
        {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        { transaction }
      );

      await queryInterface.addColumn(
        'DeviceAttestation',
        'androidSafetyNetJWS',
        {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn(
        'DeviceAttestation',
        'iosAttestationCBOR',
        { transaction }
      );

      await queryInterface.removeColumn(
        'DeviceAttestation',
        'androidKeyAttestationCertificateChain',
        { transaction }
      );

      await queryInterface.removeColumn(
        'DeviceAttestation',
        'androidSafetyNetJWS',
        { transaction }
      );
    });
  },
};

export default migration;
