import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addIndex('Nonce', {
      name: 'nonce_by_nonce_value',
      fields: ['nonce'],
      unique: true,
    });
  },

  down: ({ context: queryInterface }) => {
    return queryInterface.removeIndex('Nonce', 'nonce_by_nonce_value');
  },
};

export default migration;
