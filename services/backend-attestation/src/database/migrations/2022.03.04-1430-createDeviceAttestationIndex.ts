import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addIndex('DeviceAttestation', {
      name: 'device_attestation_deviceid',
      fields: ['deviceId'],
      unique: true,
    });
  },

  down: ({ context: queryInterface }) => {
    return queryInterface.removeIndex(
      'DeviceAttestation',
      'device_attestation_deviceid'
    );
  },
};

export default migration;
