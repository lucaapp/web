import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.changeColumn('DeviceAttestation', 'deviceId', {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.changeColumn('DeviceAttestation', 'deviceId', {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
    });
  },
};

export default migration;
