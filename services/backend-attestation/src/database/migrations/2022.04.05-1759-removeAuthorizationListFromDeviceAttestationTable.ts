import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn(
      'DeviceAttestation',
      'androidAuthorizationListASN1'
    );
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.addColumn(
      'DeviceAttestation',
      'androidAuthorizationListASN1',
      {
        type: DataTypes.BLOB,
        allowNull: true,
      }
    );
  },
};

export default migration;
