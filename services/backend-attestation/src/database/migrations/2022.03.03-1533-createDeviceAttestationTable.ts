import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('DeviceAttestation', {
      deviceId: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
      },
      publicKey: {
        type: DataTypes.BLOB,
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      platform: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      iosReceipt: {
        type: DataTypes.BLOB,
        allowNull: true,
      },
      iosSignCount: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      androidAuthorizationListASN1: {
        type: DataTypes.BLOB,
        allowNull: true,
      },
    });
  },

  down: ({ context: queryInterface }) => {
    return queryInterface.dropTable('DeviceAttestation');
  },
};

export default migration;
