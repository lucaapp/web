/* eslint-disable import/no-cycle, max-lines */
import config from 'config';
import {
  Sequelize,
  Options,
  ConnectionError,
  ConnectionRefusedError,
  ConnectionTimedOutError,
} from 'sequelize';
import logger from 'utils/logger';
import client from 'utils/metrics';
import dbConfig from 'database/config';

import { initInternalAccessUser } from './models/internalAccessUser';
import { initIPAddressAllowLists } from './models/ipAddressAllowList';
import { initIPAddressBlockList } from './models/ipAddressBlockList';
import { initIPAddressDenyLists } from './models/ipAddressDenyList';
import { initJobCompletions } from './models/jobCompletions';
import { initNonce } from './models/nonce';
import { initDeviceAttestation } from './models/deviceAttestation';

const environment = config.util.getEnv('NODE_ENV');
const databaseConfig = dbConfig[environment as keyof typeof dbConfig];

export const database = new Sequelize({
  ...(databaseConfig as Partial<Options>),
  logging: (message, time) => logger.debug({ time, message }),
  logQueryParameters: false,
  benchmark: true,
  retry: {
    name: 'database',
    max: 60 * 2,
    backoffBase: 500,
    backoffExponent: 1,
    report: (_message: string, info: { $current: number }) => {
      if (info.$current === 1) return;
      logger.warn(`Trying to connect to database (try #${info.$current})`);
    },
    match: [ConnectionError, ConnectionRefusedError, ConnectionTimedOutError],
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } as any,
});

const counter = new client.Counter({
  name: 'sequelize_table_creation_count',
  help: 'Total database rows created since process start.',
  labelNames: ['tableName'],
});

database.addHook('beforeCreate', instance => {
  counter.inc({
    tableName: instance.constructor.name,
  });
});

database.addHook('beforeBulkCreate', instances => {
  if (instances.length <= 0) return;
  counter.inc(
    {
      tableName: instances[0].constructor.name,
    },
    instances.length
  );
});

export const internalAccessUser = initInternalAccessUser(database);
export const DeviceAttestation = initDeviceAttestation(database);
export const IPAddressAllowList = initIPAddressAllowLists(database);
export const IPAddressBlockList = initIPAddressBlockList(database);
export const IPAddressDenyList = initIPAddressDenyLists(database);
export const JobCompletion = initJobCompletions(database);
export const Nonce = initNonce(database);

const models = {
  IPAddressAllowList,
  IPAddressBlockList,
  IPAddressDenyList,
  JobCompletion,
  Nonce,
};

export type Models = typeof models;
