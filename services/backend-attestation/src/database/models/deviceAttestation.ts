import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

export enum DeviceAttestationPlatform {
  IOS = 'ios',
  ANDROID = 'android',
}

interface CreationAttributes {
  publicKey: Buffer;
  platform: DeviceAttestationPlatform;
  iosReceipt?: Buffer;
  iosSignCount?: number;
  iosAttestationCBOR?: Buffer;
  androidKeyAttestationCertificateChainPEM?: string;
  androidSafetyNetJWS?: string;
}

type Attributes = CreationAttributes & {
  deviceId: string;
  createdAt: Date;
};

export interface DeviceAttestationInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initDeviceAttestation = (
  sequelize: Sequelize
): ModelCtor<DeviceAttestationInstance> => {
  return sequelize.define<DeviceAttestationInstance>(
    'DeviceAttestation',
    {
      deviceId: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
      },
      publicKey: {
        type: DataTypes.BLOB,
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      platform: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      iosSignCount: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      iosReceipt: {
        type: DataTypes.BLOB,
        allowNull: true,
      },
      iosAttestationCBOR: {
        type: DataTypes.BLOB,
        allowNull: true,
      },
      androidKeyAttestationCertificateChainPEM: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      androidSafetyNetJWS: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
    },
    {
      tableName: 'DeviceAttestation',
      timestamps: false,
    }
  );
};
