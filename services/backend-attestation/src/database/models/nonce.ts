import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

interface CreationAttributes {
  nonce: string;
}

type Attributes = CreationAttributes & {
  uuid: string;
  createdAt: Date;
};

export interface NonceInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initNonce = (sequelize: Sequelize): ModelCtor<NonceInstance> => {
  return sequelize.define<NonceInstance>(
    'Nonce',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      nonce: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      tableName: 'Nonce',
      timestamps: false,
    }
  );
};
