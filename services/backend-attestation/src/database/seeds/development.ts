import { Seed } from 'types/umzug';

const seed: Seed = {
  up: async () => {
    console.log('Dev initial database');
  },
  down: async () => {
    console.warn('Not implemented');
  },
};

export default seed;
