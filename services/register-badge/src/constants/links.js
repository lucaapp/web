export const TERMS_CONDITIONS_LINK =
  'https://www.luca-app.de/operator-terms-and-conditions/';

export const FAQ_LINK = 'https://www.luca-app.de/faq/';
export const GITLAB_LINK =
  'https://gitlab.com/lucaapp/web/-/tree/master/services/register-badge';

export const REGISTER_BADGE_PRIVACY_LINK =
  'https://www.luca-app.de/badge-privacy-policy/';
