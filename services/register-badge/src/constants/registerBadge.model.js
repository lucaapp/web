export const registerBadge = {
  data: {},
  get badgeInfo() {
    return this.data;
  },
  set badgeInfo(value) {
    this.data = { ...this.data, ...value };
  },
};
