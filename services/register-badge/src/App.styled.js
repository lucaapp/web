import styled from 'styled-components';
import { Media } from 'utils/media';

export const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 25px;
  overflow-y: auto;

  ${Media.minHeight`
    height: 100vh;
  `}
`;
