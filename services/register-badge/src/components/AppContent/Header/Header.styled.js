import { Alert } from 'antd';
import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const HeaderWrapper = styled.div`
  display: flex;
`;

export const SubTitle = styled.h4`
  align-self: end;
  margin: 0 0 -3px 12px;
  color: white;
`;

export const Logo = styled.img`
  height: 48px;
  color: white;
`;

export const LinkWrapper = styled.div`
  align-self: end;
`;

export const OperatorTitleWrapper = styled.div`
  font-size: 24px;
  font-weight: bold;
  display: flex;
  justify-content: end;
  margin-right: 35px;
  align-items: flex-end;
  flex: 1;
`;

export const AlertWrapper = styled(Alert)`
  position: absolute;
  top: 15px;
  right: 15px;
  width: 350px;
`;
