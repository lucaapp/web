import React from 'react';
import { useIntl } from 'react-intl';

// Components
import { LucaLogoWhiteIconSVG } from 'assets/icons';
import {
  SubTitle,
  HeaderWrapper,
  Logo,
  Wrapper,
  LinkWrapper,
} from './Header.styled';
import { LinkMenu } from './LinkMenu';
import { OperatorTitle } from './OperatorTitle';

export const Header = ({ operator, isLoggedIn }) => {
  const intl = useIntl();

  return (
    <Wrapper>
      <HeaderWrapper>
        <Logo src={LucaLogoWhiteIconSVG} data-cy="logo" />
        <SubTitle>
          {intl.formatMessage({ id: 'header.registerBadge.title' })}
        </SubTitle>
      </HeaderWrapper>
      <OperatorTitle operator={operator} isLoggedIn={isLoggedIn} />
      <LinkWrapper>
        <LinkMenu />
      </LinkWrapper>
    </Wrapper>
  );
};
