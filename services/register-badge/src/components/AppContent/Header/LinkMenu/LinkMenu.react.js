import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Menu, Dropdown } from 'antd';
import Icon from '@ant-design/icons';

import { LICENSES_ROUTE } from 'constants/routes';

import { MenuActiveIcon, MenuInactiveIcon } from 'assets/icons';
import { REGISTER_BADGE_PRIVACY_LINK } from 'constants/links';

const MenuIcon = active => (
  <Icon
    data-cy="linkMenu"
    component={active ? MenuActiveIcon : MenuInactiveIcon}
    style={{ fontSize: 32, cursor: 'pointer', color: 'white' }}
  />
);

export const LinkMenu = () => {
  const intl = useIntl();
  const [isOpen, setIsOpen] = useState(false);
  const menu = (
    <Menu>
      <Menu.Item>
        <a target="_blank" rel="noopener noreferrer" href={LICENSES_ROUTE}>
          {intl.formatMessage({
            id: 'license.license',
          })}
        </a>
      </Menu.Item>
      <Menu.Item>
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={REGISTER_BADGE_PRIVACY_LINK}
        >
          {intl.formatMessage({
            id: 'header.menu.privacy',
          })}
        </a>
      </Menu.Item>
    </Menu>
  );

  return (
    <Dropdown
      onVisibleChange={() => setIsOpen(!isOpen)}
      overlay={menu}
      placement="bottomLeft"
    >
      {MenuIcon(isOpen)}
    </Dropdown>
  );
};
