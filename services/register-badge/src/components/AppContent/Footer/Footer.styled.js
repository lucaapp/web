import styled from 'styled-components';
import { Media } from 'utils/media';

export const Link = styled.a`
  font-size: 14px;
  font-weight: 600;
  display: block;
  color: white;
  text-decoration: none;
`;

export const Version = styled.span`
  cursor: default;
  user-select: none;
  font-size: 14px;
  font-weight: 600;
  display: block;
  text-decoration: none;

  ${Media.mobile`
    line-break: anywhere;
  `}
`;

export const Wrapper = styled.div`
  position: absolute;
  left: 24px;
  bottom: 24px;
`;
