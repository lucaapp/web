import React from 'react';

import { useIntl } from 'react-intl';
import { usePersonNameValidator } from 'components/hooks/useValidators';
import {
  ButtonWrapper,
  RegisterInput,
  FormWrapper,
  Title,
  StyledFormItem,
} from 'components/AppContent/RegisterCard/RegisterCard.styled';
import { SecondaryButton, WhiteButton } from 'components/general';
import { Description } from 'components/AppContent/RegisterCard/Description';
import { handleFormInputValues, saveInput } from '../../RegisterCard.helper';

export const ContactInformation = ({
  next,
  back,
  stepNumber,
  form,
  stepLength,
}) => {
  const intl = useIntl();
  const firstNameValidator = usePersonNameValidator('firstName');
  const lastNameValidator = usePersonNameValidator('lastName');

  return (
    <>
      <Title>
        {intl.formatMessage({ id: 'registerBadge.serialCode.title' })}
      </Title>
      <Description
        stepNumber={stepNumber}
        stepLength={stepLength}
        stepTitle={intl.formatMessage({
          id: 'registerBadge.serialCode.contactInfo.stepTitle',
        })}
      >
        {intl.formatMessage({
          id: 'registerBadge.serialCode.contactInfo.description',
        })}
      </Description>
      <FormWrapper>
        <StyledFormItem
          rules={firstNameValidator}
          name="firstName"
          label={intl.formatMessage({
            id: 'registerBadge.contactInfo.firstName',
          })}
        >
          <RegisterInput />
        </StyledFormItem>
        <StyledFormItem
          rules={lastNameValidator}
          name="lastName"
          label={intl.formatMessage({
            id: 'registerBadge.contactInfo.lastName',
          })}
        >
          <RegisterInput />
        </StyledFormItem>
        <ButtonWrapper multipleButtons>
          <SecondaryButton onClick={back}>
            {intl.formatMessage({ id: 'registerBadge.sericalCode.back' })}
          </SecondaryButton>
          <WhiteButton
            onClick={() => handleFormInputValues(form, next, saveInput, intl)}
          >
            {intl.formatMessage({ id: 'registerBadge.sericalCode.next' })}
          </WhiteButton>
        </ButtonWrapper>
      </FormWrapper>
    </>
  );
};
