import styled from 'styled-components';
import { Form } from 'antd';
import { Media } from 'utils/media';
import { RegisterInput, ButtonWrapper } from '../../RegisterCard.styled';

export const TanCodeInput = styled(RegisterInput)`
  width: 280px;
  height: 80px;
  padding: 28px;
  background-color: transparent !important;

  font-size: 24px;
  font-weight: bold;
  text-align: center;

  ${Media.mobile`
    width: 80%;
    height: 40px;
    font-size: 16px;
    margin: auto;
    text-align: left;
    padding: 0 8px;
  `}
`;

export const FormItem = styled(Form.Item)`
  display: flex;
  height: 150px;

  & .ant-form-item-control-input {
    align-self: center;
  }

  ${Media.mobile`
    padding: 0 8px;

    & .ant-form-item-control-input-content {
      display: flex;
    }
  `}

  ${Media.minWidth`
    width: 280px;
    margin: 16px auto;
  `}
`;

export const StyledButtonWrapper = styled(ButtonWrapper)`
  margin-top: 0;
`;
