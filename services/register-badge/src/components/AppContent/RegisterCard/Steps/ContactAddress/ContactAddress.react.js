import React from 'react';

import { useIntl } from 'react-intl';

import {
  ButtonWrapper,
  FormWrapper,
  RegisterInput,
  StyledFormItem,
  Title,
} from 'components/AppContent/RegisterCard/RegisterCard.styled';
import { SecondaryButton, WhiteButton } from 'components/general';
import { Description } from 'components/AppContent/RegisterCard/Description';
import {
  useStreetValidator,
  useHouseNoValidator,
  useZipCodeValidator,
  useCityValidator,
} from 'components/hooks/useValidators';
import { handleFormInputValues, saveInput } from '../../RegisterCard.helper';

export const ContactAddress = ({
  next,
  back,
  stepNumber,
  form,
  stepLength,
}) => {
  const intl = useIntl();
  const streetValidator = useStreetValidator('street');
  const houseNoValidator = useHouseNoValidator();
  const zipCodeValidator = useZipCodeValidator();
  const cityValidator = useCityValidator('city');

  return (
    <>
      <Title>
        {intl.formatMessage({ id: 'registerBadge.serialCode.title' })}
      </Title>
      <Description
        stepNumber={stepNumber}
        stepLength={stepLength}
        stepTitle={intl.formatMessage({
          id: 'registerBadge.serialCode.contactInfo.stepTitle',
        })}
      >
        {intl.formatMessage({
          id: 'registerBadge.serialCode.contactInfo.description',
        })}
      </Description>
      <FormWrapper>
        <StyledFormItem
          rules={streetValidator}
          name="street"
          label={intl.formatMessage({
            id: 'registerBadge.contactInfo.street',
          })}
        >
          <RegisterInput />
        </StyledFormItem>
        <StyledFormItem
          name="houseNr"
          rules={houseNoValidator}
          label={intl.formatMessage({
            id: 'registerBadge.contactInfo.number',
          })}
        >
          <RegisterInput />
        </StyledFormItem>
        <StyledFormItem
          name="zipCode"
          rules={zipCodeValidator}
          label={intl.formatMessage({
            id: 'registerBadge.contactInfo.zip',
          })}
        >
          <RegisterInput />
        </StyledFormItem>
        <StyledFormItem
          name="city"
          rules={cityValidator}
          label={intl.formatMessage({
            id: 'registerBadge.contactInfo.city',
          })}
        >
          <RegisterInput />
        </StyledFormItem>
        <ButtonWrapper multipleButtons>
          <SecondaryButton onClick={back}>
            {intl.formatMessage({ id: 'registerBadge.sericalCode.back' })}
          </SecondaryButton>
          <WhiteButton
            onClick={() => handleFormInputValues(form, next, saveInput, intl)}
          >
            {intl.formatMessage({ id: 'registerBadge.sericalCode.next' })}
          </WhiteButton>
        </ButtonWrapper>
      </FormWrapper>
    </>
  );
};
