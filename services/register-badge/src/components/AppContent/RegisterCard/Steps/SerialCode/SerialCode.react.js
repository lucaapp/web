import React, { useRef } from 'react';
import { useIntl } from 'react-intl';

import {
  FormWrapper,
  Title,
} from 'components/AppContent/RegisterCard/RegisterCard.styled';
import { Description } from 'components/AppContent/RegisterCard/Description';
import { WhiteButton } from 'components/general';
import {
  getTanRules,
  TAN_SECTION_LENGTH,
  userBadgeHandler,
  moveToNextInput,
} from './SerialCode.helper';

import {
  SericalCodeWrapper,
  SerialCodeInput,
  Divider,
  FormItem,
  SubmitWrapper,
} from './SerialCode.styled';

export const SerialCode = ({
  next,
  stepNumber,
  setUserSecrets,
  stepLength,
}) => {
  const intl = useIntl();
  const inputReference0 = useRef(null);
  const inputReference1 = useRef(null);
  const inputReference2 = useRef(null);
  const inputReferences = { inputReference0, inputReference1, inputReference2 };

  const handleMoveToNextInput = event =>
    moveToNextInput(event, inputReferences);

  return (
    <>
      <Title>
        {intl.formatMessage({ id: 'registerBadge.serialCode.title' })}
      </Title>
      <Description
        stepNumber={stepNumber}
        stepLength={stepLength}
        stepTitle={intl.formatMessage({
          id: 'registerBadge.serialCode.stepTitle',
        })}
      >
        {intl.formatMessage({ id: 'registerBadge.serialCode.description' })}
      </Description>
      <FormWrapper>
        <SericalCodeWrapper>
          <FormItem
            name="tanChunk0"
            rules={getTanRules(intl, inputReference0)}
            validateTrigger={['onBlur', 'onFocus']}
            normalize={value => value.toUpperCase()}
          >
            <SerialCodeInput
              autoFocus
              maxLength={TAN_SECTION_LENGTH}
              onChange={handleMoveToNextInput}
              data-index={0}
              ref={inputReference0}
            />
          </FormItem>
          <Divider>-</Divider>
          <FormItem
            name="tanChunk1"
            rules={getTanRules(intl, inputReference1)}
            validateTrigger={['onBlur', 'onFocus']}
            normalize={value => value.toUpperCase()}
          >
            <SerialCodeInput
              maxLength={TAN_SECTION_LENGTH}
              onChange={handleMoveToNextInput}
              data-index={1}
              ref={inputReference1}
            />
          </FormItem>
          <Divider>-</Divider>
          <FormItem
            name="tanChunk2"
            rules={getTanRules(intl, inputReference2)}
            validateTrigger={['onBlur', 'onFocus']}
            normalize={value => value.toUpperCase()}
          >
            <SerialCodeInput
              maxLength={TAN_SECTION_LENGTH}
              onChange={handleMoveToNextInput}
              data-index={2}
              ref={inputReference2}
            />
          </FormItem>
        </SericalCodeWrapper>
        <SubmitWrapper>
          <WhiteButton
            onClick={() =>
              userBadgeHandler(inputReferences, intl, next, setUserSecrets)
            }
          >
            {intl.formatMessage({ id: 'registerBadge.sericalCode.next' })}
          </WhiteButton>
        </SubmitWrapper>
      </FormWrapper>
    </>
  );
};
