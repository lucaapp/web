import styled from 'styled-components';
import { Media } from 'utils/media';
import { Form } from 'antd';
import { RegisterInput } from '../../RegisterCard.styled';

export const SericalCodeWrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
`;

export const SerialCodeInput = styled(RegisterInput)`
  height: 80px;
  width: 160px;
  font-size: 24px;
  text-align: center;
  padding: 28px;
  font-weight: bold;
  background-color: transparent !important;

  ${Media.mobile`
    width: 78px;
    height: 40px;
    padding: 0 8px;
    margin: 0;

    font-size: 16px;
    text-align: left;
  `}
`;

export const Divider = styled.div`
  margin: 0 12px;
  display: flex;
  height: 80px;
  align-items: center;
`;

export const FormItem = styled(Form.Item)`
  margin: 0;
  height: 150px;

  ${Media.minWidth`
    width: auto !important;
    max-width: 160px;
      & .ant-form-item-control {
        position: relative;
      }
  `};
`;

export const SubmitWrapper = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;

  ${Media.smallMobile`
    flex-direction: column-reverse;

    & button:last-child {
      margin-bottom: 8px;
    }
  `}
`;
