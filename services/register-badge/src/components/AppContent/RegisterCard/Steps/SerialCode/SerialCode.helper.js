import { getBadgeUser } from 'network/api';
import { notify } from 'utils/notify';
import { decodeSerial } from './SerialCode.crypto';

const SERIAL_ERRORS = {
  error: 'error.registerBadge.serialNumber',
  badge_not_defined: 'error.registerBadge.badgeNotDefined',
  too_short: 'error.registerBadge.serialNumber.tooShort',
  already_registered: 'error.registerBadge.alreadyRegistered',
};

export const TAN_SECTION_LENGTH = 4;

export const moveToNextInput = (event, inputReferences) => {
  const index = Number.parseInt(event.currentTarget.dataset.index, 10);
  if (event.target.value.length >= TAN_SECTION_LENGTH && index + 1 <= 2) {
    inputReferences[`inputReference${index + 1}`].current.focus();
  }
};

export const getTanRules = (intl, reference) => [
  () => ({
    validator: (rule, value) => {
      if (reference.current.state.focused) return Promise.resolve();
      if (
        !reference.current.state.focused &&
        (!value || value.length < TAN_SECTION_LENGTH)
      )
        return Promise.reject(
          intl.formatMessage({
            id: 'registerBadge.serialCode.tan.error.min',
          })
        );

      return Promise.resolve();
    },
  }),
];

export const userBadgeHandler = async (
  { inputReference0, inputReference1, inputReference2 },
  intl,
  next,
  setUserSecret
) => {
  const serialNumber =
    inputReference0?.current.state.value +
    inputReference1?.current.state.value +
    inputReference2?.current.state.value;

  if (!serialNumber) {
    notify(SERIAL_ERRORS.error, intl);
    return;
  }

  if (serialNumber.length < 12) {
    notify(SERIAL_ERRORS.too_short, intl);
    return;
  }

  const badgeInfo = await decodeSerial(serialNumber);
  if (!badgeInfo) {
    notify(SERIAL_ERRORS.badge_not_defined, intl);
    return;
  }

  try {
    const user = await getBadgeUser(badgeInfo.userId);

    if (user.status >= 404 && user.status <= 500) {
      notify(SERIAL_ERRORS.error, intl);
      return;
    }

    if (user.data) {
      notify(SERIAL_ERRORS.already_registered, intl);
      return;
    }
    setUserSecret(badgeInfo);
    next();
  } catch {
    notify(SERIAL_ERRORS.error, intl);
  }
};
