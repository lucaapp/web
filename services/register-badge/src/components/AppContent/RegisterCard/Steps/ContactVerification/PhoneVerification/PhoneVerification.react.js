import React from 'react';
import { useIntl } from 'react-intl';
import { WhiteButton } from 'components/general';
import {
  handleFormInputValues,
  requestTanForUser,
} from 'components/AppContent/RegisterCard/RegisterCard.helper';

export const PhoneVerification = ({ requirePhoneVerification, form, next }) => {
  const intl = useIntl();

  return (
    <>
      {requirePhoneVerification ? (
        <WhiteButton
          onClick={() => {
            handleFormInputValues(form, next, requestTanForUser, intl);
          }}
        >
          {intl.formatMessage({ id: 'registerBadge.sericalCode.next' })}
        </WhiteButton>
      ) : (
        <WhiteButton htmlType="submit">
          {intl.formatMessage({ id: 'registerBadge.sericalCode.next' })}
        </WhiteButton>
      )}
    </>
  );
};
