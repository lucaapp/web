import React from 'react';

import { useIntl } from 'react-intl';

import {
  ButtonWrapper,
  RegisterInput,
  FormWrapper,
  Title,
  StyledFormItem,
} from 'components/AppContent/RegisterCard/RegisterCard.styled';
import { SecondaryButton } from 'components/general';
import { Description } from 'components/AppContent/RegisterCard/Description';
import {
  usePhoneValidator,
  useEmailValidator,
} from 'components/hooks/useValidators';
import { PhoneVerification } from './PhoneVerification';

export const ContactVerification = ({
  next,
  back,
  stepNumber,
  form,
  requirePhoneVerification,
  stepLength,
}) => {
  const intl = useIntl();
  const phoneValidator = usePhoneValidator(requirePhoneVerification);
  const emailValidator = useEmailValidator();

  return (
    <>
      <Title>
        {intl.formatMessage({ id: 'registerBadge.serialCode.title' })}
      </Title>
      <Description
        stepNumber={stepNumber}
        stepLength={stepLength}
        stepTitle={intl.formatMessage({
          id: requirePhoneVerification
            ? 'registerBadge.contactVerification.stepTitle'
            : 'registerBadge.serialCode.contactInfo.stepTitle',
        })}
      >
        {requirePhoneVerification &&
          intl.formatMessage(
            { id: 'registerBadge.contactVerification.description' },
            { br: <br /> }
          )}
      </Description>
      <FormWrapper>
        <StyledFormItem
          name="phone"
          rules={phoneValidator}
          label={intl.formatMessage({
            id: 'registerBadge.contactInfo.phone',
          })}
        >
          <RegisterInput />
        </StyledFormItem>
        <StyledFormItem
          name="email"
          rules={emailValidator}
          label={intl.formatMessage({
            id: 'registerBadge.contactInfo.email',
          })}
        >
          <RegisterInput />
        </StyledFormItem>
        <ButtonWrapper multipleButtons>
          <SecondaryButton onClick={back}>
            {intl.formatMessage({ id: 'registerBadge.sericalCode.back' })}
          </SecondaryButton>
          <PhoneVerification
            requirePhoneVerification={requirePhoneVerification}
            form={form}
            next={next}
          />
        </ButtonWrapper>
      </FormWrapper>
    </>
  );
};
