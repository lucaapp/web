import styled from 'styled-components';

export const SuccessWrapper = styled.div`
  margin: auto;

  & .tick,
  .circle {
    stroke: #819e57;
  }
`;
