import { css } from 'styled-components';

// Constants
import {
  MOBILE_BREAKPOINT,
  SMALL_MOBILE_BREAKPOINT,
  HEIGHT_BREAKPOINT,
} from '../constants/environment';

const createMedia = size => {
  return styles => css`
    @media (max-width: ${size}px) {
      ${styles}
    }
  `;
};
const createMediaSmall = smallSize => {
  return styles => css`
    @media (min-width: 0px) and (max-width: ${smallSize}px) {
      ${styles}
    }
  `;
};

const createMediaWidthHeight = (width, height) => {
  return styles => css`
    @media screen and (max-width: ${width}px),
      screen and (max-height: ${height}px) {
      ${styles}
    }
  `;
};

const createMediaOnlyHeight = height => {
  return styles => css`
    @media screen and (min-height: ${height}px) {
      ${styles}
    }
  `;
};

const createMinMedia = size => {
  return styles => css`
    @media (min-width: ${size}px) {
      ${styles}
    }
  `;
};

export const Media = {
  mobile: createMedia(MOBILE_BREAKPOINT),
  smallMobile: createMediaSmall(SMALL_MOBILE_BREAKPOINT),
  desktopWidthHeight: createMediaWidthHeight(
    MOBILE_BREAKPOINT,
    HEIGHT_BREAKPOINT
  ),
  minHeight: createMediaOnlyHeight(HEIGHT_BREAKPOINT),
  minWidth: createMinMedia(MOBILE_BREAKPOINT),
};
