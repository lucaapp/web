import { notification } from 'antd';

export const notify = (messageId, intl) =>
  notification.error({
    message: intl.formatMessage({
      id: messageId,
    }),
  });
