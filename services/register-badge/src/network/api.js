import axios from 'axios';

axios.defaults.headers.post['Content-Type'] = 'application/json';
const API_PATH = '/api';

class ApiError extends Error {
  constructor(response) {
    super();
    this.response = response;
    this.status = response.status;
    this.message = `Request to ${response.url} failed with status ${response.status}`;
  }
}

const getRequest = path =>
  axios
    .get(path)
    .then(response => response.data)
    .catch(error => {
      throw new ApiError(error.response);
    });

export const getMe = () => {
  return getRequest(`${API_PATH}/v4/auth/operator/me`);
};

// BADGE
export const getBadgeUser = userId =>
  getRequest(`${API_PATH}/v3/users/${userId}`);

export const updateBadgeUser = (userId, data) => {
  return axios
    .patch(`${API_PATH}/v3/users/${userId}`, data)
    .then(response => response.data)
    .catch(error => {
      throw new ApiError(error.response);
    });
};

// TAN
export const requestTan = async data => {
  try {
    const response = await axios.post(`${API_PATH}/v3/sms/request`, data);
    return response.data;
  } catch (error) {
    throw new ApiError(error.response);
  }
};

export const verifyTan = data => {
  return axios.post(`${API_PATH}/v3/sms/verify`, data);
};
