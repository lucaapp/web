/* eslint-disable unicorn/prefer-module */
/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  collectCoverageFrom: ['src/**/*.{js,ts,tsx,jsx}'],
  coverageReporters: ['lcov', 'text', 'cobertura'],
  coverageThreshold: {
    global: {
      statements: 56,
      branches: 51,
      lines: 55,
      functions: 20,
    },
  },
  reporters: [
    'default',
    [
      'jest-junit',
      {
        classNameTemplate: '{suitename}',
        titleTemplate: '{classname} {title}',
        addFileAttribute: 'true',
        filePathPrefix: 'services/backend',
      },
    ],
  ],
};
