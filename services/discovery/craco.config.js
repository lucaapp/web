/* eslint-disable import/no-extraneous-dependencies,unicorn/prefer-module,@typescript-eslint/no-var-requires */
const CracoLessPlugin = require('craco-less');
const CspHtmlWebpackPlugin = require('csp-html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const { LicenseWebpackPlugin } = require('license-webpack-plugin');
const { DefinePlugin } = require('webpack');

const licenseTypeOverrides = {};

module.exports = {
  webpack: {
    plugins: {
      add: [
        new CspHtmlWebpackPlugin(
          {
            'img-src': "'self' data:",
            'script-src': "'self'",
            'connect-src':
              "'self' wss: https://nominatim.openstreetmap.org/",
            'style-src': "'self' 'unsafe-inline'",
            'default-src': "'self' luca-app.de *.luca-app.de",
          },
          {
            enabled: true,
            hashingMethod: 'sha256',
            hashEnabled: {
              'script-src': true,
              'style-src': true,
            },
            nonceEnabled: {
              'script-src': false,
              'style-src': false,
            },
          }
        ),
        new CompressionPlugin({
          algorithm: 'gzip',
          test: /\.(js|css|html|svg|json|ico|eot|otf|ttf|woff|woff2)$/,
          deleteOriginalAssets: false,
        }),
        new DefinePlugin({
          'process.env.REACT_APP_VERSION': JSON.stringify(
            require('./package.json').version
          ),
        }),
        new LicenseWebpackPlugin({
          perChunkOutput: false,
          outputFilename: 'licenses.json',
          stats: {
            warnings: false,
            errors: false,
          },
          licenseTypeOverrides,
          renderLicenses: modules => {
            const licenseList = [];
            modules.forEach(
              ({ packageJson: { name, version, licenses }, licenseId }) => {
                if (!licenseId && !licenses) {
                  // eslint-disable-next-line no-console
                  console.error(`ERROR: Unknown license for package ${name}`);
                  // eslint-disable-next-line unicorn/no-process-exit
                  process.exit(1);
                }
                licenseList.push({
                  name,
                  version,
                  license: licenseId || licenses,
                });
              }
            );
            return JSON.stringify(licenseList);
          },
        }),
        new LicenseWebpackPlugin({
          perChunkOutput: false,
          outputFilename: 'licenses-full.txt',
          stats: {
            warnings: false,
            errors: false,
          },
          licenseTypeOverrides,
        }),
      ],
    },
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: {
              hack: `true;@import "${require.resolve('./antd.theme.less')}";`,
            },
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
