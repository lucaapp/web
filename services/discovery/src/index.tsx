import React from 'react';
import ReactDOM from 'react-dom';

import 'antd/dist/antd.less';
import './index.css';
import './font.css';
import { BrowserRouter } from 'react-router-dom';
import { App } from 'App';
import { IntlProvider } from 'react-intl';
import { messages } from 'messages';
import { HelmetProvider } from 'react-helmet-async';
import { QueryClient, QueryClientProvider } from 'react-query';
import { getLanguage } from './utils/language';
import { ErrorWrapper } from './components/ErrorWrapper';

const queryClient = new QueryClient();

ReactDOM.render(
  <HelmetProvider>
    <IntlProvider
      locale={getLanguage()}
      messages={messages[getLanguage()]}
      wrapRichTextChunksInFragment
    >
      <BrowserRouter>
        <QueryClientProvider client={queryClient}>
          <ErrorWrapper>
            <App />
          </ErrorWrapper>
        </QueryClientProvider>
      </BrowserRouter>
    </IntlProvider>
  </HelmetProvider>,
  document.querySelector('#root')
);
