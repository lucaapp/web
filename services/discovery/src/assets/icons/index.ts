export { default as LucaLogoWhiteIconSVG } from './LucaLogoWhiteIcon.svg';
export { default as LucaLogoBlackIconSVG } from './LucaLogoBlackIcon.svg';
export { default as ErrorIconSVG } from './ErrorIcon.svg';

export { ReactComponent as MapIcon } from './MapIcon.svg';
export { ReactComponent as GastroIcon } from './GastroIcon.svg';
export { ReactComponent as HotelIcon } from './HotelIcon.svg';
export { ReactComponent as StoreIcon } from './StoreIcon.svg';
export { ReactComponent as OtherIcon } from './OtherIcon.svg';
export { ReactComponent as DistanceIcon } from './DistanceIcon.svg';
