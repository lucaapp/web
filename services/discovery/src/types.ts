export interface Place {
  formattedAddress: string;
  lat: number;
  lng: number;
  name: string;
}

export interface LucaLocation {
  discoverId: string;
  groupId: string;
  groupName: string;
  locationName: string | null;
  streetName: string;
  streetNr: string;
  type: string;
  zipCode: string;
  city: string;
  distance: number | null;
  openingHours: null;
  lat: number | null;
  lng: number | null;
}

export interface LocationsResponse {
  limit: number;
  locations: LucaLocation[];
  offset: number;
  total: number;
}

export interface LocationFilter {
  filter: string;
  name: string;
  lat: number | null;
  lng: number | null;
  offset?: number;
}

export interface GeoCoordinates {
  lat: number;
  lng: number;
}

export interface NamedCoordinates extends GeoCoordinates {
  readableName: string;
}

export interface GooglePlaceResult {
  description: string;
  place_id: string;
}

export interface GoogleSearchResults {
  predictions: GooglePlaceResult[];
}
