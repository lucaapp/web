import {
  GoogleSearchResults,
  LocationFilter,
  LocationsResponse,
  Place,
} from 'types';

const API_PATH = '/api';

const headers = {
  'Content-Type': 'application/json',
};

const apiEndpoint = `${API_PATH}/v4/locationGroups/`;

const fetchLocation = async <T>(uri: string): Promise<T> => {
  const response = await fetch(apiEndpoint + uri, {
    headers,
  });

  const result = await response.json();

  if (!response.ok) {
    const error =
      result?.error ??
      `Failed to fetch location info, status code ${response.status}`;

    throw new Error(error);
  }

  return result as Promise<T>;
};

export const getGoogleSearchResults = (
  searchString: string
): Promise<GoogleSearchResults> =>
  fetchLocation(`placeAutocomplete?term=${searchString}`);

export const getPlace = (placeId: string): Promise<Place> =>
  fetchLocation(`places/${placeId}`);

export const getLucaLocations = async (
  data: LocationFilter
): Promise<LocationsResponse> => {
  const parameters = [
    `term=${data.name}`,
    `filter=${data.filter}`,
    `limit=10`,
    `offset=${data.offset ?? '0'}`,
  ];

  if (data.lng && data.lat) {
    parameters.push(`lng=${data.lng}`, `lat=${data.lat}`);
  }

  const locationResponse: LocationsResponse = await fetchLocation(
    `search?${parameters.join('&')}`
  );

  // If there is no geolocation filter given it is useless to show and calculate distances
  if (data.lng === null && data.lat === null) {
    locationResponse.locations = locationResponse.locations.map(location => ({
      ...location,
      distance: 0,
    }));
  }

  return locationResponse;
};
