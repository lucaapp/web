const PUBLIC_URL_PATH = window.location.origin;

// Version
export const getVersion = () =>
  fetch(`${PUBLIC_URL_PATH}/version.json`).then(response => response.json());
