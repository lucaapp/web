// https://nominatim.openstreetmap.org/reverse?lat=53.8378519&lon=10.5009545
// Geolocation OSM api

export interface OsmAddress {
  house_number: string | undefined;
  road: string | undefined;
  hamlet: string;
  city: string | undefined;
  county: string | undefined;
  state: string | undefined;
  'ISO3166-2-lvl4': string;
  postcode: string | undefined;
  country: string | undefined;
  country_code: string;
}

export interface OsmLocation {
  place_id: number;
  licence: string;
  osm_type: string;
  osm_id: number;
  lat: string;
  lon: string;
  display_name: string;
  address: OsmAddress;
  boundingbox: string[];
}

export const getOsmLocation = async (
  lat: string,
  lon: string
): Promise<OsmLocation> => {
  const response = await fetch(
    `https://nominatim.openstreetmap.org/reverse?lat=${lat}&lon=${lon}&format=jsonv2`
  );
  if (!response.ok) {
    throw new Error(`Failed to fetch address from osm ${response.status}`);
  }
  return response.json() as Promise<OsmLocation>;
};
