import React from 'react';
import { Helmet } from 'react-helmet-async';
import { useFormatMessage } from 'utils/language';
import { Discovery } from 'components/Discovery';
import { Wrapper } from './App.styled';

export const App = () => {
  const formatMessage = useFormatMessage();

  return (
    <>
      <Helmet>
        <title>{formatMessage('discovery.site.title')}</title>
      </Helmet>
      <Wrapper>
        <Discovery />
      </Wrapper>
    </>
  );
};
