import styled from 'styled-components';
import { MapIcon } from 'assets/icons';

export const Wrapper = styled.div`
  margin-top: 16px;
  color: rgb(160, 200, 255);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  cursor: pointer;
`;

export const StyledMapIcon = styled(MapIcon)`
  margin-right: 12px;
`;
