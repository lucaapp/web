import React, { useState } from 'react';
import { useIntl } from 'react-intl';

import { getGoogleSearchResults, getPlace } from 'network/api';
import { GooglePlaceResult, NamedCoordinates } from 'types';
import { useDebounce } from 'components/hooks';
import { logError } from 'utils/errors';
import {
  StyledDrawer,
  ButtonWrapper,
  SearchResultsWrapper,
  StyledMapIcon,
  InfoWrapper,
  ResultWrapper,
  Name,
  Wrapper,
  Title,
  StyledButton,
  StyledSearch,
} from './GeoLocationDrawer.styled';

const MIN_SEARCH_TERM = 3;

export const GeoLocationDrawer = ({
  onClose,
  setNamedCoordinates,
}: {
  onClose: () => void;
  setNamedCoordinates: (value: NamedCoordinates) => void;
}) => {
  const intl = useIntl();
  const [searchResults, setSearchResults] = useState<GooglePlaceResult[]>([]);
  const [serviceUnavailable, setServiceUnavailable] = useState(false);

  const getPlaceInfos = (placeId: string) => {
    getPlace(placeId)
      .then(result => {
        setNamedCoordinates({
          lat: result.lat,
          lng: result.lng,
          readableName: result.formattedAddress,
        });
        onClose();
      })
      .catch(error => logError(error));
  };

  const getGooglePredictionsByValue = useDebounce(async (value: string) => {
    if (serviceUnavailable) return;

    if (value.length === 0) {
      setSearchResults([]);
      return;
    }

    if (value.length < MIN_SEARCH_TERM) return;

    try {
      const { predictions } = await getGoogleSearchResults(value);

      setSearchResults(predictions);
    } catch (error) {
      logError(error);
      setServiceUnavailable(true);
    }
  });

  return (
    <StyledDrawer visible placement="bottom" onClose={onClose} height="90%">
      <Wrapper>
        <Title>{intl.formatMessage({ id: 'geoLocationDrawer.title' })}</Title>
        <StyledSearch
          onChange={event => getGooglePredictionsByValue(event.target.value)}
          placeholder={intl.formatMessage({ id: 'search.geoLocation' })}
        />
        {searchResults.length > 0 && (
          <SearchResultsWrapper>
            {searchResults.map((result: GooglePlaceResult) => (
              <ResultWrapper
                key={result.place_id}
                onClick={() => getPlaceInfos(result.place_id)}
              >
                <StyledMapIcon />
                <InfoWrapper>
                  <Name>{result.description}</Name>
                </InfoWrapper>
              </ResultWrapper>
            ))}
          </SearchResultsWrapper>
        )}
        <ButtonWrapper>
          <StyledButton>{intl.formatMessage({ id: 'search' })}</StyledButton>
        </ButtonWrapper>
      </Wrapper>
    </StyledDrawer>
  );
};
