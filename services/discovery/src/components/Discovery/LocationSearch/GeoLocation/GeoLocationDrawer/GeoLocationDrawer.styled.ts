import styled from 'styled-components';
import { Drawer, Input, Button } from 'antd';
import { MapIcon } from 'assets/icons';

const { Search } = Input;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  height: 100%;
`;

export const ButtonWrapper = styled.div`
  position: absolute;
  bottom: 16px;
  width: 100%;
  height: 10%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const SearchResultsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  background: rgb(38, 38, 38);
  border-radius: 6px;
  margin: 8px 0;
  height: 60%;
  overflow-y: auto;

  @media (max-width: 320px) {
    height: 55%;
  }
`;

export const Title = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 24px;
`;

export const StyledMapIcon = styled(MapIcon)`
  margin-right: 12px;
`;

export const InfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const ResultWrapper = styled.div`
  display: flex;
  padding: 32px 16px;

  @media (max-width: 360px) {
    padding: 16px;
  }
`;

export const Name = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;

export const StyledDrawer = styled(Drawer)`
  > .ant-drawer-mask {
    background: #979797;
    opacity: 0.8 !important;
  }
  & .ant-drawer-header {
    background: #000;
    padding-right: 16px;
    margin-bottom: -1px;
  }
  & .ant-drawer-body {
    background: #000;
  }
  & .ant-drawer-header-title {
    display: flex;
    justify-content: flex-end;
  }
  & .ant-drawer-header-title > button {
    color: #a0c8ff;
    font-size: 24px;
    margin-right: 0;
  }
`;

export const StyledSearch = styled(Search)`
  > span > input {
    background: rgba(255, 255, 255, 0.15);
    border-radius: 6px;
    border: none;
    height: 40px;
  }
  > span > span {
    background: rgba(255, 255, 255, 0.15);
    left: 0 !important;
  }
  > span > span > button {
    background: transparent;
    color: #a0c8ff !important;
    border: none;
  }
  > span > span > button > span > svg {
    font-size: 20px;
  }
`;

export const StyledButton = styled(Button)`
  background: rgb(160, 200, 255);
  border-radius: 24px;
  height: 48px;
  color: black;
  width: 100%;
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;
`;
