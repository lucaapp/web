import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { NamedCoordinates } from 'types';
import { GeoLocationDrawer } from './GeoLocationDrawer';
import { Wrapper, StyledMapIcon } from './GeoLocation.styled';

export const GeoLocation = ({
  namedCoordinates,
  setNamedCoordinates,
}: {
  namedCoordinates: NamedCoordinates | null;
  setNamedCoordinates: (value: NamedCoordinates) => void;
}) => {
  const intl = useIntl();
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);

  return (
    <>
      <Wrapper onClick={() => setIsDrawerOpen(true)}>
        <StyledMapIcon />
        {namedCoordinates?.readableName ??
          intl.formatMessage({ id: 'geoLocation.germanyWide' })}
      </Wrapper>
      {isDrawerOpen && (
        <GeoLocationDrawer
          onClose={() => setIsDrawerOpen(false)}
          setNamedCoordinates={setNamedCoordinates}
        />
      )}
    </>
  );
};
