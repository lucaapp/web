import styled from 'styled-components';
import { Input } from 'antd';

const { Search } = Input;

export const Wrapper = styled.div`
  padding: 0 16px;
`;

export const StyledSearch = styled(Search)`
  > span > input {
    background: rgba(255, 255, 255, 0.15);
    border-radius: 6px;
    border: none;
    height: 40px;
    font-size: 16px;

    &:hover,
    &:focus,
    &:active {
      box-shadow: none;
    }
  }

  > span > span {
    background: rgba(255, 255, 255, 0.15);
    left: 0 !important;
  }

  > span > span > button {
    background: transparent;
    color: #a0c8ff !important;
    border: none;

    &:hover,
    &:active,
    &:focus {
      background: rgba(255, 255, 255, 0.15);
      box-shadow: none;
    }

    &[ant-click-animating-without-extra-node='true']::after {
      display: none;
    }
  }

  > span > span > button > span > svg {
    font-size: 20px;
  }
`;
