import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { LocationFilter, NamedCoordinates } from 'types';
import useGeolocation from 'react-hook-geolocation';
import { getOsmLocation, OsmLocation } from 'network/osm';
import { getNamedCoordinates } from 'utils/osm';
import { useDebounce } from 'components/hooks';
import { logError } from 'utils/errors';
import { GeoLocation } from './GeoLocation';
import { Wrapper, StyledSearch } from './LocationSearch.styled';

type Properties = {
  locationFilters: LocationFilter;
  setLocationFilter: (locationFilter: LocationFilter) => void;
};

export const LocationSearch = ({
  locationFilters,
  setLocationFilter,
}: Properties) => {
  const intl = useIntl();
  const geolocation = useGeolocation();

  const [namedCoordinates, setNamedCoordinates]: [
    NamedCoordinates | null,
    (value: NamedCoordinates | null) => void
  ] = useState<NamedCoordinates | null>(null);

  const [osmLocation, setOsmLocation] = useState<OsmLocation | null>(null);

  useEffect(() => {
    if (!osmLocation && geolocation.latitude && geolocation.longitude) {
      getOsmLocation(
        geolocation.latitude.toString(),
        geolocation.longitude.toString()
      )
        .then(osm => {
          if (osm.display_name && osm.lat && osm.lon) {
            setOsmLocation(osm);
            setNamedCoordinates(getNamedCoordinates(osm));
          }
        })
        .catch(error => {
          logError(error);
        });
    }
  }, [geolocation, osmLocation]);

  useEffect(() => {
    if (namedCoordinates !== null) {
      setLocationFilter({
        filter: locationFilters.filter,
        name: locationFilters.name,
        lat: namedCoordinates?.lat ?? null,
        lng: namedCoordinates?.lng ?? null,
      });
    }
  }, [namedCoordinates]);

  const filterLocationsByName = useDebounce((name: string) => {
    setLocationFilter({
      filter: locationFilters.filter,
      name,
      lat: namedCoordinates?.lat ?? null,
      lng: namedCoordinates?.lng ?? null,
    });
  });

  return (
    <Wrapper>
      <StyledSearch
        onChange={event => filterLocationsByName(event.target.value)}
        placeholder={intl.formatMessage({ id: 'search.name' })}
      />
      <GeoLocation
        namedCoordinates={namedCoordinates}
        setNamedCoordinates={setNamedCoordinates}
      />
    </Wrapper>
  );
};
