import React, { useState } from 'react';
import { ALL_TYPE } from 'constants/locationTypes';
import { Header } from 'components/Header';
import { Footer } from 'components/Footer';
import { LocationFilter } from 'types';
import { LocationSearch } from './LocationSearch';
import { SearchResults } from './SearchResults';
import { Wrapper } from './Discovery.styled';

export const Discovery = () => {
  const [locationFilters, setLocationFilters] = useState<LocationFilter>({
    filter: ALL_TYPE,
    name: '',
    lat: null,
    lng: null,
  });

  return (
    <Wrapper>
      <Header />
      <LocationSearch
        locationFilters={locationFilters}
        setLocationFilter={setLocationFilters}
      />
      <SearchResults locationFilters={locationFilters} />
      <Footer />
    </Wrapper>
  );
};
