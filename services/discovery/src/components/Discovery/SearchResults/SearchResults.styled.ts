import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 0 16px;
  max-height: 75%;
  overflow: auto;
  scroll-behavior: smooth;
`;

export const SearchItemsWrapper = styled.div`
  margin-top: 24px;
  overflow: auto;
  scroll-behavior: smooth;
  padding-bottom: 48px;
  height: calc(100% - 52px); /* Footer height */
  -ms-overflow-style: none; /* Internet Explorer 10+ */
  scrollbar-width: none; /* Firefox */

  ::-webkit-scrollbar {
    display: none; /* Safari and Chrome */
  }
`;

export const LoadingText = styled.p`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  text-align: center;
  width: 100%;
`;
