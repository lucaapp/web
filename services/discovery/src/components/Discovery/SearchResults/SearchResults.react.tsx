import React, { useEffect, useState } from 'react';
import { SearchItem } from 'components/SearchItem';
import { LocationFilter, LucaLocation } from 'types';
import InfiniteScroll from 'react-infinite-scroll-component';
import { getLucaLocations } from 'network/api';
import { FormattedMessage } from 'react-intl';
import {
  Wrapper,
  SearchItemsWrapper,
  LoadingText,
} from './SearchResults.styled';

type Properties = {
  locationFilters: LocationFilter;
};

export const SearchResults = ({ locationFilters }: Properties) => {
  const [locations, setLocations] = useState<LucaLocation[]>([]);
  const [hasMore, setHasMore] = useState<boolean>(false);
  const [offset, setOffset] = useState<number>(0);

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await getLucaLocations(locationFilters);
        setHasMore(response.total > response.limit);
        setOffset(0);
        setLocations(response.locations);
      } catch (error) {
        console.error(error);
      }
    }
    fetchData();
  }, [locationFilters]);

  const fetchMore = async () => {
    try {
      const response = await getLucaLocations({
        ...locationFilters,
        offset: offset + 10,
      });
      setOffset(offset + 10);
      setHasMore(response.total > locations.length);
      setLocations([...locations, ...response.locations]);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Wrapper>
      <SearchItemsWrapper id="scrollableDiv">
        <InfiniteScroll
          dataLength={locations.length}
          next={fetchMore}
          hasMore={hasMore}
          loader={
            <LoadingText>
              <FormattedMessage id="search.loading" />
            </LoadingText>
          }
          scrollableTarget="scrollableDiv"
        >
          {locations.map((location: LucaLocation) => {
            const locationAddress = `${location.streetName} ${location.streetNr}, ${location.city} ${location.zipCode}`;
            return (
              <SearchItem
                key={location.discoverId}
                discoverId={location.discoverId}
                name={location.groupName}
                type={location.type}
                distance={location.distance || 0}
                locationAddress={locationAddress}
              />
            );
          })}
        </InfiniteScroll>
      </SearchItemsWrapper>
    </Wrapper>
  );
};
