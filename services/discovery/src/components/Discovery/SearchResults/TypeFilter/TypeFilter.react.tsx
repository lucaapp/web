import React from 'react';
import { useIntl } from 'react-intl';
import { locationTypes } from 'constants/locationTypes';

import {
  Wrapper,
  Filter,
  FilterName,
  ScrollContainer,
} from './TypeFilter.styled';

export const TypeFilter = ({
  filter,
  setFilter,
}: {
  filter: string;
  setFilter: (value: string) => void;
}) => {
  const intl = useIntl();
  return (
    <Wrapper>
      <ScrollContainer>
        {locationTypes.map((locationType: string) => (
          <Filter
            key={locationType}
            active={locationType === filter}
            onClick={() => setFilter(locationType)}
          >
            <FilterName active={locationType === filter}>
              {intl.formatMessage({ id: `filter.${locationType}` })}
            </FilterName>
          </Filter>
        ))}
      </ScrollContainer>
    </Wrapper>
  );
};
