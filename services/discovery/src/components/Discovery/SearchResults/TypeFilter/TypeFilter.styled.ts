import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  margin-top: 20px;
  overflow: auto;
  padding-bottom: 24px;
  border-bottom: 1px solid rgb(116, 116, 128);
`;

export const ScrollContainer = styled.div`
  display: flex;
  overflow: scroll;
  -ms-overflow-style: none; /* Internet Explorer 10+ */
  scrollbar-width: none; /* Firefox */
  ::-webkit-scrollbar {
    display: none; /* Safari and Chrome */
  }
`;

export const Filter = styled.div`
  background: ${({ active }: { active: boolean }) =>
    active ? 'rgb(160, 200, 255)' : 'transparent'};
  border-radius: 12px;
  height: 24px;
  padding: 0 12px;
  margin-right: 24px;
  display: flex;
  align-items: center;
`;

export const FilterName = styled.div`
  color: ${({ active }: { active: boolean }) =>
    active ? 'rgb(0, 0, 0)' : 'rgb(255, 255, 255)'};
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  cursor: pointer;
`;
