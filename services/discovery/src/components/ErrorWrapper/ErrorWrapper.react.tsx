import React, { Component, ErrorInfo } from 'react';
import { injectIntl, WrappedComponentProps } from 'react-intl';

import { ErrorIconSVG, LucaLogoBlackIconSVG } from 'assets/icons';

import {
  ContentWrapper,
  ErrorDescription,
  ErrorGraphic,
  ErrorHeadline,
  FooterWrapper,
  HeaderLogo,
  HeaderWrapper,
  Wrapper,
} from './ErrorWrapper.styled';

interface ErrorWrapperState {
  hasError: boolean;
}

class ErrorWrapperComponent extends Component<
  WrappedComponentProps,
  ErrorWrapperState
> {
  constructor(properties: WrappedComponentProps) {
    super(properties);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  // eslint-disable-next-line no-shadow
  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    // eslint-disable-next-line no-console
    console.error(error, errorInfo);
  }

  render() {
    const { hasError } = this.state;
    const { intl, children } = this.props;

    if (hasError) {
      return (
        <Wrapper>
          <HeaderWrapper>
            <HeaderLogo src={LucaLogoBlackIconSVG} />
          </HeaderWrapper>
          <ContentWrapper>
            <ErrorHeadline>
              {intl.formatMessage({ id: 'error.headline' })}
            </ErrorHeadline>
            <ErrorDescription>
              {intl.formatMessage({ id: 'error.description' })}
            </ErrorDescription>
            <ErrorGraphic src={ErrorIconSVG} />
          </ContentWrapper>
          <FooterWrapper />
        </Wrapper>
      );
    }

    return children;
  }
}
export const ErrorWrapper = injectIntl(ErrorWrapperComponent);
