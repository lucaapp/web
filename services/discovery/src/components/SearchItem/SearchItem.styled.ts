import styled from 'styled-components';
import {
  GastroIcon,
  HotelIcon,
  StoreIcon,
  OtherIcon,
  DistanceIcon,
} from 'assets/icons';

export const Wrapper = styled.div`
  display: flex;
  padding: 10px 16px;
  background: rgb(38, 38, 38);
  border-radius: 6px;
  min-height: 64px;
  align-items: center;
  margin-bottom: 16px;
`;

export const StyledGastroIcon = styled(GastroIcon)`
  margin-right: 12px;
`;

export const StyledHotelIcon = styled(HotelIcon)`
  margin-right: 12px;
`;

export const StyledStoreIcon = styled(StoreIcon)`
  margin-right: 12px;
`;

export const StyledOtherIcon = styled(OtherIcon)`
  margin-right: 12px;
`;
export const StyledDistanceIcon = styled(DistanceIcon)`
  margin-right: 12px;
`;

export const InfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const DistanceWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const Name = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;

export const ContentWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

export const ContentItem = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;

export const InvisibleLink = styled.a`
  text-decoration: none;
`;
