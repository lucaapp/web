import React from 'react';
import { getLanguage } from 'utils/language';

import {
  HOTEL_TYPE,
  RESTAURANT_TYPE,
  STORE_TYPE,
} from 'constants/locationTypes';
import { GUIDE_ROUTE } from 'constants/routes';

import {
  Wrapper,
  StyledGastroIcon,
  StyledHotelIcon,
  StyledStoreIcon,
  StyledOtherIcon,
  StyledDistanceIcon,
  DistanceWrapper,
  InfoWrapper,
  Name,
  ContentWrapper,
  ContentItem,
  InvisibleLink,
} from './SearchItem.styled';

export const formatLocationDistance = (distance: number) =>
  distance < 500
    ? `${distance.toFixed(0)} m`
    : `${(distance / 1000).toFixed(2)} km`;

export const SearchItem = ({
  discoverId,
  type,
  name,
  distance,
  city,
  locationAddress,
}: {
  discoverId: string;
  type: string;
  name: string;
  distance?: number;
  city?: string;
  locationAddress?: string;
}) => {
  const renderIcon = () => {
    switch (type) {
      case RESTAURANT_TYPE:
        return <StyledGastroIcon />;
      case HOTEL_TYPE:
        return <StyledHotelIcon />;
      case STORE_TYPE:
        return <StyledStoreIcon />;
      default:
        return <StyledOtherIcon />;
    }
  };

  return (
    <InvisibleLink
      href={`${GUIDE_ROUTE}/${discoverId}?lang=${getLanguage()}`}
      target="_self"
    >
      <Wrapper>
        {renderIcon()}
        <InfoWrapper>
          <Name>{name}</Name>
          <ContentWrapper>
            <ContentItem>{locationAddress}</ContentItem>
            {city && <ContentItem>{city}</ContentItem>}
            {!!distance && (
              <DistanceWrapper>
                <StyledDistanceIcon />
                <ContentItem>{formatLocationDistance(distance)}</ContentItem>
              </DistanceWrapper>
            )}
          </ContentWrapper>
        </InfoWrapper>
      </Wrapper>
    </InvisibleLink>
  );
};
