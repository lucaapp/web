import { useCallback } from 'react';
import debounce from 'lodash/debounce';

export const useDebounce = <T>(
  callback: (value: T) => void,
  milliseconds = 500
) => {
  return useCallback(debounce(callback, milliseconds), []);
};
