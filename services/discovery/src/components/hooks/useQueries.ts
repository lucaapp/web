import { useQuery } from 'react-query';

import { getVersion } from 'network/static';

export const QUERY_KEYS = {
  VERSION: 'VERSION',
};

export const useGetVersion = () =>
  useQuery([QUERY_KEYS.VERSION], () => getVersion(), {
    refetchOnWindowFocus: false,
  });
