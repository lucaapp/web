import styled from 'styled-components';

export const StyledHeader = styled.div`
  padding: 16px 16px 24px 16px;
`;

export const StyledLucaLogo = styled.img`
  height: 24px;
`;

export const StyledTitle = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 20px;
  font-weight: 500;
  margin-top: 24px;
`;
