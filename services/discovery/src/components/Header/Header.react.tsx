import React from 'react';
import { useIntl } from 'react-intl';
import { useLocation } from 'react-router-dom';
import { LucaLogoWhiteIconSVG } from 'assets/icons';

import { StyledLucaLogo, StyledHeader, StyledTitle } from './Header.styled';

export const Header = () => {
  const intl = useIntl();

  const { search } = useLocation();

  const disableHeaderParameter = new URLSearchParams(search).get(
    'disableHeader'
  );

  const disableHeader = disableHeaderParameter === 'true';

  return (
    <StyledHeader>
      {!disableHeader && (
        <StyledLucaLogo alt="luca" src={LucaLogoWhiteIconSVG} />
      )}
      <StyledTitle>{intl.formatMessage({ id: 'header.title' })}</StyledTitle>
    </StyledHeader>
  );
};
