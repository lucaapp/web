import styled from 'styled-components';

export const FooterWrapper = styled.div`
  position: absolute;
  bottom: 0;
  padding: 16px;
  left: 0;
  right: 0;
  background-color: black;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-top: 1px solid rgb(116, 116, 128);
  gap: 14px;
`;

export const StyledLink = styled.a`
  color: rgba(255, 255, 255, 0.87);
  font-family: Montserrat-SemiBold, sans-serif;
  font-size: 12px;
  font-weight: 600;
  text-align: right;
  text-decoration: none;
`;

export const VersionText = styled.p`
  color: rgb(116, 116, 128);
  font-family: Montserrat-SemiBold, sans-serif;
  font-size: 12px;
  font-weight: 600;
  margin: 0;
  word-break: break-all;
`;
