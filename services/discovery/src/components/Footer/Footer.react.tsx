import React from 'react';
import { useIntl } from 'react-intl';
import { useLocation } from 'react-router-dom';
import { IMPRESSUM_LINK, TERMS_CONDITIONS_LINK } from 'constants/links';

import { useGetVersion } from 'components/hooks';
import { FooterWrapper, StyledLink, VersionText } from './Footer.styled';

export const Footer = () => {
  const intl = useIntl();

  const { isSuccess, data: info } = useGetVersion();

  const { search } = useLocation();
  const disableFooterParameter = new URLSearchParams(search).get(
    'disableFooter'
  );

  const disableFooter = disableFooterParameter === 'true';

  if (disableFooter) return null;

  return (
    <FooterWrapper>
      <StyledLink target="_blank" href={IMPRESSUM_LINK}>
        {intl.formatMessage({ id: 'discovery.imprint' })}
      </StyledLink>
      <StyledLink target="_blank" href={TERMS_CONDITIONS_LINK}>
        {intl.formatMessage({ id: 'discovery.termsOfUse' })}
      </StyledLink>
      <VersionText
        data-cy="lucaVersionNumber"
        title={isSuccess ? `(${info.version})` : ''}
      >
        {isSuccess ? `(${info.version})` : ''}
      </VersionText>
    </FooterWrapper>
  );
};
