import React, { PropsWithChildren, ReactElement } from 'react';
import {
  render as rtlRender,
  RenderOptions as RtlRenderOptions,
} from '@testing-library/react';
import { IntlProvider } from 'react-intl';
import { HelmetProvider } from 'react-helmet-async';
import { QueryClient, QueryClientProvider } from 'react-query';
import { SupportedLanguage } from './language';
import { messages } from '../messages';

export type RenderOptions = Omit<RtlRenderOptions, 'wrapper'> & {
  locale?: SupportedLanguage;
};

const testQueryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false,
      cacheTime: Infinity, // important so Jest can exist
    },
  },
});

function render(ui: ReactElement, options?: RenderOptions) {
  const locale = options?.locale || 'en';
  function TestProviders({ children }: PropsWithChildren<any>) {
    return (
      <HelmetProvider>
        <IntlProvider
          locale={locale}
          messages={messages[locale]}
          wrapRichTextChunksInFragment
        >
          <QueryClientProvider client={testQueryClient}>
            {children}
          </QueryClientProvider>
        </IntlProvider>
      </HelmetProvider>
    );
  }
  return rtlRender(ui, { wrapper: TestProviders, ...options });
}

export * from '@testing-library/react';
export { render };
