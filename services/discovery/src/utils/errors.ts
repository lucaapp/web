export const logError = <T>(error: T) => {
  if (error instanceof Error) {
    console.error(error.message);
    return;
  }
  console.error('ERROR', error);
};
