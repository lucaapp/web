import { formatAmount } from './money';

export const getTipAmount = (amount: number, tipPercentage: number): number => {
  return Math.round(amount * tipPercentage) / 100;
};

export const getTotalAmount = (
  amount: number,
  tipPercentage: number
): number => {
  const tipAmount = getTipAmount(amount, tipPercentage);
  return amount + tipAmount;
};

export const getNominalPercentage = (amount: number, percentage: number) => {
  return formatAmount(getTipAmount(amount, percentage));
};
