/* eslint-disable camelcase */

import { OsmAddress, OsmLocation } from 'network/osm';
import { NamedCoordinates } from 'types';

export const formatOsmAddress = (osmAddress: OsmAddress) => {
  const { road, house_number, postcode, city, country } = osmAddress;

  return `${road || ''} ${house_number || ''}, ${postcode || ''} ${
    city || ''
  }, ${country || ''}`;
};

export const getNamedCoordinates = (osm: OsmLocation): NamedCoordinates => ({
  lat: parseFloat(parseFloat(osm.lat).toFixed(6)),
  lng: parseFloat(parseFloat(osm.lon).toFixed(6)),
  readableName: formatOsmAddress(osm.address),
});
