const GOOGLE_PLAY_URL =
  'https://play.google.com/store/apps/details?id=de.culture4life.luca';
const APP_STORE_URL = 'https://apps.apple.com/de/app/luca-app/id1531742708';

export { GOOGLE_PLAY_URL, APP_STORE_URL };
