import { useIntl } from 'react-intl';
import { FormatXMLElementFn, PrimitiveType } from 'intl-messageformat';

export type SupportedLanguage = 'de' | 'en';

const isSupportedLanguage = (lang: string): lang is SupportedLanguage =>
  lang === 'de' || lang === 'en';

export const getLanguage = (): SupportedLanguage => {
  const language = navigator.language.split(/[_-]/)[0];
  if (isSupportedLanguage(language)) {
    return language;
  }
  return 'en';
};

export const useFormatMessage = () => {
  const intl = useIntl();

  return (
    id: string,
    values?: Record<string, PrimitiveType | FormatXMLElementFn<string, string>>
  ) => intl.formatMessage({ id }, values);
};
