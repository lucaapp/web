export interface LocationState {
  locationId: string;
  locationName: string;
  referer: string | null;
  paymentRequestId?: string;
  table?: string;
}
