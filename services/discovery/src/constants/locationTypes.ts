export const RESTAURANT_TYPE = 'restaurant';
export const HOTEL_TYPE = 'hotel';
export const STORE_TYPE = 'store';
export const BASE_TYPE = 'base';
export const ALL_TYPE = 'all';

export const locationTypes = [
  ALL_TYPE,
  RESTAURANT_TYPE,
  HOTEL_TYPE,
  STORE_TYPE,
  BASE_TYPE,
];
