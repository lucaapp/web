export const IMPRESSUM_LINK = 'https://www.luca-app.de/impressum/';
export const TERMS_CONDITIONS_LINK =
  'https://www.luca-app.de/operator-terms-and-conditions/';
