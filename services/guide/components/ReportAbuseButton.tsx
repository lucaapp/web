import React from 'react';
import { useIntl } from 'react-intl';
import { StyledLink } from 'utils/styles';
import { SUPPORT_EMAIL } from '../constants';

const ReportAbuseButton = () => {
  const intl = useIntl();
  const linkText = intl.formatMessage({ id: 'guide.reportAbuse' });

  return (
    <StyledLink
      target="_blank"
      href={`mailto:${SUPPORT_EMAIL}?subject=${linkText}`}
      rel="noopener noreferrer"
    >
      {linkText}
    </StyledLink>
  );
};

export default ReportAbuseButton;
