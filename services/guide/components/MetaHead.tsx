import React from 'react';
import { Head } from 'next/document';
import { useRouter } from 'next/router';

type MetaHeadProperties = {
  title: string;
  description: string;
  name?: string;
  url?: string;
  keywords?: string;
  image?: string;
};

const MetaHead = ({
  title,
  description,
  name,
  url,
  keywords,
  image,
}: MetaHeadProperties) => {
  const router = useRouter();
  const { lang } = router.query;
  const language = lang === 'de' ? 'German' : 'English';
  // TODO do add base url by stage to get proper url
  // TODO add image

  return (
    <Head>
      <title>{title}</title>
      <meta name="title" content={title} />
      <meta name="description" content={description} />
      <meta name="keywords" content={keywords || ''} />
      <meta name="robots" content="index, follow" />
      <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="language" content={language} />

      <meta property="og:title" content={title} />
      <meta property="og:site_name" content={name || title} />
      <meta property="og:url" content={url || router.asPath} />
      <meta property="og:description" content={description} />
      <meta property="og:type" content="website" />
      {image && <meta property="og:image" content={image} />}
    </Head>
  );
};

export default MetaHead;
