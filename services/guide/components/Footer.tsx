import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useIntl } from 'react-intl';
import { useRouter } from 'next/router';
import { fetchVersion } from 'utils/static';
import { Divider } from 'utils/styles';
import { IMPRESSUM_LINK, TERMS_CONDITIONS_LINK } from '../constants';

const FooterWrapper = styled.div`
  background-color: black;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  gap: 14px;
`;

const StyledLink = styled.a`
  color: rgba(255, 255, 255, 0.87);
  font-family: Montserrat-SemiBold, sans-serif;
  font-size: 12px;
  font-weight: 600;
  text-align: right;
  -webkit-text-decoration: none;
  text-decoration: none;
  height: 16px;
  letter-spacing: 0;
  line-height: 16px;
`;

const VersionText = styled.p`
  color: rgb(116, 116, 128);
  font-family: Montserrat-SemiBold, sans-serif;
  font-size: 12px;
  font-weight: 600;
  height: 16px;
  letter-spacing: 0;
  line-height: 16px;
  text-align: right;
  margin: 0;
  word-break: break-all;
`;

const Footer = () => {
  const intl = useIntl();
  const [version, setVersion] = useState('');
  const router = useRouter();
  const { disableFooter } = router.query;

  const disableFooterQueryParameter = disableFooter === 'true';

  useEffect(() => {
    // eslint-disable-next-line promise/catch-or-return
    fetchVersion(window.location.origin).then(data => {
      setVersion(data.version);
    });
  }, []);

  if (disableFooterQueryParameter) return null;

  return (
    <>
      <Divider />
      <FooterWrapper>
        <StyledLink target="_blank" href={IMPRESSUM_LINK}>
          {intl.formatMessage({ id: 'guide.imprint' })}
        </StyledLink>
        <StyledLink target="_blank" href={TERMS_CONDITIONS_LINK}>
          {intl.formatMessage({ id: 'guide.termsOfUse' })}
        </StyledLink>
        {version && <VersionText>{`(${version})`}</VersionText>}
      </FooterWrapper>
    </>
  );
};

export default Footer;
