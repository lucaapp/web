import React from 'react';
import styled from 'styled-components';

type Props = {
  Icon: typeof React.Component;
  title: string;
  href: string;
  sameWindow?: boolean;
};

const IconLinkBlockWrapper = styled.div`
  background: rgba(255, 255, 255, 0.15);
  border-radius: 6px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.15);
  height: 64px;
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 16px 0;

  p {
    color: white;
    font-family: Montserrat-Bold, sans-serif;
    font-size: 14px;
    font-weight: bold;
    height: 24px;
    letter-spacing: 0;
    line-height: 24px;
  }
`;

const IconWrapper = styled.div`
  display: block;
  width: 32px;
  height: 32px;
  margin: 16px;
`;

const InvisibleLink = styled.a``;

const IconLinkBlock = ({ Icon, title, href, sameWindow = false }: Props) => {
  return (
    <InvisibleLink href={href} target={!sameWindow ? '_blank' : '_self'}>
      <IconLinkBlockWrapper>
        <IconWrapper>
          <Icon />
        </IconWrapper>
        <p>{title}</p>
      </IconLinkBlockWrapper>
    </InvisibleLink>
  );
};

export default IconLinkBlock;
