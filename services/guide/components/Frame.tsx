import React, { ReactNode } from 'react';
import { ContentWrapper, Spacer } from 'utils/styles';
import Header from './Header';
import Footer from './Footer';

type Props = {
  children?: ReactNode;
  hasBackButton?: boolean;
};

const Frame = ({ children, hasBackButton = false }: Props) => {
  return (
    <>
      <Header hasBackButton={hasBackButton} />
      <ContentWrapper>
        {children}
        <Spacer />
        <Footer />
      </ContentWrapper>
    </>
  );
};

export default Frame;
