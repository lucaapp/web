import React from 'react';
import { useRouter } from 'next/router';
import styled from 'styled-components';
import { HeaderLogo, HeaderWrapper } from 'utils/styles';
import { ArrowLeft } from 'utils/images';

const Placeholder = styled.div`
  display: block;
  width: 24px;
`;

const SimpleButton = styled.button`
  background-color: transparent;
  border: none;
  display: block;
  padding: 0;
  margin: 0;
  width: 24px;
`;

type Props = {
  hasBackButton: boolean;
};

const Header = ({ hasBackButton = false }: Props) => {
  const router = useRouter();

  return (
    <HeaderWrapper>
      {hasBackButton && (
        <SimpleButton type="button" onClick={() => router.back()}>
          <ArrowLeft />
        </SimpleButton>
      )}
      <HeaderLogo src="/guide/LucaLogoWhiteIcon.svg" alt="Luca logo" />
      <Placeholder />
    </HeaderWrapper>
  );
};

export default Header;
