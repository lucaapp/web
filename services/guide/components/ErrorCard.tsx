import React from 'react';
import styled from 'styled-components';
import { useIntl } from 'react-intl';
import { Title, Text } from 'utils/styles';
import * as Images from 'utils/images';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  gap: 48px;
`;

const ErrorCard = () => {
  const intl = useIntl();
  const { ErrorIcon } = Images;

  return (
    <Wrapper>
      <Title>{intl.formatMessage({ id: 'error.headline' })}</Title>
      <ErrorIcon />
      <Text>{intl.formatMessage({ id: 'error.description' })}</Text>
    </Wrapper>
  );
};

export default ErrorCard;
