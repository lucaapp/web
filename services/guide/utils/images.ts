/* eslint-disable unicorn/prefer-export-from */
import LucaLogoWhiteIcon from 'public/LucaLogoWhiteIcon.svg';
import Clock from 'public/Clock.svg';
import Menu from 'public/Menu.svg';
import MoreInfos from 'public/More_Infos.svg';
import Pin from 'public/Pin.svg';
import Reservation from 'public/Reservation.svg';
import Timetable from 'public/Timetable.svg';
import Website from 'public/Website.svg';
import ArrowLeft from 'public/Arrow_Left.svg';
import ErrorIcon from 'public/ErrorIcon.svg';
import Phone from 'public/Phone.svg';
import WomanOpenArmsIcon from 'public/WomanOpenArmsIcon.svg';

export {
  LucaLogoWhiteIcon,
  Clock,
  Menu,
  MoreInfos,
  Pin,
  Reservation,
  Timetable,
  Website,
  ArrowLeft,
  ErrorIcon,
  Phone,
  WomanOpenArmsIcon,
};
