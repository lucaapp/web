import styled from 'styled-components';

const HeaderWrapper = styled.div`
  background-color: black;
  width: 100vw;
  padding: 16px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  height: 55px;
  border-bottom: 1px solid rgb(116, 116, 128);
`;

const HeaderLogo = styled.img`
  height: 24px;
`;

const ContentWrapper = styled.div`
  background-color: black;
  padding: 16px;
  color: white;
  display: flex;
  flex-direction: column;
  min-height: calc(100vh - 55px);
`;

const Spacer = styled.div`
  flex: 1;
  width: 100%;
`;

const Divider = styled.hr`
  background-color: rgb(116, 116, 128);
  border: none;
  height: 1px;
  margin: 24px 0;
  padding: 0;
`;

const Title = styled.h2`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 24px;
  margin: 8px 0;
  padding: 0;
`;

const Text = styled.p`
  color: rgb(255, 255, 255);
  font-size: 14px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
  padding: 0;
  margin: 0;
`;

const StyledLink = styled.a`
  height: 16px;
  color: rgb(255, 255, 255);
  font-size: 12px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  text-decoration: underline;
  letter-spacing: 0;
  line-height: 16px;
`;

export {
  HeaderWrapper,
  HeaderLogo,
  ContentWrapper,
  Spacer,
  Divider,
  Title,
  Text,
  StyledLink,
};
