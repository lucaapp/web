// Version
export const fetchVersion = (basePath: string) =>
  fetch(`${basePath}/version.json`).then(response => response.json());
