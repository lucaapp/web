export type OpeningHour = {
  included: boolean;
  openingHours: string[];
};

// eslint-disable-next-line no-shadow
export enum OpeningHoursKey {
  sun = 'sun',
  mon = 'mon',
  tue = 'tue',
  wed = 'wed',
  thu = 'thu',
  fri = 'fri',
  sat = 'sat',
}

export type OpeningHours = {
  [OpeningHoursKey.mon]: OpeningHour[];
  [OpeningHoursKey.tue]: OpeningHour[];
  [OpeningHoursKey.wed]: OpeningHour[];
  [OpeningHoursKey.thu]: OpeningHour[];
  [OpeningHoursKey.fri]: OpeningHour[];
  [OpeningHoursKey.sat]: OpeningHour[];
  [OpeningHoursKey.sun]: OpeningHour[];
};

export type LocationUrls = {
  map: string | null;
  menu: string | null;
  schedule: string | null;
  general: string | null;
  website: string | null;
};

export type Location = {
  name: string;
  lat: number;
  lng: number;
  openingHours: OpeningHours | null;
  locationType: string;
  address: string;
  locationUrls: LocationUrls;
};
