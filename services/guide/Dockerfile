FROM node:16.14.0-alpine3.15 as builder
WORKDIR /app

ARG NPM_CONFIG__AUTH

COPY package.json yarn.lock .npmrc .yarnrc ./
RUN yarn install --check-files --frozen-lockfile && yarn cache clean
COPY tsconfig.json next.config.js env.d.ts ./
COPY public/  ./public/
COPY . .

ARG GIT_COMMIT
ARG GIT_VERSION
ENV NODE_ENV=production
ENV PUBLIC_URL=/guide
ENV NEXT_TELEMETRY_DISABLED 1

RUN yarn build

# production container

FROM node:16.14.0-alpine3.15 AS runner
WORKDIR /app
ENV NODE_ENV production
ENV NEXT_TELEMETRY_DISABLED 1

COPY --from=builder /app/next.config.js ./next.config.js
COPY --from=builder /app/public ./public
COPY --from=builder --chown=node:node /app/build ./build
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json
COPY --from=builder /app/pages ./pages

USER node

EXPOSE 8080

ENV PORT 8080

CMD ["yarn", "start"]
# TODO let sec check
