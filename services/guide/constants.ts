export const SUPPORT_EMAIL = 'abuse@luca-app.de';
export const INTERNAL_BACKEND_URI = `${
  process.env.LUCA_BACKEND_URL || 'http://backend:8080'
}/api`;
export const IMPRESSUM_LINK = 'https://www.luca-app.de/impressum/';
export const TERMS_CONDITIONS_LINK =
  'https://www.luca-app.de/operator-terms-and-conditions/';
