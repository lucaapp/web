import React from 'react';
import type { NextPage } from 'next';
import styled from 'styled-components';
import { Title, Text } from 'utils/styles';
import Frame from 'components/Frame';
import { useIntl } from 'react-intl';
import * as Images from 'utils/images';

const ButtonStyledLink = styled.a`
  font-family: Montserrat-SemiBold, sans-serif;
  color: black;
  background: rgb(160, 200, 255);
  border-radius: 24px;
  height: 48px;
  text-align: center;
  line-height: 48px;
  margin: 24px 0;
`;

const ImageWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 48px;
`;

const Home: NextPage = () => {
  const intl = useIntl();
  const Illustration = Images.WomanOpenArmsIcon;

  return (
    <Frame>
      <Title>{intl.formatMessage({ id: 'guide.start.title' })}</Title>
      <Text>{intl.formatMessage({ id: 'guide.start.description' })}</Text>
      <ImageWrapper>
        <Illustration />
      </ImageWrapper>
      <ButtonStyledLink href="./discovery" target="_self">
        {intl.formatMessage({ id: 'guide.start.cta' })}
      </ButtonStyledLink>
    </Frame>
  );
};

export default Home;
