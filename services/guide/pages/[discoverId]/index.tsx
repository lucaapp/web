import React from 'react';
import { useRouter } from 'next/router';
import { IntlShape, useIntl } from 'react-intl';
import { Divider, Text, Title } from 'utils/styles';
import Frame from 'components/Frame';
import IconLinkBlock from 'components/IconLinkBlock';
import * as Images from 'utils/images';
import ReportAbuseButton from 'components/ReportAbuseButton';
import ErrorCard from 'components/ErrorCard';
import { INTERNAL_BACKEND_URI } from '../../constants';
import { OpeningHoursKey, Location } from '../../types';

export async function getServerSideProps(context: {
  params: { discoverId: string };
}) {
  const { discoverId } = context.params;
  let location = null;

  try {
    const locationResponse = await fetch(
      `${INTERNAL_BACKEND_URI}/v4/locationGroups/discover/${discoverId}`
    );

    if (locationResponse.status === 200) {
      location = await locationResponse.json();
    }
  } catch (error) {
    console.error(error); // TODO
  }

  return {
    props: { location },
  };
}

type Props = {
  location: Location;
};

const getCurrentDayKey = (): OpeningHoursKey | null => {
  const index = new Date().getDay() % 7;

  switch (index) {
    case 0:
      return OpeningHoursKey.sun;
    case 1:
      return OpeningHoursKey.mon;
    case 2:
      return OpeningHoursKey.tue;
    case 3:
      return OpeningHoursKey.wed;
    case 4:
      return OpeningHoursKey.thu;
    case 5:
      return OpeningHoursKey.fri;
    case 6:
      return OpeningHoursKey.sat;
    default:
      return null;
  }
};

const getClosingText = (location: Location, intl: IntlShape): string => {
  const currentKey = getCurrentDayKey();
  if (!location.openingHours || !currentKey) {
    return '';
  }

  const openingHourList = location.openingHours[currentKey];

  if (
    openingHourList.length === 0 ||
    openingHourList.filter(x => x.included).length === 0
  ) {
    return intl.formatMessage({ id: 'guide.closedToday' });
  }

  const lastIncluded = openingHourList.filter(x => x.included).pop();

  if (lastIncluded) {
    return intl.formatMessage(
      { id: 'guide.closingAt' },
      { time: lastIncluded.openingHours[1] }
    );
  }

  return '';
};

const MainPage = ({ location }: Props) => {
  const router = useRouter();
  const intl = useIntl();
  const { discoverId, lang } = router.query;
  const isReserveEnabled = false;

  if (!location) {
    return (
      <Frame hasBackButton>
        <ErrorCard />
      </Frame>
    );
  }

  // TODO Metadata MetaHead usage
  return (
    <Frame hasBackButton>
      <Title>
        {intl.formatMessage(
          { id: 'guide.welcomeTitle' },
          { name: location.name }
        )}
      </Title>
      <Text>{location.address}</Text>

      <Divider />

      {location.locationUrls.menu && (
        <IconLinkBlock
          Icon={Images.Menu}
          title={intl.formatMessage({ id: 'guide.menu.headline' })}
          href={location.locationUrls.menu}
        />
      )}
      {isReserveEnabled && (
        <IconLinkBlock
          Icon={Images.Reservation}
          title={intl.formatMessage({ id: 'guide.reserve.headline' })}
          href=""
        />
      )}

      {location.locationUrls.website && (
        <IconLinkBlock
          Icon={Images.Website}
          title={intl.formatMessage({ id: 'guide.website.headline' })}
          href={location.locationUrls.website}
        />
      )}

      {location.locationUrls.schedule && (
        <IconLinkBlock
          Icon={Images.Timetable}
          title={intl.formatMessage({ id: 'guide.timetable.headline' })}
          href={location.locationUrls.schedule}
        />
      )}

      {location.locationUrls.general && (
        <IconLinkBlock
          Icon={Images.MoreInfos}
          title={intl.formatMessage({ id: 'guide.information.headline' })}
          href={location.locationUrls.general}
        />
      )}
      {location.locationUrls.map && (
        <IconLinkBlock
          Icon={Images.Pin}
          title={intl.formatMessage({ id: 'guide.plan.headline' })}
          href={location.locationUrls.map}
        />
      )}
      {Object.keys(location.locationUrls).length > 0 && <ReportAbuseButton />}
      {location.openingHours && (
        <>
          <Divider />
          <Title>{getClosingText(location, intl)}</Title>
          <IconLinkBlock
            Icon={Images.Clock}
            title={intl.formatMessage({ id: 'guide.openingHours.headline' })}
            href={`${discoverId}/openingHours?lang=${lang}`}
            sameWindow
          />
        </>
      )}
      {!!location.lat && !!location.lng && (
        <IconLinkBlock
          Icon={Images.Pin}
          title={intl.formatMessage({ id: 'guide.navigate.headline' })}
          href={`https://www.google.com/maps/search/?api=1&query=${location.lat},${location.lng}`}
        />
      )}
    </Frame>
  );
};

export default MainPage;
