import React from 'react';
import { IntlShape, useIntl } from 'react-intl';
import styled from 'styled-components';
import Frame from 'components/Frame';
import { Title } from 'utils/styles';
import { INTERNAL_BACKEND_URI } from '../../constants';
import { OpeningHour, Location } from '../../types';

const getDayTranslation = (day: string, intl: IntlShape): string =>
  intl.formatMessage({ id: `guide.weekday.${day}` });

const getOpeningHoursString = (
  openingHours: OpeningHour[],
  intl: IntlShape
): string => {
  if (
    openingHours.length === 0 ||
    openingHours.filter(x => x.included).length === 0
  ) {
    return intl.formatMessage({ id: 'guide.openingHours.closed' });
  }

  return openingHours
    .map(
      openingHour =>
        `${openingHour.openingHours[0]} - ${openingHour.openingHours[1]}`
    )
    .join(', ');
};

const HoursLine = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const TimeText = styled.p`
  color: white;
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  height: 20px;
  letter-spacing: 0;
  line-height: 20px;
  margin: 4px 0;
`;

const DayText = styled(TimeText)`
  font-weight: 800;
`;

type Props = {
  location: Location;
};

export async function getServerSideProps(context: {
  params: { discoverId: string };
}) {
  const { discoverId } = context.params;
  let location = null;

  try {
    const locationResponse = await fetch(
      `${INTERNAL_BACKEND_URI}/v4/locationGroups/discover/${discoverId}`
    );

    if (locationResponse.status === 200) {
      location = await locationResponse.json();
    }
  } catch (error) {
    console.error(error); // TODO
  }

  return {
    props: {
      location,
    },
  };
}

const OpeningHoursPage = ({ location }: Props) => {
  const intl = useIntl();

  return (
    <Frame hasBackButton>
      <Title>{intl.formatMessage({ id: 'guide.openingHours.headline' })}</Title>
      {location.openingHours &&
        Object.entries(location.openingHours).map(data => (
          <HoursLine key={data[0]}>
            <DayText>{getDayTranslation(data[0], intl)}:</DayText>
            <TimeText>{getOpeningHoursString(data[1], intl)}</TimeText>
          </HoursLine>
        ))}
    </Frame>
  );
};

export default OpeningHoursPage;
