import React, { useMemo } from 'react';
import 'styles/globals.css';
import 'styles/font.css';
import type { AppProps } from 'next/app';
import { IntlProvider } from 'react-intl';
import { messages } from 'utils/messages';
import { useRouter } from 'next/router';

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const { lang } = router.query;

  const selectedMessages = useMemo(() => {
    switch (lang) {
      case 'en':
        return messages.en;
      case 'de':
      default:
        return messages.de;
    }
  }, [lang]);

  const selectedLocale = useMemo(() => {
    switch (lang) {
      case 'en':
        return 'en';
      case 'de':
      default:
        return 'de';
    }
  }, [lang]);

  return (
    <IntlProvider locale={selectedLocale} messages={selectedMessages}>
      <Component {...pageProps} />
    </IntlProvider>
  );
}

export default MyApp;
