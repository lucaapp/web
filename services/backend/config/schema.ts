/* eslint-disable max-lines */
import { z } from 'utils/validation';
import type { infer as ZodInfer } from 'zod';

const pinoLogLevelSchema = z.union([
  z.literal('debug'),
  z.literal('error'),
  z.literal('info'),
  z.literal('trace'),
  z.literal('fatal'),
  z.literal('warn'),
]);

const configSchema = z.object({
  debug: z.boolean(),
  loglevel: z.union([z.literal('silent'), pinoLogLevelSchema]),
  defaultHttpLogLevel: pinoLogLevelSchema,
  e2e: z.boolean(),
  hostname: z.string(),
  skipSmsVerification: z.boolean(),
  allowRateLimitBypass: z.boolean(),
  enableJobRoutes: z.boolean(),
  shutdownDelay: z.number().int().min(0),
  port: z.number().int().min(0).max(65536),
  healthCheckPort: z.number().int().min(0).max(65536),
  trustProxy: z.number(),
  version: z.object({
    commit: z.string(),
    version: z.string(),
  }),
  cookies: z.object({
    // DEV ONLY TOKEN
    secret: z.string(),
    maxAge: z.number().int().min(0),
    name: z.string(),
    path: z.string(),
  }),
  db: z.object({
    host: z.string(),
    host_read1: z.string(),
    host_read2: z.string(),
    host_read3: z.string(),
    username: z.string(),
    password: z.string(),
    database: z.string(),
    // only for production usage
    ssl: z.object({
      disabled: z.boolean(),
      fingerprints: z.string(),
      ca: z.string().optional().nullable(),
    }),
  }),
  redis: z.object({
    hostname: z.string(),
    database: z.number().int().min(0),
    password: z.string(),
  }),
  pow: z.object({
    modulusLength: z.number().int().min(0),
    resetAfter: z.number().int().min(0),
    validFor: z.number().int().min(0),
    userAgentMaxAge: z.number().int().min(0),
  }),
  campaigns: z.object({
    maxCampaignsPerOperator: z.number().int().min(0),
  }),
  mailer: z.object({
    apiKey: z.string(),
    sender: z.object({
      from: z.string(),
      name: z.string(),
    }),
  }),
  messagemobile: z.object({
    accessKey: z.string(),
    gateway: z.string(),
  }),
  sinch: z.object({
    cid: z.string(),
    password: z.string(),
    gateway1: z.string(),
    gateway2: z.string(),
  }),
  gtx: z.object({
    authKey: z.string(),
    gateway: z.string(),
  }),
  luca: z.object({
    appVersion: z.object({
      cacheTTL: z.number().int().min(0),
    }),
    trustList: z.object({
      source: z.string(),
      trustAnchor: z.string(),
    }),
    dcc: z.object({
      source: z.string(),
      trustAnchor: z.string(),
    }),
    challenges: z.object({
      operatorDeviceCreation: z.object({
        maxAgeMinutes: z.number().int().min(0),
      }),
    }),
    traces: z.object({
      maximumRequestablePeriod: z.number().int().min(0),
      maxAge: z.number().int().min(0),
      maxDuration: z.number().int().min(0),
    }),
    locationGroupEmployees: z.object({
      maxAmount: z.number().int().min(0),
    }),
    locationTransfers: z.object({
      maxLocations: z.number().int().min(0),
    }),
    smsChallenges: z.object({
      maxAge: z.number().int().min(0),
    }),
    userTransfers: z.object({
      maxAgeUnused: z.number().int().min(0),
      maxAge: z.number().int().min(0),
    }),
    locations: z.object({
      maxAge: z.number().int().min(0),
      maxAdditionalData: z.number().int().min(0),
    }),
    operators: z.object({
      deleted: z.object({
        maxAgeHours: z.number().int().min(0),
      }),
      password: z.object({
        minDifficultyLevel: z.number().int().min(0),
      }),
    }),
    operatorDevice: z.object({
      unactivated: z.object({
        maxAgeMinutes: z.number().int().min(0),
      }),
    }),
    users: z.object({
      deleted: z.object({
        maxAge: z.number().int().min(0),
      }),
      inactive: z.object({
        maxAge: z.number().int().min(0),
      }),
    }),
    tracingProcess: z.object({
      maxAge: z.number().int().min(0),
      maxRiskLevel: z.number().int().min(0),
    }),
    testRedeems: z.object({
      maxAge: z.number().int().min(0),
    }),
    notificationChunks: z.object({
      initialChunkCoverage: z.number().int().min(0),
      maxAge: z.number().int().min(0),
      cacheTTL: z.number().int().min(0),
    }),
    auditLogs: z.object({
      maxAge: z.number().int().min(0),
    }),
    healthDepartments: z.object({
      maxAmount: z.number().int().min(0),
    }),
    alerts: z.object({
      receiverEmail: z.string(),
    }),
    connect: z.object({
      contacts: z.object({
        maxAge: z.number().int().min(0),
      }),
      conversations: z.object({
        maxAge: z.number().int().min(0),
      }),
      searchProcesses: z.object({
        maxAge: z.number().int().min(0),
      }),
    }),
  }),
  emails: z.object({
    expiry: z.number().int().min(0),
  }),
  sms: z.object({
    expiry: z.number().int().min(0),
  }),
  phoneNumber: z.object({
    salt: z.string(),
  }),
  proxy: z.object({
    http: z.string().nullable(),
    https: z.string().nullable(),
  }),
  jwt: z.object({
    remoteUrl: z.string(),
    expiration: z.string(),
  }),
  keys: z.object({
    daily: z.object({
      max: z.number().int().min(0),
      minKeyAge: z.number().int().min(0),
      maxJwtExp: z.number().int().min(0),
    }),
    operatorDevice: z.object({
      expire: z.number().int().min(0),
      maxReactivationAge: z.number().int().min(0),
      // DEV ONLY TOKEN
      publicKey: z.string(),
      // DEV ONLY TOKEN
      privateKey: z.string(),
    }),
    badge: z.object({
      targetKeyId: z.number().int().min(0),
      // DEV ONLY
      attestation: z.object({
        // DEV ONLY
        // private: L+M8om9401/tUo2I/9DzDVMBJ58g6OxyN3xHOXd72HI=
        v3: z.string(),
        v4: z.string(),
        v5: z.string(),
      }),
    }),
  }),
  bloomFilter: z.object({
    // One in 100 million badge users
    falsePositiveRate: z.number().min(0).max(1),
    keepAlivePing: z.number().int().min(0),
    expirationTime: z.number().int().min(0),
  }),
  certs: z.object({
    client: z.object({
      organizationalUnit: z.string().nullable(),
    }),
    dtrust: z.object({
      root: z.string(),
      basic: z.string(),
    }),
  }),
  blockListSources: z.object({
    netset: z.string(),
    singleCSV: z.string(),
    doubleCSV: z.string(),
  }),
  entity_limits: z.object({
    operator_devices_per_operator: z.number().int().min(0),
    locations_per_operator: z.number().int().min(0),
    location_groups_per_operator: z.number().int().min(0),
    tables_per_location: z.number().int().min(0),
  }),
  rate_limits: z.object({
    enable_headers: z.boolean(),
    default_rate_limit_minute: z.number().int().min(0),
    default_rate_limit_hour: z.number().int().min(0),
    default_rate_limit_day: z.number().int().min(0),
    pow_request_post_rate_limit_hour: z.number().int().min(0),
    createConnectContact_per_hour: z.number().int().min(0),
    deleteConnectContact_per_hour: z.number().int().min(0),
    createConnectConversation_per_day: z.number().int().min(0),
    searchConnect_per_day: z.number().int().min(0),
    sendConnectMessage_per_day: z.number().int().min(0),
    readConnectMessage_per_day: z.number().int().min(0),
    receiveConnectMessage_per_day: z.number().int().min(0),
    sms_request_post_ratelimit_minute: z.number().int().min(0),
    sms_request_post_ratelimit_hour: z.number().int().min(0),
    sms_verify_post_ratelimit_day: z.number().int().min(0),
    sms_verify_bulk_post_ratelimit_day: z.number().int().min(0),
    sms_request_post_ratelimit_phone_number: z.number().int().min(0),
    sms_request_post_ratelimit_fixed_phone_number: z.number().int().min(0),
    auth_login_post_ratelimit_minute: z.number().int().min(0),
    auth_hd_login_post_ratelimit_minute: z.number().int().min(0),
    auth_operatordevice_login_post_ratelimit_minute: z
      .number()
      .int()
      .positive(),
    locations_traces_get_ratelimit_hour: z.number().int().min(0),
    traces_checkin_post_ratelimit_hour: z.number().int().min(0),
    traces_delete_hour: z.number().int().min(0),
    traces_additionaldata_post_ratelimit_hour: z.number().int().min(0),
    users_post_ratelimit_hour: z.number().int().min(0),
    users_get_ratelimit_hour: z.number().int().min(0),
    users_patch_ratelimit_hour: z.number().int().min(0),
    users_delete_ratelimit_hour: z.number().int().min(0),
    usertransfers_post_ratelimit_hour: z.number().int().min(0),
    usertransfers_get_ratelimit_hour: z.number().int().min(0),
    usertransfers_get_user_ratelimit_hour: z.number().int().min(0),
    hd_password_change_post_ratelimit_hour: z.number().int().min(0),
    hd_password_renew_patch_ratelimit_hour: z.number().int().min(0),
    hd_employee_post_ratelimit_hour: z.number().int().min(0),
    hd_support_email_post_ratelimit_day: z.number().int().min(0),
    password_change_post_ratelimit_hour: z.number().int().min(0),
    password_forgot_post_ratelimit_hour: z.number().int().min(0),
    password_forgot_post_ratelimit_email: z.number().int().min(0),
    password_reset_post_ratelimit_hour: z.number().int().min(0),
    password_reset_get_ratelimit_hour: z.number().int().min(0),
    keys_badges_rekey_post_ratelimit_user_minute: z.number().int().min(0),
    keys_badges_rotate_post_ratelimit_user_minute: z.number().int().min(0),
    locations_private_post_ratelimit_day: z.number().int().min(0),
    locations_delete_ratelimit_day: z.number().int().min(0),
    locationgroup_post_ratelimit_day: z.number().int().min(0),
    locationgroup_search_get_ratelimit_minute: z.number().int().min(0),
    locationtransfer_contact_post_ratelimit_hour: z.number().int().min(0),
    locationtransfer_post_ratelimit_day: z.number().int().min(0),
    operators_post_ratelimit_day: z.number().int().min(0),
    operators_support_email_post_ratelimit_day: z.number().int().min(0),
    operators_reset_public_key_user_ratelimit_day: z.number().int().min(0),
    operator_location_post_ratelimit_day: z.number().int().min(0),
    operatordevices_post_ratelimit_minute: z.number().int().min(0),
    risklevels_traces_post_ratelimit_minute: z.number().int().min(0),
    vault_post_ratelimit_minute: z.number().int().min(0),
    vault_get_ratelimit_minute: z.number().int().min(0),
    dummy_max_tracings: z.number().int().min(0),
    dummy_max_traces: z.number().int().min(0),
    tests_redeem_post_ratelimit_minute: z.number().int().min(0),
    tests_redeem_delete_ratelimit_minute: z.number().int().min(0),
    badges_post_ratelimit_hour: z.number().int().min(0),
    badges_bloomfilter_get_ratelimit_hour: z.number().int().min(0),
    operator_email_confirm_post_ratelimit_hour: z.number().int().min(0),
    operator_email_patch_ratelimit_day: z.number().int().min(0),
    operator_email_patch_user_ratelimit_day: z.number().int().min(0),
    operator_email_get_ratelimit_day: z.number().int().min(0),
    keys_daily_rotate_post_ratelimit_hour: z.number().int().min(0),
    keys_daily_rotate_post_user_ratelimit_day: z.number().int().min(0),
    keys_daily_rotate_post_ratelimit_day: z.number().int().min(0),
    location_transfer_post_ratelimit_hour: z.number().int().min(0),
    challenges_operatorDevice_get_ratelimit_hour: z.number().int().min(0),
    challenges_operatorDevice_post_ratelimit_day: z.number().int().min(0),
    challenges_operatorDevice_post_ratelimit_hour: z.number().int().min(0),
    challenges_operatorDevice_patch_ratelimit_day: z.number().int().min(0),
    challenges_operatorDevice_patch_ratelimit_hour: z.number().int().min(0),
    notifications_traces_get_ratelimit_hour: z.number().int().min(0),
    notifications_v4_traces_active_chunk_get_ratelimit_hour: z
      .number()
      .int()
      .positive(),
    notifications_v4_traces_archived_chunk_get_ratelimit_hour: z
      .number()
      .int()
      .positive(),
    notifications_v4_config_get_ratelimit_hour: z.number().int().min(0),
    location_transfers_get_ratelimit_hour: z.number().int().min(0),
    location_transfer_get_ratelimit_hour: z.number().int().min(0),
    keys_alert_ratelimit_hour: z.number().int().min(0),
    audit_log_event_download_traces_ratelimit_hour: z.number().int().min(0),
    audit_log_event_export_traces_ratelimit_hour: z.number().int().min(0),
    audit_log_download_ratelimit_hour: z.number().int().min(0),
    audit_log_download_ratelimit_user_hour: z.number().int().min(0),
    audit_log_download_ratelimit_minute: z.number().int().min(0),
    audit_log_download_ratelimit_user_minute: z.number().int().min(0),
    trustlist_dsc_get_ratelimit_day: z.number().int().min(0),
    location_get_urls_hour: z.number().int().min(0),
    operator_update_location_url_user_hour: z.number().int().min(0),
    location_groups_delete_minute: z.number().int().min(0),
    feature_rollouts_hour: z.number().int().min(0),
    location_menu_update_hour: z.number().int().min(0),
    rules_dcc_get_ratelimit_day: z.number().int().min(0),
    rules_dcc_get_hash_ratelimit_day: z.number().int().min(0),
    campaigns_post_ratelimit_minute: z.number().int().min(0),
    campaigns_get_ratelimit_minute: z.number().int().min(0),
    campaign_get_ratelimit_minute: z.number().int().min(0),
    location_group_query_ratelimit_minute: z.number().int().min(0),
  }),
  jwk: z.object({
    privateKey: z.string(),
    publicKey: z.string(),
  }),
  google: z.object({
    api: z.object({
      key: z.string(),
    }),
  }),
});

export default configSchema;

type DeepPartial<T> = T extends Record<string, unknown>
  ? {
      [P in keyof T]?: DeepPartial<T[P]>;
    }
  : T;
export type Config = ZodInfer<typeof configSchema>;
export type PartialConfig = DeepPartial<Config>;
