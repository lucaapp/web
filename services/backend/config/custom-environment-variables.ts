/* eslint-disable import/no-import-module-exports */
import { EnvironmentMapping } from 'config';
import { Config } from './schema';

const config: EnvironmentMapping<Config> = {
  port: {
    __name: 'PORT',
    __format: 'number',
  },
  trustProxy: {
    __name: 'TRUST_PROXY',
    __format: 'number',
  },
  hostname: 'LUCA_HOSTNAME',
  loglevel: 'LOGLEVEL',
  defaultHttpLogLevel: 'DEFAULT_HTTP_LOG_LEVEL',
  debug: {
    __name: 'DEBUG',
    __format: 'boolean',
  },
  e2e: {
    __name: 'E2E',
    __format: 'boolean',
  },
  skipSmsVerification: {
    __name: 'SKIP_SMS_VERIFICATION',
    __format: 'boolean',
  },
  version: {
    commit: 'GIT_COMMIT',
    version: 'GIT_VERSION',
  },
  allowRateLimitBypass: {
    __name: 'ALLOW_RATE_LIMIT_BYPASS',
    __format: 'boolean',
  },
  enableJobRoutes: {
    __name: 'ENABLE_JOB_ROUTES',
    __format: 'boolean',
  },
  cookies: {
    secret: 'COOKIES_SECRET',
    name: 'COOKIE_NAME',
    path: 'COOKIE_PATH',
  },
  db: {
    host: 'DB_HOSTNAME',
    host_read1: 'DB_HOSTNAME_READ1',
    host_read2: 'DB_HOSTNAME_READ2',
    host_read3: 'DB_HOSTNAME_READ3',
    username: 'DB_USERNAME',
    password: 'DB_PASSWORD',
    database: 'DB_DATABASE',
    ssl: {
      disabled: { __name: 'DB_SSL_DISABLED', __format: 'boolean' },
      fingerprints: 'DB_SSL_FINGERPRINTS',
      ca: 'DB_SSL_CA',
    },
  },
  redis: {
    hostname: 'REDIS_HOSTNAME',
    password: 'REDIS_PASSWORD',
    database: { __name: 'REDIS_DATABASE', __format: 'number' },
  },
  mailer: {
    apiKey: 'MAILER_API_KEY_V2',
    sender: {
      from: 'MAILER_SENDER_FROM',
      name: 'MAILER_SENDER_NAME',
    },
  },
  messagemobile: {
    accessKey: 'MM_ACCESS_KEY',
    gateway: 'MM_GATEWAY',
  },
  sinch: {
    cid: 'SINCH_CID',
    password: 'SINCH_PASSWORD',
    gateway1: 'SINCH_GATEWAY1',
    gateway2: 'SINCH_GATEWAY2',
  },
  gtx: {
    authKey: 'GTX_AUTH_KEY',
    gateway: 'GTX_GATEWAY',
  },
  keys: {
    badge: {
      targetKeyId: {
        __name: 'BADGE_TARGET_KEY_ID',
        __format: 'number',
      },
      attestation: {
        v3: 'BADGE_ATTESTATION_KEY_PUBLIC_V3',
        v4: 'BADGE_ATTESTATION_KEY_PUBLIC_V4',
        v5: 'BADGE_ATTESTATION_KEY_PUBLIC_V5',
      },
    },
    operatorDevice: {
      expire: {
        __name: 'OPERATOR_DEVICE_EXPIRE',
        __format: 'number',
      },
      publicKey: 'OPERATOR_DEVICE_PUBLIC_KEY',
      privateKey: 'OPERATOR_DEVICE_PRIVATE_KEY',
    },
  },
  luca: {
    alerts: {
      receiverEmail: 'ALERTS_MAILING_RECEIVER',
    },
  },
  phoneNumber: {
    salt: 'PHONE_NUMBER_SALT',
  },
  proxy: {
    http: 'http_proxy',
    https: 'http_proxy',
  },
  blockListSources: {
    netset: 'DENY_LIST_NETSET_URLS',
    singleCSV: 'DENY_LIST_SINGLE_IP_CSV_URLS',
    doubleCSV: 'DENY_LIST_DOUBLE_IP_CSV_URLS',
  },
  certs: {
    client: {
      organizationalUnit: 'CLIENT_OU',
    },
    dtrust: {
      root: 'DTRUST_ROOT_CA',
      basic: 'DTRUST_BASIC_CA',
    },
  },
  rate_limits: {
    location_transfer_post_ratelimit_hour: {
      __name: 'LOCATION_TRANSFER_POST_RATELIMIT_HOUR',
      __format: 'number',
    },
    badges_bloomfilter_get_ratelimit_hour: {
      __name: 'BADGES_BLOOMFILTER_GET_RATELIMIT_HOUR',
      __format: 'number',
    },
    notifications_traces_get_ratelimit_hour: {
      __name: 'NOTIFICATIONS_TRACES_GET_RATELIMIT_HOUR',
      __format: 'number',
    },
  },
  jwt: {
    expiration: 'JWT_EXPIRATION',
  },
  jwk: {
    privateKey: 'JWK_PRIVATE_KEY',
    publicKey: 'JWK_PUBLIC_KEY',
  },
  google: {
    api: {
      key: 'GOOGLE_API_KEY',
    },
  },
};

// We cannot `export default` until this is fixed: https://github.com/node-config/node-config/issues/521
// eslint-disable-next-line unicorn/prefer-module
module.exports = config;
