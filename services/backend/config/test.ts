/* eslint-disable import/no-import-module-exports */
import { PartialConfig } from './schema';

const config: PartialConfig = {
  loglevel: 'silent',
};

// We cannot `export default` until this is fixed: https://github.com/node-config/node-config/issues/521
// eslint-disable-next-line unicorn/prefer-module
module.exports = config;
