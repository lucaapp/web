/* eslint-disable max-lines, import/no-import-module-exports */
import fs from 'fs';
import moment from 'moment';
import { Config } from './schema';

let ROOT_CA;
let BASIC_CA;

try {
  ROOT_CA = fs.readFileSync('./certs/root.pem').toString();
  BASIC_CA = fs.readFileSync('./certs/basic.pem').toString();
} catch {
  ROOT_CA = '';
  BASIC_CA = '';
}

const config: Config = {
  e2e: true,
  debug: true,
  loglevel: 'info',
  defaultHttpLogLevel: 'debug',
  hostname: 'localhost',
  skipSmsVerification: true,
  allowRateLimitBypass: false,
  enableJobRoutes: true,
  shutdownDelay: 0,
  port: 8080,
  healthCheckPort: 8040,
  trustProxy: 2,
  version: {
    commit: 'dev',
    version: 'dev',
  },
  cookies: {
    // DEV ONLY TOKEN
    secret: 'NJrbMwHlI2uILDVkN0w6Aw==',
    maxAge: moment.duration(15, 'minutes').as('ms'),
    name: 'connect.sid',
    path: '/api',
  },
  db: {
    host: 'database',
    host_read1: 'database',
    host_read2: 'database',
    host_read3: 'database',
    username: 'luca',
    password: 'lcadmin',
    database: 'luca-backend',
    // only for production usage
    ssl: {
      disabled: true,
      fingerprints: '',
    },
  },
  redis: {
    hostname: 'redis',
    database: 0,
    password:
      // DEV ONLY TOKEN
      'ConqsCqWd]eaR82wv%C.iDdRybor8Ms2bM*h=m?V3@x2w^UxKA9pEjMjHn^y7?78',
  },
  campaigns: {
    maxCampaignsPerOperator: 100,
  },
  pow: {
    modulusLength: 1024,
    resetAfter: moment.duration(1, 'hour').as('milliseconds'),
    validFor: moment.duration(1, 'hour').as('hours'),
    userAgentMaxAge: moment.duration(28, 'days').as('hours'),
  },
  mailer: {
    apiKey: '',
    sender: {
      from: 'hello@luca-app.de',
      name: 'luca',
    },
  },
  messagemobile: {
    accessKey: '',
    gateway: '',
  },
  sinch: {
    cid: '',
    password: '',
    gateway1: '',
    gateway2: '',
  },
  gtx: {
    authKey: '',
    gateway: '',
  },
  luca: {
    appVersion: {
      cacheTTL: moment.duration(1, 'hours').as('seconds'),
    },
    trustList: {
      source: 'https://de.dscg.ubirch.com/trustList/DSC',
      trustAnchor: `BEx34vH6EBeFLUjVcUhcXru1voDMXUhhJYXaO/NuUg9y8m/kqoeBuiq2tK6NQXazOkMfobFMeEMHW8u0GNKc8cg=`,
    },
    dcc: {
      source: 'https://distribution.dcc-rules.de/rules/DE',
      trustAnchor: `BHfbPQag7Yuorh2ExAlPqw6akyeaok00xtIBqPgNwLPC3MaO62DevIRK1eodp/wLbnZ5MpaLxc1/6mWIUXQ9WR8=`,
    },
    challenges: {
      operatorDeviceCreation: {
        maxAgeMinutes: moment.duration(30, 'minutes').as('minutes'),
      },
    },
    traces: {
      maximumRequestablePeriod: moment.duration(24, 'hours').as('hours'),
      maxAge: moment.duration(28, 'days').as('days'),
      maxDuration: moment.duration(24, 'hours').as('hours'),
    },
    locationGroupEmployees: {
      maxAmount: 250,
    },
    locationTransfers: {
      maxLocations: 100 * 14, // Max 100 location transfers per day for 14 days of contact tracing
    },
    smsChallenges: {
      maxAge: moment.duration(45, 'days').as('hours'),
    },
    userTransfers: {
      maxAgeUnused: moment.duration(48, 'hours').as('hours'),
      maxAge: moment.duration(28, 'days').as('hours'),
    },
    locations: {
      maxAge: moment.duration(28, 'days').as('hours'),
      maxAdditionalData: 10,
    },
    operators: {
      deleted: {
        maxAgeHours: moment.duration(28, 'days').as('hours'),
      },
      password: {
        minDifficultyLevel: 4,
      },
    },
    operatorDevice: {
      unactivated: {
        maxAgeMinutes: moment.duration(30, 'minutes').as('minutes'),
      },
    },
    users: {
      deleted: {
        maxAge: moment.duration(28, 'days').as('hours'),
      },
      inactive: {
        maxAge: moment.duration(1, 'year').as('hours'),
      },
    },
    tracingProcess: {
      maxAge: moment.duration(28, 'days').as('hours'),
      maxRiskLevel: 2,
    },
    testRedeems: {
      maxAge: moment.duration(72, 'hours').as('hours'),
    },
    notificationChunks: {
      initialChunkCoverage: moment.duration(14, 'days').as('hours'),
      maxAge: moment.duration(14, 'days').as('hours'),
      cacheTTL: moment.duration(2, 'hours').as('seconds'),
    },
    auditLogs: {
      maxAge: moment.duration(1, 'years').as('hours'),
    },
    healthDepartments: {
      maxAmount: 450,
    },
    alerts: {
      receiverEmail: '',
    },
    connect: {
      contacts: {
        maxAge: moment.duration(7, 'days').as('hours'),
      },
      conversations: {
        maxAge: moment.duration(7, 'days').as('hours'),
      },
      searchProcesses: {
        maxAge: moment.duration(72, 'hours').as('hours'),
      },
    },
  },
  emails: {
    expiry: moment.duration(1, 'hours').as('hours'),
  },
  sms: {
    expiry: moment.duration(1, 'hours').as('hours'),
  },
  phoneNumber: {
    salt: 'salt',
  },
  proxy: {
    http: null,
    https: null,
  },
  jwt: {
    remoteUrl: 'http://backend-attestation:8080/attestation/api/v1/jwks',
    expiration: '24h',
  },
  keys: {
    daily: {
      max: 35,
      minKeyAge: moment.duration(24, 'hours').as('hours'),
      maxJwtExp: moment.duration(30, 'days').as('hours'),
    },
    operatorDevice: {
      expire: moment.duration(31, 'days').as('millisecond'),
      maxReactivationAge: moment.duration(20, 'minutes').as('millisecond'),
      // DEV ONLY TOKEN
      publicKey: `-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEc0JU9Xhlom553niIAc4K9C/1ZXOT
AQp4BE3MdB9LqeGgVw78Krp0/YoQRPZmvBzBwXUZFmB+ZmcMcywB7aAXTw==
-----END PUBLIC KEY-----`,
      // DEV ONLY TOKEN
      privateKey: `-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIGEXxQ0ksJNT0AV4srZvxR86UTSUv63yuvfdqv5+ZyTfoAoGCCqGSM49
AwEHoUQDQgAEc0JU9Xhlom553niIAc4K9C/1ZXOTAQp4BE3MdB9LqeGgVw78Krp0
/YoQRPZmvBzBwXUZFmB+ZmcMcywB7aAXTw==
-----END EC PRIVATE KEY-----`,
    },
    badge: {
      targetKeyId: 2,
      // DEV ONLY
      attestation: {
        // DEV ONLY
        // private: L+M8om9401/tUo2I/9DzDVMBJ58g6OxyN3xHOXd72HI=
        v3:
          // eslint-disable-next-line sonarjs/no-duplicate-string
          'BA44IJ3GumJIISMrMkuYdoJaL8WQVQ0YOq2lKQpacGfk9SslSZ23/Z3MVdi0poI7ElrPKhdia6f5FDpJt31xrM4=',
        v4:
          // eslint-disable-next-line sonarjs/no-duplicate-string
          'BA44IJ3GumJIISMrMkuYdoJaL8WQVQ0YOq2lKQpacGfk9SslSZ23/Z3MVdi0poI7ElrPKhdia6f5FDpJt31xrM4=',
        v5:
          // eslint-disable-next-line sonarjs/no-duplicate-string
          'BA44IJ3GumJIISMrMkuYdoJaL8WQVQ0YOq2lKQpacGfk9SslSZ23/Z3MVdi0poI7ElrPKhdia6f5FDpJt31xrM4=',
      },
    },
  },
  bloomFilter: {
    // One in 100 million badge users
    falsePositiveRate: 0.00000001,
    keepAlivePing: moment.duration(1, 'minutes').as('millisecond'),
    expirationTime: moment.duration(2, 'minutes').as('millisecond'),
  },
  certs: {
    client: {
      organizationalUnit: null,
    },
    dtrust: {
      root: ROOT_CA,
      basic: BASIC_CA,
    },
  },
  blockListSources: {
    netset: '',
    singleCSV: '',
    doubleCSV: '',
  },
  entity_limits: {
    operator_devices_per_operator: 500,
    locations_per_operator: 1000,
    location_groups_per_operator: 1000,
    tables_per_location: 1000,
  },
  rate_limits: {
    enable_headers: false,
    default_rate_limit_minute: 5,
    default_rate_limit_hour: 10,
    default_rate_limit_day: 50,
    pow_request_post_rate_limit_hour: 100,
    createConnectContact_per_hour: 10,
    deleteConnectContact_per_hour: 70,
    createConnectConversation_per_day: 1000,
    searchConnect_per_day: 500,
    sendConnectMessage_per_day: 1500,
    readConnectMessage_per_day: 1000,
    receiveConnectMessage_per_day: 1000,
    sms_request_post_ratelimit_minute: 7200,
    sms_request_post_ratelimit_hour: 10,
    sms_verify_post_ratelimit_day: 50,
    sms_verify_bulk_post_ratelimit_day: 50,
    sms_request_post_ratelimit_phone_number: 5,
    sms_request_post_ratelimit_fixed_phone_number: 2,
    auth_login_post_ratelimit_minute: 5,
    auth_hd_login_post_ratelimit_minute: 5,
    auth_operatordevice_login_post_ratelimit_minute: 5,
    locations_traces_get_ratelimit_hour: 1000,
    traces_checkin_post_ratelimit_hour: 1000,
    traces_delete_hour: 200,
    traces_additionaldata_post_ratelimit_hour: 60,
    users_post_ratelimit_hour: 200,
    users_get_ratelimit_hour: 1000,
    users_patch_ratelimit_hour: 1000,
    users_delete_ratelimit_hour: 100,
    usertransfers_post_ratelimit_hour: 15,
    usertransfers_get_ratelimit_hour: 100,
    usertransfers_get_user_ratelimit_hour: 100,
    hd_password_change_post_ratelimit_hour: 15,
    hd_password_renew_patch_ratelimit_hour: 15,
    hd_employee_post_ratelimit_hour: 50,
    hd_support_email_post_ratelimit_day: 100,
    password_change_post_ratelimit_hour: 15,
    password_forgot_post_ratelimit_hour: 5,
    password_forgot_post_ratelimit_email: 10,
    password_reset_post_ratelimit_hour: 15,
    password_reset_get_ratelimit_hour: 15,
    keys_badges_rekey_post_ratelimit_user_minute: 10,
    keys_badges_rotate_post_ratelimit_user_minute: 10,
    locations_private_post_ratelimit_day: 100,
    locations_delete_ratelimit_day: 1000,
    locationgroup_post_ratelimit_day: 50,
    locationgroup_search_get_ratelimit_minute: 10,
    locationtransfer_contact_post_ratelimit_hour: 100,
    locationtransfer_post_ratelimit_day: 100,
    operators_post_ratelimit_day: 10,
    operators_support_email_post_ratelimit_day: 5,
    operators_reset_public_key_user_ratelimit_day: 3,
    operator_location_post_ratelimit_day: 250,
    operatordevices_post_ratelimit_minute: 10,
    risklevels_traces_post_ratelimit_minute: 10,
    vault_post_ratelimit_minute: 50,
    vault_get_ratelimit_minute: 50,
    dummy_max_tracings: 10,
    dummy_max_traces: 20,
    tests_redeem_post_ratelimit_minute: 50,
    tests_redeem_delete_ratelimit_minute: 50,
    badges_post_ratelimit_hour: 10,
    badges_bloomfilter_get_ratelimit_hour: 10,
    operator_email_confirm_post_ratelimit_hour: 15,
    operator_email_patch_ratelimit_day: 3,
    operator_email_patch_user_ratelimit_day: 3,
    operator_email_get_ratelimit_day: 3,
    keys_daily_rotate_post_ratelimit_hour: 5,
    keys_daily_rotate_post_user_ratelimit_day: 10,
    keys_daily_rotate_post_ratelimit_day: 1000,
    location_transfer_post_ratelimit_hour: 1000,
    challenges_operatorDevice_get_ratelimit_hour: 50,
    challenges_operatorDevice_post_ratelimit_day: 1000,
    challenges_operatorDevice_post_ratelimit_hour: 100,
    challenges_operatorDevice_patch_ratelimit_day: 8000,
    challenges_operatorDevice_patch_ratelimit_hour: 800,
    notifications_traces_get_ratelimit_hour: 5,
    notifications_v4_traces_active_chunk_get_ratelimit_hour: 1000,
    notifications_v4_traces_archived_chunk_get_ratelimit_hour: 7500,
    notifications_v4_config_get_ratelimit_hour: 1000,
    location_transfers_get_ratelimit_hour: 1000,
    location_transfer_get_ratelimit_hour: 1000,
    keys_alert_ratelimit_hour: 5,
    audit_log_event_download_traces_ratelimit_hour: 20,
    audit_log_event_export_traces_ratelimit_hour: 20,
    audit_log_download_ratelimit_hour: 20,
    audit_log_download_ratelimit_user_hour: 10,
    audit_log_download_ratelimit_minute: 5,
    audit_log_download_ratelimit_user_minute: 1,
    trustlist_dsc_get_ratelimit_day: 500,
    location_get_urls_hour: 1000,
    operator_update_location_url_user_hour: 1000,
    location_groups_delete_minute: 5,
    feature_rollouts_hour: 1000,
    location_menu_update_hour: 1000,
    rules_dcc_get_ratelimit_day: 500,
    rules_dcc_get_hash_ratelimit_day: 500,
    campaigns_post_ratelimit_minute: 10,
    campaigns_get_ratelimit_minute: 10,
    campaign_get_ratelimit_minute: 10,
    location_group_query_ratelimit_minute: 50,
  },
  jwk: {
    privateKey: `-----BEGIN PRIVATE KEY-----
MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgZIHDjbDRyhYuY9RQ
WTB7LeKmaOBaKN+gxBw39u6nRM6hRANCAATKxxdQIEsqsUK8P/1rRrpaUlkMA43F
4gBV6gY+h4ndULKSs0lSd2qpuSI1huX3yy604QrfseYaznxShsPJeoPo
-----END PRIVATE KEY-----`,
    publicKey: `-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEyscXUCBLKrFCvD/9a0a6WlJZDAON
xeIAVeoGPoeJ3VCykrNJUndqqbkiNYbl98sutOEK37HmGs58UobDyXqD6A==
-----END PUBLIC KEY-----`,
  },
  google: {
    api: {
      key: 'AIzaSyBrSNm19GYq1xT-TW78ikk9IlWhPksqpk4',
    },
  },
};

// We cannot `export default` until this is fixed: https://github.com/node-config/node-config/issues/521
// eslint-disable-next-line unicorn/prefer-module
module.exports = config;
