/* eslint-disable import/no-import-module-exports */
import { PartialConfig } from './schema';

const config: PartialConfig = {
  e2e: false,
  debug: false,
  enableJobRoutes: false,
  skipSmsVerification: false,
  loglevel: 'info',
  defaultHttpLogLevel: 'info',
  hostname: 'app.luca-app.de',
  shutdownDelay: 15,
  jwt: {
    expiration: '60s',
  },
};

// We cannot `export default` until this is fixed: https://github.com/node-config/node-config/issues/521
// eslint-disable-next-line unicorn/prefer-module
module.exports = config;
