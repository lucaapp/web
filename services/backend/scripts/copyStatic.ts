import { copyFile, mkdir } from 'fs/promises';
import { glob } from 'glob';
import path from 'path';

const OUTPUT_PATH = path.resolve('./build');
const STATIC_FILE_GLOB = './src/**/*.openapi.yaml';

(async () => {
  const relativeFilePaths = glob.sync(STATIC_FILE_GLOB);

  for (const relativeFilePath of relativeFilePaths) {
    const parsedStaticFilePath = path.parse(relativeFilePath);
    const absoluteFileOutputPath = path.join(
      OUTPUT_PATH,
      parsedStaticFilePath.dir
    );

    await mkdir(absoluteFileOutputPath, { recursive: true });
    await copyFile(
      relativeFilePath,
      path.join(
        absoluteFileOutputPath,
        `${parsedStaticFilePath.name}${parsedStaticFilePath.ext}`
      )
    );
  }
})();
