const config = require('config');
const express = require('express');
const session = require('express-session');
const SequelizeStore = require('connect-session-sequelize')(session.Store);
require('express-async-errors');
const cookieParser = require('cookie-parser');
const passport = require('passport');
const helmet = require('helmet');
const { noCache } = require('@lucaapp/no-cache');
const healthRouter = require('./routes/health').default;
const { database } = require('./database');

const error = require('./middlewares/error');
const { etagMiddleware } = require('./middlewares/etag');
const { httpLogger } = require('./utils/logger');

const passportSession = require('./passport/session');
const bearerBadgeGeneratorStrategy = require('./passport/bearerBadgeGenerator');
const bearerInternalAccessStrategy = require('./passport/bearerInternalAccess');
const localOperatorStrategy = require('./passport/localOperator');
const operatorDeviceStrategy = require('./passport/operatorDevice');
const { operatorStrategy } = require('./passport/operator');
const deviceStrategy = require('./passport/device');
const { emailLoginTokenStrategy } = require('./passport/emailLoginToken');
const localHealthDepartmentEmployeeStrategy = require('./passport/localHealthDepartmentEmployee');
const requestMetricsMiddleware = require('./middlewares/requestMetrics');
const {
  requireMinimumUserAgentVersionMiddleware,
} = require('./middlewares/requireMinimumUserAgentVersion');

// Routes
const versionRouter = require('./routes/version');
const licensesRouter = require('./routes/licenses');
const internalRouter = require('./routes/internal');
const v2Router = require('./routes/v2').default;
const v3Router = require('./routes/v3');
const v4Router = require('./routes/v4').default;

let app;

const getApp = () => {
  return app;
};

const configureHealthChecks = () => {
  const healthCheckApp = express();
  healthCheckApp.use(httpLogger);
  healthCheckApp.use(requestMetricsMiddleware);

  healthCheckApp.use('/health', healthRouter);
  healthCheckApp.use('/internal', internalRouter);

  return healthCheckApp;
};

const configureApp = () => {
  // Passport Strategies
  passport.serializeUser(passportSession.serializeUser);
  passport.deserializeUser(passportSession.deserializeUser);
  passport.use('bearer-badgeGenerator', bearerBadgeGeneratorStrategy);
  passport.use('bearer-internalAccess', bearerInternalAccessStrategy);
  passport.use('local-operator', localOperatorStrategy);
  passport.use('emailLoginToken', emailLoginTokenStrategy);
  passport.use('header-device', deviceStrategy);
  passport.use(
    'local-healthDepartmentEmployee',
    localHealthDepartmentEmployeeStrategy
  );
  passport.use('jwt-operatorDevice', operatorDeviceStrategy);
  passport.use('jwt-operator', operatorStrategy);

  app = express();
  const router = express.Router();

  const sequelizeStore = new SequelizeStore({
    db: database,
    table: 'Session',
    extendDefaultFields: (defaults, data) => ({
      data: defaults.data,
      expires: defaults.expires,
      userId: data.passport.user.uuid,
      type: data.passport.user.type,
    }),
  });

  sequelizeStore.sync({ alter: true });

  app.disable('x-powered-by');
  app.disable('etag');
  app.enable('strict routing');

  app.set('trust proxy', config.get('trustProxy'));

  // Global Middlewares
  app.use(httpLogger);
  if (!config.get('debug')) {
    app.use(helmet.hsts());
  }
  app.use(requestMetricsMiddleware);
  app.use(requireMinimumUserAgentVersionMiddleware(['/version']));
  app.use(cookieParser());
  app.use(
    session({
      secret: config.get('cookies.secret').split(','),
      name: `__Secure-${config.get('cookies.name')}`,
      store: sequelizeStore,
      resave: true,
      rolling: true,
      saveUninitialized: false,
      unset: 'destroy',
      cookie: {
        secure: true,
        httpOnly: true,
        sameSite: 'strict',
        maxAge: config.get('cookies.maxAge'),
        path: config.get('cookies.path'),
      },
    })
  );
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(noCache);
  app.use(etagMiddleware);

  app.use('/api', router);
  app.use(error.handle404);
  app.use(error.handle500);

  // Routing
  router.use('/version', versionRouter);
  router.use('/licenses', licensesRouter);
  router.use('/internal', internalRouter);
  router.use('/v2', v2Router);
  router.use('/v3', v3Router);
  router.use('/v4', v4Router);

  return app;
};

module.exports = { configureHealthChecks, configureApp, getApp };
