import config from 'config';

export const certificateChain = [
  config.get('certs.dtrust.root'),
  config.get('certs.dtrust.basic'),
];
