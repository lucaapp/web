// eslint-disable-next-line no-shadow
export enum OperatorDevice {
  scanner = 'scanner',
  employee = 'employee',
  manager = 'manager',
  link = 'link',
}

// eslint-disable-next-line no-shadow
export enum OperatorDeviceSupportedOSTypes {
  IOS = 'ios',
  Android = 'android',
}
