// eslint-disable-next-line no-shadow
export enum LocationEntryPolicyTypes {
  TWO_G = '2G',
  THREE_G = '3G',
  TWO_G_PLUS = '2GPlus',
}

// eslint-disable-next-line no-shadow
export enum LocationEntryPolicyInfoTypes {
  TWO_G = '2G',
  THREE_G = '3G',
  TWO_G_PLUS = '2GPlus',
  NONE = 'none',
}

// eslint-disable-next-line no-shadow
export enum LocationVentilationTypes {
  ELECTRIC = 'electric',
  WINDOW = 'window',
  PARTIAL = 'partial',
  NO = 'no',
}

// eslint-disable-next-line no-shadow
export enum LocationMaskTypes {
  YES = 'yes',
  PARTIAL = 'partial',
  NO = 'no',
}

export enum LocationType {
  RESTAURANT_TYPE = 'restaurant',
  NURSING_HOME_TYPE = 'nursing_home',
  HOTEL_TYPE = 'hotel',
  STORE_TYPE = 'store',
  BASE_TYPE = 'base',
}

export enum AreaType {
  RESTAURANT_TYPE = 'restaurant',
  ROOM_TYPE = 'room',
  BUILDING_TYPE = 'building',
  BASE_TYPE = 'base',
}

export const AreaSubtype = {
  restaurant: 'restaurant',
  cafe: 'cafe',
  club: 'club',
  bar: 'bar',
  cafeteria: 'cafeteria',
  other: 'other',
  NULL: null,
};

export const AllowedSubtypeConfiguration = {
  [LocationType.BASE_TYPE]: {
    [AreaType.RESTAURANT_TYPE]: Object.values(AreaSubtype),
    [AreaType.ROOM_TYPE]: [AreaSubtype.NULL],
    [AreaType.BUILDING_TYPE]: [AreaSubtype.NULL],
    [AreaType.BASE_TYPE]: [AreaSubtype.NULL],
  },
  [LocationType.RESTAURANT_TYPE]: {
    [AreaType.RESTAURANT_TYPE]: Object.values(AreaSubtype),
    [AreaType.ROOM_TYPE]: Object.values(AreaSubtype),
    [AreaType.BUILDING_TYPE]: Object.values(AreaSubtype),
    [AreaType.BASE_TYPE]: Object.values(AreaSubtype),
  },
  [LocationType.HOTEL_TYPE]: {
    [AreaType.RESTAURANT_TYPE]: Object.values(AreaSubtype),
    [AreaType.ROOM_TYPE]: [AreaSubtype.NULL],
    [AreaType.BUILDING_TYPE]: [AreaSubtype.NULL],
    [AreaType.BASE_TYPE]: [AreaSubtype.NULL],
  },
  [LocationType.NURSING_HOME_TYPE]: {
    [AreaType.RESTAURANT_TYPE]: Object.values(AreaSubtype),
    [AreaType.ROOM_TYPE]: [AreaSubtype.NULL],
    [AreaType.BUILDING_TYPE]: [AreaSubtype.NULL],
    [AreaType.BASE_TYPE]: [AreaSubtype.NULL],
  },
  [LocationType.STORE_TYPE]: {
    [AreaType.RESTAURANT_TYPE]: Object.values(AreaSubtype),
    [AreaType.ROOM_TYPE]: [AreaSubtype.NULL],
    [AreaType.BUILDING_TYPE]: [AreaSubtype.NULL],
    [AreaType.BASE_TYPE]: [AreaSubtype.NULL],
  },
};
