import { registerHooks, registerShutdownHandler } from 'utils/lifecycle';
import http from 'http';
import config from 'config';
import * as client from 'utils/metrics';
import logger from 'utils/logger';
import { database } from 'database';

import { validateConfig } from 'utils/config';
import { configureApp, configureHealthChecks } from './app';

let server: http.Server;
let healthCheckServer: http.Server;

const startHTTPServer = async () => {
  logger.info('starting http server');

  healthCheckServer = http.createServer(configureHealthChecks());
  healthCheckServer.listen(config.get('healthCheckPort'));

  await new Promise<void>((resolve, reject) => {
    healthCheckServer.on('listening', () => {
      logger.info(
        `health check server listening on ${config.get('healthCheckPort')}`
      );
      resolve();
    });
    healthCheckServer.on('error', error => {
      logger.error(error, `could not start health check server`);
      reject();
    });
  });

  server = http.createServer(configureApp());
  server.listen(config.get('port'));

  await new Promise<void>(resolve => {
    server.on('listening', () => {
      logger.info(`http server listening on ${config.get('port')}`);
      resolve();
    });
  });
};

const stopHTTPServer = async () => {
  logger.info('stopping http server');

  await new Promise<void>((resolve, reject) => {
    server.close(error => {
      if (error) return reject(error);
      logger.info('http server stopped');
      return resolve();
    });
  });

  await new Promise<void>((resolve, reject) => {
    healthCheckServer.close(error => {
      if (error) return reject(error);
      logger.info('health check server stopped');
      return resolve();
    });
  });
};

const connectDatabase = async () => {
  logger.info('connecting to database');
  await database.authenticate();
  logger.info('connected to database');
};

const main = async () => {
  logger.info(
    `running with PID ${process.pid} in ${process.env.NODE_ENV} mode`
  );
  validateConfig();
  registerHooks();
  client.collectDefaultMetrics();
  await connectDatabase();
  await startHTTPServer();
  registerShutdownHandler(stopHTTPServer);
};

main();
