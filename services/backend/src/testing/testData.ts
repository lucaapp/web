import faker from 'faker';
import { OperatorInstance } from 'database/models/operator';
import { generateSupportCode } from '../utils/generators';
import { Operator } from '../database';

const DEVELOPMENT_EMAIL = 'luca@nexenio.com';

const setupOperator = async (operatorId: string): Promise<OperatorInstance> => {
  return Operator.create({
    uuid: operatorId,
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    businessEntityName: faker.name.lastName(),
    businessEntityStreetName: faker.address.streetName(),
    businessEntityStreetNumber: faker.address.streetSuffix(),
    businessEntityZipCode: faker.address.zipCode(),
    businessEntityCity: faker.address.city(),
    email: DEVELOPMENT_EMAIL,
    username: DEVELOPMENT_EMAIL,
    password: 'foo12145AD$',
    phone: faker.phone.phoneNumber(),
    salt: 'uyfd0yD92KwjGC7g/71z5w==',
    supportCode: generateSupportCode(),
  });
};

export { setupOperator };
