import { database } from 'database';
import { gracefulShutdown } from 'utils/lifecycle';
import { configureApp } from '../app';

let needsTearDown = true;
export async function setupDBRedisAndApp(): Promise<void> {
  needsTearDown = true;
  await database.sync();
  configureApp();
}

afterAll(async () => {
  try {
    if (needsTearDown) {
      await gracefulShutdown(true);
    }
  } finally {
    needsTearDown = false;
  }
});
