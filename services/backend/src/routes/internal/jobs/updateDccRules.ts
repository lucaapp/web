import { Router } from 'express';
import {
  base64ToHex,
  bytesToHex,
  SHA256,
  encodeUtf8,
  VERIFY_EC_SHA256_DER_SIGNATURE,
} from '@lucaapp/crypto';
import config from 'config';
import { z } from 'utils/validation';
import { cacheUrl } from 'utils/urlCache';

const router = Router();

const listValidator = async (payload: string) => {
  const parsedPayload = JSON.parse(payload);
  await z.dccListPayload().parseAsync(parsedPayload);
  return true;
};

router.post('/updateDCCRules', async (_, response) => {
  const url = config.get('luca.dcc.source');

  const data: string | null = await cacheUrl(url, listValidator);

  if (data) {
    const ruleValidator = async (
      payload: string,
      headers: Record<string, string>
    ) => {
      const isValid = VERIFY_EC_SHA256_DER_SIGNATURE(
        base64ToHex(config.get('luca.dcc.trustAnchor')),
        bytesToHex(SHA256(bytesToHex(encodeUtf8(payload)))),
        base64ToHex(headers['x-signature'])
      );

      if (!isValid) {
        throw new Error('Verification failed');
      }

      const parsedPayload = JSON.parse(payload);

      await z.dccRulePayload().parseAsync(parsedPayload);

      return true;
    };

    for (const { hash } of JSON.parse(data)) {
      const ruleUrl = `${url}/${hash}`;
      await cacheUrl(ruleUrl, ruleValidator);
    }
  }

  response.send({ affectedRows: null });
});

export default router;
