import { Router } from 'express';
import { Op, fn, col } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { Trace } from 'database';

const router = Router();

router.post('/checkoutOldTraces', async (_, response) => {
  const [affectedRows] = await Trace.update(
    {
      time: fn('tstzrange', fn('lower', col('time')), moment().toISOString()),
    },
    {
      where: {
        time: {
          [Op.contains]: [
            moment()
              .subtract(config.get('luca.traces.maxDuration'), 'hours')
              .toDate(),
            moment().toDate(),
          ],
        },
      },
    }
  );

  response.send({ affectedRows });
});

export default router;
