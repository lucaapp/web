import { Router } from 'express';
import { generateActiveChunk } from 'utils/notifications/notificationsV4';

const router = Router();

router.post('/regenerateV4NotificationsActiveChunk', async (_, response) => {
  await generateActiveChunk();

  response.send({ affectedRows: null });
});

export default router;
