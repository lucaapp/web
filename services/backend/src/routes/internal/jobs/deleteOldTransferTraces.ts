import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { LocationTransferTrace } from 'database';

const router = Router();

router.post('/deleteOldTransferTraces', async (_, response) => {
  const affectedRows = await LocationTransferTrace.destroy({
    where: {
      time: {
        [Op.strictLeft]: [
          moment().subtract(config.get('luca.traces.maxAge'), 'days').toDate(),
          // Couldn´t find a way to use Op.strictLeft with the null value conversion in typescript
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          null as any,
        ],
      },
    },
  });

  response.send({ affectedRows });
});

export default router;
