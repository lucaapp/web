import { Router } from 'express';
import { updateBloomFilter } from 'utils/bloomFilter';

const router = Router();

router.post('/regenerateBloomFilter', async (_, response) => {
  await updateBloomFilter();
  response.send({ affectedRows: null });
});

export default router;
