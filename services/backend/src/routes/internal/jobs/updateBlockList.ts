import { Router } from 'express';
import { updateBlockList } from 'utils/ipBlockList';

const router = Router();

router.post('/updateBlockList', async (_, response) => {
  await updateBlockList();
  response.send({ affectedRows: null });
});

export default router;
