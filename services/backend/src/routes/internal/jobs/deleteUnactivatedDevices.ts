import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { OperatorDevice } from 'database';

const router = Router();

router.post('/deleteUnactivatedDevices', async (_, response) => {
  const maxAge = config.get('luca.operatorDevice.unactivated.maxAgeMinutes');
  const affectedRows = await OperatorDevice.destroy({
    where: {
      activated: false,
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'minutes').toDate(),
      },
    },
  });

  response.send({ affectedRows });
});

export default router;
