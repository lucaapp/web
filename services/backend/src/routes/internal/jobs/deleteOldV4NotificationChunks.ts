import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { NotificationChunk } from 'database';

const router = Router();

router.post('/deleteOldV4NotificationChunks', async (_, response) => {
  const earliestTimeToKeep = moment()
    .subtract(config.get('luca.notificationChunks.maxAge'), 'hours')
    .toDate();

  const affectedRows = await NotificationChunk.destroy({
    where: {
      createdAt: {
        [Op.lt]: earliestTimeToKeep,
      },
    },
    force: true,
  });

  response.send({ affectedRows });
});

export default router;
