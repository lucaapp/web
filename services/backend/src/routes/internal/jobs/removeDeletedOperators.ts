import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { Operator } from 'database';

const router = Router();

router.post('/removeDeletedOperators', async (_, response) => {
  const earliestTimeToKeep = moment()
    .subtract(config.get('luca.operators.deleted.maxAgeHours'), 'hours')
    .toDate();

  const affectedRows = await Operator.destroy({
    where: {
      deletedAt: {
        [Op.lt]: earliestTimeToKeep,
      },
    },
    force: true,
  });

  response.send({ affectedRows });
});

export default router;
