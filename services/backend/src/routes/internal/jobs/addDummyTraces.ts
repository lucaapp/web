import { Router } from 'express';
import { Op } from 'sequelize';
import crypto from 'crypto';
import { GET_RANDOM_BYTES, hexToBase64 } from '@lucaapp/crypto';
import moment from 'moment';
import { get } from 'utils/featureFlag';
import { HealthDepartment, DummyTrace } from 'database';

const router = Router();

router.post('/addDummyTraces', async (_, response) => {
  const healthDepartments = await HealthDepartment.findAll({
    where: {
      publicHDSKP: {
        [Op.not]: null,
      },
    },
  });
  const healthDepartment =
    healthDepartments[crypto.randomInt(0, healthDepartments.length)];

  for (
    let tracingIndex = 0;
    tracingIndex < crypto.randomInt(0, await get('dummy_max_tracings'));
    tracingIndex += 1
  ) {
    const traces = [];
    const baseTime = moment().subtract(
      crypto.randomInt(0, moment.duration(10, 'days').asSeconds()),
      's'
    );

    for (
      let traceIndex = 0;
      traceIndex < crypto.randomInt(0, await get('dummy_max_traces'));
      traceIndex += 1
    ) {
      traces.push({
        healthDepartmentId: healthDepartment.uuid,
        traceId: hexToBase64(GET_RANDOM_BYTES(16)),
        createdAt: moment(baseTime).subtract(
          crypto.randomInt(0, 720) - 360,
          'minutes'
        ),
      });
    }
    await DummyTrace.bulkCreate(traces);
  }

  response.send({ affectedRows: null });
});

export default router;
