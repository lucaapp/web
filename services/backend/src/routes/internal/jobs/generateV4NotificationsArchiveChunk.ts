import { Router } from 'express';
import { generateArchiveChunk } from 'utils/notifications/notificationsV4';

const router = Router();

router.post('/generateV4NotificationsArchiveChunk', async (_, response) => {
  await generateArchiveChunk();

  response.send({ affectedRows: null });
});

export default router;
