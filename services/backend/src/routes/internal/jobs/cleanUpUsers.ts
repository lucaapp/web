import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { User } from 'database';

const router = Router();

router.post('/cleanUpUsers', async (_, response) => {
  const maxAge = config.get('luca.users.deleted.maxAge');
  const affectedRows = await User.destroy({
    where: {
      deletedAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours').toDate(),
      },
    },
    force: true,
  });

  response.send({ affectedRows });
});

export default router;
