import { Router } from 'express';
import moment from 'moment';
import { Op } from 'sequelize';
import { VaultContents } from 'database';

const router = Router();

router.post('/deleteOldVaults', async (_, response) => {
  const affectedRows = await VaultContents.destroy({
    where: {
      expirationDate: {
        [Op.lte]: moment().toDate(),
      },
    },
  });
  const responseData = { affectedRows };
  response.send(responseData);
});

export default router;
