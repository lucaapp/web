import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { User } from 'database';

const router = Router();

router.post('/deleteInactiveUsers', async (_, response) => {
  const maxAge = config.get('luca.users.inactive.maxAge');
  const affectedRows = await User.destroy({
    where: {
      updatedAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours').toDate(),
      },
      deviceType: null,
    },
    force: true,
  });

  response.send({ affectedRows });
});

export default router;
