import { Router } from 'express';
import {
  base64ToHex,
  bytesToHex,
  VERIFY_EC_SHA256_IEEE_SIGNATURE,
} from '@lucaapp/crypto';
import config from 'config';
import { z } from 'utils/validation';
import { cacheUrl } from 'utils/urlCache';

const router = Router();

router.post('/updateTrustList', async (_, response) => {
  const url = config.get('luca.trustList.source');

  const validator = async (data: string) => {
    const [signature, payload] = data.split('\n');

    const isValid = VERIFY_EC_SHA256_IEEE_SIGNATURE(
      base64ToHex(config.get('luca.trustList.trustAnchor')),
      bytesToHex(payload),
      base64ToHex(signature)
    );

    if (!isValid) {
      throw new Error('Verification failed');
    }

    const parsedPayload = JSON.parse(payload);

    await z.dsgcPayload().parseAsync(parsedPayload);

    return true;
  };

  await cacheUrl(url, validator);

  response.send({ affectedRows: null });
});

export default router;
