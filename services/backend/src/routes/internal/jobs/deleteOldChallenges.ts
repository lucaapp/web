import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { Challenge } from 'database';

const router = Router();

router.post('/deleteOldChallenges', async (_, response) => {
  const maxAge = config.get(
    'luca.challenges.operatorDeviceCreation.maxAgeMinutes'
  );
  const affectedRows = await Challenge.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'minutes').toDate(),
      },
    },
  });

  response.send({ affectedRows });
});

export default router;
