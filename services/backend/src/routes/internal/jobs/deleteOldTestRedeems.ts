import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { TestRedeem } from 'database';

const router = Router();

router.post('/deleteOldTestRedeems', async (_, response) => {
  const maxAge = config.get('luca.testRedeems.maxAge');
  const affectedRows = await TestRedeem.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours').toDate(),
      },
    },
  });

  response.send({ affectedRows });
});

export default router;
