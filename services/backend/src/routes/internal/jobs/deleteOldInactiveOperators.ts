import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { Operator } from 'database';

const router = Router();

router.post('/deleteOldInactiveOperators', async (_, response) => {
  const affectedRows = await Operator.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment()
          .subtract(config.get('emails.expiry'), 'hours')
          .toDate(),
      },
      activated: false,
    },
    force: true,
  });

  response.send({ affectedRows });
});

export default router;
