import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { UserTransfer } from 'database';

const router = Router();

router.post('/deleteUnusedUserTransfers', async (_, response) => {
  const affectedRows = await UserTransfer.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment()
          .subtract(config.get('luca.userTransfers.maxAgeUnused'), 'hours')
          .toDate(),
      },
      departmentId: null,
    },
  });

  response.send({ affectedRows });
});

export default router;
