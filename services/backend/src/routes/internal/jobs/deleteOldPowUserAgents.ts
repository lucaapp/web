import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { PowChallenge } from 'database';

const router = Router();

router.post('/deleteOldPowUserAgents', async (_, response) => {
  const maxAge = config.get('pow.userAgentMaxAge');
  const affectedRows = await PowChallenge.update(
    {
      userAgent: null,
    },
    {
      where: {
        userAgent: { [Op.not]: null },
        createdAt: {
          [Op.lt]: moment().subtract(maxAge, 'hours').toDate(),
        },
      },
    }
  );

  response.send({ affectedRows });
});

export default router;
