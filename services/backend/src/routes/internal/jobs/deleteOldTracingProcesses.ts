import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { TracingProcess } from 'database';

const router = Router();

router.post('/deleteOldTracingProcesses', async (_, response) => {
  const affectedRows = await TracingProcess.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment()
          .subtract(config.get('luca.tracingProcess.maxAge'), 'hours')
          .toDate(),
      },
    },
    force: true,
  });

  response.send({ affectedRows });
});

export default router;
