import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { HealthDepartmentAuditLog } from 'database';

const router = Router();

router.post('/deleteOldAuditLogs', async (_, response) => {
  const maxAge = config.get('luca.auditLogs.maxAge');
  const affectedRows = await HealthDepartmentAuditLog.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours').toDate(),
      },
    },
  });

  response.send({ affectedRows });
});

export default router;
