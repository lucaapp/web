import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { SMSChallenge } from 'database';

const router = Router();

router.post('/cleanUpChallenges', async (_, response) => {
  const affectedRows = await SMSChallenge.update(
    { messageId: '' },
    {
      where: {
        createdAt: {
          [Op.lt]: moment()
            .subtract(config.get('luca.smsChallenges.maxAge'), 'hours')
            .toDate(),
        },
      },
    }
  );

  response.send({ affectedRows });
});

export default router;
