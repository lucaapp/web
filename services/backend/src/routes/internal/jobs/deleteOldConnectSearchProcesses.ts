import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { ConnectSearchProcess } from 'database';

const router = Router();

router.post('/deleteOldConnectSearchProcesses', async (_, response) => {
  const maxAge = config.get('luca.connect.searchProcesses.maxAge');
  const affectedRows = await ConnectSearchProcess.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours').toDate(),
      },
    },
  });

  response.send({ affectedRows });
});

export default router;
