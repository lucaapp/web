import { Router } from 'express';
import moment from 'moment';
import { Op } from 'sequelize';
import config from 'config';
import { Trace } from 'database';

const router = Router();

router.post('/deleteOldTraces', async (_, response) => {
  let affectedRows = await Trace.destroy({
    where: {
      time: {
        [Op.strictLeft]: [
          moment().subtract(config.get('luca.traces.maxAge'), 'days').toDate(),
          // Couldn´t find a way to use Op.strictLeft with the null value conversion in typescript
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          null as any,
        ],
      },
    },
  });

  affectedRows += await Trace.destroy({
    where: {
      expiresAt: {
        [Op.lte]: moment().toDate(),
      },
    },
  });

  response.send({ affectedRows });
});

export default router;
