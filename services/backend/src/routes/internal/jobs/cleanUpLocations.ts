import { Router } from 'express';
import moment from 'moment';
import { Op } from 'sequelize';
import config from 'config';
import { LocationGroup, Location } from 'database';

const router = Router();

router.post('/cleanUpLocations', async (_, response) => {
  const maxAge = config.get('luca.locations.maxAge');
  const affectedGroupRows = await LocationGroup.destroy({
    where: {
      deletedAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours').toDate(),
      },
    },
    force: true,
  });
  const affectedLocationRows = await Location.destroy({
    where: {
      deletedAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours').toDate(),
      },
    },
    force: true,
  });

  response.send({ affectedRows: affectedGroupRows + affectedLocationRows });
});

export default router;
