import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { ConnectConversation } from 'database';

const router = Router();

router.post('/deleteOldConnectConversations', async (_, response) => {
  const maxAge = config.get('luca.connect.conversations.maxAge');
  const affectedRows = await ConnectConversation.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours').toDate(),
      },
    },
  });

  response.send({ affectedRows });
});

export default router;
