import { Router } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import { ConnectContact } from 'database';

const router = Router();

router.post('/deleteOldConnectContacts', async (_, response) => {
  const maxAge = config.get('luca.connect.contacts.maxAge');
  const affectedRows = await ConnectContact.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours').toDate(),
      },
    },
  });

  response.send({ affectedRows });
});

export default router;
