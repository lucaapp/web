/* eslint-disable sonarjs/no-duplicate-string */
const router = require('express').Router();
const status = require('http-status');
const crypto = require('crypto');
const { performance } = require('perf_hooks');
const { generatePassword } = require('../../utils/generators');
const { validateSchema } = require('../../middlewares/validateSchema');
const database = require('../../database');

const {
  storeKeysSchema,
  activationSchema,
  createHDAdminSchema,
} = require('./en2end.schema');

router.get('/getHealthDepartment', async (request, response) => {
  const department = await database.HealthDepartment.findOne({
    where: {
      name: 'neXenio Testing',
    },
  });

  if (!department) {
    return response.send(status.NOT_FOUND);
  }
  return response.send({
    departmentId: department.uuid,
    name: department.name,
    publicHDEKP: department.publicHDEKP,
    publicHDSKP: department.publicHDSKP,
    email: department.email,
    phone: department.phone,
  });
});

router.post(
  '/healthDepartmentEmployees',
  validateSchema(createHDAdminSchema),
  async (request, response) => {
    const initialPassword = generatePassword(8);

    const {
      email,
      firstName,
      lastName,
      phone,
      isAdmin,
      departmentId,
    } = request.body;

    const employee = await database.HealthDepartmentEmployee.create({
      email,
      firstName,
      lastName,
      phone,
      isAdmin,
      departmentId,
      password: initialPassword,
      salt: crypto.randomBytes(16).toString('base64'),
    });

    response.status(status.CREATED);
    return response.send({ email: employee.email, password: initialPassword });
  }
);

router.post(
  '/signHealthDepartment',
  validateSchema(storeKeysSchema),
  async (request, response) => {
    const healthDepartment = await database.HealthDepartment.findOne({
      where: {
        name: 'neXenio Testing',
      },
    });

    await healthDepartment.update({
      publicCertificate: request.body.publicCertificate,
      signedPublicHDEKP: request.body.signedPublicHDEKP,
      signedPublicHDSKP: request.body.signedPublicHDSKP,
    });

    response.sendStatus(status.NO_CONTENT);
  }
);

router.post('/unsignHealthDepartment', async (request, response) => {
  const healthDepartment = await database.HealthDepartment.findOne({
    where: {
      name: 'neXenio Testing',
    },
  });

  await Promise.all([
    healthDepartment.update({
      publicHDEKP: null,
      publicHDSKP: null,
      publicCertificate: null,
      signedPublicHDEKP: null,
      signedPublicHDSKP: null,
    }),
    database.TracingProcess.destroy({ where: {} }),
    database.DailyPublicKey.destroy({ where: {} }),
    database.EncryptedDailyPrivateKey.destroy({ where: {} }),
  ]);

  response.sendStatus(status.NO_CONTENT);
});

router.post(
  '/activateOperator',
  validateSchema(activationSchema),
  async (request, response) => {
    const operator = await database.Operator.findOne({
      where: {
        email: request.body.email,
      },
    });

    await operator.update({
      activated: true,
    });

    response.sendStatus(status.NO_CONTENT);
  }
);

router.post('/clean', async (request, response) => {
  const t0 = performance.now();

  const [workflowOperator, healthDepartmentEmployee] = await Promise.all([
    database.Operator.findOne({
      where: {
        email: 'complete_workflow@nexenio.com',
      },
    }),
    database.HealthDepartmentEmployee.findOne({
      where: {
        email: 'luca@nexenio.com',
      },
    }),
  ]);

  await Promise.all([
    workflowOperator?.update({
      publicKey: null,
      password: 'workflowTesting!',
    }),
    healthDepartmentEmployee?.update({
      password: 'testing',
    }),
    workflowOperator &&
      database.LocationGroup.destroy({
        where: {
          operatorId: workflowOperator.uuid,
        },
      }),
    database.HealthDepartment.update(
      {
        publicHDEKP: null,
        publicHDSKP: null,
        publicCertificate: null,
        signedPublicHDEKP: null,
        signedPublicHDSKP: null,
      },
      { where: {} }
    ),
    database.TracingProcess.destroy({ where: {} }),
    database.DailyPublicKey.destroy({ where: {} }),
    database.EncryptedDailyPrivateKey.destroy({ where: {} }),
  ]);

  return response.send({ time: performance.now() - t0 });
});
module.exports = router;
