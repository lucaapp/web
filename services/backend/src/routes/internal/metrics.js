const router = require('express').Router();

const client = require('../../utils/metrics');

router.get('/', async (request, response) => {
  response.type('text/plain');
  response.send(await client.register.metrics());
});

module.exports = router;
