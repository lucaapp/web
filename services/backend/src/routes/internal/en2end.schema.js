const { z } = require('../../utils/validation');

const storeKeysSchema = z.object({
  publicCertificate: z.string(),
  signedPublicHDEKP: z.string(),
  signedPublicHDSKP: z.string(),
});

const activationSchema = z.object({
  email: z.string(),
});

const createHDAdminSchema = z.object({
  firstName: z.safeString().max(255),
  lastName: z.safeString().max(255),
  phone: z.safeString().max(255),
  email: z.email(),
  isAdmin: z.boolean(),
  departmentId: z.safeString(),
});

module.exports = {
  storeKeysSchema,
  activationSchema,
  createHDAdminSchema,
};
