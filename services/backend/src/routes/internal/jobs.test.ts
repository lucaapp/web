/* eslint-disable unicorn/consistent-function-scoping */
/* eslint-disable @typescript-eslint/no-explicit-any */
import express from 'express';
import request from 'supertest';
import moment from 'moment';
import 'express-async-errors';
import { setupDBRedisAndApp } from 'testing/utils';
import { VaultContents } from '../../database';
import { handle500 } from '../../middlewares/error';
import jobsRouter from './jobs';
import { httpLogger } from '../../utils/logger';

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(httpLogger);
app.use(handle500);
app.use('', jobsRouter);

const vaultId = '57a21705-be97-402c-8ec2-eded39ee17a1';
const expiredVaultId = 'a53fd3ac-c750-4c36-ac1a-da11a919a559';

const vaultContent = {
  id: vaultId,
  version: 1,
  expirationDate: moment().add(1, 'day').unix(),
  encryptedPayload: 'dGhpcyBpcyBob3BlZnVsbHkgbG9uZ2VyIHRoYW4gMTYgYnl0ZXM=',
};

const totpSecret = 'c3RpbGwgYWxpdmU=';

const createVaultInDatabase = async () =>
  VaultContents.create({
    id: vaultId,
    version: vaultContent.version,
    encryptedPayload: Buffer.from(vaultContent.encryptedPayload, 'base64'),
    expirationDate: moment.unix(vaultContent.expirationDate).toDate(),
    totpSecret: Buffer.from(totpSecret, 'base64'),
  });

const createExpiredVaultInDatabase = async () =>
  VaultContents.create({
    id: expiredVaultId,
    version: vaultContent.version,
    encryptedPayload: Buffer.from(vaultContent.encryptedPayload, 'base64'),
    expirationDate: moment().toDate(),
    totpSecret: Buffer.from(totpSecret, 'base64'),
  });

describe('Interal jobs router', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
  });

  afterEach(() => {
    VaultContents.truncate();
  });

  describe('vault route', () => {
    it('should only cleanup expired vault objects', async () => {
      createExpiredVaultInDatabase();
      createVaultInDatabase();
      await request(app)
        .post('/deleteOldVaults')
        .expect('Content-Type', /json/)
        .expect(200);

      const expiredVaultInDatabase = await VaultContents.findByPk(
        expiredVaultId
      );
      expect(expiredVaultInDatabase).toBeNull();

      const vaultInDatabase = await VaultContents.findByPk(vaultId);
      expect(vaultInDatabase).not.toBeNull();
      expect(vaultInDatabase?.id).toBe(vaultId);
    });
  });
});
