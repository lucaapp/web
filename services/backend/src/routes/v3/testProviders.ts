import { Router } from 'express';
import { TestProvider } from 'database';

const router = Router();

router.get('/', async (_, response) => {
  const testProviders = await TestProvider.findAll();

  return response.send(
    testProviders.map(({ name, publicKey, fingerprint }) => ({
      name,
      publicKey,
      fingerprint,
    }))
  );
});

export default router;
