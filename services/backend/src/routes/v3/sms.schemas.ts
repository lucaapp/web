import { z } from 'utils/validation';

export const requestSchema = z.object({
  phone: z.phoneNumber(),
});

export const verifySchema = z.object({
  challengeId: z.uuid(),
  tan: z.string().max(6),
});

export const bulkVerifySchema = z.object({
  challengeIds: z.array(z.uuid()).min(1).max(10),
  tan: z.string().max(6),
});
