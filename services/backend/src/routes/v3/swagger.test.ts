import request from 'supertest';
import SwaggerParser from '@apidevtools/swagger-parser';

import { setupDBRedisAndApp } from 'testing/utils';
import { getApp } from '../../app';

describe('OpenAPI', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
  });

  it('should serve the OpenAPI spec', async () => {
    const response = await request(getApp())
      .get('/api/v3/swagger/swagger.json')
      .expect(200);

    const spec = JSON.parse(response.text);
    expect(spec.openapi).toEqual('3.0.0');
  });

  it('should generate a valid spec', async () => {
    const response = await request(getApp()).get(
      '/api/v3/swagger/swagger.json'
    );

    const spec = JSON.parse(response.text);
    const api = await SwaggerParser.validate(spec);
    expect(api.openapi).toEqual('3.0.0');
  });
});
