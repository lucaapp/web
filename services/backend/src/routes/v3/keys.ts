import { Router } from 'express';
import dailyRouter from './keys/daily';
import badgesRouter from './keys/badges';
import issuersRouter from './keys/issuers';

const router = Router();

router.use('/daily', dailyRouter);
router.use('/badge', badgesRouter);
router.use('/badges', badgesRouter);
router.use('/issuers', issuersRouter);

export default router;
