/* eslint-disable max-lines */
const router = require('express').Router();
const config = require('config');
const status = require('http-status');
const moment = require('moment');
const { Op } = require('sequelize');
const { convertLngLatToGeoPoint } = require('../../utils/geospatial');

const {
  createSchema,
  searchSchema,
  groupIdSchema,
  updateSchema,
  deleteSchema,
} = require('./locationGroups.schemas');

const {
  database,
  Location,
  LocationGroup,
  Operator,
  AdditionalDataSchema,
  Trace,
} = require('../../database');
const {
  validateSchema,
  validateParametersSchema,
  validateQuerySchema,
} = require('../../middlewares/validateSchema');
const {
  limitRequestsPerDay,
  limitRequestsByUserPerMinute,
  limitRequestsPerMinute,
} = require('../../middlewares/rateLimit');
const {
  requireOperator,
  requireNonDeletedUser,
  requireOperatorOROperatorDevice,
  requireHealthDepartmentEmployee,
} = require('../../middlewares/requireUser');
const { AuditLogEvents, AuditStatusType } = require('../../constants/auditLog');
const { logEvent } = require('../../utils/hdAuditLog');

// HD search for location group
router.get(
  '/search',
  requireHealthDepartmentEmployee,
  limitRequestsByUserPerMinute('locationgroup_search_get_ratelimit_minute'),
  validateQuerySchema(searchSchema),
  async (request, response) => {
    const limit = Number.parseInt(request.query.limit, 10);
    const offset = Number.parseInt(request.query.offset, 10);
    const maxLimit = 100;

    const { name, zipCode } = request.query;

    if (!name && !zipCode) return response.send(status.BAD_REQUEST);

    const matchedGroups = await LocationGroup.findAll({
      where: {
        ...(name && {
          name: {
            [Op.iLike]: `%${name}%`,
          },
        }),
      },
      include: [
        {
          model: Location,
          attributes: ['uuid'],
          where: {
            name: null,
            ...(zipCode && { zipCode }),
          },
          paranoid: false,
        },
      ],
      limit: Math.min(limit || 10, maxLimit),
      offset: offset || 0,
      paranoid: false,
    });

    const groups = await LocationGroup.findAll({
      where: {
        uuid: matchedGroups.map(group => group.uuid),
      },
      include: [
        {
          model: Operator,
          attributes: ['uuid', 'email'],
          required: true,
        },
        {
          model: Location,
          attributes: [
            'uuid',
            'name',
            'streetName',
            'streetNr',
            'zipCode',
            'city',
          ],
          where: { name: null },
          as: 'BaseLocation',
          paranoid: false,
        },
        {
          model: Location,
          attributes: ['uuid'],
          limit: config.get('entity_limits.locations_per_operator'),
        },
      ],
      paranoid: false,
    });

    logEvent(request.user, {
      type: AuditLogEvents.SEARCH,
      status: AuditStatusType.SUCCESS,
    });

    return response.send(
      groups.map(group => ({
        groupId: group.uuid,
        name: group.name,
        operator: {
          uuid: group.Operator.uuid,
          email: group.Operator.email,
        },
        baseLocation: {
          uuid: group.BaseLocation.uuid,
          name: group.BaseLocation.name,
          streetName: group.BaseLocation.streetName,
          streetNr: group.BaseLocation.streetNr,
          zipCode: group.BaseLocation.zipCode,
          city: group.BaseLocation.city,
        },
        locations: group.Locations.map(location => location.uuid),
      }))
    );
  }
);

// create location group
router.post(
  '/',
  requireOperator,
  requireNonDeletedUser,
  validateSchema(createSchema),
  limitRequestsPerDay('locationgroup_post_ratelimit_day'),
  async (request, response) => {
    const operator = await Operator.findOne({
      where: {
        uuid: request.user.uuid,
      },
    });

    if (!operator) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const existingLocationGroupCount = await LocationGroup.count({
      where: {
        operatorId: request.user.uuid,
      },
    });

    const maxLocationGroupCount = config.get(
      'entity_limits.location_groups_per_operator'
    );

    if (existingLocationGroupCount >= maxLocationGroupCount) {
      return response.sendStatus(status.LOCKED);
    }

    let group;
    let location;
    let tableCount;

    await database.transaction(async transaction => {
      group = await LocationGroup.create(
        {
          name: request.body.name,
          operatorId: request.user.uuid,
          type: request.body.type,
        },
        { transaction }
      );

      location = await Location.create(
        {
          operator: request.user.uuid,
          groupId: group.uuid,
          publicKey: operator.publicKey,
          name: null,
          firstName: request.body.firstName || request.user.firstName,
          lastName: request.body.lastName || request.user.lastName,
          phone: request.body.phone,
          streetName: request.body.streetName,
          streetNr: request.body.streetNr,
          zipCode: request.body.zipCode,
          city: request.body.city,
          state: request.body.state,
          geometricPoint: convertLngLatToGeoPoint({
            lng: request.body.lng,
            lat: request.body.lat,
          }),
          radius: request.body.radius,
          entryPolicy: request.body.entryPolicy,
          type: request.body.type,
          isIndoor: request.body.isIndoor,
          masks: request.body.masks,
          ventilation: request.body.ventilation,
          entryPolicyInfo: request.body.entryPolicyInfo,
          roomHeight: request.body.roomHeight,
          roomWidth: request.body.roomWidth,
          roomDepth: request.body.roomDepth,
          averageCheckinTime: request.body.averageCheckinTime || null,
          discoveryEnabled: request.body.discoveryEnabled || false,
        },
        { transaction }
      );

      const tables = await location.initLocationTables(
        request.body.tableCount,
        transaction
      );

      tableCount = tables.length;

      if (request.body.additionalData) {
        await Promise.all(
          request.body.additionalData.map(data =>
            AdditionalDataSchema.create(
              {
                locationId: location.uuid,
                ...data,
              },
              { transaction }
            )
          )
        );
      }

      if (request.body.areas) {
        await Promise.all(
          request.body.areas.map(area =>
            Location.create(
              {
                operator: request.user.uuid,
                groupId: group.uuid,
                publicKey: operator.publicKey,
                name: area.name,
                firstName: request.body.firstName || request.user.firstName,
                lastName: request.body.lastName || request.user.lastName,
                phone: request.body.phone,
                streetName: request.body.streetName,
                streetNr: request.body.streetNr,
                zipCode: request.body.zipCode,
                city: request.body.city,
                state: request.body.state,
                geometricPoint: convertLngLatToGeoPoint({
                  lng: request.body.lng,
                  lat: request.body.lat,
                }),
                isIndoor: area.isIndoor,
              },
              { transaction }
            )
          )
        );
      }
    });

    response.status(status.CREATED);
    return response.send({
      groupId: group.uuid,
      name: group.name,
      location: {
        scannerId: location.scannerId,
        locationId: location.uuid,
        tableCount,
      },
    });
  }
);

// get all location groups
router.get('/', requireOperatorOROperatorDevice, async (request, response) => {
  const groups = await LocationGroup.findAll({
    where: { operatorId: request.user.uuid },
    order: [['name', 'ASC']],
    include: {
      model: Location,
      attributes: ['uuid', 'name', 'groupId', 'type', 'subtype'],
      order: [['name', 'ASC']],
      limit: config.get('entity_limits.locations_per_operator'),
    },
  });

  response.send(
    groups.map(group => ({
      groupId: group.uuid,
      name: group.name,
      locations: group.Locations,
      type: group.type,
    }))
  );
});

// get a single location group
router.get(
  '/:groupId',
  validateParametersSchema(groupIdSchema),
  async (request, response) => {
    const group = await LocationGroup.findOne({
      where: { uuid: request.params.groupId },
      include: {
        model: Location,
        attributes: [
          'uuid',
          'name',
          'streetName',
          'streetNr',
          'city',
          'zipCode',
          'phone',
          'lat',
          'lng',
          'entryPolicy',
          'isContactDataMandatory',
          'type',
          'subtype',
        ],
        order: [['name', 'ASC']],
      },
    });

    if (!group) {
      return response.sendStatus(status.NOT_FOUND);
    }

    return response.send({
      groupId: group.uuid,
      operatorId: group.operatorId,
      name: group.name,
      locations: group.Locations,
      type: group.type,
      discoveryEnabled: group.discoveryEnabled,
    });
  }
);

// update location group
router.patch(
  '/:groupId',
  requireOperator,
  requireNonDeletedUser,
  validateParametersSchema(groupIdSchema),
  validateSchema(updateSchema),
  async (request, response) => {
    const group = await LocationGroup.findOne({
      where: { uuid: request.params.groupId, operatorId: request.user.uuid },
    });

    if (!group) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const baseLocation = await Location.findOne({
      where: { groupId: request.params.groupId, name: null },
    });

    if (!baseLocation) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const {
      phone,
      name,
      openingHours,
      individualOpeningHours,
      type,
      discoveryEnabled,
    } = request.body;

    await database.transaction(async transaction => {
      return Promise.all([
        baseLocation.update(
          {
            phone,
            openingHours,
          },
          { transaction }
        ),
        group.update(
          {
            name,
            individualOpeningHours,
            openingHours,
            type,
            discoveryEnabled,
          },
          { transaction }
        ),
      ]);
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

// delete location group
router.delete(
  '/:groupId',
  limitRequestsPerMinute('location_groups_delete_minute', {
    skipSuccessfulRequests: true,
    global: false,
  }),
  requireOperator,
  requireNonDeletedUser,
  validateParametersSchema(groupIdSchema),
  validateSchema(deleteSchema),
  async (request, response) => {
    const operator = request.user;

    const group = await LocationGroup.findOne({
      where: {
        uuid: request.params.groupId,
        operatorId: operator.uuid,
      },
      include: {
        model: Location,
      },
    });

    if (!group) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const { password } = request.body;

    const isPasswordCorrect = await operator.checkPassword(password);

    if (!isPasswordCorrect) {
      return response.sendStatus(status.FORBIDDEN);
    }

    await group.destroy();
    for (const location of group.Locations) {
      await location.destroy();
    }

    return response.sendStatus(status.NO_CONTENT);
  }
);

/**
 * Check-out all guests of all group venues
 * @param groupId
 */
router.post(
  '/:groupId/checkout',
  requireOperatorOROperatorDevice,
  requireNonDeletedUser,
  validateParametersSchema(groupIdSchema),
  async (request, response) => {
    const locations = await Location.findAll({
      where: {
        operator: request.user.uuid,
        groupId: request.params.groupId,
      },
      attributes: ['uuid'],
    });

    await database.transaction(async transaction => {
      for (const location of locations) {
        await location.checkoutAllTraces(transaction);
      }
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

/**
 * Returns total amount of check-ins for the an array of scanner access ids.
 */
router.get(
  '/:groupId/traces/count/total',
  requireOperatorOROperatorDevice,
  requireNonDeletedUser,
  validateParametersSchema(groupIdSchema),
  async (request, response) => {
    const locations = await Location.findAll({
      attributes: ['uuid'],
      where: {
        operator: request.user.uuid,
        groupId: request.params.groupId,
      },
    });

    const locationIds = locations.map(location => location.uuid);

    const count = await Trace.count({
      where: {
        locationId: locationIds,
        time: {
          [Op.contains]: moment(),
        },
      },
    });

    response.type('text/plain');
    return response.send(`${count}`);
  }
);

module.exports = router;
