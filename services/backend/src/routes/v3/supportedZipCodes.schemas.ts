import { z } from 'utils/validation';

export const zipParametersSchema = z.object({
  zipCode: z.zipCode(),
});
