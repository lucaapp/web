import { z } from 'utils/validation';

export const checkinSchema = z.object({
  traceId: z.traceId(),
  scannerId: z.uuid(),
  timestamp: z.unixTimestamp(),
  data: z.base64({ max: 128 }),
  iv: z.iv(),
  mac: z.mac(),
  publicKey: z.ecPublicKey(),
  deviceType: z.deviceType(),
  authPublicKey: z.ecPublicKey().optional(),
});

export const checkoutSchema = z.object({
  traceId: z.traceId(),
  timestamp: z.unixTimestamp(),
  signature: z.ecSignature().optional(),
});

export const deleteSchema = z.object({
  traceId: z.traceId(),
  signature: z.ecSignature(),
  timestamp: z.unixTimestamp(),
});

export const additionalDataSchema = z.object({
  traceId: z.traceId(),
  data: z.base64({ max: 4096 }),
  iv: z.iv(),
  mac: z.mac(),
  publicKey: z.ecPublicKey(),
});

export const bulkSchema = z.object({
  traceIds: z.array(z.traceId()).max(360),
});

export const traceIdParametersSchema = z.object({
  traceId: z
    .string()
    .regex(/^[\da-f]+$/)
    .length(32),
});

const maxTracesPerDay = 1500; // rounded up from 60 [minutes] * 24 [hours]
const numberOfDays = 14;
export const traceSchema = z.object({
  traceIds: z.array(z.traceId()).max(maxTracesPerDay * numberOfDays),
});
