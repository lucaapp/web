paths:
  /locationTransfers:
    post:
      tags:
        - LocationTransfers
      summary: Create new
      operationId: post-locationTransfers
      responses:
        '200':
          description: OK
        '400':
          $ref: '#/components/schemas/BadRequest'
        '401':
          $ref: '#/components/schemas/RequiredUserOfTypeHealthDepartmentEmployee'
        '404':
          description: User Transfer not found
        '413':
          description: Too many locations attached to request
      requestBody:
        content:
          application/json:
            schema:
              description: ''
              type: object
              properties:
                locations:
                  type: array
                  uniqueItems: true
                  items:
                    type: object
                    properties:
                      time:
                        type: array
                        minItems: 2
                        maxItems: 2
                        items:
                          type: integer
                      locationId:
                        type: string
                        minLength: 1
                    required:
                      - locationId
                userTransferId:
                  type: string
                  minLength: 1
              required:
                - locations
                - lang
            examples:
              Example:
                value:
                  locations:
                    - time:
                        - 100
                        - 120
                      locationId: string
                  userTransferId: string
    get:
      tags:
        - LocationTransfers
      summary: ''
      parameters:
        - in: query
          name: completed
          required: false
          schema:
            type: boolean
          allowEmptyValue: true
          description: Filter by completed or uncompleted
        - in: query
          name: deleted
          required: false
          schema:
            type: boolean
          allowEmptyValue: true
          description: Filter by deleted
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                description: ''
                minItems: 1
                uniqueItems: true
                items:
                  type: object
                  properties:
                    uuid:
                      type: string
                      minLength: 1
                    groupName:
                      type: string
                      minLength: 1
                    locationName: {}
                    name:
                      type: string
                      minLength: 1
                    departmentName:
                      type: string
                      minLength: 1
                    time:
                      type: array
                      minItems: 2
                      maxItems: 2
                      items:
                        type: integer
                    isCompleted:
                      type: boolean
                    createdAt:
                      type: integer
                    deletedAt:
                      type: integer
                    approvedAt:
                      type: integer
                  required:
                    - uuid
                    - groupName
                    - name
                    - departmentName
                    - time
                    - isCompleted
                    - createdAt
        '401':
          $ref: '#/components/schemas/RequiredUserOfTypeOperator'
  '/locationTransfers/{transferId}':
    parameters:
      - schema:
          type: string
        name: transferId
        in: path
        required: true
    get:
      summary: Get a specific transfer
      tags:
        - LocationTransfers
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  transferId:
                    type: string
                    minLength: 1
                  department:
                    type: object
                    required:
                      - uuid
                      - name
                      - publicHDEKP
                    properties:
                      uuid:
                        type: string
                        minLength: 1
                      name:
                        type: string
                        minLength: 1
                      publicHDEKP:
                        type: string
                        minLength: 1
                  location:
                    type: object
                    required:
                      - groupId
                      - groupName
                      - name
                      - publicKey
                    properties:
                      groupId:
                        type: string
                        minLength: 1
                      groupName:
                        type: string
                        minLength: 1
                      locationName: {}
                      name:
                        type: string
                        minLength: 1
                      publicKey:
                        type: string
                        minLength: 1
                  time:
                    type: array
                    items:
                      type: integer
                  traces:
                    type: array
                    uniqueItems: true
                    minItems: 1
                    items:
                      type: object
                      properties:
                        traceId:
                          type: string
                          minLength: 1
                        time:
                          type: array
                          items:
                            type: object
                        data:
                          oneOf:
                            - type: string
                              minLength: 1
                            - type: object
                              required:
                                - data
                                - iv
                                - mac
                                - publicKey
                        publicKey:
                          type: string
                          minLength: 1
                        iv:
                          type: string
                          minLength: 1
                        mac:
                          type: string
                          minLength: 1
                        additionalData: {}
                      required:
                        - traceId
                        - data
                        - publicKey
                        - iv
                        - mac
                  deletedAt:
                    type: integer
                required:
                  - transferId
                  - department
                  - location
                  - time
                  - traces
        '400':
          $ref: '#/components/schemas/BadRequest'
        '403':
          description: Requesting user is not owner
        '404':
          description: Transfer or Department not found
        '410':
          description: Transfer already completed
      operationId: get-locationTransfers-transferId
      description: Get a specific transfer. Contains more details.
    post:
      summary: Create a location transfer
      tags:
        - LocationTransfers
      operationId: post-locationTransfers-transferId
      responses:
        '200':
          description: OK
        '403':
          description: Requesting user is not owner
        '400':
          $ref: '#/components/schemas/BadRequest'
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                traces:
                  type: array
                  items:
                    type: object
                    properties:
                      data:
                        type: object
                        required:
                          - data
                          - iv
                          - mac
                          - publicKey
                      publicKey:
                        type: string
                      keyId:
                        type: string
                      version:
                        type: integer
                      verification:
                        type: string
                      deviceType:
                        type: integer
                      additionalData:
                        type: object
                      entryPolicyInfo:
                        type: string
                        enum: [2G, 3G, 2GPlus, none]
                      isIndoor:
                        type: boolean
                      ventilation:
                        type: string
                        enum: [electric, window, partial, no]
                      masks:
                        type: string
                        enum: [yes, partial, no]
                      roomHeight:
                        type: number
                      roomWidth:
                        type: number
                      roomDepth:
                        type: number
                    required:
                      - data
                      - traceId
                      - publicKey
                      - keyId
                      - verification
                      - deviceType
              required:
                - traces
        description: ''
  '/locationTransfers/{transferId}/traces':
    parameters:
      - schema:
          type: string
        name: transferId
        in: path
        required: true
    get:
      summary: Get traces of a location transfer
      tags:
        - LocationTransfers
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                description: ''
                type: object
                properties:
                  traces:
                    type: array
                    uniqueItems: true
                    minItems: 1
                    items:
                      required:
                        - traceId
                        - checkin
                        - checkout
                        - data
                        - publicKey
                        - keyId
                        - version
                        - verification
                        - deviceType
                      properties:
                        traceId:
                          type: string
                          minLength: 1
                        checkin:
                          type: number
                        checkout:
                          type: number
                        data:
                          type: object
                          required:
                            - data
                            - iv
                            - mac
                            - publicKey
                        publicKey:
                          type: string
                          minLength: 1
                        keyId:
                          type: number
                        version:
                          type: number
                        verification:
                          type: string
                          minLength: 1
                        initialTracesCount:
                          type: number
                        deviceType:
                          type: number
                        additionalData: {}
                required:
                  - traces
        '400':
          $ref: '#/components/schemas/BadRequest'
        '401':
          $ref: '#/components/schemas/RequiredUserOfTypeHealthDepartmentEmployee'
        '404':
          description: Transfer not found
      operationId: get-locationTransfers-transferId-traces
      description:
        Get traces of a location transfer. Only accessible by health department
        employees.
  '/locationTransfers/{transferId}/contact':
    parameters:
      - schema:
          type: string
        name: transferId
        in: path
        required: true
    post:
      summary: Notify location
      operationId: post-locationTransfers-transferId-contact
      responses:
        '204':
          description: Success
        '400':
          $ref: '#/components/schemas/BadRequest'
        '401':
          $ref: '#/components/schemas/RequiredUserOfTypeHealthDepartmentEmployee'
        '404':
          description: Transfer not found
        '410':
          description: Transfer already completed
      tags:
        - LocationTransfers
      parameters: []
      description: Sends a locationTransfer notification.
  '/locationTransfers/uncompleted':
    get:
      summary: Get non-completed transfers for this operator's locations
      description: Get non-completed transfers for this operator's locations
      tags: [LocationTransfers]
      responses:
        '200':
          description: OK
        '401':
          $ref: '#/components/schemas/RequiredUserOfTypeOperator'
