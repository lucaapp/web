import { z } from 'utils/validation';

export const issuerIdParametersSchema = z.object({
  issuerId: z.uuid(),
});
