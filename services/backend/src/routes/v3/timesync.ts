import moment from 'moment';
import { Router } from 'express';

const router = Router();

// get current server time
router.get('/', (request, response) => {
  return response.send({ unix: moment().unix() });
});

export default router;
