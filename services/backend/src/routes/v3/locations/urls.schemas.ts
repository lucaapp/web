import { z } from 'utils/validation';
import { UrlType } from 'database/models/locationUrl';

export const parameterSchema = z.object({
  locationId: z.uuid(),
});

export const updateSchema = z.object({
  type: z.nativeEnum(UrlType),
  url: z.urlString().nullable(),
});
