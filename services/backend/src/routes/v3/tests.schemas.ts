import { z } from 'utils/validation';

export const redeemSchema = z.object({
  hash: z.base64({ length: 44, rawLength: 32 }),
  tag: z.base64({ length: 24, rawLength: 16 }),
});

export const redeemDeleteSchema = z.object({
  hash: z.string().length(44),
  tag: z.string().length(24),
});
