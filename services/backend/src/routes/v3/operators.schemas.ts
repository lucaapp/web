import { z } from 'utils/validation';
import { LocationType } from 'constants/location';
import { campaignBodySchema } from '../v4/campaigns.schemas';

export const createSchema = z.object({
  firstName: z.safeString().max(255),
  lastName: z.safeString().max(255),
  businessEntityName: z.safeString().max(255),
  businessEntityStreetName: z.safeString().max(255),
  businessEntityStreetNumber: z.safeString().max(255),
  businessEntityZipCode: z.zipCode(),
  businessEntityCity: z.safeString().max(255),
  email: z.email(),
  password: z.zxcvbnPassword(),
  agreement: z.boolean(),
  avvAccepted: z.literal(true),
  lastVersionSeen: z.string().max(32).optional(),
  phone: z.phoneNumber(),
  initialLocationGroup: z
    .object({
      name: z.safeString().max(255),
      tableCount: z.number().int().positive().max(1000).optional().nullable(),
      type: z.nativeEnum(LocationType),
    })
    .optional(),
  campaign: campaignBodySchema.optional(),
});

export const activationSchema = z.object({
  activationId: z.uuid(),
});

export const storePublicKeySchema = z.object({
  publicKey: z.ecPublicKey(),
});

export const updateOperatorSchema = z.object({
  firstName: z.safeString().max(255).optional(),
  phone: z.phoneNumber().optional(),
  lastName: z.safeString().max(255).optional(),
  businessEntityName: z.safeString().max(255).optional(),
  businessEntityStreetName: z.safeString().max(255).optional(),
  businessEntityStreetNumber: z.safeString().max(255).optional(),
  businessEntityZipCode: z.zipCode().optional(),
  businessEntityCity: z.safeString().max(255).optional(),
  avvAccepted: z.literal(true).optional(),
  lastVersionSeen: z.string().max(32).optional(),
  languageOverwrite: z.string().max(5).nullable().optional(),
});

export const supportSchema = z.object({
  requestText: z.safeText().max(3000),
  phone: z.safeString().max(255).optional(),
});

export const publicKeyResetSchema = z.object({
  publicKey: z.ecPublicKey(),
  privateKeySecret: z.base64({ length: 44 }),
  password: z.string().max(255),
});
