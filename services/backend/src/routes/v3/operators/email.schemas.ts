import { z } from 'utils/validation';

export const updateMailSchema = z.object({
  email: z.email(),
  password: z.string().max(255),
});

export const activationSchema = z.object({
  activationId: z.uuid(),
});
