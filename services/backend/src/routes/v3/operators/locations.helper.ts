import {
  AreaType,
  LocationType,
  AllowedSubtypeConfiguration,
} from 'constants/location';
import { LocationInstance } from 'database/models/location';

export const getAllowedSubtypes = (
  locationType?: AreaType,
  locationGroupType?: LocationType
): (string | null)[] | undefined =>
  AllowedSubtypeConfiguration?.[`${locationGroupType ?? 'base'}`]?.[
    `${locationType ?? 'base'}`
  ];

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const getOperatorLocationDTO = (
  location: LocationInstance,
  additional = {}
) => ({
  uuid: location.uuid,
  scannerId: location.scannerId,
  accessId: location.accessId,
  formId: location.formId,
  scannerAccessId: location.scannerAccessId,
  name: location.name,
  firstName: location.firstName,
  lastName: location.lastName,
  phone: location.phone,
  streetName: location.streetName,
  streetNr: location.streetNr,
  zipCode: location.zipCode,
  city: location.city,
  state: location.state,
  lat: location.lat,
  lng: location.lng,
  radius: location.radius,
  endsAt: location.endsAt,
  entryPolicy: location.entryPolicy,
  isPrivate: location.isPrivate,
  publicKey: location.publicKey,
  isIndoor: location.isIndoor,
  type: location.type,
  operator: location.operator,
  groupId: location.groupId,
  groupName: location.LocationGroup?.name,
  averageCheckinTime: location.averageCheckinTime,
  isContactDataMandatory: location.isContactDataMandatory,
  entryPolicyInfo: location.entryPolicyInfo,
  ventilation: location.ventilation,
  masks: location.masks,
  roomHeight: location.roomHeight,
  roomWidth: location.roomWidth,
  roomDepth: location.roomDepth,
  openingHours: location.openingHours,
  subtype: location.subtype,
  tableCount: location.LocationTables?.length || 0,
  ...additional,
});
