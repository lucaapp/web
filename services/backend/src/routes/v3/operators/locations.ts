/**
 * @overview Provides endpoints allowing operators to register/update/delete their venue and to
 * check-out all guests at once
 *
 * @see https://www.luca-app.de/securityoverview/processes/venue_registration.html
 * @see https://www.luca-app.de/securityoverview/properties/actors.html#term-Venue-Owner
 */

/* eslint-disable sonarjs/no-duplicate-string, max-lines */
import { Router } from 'express';
import config from 'config';
import status from 'http-status';
import moment from 'moment';
import { internal, badRequest } from '@hapi/boom';
import { Op, col, fn, where, and, Rangable } from 'sequelize';
import { formatPhoneNumber } from 'utils/format';
import {
  database,
  Location,
  LocationGroup,
  AdditionalDataSchema,
  Trace,
  TraceData,
  LocationTables,
} from 'database';

import {
  validateSchema,
  validateQuerySchema,
  validateParametersSchema,
} from 'middlewares/validateSchema';
import {
  limitRequestsPerHour,
  limitRequestsPerDay,
} from 'middlewares/rateLimit';
import {
  requireOperator,
  requireNonDeletedUser,
  requireOperatorDeviceRoles,
  requireOperatorOROperatorDevice,
} from 'middlewares/requireUser';
import { OperatorDevice } from 'constants/operatorDevice';

import { LocationInstance } from 'database/models/location';
import { convertLngLatToGeoPoint } from 'utils/geospatial';
import {
  createSchema,
  updateSchema,
  updateAddressSchema,
  locationTracesQuerySchema,
  locationIdParametersSchema,
  locationDeleteSchema,
} from './locations.schemas';
import { getOperatorLocationDTO, getAllowedSubtypes } from './locations.helper';

const router = Router();

/**
 * Get all locations (venues) operated by the currently logged-in owner
 */
router.get('/', requireOperatorOROperatorDevice, async (request, response) => {
  const locations = await Location.findAll({
    limit: config.get('entity_limits.locations_per_operator'),
    where: {
      operator: request.user.uuid,
    },
    include: [{ model: LocationTables, attributes: ['locationId'] }],
  });
  const locationDtos = locations.map(location =>
    getOperatorLocationDTO(location)
  );

  response.addEtag(locationDtos);
  response.send(locationDtos);
});

/**
 * Get specific location (venue). Requires the logged-in user to be the operator of said location
 */
router.get(
  '/:locationId',
  requireOperatorOROperatorDevice,
  validateParametersSchema(locationIdParametersSchema),
  async (request, response) => {
    const location = await Location.findOne({
      where: {
        uuid: request.params.locationId,
        operator: request.user.uuid,
      },
      include: [
        {
          model: LocationGroup,
          attributes: ['name'],
        },
        { model: LocationTables, attributes: ['locationId'] },
      ],
    });

    if (!location) {
      return response.sendStatus(status.NOT_FOUND);
    }
    const locationDto = getOperatorLocationDTO(location);

    response.addEtag(locationDto);
    return response.send(locationDto);
  }
);

/**
 * Create a new location (venue). The venue's private key will remain in the venue frontend
 * @see https://www.luca-app.de/securityoverview/processes/venue_registration.html
 * @see https://www.luca-app.de/securityoverview/properties/secrets.html#term-venue-keypair
 */
router.post(
  '/',
  requireOperator,
  requireNonDeletedUser,
  limitRequestsPerDay('operator_location_post_ratelimit_day'),
  validateSchema(createSchema),
  // eslint-disable-next-line sonarjs/cognitive-complexity
  async (request, response) => {
    const group = await LocationGroup.findOne({
      where: { uuid: request.body.groupId, operatorId: request.user.uuid },
    });

    if (!group) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const allowedSubtypes = getAllowedSubtypes(request.body.type, group.type);
    if (
      request.body.subtype !== undefined &&
      allowedSubtypes &&
      !allowedSubtypes.includes(request.body.subtype)
    ) {
      throw badRequest(
        `Subtype is not allowed for group type: ${group.type} and location type: ${request.body.type}`
      );
    }

    const trimmedLocationName = request.body.locationName.trim();
    const existingLocation = await Location.findOne({
      where: {
        name: trimmedLocationName,
        groupId: request.body.groupId,
      },
    });

    if (existingLocation) {
      return response.sendStatus(status.CONFLICT);
    }

    const existingLocationCount = await Location.count({
      where: {
        operator: request.user.uuid,
      },
    });

    const maxLocationsCount = config.get(
      'entity_limits.locations_per_operator'
    );

    if (existingLocationCount >= maxLocationsCount) {
      return response.sendStatus(status.LOCKED);
    }

    const baseLocation = await Location.findOne({
      where: { groupId: request.body.groupId },
    });

    if (!baseLocation) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const [location, tableCount] = await database.transaction(
      async transaction => {
        const createdLocation = await Location.create(
          {
            operator: request.user.uuid,
            publicKey: baseLocation.publicKey,
            groupId: request.body.groupId,
            name: trimmedLocationName,
            firstName: request.body.firstName || request.user.firstName,
            lastName: request.body.lastName || request.user.lastName,
            phone: formatPhoneNumber(request.body.phone ?? ''),
            streetName: request.body.streetName,
            streetNr: request.body.streetNr,
            zipCode: request.body.zipCode,
            city: request.body.city,
            state: request.body.state,
            geometricPoint: convertLngLatToGeoPoint({
              lng: request.body.lng,
              lat: request.body.lat,
            }),
            radius: request.body.radius ?? 0,
            entryPolicy: request.body.entryPolicy,
            isIndoor: request.body.isIndoor,
            masks: request.body.masks,
            ventilation: request.body.ventilation,
            entryPolicyInfo: request.body.entryPolicyInfo,
            roomHeight: request.body.roomHeight,
            roomWidth: request.body.roomWidth,
            roomDepth: request.body.roomDepth,
            type: request.body.type,
            averageCheckinTime: request.body.averageCheckinTime,
            subtype: request.body.subtype,
          },
          { transaction }
        );

        const tables = await createdLocation.initLocationTables(
          request.body.tableCount,
          transaction
        );

        if (request.body.additionalData) {
          await Promise.all(
            request.body.additionalData.map(data =>
              AdditionalDataSchema.create(
                {
                  locationId: (location as LocationInstance).uuid,
                  key: data.key,
                  label: data.label,
                  isRequired: data.isRequired,
                },
                { transaction }
              )
            )
          );
        }

        return [createdLocation, tables.length];
      }
    );

    response.status(status.CREATED);
    return response.send(
      getOperatorLocationDTO(location as LocationInstance, { tableCount })
    );
  }
);

/**
 * Update given location, owned by the logged-in operator
 * @param locationId of the venue to update
 */
router.patch(
  '/:locationId',
  requireOperator,
  requireNonDeletedUser,
  validateSchema(updateSchema),
  validateParametersSchema(locationIdParametersSchema),
  async (request, response) => {
    const location = await Location.findOne({
      where: {
        operator: request.user.uuid,
        uuid: request.params.locationId,
      },
      include: [{ model: LocationTables, attributes: ['locationId'] }],
    });

    if (!location) {
      return response.sendStatus(status.NOT_FOUND);
    }

    if (request.body.locationName) {
      const existingLocation = await Location.findOne({
        where: {
          name: request.body.locationName.trim(),
          groupId: location.groupId,
          uuid: {
            [Op.not]: request.params.locationId,
          },
        },
      });
      if (existingLocation) {
        return response.sendStatus(status.CONFLICT);
      }
    }

    if (request.body.subtype !== undefined) {
      const groupType = await location.getGroupType();
      const allowedSubtypes = getAllowedSubtypes(location.type, groupType);

      if (allowedSubtypes && !allowedSubtypes.includes(request.body.subtype)) {
        throw badRequest(
          `Subtype is not allowed for group type: ${groupType} and location type: ${location.type}`
        );
      }
    }

    await location.update({
      name: location.name ? request.body.locationName?.trim() : null,
      firstName: request.body.firstName,
      lastName: request.body.lastName,
      radius: request.body.radius,
      entryPolicy: request.body.entryPolicy,
      isIndoor: request.body.isIndoor,
      averageCheckinTime: request.body.averageCheckinTime,
      isContactDataMandatory: request.body.isContactDataMandatory,
      entryPolicyInfo: request.body.entryPolicyInfo,
      ventilation: request.body.ventilation,
      masks: request.body.masks,
      roomHeight: request.body.roomHeight,
      roomWidth: request.body.roomWidth,
      roomDepth: request.body.roomDepth,
      subtype: request.body.subtype,
      phone: formatPhoneNumber(request.body.phone),
    });

    return response.send(getOperatorLocationDTO(location));
  }
);

/**
 * Update given location address, owned by the logged-in operator
 * @param locationId of the venue to update
 */
router.patch(
  '/:locationId/address',
  requireOperator,
  requireNonDeletedUser,
  validateSchema(updateAddressSchema),
  validateParametersSchema(locationIdParametersSchema),
  async (request, response) => {
    const location = await Location.findOne({
      where: {
        operator: request.user.uuid,
        uuid: request.params.locationId,
      },
    });

    if (!location) return response.sendStatus(status.NOT_FOUND);

    await database.transaction(async transaction => {
      if (
        !request.body.streetName ||
        !request.body.city ||
        !request.body.zipCode
      )
        throw internal('Mandatory attributes are undefined');

      if (!location.name) {
        await Location.update(
          {
            streetName: request.body.streetName,
            streetNr: request.body.streetNr,
            zipCode: request.body.zipCode,
            city: request.body.city,
            state: request.body.state,
            geometricPoint: convertLngLatToGeoPoint({
              lng: request.body.lng,
              lat: request.body.lat,
            }),
          },
          {
            where: and(
              where(
                fn('lower', col('streetName')),
                request.body.streetName.toLowerCase() ?? ''
              ),
              where(
                fn('lower', col('city')),
                request.body.city.toLowerCase() ?? ''
              ),
              where(
                fn('lower', col('zipCode')),
                request.body.zipCode.toLowerCase() ?? ''
              ),
              {
                name: {
                  [Op.ne]: null,
                },
                groupId: location.groupId,
              }
            ),
            transaction,
          }
        );
      }
      await location.update(
        {
          streetName: request.body.streetName,
          streetNr: request.body.streetNr,
          zipCode: request.body.zipCode,
          city: request.body.city,
          state: request.body.state,
          geometricPoint: convertLngLatToGeoPoint({
            lng: request.body.lng,
            lat: request.body.lat,
          }),
          radius: request.body.radius,
        },
        {
          transaction,
        }
      );
    });
    return response.send(status.NO_CONTENT);
  }
);

// delete location
router.delete(
  '/:locationId',
  requireOperator,
  requireNonDeletedUser,
  validateSchema(locationDeleteSchema),
  validateParametersSchema(locationIdParametersSchema),
  async (request, response) => {
    const location = await Location.findOne({
      where: {
        operator: request.user.uuid,
        uuid: request.params.locationId,
      },
    });

    if (!location) return response.sendStatus(status.NOT_FOUND);

    const { password } = request.body;
    const isValidPassword = await request.user.checkPassword(password);

    if (!isValidPassword) {
      return response.sendStatus(status.FORBIDDEN);
    }

    await database.transaction(async transaction => {
      await location.checkoutAllTraces(transaction);
      await location.destroy({ transaction });
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

/**
 * Check-out all guests of a given venue
 * @param locationId of the venue
 */
router.post(
  '/:locationId/check-out',
  requireOperatorOROperatorDevice,
  requireOperatorDeviceRoles([OperatorDevice.employee, OperatorDevice.manager]),
  requireNonDeletedUser,
  validateParametersSchema(locationIdParametersSchema),
  async (request, response) => {
    const location = await Location.findOne({
      where: {
        operator: request.user.uuid,
        uuid: request.params.locationId,
      },
    });

    if (!location) {
      return response.sendStatus(status.NOT_FOUND);
    }
    await location.checkoutAllTraces();
    return response.sendStatus(status.NO_CONTENT);
  }
);

/**
 * Get the guest list of a location, effectively fetching trace IDs and their
 * associated encrypted data, decrypting contact data by a health department
 * still requires the user to consent/share required data
 * @see https://www.luca-app.de/securityoverview/processes/tracing_access_to_history.html
 */
router.get(
  '/traces/:locationId',
  requireOperatorOROperatorDevice,
  requireOperatorDeviceRoles([OperatorDevice.employee, OperatorDevice.manager]),
  validateQuerySchema(locationTracesQuerySchema),
  validateParametersSchema(locationIdParametersSchema),
  limitRequestsPerHour('locations_traces_get_ratelimit_hour', {
    skipSuccessfulRequests: true,
    global: false,
  }),
  async (request, response) => {
    const location = await Location.findOne({
      where: {
        uuid: request.params.locationId,
        operator: request.user.uuid,
      },
    });

    if (!location) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const durationEnd = {
      all: moment().subtract(28, 'days').toDate(),
      today: moment().startOf('day').toDate(),
      week: moment().subtract(7, 'days').toDate(),
    }[request.query.duration || 'all'] as Date;

    const traces = await Trace.findAll({
      where: {
        locationId: location.uuid,
        time: {
          [Op.strictRight]: ([null, durationEnd] as unknown) as Rangable,
        },
      },
      order: [['updatedAt', 'DESC']],
      include: {
        model: TraceData,
      },
    });

    return response.send(
      traces.map(trace => ({
        traceId: trace.traceId,
        deviceType: trace.deviceType,
        checkin: moment(trace.time[0].value).unix(),
        checkout: moment(trace.time?.[1]?.value).unix(),
        data: trace.TraceData
          ? {
              data: trace.TraceData.data,
              iv: trace.TraceData.iv,
              mac: trace.TraceData.mac,
              publicKey: trace.TraceData.publicKey,
            }
          : null,
      }))
    );
  }
);

export default router;
