import config from 'config';

import {
  LocationEntryPolicyTypes,
  LocationEntryPolicyInfoTypes,
  LocationVentilationTypes,
  LocationMaskTypes,
  AreaType,
  AreaSubtype,
} from 'constants/location';
import { z } from 'utils/validation';

export const createSchema = z.object({
  groupId: z.uuid(),
  locationName: z.safeString().max(255),
  firstName: z.safeString().max(255).optional().nullable(),
  lastName: z.safeString().max(255).optional().nullable(),
  phone: z.phoneNumber().optional().nullable(),
  streetName: z.safeString().max(255).optional().nullable(),
  streetNr: z.safeString().max(255).optional().nullable(),
  zipCode: z.zipCode().optional().nullable(),
  city: z.safeString().max(255).optional().nullable(),
  state: z.safeString().max(255).optional().nullable(),
  lat: z.number().optional().nullable(),
  lng: z.number().optional().nullable(),
  entryPolicy: z.nativeEnum(LocationEntryPolicyTypes).optional().nullable(),
  radius: z.number().int().nonnegative().max(5000).optional(),
  tableCount: z.number().int().positive().max(1000).optional().nullable(),
  additionalData: z
    .array(
      z.object({
        key: z.safeString().max(255),
        label: z.safeString().max(255).optional(),
        isRequired: z.boolean().optional(),
      })
    )
    .max(config.get('luca.locations.maxAdditionalData'))
    .optional(),
  isIndoor: z.boolean().optional(),
  type: z.nativeEnum(AreaType),
  subtype: z
    .string()
    .nullable()
    .refine(value => Object.values(AreaSubtype).includes(value))
    .optional(),
  averageCheckinTime: z
    .number()
    .int()
    .positive()
    .max(1440)
    .min(15)
    .optional()
    .nullable(),
  entryPolicyInfo: z
    .nativeEnum(LocationEntryPolicyInfoTypes)
    .optional()
    .nullable(),
  ventilation: z.nativeEnum(LocationVentilationTypes).optional().nullable(),
  masks: z.nativeEnum(LocationMaskTypes).optional().nullable(),
  roomHeight: z.number().optional().nullable(),
  roomWidth: z.number().optional().nullable(),
  roomDepth: z.number().optional().nullable(),
});

export const updateSchema = z.object({
  locationName: z.safeString().max(255).optional(),
  firstName: z.safeString().max(255).optional(),
  lastName: z.safeString().max(255).optional(),
  phone: z.phoneNumber().optional(),
  entryPolicy: z.nativeEnum(LocationEntryPolicyTypes).optional().nullable(),
  radius: z.number().int().nonnegative().max(5000).optional(),
  isIndoor: z.boolean().optional(),
  averageCheckinTime: z
    .number()
    .int()
    .positive()
    .max(1440)
    .min(15)
    .optional()
    .nullable(),
  isContactDataMandatory: z.boolean().optional(),
  entryPolicyInfo: z
    .nativeEnum(LocationEntryPolicyInfoTypes)
    .optional()
    .nullable(),
  ventilation: z.nativeEnum(LocationVentilationTypes).optional().nullable(),
  masks: z.nativeEnum(LocationMaskTypes).optional().nullable(),
  roomHeight: z.number().optional().nullable(),
  roomWidth: z.number().optional().nullable(),
  roomDepth: z.number().optional().nullable(),
  subtype: z
    .string()
    .nullable()
    .refine(value => Object.values(AreaSubtype).includes(value))
    .optional(),
});

export const updateAddressSchema = z.object({
  streetName: z.safeString().max(255).optional(),
  streetNr: z.safeString().max(255).optional(),
  zipCode: z.zipCode().optional(),
  city: z.safeString().max(255).optional(),
  state: z.safeString().max(255).optional().nullable(),
  lat: z.number().optional().nullable(),
  lng: z.number().optional().nullable(),
  radius: z.number().optional(),
});

export const locationIdParametersSchema = z.object({
  locationId: z.uuid(),
});

export const locationDeleteSchema = z.object({
  password: z.string().max(255),
});

export const locationTracesQuerySchema = z.object({
  duration: z.enum(['today', 'week']).optional(),
});
