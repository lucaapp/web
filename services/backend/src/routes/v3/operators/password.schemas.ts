import { z } from 'utils/validation';
import { campaignBodySchema } from '../../v4/campaigns.schemas';

export const changePasswordSchema = z.object({
  currentPassword: z.string().max(255),
  newPassword: z.zxcvbnPassword(),
});

export const forgotPasswordSchema = z.object({
  email: z.email(),

  campaign: campaignBodySchema.optional(),
});

export const resetPasswordSchema = z.object({
  resetId: z.uuid(),
  newPassword: z.zxcvbnPassword(),
  campaign: campaignBodySchema.optional(),
});

export const resetRequestSchema = z.object({
  resetId: z.uuid(),
});
