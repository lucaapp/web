import { Router } from 'express';
import { Op } from 'sequelize';
import status from 'http-status';
import config from 'config';
import moment from 'moment';

import {
  database,
  LocationGroup,
  Location,
  LocationGroupEmployee,
} from 'database';
import {
  requireOperator,
  requireNonDeletedUser,
} from 'middlewares/requireUser';
import { validateParametersSchema } from 'middlewares/validateSchema';

import { groupIdParametersSchema } from './locationGroups.schemas';

const router = Router();

// list deleted location groups
router.get('/deleted', requireOperator, async (request, response) => {
  const groups = await LocationGroup.findAll({
    where: {
      operatorId: request.user.uuid,
      deletedAt: {
        [Op.not]: null,
      },
    },
    order: [['name', 'ASC']],
    paranoid: false,
  });

  response.send(
    groups.map(group => {
      const maxAge = config.get('luca.locations.maxAge');
      const availableUntil = moment(group.deletedAt)
        .add(maxAge, 'hours')
        .unix();

      return {
        groupId: group.uuid,
        name: group.name,
        availableUntil,
      };
    })
  );
});

// recover deleted location group
router.post(
  '/:groupId/recover',
  requireOperator,
  requireNonDeletedUser,
  validateParametersSchema(groupIdParametersSchema),
  async (request, response) => {
    const locationGroup = await LocationGroup.findOne({
      where: {
        operatorId: request.user.uuid,
        uuid: request.params.groupId,
        deletedAt: {
          [Op.not]: null,
        },
      },
      paranoid: false,
    });

    if (!locationGroup) return response.sendStatus(status.NOT_FOUND);

    await database.transaction(async transaction => {
      await Location.update(
        { deletedAt: null },
        { where: { groupId: locationGroup.uuid }, paranoid: false, transaction }
      );
      await locationGroup.restore({ transaction });
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

router.get(
  '/:groupId/employees',
  requireOperator,
  requireNonDeletedUser,
  validateParametersSchema(groupIdParametersSchema),

  async (request, response) => {
    const { uuid: operatorId } = request.user as IOperator;
    const { groupId } = request.params;

    const locationGroup = await LocationGroup.findOne({
      where: {
        operatorId,
        uuid: groupId,
      },
    });

    if (!locationGroup) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const employees = await LocationGroupEmployee.findAll({
      where: {
        locationGroupId: groupId,
      },
      attributes: ['uuid', 'firstName', 'lastName', 'employeeNumber'],
    });

    return response.send(employees);
  }
);

export default router;
