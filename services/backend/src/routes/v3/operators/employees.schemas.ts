import { z } from 'utils/validation';

export const employeeIdParameterSchema = z.object({
  employeeId: z.uuid(),
});

export const updateEmployeeSchema = z.object({
  firstName: z.safeString(),
  lastName: z.safeString().nullable().optional(),
  employeeNumber: z.safeString().nullable().optional(),
});

export const createEmployeeSchema = updateEmployeeSchema.extend({
  locationGroupId: z.uuid(),
});
