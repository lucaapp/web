import { Router } from 'express';
import status from 'http-status';
import config from 'config';
import { LocationGroupEmployee, LocationGroup } from 'database';
import {
  requireNonDeletedUser,
  requireOperator,
} from 'middlewares/requireUser';
import {
  validateParametersSchema,
  validateSchema,
} from 'middlewares/validateSchema';
import {
  employeeIdParameterSchema,
  createEmployeeSchema,
  updateEmployeeSchema,
} from './employees.schemas';

const router = Router();

router.post(
  '/',
  requireOperator,
  requireNonDeletedUser,
  validateSchema(createEmployeeSchema),
  async (request, response) => {
    const { uuid: operatorId } = request.user as IOperator;
    const {
      firstName,
      lastName,
      employeeNumber,
      locationGroupId,
    } = request.body;

    const locationGroup = await LocationGroup.findOne({
      where: {
        uuid: locationGroupId,
        operatorId,
      },
    });

    if (!locationGroup) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const currentCount = await LocationGroupEmployee.count({
      where: {
        locationGroupId,
      },
    });

    if (currentCount >= config.get('luca.locationGroupEmployees.maxAmount')) {
      return response.sendStatus(status.FORBIDDEN);
    }

    await LocationGroupEmployee.create({
      locationGroupId,
      firstName,
      lastName,
      employeeNumber,
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

router.patch(
  '/:employeeId',
  requireOperator,
  requireNonDeletedUser,
  validateParametersSchema(employeeIdParameterSchema),
  validateSchema(updateEmployeeSchema),
  async (request, response) => {
    const { uuid: operatorId } = request.user as IOperator;
    const { employeeId } = request.params;
    const { firstName, lastName, employeeNumber } = request.body;

    const employee = await LocationGroupEmployee.findOne({
      where: {
        uuid: employeeId,
      },
      include: [
        {
          model: LocationGroup,
          where: {
            operatorId,
          },
        },
      ],
    });

    if (!employee) {
      return response.sendStatus(status.NOT_FOUND);
    }

    await employee.update({
      firstName,
      lastName,
      employeeNumber,
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

router.delete(
  '/:employeeId',
  requireOperator,
  requireNonDeletedUser,
  validateParametersSchema(employeeIdParameterSchema),
  async (request, response) => {
    const { uuid: operatorId } = request.user as IOperator;
    const { employeeId } = request.params;

    const employee = await LocationGroupEmployee.findOne({
      where: {
        uuid: employeeId,
      },
      include: [
        {
          model: LocationGroup,
          where: {
            operatorId,
          },
        },
      ],
    });

    if (!employee) {
      return response.sendStatus(status.NOT_FOUND);
    }

    await employee.destroy();

    return response.sendStatus(status.NO_CONTENT);
  }
);

export default router;
