/**
 * @overview Provides endpoints allowing venue operators to update their
 * respective passwords, required for operating the venue owner frontend
 *
 * @see https://www.luca-app.de/securityoverview/processes/venue_registration.html?highlight=password#process
 */

import { Router } from 'express';
import status from 'http-status';
import config from 'config';
import moment from 'moment';
import queryString from 'query-string';
import { Op } from 'sequelize';
import { validateOperatorStrongPassword } from 'middlewares/validateOperatorStrongPassword';
import { database, Operator, PasswordReset, OperatorCampaign } from 'database';
import * as mailClient from 'utils/mailClient';
import {
  validateParametersSchema,
  validateSchema,
} from 'middlewares/validateSchema';
import { requireNonBlockedIp } from 'middlewares/requireNonBlockedIp';

import {
  limitRequestsPerHour,
  limitRequestsByEmailPerDay,
} from 'middlewares/rateLimit';
import {
  requireOperator,
  requireNonDeletedUser,
} from 'middlewares/requireUser';
import escapeHTML from 'escape-html';
import { randomSleep } from 'utils/sleep';
import { logoutCurrentUser } from 'utils/authentication';

import {
  changePasswordSchema,
  forgotPasswordSchema,
  resetPasswordSchema,
  resetRequestSchema,
} from './password.schemas';

const EMAIL_EXPIRY_HOURS = config.get('emails.expiry');

const router = Router();

// change password
router.post(
  '/change',
  limitRequestsPerHour('password_change_post_ratelimit_hour', {
    skipSuccessfulRequests: true,
    global: false,
  }),
  requireOperator,
  requireNonDeletedUser,
  validateSchema(changePasswordSchema),
  validateOperatorStrongPassword('newPassword'),
  async (request, response) => {
    const operator = request.user;
    const { currentPassword, newPassword } = request.body;

    const isCurrentPasswordCorrect = await operator.checkPassword(
      currentPassword
    );

    if (!isCurrentPasswordCorrect) {
      return response.sendStatus(status.FORBIDDEN);
    }

    if (currentPassword === newPassword)
      return response.sendStatus(status.CONFLICT);

    await operator.update({
      password: newPassword,
    });

    mailClient.sendOperatorUpdatePasswordNotification(
      operator.email,
      `${operator.fullName}`,
      operator.transactionalLanguage,
      {
        email: operator.email,
      }
    );

    await logoutCurrentUser(request, response);

    return response.sendStatus(status.NO_CONTENT);
  }
);

// password forgot
router.post(
  '/forgot',
  requireNonBlockedIp,
  limitRequestsPerHour('password_forgot_post_ratelimit_hour'),
  validateSchema(forgotPasswordSchema),
  limitRequestsByEmailPerDay('password_forgot_post_ratelimit_email'),
  async (request, response) => {
    const operator = await Operator.findOne({
      where: {
        email: request.body.email,
      },
    });

    if (operator) {
      await PasswordReset.update(
        { closed: true },
        {
          where: {
            operatorId: operator.uuid,
            createdAt: {
              [Op.gt]: moment().subtract(EMAIL_EXPIRY_HOURS, 'hours').toDate(),
            },
          },
        }
      );

      const forgotPasswordRequest = await PasswordReset.create({
        operatorId: operator.uuid,
        email: operator.email,
      });

      mailClient.sendForgotPasswordMail(
        forgotPasswordRequest.email,
        operator.fullName,
        operator.transactionalLanguage,
        {
          firstName: escapeHTML(operator.firstName),
          forgotPasswordLink: `https://${config.get(
            'hostname'
          )}/resetPassword/${forgotPasswordRequest.uuid}${
            request.body.campaign
              ? `?${queryString.stringify(request.body.campaign)}`
              : ''
          }`,
        }
      );
    }
    await randomSleep();
    return response.sendStatus(status.NO_CONTENT);
  }
);

// reset password
router.post(
  '/reset',
  requireNonBlockedIp,
  limitRequestsPerHour('password_reset_post_ratelimit_hour', {
    skipSuccessfulRequests: true,
    global: false,
  }),
  validateSchema(resetPasswordSchema),
  validateOperatorStrongPassword('newPassword'),
  async (request, response) => {
    const { resetId, newPassword, campaign } = request.body;

    const resetRequest = await PasswordReset.findOne({
      where: {
        uuid: resetId,
        closed: false,
        createdAt: {
          [Op.gt]: moment().subtract(EMAIL_EXPIRY_HOURS, 'hours').toDate(),
        },
      },
    });
    if (!resetRequest) return response.sendStatus(status.NOT_FOUND);

    const operator = await Operator.findByPk(resetRequest.operatorId);
    if (!operator) return response.sendStatus(status.NOT_FOUND);

    if (await operator.checkPassword(newPassword))
      return response.sendStatus(status.CONFLICT);

    await database.transaction(async transaction => {
      return Promise.all([
        operator.update(
          {
            password: newPassword,
          },
          { transaction }
        ),
        PasswordReset.update(
          { closed: true },
          {
            where: {
              operatorId: operator.uuid,
              createdAt: {
                [Op.gt]: moment()
                  .subtract(EMAIL_EXPIRY_HOURS, 'hours')
                  .toDate(),
              },
            },
            transaction,
          }
        ),
        ...(campaign
          ? [
              OperatorCampaign.create(
                {
                  utm_id: campaign.utm_id,
                  utm_source: campaign.utm_source,
                  utm_medium: campaign.utm_medium,
                  utm_campaign: campaign.utm_campaign,
                  operatorId: operator.uuid,
                },
                { transaction }
              ),
            ]
          : []),
      ]);
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

// get password reset
router.get(
  '/reset/:resetId',
  requireNonBlockedIp,
  limitRequestsPerHour('password_reset_get_ratelimit_hour', {
    skipSuccessfulRequests: true,
    global: false,
  }),
  validateParametersSchema(resetRequestSchema),
  async (request, response) => {
    const { resetId } = request.params;

    const resetRequest = await PasswordReset.findOne({
      where: {
        uuid: resetId,
        closed: false,
        createdAt: {
          [Op.gt]: moment().subtract(EMAIL_EXPIRY_HOURS, 'hours').toDate(),
        },
      },
    });

    if (!resetRequest) {
      return response.sendStatus(status.NOT_FOUND);
    }

    return response.send({
      uuid: resetRequest.uuid,
      operatorId: resetRequest.operatorId,
      email: resetRequest.email,
      closed: resetRequest.closed,
    });
  }
);

export default router;
