/* eslint-disable max-lines */

import crypto from 'crypto';
import { Router } from 'express';
import status from 'http-status';
import moment from 'moment';
import config from 'config';
import queryString from 'query-string';
import { validateOperatorStrongPassword } from 'middlewares/validateOperatorStrongPassword';
import {
  database,
  Operator,
  OperatorCampaign,
  EmailActivation,
  Location,
  OperatorKeyHistory,
  LocationGroup,
  OperatorDevice as OperatorDeviceModel,
  PotentialOperator,
} from 'database';
import * as mailClient from 'utils/mailClient';
import { generateSupportCode } from 'utils/generators';
import { getPreferredLanguageFromRequest } from 'utils/language';
import { randomSleep } from 'utils/sleep';
import { formatPhoneNumber } from 'utils/format';
import { checkCampaignLimit } from 'utils/campaignLimit';
import { validateSchema } from 'middlewares/validateSchema';

import {
  requireOperator,
  requireNonDeletedUser,
  requireOperatorDeviceRoles,
  requireOperatorOROperatorDevice,
} from 'middlewares/requireUser';
import {
  limitRequestsPerDay,
  limitRequestsByUserPerDay,
} from 'middlewares/rateLimit';

import { OperatorDevice } from 'constants/operatorDevice';

import { requireNonBlockedIp } from 'middlewares/requireNonBlockedIp';
import locationsRouter from './operators/locations';
import passwordRouter from './operators/password';
import emailsRouter from './operators/email';
import tracesRouter from './operators/traces';
import locationGroupsRouter from './operators/locationGroups';
import employeesRouter from './operators/employees';

import {
  createSchema,
  activationSchema,
  storePublicKeySchema,
  updateOperatorSchema,
  supportSchema,
  publicKeyResetSchema,
} from './operators.schemas';

const router = Router();

const POTENTIAL_UTM = 'potential';

// create operator
router.post(
  '/',
  limitRequestsPerDay('operators_post_ratelimit_day'),
  requireNonBlockedIp,
  validateSchema(createSchema),
  validateOperatorStrongPassword(),
  async (request, response) => {
    const existingOperator = await Operator.findOne({
      where: {
        username: request.body.email,
      },
      paranoid: false,
    });

    const potentialOperator = await PotentialOperator.findByPk(
      request.body.email
    );

    if (existingOperator) {
      mailClient.sendRegisterAttemptNotification(
        existingOperator.email,
        existingOperator.fullName,
        existingOperator.transactionalLanguage,
        {
          // Preserve campaign parameters to reset link
          link: `https://${config.get('hostname')}/forgotPassword${
            request.body.campaign
              ? `?${queryString.stringify(request.body.campaign)}`
              : ''
          }`,
        }
      );
      await randomSleep();
      return response.sendStatus(status.NO_CONTENT);
    }

    const [operator, activationMail] = await database.transaction(
      async transaction => {
        const createdOperator = await Operator.create(
          {
            firstName: request.body.firstName,
            lastName: request.body.lastName,
            businessEntityName: request.body.businessEntityName,
            businessEntityStreetName: request.body.businessEntityStreetName,
            businessEntityStreetNumber: request.body.businessEntityStreetNumber,
            businessEntityZipCode: request.body.businessEntityZipCode,
            businessEntityCity: request.body.businessEntityCity,
            email: request.body.email,
            username: request.body.email,
            password: request.body.password,
            phone: formatPhoneNumber(request.body.phone) ?? undefined,
            salt: crypto.randomBytes(16).toString('base64'),
            privateKeySecret: crypto.randomBytes(32).toString('base64'),
            supportCode: generateSupportCode(),
            avvAccepted: request.body.avvAccepted,
            lastVersionSeen: request.body.lastVersionSeen,
            language: getPreferredLanguageFromRequest(request),
          },
          { transaction }
        );

        if (potentialOperator) {
          await checkCampaignLimit(createdOperator.uuid);
          await OperatorCampaign.create(
            {
              utm_id: potentialOperator.utm_id,
              utm_source: potentialOperator.utm_source || POTENTIAL_UTM,
              utm_medium: potentialOperator.utm_medium || POTENTIAL_UTM,
              utm_campaign: potentialOperator.utm_campaign,
              operatorId: createdOperator.uuid,
            },
            { transaction }
          );

          await potentialOperator.destroy({ transaction });
        }

        if (request.body.campaign) {
          await checkCampaignLimit(createdOperator.uuid);
          await OperatorCampaign.create(
            {
              utm_id: request.body.campaign.utm_id,
              utm_source: request.body.campaign.utm_source,
              utm_medium: request.body.campaign.utm_medium,
              utm_campaign: request.body.campaign.utm_campaign,
              operatorId: createdOperator.uuid,
            },
            { transaction }
          );
        }

        if (request.body.initialLocationGroup) {
          const group = await LocationGroup.create(
            {
              name: request.body.initialLocationGroup.name,
              operatorId: createdOperator.uuid,
              type: request.body.initialLocationGroup.type,
            },
            { transaction }
          );

          const createdLocation = await Location.create(
            {
              operator: createdOperator.uuid,
              groupId: group.uuid,
              firstName: request.body.firstName,
              lastName: request.body.lastName,
            },
            { transaction }
          );

          await createdLocation.initLocationTables(
            request.body.initialLocationGroup.tableCount,
            transaction
          );
        }

        const createdActivationMail = await EmailActivation.create(
          {
            operatorId: createdOperator.uuid,
            email: createdOperator.email,
            type: 'Registration',
          },
          { transaction }
        );
        return [createdOperator, createdActivationMail];
      }
    );

    mailClient.sendActivationMail(
      activationMail.email as string,
      operator.fullName,
      operator.transactionalLanguage,
      {
        firstName: request.body.firstName,
        activationLink: `https://${config.get('hostname')}/activation/${
          activationMail.uuid
        }`,
      }
    );

    await randomSleep();
    return response.sendStatus(status.NO_CONTENT);
  }
);

// active operator account
router.post(
  '/activate',
  validateSchema(activationSchema),
  async (request, response) => {
    const { activationId } = request.body;

    const activationMail = await EmailActivation.findOne({
      where: {
        uuid: activationId,
        type: 'Registration',
      },
    });

    if (!activationMail || !activationMail.email) {
      return response.sendStatus(status.NOT_FOUND);
    }

    if (activationMail.closed) {
      return response.sendStatus(status.CONFLICT);
    }

    if (
      activationMail.createdAt <
      moment().subtract(config.get('emails.expiry'), 'hours').toDate()
    ) {
      return response.sendStatus(status.GONE);
    }

    const operator = await Operator.findByPk(activationMail.operatorId);

    if (!operator) {
      return response.sendStatus(status.NOT_FOUND);
    }

    await database.transaction(async transaction => {
      return Promise.all([
        activationMail.update(
          {
            closed: true,
          },
          { transaction }
        ),
        operator.update(
          {
            activated: true,
          },
          { transaction }
        ),
      ]);
    });

    mailClient.sendRegistrationConfirmation(
      activationMail.email,
      operator.fullName,
      operator.transactionalLanguage,
      {
        firstName: operator.firstName,
      }
    );

    return response.sendStatus(status.CREATED);
  }
);

// set operator public key
router.post(
  '/publicKey',
  requireOperator,
  requireNonDeletedUser,
  validateSchema(storePublicKeySchema),
  async (request, response) => {
    if (request.user.publicKey) {
      return response.sendStatus(status.FORBIDDEN);
    }

    await database.transaction(async transaction => {
      await request.user.update(
        {
          publicKey: request.body.publicKey,
        },
        { transaction }
      );

      await Location.update(
        {
          publicKey: request.body.publicKey,
        },
        {
          where: {
            operator: request.user.uuid,
          },
          transaction,
        }
      );
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

// update operator
router.patch(
  '/',
  requireOperator,
  requireNonDeletedUser,
  validateSchema(updateOperatorSchema),
  async (request, response) => {
    const operator = request.user;
    await operator.update({
      firstName: request.body.firstName,
      lastName: request.body.lastName,
      phone: formatPhoneNumber(request.body.phone),
      businessEntityName: request.body.businessEntityName,
      businessEntityStreetName: request.body.businessEntityStreetName,
      businessEntityStreetNumber: request.body.businessEntityStreetNumber,
      businessEntityZipCode: request.body.businessEntityZipCode,
      businessEntityCity: request.body.businessEntityCity,
      avvAccepted: request.body.avvAccepted,
      lastVersionSeen: request.body.lastVersionSeen,
      languageOverwrite: request.body.languageOverwrite,
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

// get private key secret
router.get(
  '/privateKeySecret',
  requireOperatorOROperatorDevice,
  requireOperatorDeviceRoles([OperatorDevice.employee, OperatorDevice.manager]),
  (request, response) => {
    return response.send({
      privateKeySecret: request.user.privateKeySecret,
    });
  }
);

// get updated time for last key pair
router.get(
  '/lastKeyUpdate',
  requireOperatorOROperatorDevice,
  requireOperatorDeviceRoles([OperatorDevice.employee, OperatorDevice.manager]),
  async (request, response) => {
    const latestKeyHistory = await OperatorKeyHistory.findOne({
      where: { operatorId: request.user.uuid },
      order: [['createdAt', 'DESC']],
    });

    return response.send({
      lastUpdated: latestKeyHistory?.createdAt,
    });
  }
);

router.post(
  '/confirmKeyImported',
  requireOperator,
  async (request, response) => {
    const operator = request.user;

    await operator.update({
      lastSeenPrivateKey: moment(),
    });

    return response.send(status.NO_CONTENT);
  }
);

// request account deactivation
router.delete('/', requireOperator, async (request, response) => {
  const operator = request.user;

  const locations = await operator.getLocations();

  await database.transaction(async transaction => {
    for (const location of locations) {
      await location.checkoutAllTraces(transaction);
    }
    await operator.destroy({ transaction });
  });

  const deletionScheduledAfter = moment()
    .add(config.get('luca.operators.deleted.maxAgeHours'), 'hours')
    .unix();

  response.send({
    deletionScheduledAfter,
  });
});

// support
router.post(
  '/support',
  requireOperator,
  limitRequestsPerDay('operators_support_email_post_ratelimit_day'),
  validateSchema(supportSchema),
  async (request, response) => {
    const { phone, requestText } = request.body;

    const requestTime = moment().format('DD.MM.YYYY HH:mm');
    mailClient.sendLocationsSupportMail(
      'locations@luca-app.de',
      'Locations Support Mail',
      null,
      {
        userPhone: formatPhoneNumber(phone) ?? '',
        userEmail: request.user.email,
        userName: request.user.fullName,
        requestText,
        requestTime,
        supportCode: request.user.supportCode,
      }
    );

    return response.sendStatus(status.NO_CONTENT);
  }
);

// undo account deactivation
router.post('/restore', requireOperator, async (request, response) => {
  const operator = request.user;
  await operator.restore();
  response.sendStatus(status.NO_CONTENT);
});

router.patch(
  '/publicKey',
  requireOperator,
  limitRequestsByUserPerDay('operators_reset_public_key_user_ratelimit_day'),
  validateSchema(publicKeyResetSchema),
  async (request, response) => {
    const { publicKey, privateKeySecret, password } = request.body;
    const operator = request.user;

    const isPasswordCorrect = await operator.checkPassword(password);

    if (!isPasswordCorrect) {
      return response.sendStatus(status.FORBIDDEN);
    }

    await database.transaction(async transaction => {
      if (operator.publicKey) {
        await OperatorKeyHistory.create(
          {
            operatorId: operator.uuid,
            publicKey: operator.publicKey,
            privateKeySecret: operator.privateKeySecret,
          },
          { transaction }
        );
      }

      await operator.update(
        {
          publicKey,
          privateKeySecret,
        },
        { transaction }
      );

      await Location.update(
        {
          publicKey,
        },
        {
          where: {
            operator: operator.uuid,
          },
          transaction,
        }
      );
      await OperatorDeviceModel.destroy({
        where: {
          operatorId: operator.uuid,
        },
        transaction,
      });
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

router.use('/locations', locationsRouter);
router.use('/password', passwordRouter);
router.use('/email', emailsRouter);
router.use('/traces', tracesRouter);
router.use('/locationGroups', locationGroupsRouter);
router.use('/employees', employeesRouter);

export default router;
