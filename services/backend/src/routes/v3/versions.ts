import { Router } from 'express';
import { get as featureFlagGet } from 'utils/featureFlag';

const router = Router();

router.get('/apps/android', async (request, response) => {
  return response.send({
    minimumVersion: await featureFlagGet('android_minimum_version'),
  });
});

router.get('/apps/ios', async (request, response) => {
  return response.send({
    minimumVersion: await featureFlagGet('ios_minimum_version'),
  });
});

router.get('/apps/lst', async (request, response) => {
  return response.send({
    minimumVersion: await featureFlagGet('lst_minimum_version'),
  });
});

router.get('/apps/operator/android', async (request, response) => {
  return response.send({
    minimumVersion: await featureFlagGet('operator_android_minimum_semver'),
  });
});

router.get('/apps/operator/ios', async (request, response) => {
  return response.send({
    minimumVersion: await featureFlagGet('operator_ios_minimum_semver'),
  });
});

export default router;
