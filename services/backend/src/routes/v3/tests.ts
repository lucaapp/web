import { Router } from 'express';
import * as status from 'http-status';

import { TestRedeem } from 'database';
import { validateSchema } from 'middlewares/validateSchema';
import { limitRequestsPerMinute } from 'middlewares/rateLimit';

import { redeemSchema, redeemDeleteSchema } from './tests.schemas';

const router = Router();

// Redeem a test
router.post(
  '/redeem',
  limitRequestsPerMinute('tests_redeem_post_ratelimit_minute'),
  validateSchema(redeemSchema),
  async (request, response) => {
    const testRedeem = await TestRedeem.findByPk(request.body.hash);

    if (!testRedeem) {
      await TestRedeem.create({
        hash: request.body.hash,
        tag: request.body.tag,
      });
      return response.sendStatus(status.NO_CONTENT);
    }

    if (testRedeem.tag === request.body.tag) {
      return response.sendStatus(status.NO_CONTENT);
    }

    return response.sendStatus(status.CONFLICT);
  }
);

router.delete(
  '/redeem',
  limitRequestsPerMinute('tests_redeem_delete_ratelimit_minute'),
  validateSchema(redeemDeleteSchema),
  async (request, response) => {
    const {
      body: { hash, tag },
    } = request;
    const testRedeem = await TestRedeem.findOne({
      where: { hash, tag },
    });

    if (!testRedeem) {
      return response.sendStatus(status.NOT_FOUND);
    }

    await testRedeem.destroy();
    return response.sendStatus(status.NO_CONTENT);
  }
);

export default router;
