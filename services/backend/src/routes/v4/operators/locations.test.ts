import { UserType } from 'constants/user';
import { Location, LocationGroup, Operator } from 'database';
import express, { NextFunction, Request, Response } from 'express';
import 'express-async-errors';
import superTestRequest from 'supertest';
import { setupDBRedisAndApp } from 'testing/utils';
import logger from 'utils/logger';
import { v4 as uuid } from 'uuid';
import { generateSupportCode } from 'utils/generators';
import { LocationGroupInstance } from 'database/models/locationGroup';
import { OperatorInstance } from 'database/models/operator';
import { LocationInstance } from 'database/models/location';
import { handle404, handle500 } from '../../../middlewares/error';
import locationsRouter from './locations';

let mockUserId = '';
jest.mock('../../../middlewares/requireUser.js', () => {
  return {
    ...jest.requireActual('../../../middlewares/requireUser.js'),
    requireOperator: (
      request: Request,
      response: Response,
      next: NextFunction
    ) => next(),
  };
});

const getRequest = (): superTestRequest.SuperTest<superTestRequest.Test> =>
  superTestRequest(
    express()
      .use((request, response, next) => {
        request.user = {
          type: UserType.OPERATOR,
          uuid: mockUserId,
        };
        request.log = logger;
        next();
      })
      .use(locationsRouter)
      .use(handle404)
      .use(handle500)
  );

let location: LocationInstance;
let locationGroup: LocationGroupInstance;
let operator: OperatorInstance;

describe('PATCH /operators/locations/:id/openingHours', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
  });

  beforeAll(async () => {
    operator = await Operator.create({
      firstName: 'First',
      lastName: 'Last',
      businessEntityName: 'A Business',
      businessEntityStreetName: 'Street Street',
      businessEntityStreetNumber: '123',
      businessEntityZipCode: '10117',
      businessEntityCity: 'Berlin',
      email: 'abusiness@business.business',
      username: 'abusiness@business.business',
      password: 'testing',
      phone: '+1621234567',
      salt: Buffer.alloc(16).toString('base64'),
      privateKeySecret: Buffer.alloc(32).toString('base64'),
      supportCode: generateSupportCode(),
    });
    locationGroup = await LocationGroup.create({
      operatorId: operator.uuid,
    });
    location = await Location.create({
      // @ts-ignore this is not part of normal creation attributes
      uuid: uuid(),
      radius: 10,
      scannerId: uuid(),
      scannerAccessId: uuid(),
      publicKey: 'fake public key',
      isPrivate: false,
      accessId: uuid(),
      formId: uuid(),
      isIndoor: true,
      isContactDataMandatory: false,
      operator: operator.uuid,
      groupId: locationGroup.uuid,
    });

    mockUserId = operator.uuid;
  });

  describe('when passing unknown location ID', () => {
    it('responds 404', async () => {
      const response = await getRequest()
        .patch(`/unknown-location-id/openingHours`)
        .send({
          openingHours: {
            mon: null,
            tue: null,
            wed: null,
            thu: null,
            fri: null,
            sat: null,
            sun: null,
          },
        });

      expect(response.status).toBe(404);
    });
  });

  describe('when passing changed opening hours', () => {
    it('saves new opening hours to model in DB', async () => {
      const response = await getRequest()
        .patch(`/${location.uuid}/openingHours`)
        .send({
          openingHours: {
            mon: [{ openingHours: ['12:34', '12:45'], included: false }],
            tue: null,
            wed: null,
            thu: null,
            fri: null,
            sat: null,
            sun: null,
          },
        });

      expect(response.status).toBe(204);

      const foundLocation = await Location.findOne({
        where: { uuid: location.uuid },
      });
      expect(foundLocation).toMatchObject({
        openingHours: {
          mon: [{ openingHours: ['12:34', '12:45'], included: false }],
          tue: null,
          wed: null,
          thu: null,
          fri: null,
          sat: null,
          sun: null,
        },
      });
    });
  });
});
