import { Router } from 'express';
import status from 'http-status';
import { requireOperator } from 'middlewares/requireUser';
import { z } from 'zod';
import { Location, LocationGroup } from 'database';
import { ApiError, ApiErrorType } from 'utils/apiError';
import { OpeningHours } from 'utils/openingHours';
import { validateSchema } from 'middlewares/validateSchema';
import {
  locationIdParametersSchema,
  updateOpeningHoursSchema,
} from './locations.schemas';

const router = Router();

router.patch<
  z.infer<typeof locationIdParametersSchema>,
  unknown,
  z.infer<typeof updateOpeningHoursSchema>
>(
  '/:locationId/openingHours',
  requireOperator,
  validateSchema(updateOpeningHoursSchema),
  async (request, response) => {
    const { uuid: operator } = request.user as IOperator;
    const { locationId } = request.params;

    const location = await Location.findOne({
      where: {
        uuid: locationId,
        operator,
      },
      include: [
        {
          model: LocationGroup,
          attributes: ['individualOpeningHours'],
          required: true,
        },
      ],
    });

    if (!location || !location.LocationGroup) {
      throw new ApiError(ApiErrorType.LOCATION_NOT_FOUND);
    }

    await location.update({
      openingHours: request.body.openingHours as OpeningHours,
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

export default router;
