import { openingHoursSchema } from 'utils/openingHours';
import { z } from 'utils/validation';

export const locationIdParametersSchema = z.object({
  locationId: z.uuid(),
});
export const updateOpeningHoursSchema = z.object({
  openingHours: openingHoursSchema,
});
