import { z } from 'utils/validation';

export const operatorIdSchema = z.object({
  operatorId: z.uuid(),
});

export const locationGroupIdSchema = z.object({
  locationGroupId: z.uuid(),
});

export const locationIdSchema = z.object({
  locationId: z.uuid(),
});
