/* eslint-disable unicorn/consistent-function-scoping */
/* eslint-disable @typescript-eslint/no-explicit-any */
import express from 'express';
import 'express-async-errors';
import { randomUUID } from 'crypto';
import { OperatorInstance } from 'database/models/operator';
import superTestRequest from 'supertest';
import moment from 'moment';
import { setupDBRedisAndApp } from 'testing/utils';
import { setupOperator } from 'testing/testData';
import { handle404, handle500 } from 'middlewares/error';
import authRouter from './auth';
import logger from '../../utils/logger';

const AUTH_URL = '/auth';
const operatorId = randomUUID();
const password = 'foo12145AD$';

const getRequest = (): superTestRequest.SuperTest<superTestRequest.Test> =>
  superTestRequest(
    express()
      .use((request, response, next) => {
        request.log = logger;
        next();
      })
      .use(AUTH_URL, authRouter)
      .use(handle404)
      .use(handle500)
  );

jest.mock('../../middlewares/rateLimit', () => {
  const originalModule = jest.requireActual('../../middlewares/rateLimit');

  return {
    ...originalModule,
    limitRequestsPerMinute: (_key: any) => (_: any, __: any, next: () => any) =>
      next(),
  };
});

jest.mock('../../middlewares/restrictOrigin', () => {
  const originalModule = jest.requireActual('../../middlewares/restrictOrigin');

  return {
    ...originalModule,
    restrictOrigin: (_request: any, _response: any, next: () => any) => next(),
  };
});

let operator: OperatorInstance;

describe('Auth router', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
    operator = await setupOperator(operatorId);
  });

  afterEach(async () => {
    await operator.update({ activated: false, bannedAt: null });
  });

  describe('/operator/login', () => {
    it('succesful login', async () => {
      await operator.update({ activated: true });
      const response = await getRequest()
        .post(`${AUTH_URL}/operator/login`)
        .send({ username: operator.username, password });
      expect(response.status).toEqual(204);
    });

    it('succesful login for soft-deleted operator', async () => {
      await operator.update({ activated: true });
      await operator.destroy();
      const response = await getRequest()
        .post(`${AUTH_URL}/operator/login`)
        .send({ username: operator.username, password });
      expect(response.status).toEqual(204);
    });

    it('user does not exist -> 401', async () => {
      const response = await getRequest()
        .post(`${AUTH_URL}/operator/login`)
        .send({ username: 'fakeUser@nexenio.com', password: 'foo' });
      expect(response.status).toEqual(401);
    });

    it('wrong password -> 401', async () => {
      const response = await getRequest()
        .post(`${AUTH_URL}/operator/login`)
        .send({ username: operator.username, password: 'foo' });
      expect(response.status).toEqual(401);
    });

    it('user is not activated -> 403', async () => {
      const response = await getRequest()
        .post(`${AUTH_URL}/operator/login`)
        .send({ username: operator.username, password });
      expect(response.status).toEqual(403);
    });

    it('user is banned -> 403', async () => {
      await operator.update({ activated: true, bannedAt: moment().toDate() });
      const response = await getRequest()
        .post(`${AUTH_URL}/operator/login`)
        .send({ username: operator.username, password });
      expect(response.status).toEqual(403);
    });

    it('user is banned in the future -> 204', async () => {
      await operator.update({
        activated: true,
        bannedAt: moment().add(1, 'day').toDate(),
      });
      const response = await getRequest()
        .post(`${AUTH_URL}/operator/login`)
        .send({ username: operator.username, password });
      expect(response.status).toEqual(204);
    });
  });
});
