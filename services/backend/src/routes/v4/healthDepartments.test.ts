/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-shadow */
import express from 'express';
import request from 'supertest';
import { getCommonName } from '@lucaapp/crypto';
import { handle500 } from '../../middlewares/error';

import 'express-async-errors';
import healthDepartmentRouter from './healthDepartments';
import { httpLogger } from '../../utils/logger';
import {
  requireHealthDepartmentEmployee,
  requireHealthDepartmentAdmin,
} from '../../middlewares/requireUser';
import { verifySignedPublicKeys } from '../../utils/signedKeys';
import { HealthDepartment } from '../../database';

const POST_URL = '/healthDepartments/signedKeys';

const voidMiddleware = async (
  request: { user: { HealthDepartment: { commonName: string } } },
  _response: any,
  next: () => void
) => {
  request.user = {
    HealthDepartment: {
      commonName: 'test',
    },
  };
  next();
};

const fakeParameters = {
  publicCertificate: 'a'.repeat(8192),
  signedPublicHDEKP: 'a'.repeat(2048),
  signedPublicHDSKP: 'a'.repeat(2048),
};

jest.mock('../../middlewares/requireUser', () => {
  const originalModule = jest.requireActual('../../middlewares/requireUser');

  return {
    ...originalModule,
    requireHealthDepartmentEmployee: jest.fn(),
    requireHealthDepartmentAdmin: jest.fn(),
  };
});
const mockHealthDepartmentEmployee = requireHealthDepartmentEmployee as jest.MockedFunction<
  typeof requireHealthDepartmentEmployee
>;
const mockHealthDepartmentAdmin = requireHealthDepartmentAdmin as jest.MockedFunction<
  typeof requireHealthDepartmentAdmin
>;

jest.mock('../../utils/signedKeys');
const mockVerifyKeys = verifySignedPublicKeys as jest.MockedFunction<
  typeof verifySignedPublicKeys
>;

jest.mock('../../database');
const mockFindByPk = HealthDepartment.findByPk as jest.MockedFunction<
  typeof HealthDepartment.findByPk
>;

jest.mock('@lucaapp/crypto');
const mockCryptoCommonName = getCommonName as jest.MockedFunction<
  typeof getCommonName
>;

const initializeApp = () => {
  const app = express();
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(httpLogger);
  app.use(handle500);
  app.use('/healthDepartments', healthDepartmentRouter);
  return app;
};

describe('HealthDepartment router', () => {
  beforeEach(() => {
    mockHealthDepartmentAdmin.mockImplementation(voidMiddleware);
    mockHealthDepartmentEmployee.mockImplementation(voidMiddleware);
    mockVerifyKeys.mockReturnValue();
    mockCryptoCommonName.mockReturnValue('test');
    mockFindByPk.mockResolvedValue(new HealthDepartment());
  });

  describe('POST route', () => {
    it('should reject keys if keys already exist', async () => {
      mockHealthDepartmentAdmin.mockImplementation(
        async (
          request: {
            user: {
              HealthDepartment: {
                commonName: string;
                signedPublicHDEKP: string;
              };
            };
          },
          _response: any,
          next: () => void
        ) => {
          request.user = {
            HealthDepartment: {
              commonName: 'test',
              signedPublicHDEKP: 'a'.repeat(2048),
            },
          };
          next();
        }
      );

      const app = initializeApp();
      await request(app).post(POST_URL).send(fakeParameters).expect(409);
    });

    it('should reject invalid signatures', async () => {
      mockVerifyKeys.mockImplementation(() => {
        throw new Error('invalid signatures');
      });

      const app = initializeApp();
      await request(app).post(POST_URL).send(fakeParameters).expect(400);
    });

    it('should accept valid keys', async () => {
      mockHealthDepartmentAdmin.mockImplementation(
        async (
          request: {
            user: {
              HealthDepartment: {
                commonName: string;
                update: () => Promise<void>;
              };
            };
          },
          _response: any,
          next: () => Promise<void>
        ) => {
          request.user = {
            HealthDepartment: {
              commonName: 'test',
              update: () => Promise.resolve(),
            },
          };
          next();
        }
      );

      const app = initializeApp();
      await request(app).post(POST_URL).send(fakeParameters).expect(204);
    });
  });
});
