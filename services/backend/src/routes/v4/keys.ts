import { Router } from 'express';
import issuersRouter from './keys/issuers';
import dailyRouter from './keys/daily';

const router = Router();

router.use('/issuers', issuersRouter);
router.use('/daily', dailyRouter);

export default router;
