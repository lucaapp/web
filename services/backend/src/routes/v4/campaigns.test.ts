/* eslint-disable unicorn/consistent-function-scoping */
/* eslint-disable @typescript-eslint/no-explicit-any */
import express from 'express';
import 'express-async-errors';
import { randomUUID } from 'crypto';
import { OperatorInstance } from 'database/models/operator';
import moment from 'moment';
import superTestRequest from 'supertest';
import { setupDBRedisAndApp } from '../../testing/utils';
import { OperatorCampaignInstance } from '../../database/models/operatorCampaign';
import { setupOperator } from '../../testing/testData';
import { Operator, OperatorCampaign } from '../../database';
import { handle404, handle500 } from '../../middlewares/error';
import campaignRouter from './campaigns';
import logger from '../../utils/logger';
import { UserType } from '../../constants/user';

const CAMPAIGNS_URL = '/campaigns';
const operatorId = randomUUID();

const getRequest = (): superTestRequest.SuperTest<superTestRequest.Test> =>
  superTestRequest(
    express()
      .use((request, response, next) => {
        request.user = {
          type: UserType.OPERATOR,
          uuid: operatorId,
        };
        request.log = logger;
        next();
      })
      .use(CAMPAIGNS_URL, campaignRouter)
      .use(handle404)
      .use(handle500)
  );

jest.mock('../../middlewares/rateLimit', () => {
  const originalModule = jest.requireActual('../../middlewares/rateLimit');

  return {
    ...originalModule,
    limitRequestsPerMinute: (_key: any) => (_: any, __: any, next: () => any) =>
      next(),
  };
});

const campaignContent = {
  utm_id: 'abc123',
  utm_source: 'google',
  utm_medium: 'cpc',
  utm_campaign: 'springsale',
};

const createCampaignInDatabase = async (
  utmId: string
): Promise<OperatorCampaignInstance> => {
  return OperatorCampaign.create({
    utm_id: utmId,
    utm_source: campaignContent.utm_source,
    utm_medium: campaignContent.utm_medium,
    utm_campaign: campaignContent.utm_campaign,
    operatorId,
  });
};

let operator: OperatorInstance;

describe('Campaigns router', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
    operator = await setupOperator(operatorId);
  });

  afterEach(async () => {
    await OperatorCampaign.truncate();
    await operator.update({ bannedAt: null });
  });
  afterAll(async () => {
    await Operator.truncate();
  });

  describe('POST campaigns', () => {
    it('should accept valid campaigns object', async () => {
      const response = await getRequest()
        .post(CAMPAIGNS_URL)
        .send(campaignContent)
        .expect(201);
      expect(response.body.utm_id).toEqual(campaignContent.utm_id);
      expect(response.body.utm_source).toEqual(campaignContent.utm_source);
      expect(response.body.utm_medium).toEqual(campaignContent.utm_medium);
      expect(response.body.utm_campaign).toEqual(campaignContent.utm_campaign);
      expect(response.body.operatorId).toEqual(operatorId);
    });

    // eslint-disable-next-line jest/expect-expect
    it('should fail for banned operator', async () => {
      await operator.update({ bannedAt: moment().toDate() });
      await getRequest().post(CAMPAIGNS_URL).send(campaignContent).expect(404);
    });

    // eslint-disable-next-line jest/expect-expect
    it('should fail with invalid schema', async () => {
      await getRequest().post(CAMPAIGNS_URL).send({}).expect(400);
    });
  });

  describe('GET campaigns', () => {
    it('should return all campaigns', async () => {
      const numberOfCampaigns = 5;
      for (let count = 0; count < numberOfCampaigns; count++) {
        await createCampaignInDatabase(randomUUID());
      }

      const response = await getRequest().get(CAMPAIGNS_URL).expect(200);
      expect(response.body).toHaveLength(numberOfCampaigns);
    });

    it('should return nothing when database is empty', async () => {
      const response = await getRequest().get(CAMPAIGNS_URL).expect(200);
      expect(response.body).toHaveLength(0);
    });

    // eslint-disable-next-line jest/expect-expect
    it('should return nothing when operator is banned', async () => {
      await operator.update({ bannedAt: moment().toDate() });
      await getRequest().get(CAMPAIGNS_URL).expect(404);
    });
  });

  describe('GET campaigns/{campaignId}', () => {
    it('should return campaign when created previously', async () => {
      const campaign = await createCampaignInDatabase(campaignContent.utm_id);
      const response = await getRequest()
        .get(`${CAMPAIGNS_URL}/${campaign.uuid}`)
        .expect(200);
      expect(response.body.utm_campaign).toEqual(campaignContent.utm_campaign);
      expect(response.body.utm_id).toEqual(campaignContent.utm_id);
      expect(response.body.utm_source).toEqual(campaignContent.utm_source);
      expect(response.body.utm_medium).toEqual(campaignContent.utm_medium);
    });

    // eslint-disable-next-line jest/expect-expect
    it('should return nothing when database is empty', async () => {
      await getRequest().get(`${CAMPAIGNS_URL}/${randomUUID()}`).expect(404);
    });

    // eslint-disable-next-line jest/expect-expect
    it('should return nothing when operator is banned', async () => {
      const campaign = await createCampaignInDatabase(campaignContent.utm_id);
      await operator.update({ bannedAt: moment().toDate() });
      await getRequest().get(`${CAMPAIGNS_URL}/${campaign.uuid}`).expect(404);
    });
  });
});
