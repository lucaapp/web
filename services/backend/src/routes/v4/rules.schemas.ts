import { z } from 'utils/validation';

export const getRuleParameterSchema = z.object({
  hash: z.sha256(),
});
