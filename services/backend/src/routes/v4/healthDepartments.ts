import { Router } from 'express';
import moment from 'moment';
import status from 'http-status';
import { Op } from 'sequelize';
import { getCommonName } from '@lucaapp/crypto';

import { HealthDepartment, HealthDepartmentAuditLog } from 'database';
import { ApiError, ApiErrorType } from 'utils/apiError';
import { verifySignedPublicKeys } from 'utils/signedKeys';
import { logEvent, AuditLogTransformer } from 'utils/hdAuditLog';
import {
  limitRequestsByUserPerHour,
  limitRequestsPerHour,
  limitRequestsByUserPerMinute,
  limitRequestsPerMinute,
} from 'middlewares/rateLimit';
import {
  validateSchema,
  validateParametersSchema,
  validateQuerySchema,
} from 'middlewares/validateSchema';
import {
  requireHealthDepartmentEmployee,
  requireHealthDepartmentAdmin,
} from 'middlewares/requireUser';
import { AuditLogEvents, AuditStatusType } from 'constants/auditLog';

import {
  storeSignedKeysSchema,
  departmentIdParametersSchema,
  auditLogDownloadQuerySchema,
  auditLogDownloadEventSchema,
  auditLogExportEventSchema,
} from './healthDepartments.schemas';

const router = Router();

// set signed keys
router.post(
  '/signedKeys',
  requireHealthDepartmentAdmin,
  validateSchema(storeSignedKeysSchema),
  async (request, response) => {
    const department = request.user.HealthDepartment;

    const commonName = getCommonName(request.body.publicCertificate);

    if (commonName !== department.commonName) {
      throw new ApiError(ApiErrorType.BAD_REQUEST);
    }

    // check that signed keys do not exist yet
    if (department.signedPublicHDEKP || department.signedPublicHDSKP) {
      throw new ApiError(ApiErrorType.SIGNED_KEYS_ALREADY_EXIST);
    }

    try {
      verifySignedPublicKeys(
        department,
        request.body.publicCertificate,
        request.body.signedPublicHDSKP,
        request.body.signedPublicHDEKP
      );
    } catch (error) {
      throw new ApiError(
        ApiErrorType.INVALID_SIGNED_KEYS,
        (error as Error).message
      );
    }

    await department.update({
      publicCertificate: request.body.publicCertificate,
      signedPublicHDEKP: request.body.signedPublicHDEKP,
      signedPublicHDSKP: request.body.signedPublicHDSKP,
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

// get a single health department
router.get(
  '/:departmentId',
  requireHealthDepartmentEmployee,
  validateParametersSchema(departmentIdParametersSchema),
  async (request, response) => {
    const department = await HealthDepartment.findByPk(
      request.params.departmentId
    );

    if (!department) {
      throw new ApiError(ApiErrorType.HEALTH_DEPARTMENT_NOT_FOUND);
    }

    return response.send({
      uuid: department.uuid,
      name: department.name,
      commonName: department.commonName,
      publicHDEKP: department.publicHDEKP,
      publicHDSKP: department.publicHDSKP,
      publicCertificate: department.publicCertificate,
      signedPublicHDEKP: department.signedPublicHDEKP,
      signedPublicHDSKP: department.signedPublicHDSKP,
      email: department.email,
      phone: department.phone,
    });
  }
);

router.get(
  '/auditlog/download',
  limitRequestsPerMinute('audit_log_download_ratelimit_minute'),
  limitRequestsPerHour('audit_log_download_ratelimit_hour'),
  requireHealthDepartmentAdmin,
  limitRequestsByUserPerMinute('audit_log_download_ratelimit_user_minute'),
  limitRequestsByUserPerHour('audit_log_download_ratelimit_user_hour'),
  validateQuerySchema(auditLogDownloadQuerySchema),
  async (request, response) => {
    const parsedTimeframe = request.query.timeframe.map(time =>
      Number.parseInt(time, 10)
    );

    // @ts-ignore no proper extending of ModelCtor to adapt node-sequelize-stream usage
    const entryStream = HealthDepartmentAuditLog.findAllWithStream({
      batchSize: 500,
      isObjectMode: true,
      where: {
        departmentId: request.user.departmentId,
        createdAt: {
          [Op.lt]: moment.unix(parsedTimeframe[0]),
          [Op.gt]: moment.unix(parsedTimeframe[1]),
        },
      },
      order: [['createdAt', 'ASC']],
    });

    logEvent(request.user, {
      type: AuditLogEvents.DOWNLOAD_AUDITLOG,
      status: AuditStatusType.SUCCESS,
      meta: {
        timeframe: parsedTimeframe.map(time => moment.unix(time).format()),
      },
    });

    const filename = `auditlog_${moment
      .unix(parsedTimeframe[1])
      .format('YYYY-MM-DD--HH-mm')}_${moment
      .unix(parsedTimeframe[0])
      .format('YYYY-MM-DD--HH-mm')}.log.txt`;

    response.set('Content-Type', 'text/plain');
    response.set('Content-Disposition', `attachment; filename="${filename}"`);
    entryStream
      .pipe(new AuditLogTransformer({ objectMode: true }))
      .pipe(response);
  }
);

router.post(
  '/auditlog/event/downloadTraces',
  requireHealthDepartmentEmployee,
  limitRequestsByUserPerHour('audit_log_event_download_traces_ratelimit_hour'),
  validateSchema(auditLogDownloadEventSchema),
  async (request, response) => {
    const { type, transferId, amount } = request.body;

    logEvent(request.user, {
      type: AuditLogEvents.DOWNLOAD_TRACES,
      status: AuditStatusType.SUCCESS,
      meta: {
        type,
        transferId,
        amount,
      },
    });

    return response.sendStatus(status.OK);
  }
);

router.post(
  '/auditlog/event/exportTraces',
  requireHealthDepartmentEmployee,
  limitRequestsByUserPerHour('audit_log_event_export_traces_ratelimit_hour'),
  validateSchema(auditLogExportEventSchema),
  async (request, response) => {
    const { transferId, amount } = request.body;

    logEvent(request.user, {
      type: AuditLogEvents.EXPORT_TRACES,
      status: AuditStatusType.SUCCESS,
      meta: {
        transferId,
        amount,
      },
    });

    return response.sendStatus(status.OK);
  }
);

export default router;
