/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-shadow */
import express from 'express';
import request from 'supertest';
import { setupDBRedisAndApp } from 'testing/utils';

import 'express-async-errors';
import { requireHealthDepartmentEmployee } from 'middlewares/requireUser';
import clientConfigRouter from './clientConfigs';
import { FeatureFlag } from '../../database';

const app = express();
app.disable('etag');
app.use('', clientConfigRouter);

const voidMiddleware = async (
  request: { user: { HealthDepartment: { commonName: string } } },
  _response: any,
  next: () => void
) => {
  request.user = {
    HealthDepartment: {
      commonName: 'test',
    },
  };
  next();
};

jest.mock('../../middlewares/requireUser', () => {
  const originalModule = jest.requireActual('../../middlewares/requireUser');

  return {
    ...originalModule,
    requireHealthDepartmentEmployee: jest.fn(),
  };
});
const mockHealthDepartmentEmployee = requireHealthDepartmentEmployee as jest.MockedFunction<
  typeof requireHealthDepartmentEmployee
>;

const createClientConfig = async (
  key: string,
  value: string | number | boolean | object,
  locationFrontend: boolean,
  healthDepartmentFrontend: boolean,
  webapp: boolean,
  ios: boolean,
  android: boolean,
  operatorApp: boolean
) =>
  FeatureFlag.create({
    key,
    value: JSON.stringify(value),
    locationFrontend,
    healthDepartmentFrontend,
    webapp,
    ios,
    android,
    operatorApp,
  });

describe('features router', () => {
  beforeAll(async () => {
    mockHealthDepartmentEmployee.mockImplementation(voidMiddleware);
    await setupDBRedisAndApp();
  });
  afterEach(() => {
    FeatureFlag.truncate();
  });

  describe('GET route', () => {
    it('should return empty objects if no client configs are available', async () => {
      const locationFrontendResponse = await request(app)
        .get('/locationFrontend')
        .set('Accept', 'application/json')
        .send();
      expect(locationFrontendResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {},
      });
      const healthDepartmentFrontendResponse = await request(app)
        .get('/healthDepartmentFrontend')
        .set('Accept', 'application/json')
        .send();
      expect(healthDepartmentFrontendResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {},
      });
      const webappResponse = await request(app)
        .get('/webapp')
        .set('Accept', 'application/json')
        .send();
      expect(webappResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {},
      });
      const iosResponse = await request(app)
        .get('/ios')
        .set('Accept', 'application/json')
        .send();
      expect(iosResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {},
      });
      const androidResponse = await request(app)
        .get('/android')
        .set('Accept', 'application/json')
        .send();
      expect(androidResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {},
      });
      const operatorAppResponse = await request(app)
        .get('/operatorApp')
        .set('Accept', 'application/json')
        .send();
      expect(operatorAppResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {},
      });
    });
    it('should return single object if there is a single client config', async () => {
      await createClientConfig(
        'foo',
        'bar',
        true,
        false,
        false,
        false,
        false,
        false
      );
      const locationFrontendResponse = await request(app)
        .get('/locationFrontend')
        .set('Accept', 'application/json')
        .send();
      expect(locationFrontendResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 'bar',
        },
      });
      const healthDepartmentFrontendResponse = await request(app)
        .get('/healthDepartmentFrontend')
        .set('Accept', 'application/json')
        .send();
      expect(healthDepartmentFrontendResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {},
      });
      const webappResponse = await request(app)
        .get('/webapp')
        .set('Accept', 'application/json')
        .send();
      expect(webappResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {},
      });
      const iosResponse = await request(app)
        .get('/ios')
        .set('Accept', 'application/json')
        .send();
      expect(iosResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {},
      });
      const androidResponse = await request(app)
        .get('/android')
        .set('Accept', 'application/json')
        .send();
      expect(androidResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {},
      });
      const operatorAppResponse = await request(app)
        .get('/operatorApp')
        .set('Accept', 'application/json')
        .send();
      expect(operatorAppResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {},
      });
    });
    it('should return single object for each client if there is a single client config for each client', async () => {
      await createClientConfig(
        'foo',
        'bar',
        true,
        true,
        true,
        true,
        true,
        true
      );
      const locationFrontendResponse = await request(app)
        .get('/locationFrontend')
        .set('Accept', 'application/json')
        .send();
      expect(locationFrontendResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 'bar',
        },
      });
      const healthDepartmentFrontendResponse = await request(app)
        .get('/healthDepartmentFrontend')
        .set('Accept', 'application/json')
        .send();
      expect(healthDepartmentFrontendResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 'bar',
        },
      });
      const webappResponse = await request(app)
        .get('/webapp')
        .set('Accept', 'application/json')
        .send();
      expect(webappResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 'bar',
        },
      });
      const iosResponse = await request(app)
        .get('/ios')
        .set('Accept', 'application/json')
        .send();
      expect(iosResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 'bar',
        },
      });
      const androidResponse = await request(app)
        .get('/android')
        .set('Accept', 'application/json')
        .send();
      expect(androidResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 'bar',
        },
      });
      const operatorAppResponse = await request(app)
        .get('/operatorApp')
        .set('Accept', 'application/json')
        .send();
      expect(operatorAppResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 'bar',
        },
      });
    });
    it('should handle numbers and objects', async () => {
      await createClientConfig('foo', 2, true, true, true, true, true, true);
      await createClientConfig(
        'bar',
        { baz: 3 },
        true,
        true,
        true,
        true,
        true,
        true
      );
      const locationFrontendResponse = await request(app)
        .get('/locationFrontend')
        .set('Accept', 'application/json')
        .send();
      expect(locationFrontendResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 2,
          bar: {
            baz: 3,
          },
        },
      });
      const healthDepartmentFrontendResponse = await request(app)
        .get('/healthDepartmentFrontend')
        .set('Accept', 'application/json')
        .send();
      expect(healthDepartmentFrontendResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 2,
          bar: {
            baz: 3,
          },
        },
      });
      const webappResponse = await request(app)
        .get('/webapp')
        .set('Accept', 'application/json')
        .send();
      expect(webappResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 2,
          bar: {
            baz: 3,
          },
        },
      });
      const iosResponse = await request(app)
        .get('/ios')
        .set('Accept', 'application/json')
        .send();
      expect(iosResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 2,
          bar: {
            baz: 3,
          },
        },
      });
      const androidResponse = await request(app)
        .get('/android')
        .set('Accept', 'application/json')
        .send();
      expect(androidResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 2,
          bar: {
            baz: 3,
          },
        },
      });
      const operatorAppResponse = await request(app)
        .get('/operatorApp')
        .set('Accept', 'application/json')
        .send();
      expect(operatorAppResponse).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          foo: 2,
          bar: {
            baz: 3,
          },
        },
      });
    });
  });
});
