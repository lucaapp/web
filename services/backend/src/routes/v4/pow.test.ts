/* eslint-disable unicorn/consistent-function-scoping */
import express from 'express';
import { etagMiddleware } from 'middlewares/etag';
import { httpLogger } from 'utils/logger';
import { handle500 } from 'middlewares/error';
import powRouter from 'routes/v4/pow';
import { setupDBRedisAndApp } from 'testing/utils';
import request from 'supertest';
import { PowChallenge, PowConfig } from 'database';
import bigInt from 'big-integer';
import moment from 'moment';
import config from 'config';
import { createChallenge } from 'utils/pow';

const POW_MAX_AGE = config.get('pow.validFor');

const app = express();
app.disable('etag');
app.use(etagMiddleware);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(httpLogger);
app.use(handle500);
app.use('', powRouter);

jest.mock('../../middlewares/rateLimit', () => {
  const originalModule = jest.requireActual('../../middlewares/rateLimit');

  return {
    ...originalModule,
    limitRequestsPerHour: (_key: unknown) => (
      _: unknown,
      __: unknown,
      next: () => unknown
    ) => next(),
  };
});

jest.mock('../../middlewares/requireNonBlockedIp', () => {
  const originalModule = jest.requireActual(
    '../../middlewares/requireNonBlockedIp'
  );

  return {
    ...originalModule,
    requireNonBlockedIp: (
      _request: unknown,
      _response: unknown,
      next: () => unknown
    ) => next(),
  };
});

const mockDifficulty = { t: bigInt(1), difficulty: 15 };
jest.mock('../../utils/powDifficulty', () => {
  const originalModule = jest.requireActual('../../utils/powDifficulty');
  return {
    ...originalModule,
    getDifficulty: () => mockDifficulty,
  };
});

describe('pow router', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
    await PowConfig.create({ type: 'test', a: 1, b: 2, c: 3 });
  });
  afterEach(() => {
    PowChallenge.truncate();
  });

  it('should return a challenge', async () => {
    const response = await request(app)
      .post('/request')
      .send({ type: 'test', userAgent: 'safari-test' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);

    const challenges = await PowChallenge.findAll();
    expect(challenges.length).toBe(1);
    const challengeToBeChecked = challenges.pop();

    expect(response.body).toHaveProperty('id', challengeToBeChecked?.uuid);
    expect(response.body).toHaveProperty('t', challengeToBeChecked?.t);
    expect(response.body).toHaveProperty('n', challengeToBeChecked?.n);
    const expiry = moment(challengeToBeChecked?.createdAt)
      .add(POW_MAX_AGE, 'hours')
      .unix();
    expect(response.body).toHaveProperty('expiresAt', expiry);
  });

  it('should solve a challenge successfully', async () => {
    const challenge = await createChallenge(mockDifficulty.t);
    const powChallenge = await PowChallenge.create({
      ...challenge,
      type: 'test',
      difficulty: mockDifficulty.difficulty,
      userAgent: 'safari',
    });

    await request(app)
      .post('/solve')
      .send({ pow: { id: powChallenge.uuid, w: powChallenge.w } })
      .set('Accept', 'application/json')
      .expect(204);
  });

  it('should return 404 on request due to no config found', async () => {
    await request(app)
      .post('/request')
      .send({ type: 'no-config', userAgent: 'safari-test' })
      .set('Accept', 'application/json')
      .expect(404);
  });

  it('should return 400 on request due wrong request params', async () => {
    await request(app)
      .post('/request')
      .send({ typo: 'test', userAgent: 'safari-test' })
      .set('Accept', 'application/json')
      .expect(400);
  });

  it('should return 400 on solve due to pow not solved', async () => {
    const challenge = await createChallenge(mockDifficulty.t);
    const powChallenge = await PowChallenge.create({
      ...challenge,
      type: 'test',
      difficulty: mockDifficulty.difficulty,
      userAgent: 'safari',
    });

    await request(app)
      .post('/solve')
      .send({ pow: { id: powChallenge.uuid, w: powChallenge.w + 1 } })
      .set('Accept', 'application/json')
      .expect(403);
  });

  it('should return 403 on solve due no challenge', async () => {
    await request(app)
      .post('/solve')
      .send({ pow: { id: '441e0109-e125-44f5-9ec5-f0f25dfed8b0', w: '123' } })
      .set('Accept', 'application/json')
      .expect(403);
  });

  it('should solve a challenge only once', async () => {
    const challenge = await createChallenge(mockDifficulty.t);
    const powChallenge = await PowChallenge.create({
      ...challenge,
      type: 'test',
      difficulty: mockDifficulty.difficulty,
      userAgent: 'safari',
    });

    await request(app)
      .post('/solve')
      .send({ pow: { id: powChallenge.uuid, w: powChallenge.w } })
      .set('Accept', 'application/json')
      .expect(204);

    await request(app)
      .post('/solve')
      .send({ pow: { id: powChallenge.uuid, w: powChallenge.w } })
      .set('Accept', 'application/json')
      .expect(403);
  });
});
