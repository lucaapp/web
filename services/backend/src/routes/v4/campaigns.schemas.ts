import { z } from 'utils/validation';

export const campaignBodySchema = z
  .object({
    utm_id: z.alphaNumericString().max(255).optional(),
    utm_source: z.alphaNumericString().max(255),
    utm_medium: z.alphaNumericString().max(255),
    utm_campaign: z.alphaNumericString().max(255).optional(),
  })
  .strict();

export const campaignParameterSchema = z.object({
  campaignId: z.uuid(),
});
