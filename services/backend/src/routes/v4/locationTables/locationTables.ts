import { Router } from 'express';
import { LocationTables } from 'database';
import {
  validateParametersSchema,
  validateSchema,
} from 'middlewares/validateSchema';
import { requireOperator } from 'middlewares/requireUser';
import { ApiError, ApiErrorType } from 'utils/apiError';
import { LocationTableInstance } from 'database/models/locationTables';
import {
  rootLocationTableParameterSchema,
  updateLocationTableSchema,
} from './locationTables.schemas';
import { requireLocationTableOwnership } from './locationTables.helpers';

const router = Router();

router.get(
  // eslint-disable-next-line sonarjs/no-duplicate-string
  '/:tableId',
  validateParametersSchema(rootLocationTableParameterSchema),
  async (request, response) => {
    const { tableId } = request.params;

    const locationTable = await findLocationTableByIdOr404(tableId);

    const { id, locationId, customName, internalNumber } = locationTable;
    return response.send({ id, customName, internalNumber, locationId });
  }
);

router.put(
  '/:tableId',
  requireOperator,
  requireLocationTableOwnership,
  validateParametersSchema(rootLocationTableParameterSchema),
  validateSchema(updateLocationTableSchema),
  async (request, response) => {
    const { tableId } = request.params;
    const { customName: newCustomName } = request.body;

    const locationTable = await findLocationTableByIdOr404(tableId);

    await locationTable.update({ customName: newCustomName });

    const { id, internalNumber, locationId, customName } = locationTable;

    return response.send({ id, customName, internalNumber, locationId });
  }
);

router.delete(
  '/:tableId',
  requireOperator,
  requireLocationTableOwnership,
  validateParametersSchema(rootLocationTableParameterSchema),
  async (request, response) => {
    const { tableId } = request.params;

    const locationTable = await findLocationTableByIdOr404(tableId);

    await locationTable.destroy();

    return response.sendStatus(204);
  }
);

const findLocationTableByIdOr404 = async (
  tableId: string
): Promise<LocationTableInstance> => {
  const locationTable = await LocationTables.findOne({
    where: {
      id: tableId,
    },
  });

  if (!locationTable) {
    throw new ApiError(ApiErrorType.NOT_FOUND);
  }

  return locationTable;
};

export default router;
