import { RequestHandler } from 'express';
import { z } from 'zod';
import status from 'http-status';
import { LocationTables } from 'database';
import { baseRequireLocationOwnerShip } from 'routes/v4/locations/tables.helpers';
import {
  locationParameterSchema,
  rootLocationTableParameterSchema,
} from './locationTables.schemas';

export const requireLocationOwnership: RequestHandler<
  z.infer<typeof locationParameterSchema>
> = async (request, response, next) => {
  const { uuid: operatorId } = request.user as IOperator;
  const { locationId } = request.params;

  return baseRequireLocationOwnerShip(
    { locationId, operatorId },
    response,
    next
  );
};

export const requireLocationTableOwnership: RequestHandler<
  z.infer<typeof rootLocationTableParameterSchema>
> = async (request, response, next) => {
  const { uuid: operatorId } = request.user as IOperator;
  const { tableId } = request.params;

  const locationTable = await LocationTables.findOne({
    where: {
      id: tableId,
    },
  });

  if (!locationTable) {
    return response.sendStatus(status.NOT_FOUND);
  }

  return baseRequireLocationOwnerShip(
    { locationId: locationTable.locationId, operatorId },
    response,
    next
  );
};
