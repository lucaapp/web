import { LOCATION_TABLES_ID_BYTES } from 'utils/locationTables';
import { z } from 'utils/validation';

export const locationParameterSchema = z.object({
  locationId: z.uuid(),
});

export const rootLocationTableParameterSchema = z.object({
  tableId: z.base64({ rawLength: LOCATION_TABLES_ID_BYTES, isBase64Url: true }),
});

export const updateLocationTableSchema = z.object({
  customName: z.safeString().max(255).nullable(),
});
