import { UserType } from 'constants/user';
import { LocationTables, Location } from 'database';
import { LocationInstance } from 'database/models/location';
import express, { NextFunction, Request, Response } from 'express';
import 'express-async-errors';
import superTestRequest from 'supertest';
import { setupDBRedisAndApp } from 'testing/utils';
import { generateLocationTableId } from 'utils/locationTables';
import logger from 'utils/logger';
import { v4 as uuid } from 'uuid';
import { setupOperator } from 'testing/testData';
import { OperatorInstance } from 'database/models/operator';
import moment from 'moment';
import { handle404, handle500 } from '../../../middlewares/error';
import tablesRouter from './locationTables';

const operatorId = uuid();

let mockReturnHasLocationOwnership = true;

jest.mock('./locationTables.helpers.ts', () => ({
  ...jest.requireActual('./locationTables.helpers.ts'),
  hasLocationOwnership: () => mockReturnHasLocationOwnership,
  requireLocationTableOwnership: (
    request: Request,
    response: Response,
    next: NextFunction
  ) => next(),
}));

const getRequest = (): superTestRequest.SuperTest<superTestRequest.Test> =>
  superTestRequest(
    express()
      .use((request, response, next) => {
        request.user = {
          type: UserType.OPERATOR,
          uuid: operatorId,
        };
        request.log = logger;
        next();
      })
      .use('/locationTables', tablesRouter)
      .use(handle404)
      .use(handle500)
  );

let location: LocationInstance;
let operator: OperatorInstance;

describe('/locationTables', () => {
  let tableId: string;

  beforeAll(async () => {
    await setupDBRedisAndApp();
    operator = await setupOperator(operatorId);
    tableId = await generateLocationTableId();
    location = await Location.create({
      radius: 10,
      publicKey: 'fake public key',
    });
  });

  beforeEach(async () => {
    mockReturnHasLocationOwnership = true;
    await LocationTables.create({
      locationId: location.uuid,
      id: tableId,
      customName: 'Some Name',
      internalNumber: 1,
    });
  });

  afterEach(async () => {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    await LocationTables.truncate({ force: true });
    await operator.update({ bannedAt: null });
  });

  afterAll(async () => {
    await Location.truncate();
  });

  describe('GET /locationTables/:id', () => {
    describe('when calling with known table ID', () => {
      it('returns the table', async () => {
        const response = await getRequest()
          .get(`/locationTables/${tableId}`)
          .send();

        expect(response.body).toMatchObject({ id: tableId });
      });
    });

    describe('when calling with unknown table ID', () => {
      it('returns the table', async () => {
        const unknownTableId = await generateLocationTableId();
        const response = await getRequest()
          .get(`/locationTables/${unknownTableId}`)
          .send();

        expect(response.status).toBe(404);
      });
    });
  });

  describe('DELETE /locationTables/:id', () => {
    describe('when calling with known table ID afterwards', () => {
      it('cannot find model in DB anymore', async () => {
        const deleteResponse = await getRequest()
          .delete(`/locationTables/${tableId}`)
          .send();
        expect(deleteResponse.status).toBe(204);

        expect(await LocationTables.findOne({ where: { id: tableId } })).toBe(
          null
        );
      });
    });
    describe('when operator is banned', () => {
      // eslint-disable-next-line jest/expect-expect
      it('404 is returned', async () => {
        await operator.update({ bannedAt: moment().toDate() });
        await getRequest()
          .delete(`/locationTables/${tableId}`)
          .send()
          .expect(404);
      });
    });

    describe('when operator is banned in the future', () => {
      // eslint-disable-next-line jest/expect-expect
      it('204 is returned', async () => {
        await operator.update({ bannedAt: moment().add(1, 'day').toDate() });
        await getRequest()
          .delete(`/locationTables/${tableId}`)
          .send()
          .expect(204);
      });
    });
  });

  describe('PUT /locationTables/:id', () => {
    describe('when calling with known table ID and change customName', () => {
      it('model in DB has changed customName', async () => {
        const response = await getRequest()
          .put(`/locationTables/${tableId}`)
          .send({ customName: 'something else' });

        expect(response.status).toBe(200);

        expect(
          await LocationTables.findOne({ where: { id: tableId } })
        ).toMatchObject({ customName: 'something else' });
      });
    });

    describe('when operator is banned', () => {
      // eslint-disable-next-line jest/expect-expect
      it('404 is returned', async () => {
        await operator.update({ bannedAt: moment().toDate() });
        await getRequest()
          .put(`/locationTables/${tableId}`)
          .send({ customName: 'something else' })
          .expect(404);
      });
    });
  });
});
