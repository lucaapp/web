import { DownloadTracesType } from 'utils/hdAuditLog';
import { z } from 'utils/validation';

export const storeSignedKeysSchema = z.object({
  publicCertificate: z.string().max(8192),
  signedPublicHDEKP: z.string().max(2048),
  signedPublicHDSKP: z.string().max(2048),
});

export const departmentIdParametersSchema = z.object({
  departmentId: z.uuid(),
});

export const auditLogDownloadQuerySchema = z.object({
  timeframe: z.array(z.string()).length(2),
});

export const auditLogDownloadEventSchema = z.object({
  type: z.nativeEnum(DownloadTracesType),
  transferId: z.uuid(),
  amount: z.number(),
});

export const auditLogExportEventSchema = z.object({
  transferId: z.uuid(),
  amount: z.number(),
});
