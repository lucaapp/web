import { Router } from 'express';
import config from 'config';
import moment from 'moment';
import status from 'http-status';
import { validateSchema } from 'middlewares/validateSchema';
import { limitRequestsPerHour } from 'middlewares/rateLimit';
import { requireNonBlockedIp } from 'middlewares/requireNonBlockedIp';
import { requirePow } from 'middlewares/requirePow';

import { createChallenge } from 'utils/pow';
import { getDifficulty } from 'utils/powDifficulty';

import { PowChallenge, PowConfig } from 'database';

import { requestSchema, solveSchema } from './pow.schemas';

const router = Router();

const POW_MAX_AGE = config.get('pow.validFor');

router.post(
  '/request',
  limitRequestsPerHour('pow_request_post_rate_limit_hour'),
  requireNonBlockedIp,
  validateSchema(requestSchema),
  async (request, response) => {
    const powConfig = await PowConfig.findByPk(request.body.type);
    if (!powConfig) {
      return response.sendStatus(status.NOT_FOUND);
    }
    const { t, difficulty } = await getDifficulty(powConfig, request.ip);
    const challenge = await createChallenge(t);

    const powChallenge = await PowChallenge.create({
      ...challenge,
      type: request.body.type,
      difficulty,
      userAgent: request.body.userAgent,
    });

    return response.send({
      id: powChallenge.uuid,
      t: powChallenge.t,
      n: powChallenge.n,
      expiresAt: moment(powChallenge.createdAt)
        .add(POW_MAX_AGE, 'hours')
        .unix(),
    });
  }
);

router.post(
  '/solve',
  validateSchema(solveSchema),
  requirePow('test'),
  async (request, response) => {
    return response.sendStatus(status.NO_CONTENT);
  }
);

export default router;
