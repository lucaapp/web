import { Router } from 'express';
import menuRouter from './locations/menu';
import tablesRouter from './locations/tables';

const router = Router();

router.use(menuRouter);
router.use(tablesRouter);
export default router;
