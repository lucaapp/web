import express from 'express';
import zipCodeRouter from 'routes/v4/zipCodes';
import { HealthDepartment, ZipCodeResponsibility } from 'database';
import request from 'supertest';
import { etagMiddleware } from 'middlewares/etag';
import { setupDBRedisAndApp } from 'testing/utils';

const app = express();
app.disable('etag');
app.use(etagMiddleware);
app.use('', zipCodeRouter);

const createHealthDepartement = async (
  name: string,
  email: string,
  phone: string
) =>
  HealthDepartment.create({
    name,
    email,
    phone,
    signedPublicHDSKP: 'signedPublicHDSKP',
    signedPublicHDEKP: 'signedPublicHDEKP',
    privateKeySecret: 'privateKeySecret',
    notificationsEnabled: false,
  });

const createZipCodeResponsibility = async (
  healthDepartement: HealthDepartment,
  zip: string,
  name: string
) => {
  ZipCodeResponsibility.create({
    zipCode: zip,
    name,
    departmentId: healthDepartement.uuid,
  });
};

describe('zip codes', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
  });
  afterEach(() => {
    ZipCodeResponsibility.truncate();
    HealthDepartment.truncate();
  });

  it('should return a list of connected healthdepartments', async () => {
    const healthDepartmentOne = await createHealthDepartement(
      'healthOne',
      'contact@healthOne.de',
      '040/123456'
    );
    healthDepartmentOne.connectEnabled = true;
    await healthDepartmentOne.save();
    await createZipCodeResponsibility(healthDepartmentOne, '20257', 'North');

    const response = await request(app)
      .get('/connect')
      .send()
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);
    expect(response.body).toHaveLength(1);
    expect(response.body).toHaveProperty('[0].name', 'healthOne');
    expect(response.body).toHaveProperty('[0].email', 'contact@healthOne.de');
    expect(response.body).toHaveProperty('[0].phone', '040/123456');
    expect(response.body).toHaveProperty(
      '[0].signedPublicHDSKP',
      'signedPublicHDSKP'
    );
    expect(response.body).toHaveProperty(
      '[0].signedPublicHDEKP',
      'signedPublicHDEKP'
    );
    expect(response.body).toHaveProperty('[0].zipCodes[0]', '20257');
  });

  it('should not return not connected healthdepartments', async () => {
    const healthDepartmentOne = await createHealthDepartement(
      'healthOne',
      'contact@healthOne.de',
      '0221/123456'
    );
    const healthDepartmentTwo = await createHealthDepartement(
      'healthTwo',
      'contact@healtTwo.de',
      '040/123654'
    );
    await createZipCodeResponsibility(healthDepartmentOne, '20257', 'North');
    await createZipCodeResponsibility(
      healthDepartmentTwo,
      '20258',
      'NorthWest'
    );
    healthDepartmentOne.connectEnabled = true;
    await healthDepartmentOne.save();

    const response = await request(app)
      .get('/connect')
      .send()
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);
    expect(response.body).toHaveLength(1);
    expect(response.body).toHaveProperty('[0].name', 'healthOne');
    expect(response.body).toHaveProperty('[0].email', 'contact@healthOne.de');
    expect(response.body).toHaveProperty('[0].phone', '0221/123456');
    expect(response.body).toHaveProperty('[0].zipCodes[0]', '20257');
  });
});
