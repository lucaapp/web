import moment from 'moment';
import config from 'config';
import { OperatorDeviceInstance } from 'database/models/operatorDevice';
import { OperatorDevice as OperatorDeviceType } from 'constants/operatorDevice';

type OperatorDeviceDTO = {
  os: string;
  name: string;
  role: OperatorDeviceType;
  deviceId: string;
  activated: boolean;
  refreshedAt: Date;
  isExpired: boolean;
};

export const getOperatorDeviceDTO = (
  device: OperatorDeviceInstance
): OperatorDeviceDTO => ({
  os: device.os,
  name: device.name,
  role: device.role,
  deviceId: device.uuid,
  activated: device.activated,
  refreshedAt: device.refreshedAt,
  isExpired:
    moment(device.refreshedAt).unix() <
    moment()
      .subtract(config.get('keys.operatorDevice.expire'), 'milliseconds')
      .unix(),
});
