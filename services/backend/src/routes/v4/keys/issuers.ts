import { Router } from 'express';
import { validateParametersSchema } from 'middlewares/validateSchema';
import { HealthDepartment } from 'database';
import { ApiError, ApiErrorType } from 'utils/apiError';
import { issuerIdParametersSchema } from './issuers.schemas';

const router = Router();
// get all issuers
router.get('/', async (request, response) => {
  const healthDepartments = await HealthDepartment.findAll();

  const allIssuers = healthDepartments.map(department => ({
    issuerId: department.uuid,
    publicCertificate: department.publicCertificate,
    signedPublicHDEKP: department.signedPublicHDEKP,
    signedPublicHDSKP: department.signedPublicHDSKP,
  }));
  response.addEtag(allIssuers);
  return response.send(allIssuers);
});

// get single issuer
router.get(
  '/:issuerId',
  validateParametersSchema(issuerIdParametersSchema),
  async (request, response) => {
    const healthDepartment = await HealthDepartment.findByPk(
      request.params.issuerId
    );

    if (!healthDepartment) {
      throw new ApiError(ApiErrorType.HEALTH_DEPARTMENT_NOT_FOUND);
    }

    const issuer = {
      uuid: healthDepartment.uuid,
      publicCertificate: healthDepartment.publicCertificate,
      signedPublicHDEKP: healthDepartment.signedPublicHDEKP,
      signedPublicHDSKP: healthDepartment.signedPublicHDSKP,
    };
    response.addEtag(issuer);
    return response.send(issuer);
  }
);

export default router;
