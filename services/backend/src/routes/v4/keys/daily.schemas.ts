import { z } from 'utils/validation';
import type { ZodNumber } from 'zod';

export const keyIdParametersSchema = z.object({
  keyId: (z.integerString() as unknown) as ZodNumber,
});

export const rotateSchema = z.object({
  signedPublicDailyKey: z.jwt({ max: 512 }),
  signature: z.ecSignature(),
  signedEncryptedPrivateDailyKeys: z.array(
    z.object({
      jwt: z.jwt({ max: 1024 }),
      signature: z.ecSignature(),
    })
  ),
});

export const rekeySchema = z.object({
  keyId: z.dailyKeyId(),
  createdAt: z.unixTimestamp(),
  signedEncryptedPrivateDailyKeys: z.array(
    z.object({
      jwt: z.jwt({ max: 1024 }),
      signature: z.ecSignature(),
    })
  ),
});
