/**
 * @overview Provides endpoints for retrieving and rotating of daily keys
 *
 * @see https://www.luca-app.de/securityoverview/properties/secrets.html#term-daily-keypair
 * @see https://www.luca-app.de/securityoverview/processes/daily_key_rotation.html
 */
/* eslint-disable max-lines, complexity */
import { Router } from 'express';
import moment from 'moment';
import config from 'config';
import status from 'http-status';
import { Transaction, DatabaseError } from 'sequelize';
import {
  verifySignedPublicDailyKey,
  verifySignedEncryptedPrivateDailyKey,
  int32ToHex,
  base64ToHex,
  VERIFY_EC_SHA256_DER_SIGNATURE,
  getOrganizationalUnitName,
} from '@lucaapp/crypto';

import { AuditLogEvents, AuditStatusType } from 'constants/auditLog';
import { logEvent } from 'utils/hdAuditLog';

import { certificateChain } from 'constants/certificateChain';
import { ApiError, ApiErrorType } from 'utils/apiError';

import {
  validateSchema,
  validateParametersSchema,
} from 'middlewares/validateSchema';

import { database, DailyPublicKey, EncryptedDailyPrivateKey } from 'database';
import logger from 'utils/logger';

import { requireHealthDepartmentEmployee } from 'middlewares/requireUser';
import { limitRequestsByUserPerHour } from 'middlewares/rateLimit';

import {
  keyIdParametersSchema,
  rotateSchema,
  rekeySchema,
} from './daily.schemas';

const router = Router();

const ORGANIZATIONAL_UNIT =
  config.get('certs.client.organizationalUnit') || undefined;

/**
 * Fetch all the latest daily public keys used to encrypt check-in data
 *
 * @see https://www.luca-app.de/securityoverview/properties/secrets.html#term-daily-keypair
 */
router.get('/', async (request, response) => {
  const dailyPublicKeys = await DailyPublicKey.findAll({
    order: [['createdAt', 'DESC']],
  });

  const mappedKeys = dailyPublicKeys.map(dailyPublicKey => ({
    signedPublicDailyKey: dailyPublicKey.signedPublicDailyKey,
  }));

  response.addEtag(mappedKeys);
  return response.send(mappedKeys);
});

/**
 * Fetch only the current daily public key
 *
 * @see https://www.luca-app.de/securityoverview/properties/secrets.html#term-daily-keypair
 */
router.get('/current', async (request, response) => {
  const dailyPublicKey = await DailyPublicKey.findOne({
    order: [['createdAt', 'DESC']],
  });

  if (!dailyPublicKey) {
    return response.sendStatus(status.NOT_FOUND);
  }

  response.addEtag(dailyPublicKey.signedPublicDailyKey);
  return response.send({
    signedPublicDailyKey: dailyPublicKey.signedPublicDailyKey,
  });
});

/**
 * Fetch a specific daily public key
 * @param keyId of the daily key to fetch
 * @see https://www.luca-app.de/securityoverview/properties/secrets.html#term-daily-keypair
 */
router.get(
  '/:keyId',
  validateParametersSchema(keyIdParametersSchema),
  async (request, response) => {
    const dailyPublicKey = await DailyPublicKey.findByPk(request.params.keyId);

    if (!dailyPublicKey) {
      return response.sendStatus(status.NOT_FOUND);
    }

    response.addEtag(dailyPublicKey.signedPublicDailyKey);
    return response.send({
      keyId: dailyPublicKey.keyId,
      signedPublicDailyKey: dailyPublicKey.signedPublicDailyKey,
    });
  }
);

/**
 * Fetch only the current daily encrypted private key
 *
 * @see https://www.luca-app.de/securityoverview/properties/secrets.html#term-daily-keypair
 */
router.get(
  '/encrypted/:keyId',
  requireHealthDepartmentEmployee,
  validateParametersSchema(keyIdParametersSchema),
  async (request, response) => {
    const encryptedDailyPrivateKey = await EncryptedDailyPrivateKey.findOne({
      where: {
        keyId: request.params.keyId,
        healthDepartmentId: request.user.departmentId,
      },
    });

    if (!encryptedDailyPrivateKey) {
      return response.sendStatus(status.NOT_FOUND);
    }
    const { signedEncryptedPrivateDailyKey } = encryptedDailyPrivateKey;

    response.addEtag(signedEncryptedPrivateDailyKey);
    return response.send({
      signedEncryptedPrivateDailyKey,
    });
  }
);

router.get(
  '/encrypted/:keyId/keyed',
  requireHealthDepartmentEmployee,
  validateParametersSchema(keyIdParametersSchema),
  async (request, response) => {
    const encryptedDailyPrivateKeys = await EncryptedDailyPrivateKey.findAll({
      where: {
        keyId: request.params.keyId,
      },
    });

    const encryptedDailyPrivateKeysDTO = encryptedDailyPrivateKeys.map(
      encryptedDailyPrivateKey => ({
        healthDepartmentId: encryptedDailyPrivateKey.healthDepartmentId,
        createdAt: moment(encryptedDailyPrivateKey.createdAt).unix(),
      })
    );

    if (encryptedDailyPrivateKeysDTO?.length > 0) {
      response.addEtag(encryptedDailyPrivateKeysDTO);
    }
    return response.send(encryptedDailyPrivateKeysDTO);
  }
);

/**
 * Rotate the daily key by having a health department generate a new key pair
 * and encrypting it for each health department using their respective HDEKP,
 * so they may retrieve it securely
 * @see https://www.luca-app.de/securityoverview/processes/daily_key_rotation.html
 * @see https://www.luca-app.de/securityoverview/properties/secrets.html#term-HDEKP
 */
router.post(
  '/rotate',
  requireHealthDepartmentEmployee,
  limitRequestsByUserPerHour('keys_daily_rotate_post_ratelimit_hour'),
  validateSchema(rotateSchema, '600kb'),
  // eslint-disable-next-line sonarjs/cognitive-complexity
  async (request, response) => {
    const {
      HealthDepartment: healthDepartment,
    } = request.user as IHealthDepartmentEmployee;

    if (
      !healthDepartment.publicHDSKP ||
      !healthDepartment.publicCertificate ||
      !healthDepartment.signedPublicHDSKP ||
      getOrganizationalUnitName(healthDepartment.publicCertificate) !==
        ORGANIZATIONAL_UNIT
    ) {
      throw new ApiError(ApiErrorType.FORBIDDEN);
    }

    const issuer = {
      issuerId: healthDepartment.uuid,
      signedPublicHDSKP: healthDepartment.signedPublicHDSKP,
      publicCertificate: healthDepartment.publicCertificate,
    };
    // verify signature of daily key and verifiy issue date
    let verifiedDailyPublicKey;
    try {
      verifiedDailyPublicKey = verifySignedPublicDailyKey({
        certificateChain,
        issuer,
        signedPublicDailyKey: request.body.signedPublicDailyKey,
        isIssuing: true,
      });
    } catch {
      logEvent(request.user, {
        type: AuditLogEvents.ISSUE_DAILY_KEYPAIR,
        status: AuditStatusType.ERROR_INVALID_SIGNATURE,
      });
      throw new ApiError(ApiErrorType.INVALID_SIGNATURE);
    }

    if (verifiedDailyPublicKey.exp) {
      const expireDate = moment.unix(verifiedDailyPublicKey.exp);
      const maxDate = moment().add(config.get('keys.daily.maxJwtExp'), 'hours');

      if (expireDate.isAfter(maxDate)) {
        throw new ApiError(ApiErrorType.BAD_REQUEST, 'Invalid expire date');
      }
    }

    const dailyPublicKey = {
      keyId: verifiedDailyPublicKey.keyId,
      publicKey: verifiedDailyPublicKey.key,
      signature: request.body.signature,
      createdAt: moment.unix(verifiedDailyPublicKey.iat),
      updatedAt: moment.unix(verifiedDailyPublicKey.iat),
      issuerId: request.user.departmentId,
      signedPublicDailyKey: request.body.signedPublicDailyKey,
    };

    // verify legacy signature of daily key
    const signedDailyKeyData =
      int32ToHex(dailyPublicKey.keyId) +
      int32ToHex(dailyPublicKey.createdAt.unix()) +
      base64ToHex(dailyPublicKey.publicKey);
    const isValidDailyPublicKeySignature = VERIFY_EC_SHA256_DER_SIGNATURE(
      base64ToHex(healthDepartment.publicHDSKP),
      signedDailyKeyData,
      base64ToHex(request.body.signature)
    );

    if (!isValidDailyPublicKeySignature) {
      logEvent(request.user, {
        type: AuditLogEvents.ISSUE_DAILY_KEYPAIR,
        status: AuditStatusType.ERROR_INVALID_SIGNATURE,
      });
      throw new ApiError(ApiErrorType.INVALID_SIGNATURE);
    }

    // check createdAt
    const now = moment();
    if (moment.duration(now.diff(dailyPublicKey.createdAt)).as('minutes') > 5) {
      logEvent(request.user, {
        type: AuditLogEvents.ISSUE_DAILY_KEYPAIR,
        status: AuditStatusType.ERROR_TIMEFRAME,
      });
      throw new ApiError(ApiErrorType.CONFLICT);
    }

    // verify signatures of encryptedKeys
    const encryptedDailyPrivateKeys = request.body.signedEncryptedPrivateDailyKeys.map(
      encryptedDailyPrivateKey => {
        if (!healthDepartment.publicHDSKP) {
          throw new Error('Health department public HDSKP is missing');
        }
        let verifiedEncryptedPrivateDailyKey;
        try {
          verifiedEncryptedPrivateDailyKey = verifySignedEncryptedPrivateDailyKey(
            {
              certificateChain,
              issuer,
              signedEncryptedPrivateDailyKey: encryptedDailyPrivateKey.jwt,
              isIssuing: true,
            }
          );
        } catch {
          logEvent(request.user, {
            type: AuditLogEvents.ISSUE_DAILY_KEYPAIR,
            status: AuditStatusType.ERROR_INVALID_SIGNATURE,
          });
          throw new ApiError(ApiErrorType.INVALID_SIGNATURE);
        }

        // verify legacy signatures of encryptedKeys
        // this will be removed after client implement new signature verification
        const signedData =
          int32ToHex(verifiedEncryptedPrivateDailyKey.keyId) +
          int32ToHex(verifiedEncryptedPrivateDailyKey.iat) +
          base64ToHex(verifiedEncryptedPrivateDailyKey.key.publicKey);
        const isValidSignature = VERIFY_EC_SHA256_DER_SIGNATURE(
          base64ToHex(healthDepartment.publicHDSKP),
          signedData,
          base64ToHex(encryptedDailyPrivateKey.signature)
        );

        if (!isValidSignature) {
          logEvent(request.user, {
            type: AuditLogEvents.ISSUE_DAILY_KEYPAIR,
            status: AuditStatusType.ERROR_INVALID_SIGNATURE,
          });
          throw new ApiError(ApiErrorType.INVALID_SIGNATURE);
        }

        return {
          keyId: verifiedEncryptedPrivateDailyKey.keyId,
          createdAt: moment.unix(verifiedEncryptedPrivateDailyKey.iat),
          issuerId: request.user.departmentId,
          healthDepartmentId: verifiedEncryptedPrivateDailyKey.sub,
          data: verifiedEncryptedPrivateDailyKey.key.data,
          iv: verifiedEncryptedPrivateDailyKey.key.iv,
          mac: verifiedEncryptedPrivateDailyKey.key.mac,
          publicKey: verifiedEncryptedPrivateDailyKey.key.publicKey,
          signature: encryptedDailyPrivateKey.signature,
          signedEncryptedPrivateDailyKey: encryptedDailyPrivateKey.jwt,
        };
      }
    );

    // verify that the encryptedPrivateKeys have the same keyId as the public key
    const doKeyIdsAndIssueTimeMatch = encryptedDailyPrivateKeys.every(
      encryptedDailyPrivateKey =>
        encryptedDailyPrivateKey.keyId === dailyPublicKey.keyId &&
        encryptedDailyPrivateKey.createdAt.unix() ===
          dailyPublicKey.createdAt.unix()
    );

    if (!doKeyIdsAndIssueTimeMatch) {
      logEvent(request.user, {
        type: AuditLogEvents.ISSUE_DAILY_KEYPAIR,
        status: AuditStatusType.ERROR_INVALID_KEYID,
      });
      throw new ApiError(ApiErrorType.BAD_REQUEST);
    }

    const transaction = await database.transaction({
      isolationLevel: Transaction.ISOLATION_LEVELS.SERIALIZABLE,
    });

    try {
      const currentDailyPublicKey = await DailyPublicKey.findOne({
        order: [['createdAt', 'DESC']],
        transaction,
      });

      // initial keyId should be 0
      if (!currentDailyPublicKey && dailyPublicKey.keyId !== 0) {
        logEvent(request.user, {
          type: AuditLogEvents.ISSUE_DAILY_KEYPAIR,
          status: AuditStatusType.ERROR_INVALID_KEYID,
        });
        throw new ApiError(ApiErrorType.BAD_REQUEST, 'Invalid keyId.');
      }

      // new keyId should +1 the old keyId
      if (
        currentDailyPublicKey &&
        (currentDailyPublicKey.keyId + 1) % config.get('keys.daily.max') !==
          dailyPublicKey.keyId
      ) {
        logEvent(request.user, {
          type: AuditLogEvents.ISSUE_DAILY_KEYPAIR,
          status: AuditStatusType.ERROR_LIMIT_EXCEEDED,
        });

        throw new ApiError(ApiErrorType.BAD_REQUEST, 'Invalid keyId.');
      }

      // old key should be at least 1 day old before rotation
      if (currentDailyPublicKey) {
        const keyAge = moment.duration(
          moment().diff(currentDailyPublicKey.createdAt)
        );

        if (keyAge.asHours() < config.get('keys.daily.minKeyAge')) {
          logEvent(request.user, {
            type: AuditLogEvents.ISSUE_DAILY_KEYPAIR,
            status: AuditStatusType.ERROR_LIMIT_EXCEEDED,
          });

          throw new ApiError(ApiErrorType.BAD_REQUEST, 'Too early.');
        }
      }

      // update daily key
      await DailyPublicKey.upsert(dailyPublicKey, { transaction });

      // update private keys
      await Promise.all(
        encryptedDailyPrivateKeys.map(key =>
          EncryptedDailyPrivateKey.upsert(key, { transaction })
        )
      );

      await transaction.commit();

      logEvent(request.user, {
        type: AuditLogEvents.ISSUE_DAILY_KEYPAIR,
        status: AuditStatusType.SUCCESS,
      });

      return response.sendStatus(status.NO_CONTENT);
    } catch (error) {
      await transaction.rollback();
      logger.error('Rotation failed.', error);

      // Transaction error
      if (error instanceof DatabaseError) {
        throw new ApiError(ApiErrorType.CONFLICT);
      }

      logEvent(request.user, {
        type: AuditLogEvents.ISSUE_DAILY_KEYPAIR,
        status: AuditStatusType.ERROR_UNKNOWN_SERVER_ERROR,
      });

      throw error;
    }
  }
);

/**
 * Provide missing daily keys to other health departments by having a health
 * department generate a new one and encrypting it for each other department and
 * finally uploading it to the luca Server to be retrieved by the other departments
 *
 * @see https://www.luca-app.de/securityoverview/processes/daily_key_rotation.html
 */
router.post(
  '/rekey',
  requireHealthDepartmentEmployee,
  validateSchema(rekeySchema, '600kb'),
  async (request, response) => {
    const {
      HealthDepartment: healthDepartment,
    } = request.user as IHealthDepartmentEmployee;

    if (
      !healthDepartment.publicHDSKP ||
      !healthDepartment.publicCertificate ||
      !healthDepartment.signedPublicHDSKP
    ) {
      throw new ApiError(ApiErrorType.FORBIDDEN);
    }

    const { signedEncryptedPrivateDailyKeys, keyId, createdAt } = request.body;

    const issuer = {
      issuerId: healthDepartment.uuid,
      signedPublicHDSKP: healthDepartment.signedPublicHDSKP,
      publicCertificate: healthDepartment.publicCertificate,
    };

    // provided createdAt should match the current createdAt
    const dailyPublicKey = await DailyPublicKey.findOne({
      where: { keyId, createdAt: moment.unix(createdAt) },
    });

    if (!dailyPublicKey) {
      throw new ApiError(ApiErrorType.CONFLICT, 'No such daily key.');
    }

    // verify signatures of encryptedKeys
    const newKeys = signedEncryptedPrivateDailyKeys.map(
      // eslint-disable-next-line sonarjs/no-identical-functions
      encryptedDailyPrivateKey => {
        let verifiedEncryptedPrivateDailyKey;
        try {
          verifiedEncryptedPrivateDailyKey = verifySignedEncryptedPrivateDailyKey(
            {
              certificateChain,
              issuer,
              signedEncryptedPrivateDailyKey: encryptedDailyPrivateKey.jwt,
            }
          );
        } catch {
          logEvent(request.user, {
            type: AuditLogEvents.ISSUE_DAILY_KEYPAIR,
            status: AuditStatusType.ERROR_INVALID_SIGNATURE,
          });
          throw new ApiError(ApiErrorType.INVALID_SIGNATURE);
        }

        // verify legacy signatures of encryptedKeys
        // this will be removed after client implement new signature verification
        const signedData =
          int32ToHex(verifiedEncryptedPrivateDailyKey.keyId) +
          int32ToHex(verifiedEncryptedPrivateDailyKey.iat) +
          base64ToHex(verifiedEncryptedPrivateDailyKey.key.publicKey);
        const isValidSignature = VERIFY_EC_SHA256_DER_SIGNATURE(
          base64ToHex(healthDepartment.publicHDSKP || ''),
          signedData,
          base64ToHex(encryptedDailyPrivateKey.signature)
        );

        if (!isValidSignature) {
          throw new ApiError(ApiErrorType.INVALID_SIGNATURE);
        }

        return {
          keyId: verifiedEncryptedPrivateDailyKey.keyId,
          createdAt: moment.unix(verifiedEncryptedPrivateDailyKey.iat),
          issuerId: request.user.departmentId,
          healthDepartmentId: verifiedEncryptedPrivateDailyKey.sub,
          data: verifiedEncryptedPrivateDailyKey.key.data,
          iv: verifiedEncryptedPrivateDailyKey.key.iv,
          mac: verifiedEncryptedPrivateDailyKey.key.mac,
          publicKey: verifiedEncryptedPrivateDailyKey.key.publicKey,
          signature: encryptedDailyPrivateKey.signature,
          signedEncryptedPrivateDailyKey: encryptedDailyPrivateKey.jwt,
        };
      }
    );

    // verify that the encryptedPrivateKeys have the same keyId as the public key
    const doKeyIdsAndIssueTimeMatch = newKeys.every(
      encryptedDailyPrivateKey =>
        encryptedDailyPrivateKey.keyId === dailyPublicKey.keyId &&
        encryptedDailyPrivateKey.createdAt.unix() === createdAt
    );

    if (!doKeyIdsAndIssueTimeMatch) {
      throw new ApiError(
        ApiErrorType.BAD_REQUEST,
        'Issue times or keyIds do not match.'
      );
    }

    for (const newKey of newKeys) {
      const oldKey = await EncryptedDailyPrivateKey.findOne({
        where: {
          keyId,
          healthDepartmentId: newKey.healthDepartmentId,
        },
      });

      if (!oldKey) {
        await EncryptedDailyPrivateKey.create(newKey);
      } else if (oldKey.createdAt === dailyPublicKey.createdAt) {
        logger.warn('key already current.');
      } else {
        oldKey.update(newKey);
      }
    }

    return response.sendStatus(status.OK);
  }
);

export default router;
