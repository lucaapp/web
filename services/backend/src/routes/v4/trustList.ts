import { Router } from 'express';
import config from 'config';
import { getCachedUrl } from 'utils/urlCache';
import { limitRequestsPerDay } from 'middlewares/rateLimit';

const router = Router();

router.get(
  '/dsc',
  limitRequestsPerDay('trustlist_dsc_get_ratelimit_day'),
  async (request, response) => {
    const url = config.get('luca.trustList.source') as string;

    const [data, etag] = await getCachedUrl(url);

    response.setHeader('Content-Type', 'application/json');
    if (etag) {
      response.addEtag(data, etag);
    }

    return response.send(data);
  }
);

export default router;
