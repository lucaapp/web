import { Router } from 'express';
import logger from 'utils/logger';
import { constructSwaggerSpec } from 'utils/swagger';

const router = Router();

const swaggerSpec = {
  openapi: '3.0.0',
  info: {
    title: 'luca API',
    version: '4.0.0',
    description: '',
  },
  servers: [
    { url: '/api/v4', description: 'This server' },
    { url: 'https://app.luca-app.de/api/v4', description: 'Production' },
  ],
};

(async () => {
  try {
    const { spec, middlewares } = await constructSwaggerSpec(
      './src/routes/{v4,swagger}/**/*.openapi.yaml',
      swaggerSpec
    );

    router.get('/swagger.json', (_request, response) => response.json(spec));
    router.use('/', ...middlewares);
  } catch (error) {
    logger.error(error as Error);
  }
})();

export default router;
