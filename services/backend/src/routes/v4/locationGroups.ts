import { Router } from 'express';
import searchRouter from './locationGroups/search';
import discoverRouter from './locationGroups/discover';

const router = Router();

router.use(searchRouter);
router.use(discoverRouter);

export default router;
