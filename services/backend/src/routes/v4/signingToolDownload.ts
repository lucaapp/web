import { Router } from 'express';
import { SigningToolDownload } from 'database';
import { requireHealthDepartmentAdmin } from 'middlewares/requireUser';

const router = Router();

router.get('/downloads', requireHealthDepartmentAdmin, async (_, response) => {
  const downloads = await SigningToolDownload.findAll({
    order: [['version', 'DESC']],
  });

  return response.send(
    downloads.map(({ version, downloadUrl, hash }) => ({
      version,
      downloadUrl,
      hash,
    }))
  );
});

export default router;
