/* eslint-disable unicorn/consistent-function-scoping */
/* eslint-disable @typescript-eslint/no-explicit-any */
import express from 'express';
import request from 'supertest';
import moment from 'moment';
import 'express-async-errors';
import { setupDBRedisAndApp } from 'testing/utils';
import { VaultContents } from '../../database';
import { handle500 } from '../../middlewares/error';
import { vaultGETValidation } from '../../utils/vaultValidation';
import vaultRouter from './vault';
import { httpLogger } from '../../utils/logger';

const VAULT_URL = '/vault';

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(httpLogger);
app.use(handle500);
app.use(VAULT_URL, vaultRouter);

jest.mock('../../middlewares/rateLimit', () => {
  const originalModule = jest.requireActual('../../middlewares/rateLimit');

  return {
    ...originalModule,
    limitRequestsPerMinute: (_key: any) => (_: any, __: any, next: () => any) =>
      next(),
  };
});

jest.mock('../../middlewares/requirePow', () => ({
  requirePow: (_string: any) => (_: any, __: any, next: () => any) => next(),
}));

jest.mock('../../middlewares/requireUser', () => {
  const originalModule = jest.requireActual('../../middlewares/requireUser');

  return {
    ...originalModule,
    requireOperatorDevice: (_request: any, _response: any, next: () => any) =>
      next(),
  };
});

jest.mock('passport', () => {
  const originalModule = jest.requireActual('passport');

  originalModule.authenticate = (_authType: any, _options: any) => (
    _request: any,
    _response: any,
    next: () => any
  ) => {
    next();
  };

  return originalModule;
});

jest.mock('../../utils/vaultValidation');
const mockVaultValidation = vaultGETValidation as jest.MockedFunction<
  typeof vaultGETValidation
>;

const vaultContent = {
  id: '57a21705-be97-402c-8ec2-eded39ee17a1',
  version: 1,
  expirationDate: moment().add(1, 'day').unix(),
  encryptedPayload: 'dGhpcyBpcyBob3BlZnVsbHkgbG9uZ2VyIHRoYW4gMTYgYnl0ZXM=',
};

const totpSecret = 'c3RpbGwgYWxpdmU=';
const pow = {
  id: '3a197b5c-6fcb-412a-95df-306a4b3367e4',
  w:
    '52571132426657360441427045055983635933515940268316843025244762401389879291316542034234242948932966607766801473906759577822628298915837996423643809935627166826033619751573226601902604144301515523634718827548161237613335611612569397881443713947965167915036023670897926017830374588217369745916695296266512681931',
};

const requestParameters = {
  content: vaultContent,
  totpSecret,
  pow,
};

const createVaultInDatabase = async () =>
  VaultContents.create({
    id: vaultContent.id,
    version: vaultContent.version,
    encryptedPayload: Buffer.from(vaultContent.encryptedPayload, 'base64'),
    expirationDate: moment.unix(vaultContent.expirationDate).toDate(),
    totpSecret: Buffer.from(totpSecret, 'base64'),
  });

const createExpiredVaultInDatabase = async () =>
  VaultContents.create({
    id: vaultContent.id,
    version: vaultContent.version,
    encryptedPayload: Buffer.from(vaultContent.encryptedPayload, 'base64'),
    expirationDate: moment().toDate(),
    totpSecret: Buffer.from(totpSecret, 'base64'),
  });

describe('Vault router', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
  });

  afterEach(() => {
    VaultContents.truncate();
  });

  describe('POST route', () => {
    it('should accept valid vault object', async () => {
      const response = await request(app)
        .post(VAULT_URL)
        .send(requestParameters)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(201);
      expect(response.body).toEqual(requestParameters.content);
    });

    it('should fail with invalid schema', async () => {
      await request(app)
        .post(VAULT_URL)
        .send({})
        .set('Accept', 'application/json')
        .expect(400);
    });

    it('should fail with invalid version', async () => {
      await request(app)
        .post(VAULT_URL)
        .send({
          content: {
            ...vaultContent,
            expirationDate: 2,
          },
          totpSecret,
          pow,
        })
        .set('Accept', 'application/json')
        .expect(400);
    });

    it('should fail with expired vaultObject', async () => {
      await request(app)
        .post(VAULT_URL)
        .send({
          content: {
            ...vaultContent,
            expirationDate: moment().unix(),
          },
          totpSecret,
          pow,
        })
        .set('Accept', 'application/json')
        .expect(400);
    });

    it('should fail when vaultObject expires too late', async () => {
      await request(app)
        .post(VAULT_URL)
        .send({
          content: {
            ...vaultContent,
            expirationDate: moment().add(1, 'month').unix(),
          },
          totpSecret,
          pow,
        })
        .set('Accept', 'application/json')
        .expect(400);
    });

    it('should fail when conflicting with existing vault object', async () => {
      await createVaultInDatabase();
      await request(app)
        .post(VAULT_URL)
        .send(requestParameters)
        .set('Accept', 'application/json')
        .expect(403);
    });
  });

  describe('GET route', () => {
    it('should return vault when created previously', async () => {
      mockVaultValidation.mockResolvedValue(true);
      await createVaultInDatabase();
      const response = await request(app)
        .get(`${VAULT_URL}/${requestParameters.content.id}`)
        .set('Accept', 'application/json')
        .expect(200);
      expect(response.body).toEqual(requestParameters.content);
    });

    it('should return nothing when database is empty', async () => {
      await request(app)
        .get(`${VAULT_URL}/${requestParameters.content.id}`)
        .set('Accept', 'application/json')
        .expect(404);
    });

    it('should fail with invalid schema', async () => {
      await request(app)
        .get(`${VAULT_URL}/123invalid`)
        .set('Accept', 'application/json')
        .expect(400);
    });

    it('should return nothing when vault already expired', async () => {
      await createExpiredVaultInDatabase();
      await request(app)
        .get(`${VAULT_URL}/${requestParameters.content.id}`)
        .set('Accept', 'application/json')
        .expect(404);
    });

    it('should fail with invalid totp token', async () => {
      mockVaultValidation.mockResolvedValue(false);
      await createVaultInDatabase();
      await request(app)
        .get(`${VAULT_URL}/${requestParameters.content.id}`)
        .set('Accept', 'application/json')
        .expect(401);
    });
  });
});
