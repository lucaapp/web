import { Router } from 'express';

import { ZipCodeResponsibility, HealthDepartment } from 'database';

const router = Router();

router.get('/connect', async (_, response) => {
  const departments = await HealthDepartment.findAll({
    attributes: [
      'uuid',
      'name',
      'signedPublicHDSKP',
      'signedPublicHDEKP',
      'email',
      'phone',
      'connectEnabled',
    ],
    where: {
      connectEnabled: true,
    },
    include: {
      model: ZipCodeResponsibility,
      required: true,
      attributes: ['zipCode'],
    },
  });

  const responsibleZipCodesDTO = departments.map(department => ({
    departmentId: department.uuid,
    name: department.name,
    signedPublicHDSKP: department.signedPublicHDSKP,
    signedPublicHDEKP: department.signedPublicHDEKP,
    email: department.email,
    phone: department.phone,
    zipCodes: (department.ZipCodeResponsibilities || []).map(
      ({ zipCode }) => zipCode
    ),
  }));

  response.addEtag(responsibleZipCodesDTO);

  return response.send(responsibleZipCodesDTO);
});

export default router;
