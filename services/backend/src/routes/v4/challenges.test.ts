/* eslint-disable sonarjs/no-identical-functions */
/* eslint-disable unicorn/consistent-function-scoping */
/* eslint-disable max-lines */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-shadow */
import express from 'express';
import { setupDBRedisAndApp } from 'testing/utils';

import 'express-async-errors';
import moment from 'moment';
import { randomUUID } from 'crypto';
import { OperatorInstance } from 'database/models/operator';
import superTestRequest from 'supertest';
import {
  ChallengeType,
  OperatorDeviceCreationChallengeState,
} from 'constants/challenges';
import { setupOperator } from '../../testing/testData';
import { handle404, handle500 } from '../../middlewares/error';
import challengesRouter from './challenges';
import { Challenge } from '../../database';
import { UserType } from '../../constants/user';
import logger from '../../utils/logger';

// eslint-disable-next-line security/detect-unsafe-regex
const uuidRegex = /^[\dA-Fa-f]{8}(?:\b-[\dA-Fa-f]{4}){3}\b-[\dA-Fa-f]{12}$/;

const operatorId = randomUUID();

const getRequest = (): superTestRequest.SuperTest<superTestRequest.Test> =>
  superTestRequest(
    express()
      .disable('etag')
      .use((request, response, next) => {
        request.user = {
          type: UserType.OPERATOR,
          uuid: operatorId,
        };
        request.log = logger;
        next();
      })
      .use('', challengesRouter)
      .use(handle404)
      .use(handle500)
  );

jest.mock('../../middlewares/rateLimit', () => {
  const originalModule = jest.requireActual('../../middlewares/rateLimit');

  return {
    ...originalModule,
    limitRequestsPerHour: (_key: unknown) => (
      _: unknown,
      __: unknown,
      next: () => unknown
    ) => next(),
    limitRequestsPerDay: (_key: unknown) => (
      _: unknown,
      __: unknown,
      next: () => unknown
    ) => next(),
  };
});

const createNewChallenge = async () => {
  const challenge = await Challenge.create({
    type: ChallengeType.OperatorDeviceCreation,
    state: OperatorDeviceCreationChallengeState.Ready,
  });

  return challenge.uuid;
};

let operator: OperatorInstance;

describe('challenges router', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
    operator = await setupOperator(operatorId);
  });
  afterEach(async () => {
    Challenge.truncate();
    await operator.update({ bannedAt: null });
  });

  describe('POST route', () => {
    it('should return new challenge', async () => {
      const response = await getRequest()
        .post('/operatorDevice')
        .set('Accept', 'application/json')
        .send();
      expect(response).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          uuid: expect.stringMatching(uuidRegex),
          type: ChallengeType.OperatorDeviceCreation,
          state: OperatorDeviceCreationChallengeState.Ready,
        },
      });
    });
    it('should return 404 if operator is banned', async () => {
      await operator.update({ bannedAt: moment().toDate() });
      const response = await getRequest()
        .post('/operatorDevice')
        .set('Accept', 'application/json')
        .send()
        .expect(404);
      expect(response).toMatchObject({
        status: 404,
      });
    });
  });
  describe('GET route', () => {
    it('should return 404 if challenge not found', async () => {
      const response = await getRequest()
        .get('/operatorDevice/1895d505-2798-4de7-8d54-ce9ccfb61021')
        .set('Accept', 'application/json')
        .send();
      expect(response).toMatchObject({
        status: 404,
      });
    });
    it('should return previously created challenge', async () => {
      const challengeId = await createNewChallenge();
      const response = await getRequest()
        .get(`/operatorDevice/${challengeId}`)
        .set('Accept', 'application/json')
        .send();
      expect(response).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          uuid: challengeId,
          type: ChallengeType.OperatorDeviceCreation,
          state: OperatorDeviceCreationChallengeState.Ready,
        },
      });
    });
  });
  describe('PATCH route', () => {
    it('should return 404 if challenge not found', async () => {
      const response = await getRequest()
        .patch('/operatorDevice/1895d505-2798-4de7-8d54-ce9ccfb61021')
        .set('Accept', 'application/json')
        .send({
          state: OperatorDeviceCreationChallengeState.Ready,
        });
      expect(response).toMatchObject({
        status: 404,
      });
    });
    it('should be idempotent', async () => {
      const challengeId = await createNewChallenge();
      const response = await getRequest()
        .patch(`/operatorDevice/${challengeId}`)
        .set('Accept', 'application/json')
        .send({
          state: OperatorDeviceCreationChallengeState.Ready,
        });
      expect(response).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          uuid: challengeId,
          type: ChallengeType.OperatorDeviceCreation,
          state: OperatorDeviceCreationChallengeState.Ready,
        },
      });
    });
    it('should change state of the challenge', async () => {
      const challengeId = await createNewChallenge();
      const response = await getRequest()
        .patch(`/operatorDevice/${challengeId}`)
        .set('Accept', 'application/json')
        .send({
          state: OperatorDeviceCreationChallengeState.AuthenticationPINRequired,
        });
      expect(response).toMatchObject({
        status: 200,
        headers: {
          'content-type': expect.stringMatching(/json/),
        },
        body: {
          uuid: challengeId,
          type: ChallengeType.OperatorDeviceCreation,
          state: OperatorDeviceCreationChallengeState.AuthenticationPINRequired,
        },
      });
    });
  });
});
