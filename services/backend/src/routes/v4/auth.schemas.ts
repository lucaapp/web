import { z } from 'utils/validation';

export const authSchema = z.object({
  username: z.email(),
  password: z.string().nonempty().max(255),
});

export const authOperatorDeviceSchema = z.object({
  refreshToken: z.string().max(255),
});
