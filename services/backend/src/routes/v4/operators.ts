import { Router } from 'express';
import locationsRouter from './operators/locations';

const router = Router();

router.use('/locations', locationsRouter);

export default router;
