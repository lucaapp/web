import moment from 'moment';
import * as status from 'http-status';
import { Router } from 'express';
import passport from 'passport';
import { vaultGETValidation } from '../../utils/vaultValidation';
import { ApiError, ApiErrorType } from '../../utils/apiError';
import { VaultContents } from '../../database';
import { limitRequestsPerMinute } from '../../middlewares/rateLimit';
import {
  validateParametersSchema,
  validateSchema,
} from '../../middlewares/validateSchema';
import { createSchema, vaultContentIdParametersSchema } from './vault.schemas';

import { requireOperatorDevice } from '../../middlewares/requireUser';

import { requirePow } from '../../middlewares/requirePow';

const router = Router();

// URL: https://localhost/api/v4/vault
router.post(
  '/',
  validateSchema(createSchema),
  limitRequestsPerMinute('vault_post_ratelimit_minute'),
  requirePow('createVaultContent'),
  async (request, response) => {
    const now = moment.utc();
    const latestExpirationDate = now.clone();
    latestExpirationDate.add(1, 'day').add(5, 'minutes');
    const { content, totpSecret } = request.body;
    const expirationDate = moment.unix(content.expirationDate);

    if (content.version !== 1) {
      throw new ApiError(ApiErrorType.VERSION_NOT_SUPPORTED);
    } else if (now > expirationDate) {
      throw new ApiError(ApiErrorType.VAULT_OBJECT_ALREADY_EXPIRED);
    } else if (expirationDate > latestExpirationDate) {
      throw new ApiError(ApiErrorType.VAULT_OBJECT_EXPIRES_TOO_LATE);
    }

    try {
      await VaultContents.create({
        id: content.id,
        version: content.version,
        encryptedPayload: Buffer.from(content.encryptedPayload, 'base64'),
        expirationDate: expirationDate.toDate(),
        totpSecret: Buffer.from(totpSecret, 'base64'),
      });
      return response.status(201).send(content);
    } catch {
      return response.sendStatus(status.FORBIDDEN);
    }
  }
);

router.get(
  '/:vaultContentId',
  requireOperatorDevice,
  validateParametersSchema(vaultContentIdParametersSchema),
  limitRequestsPerMinute('vault_get_ratelimit_minute'),
  passport.authenticate('header-device', { session: false }),
  async (request, response) => {
    const vaultContent = await VaultContents.findByPk(
      request.params.vaultContentId
    );

    if (!vaultContent) throw new ApiError(ApiErrorType.VAULT_NOT_FOUND);

    const now = moment.utc();
    if (now.toDate() > vaultContent.expirationDate) {
      throw new ApiError(ApiErrorType.VAULT_NOT_FOUND);
    }

    const {
      version,
      totpSecret,
      id,
      encryptedPayload,
      expirationDate,
    } = vaultContent;
    const { headers } = request;

    const isTotpTokenValid = await vaultGETValidation(
      headers,
      version,
      now.unix(),
      totpSecret
    );

    if (!isTotpTokenValid) {
      throw new ApiError(ApiErrorType.TOTP_TOKEN_INVALID);
    }

    response.send({
      id,
      version,
      encryptedPayload: encryptedPayload.toString('base64'),
      expirationDate: moment(expirationDate).unix(),
    });
  }
);

export default router;
