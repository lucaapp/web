import { Router } from 'express';
import status from 'http-status';

import {
  validateParametersSchema,
  validateSchema,
} from 'middlewares/validateSchema';
import { limitRequestsPerMinute } from 'middlewares/rateLimit';
import {
  campaignBodySchema,
  campaignParameterSchema,
} from 'routes/v4/campaigns.schemas';
import { OperatorCampaign } from 'database';
import { requireOperator } from 'middlewares/requireUser';
import { ApiError, ApiErrorType } from 'utils/apiError';
import { checkCampaignLimit } from 'utils/campaignLimit';

const router = Router();

router.get(
  '/',
  limitRequestsPerMinute('campaigns_get_ratelimit_minute', {
    skipSuccessfulRequests: true,
    global: false,
  }),
  requireOperator,
  async (request, response) => {
    const operatorId = request.user.uuid;
    const campaigns = await OperatorCampaign.findAll({ where: { operatorId } });

    response.status(status.OK);
    response.send(campaigns);
  }
);

router.get(
  '/:campaignId',
  limitRequestsPerMinute('campaign_get_ratelimit_minute', {
    skipSuccessfulRequests: true,
    global: false,
  }),
  validateParametersSchema(campaignParameterSchema),
  requireOperator,
  async (request, response) => {
    const operatorId = request.user.uuid;
    const { campaignId } = request.params;

    const campaign = await OperatorCampaign.findOne({
      where: { uuid: campaignId, operatorId },
    });

    if (!campaign) {
      throw new ApiError(
        ApiErrorType.NOT_FOUND,
        `Campaign not found (${campaignId})`
      );
    }

    response.status(status.OK);
    response.send(campaign);
  }
);

router.post(
  '/',
  limitRequestsPerMinute('campaigns_post_ratelimit_minute', {
    skipSuccessfulRequests: true,
    global: false,
  }),
  requireOperator,
  validateSchema(campaignBodySchema),
  async (request, response) => {
    const operatorId = request.user.uuid;
    await checkCampaignLimit(operatorId);

    const campaign = await OperatorCampaign.create({
      utm_id: request.body.utm_id,
      utm_source: request.body.utm_source,
      utm_medium: request.body.utm_medium,
      utm_campaign: request.body.utm_campaign,
      operatorId,
    });

    response.status(status.CREATED);
    return response.send(campaign);
  }
);

export default router;
