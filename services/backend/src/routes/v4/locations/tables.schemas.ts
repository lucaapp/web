import { z } from 'utils/validation';
import { ZodNumber } from 'zod';

export const locationParameterSchema = z.object({
  locationId: z.uuid(),
});

export const nestedLocationTableParameterSchema = z.object({
  locationId: z.uuid(),
  internalNumber: (z.integerString() as unknown) as ZodNumber,
});

export const createLocationTableSchema = z.object({
  tables: z.array(
    z.object({
      customName: z.safeString().max(255).nullable(),
    })
  ),
});

export const updateLocationTableSchema = z.object({
  customName: z.safeString().max(255).nullable(),
});
