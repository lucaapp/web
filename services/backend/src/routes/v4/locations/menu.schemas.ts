import { z } from 'utils/validation';

const maxTranslatableLength = 1000;
const maxAmountOfAllergensAndAdditivesLength = 100;
const maxAmountOfAllergensAndAdditives = 100;

const translatable = z
  .object({
    de: z.safeString().max(maxTranslatableLength),
    en: z.safeString().max(maxTranslatableLength).optional(),
  })
  .optional();

export const locationParameterSchema = z.object({
  locationId: z.uuid(),
});

export const updateMenuSchema = z.object({
  categories: z
    .array(
      z.object({
        name: translatable,
        description: translatable,
        items: z
          .array(
            z.object({
              name: translatable,
              description: translatable,
              additionalInfo: translatable,
              price: z.number(),
              allergensAndAdditives: z
                .array(
                  z.safeString().max(maxAmountOfAllergensAndAdditivesLength)
                )
                .max(maxAmountOfAllergensAndAdditives),
            })
          )
          .max(100),
      })
    )
    .max(40),
  allergenesAndAdditives: z
    .record(translatable)
    .refine(
      async value =>
        Object.keys(value).length <= maxAmountOfAllergensAndAdditives
    ),
});
