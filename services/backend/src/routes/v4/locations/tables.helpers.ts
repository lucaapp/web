import { NextFunction, RequestHandler, Response } from 'express';
import { z } from 'zod';
import status from 'http-status';
import { Location } from 'database';
import { locationParameterSchema } from './tables.schemas';

export const hasLocationOwnership = async ({
  locationId,
  operatorId,
}: {
  locationId: string;
  operatorId: string;
}): Promise<boolean> => {
  const location = await Location.findOne({
    where: {
      uuid: locationId,
      operator: operatorId,
    },
    attributes: ['uuid'],
  });

  return location != null;
};

export const baseRequireLocationOwnerShip = async <
  TResponse extends Response,
  TNext extends NextFunction
>(
  parameters: { locationId: string; operatorId: string },
  response: TResponse,
  next: TNext
): Promise<void> => {
  if (!(await hasLocationOwnership(parameters))) {
    response.sendStatus(status.NOT_FOUND);
  }

  return next();
};

export const requireLocationOwnership: RequestHandler<
  z.infer<typeof locationParameterSchema>
> = async (request, response, next) => {
  const { uuid: operatorId } = request.user as IOperator;
  const { locationId } = request.params;

  return baseRequireLocationOwnerShip(
    { locationId, operatorId },
    response,
    next
  );
};
