import { UserType } from 'constants/user';
import { LocationTables, Location } from 'database';
import { LocationInstance } from 'database/models/location';
import { LocationTableInstance } from 'database/models/locationTables';
import express, { NextFunction, Request, Response } from 'express';
import 'express-async-errors';
import superTestRequest from 'supertest';
import { setupDBRedisAndApp } from 'testing/utils';
import { generateLocationTableId } from 'utils/locationTables';
import logger from 'utils/logger';
import { handle404, handle500 } from 'middlewares/error';
import { v4 as uuid } from 'uuid';
import { setupOperator } from 'testing/testData';
import moment from 'moment';
import { OperatorInstance } from 'database/models/operator';
import tablesRouter from './tables';

const operatorId = uuid();

let mockReturnHasLocationOwnership = true;

jest.mock('./tables.helpers.ts', () => ({
  ...jest.requireActual('./tables.helpers.ts'),
  hasLocationOwnership: () => mockReturnHasLocationOwnership,
  requireLocationTableOwnership: (
    request: Request,
    response: Response,
    next: NextFunction
  ) => next(),
}));

const getRequest = (): superTestRequest.SuperTest<superTestRequest.Test> =>
  superTestRequest(
    express()
      .use((request, response, next) => {
        request.user = {
          type: UserType.OPERATOR,
          uuid: operatorId,
        };
        request.log = logger;
        next();
      })
      .use('/locations', tablesRouter)
      .use(handle404)
      .use(handle500)
  );

let location: LocationInstance;
let locationTable: LocationTableInstance;
let operator: OperatorInstance;

describe('/locations/:id/tables', () => {
  let tableId: string;

  beforeAll(async () => {
    await setupDBRedisAndApp();
    tableId = await generateLocationTableId();
    operator = await setupOperator(operatorId);
    location = await Location.create({
      radius: 10,
      publicKey: 'fake public key',
      isIndoor: true,
    });
  });

  beforeEach(async () => {
    mockReturnHasLocationOwnership = true;
    locationTable = await LocationTables.create({
      locationId: location.uuid,
      id: tableId,
      customName: 'Some Name',
      internalNumber: 1,
    });
  });

  afterEach(async () => {
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    await LocationTables.truncate({ force: true });
    await operator.update({ bannedAt: null });
  });

  describe('when calling GET /locations/:id/tables as normal user', () => {
    it('returns the tables without their IDs', async () => {
      mockReturnHasLocationOwnership = false;
      const response = await getRequest()
        .get(`/locations/${location.uuid}/tables`)
        .send();

      expect(response.body).toMatchObject([{}]);
      expect('id' in response.body[0]).toBe(false);
    });
  });

  describe('when calling GET /locations/:id/tables as operator', () => {
    it('returns the tables including their IDs', async () => {
      const response = await getRequest()
        .get(`/locations/${location.uuid}/tables`)
        .send();
      expect(response.body).toMatchObject([{ id: locationTable.id }]);
    });

    // eslint-disable-next-line jest/expect-expect
    it('operator is banned', async () => {
      await operator.update({ bannedAt: moment().toDate() });

      await getRequest()
        .delete(`/locations/${location.uuid}/tables/1`)
        .send()
        .expect(404);
    });
  });
});
