import { Router } from 'express';
import status from 'http-status';
import config from 'config';
import { database, LocationTables } from 'database';
import {
  validateParametersSchema,
  validateSchema,
} from 'middlewares/validateSchema';
import { requireOperator, isUserOfType } from 'middlewares/requireUser';
import { ApiError, ApiErrorType } from 'utils/apiError';
import { generateLocationTableId } from 'utils/locationTables';
import { UserType } from 'constants/user';
import {
  createLocationTableSchema,
  locationParameterSchema,
  nestedLocationTableParameterSchema,
  updateLocationTableSchema,
} from './tables.schemas';
import {
  requireLocationOwnership,
  hasLocationOwnership,
} from './tables.helpers';

const maxTables = config.get('entity_limits.tables_per_location');

const router = Router();

router.get(
  '/:locationId/tables',
  validateParametersSchema(locationParameterSchema),
  async (request, response) => {
    const { locationId } = request.params;

    const tables = await LocationTables.findAll({
      where: {
        locationId,
      },
      order: [['internalNumber', 'ASC']],
    });

    const isOperatorOrOperatorDevice =
      isUserOfType(UserType.OPERATOR, request) ||
      isUserOfType(UserType.OPERATOR_DEVICE, request);

    const canIncludeId =
      isOperatorOrOperatorDevice &&
      (await hasLocationOwnership({
        locationId,
        operatorId: request.user.uuid,
      }));

    return response.send(
      tables.map(table => ({
        ...(canIncludeId && { id: table.id }),
        internalNumber: table.internalNumber,
        customName: table.customName,
      }))
    );
  }
);

router.post(
  '/:locationId/tables',
  requireOperator,
  requireLocationOwnership,
  validateParametersSchema(locationParameterSchema),
  validateSchema(createLocationTableSchema),
  async (request, response) => {
    const { locationId } = request.params;
    const { tables } = request.body;

    const currentCount = await LocationTables.count({
      where: {
        locationId,
      },
    });

    if (currentCount + tables.length > maxTables) {
      throw new ApiError(ApiErrorType.LIMIT_REACHED);
    }

    const hightest: number = await LocationTables.max('internalNumber', {
      where: {
        locationId,
      },
    });

    await database.transaction(async transaction => {
      for (const [index, value] of tables.entries()) {
        await LocationTables.create(
          {
            locationId,
            internalNumber: hightest + index + 1,
            customName: value.customName,
            id: await generateLocationTableId(),
          },
          { transaction }
        );
      }
    });

    return response.sendStatus(status.CREATED);
  }
);

router.put(
  '/:locationId/tables/:internalNumber',
  requireOperator,
  requireLocationOwnership,
  validateParametersSchema(nestedLocationTableParameterSchema),
  validateSchema(updateLocationTableSchema),
  async (request, response) => {
    const { locationId, internalNumber } = request.params;
    const { customName } = request.body;

    const [updatedRows] = await LocationTables.update(
      {
        customName,
      },

      {
        where: {
          locationId,
          internalNumber,
        },
      }
    );

    if (!updatedRows) {
      throw new ApiError(ApiErrorType.NOT_FOUND);
    }

    return response.sendStatus(status.NO_CONTENT);
  }
);

router.delete(
  '/:locationId/tables/:internalNumber',
  requireOperator,
  requireLocationOwnership,
  validateParametersSchema(nestedLocationTableParameterSchema),
  async (request, response) => {
    const { locationId, internalNumber } = request.params;

    const destroyedRows = await LocationTables.destroy({
      where: {
        locationId,
        internalNumber,
      },
    });

    if (!destroyedRows) {
      throw new ApiError(ApiErrorType.NOT_FOUND);
    }

    return response.sendStatus(status.NO_CONTENT);
  }
);

export default router;
