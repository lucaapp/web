import { Router } from 'express';
import status from 'http-status';
import { LocationMenu, Location } from 'database';
import {
  validateSchema,
  validateParametersSchema,
} from 'middlewares/validateSchema';
import { limitRequestsByUserPerHour } from 'middlewares/rateLimit';
import { requireOperator } from 'middlewares/requireUser';
import { ApiError, ApiErrorType } from 'utils/apiError';
import { updateMenuSchema, locationParameterSchema } from './menu.schemas';

const router = Router();

router.get(
  '/:locationId/menu',
  validateParametersSchema(locationParameterSchema),
  async (request, response) => {
    const { locationId } = request.params;

    const menu = await LocationMenu.findOne({
      where: {
        locationId,
      },
    });

    if (!menu) {
      throw new ApiError(ApiErrorType.MENU_NOT_FOUND);
    }

    return response.send(menu);
  }
);

router.put(
  '/:locationId/menu',
  limitRequestsByUserPerHour('location_menu_update_hour'),
  requireOperator,
  validateParametersSchema(locationParameterSchema),
  validateSchema(updateMenuSchema),
  async (request, response) => {
    const { uuid: operatorId } = request.user as IOperator;
    const { locationId } = request.params;
    const menu = request.body;

    const location = await Location.findOne({
      where: {
        uuid: locationId,
        operator: operatorId,
      },
    });

    if (!location) {
      throw new ApiError(ApiErrorType.LOCATION_NOT_FOUND);
    }

    await LocationMenu.upsert({
      locationId,
      menu,
    });

    return response.send(status.NO_CONTENT);
  }
);

export default router;
