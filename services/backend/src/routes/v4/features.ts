import { Router } from 'express';

import { FeatureRollout } from 'database';
import { limitRequestsPerHour } from 'middlewares/rateLimit';

const router = Router();

router.get(
  '/',
  limitRequestsPerHour('feature_rollouts_hour'),
  async (request, response) => {
    const features = await FeatureRollout.findAll({
      where: {
        active: true,
      },
      attributes: ['name', 'percentage'],
    });

    return response.send(features);
  }
);

export default router;
