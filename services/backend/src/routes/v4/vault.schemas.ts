import { z } from '../../utils/validation';

export const createSchema = z.object({
  content: z.object({
    version: z.number(),
    id: z.uuidv4(),
    encryptedPayload: z.base64({ min: 21 }), // 16 bytes * 4/3 base64 overhead
    expirationDate: z.unixTimestamp(),
  }),
  totpSecret: z.base64(),
  pow: z.object({
    id: z.uuid(),
    w: z.bigIntegerString(),
  }),
});

export const vaultContentIdParametersSchema = z.object({
  vaultContentId: z.uuidv4(),
});
