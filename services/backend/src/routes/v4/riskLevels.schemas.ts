import { z } from 'utils/validation';
import { RiskLevel } from 'constants/riskLevels';

const allowedLevels = {
  RISK_LEVEL_2: RiskLevel.RISK_LEVEL_2,
  RISK_LEVEL_3: RiskLevel.RISK_LEVEL_3,
};

export const getRiskLevelParameterSchema = z.object({
  locationTransferId: z.uuid(),
});

export const addRiskLevelTracesSchema = z.object({
  locationTransferId: z.uuid(),
  traceIds: z.array(z.traceId()),
  riskLevel: z.nativeEnum(allowedLevels),
});
