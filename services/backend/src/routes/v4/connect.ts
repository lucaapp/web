import { Router } from 'express';
import contactsRouter from './connect/contacts';
import conversationsRouter from './connect/conversations';
import messagesRouter from './connect/messages';
import searchRouter from './connect/search';

const router = Router();

router.use('/contacts', contactsRouter);
router.use('/conversations', conversationsRouter);
router.use('/messages', messagesRouter);
router.use('/search', searchRouter);

export default router;
