/* eslint-disable unicorn/consistent-function-scoping */
/* eslint-disable @typescript-eslint/no-explicit-any */
import express from 'express';
import trustListRouter from 'routes/v4/trustList';
import request from 'supertest';
import { etagMiddleware } from 'middlewares/etag';
import { setupDBRedisAndApp } from 'testing/utils';
import { httpLogger } from 'utils/logger';
import { handle500 } from 'middlewares/error';

const app = express();
app.disable('etag');
app.use(etagMiddleware);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(httpLogger);
app.use(handle500);
app.use('', trustListRouter);

jest.mock('../../middlewares/rateLimit', () => {
  const originalModule = jest.requireActual('../../middlewares/rateLimit');

  return {
    ...originalModule,
    limitRequestsPerDay: (_key: any) => (_: any, __: any, next: () => any) =>
      next(),
  };
});
const mockData = { certificates: ['cert1', 'cert2'] };
jest.mock('../../utils/urlCache', () => {
  const originalModule = jest.requireActual('../../utils/urlCache');

  return {
    ...originalModule,
    getCachedUrl: jest.fn(() => {
      return [mockData, 'some etag'];
    }),
  };
});

describe('trustlist router', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
  });
  it('should return a list of trusted dsc', async () => {
    const response = await request(app)
      .get('/dsc')
      .send()
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);
    expect(response.body).toMatchObject(mockData);
  });
});
