import express from 'express';
import request from 'supertest';
import moment from 'moment';
import timeRouter from './time';

const TIME_URL = '/time';

const app = express();
app.use(TIME_URL, timeRouter);

describe('Time router', () => {
  describe('GET time', () => {
    it('returns current time', async () => {
      const response = await request(app)
        .get(TIME_URL)
        .set('Accept', 'application/json')
        .expect(200);

      const diff = moment().unix() - response.body.unix;
      expect(diff).toBeLessThanOrEqual(60);
      expect(diff).toBeGreaterThanOrEqual(0);
    });
  });
});
