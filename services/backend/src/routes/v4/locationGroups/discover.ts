import { Router } from 'express';
import { validateParametersSchema } from 'middlewares/validateSchema';
import { Location, LocationGroup, LocationURL } from 'database';
import status from 'http-status';
import { UrlType } from 'database/models/locationUrl';
import { discoverIdParametersSchema } from './discover.schemas';

const router = Router();

router.get(
  '/discover/:discoverId',
  validateParametersSchema(discoverIdParametersSchema),
  async (request, response) => {
    const location = await Location.findOne({
      where: {
        name: null,
      },
      include: [
        {
          model: LocationGroup,
          where: {
            discoverId: request.params.discoverId,
            discoveryEnabled: true,
          },
          attributes: ['name'],
        },
      ],
    });

    if (!location) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const locationUrlList = await LocationURL.findAll({
      where: {
        locationId: location.uuid,
      },
      attributes: ['type', 'url'],
    });

    const locationUrls = Object.fromEntries(
      new Map(
        locationUrlList.map((url: { type: UrlType; url: string | null }) => [
          url.type,
          url.url,
        ])
      )
    );

    return response.send({
      name: location.LocationGroup?.name,
      lat: location.lat || 0,
      lng: location.lng || 0,
      openingHours: location.openingHours,
      locationType: location.type,
      address: `${location.streetName} ${location.streetNr}, ${location.zipCode} ${location.city}`,
      locationUrls,
    });
  }
);

export default router;
