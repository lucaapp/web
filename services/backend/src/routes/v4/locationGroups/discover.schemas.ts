import { z } from 'utils/validation';

export const discoverIdParametersSchema = z.object({
  discoverId: z.uuid(),
});
