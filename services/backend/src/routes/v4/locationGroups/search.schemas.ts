import { z } from 'utils/validation';
import { ZodNumber, ZodUndefined } from 'zod';

export const searchQuerySchema = z.object({
  limit: (z.integerString().optional() as unknown) as ZodNumber | ZodUndefined,
  offset: (z.integerString().optional() as unknown) as ZodNumber | ZodUndefined,
  lat: (z.floatString().optional() as unknown) as ZodNumber | ZodUndefined,
  lng: (z.floatString().optional() as unknown) as ZodNumber | ZodUndefined,
  term: z.safeString().max(255).optional(),
  filter: z.safeString().max(255),
});
export const autocompleteQuerySchema = z.object({
  term: z.safeString().min(3).max(255),
});
export const placesParametersSchema = z.object({
  placeId: z.safeString(),
});
