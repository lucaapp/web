import { Request, Response, Router } from 'express';
import { Op, literal, fn, col, FindAttributeOptions } from 'sequelize';
import {
  Client,
  PlaceAutocompleteType,
} from '@googlemaps/google-maps-services-js';
import { proxyClient } from 'utils/proxy';
import { randomUUID } from 'crypto';
import config from 'config';
import { NOT_FOUND } from 'http-status';

import { Location, LocationGroup } from 'database';
import { LocationInstance } from 'database/models/location';

import {
  validateParametersSchema,
  validateQuerySchema,
} from 'middlewares/validateSchema';

import { limitRequestsByUserPerMinute } from 'middlewares/rateLimit';
import { validateGoogleApiKey } from 'middlewares/validateGoogleApiKey';
import {
  searchQuerySchema,
  autocompleteQuerySchema,
  placesParametersSchema,
} from './search.schemas';

const router = Router();
const MAX_LIMIT = 100;
const DEFAULT_LIMIT = 100;
const client = new Client({ axiosInstance: proxyClient });

router.get(
  '/search',
  limitRequestsByUserPerMinute('location_group_query_ratelimit_minute'),
  validateQuerySchema(searchQuerySchema),
  async (request, response) => {
    const limit = Math.min(request.query.limit || DEFAULT_LIMIT, MAX_LIMIT);
    const offset = request.query.offset || 0;
    const { term, lat, lng, filter } = request.query;

    const orderByDistance = lat && lng;

    const where: { type?: object; name: null } = {
      name: null,
    };

    if (filter !== 'all') {
      where.type = { [Op.eq]: filter };
    }

    const distance = fn(
      'st_distance',
      col('geometricPoint'),
      literal(`st_geogfromtext('SRID=4326;POINT(:lng :lat)')`)
    );

    const attributes: FindAttributeOptions = [
      'openingHours',
      'streetName',
      'streetNr',
      'zipCode',
      'city',
      'lat',
      'lng',
      'type',
    ];

    if (orderByDistance) {
      attributes.push([distance, 'distance']);
    }

    const locations: {
      rows: LocationInstance[];
      count: number;
    } = await Location.findAndCountAll({
      where,
      attributes,
      replacements: { lng, lat },
      include: [
        {
          model: LocationGroup,
          attributes: ['discoverId', 'name'],
          where: {
            name: {
              [Op.iLike]: `%${term}%`,
            },
            discoveryEnabled: true,
          },
        },
      ],
      order: !orderByDistance ? undefined : [[literal('distance'), 'ASC']],
      offset,
      limit,
    });
    return response.send({
      total: locations.count,
      offset,
      limit,
      locations: locations.rows.map((location: LocationInstance) => ({
        discoverId: location.LocationGroup?.discoverId,
        groupName: location.LocationGroup?.name,
        openingHours: location.openingHours,
        streetName: location.streetName,
        streetNr: location.streetNr,
        zipCode: location.zipCode,
        city: location.city,
        lat: location.lat,
        lng: location.lng,
        type: location.type,
        // @ts-ignore distance is not defined as attribute in model
        distance: location.getDataValue('distance'),
      })),
    });
  }
);

const SESSIONTOKEN_COOKIE = 'autocomplete_sessiontoken';
const getOrCreateSessionToken = (
  request: Request<unknown, unknown, unknown, unknown>,
  response: Response
) => {
  // eslint-disable-next-line security/detect-object-injection
  const currentSession = request.cookies[SESSIONTOKEN_COOKIE] as string;
  if (currentSession) {
    return currentSession;
  }
  const sessionToken = randomUUID();
  response.cookie(SESSIONTOKEN_COOKIE, sessionToken, {
    maxAge: 900000,
    httpOnly: true,
  });

  return sessionToken;
};

router.get(
  '/placeAutocomplete',
  limitRequestsByUserPerMinute('location_group_query_ratelimit_minute'),
  validateQuerySchema(autocompleteQuerySchema),
  validateGoogleApiKey,
  async (request, response) => {
    const input = request.query.term;
    const sessiontoken = getOrCreateSessionToken(request, response);

    const autocomplete = await client.placeAutocomplete({
      params: {
        input,
        sessiontoken,
        types: PlaceAutocompleteType.address,
        key: config.get('google.api.key'),
        components: ['country:de'],
      },
    });

    response.send({
      predictions: autocomplete.data.predictions.map(prediction => ({
        place_id: prediction.place_id,
        description: prediction.description,
      })),
    });
  }
);

router.get(
  '/places/:placeId',
  limitRequestsByUserPerMinute('location_group_query_ratelimit_minute'),
  validateParametersSchema(placesParametersSchema),
  async (request, response) => {
    const sessiontoken = getOrCreateSessionToken(request, response);
    const { placeId } = request.params;

    const place = await client.placeDetails({
      params: {
        place_id: placeId,
        sessiontoken,
        key: config.get('google.api.key'),
        fields: ['name', 'geometry', 'formatted_address'],
      },
    });
    const placeResult = place.data.result;
    if (!placeResult) {
      response.sendStatus(NOT_FOUND);
      return;
    }
    response.send({
      lat: placeResult.geometry?.location.lat,
      lng: placeResult.geometry?.location.lng,
      name: placeResult.name,
      formattedAddress: placeResult.formatted_address,
    });
  }
);

export default router;
