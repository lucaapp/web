import { Router } from 'express';
import moment from 'moment';
import { Op } from 'sequelize';

import {
  LocationTransfer,
  HealthDepartment,
  SeenOverlappingLocationTranfers,
} from 'database';
import { validateParametersSchema } from 'middlewares/validateSchema';
import { requireHealthDepartmentEmployee } from 'middlewares/requireUser';
import { ApiError, ApiErrorType } from 'utils/apiError';

import { transferIdParametersSchema } from './associated.schemas';

const router = Router();

router.get(
  '/:transferId/view',
  requireHealthDepartmentEmployee,
  validateParametersSchema(transferIdParametersSchema),
  async (request, response) => {
    const user = request.user as IHealthDepartmentEmployee;

    const locationTransfer = await LocationTransfer.findOne({
      where: {
        uuid: request.params.transferId,
        departmentId: user.departmentId,
      },
    });

    if (!locationTransfer)
      throw new ApiError(ApiErrorType.LOCATION_TRANSFER_NOT_FOUND);

    const associatedLocationTransfers = await LocationTransfer.findAll({
      where: {
        locationId: locationTransfer.locationId,
        departmentId: { [Op.not]: user.departmentId },
        contactedAt: { [Op.not]: null },
        time: { [Op.overlap]: locationTransfer.time as [Date, Date] },
      },
      include: {
        attributes: ['name'],
        model: HealthDepartment,
        required: true,
      },
    });

    const recievedEntries = associatedLocationTransfers.map(
      associatedLocationTransfer => ({
        departmentId: user.departmentId,
        requestingDepartmentId: associatedLocationTransfer.departmentId,
        locationTransferId: associatedLocationTransfer.uuid,
      })
    );
    await SeenOverlappingLocationTranfers.bulkCreate(recievedEntries, {
      ignoreDuplicates: true,
    });

    const associatedLocationTransfersDTO = associatedLocationTransfers.map(
      associatedLocationTransfer => ({
        uuid: associatedLocationTransfer.uuid,
        departmentId: associatedLocationTransfer.departmentId,
        departmentName: associatedLocationTransfer.HealthDepartment?.name,
        groupName: associatedLocationTransfer.Location?.LocationGroup?.name,
        time: [
          moment(associatedLocationTransfer?.time[0].value).unix(),
          moment(associatedLocationTransfer?.time[1].value).unix(),
        ],
        contactedAt: moment(associatedLocationTransfer.contactedAt).unix(),
      })
    );

    response.send(associatedLocationTransfersDTO);
  }
);

router.get(
  '/:transferId',
  requireHealthDepartmentEmployee,
  validateParametersSchema(transferIdParametersSchema),
  async (request, response) => {
    const user = request.user as IHealthDepartmentEmployee;

    const locationTransfer = await LocationTransfer.findOne({
      where: {
        uuid: request.params.transferId,
        departmentId: user.departmentId,
      },
    });

    if (!locationTransfer)
      throw new ApiError(ApiErrorType.LOCATION_TRANSFER_NOT_FOUND);

    const associatedLocationTransfers = await LocationTransfer.findAll({
      where: {
        locationId: locationTransfer.locationId,
        departmentId: { [Op.not]: user.departmentId },
        contactedAt: { [Op.not]: null },
        time: { [Op.overlap]: locationTransfer.time as [Date, Date] },
      },
      include: [
        {
          attributes: ['name'],
          model: HealthDepartment,
          required: true,
        },
        {
          model: SeenOverlappingLocationTranfers,
          where: { departmentId: user.departmentId },
          required: false,
        },
      ],
    });

    response.send({
      available: associatedLocationTransfers.length,
      unread: associatedLocationTransfers.filter(
        associatedLocationTransfer =>
          !associatedLocationTransfer.SeenOverlappingLocationTranfers ||
          associatedLocationTransfer.SeenOverlappingLocationTranfers.length ===
            0
      ).length,
    });
  }
);

export default router;
