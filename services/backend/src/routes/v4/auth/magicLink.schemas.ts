import { z } from 'utils/validation';
import { campaignBodySchema } from 'routes/v4/campaigns.schemas';

export const emailSchema = z.object({
  email: z.email(),
  campaign: campaignBodySchema.optional().nullable(),
});

export const tokenSchema = z.object({
  token: z.base64({ rawLength: 32, isBase64Url: true }),
});
