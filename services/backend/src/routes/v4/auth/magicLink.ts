import { Router } from 'express';
import config from 'config';
import cors from 'cors';
import status from 'http-status';
import passport from 'passport';
import queryString from 'query-string';

import { Operator, EmailLoginToken, PotentialOperator } from 'database';
import { ApiError, ApiErrorType } from 'utils/apiError';

import { validateSchema } from 'middlewares/validateSchema';
import { restrictOrigin } from 'middlewares/restrictOrigin';
import { limitRequestsPerMinute } from 'middlewares/rateLimit';
import { randomSleep } from 'utils/sleep';
import {
  getPreferredLanguageFromRequest,
  getTransactionalLanguage,
} from 'utils/language';
import { generateEmailLoginToken } from 'utils/generators';
import {
  sendMagicLinkEmail,
  sendMagicLinkRegisterEmail,
} from 'utils/mailClient';

import { tokenSchema, emailSchema } from './magicLink.schemas';

const HOSTNAME = config.get('hostname');

const corsOptions = {
  origin: [`https://${HOSTNAME}`, 'https://pay.luca-app.de'],
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) have a problem with 204
};

const router = Router();

router.use(cors(corsOptions));

type Campaign = {
  utm_id?: string;
  utm_source: string;
  utm_medium: string;
  utm_campaign?: string;
};

const generateMagicLoginLink = (
  token: string,
  campaign: Campaign | undefined | null
) => {
  if (!campaign) {
    return `https://${HOSTNAME}/magicLogin#${token}`;
  }
  return `https://${HOSTNAME}/magicLogin?${queryString.stringify(
    campaign
  )}#${token}`;
};

const generateRegisterLink = (
  campaign: Campaign | undefined | null,
  email: string
) => {
  if (!campaign) {
    return `https://${HOSTNAME}/register?${queryString.stringify({ email })}`;
  }
  return `https://${HOSTNAME}/register?${queryString.stringify({
    ...campaign,
    email,
  })}`;
};

router.post(
  '/',
  limitRequestsPerMinute('auth_login_post_ratelimit_minute', {
    skipSuccessfulRequests: false,
    global: false,
  }),
  validateSchema(emailSchema),
  async (request, response) => {
    await randomSleep();
    const { email, campaign } = request.body;
    const operator = await Operator.findOne({
      where: {
        username: email,
        activated: true,
      },
    });

    if (operator) {
      const token = await EmailLoginToken.create({
        token: await generateEmailLoginToken(),
        operatorId: operator.uuid,
      });

      sendMagicLinkEmail(
        operator.email,
        operator.fullName,
        operator.transactionalLanguage,
        {
          magicLoginLink: generateMagicLoginLink(token.token, campaign),
        }
      );
    } else {
      const locale = getPreferredLanguageFromRequest(request);
      const language = getTransactionalLanguage(locale);
      sendMagicLinkRegisterEmail(email, email, language, {
        emailRegisterLink: generateRegisterLink(campaign, email),
      });
      await PotentialOperator.upsert({
        email,
        utm_id: campaign?.utm_id,
        utm_source: campaign?.utm_source,
        utm_medium: campaign?.utm_medium,
        utm_campaign: campaign?.utm_campaign,
      });
    }
    response.sendStatus(status.NO_CONTENT);
  }
);

router.post(
  '/login',
  limitRequestsPerMinute('auth_login_post_ratelimit_minute', {
    skipSuccessfulRequests: true,
    global: false,
  }),
  restrictOrigin,
  validateSchema(tokenSchema),
  passport.authenticate('emailLoginToken', { session: false }),
  async (request, response) => {
    request.logIn(request.user, loginError => {
      if (loginError) {
        throw new ApiError(ApiErrorType.UNAUTHORIZED);
      }
      return response.sendStatus(status.NO_CONTENT);
    });
  }
);

export default router;
