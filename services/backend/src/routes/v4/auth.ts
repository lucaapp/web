import { Router } from 'express';
import status from 'http-status';
import passport from 'passport';
import moment from 'moment';
import config from 'config';
import jwt from 'jsonwebtoken';

import { OperatorDevice } from 'database';

import { logEvent } from 'utils/hdAuditLog';
import { ApiError, ApiErrorType } from 'utils/apiError';
import { createJWT, logoutCurrentUser } from 'utils/authentication';

import { validateSchema } from 'middlewares/validateSchema';
import { restrictOrigin } from 'middlewares/restrictOrigin';
import { handle500 } from 'middlewares/error';
import { limitRequestsPerMinute } from 'middlewares/rateLimit';
import * as jwkStore from 'utils/jwkStore';

import {
  isUserOfType,
  requireOperatorOROperatorDevice,
  requireOperatorDevice,
  requireOperator,
  requireHealthDepartmentEmployee,
  requireNonDeletedUser,
} from 'middlewares/requireUser';

import { AuditLogEvents, AuditStatusType } from 'constants/auditLog';

import { UserType } from 'constants/user';

import { formatPhoneNumber } from 'utils/format';
import { getOperatorDeviceDTO } from './operatorDevices.helper';
import { authSchema, authOperatorDeviceSchema } from './auth.schemas';
import magicLinkRouter from './auth/magicLink';

const router = Router();
router.use('/magicLink', magicLinkRouter);

// Certificate check is done in Load Balancer
router.post(
  '/healthDepartmentEmployee/login',
  limitRequestsPerMinute('auth_hd_login_post_ratelimit_minute', {
    skipSuccessfulRequests: true,
    global: false,
  }),
  restrictOrigin,
  validateSchema(authSchema),
  passport.authenticate('local-healthDepartmentEmployee'),
  (request, response) => {
    response.sendStatus(status.NO_CONTENT);
  }
);

router.post(
  '/operator/login',
  limitRequestsPerMinute('auth_login_post_ratelimit_minute', {
    skipSuccessfulRequests: true,
    global: false,
  }),
  restrictOrigin,
  validateSchema(authSchema),
  (request, response) =>
    passport.authenticate('local-operator', {}, (error, user) => {
      try {
        if (
          error &&
          (error.errorType === 'UNACTIVATED' ||
            error.errorType === 'USER_BANNED')
        ) {
          throw new ApiError(ApiErrorType.FORBIDDEN);
        }
        if (!user || error) {
          throw new ApiError(ApiErrorType.UNAUTHORIZED);
        }

        request.logIn(user, loginError => {
          if (loginError) {
            throw new ApiError(ApiErrorType.UNAUTHORIZED);
          }
          return response.sendStatus(status.NO_CONTENT);
        });
      } catch (authorizationError) {
        handle500(authorizationError, request, response);
      }
    })(request, response)
);

router.get(
  '/operatorDevice/me',
  requireOperatorDevice,
  async (request, response) => {
    return response.send({
      ...getOperatorDeviceDTO(request.user.device),
      operatorId: request.user.uuid,
      username: request.user.username,
      firstName: request.user.firstName,
      lastName: request.user.lastName,
      publicKey: request.user.publicKey,
      supportCode: request.user.supportCode,
      email: request.user.email,
      avvAccepted: request.user.avvAccepted,
      lastVersionSeen: request.user.lastVersionSeen,
      deletedAt:
        request.user.deletedAt && moment(request.user.deletedAt).unix(),
      isTrusted: request.user.isTrusted,
      language: request.user.language,
      languageOverwrite: request.user.languageOverwrite,
    });
  }
);

// /healthDepartmentEmployee/me

router.get(
  '/healthDepartmentEmployee/me',
  requireHealthDepartmentEmployee,
  (request, response) => {
    return response.send({
      employeeId: request.user.uuid,
      firstName: request.user.firstName,
      lastName: request.user.lastName,
      email: request.user.email,
      departmentId: request.user.departmentId,
      isAdmin: request.user.isAdmin,
      isSigned: !!request.user.HealthDepartment.signedPublicHDSKP,
      notificationsEnabled: request.user.HealthDepartment.notificationsEnabled,
      connectEnabled: request.user.HealthDepartment.connectEnabled,
    });
  }
);

router.get('/operator/me', requireOperator, async (request, response) => {
  const payload = {
    operatorId: request.user.uuid,
    username: request.user.username,
    firstName: request.user.firstName,
    lastName: request.user.lastName,
    phone: formatPhoneNumber(request.user.phone),
    businessEntityName: request.user.businessEntityName,
    businessEntityStreetName: request.user.businessEntityStreetName,
    businessEntityStreetNumber: request.user.businessEntityStreetNumber,
    businessEntityZipCode: request.user.businessEntityZipCode,
    businessEntityCity: request.user.businessEntityCity,
    publicKey: request.user.publicKey,
    supportCode: request.user.supportCode,
    email: request.user.email,
    avvAccepted: request.user.avvAccepted,
    lastVersionSeen: request.user.lastVersionSeen,
    createdAt: moment(request.user.createdAt).unix(),
    deletedAt: request.user.deletedAt && moment(request.user.deletedAt).unix(),
    isTrusted: request.user.isTrusted,
    lastSeenPrivateKey: request.user.lastSeenPrivateKey,
    language: request.user.language,
    languageOverwrite: request.user.languageOverwrite,
    contactTracingEnabled: request.user.publicKey != null,
  };
  return response.send(payload);
});

router.post(
  '/operatorDevice/login',
  limitRequestsPerMinute('auth_operatordevice_login_post_ratelimit_minute', {
    skipSuccessfulRequests: true,
    global: false,
  }),
  validateSchema(authOperatorDeviceSchema),
  (request, response) => {
    return passport.authenticate(
      'jwt-operatorDevice',
      { session: false },
      async (error, user) => {
        try {
          if (error) {
            if (error.errorType === 'DEVICE_EXPIRED') {
              throw new ApiError(ApiErrorType.DEVICE_EXPIRED);
            }

            throw new ApiError(ApiErrorType.UNAUTHORIZED);
          }

          if (!user) {
            throw new ApiError(ApiErrorType.UNAUTHORIZED);
          }

          if (!user.activated) {
            throw new ApiError(ApiErrorType.FORBIDDEN);
          }
          const device = await OperatorDevice.findByPk(user.deviceId);
          if (!device) {
            throw new ApiError(ApiErrorType.FORBIDDEN);
          }
          await device.update({
            reactivatedAt: null,
            refreshedAt: moment(),
            refreshedAtBefore: device.refreshedAt,
          });

          const refreshToken = jwt.sign(
            { deviceId: user.deviceId },
            config.get('keys.operatorDevice.privateKey'),
            {
              algorithm: 'ES256',
              expiresIn: config.get('keys.operatorDevice.expire'),
            }
          );

          request.logIn(user, loginError => {
            if (loginError) {
              throw new ApiError(ApiErrorType.UNAUTHORIZED);
            }
            return response.send({ refreshToken });
          });
        } catch (authorizationError) {
          handle500(authorizationError, request, response);
        }
      }
    )(request, response);
  }
);

router.post(
  '/operatorDevice/logout',
  requireOperatorDevice,
  async (request, response) => {
    await request.user.device.destroy();
    request.logout();
    response.clearCookie('connect.sid');
    response.sendStatus(status.NO_CONTENT);
  }
);

router.post('/logout', restrictOrigin, async (request, response) => {
  const { user } = request;
  const isHDUser = isUserOfType(UserType.HEALTH_DEPARTMENT_EMPLOYEE, request);

  try {
    await logoutCurrentUser(request, response);

    response.sendStatus(status.NO_CONTENT);

    if (isHDUser) {
      logEvent(user, {
        type: AuditLogEvents.LOGOUT,
        status: AuditStatusType.SUCCESS,
      });
    }
  } catch (error) {
    if (isHDUser) {
      logEvent(user, {
        type: AuditLogEvents.LOGOUT,
        status: AuditStatusType.ERROR_UNKNOWN_SERVER_ERROR,
      });
    }

    throw error;
  }
});

router.get(
  '/jwt',
  requireOperatorOROperatorDevice,
  requireNonDeletedUser,
  async (request, response) => {
    return response.send(await createJWT(request));
  }
);

router.get('/jwks', async (request, response) => {
  const jwks = await jwkStore.getPublicKeys();
  request.log.info(
    {
      keyIdentifiers: jwks.keys.map(({ kid }) => kid),
    },
    `Sending JWKS`
  );
  response.send(jwks);
});

export default router;
