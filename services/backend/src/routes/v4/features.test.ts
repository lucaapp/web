/* eslint-disable unicorn/consistent-function-scoping */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-shadow */
import express from 'express';
import request from 'supertest';
import { setupDBRedisAndApp } from 'testing/utils';

import 'express-async-errors';
import featuresRouter from './features';
import { FeatureRollout } from '../../database';

const app = express();
app.disable('etag');
app.use('', featuresRouter);

jest.mock('../../middlewares/rateLimit', () => {
  const originalModule = jest.requireActual('../../middlewares/rateLimit');

  return {
    ...originalModule,
    limitRequestsPerHour: (_key: unknown) => (
      _: unknown,
      __: unknown,
      next: () => unknown
    ) => next(),
  };
});

const createFeatureRollout = async (
  name: string,
  percentage: number,
  active: boolean
) =>
  FeatureRollout.create({
    name,
    percentage,
    active,
  });

describe('features router', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
  });
  afterEach(() => {
    FeatureRollout.truncate();
  });

  describe('GET route', () => {
    it('should return empty list if no features are rolled out', async () => {
      const response = await request(app)
        .get('/')
        .send()
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200);
      expect(response.body).toHaveLength(0);
    });
    it('should return single element if single active feature is rolled out', async () => {
      await createFeatureRollout('foo', 0.5, true);
      const response = await request(app)
        .get('/')
        .send()
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200);
      expect(response.body).toHaveLength(1);
      expect(response.body).toHaveProperty('[0].name', 'foo');
      expect(response.body).toHaveProperty('[0].percentage', 0.5);
    });
    it('should return single element when mixing active and in-active features', async () => {
      await createFeatureRollout('foo', 0.5, true);
      await createFeatureRollout('bar', 0.3, false);
      const response = await request(app)
        .get('/')
        .send()
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200);
      expect(response.body).toHaveLength(1);
      expect(response.body).toHaveProperty('[0].name', 'foo');
      expect(response.body).toHaveProperty('[0].percentage', 0.5);
    });
    it('should return empty list if single in-active feature is rolled out', async () => {
      await createFeatureRollout('foo', 0.5, false);
      const response = await request(app)
        .get('/')
        .send()
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200);
      expect(response.body).toHaveLength(0);
    });
    it('should return multiple elements if multiple active features are rolled out', async () => {
      await createFeatureRollout('foo', 0.5, true);
      await createFeatureRollout('bar', 0.3, true);
      const response = await request(app)
        .get('/')
        .send()
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200);
      expect(response.body).toHaveLength(2);
      expect(response.body).toHaveProperty('[0].name', 'foo');
      expect(response.body).toHaveProperty('[0].percentage', 0.5);
      expect(response.body).toHaveProperty('[1].name', 'bar');
      expect(response.body).toHaveProperty('[1].percentage', 0.3);
    });
  });
});
