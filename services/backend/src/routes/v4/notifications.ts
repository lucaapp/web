import { Router } from 'express';
import status from 'http-status';
import { hexToBase64 } from '@lucaapp/crypto';
import {
  getActiveChunk,
  getArchivedChunk,
} from 'utils/notifications/notificationsV4';
import { getNotificationConfig } from 'utils/notifications/notificationsConfig';
import {
  validateParametersSchema,
  validateSchema,
} from 'middlewares/validateSchema';
import { limitRequestsPerHour } from 'middlewares/rateLimit';
import { NotificationConfig } from 'database';
import { requireHealthDepartmentAdmin } from 'middlewares/requireUser';

import {
  chunkIdParametersSchema,
  hexChunkIdParametersSchema,
  notificationConfigPutSchema,
} from './notifications.schemas';

const router = Router();

const normalizeBase64 = (chunkId: string): string => {
  let normalizedChunkId = chunkId;
  while (normalizedChunkId.length < 24) {
    normalizedChunkId = `/${normalizedChunkId}`;
  }
  return normalizedChunkId;
};

/**
 * Provides hashed trace IDs allowing users to be notified if their data was accessed by a health department.
 * @see https://www.luca-app.de/securityoverview/processes/tracing_find_contacts.html#notifying-guests-about-data-access
 */
router.get(
  '/traces',
  limitRequestsPerHour(
    'notifications_v4_traces_active_chunk_get_ratelimit_hour'
  ),
  async (request, response) => {
    const notificationChunk = await getActiveChunk();

    if (notificationChunk) {
      return response.send(notificationChunk);
    }
    return response.send(status.NOT_FOUND);
  }
);

router.get(
  '/traces/hex/:chunkId',
  limitRequestsPerHour(
    'notifications_v4_traces_archived_chunk_get_ratelimit_hour'
  ),
  validateParametersSchema(hexChunkIdParametersSchema),
  async (request, response) => {
    const notificationChunk = await getArchivedChunk(
      hexToBase64(request.params.chunkId)
    );
    if (notificationChunk) {
      response.addEtag(notificationChunk);
      return response.send(notificationChunk);
    }
    return response.send(status.NOT_FOUND);
  }
);

router.get(
  '/traces/:chunkId(*)',
  limitRequestsPerHour(
    'notifications_v4_traces_archived_chunk_get_ratelimit_hour'
  ),
  validateParametersSchema(chunkIdParametersSchema),
  async (request, response) => {
    const notificationChunk = await getArchivedChunk(
      normalizeBase64(request.params.chunkId)
    );
    if (notificationChunk) {
      response.addEtag(notificationChunk);
      return response.send(notificationChunk);
    }
    return response.send(status.NOT_FOUND);
  }
);

router.get(
  '/config',
  limitRequestsPerHour('notifications_v4_config_get_ratelimit_hour'),
  async (request, response) => {
    const notificationConfig = await getNotificationConfig();
    if (notificationConfig) {
      response.addEtag(notificationConfig);
      return response.send(notificationConfig);
    }
    return response.send(status.NOT_FOUND);
  }
);

router.put(
  '/config',
  requireHealthDepartmentAdmin,
  validateSchema(notificationConfigPutSchema),
  async (request, response) => {
    const keys = ['showMail', 'showPhone', 'infoUrl'] as const;
    const promises = keys.map(async key => {
      if (request.body[`${key}`] === undefined) return null;
      const existingKeyValuePair = await NotificationConfig.findOne({
        where: {
          key,
          level: request.body.level,
          departmentId: request.user.departmentId,
        },
      });

      if (existingKeyValuePair)
        return existingKeyValuePair.update({
          value: JSON.stringify(request.body[`${key}`]),
        });

      return NotificationConfig.create({
        key,
        level: request.body.level,
        departmentId: request.user.departmentId,
        value: JSON.stringify(request.body[`${key}`]),
      });
    });

    await Promise.all(promises);

    return response.sendStatus(status.NO_CONTENT);
  }
);

export default router;
