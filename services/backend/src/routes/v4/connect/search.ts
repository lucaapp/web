import { Router } from 'express';
import moment from 'moment';
import status from 'http-status';
import { PrefixTableMapping, PrefixType } from 'constants/prefixType';
import {
  validateSchema,
  validateParametersSchema,
} from 'middlewares/validateSchema';
import { requireHealthDepartmentEmployee } from 'middlewares/requireUser';
import { limitRequestsPerDay } from 'middlewares/rateLimit';

import {
  database,
  ConnectSearchProcess,
  ConnectSearch,
  ConnectContact,
} from 'database';

import { logEvent } from 'utils/hdAuditLog';
import { AuditLogEvents, AuditStatusType } from 'constants/auditLog';
import {
  searchSchema,
  processIdParameterSchema,
  searchIdParameterSchema,
} from './search.schemas';
import { requireConnectEnabled } from './connect.helper';

const router = Router();

router.use(requireHealthDepartmentEmployee);
router.use(requireConnectEnabled);

router.post(
  '/',
  limitRequestsPerDay('searchConnect_per_day'),
  validateSchema(searchSchema),
  async (request, response) => {
    await database.transaction(async transaction => {
      const process = await ConnectSearchProcess.create(
        {
          departmentId: request.user.departmentId,
        },
        { transaction }
      );

      const { search } = request.body;
      const numbersSearched = search.filter(s => s.type === PrefixType.PHONE)
        .length;
      const namesSearched = search.filter(s => s.type === PrefixType.NAME)
        .length;

      logEvent(request.user, {
        type: AuditLogEvents.CONNECT_SEARCH,
        status: AuditStatusType.SUCCESS,
        meta: {
          searchId: process.uuid,
          namesSearched,
          numbersSearched,
        },
      });

      await ConnectSearch.bulkCreate(
        search.map(searchParameter => ({
          processId: process.uuid,
          prefix: searchParameter.prefix,
          type: searchParameter.type,
          data: searchParameter.term.data,
          publicKey: searchParameter.term.publicKey,
          iv: searchParameter.term.iv,
          mac: searchParameter.term.mac,
        })),
        { transaction, returning: false }
      );
      return response.send({ searchProcessId: process.uuid });
    });
  }
);

router.get('/processes', async (request, response) => {
  const processes = await ConnectSearchProcess.findAll({
    attributes: ['uuid', 'createdAt'],
    where: {
      departmentId: request.user.departmentId,
    },
    include: {
      required: true,
      model: ConnectSearch,
      attributes: ['uuid', 'completedAt'],
    },
  });

  return response.send(
    processes.map(process => ({
      uuid: process.uuid,
      createdAt: moment(process.createdAt).unix(),
      length: process.ConnectSearches.length,
      completed: process.ConnectSearches.filter(search => !!search.completedAt)
        .length,
    }))
  );
});

router.get(
  '/processes/:processId',
  validateParametersSchema(processIdParameterSchema),
  async (request, response) => {
    const process = await ConnectSearchProcess.findOne({
      where: {
        departmentId: request.user.departmentId,
        uuid: request.params.processId,
      },
      include: {
        model: ConnectSearch,
      },
    });
    if (!process) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const availableMap = new Map<string, boolean>();
    for (const search of process.ConnectSearches) {
      if (search.completedAt) {
        availableMap.set(search.uuid, false);
      } else {
        const contacts = await ConnectContact.findAll({
          where: {
            [PrefixTableMapping[search.type]]: search.prefix,
            departmentId: request.user.departmentId,
          },
        });
        availableMap.set(search.uuid, contacts.length > 0);
      }
    }

    return response.send({
      uuid: process.uuid,
      createdAt: moment(process.createdAt).unix(),
      searches: process.ConnectSearches.map(search => ({
        uuid: search.uuid,
        prefix: search.prefix,
        type: search.type,
        term: {
          data: search.data,
          iv: search.iv,
          mac: search.mac,
          publicKey: search.publicKey,
        },
        done: !!search.completedAt,
        available: availableMap.get(search.uuid),
      })),
    });
  }
);

router.get(
  '/:searchId',
  validateParametersSchema(searchIdParameterSchema),
  async (request, response) => {
    const search = await ConnectSearch.findOne({
      attributes: ['uuid', 'prefix', 'type'],
      where: {
        uuid: request.params.searchId,
      },
      include: [
        {
          model: ConnectSearchProcess,
          attributes: ['departmentId'],
          where: {
            departmentId: request.user.departmentId,
          },
        },
      ],
    });
    if (!search) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const contacts = await ConnectContact.findAll({
      where: {
        [PrefixTableMapping[search.type]]: search.prefix,
        departmentId: request.user.departmentId,
      },
    });

    return response.send(
      contacts.map(contact => ({
        contactId: contact.uuid,
        reference: {
          data: contact.referenceData,
          iv: contact.referenceIV,
          mac: contact.referenceMAC,
          publicKey: contact.referencePublicKey,
        },
        data: {
          data: contact.dataData,
          iv: contact.dataIV,
          mac: contact.dataMAC,
          publicKey: contact.dataPublicKey,
        },
      }))
    );
  }
);

export default router;
