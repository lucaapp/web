import { Router } from 'express';
import status from 'http-status';
import { validateSchema } from 'middlewares/validateSchema';
import { limitRequestsPerHour } from 'middlewares/rateLimit';
import { requireNonBlockedIp } from 'middlewares/requireNonBlockedIp';
import { requirePow } from 'middlewares/requirePow';

import { base64ToHex, VERIFY_EC_SHA256_DER_SIGNATURE } from '@lucaapp/crypto';

import { concatSha256 } from 'utils/hash';
import { HealthDepartment, ConnectContact } from 'database';

import { createSchema, deleteSchema } from './contacts.schemas';

const router = Router();

router.post(
  '/',
  requireNonBlockedIp,
  limitRequestsPerHour('createConnectContact_per_hour'),
  validateSchema(createSchema),
  requirePow('createConnectContact'),
  async (request, response) => {
    const department = await HealthDepartment.findOne({
      where: { uuid: request.body.departmentId, connectEnabled: true },
    });
    if (!department) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const isValidSignature = VERIFY_EC_SHA256_DER_SIGNATURE(
      base64ToHex(request.body.authPublicKey),
      concatSha256([
        'CREATE_CONNECTCONTACT',
        request.body.departmentId,
        request.body.namePrefix,
        request.body.phonePrefix,
        request.body.reference.data,
        request.body.reference.publicKey,
        request.body.reference.iv,
        request.body.reference.mac,
        request.body.data.data,
        request.body.data.publicKey,
        request.body.data.iv,
        request.body.data.mac,
      ]),
      base64ToHex(request.body.signature)
    );

    if (!isValidSignature) {
      return response.sendStatus(status.FORBIDDEN);
    }

    let contact;
    try {
      contact = await ConnectContact.create({
        departmentId: request.body.departmentId,
        namePrefix: request.body.namePrefix,
        phonePrefix: request.body.phonePrefix,
        authPublicKey: request.body.authPublicKey,
        referenceData: request.body.reference.data,
        referencePublicKey: request.body.reference.publicKey,
        referenceIV: request.body.reference.iv,
        referenceMAC: request.body.reference.mac,
        dataData: request.body.data.data,
        dataPublicKey: request.body.data.publicKey,
        dataIV: request.body.data.iv,
        dataMAC: request.body.data.mac,
        signature: request.body.signature,
      });
    } catch {
      return response.sendStatus(status.CONFLICT);
    }

    return response.send({
      contactId: contact.uuid,
    });
  }
);

router.delete(
  '/',
  requireNonBlockedIp,
  limitRequestsPerHour('deleteConnectContact_per_hour'),
  validateSchema(deleteSchema),
  async (request, response) => {
    const contact = await ConnectContact.findByPk(request.body.contactId);
    if (!contact) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const isValidSignature = VERIFY_EC_SHA256_DER_SIGNATURE(
      base64ToHex(contact.authPublicKey),
      concatSha256(['DELETE_CONNECTCONTACT', request.body.contactId]),
      base64ToHex(request.body.signature)
    );

    if (!isValidSignature) {
      return response.sendStatus(status.FORBIDDEN);
    }

    await contact.destroy();
    return response.sendStatus(status.NO_CONTENT);
  }
);

export default router;
