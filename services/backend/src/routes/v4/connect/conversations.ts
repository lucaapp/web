import { Router } from 'express';
import moment from 'moment';
import status from 'http-status';

import { validateSchema } from 'middlewares/validateSchema';
import { requireHealthDepartmentEmployee } from 'middlewares/requireUser';
import { limitRequestsPerDay } from 'middlewares/rateLimit';

import {
  database,
  ConnectConversation,
  ConnectMessage,
  ConnectSearch,
  ConnectSearchProcess,
} from 'database';

import { logEvent } from 'utils/hdAuditLog';
import { AuditLogEvents, AuditStatusType } from 'constants/auditLog';
import { createSchema } from './conversations.schemas';
import { requireConnectEnabled } from './connect.helper';

const router = Router();

router.use(requireHealthDepartmentEmployee);
router.use(requireConnectEnabled);

router.post(
  '/',
  limitRequestsPerDay('createConnectConversation_per_day'),
  validateSchema(createSchema),
  async (request, response) => {
    const search = await ConnectSearch.findOne({
      attributes: ['uuid', 'completedAt'],
      where: {
        uuid: request.body.searchId,
        completedAt: null,
      },
      include: {
        required: true,
        model: ConnectSearchProcess,
        attributes: ['departmentId'],
        where: {
          departmentId: request.user.departmentId,
        },
      },
    });
    if (!search) {
      return response.sendStatus(status.NOT_FOUND);
    }
    await database.transaction(async transaction => {
      await search.update(
        {
          completedAt: moment().toDate(),
        },
        { transaction }
      );
      const conversation = await ConnectConversation.create(
        {
          departmentId: request.user.departmentId,
          data: request.body.contact.data,
          iv: request.body.contact.iv,
          mac: request.body.contact.mac,
          publicKey: request.body.contact.publicKey,
        },
        { transaction }
      );

      logEvent(request.user, {
        type: AuditLogEvents.CONNECT_CONVERSATION_CREATED,
        status: AuditStatusType.SUCCESS,
        meta: {
          conversationId: conversation.uuid,
        },
      });
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

router.get('/', async (request, response) => {
  const conversations = await ConnectConversation.findAll({
    where: {
      departmentId: request.user.departmentId,
    },
  });

  return response.send(
    conversations.map(conversation => ({
      uuid: conversation.uuid,
      contact: {
        data: conversation.data,
        iv: conversation.iv,
        mac: conversation.mac,
        publicKey: conversation.publicKey,
      },
      createdAt: moment(conversation.createdAt).unix(),
      lastContactedAt: moment(conversation.lastContactedAt).unix(),
    }))
  );
});

router.get('/:conversationId/messages', async (request, response) => {
  const conversation = await ConnectConversation.findOne({
    attributes: ['uuid', 'departmentId'],
    where: {
      uuid: request.params.conversationId,
      departmentId: request.user.departmentId,
    },
    include: {
      model: ConnectMessage,
    },
  });
  if (!conversation || !conversation.ConnectMessages) {
    return response.sendStatus(status.NOT_FOUND);
  }

  return response.send(
    conversation.ConnectMessages.map(message => ({
      messageId: message.messageId,
      data: message.data,
      iv: message.iv,
      mac: message.mac,
      createdAt: moment(message.createdAt).unix(),
      receivedAt: moment(message.receivedAt).unix(),
      readAt: moment(message.readAt).unix(),
      readSignature: message.readSignature,
    }))
  );
});

export default router;
