import { z } from 'utils/validation';

export const sendSchema = z.object({
  messageId: z.base64({ rawLength: 16 }),
  conversationId: z.uuid(),
  data: z.base64({ max: 4096 }),
  iv: z.iv(),
  mac: z.mac(),
});

export const receiveSchema = z.object({
  messageIds: z
    .array(z.base64({ rawLength: 16 }))
    .min(1)
    .max(2016),
});

export const readSchema = z.object({
  messageId: z.base64({ rawLength: 16 }),
  timestamp: z.unixTimestamp(),
  signature: z.ecSignature(),
});
