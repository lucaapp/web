import { Router } from 'express';

import versionsRouter from './v2/versions';

const router = Router();

router.use('/versions', versionsRouter);

export default router;
