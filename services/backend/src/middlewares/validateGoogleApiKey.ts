import { RequestHandler } from 'express';
import config from 'config';

export const validateGoogleApiKey: RequestHandler = (_, response, next) => {
  if (!config.get('google.api.key')) {
    return response
      .status(503)
      .send({ error: 'Google API key not configured.' });
  }

  return next();
};
