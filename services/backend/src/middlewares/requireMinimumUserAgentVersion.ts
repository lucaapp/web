import { RequestHandler } from 'express';
import status from 'http-status';
import semver from 'semver';
import { FeatureFlags, get as featureFlagGet } from 'utils/featureFlag';
import NodeCache from 'node-cache';
import config from 'config';

const databaseCache = new NodeCache({
  stdTTL: config.get('luca.appVersion.cacheTTL'),
});

// false positive
// eslint-disable-next-line security/detect-unsafe-regex
const userAgentRegex = /^luca\/(?<platform>android|ios)\/?(?<business>busines{2}|)\s(?<version>\S+)((\s\S+)+)?$/i;
export const requireMinimumUserAgentVersionMiddleware = (
  exclude: string[] = []
): RequestHandler => async (request, response, next) => {
  const requestUrl = request.originalUrl.toLowerCase();
  const isExcluded = exclude.some(exludeString =>
    requestUrl.includes(exludeString)
  );
  if (isExcluded) {
    return next();
  }

  const userAgent = request.headers['user-agent'];

  if (!userAgent) {
    return next();
  }

  const parsedUserAgent = userAgentRegex.exec(userAgent);
  if (!parsedUserAgent?.length) {
    return next();
  }
  const [, platform, business, version] = parsedUserAgent;
  if (!semver.valid(version)) {
    return next();
  }

  let featureFlagKey =
    platform?.toLowerCase() === 'android'
      ? 'android_minimum_semver'
      : 'ios_minimum_semver';

  if (business) {
    featureFlagKey = `operator_${featureFlagKey}`;
  }

  let minimumVersion = databaseCache.get<string | null>(featureFlagKey);
  if (!minimumVersion) {
    minimumVersion = await featureFlagGet<string>(
      featureFlagKey as FeatureFlags
    );
    databaseCache.set(featureFlagKey, minimumVersion);
  }

  if (semver.gte(version, minimumVersion)) {
    return next();
  }

  return response.sendStatus(status.UPGRADE_REQUIRED);
};
