import status from 'http-status';
import { RequestHandler } from 'express';
import { z } from '../utils/validation';

export const validateOperatorStrongPassword = (
  payloadKey = 'password'
): RequestHandler => async (request, response, next) => {
  const { firstName, lastName, email } = request.user ?? request.body;
  const userInputs = [firstName, lastName, email];

  const password = request.body[String(payloadKey)];

  try {
    await z.zxcvbnPassword(userInputs).parseAsync(password);
  } catch (error) {
    request.log.warn(error, 'validate operator strong password');
    response.status(status.BAD_REQUEST);
    return response.send(error);
  }

  return next();
};
