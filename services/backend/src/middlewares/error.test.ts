import request from 'supertest';
import { setupDBRedisAndApp } from 'testing/utils';
import { getApp } from '../app';

const INVALID_URL = '/api/notexistant';

describe('Error handler', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
  });
  it('should return 404 on an invalid url', async () => {
    await request(getApp()).get(INVALID_URL).expect(404);
  });
});
