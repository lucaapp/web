const config = require('config');
const moment = require('moment');
const RateLimit = require('express-rate-limit');
const RedisStore = require('rate-limit-redis');
const parsePhoneNumber = require('libphonenumber-js/max');
const { SHA256, bytesToHex } = require('@lucaapp/crypto');
const { rateLimitSkip } = require('../utils/rateLimitSkip');
const { client } = require('../utils/redis').default;

const { isInternalIp, isRateLimitExemptIp } = require('../utils/ipChecks');

const minuteDuration = moment.duration(1, 'minute');
const hourDuration = moment.duration(1, 'hour');
const dayDuration = moment.duration(1, 'day');

const DEFAULT_RATE_LIMIT_MINUTE = config.get(
  `rate_limits.default_rate_limit_minute`
);
const DEFAULT_RATE_LIMIT_HOUR = config.get(
  `rate_limits.default_rate_limit_hour`
);
const DEFAULT_RATE_LIMIT_DAY = config.get(`rate_limits.default_rate_limit_day`);

const ipKeyGenerator = request => {
  return `${SHA256(bytesToHex(request.ip))}:${request.method}:${
    request.baseUrl
  }${request.route.path}`.toLowerCase();
};

const globalKeyGenerator = request => {
  return `global:${request.method}:${request.baseUrl}${request.route.path}`.toLowerCase();
};

const userKeyGenerator = request => {
  const { user } = request;
  return !user ? ipKeyGenerator(request) : user.uuid;
};

const phoneNumberKeyGenerator = request => {
  const phone = parsePhoneNumber(request.body.phone, 'DE');
  if (!phone) {
    throw new Error('phone number invalid');
  }
  const hashedPhoneNumber = SHA256(bytesToHex(phone.number));
  return `phone:${hashedPhoneNumber}`;
};

const emailKeyGenerator = request => {
  const hashedEmail = SHA256(bytesToHex(request.body.email.toLowerCase()));
  return `email:${hashedEmail}`;
};

const isFixedLinePhoneNumber = request => {
  const phone = parsePhoneNumber(request.body.phone, 'DE');
  return !!phone && phone.getType() === 'FIXED_LINE';
};

const minuteStore = new RedisStore({
  client,
  expiry: minuteDuration.as('s'),
});

const hourStore = new RedisStore({
  client,
  expiry: hourDuration.as('s'),
});

const dayStore = new RedisStore({
  client,
  expiry: dayDuration.as('s'),
});

const limitRequestsByFeatureFlag = (
  { skipSuccessfulRequests, global },
  rateLimitOptions,
  key = ''
) => (request, response, next) => {
  const max = config.get(`rate_limits.${key}`);
  const rateLimit = RateLimit({
    skip: ({ ip }) => rateLimitSkip(request.headers, ip),
    keyGenerator: global ? globalKeyGenerator : ipKeyGenerator,
    max,
    headers: config.get('rate_limits.enable_headers'),
    skipSuccessfulRequests,
    ...rateLimitOptions,
  });
  rateLimit(request, response, next);
};

const limitRequestsPerMinute = (key, { skipSuccessfulRequests, global } = {}) =>
  limitRequestsByFeatureFlag(
    { skipSuccessfulRequests, global },
    {
      store: minuteStore,
      windowMs: minuteDuration.as('ms'),
    },
    key || DEFAULT_RATE_LIMIT_MINUTE
  );

const limitRequestsPerHour = (key, { skipSuccessfulRequests, global } = {}) =>
  limitRequestsByFeatureFlag(
    { skipSuccessfulRequests, global },
    {
      store: hourStore,
      windowMs: hourDuration.as('ms'),
    },
    key || DEFAULT_RATE_LIMIT_HOUR
  );

const limitRequestsPerDay = (key, { skipSuccessfulRequests, global } = {}) =>
  limitRequestsByFeatureFlag(
    { skipSuccessfulRequests, global },
    {
      store: dayStore,
      windowMs: dayDuration.as('ms'),
    },
    key || DEFAULT_RATE_LIMIT_DAY
  );

const limitRequestsByPhoneNumberPerDay = key =>
  limitRequestsByFeatureFlag(
    {},
    {
      store: dayStore,
      windowMs: dayDuration.as('ms'),
      keyGenerator: phoneNumberKeyGenerator,
    },
    key
  );

const limitRequestsByEmailPerDay = key =>
  limitRequestsByFeatureFlag(
    {},
    {
      store: dayStore,
      windowMs: dayDuration.as('ms'),
      keyGenerator: emailKeyGenerator,
    },
    key
  );

const limitRequestsByFixedLinePhoneNumberPerDay = key =>
  limitRequestsByFeatureFlag(
    {},
    {
      store: dayStore,
      windowMs: dayDuration.as('ms'),
      skip: async request =>
        isInternalIp(request.ip) ||
        !isFixedLinePhoneNumber(request) ||
        isRateLimitExemptIp(request.ip),
      keyGenerator: phoneNumberKeyGenerator,
    },
    key
  );

const limitRequestsByUserPerMinute = key =>
  limitRequestsByFeatureFlag(
    {},
    {
      store: minuteStore,
      windowMs: minuteDuration.as('ms'),
      keyGenerator: request => {
        const { user } = request;
        return !user ? ipKeyGenerator(request) : user.uuid;
      },
    },
    key || DEFAULT_RATE_LIMIT_MINUTE
  );

const limitRequestsByUserPerHour = key =>
  limitRequestsByFeatureFlag(
    {},
    {
      store: hourStore,
      windowMs: hourDuration.as('ms'),
      keyGenerator: userKeyGenerator,
    },
    key || DEFAULT_RATE_LIMIT_HOUR
  );

const limitRequestsByUserPerDay = key =>
  limitRequestsByFeatureFlag(
    {},
    {
      store: dayStore,
      windowMs: dayDuration.as('ms'),
      keyGenerator: userKeyGenerator,
    },
    key || DEFAULT_RATE_LIMIT_DAY
  );

module.exports = {
  limitRequestsPerMinute,
  limitRequestsPerHour,
  limitRequestsPerDay,
  limitRequestsByPhoneNumberPerDay,
  limitRequestsByEmailPerDay,
  limitRequestsByFixedLinePhoneNumberPerDay,
  limitRequestsByUserPerMinute,
  limitRequestsByUserPerHour,
  limitRequestsByUserPerDay,
};
