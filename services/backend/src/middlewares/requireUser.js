const status = require('http-status');
const config = require('config');
const { verifyCertificateChain, getCommonName } = require('@lucaapp/crypto');
const passport = require('passport');
const moment = require('moment');
const { Op } = require('sequelize');
const { UserType } = require('../constants/user');
const { certificateChain } = require('../constants/certificateChain');
const { combineMiddlewares } = require('../utils/middlewares');
const { Operator } = require('../database');

const isUserOfType = (type, request) =>
  request.user && request.user.type === type;

const isUserOfRoleType = (role, request) =>
  request.user && request.user.device && request.user.device.role === role;

const hasValidClientCertificate = (user, request) => {
  if (!request.headers['ssl-client-cert']) return false;

  const certificatePem = unescape(request.headers['ssl-client-cert']);
  const commonName = getCommonName(certificatePem);
  if (commonName !== user.HealthDepartment.commonName) return false;
  return verifyCertificateChain([...certificateChain, certificatePem]);
};

const requireOperator = async (request, response, next) => {
  if (!isUserOfType(UserType.OPERATOR, request)) {
    return response.sendStatus(status.UNAUTHORIZED);
  }

  const bannedOperator = await Operator.findOne({
    where: {
      uuid: request.user.uuid,
      bannedAt: { [Op.lte]: moment().toDate() },
    },
  });

  if (bannedOperator) {
    return response.sendStatus(status.NOT_FOUND);
  }

  return next();
};

const requireJwtOperator = async (request, response, next) => {
  passport.authenticate(
    'jwt-operator',
    { session: false },
    async (error, user) => {
      if (error || !user) {
        return response.sendStatus(status.UNAUTHORIZED);
      }

      request.user = user;
      return next();
    }
  )(request, response, next);
};

const requireOperatorDevice = (request, response, next) => {
  if (isUserOfType(UserType.OPERATOR_DEVICE, request)) {
    return next();
  }
  return response.sendStatus(status.UNAUTHORIZED);
};

const requireOperatorDeviceRole = role => (request, response, next) => {
  if (isUserOfType(UserType.OPERATOR, request)) {
    return next();
  }

  if (isUserOfRoleType(role, request)) {
    return next();
  }

  return response.sendStatus(status.FORBIDDEN);
};
const requireOperatorDeviceRoles = roles => {
  const rolesMap = {};

  for (const role of roles) {
    // eslint-disable-next-line security/detect-object-injection
    rolesMap[role] = true;
  }

  return (request, response, next) => {
    if (isUserOfType(UserType.OPERATOR, request)) {
      return next();
    }

    if (
      request.user &&
      request.user.device &&
      rolesMap[request.user.device.role]
    ) {
      return next();
    }

    return response.sendStatus(status.FORBIDDEN);
  };
};

const requireOperatorOROperatorDevice = (request, response, next) => {
  if (
    isUserOfType(UserType.OPERATOR, request) ||
    isUserOfType(UserType.OPERATOR_DEVICE, request)
  ) {
    return next();
  }
  return response.sendStatus(status.UNAUTHORIZED);
};

const requireHealthDepartmentEmployee = (request, response, next) => {
  if (!isUserOfType(UserType.HEALTH_DEPARTMENT_EMPLOYEE, request))
    return response.sendStatus(status.UNAUTHORIZED);

  if (!(hasValidClientCertificate(request.user, request) || config.get('e2e')))
    return response.status(status.UNAUTHORIZED).send('Invalid Certificate');

  return next();
};

const requireAdmin = (request, response, next) => {
  if (request.user && request.user.isAdmin === true) {
    return next();
  }
  return response.sendStatus(status.FORBIDDEN);
};

const requireNonDeletedUser = (request, response, next) => {
  if (!request.user) {
    return response.sendStatus(status.UNAUTHORIZED);
  }
  if (!request.user.deletedAt) {
    return next();
  }
  return response.status(status.FORBIDDEN).send({
    message: 'The account has been marked for deletion',
    errorCode: 'ACCOUNT_DEACTIVATED',
  });
};

const requireHealthDepartmentAdmin = combineMiddlewares([
  requireHealthDepartmentEmployee,
  requireAdmin,
]);

module.exports = {
  requireOperator,
  requireJwtOperator,
  requireOperatorDevice,
  requireOperatorDeviceRole,
  requireOperatorDeviceRoles,
  requireOperatorOROperatorDevice,
  requireHealthDepartmentEmployee,
  requireHealthDepartmentAdmin,
  requireNonDeletedUser,
  isUserOfType,
};
