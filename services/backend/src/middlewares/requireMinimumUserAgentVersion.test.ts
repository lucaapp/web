import request from 'supertest';
import { setupDBRedisAndApp } from 'testing/utils';

import { getApp } from '../app';

import { DEFAULT_VALUES } from '../utils/featureFlag';

const VALID_VERSION = DEFAULT_VALUES.ios_minimum_semver;
const HEALTH_CHECK_URL = '/api/v3/health';
const UA_HEADER = 'User-Agent';
const VALID_UA = `luca/iOS ${VALID_VERSION}`;
const VALID_UA_BUSINESS = `luca/iOS/business ${VALID_VERSION}`;
const OUTDATED_UA = 'luca/iOS 1.0.0';
const OUTDATED_WITH_ENVIRONMENT = 'luca/iOS 1.0.0 AQS FOO BAR';
const VALID_WITH_ENVIRONMENT = `luca/iOS ${DEFAULT_VALUES.ios_minimum_semver} AQS`;
const OUTDATED_UA_BUSINESS = 'luca/Android/business v0.1.0';
const INVALID_VERSION = 'luca/iOS 100';
const INVALID_PLATFORM = `luca/operator-app ${VALID_VERSION}`;
const INVALID_BUSINESS = `luca/iOS/operator-app ${VALID_VERSION}`;

describe('Minimum User Agent Version Middleware', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
  });

  it('Passes if User Agent is not given', async () => {
    await request(getApp()).get(HEALTH_CHECK_URL).expect(200);
  });

  it('Passes if User Agent Version is high enough', async () => {
    await request(getApp())
      .get(HEALTH_CHECK_URL)
      .set(UA_HEADER, VALID_UA)
      .expect(200);
  });

  it('Passes if User Agent with Enviroment Version is high enough', async () => {
    await request(getApp())
      .get(HEALTH_CHECK_URL)
      .set(UA_HEADER, VALID_WITH_ENVIRONMENT)
      .expect(200);
  });
});

it('Passes if User Agent Version is high enough for operator app', async () => {
  await request(getApp())
    .get(HEALTH_CHECK_URL)
    .set(UA_HEADER, VALID_UA_BUSINESS)
    .expect(200);
});

it('Returns Upgrade Required if version is too low', async () => {
  await request(getApp())
    .get(HEALTH_CHECK_URL)
    .set(UA_HEADER, OUTDATED_UA)
    .expect(426);
});

it('Returns Upgrade Required if version is too low for operator app', async () => {
  await request(getApp())
    .get(HEALTH_CHECK_URL)
    .set(UA_HEADER, OUTDATED_UA_BUSINESS)
    .expect(426);
});

it('Returns Upgrade Required if version is too low and environment string was sent', async () => {
  await request(getApp())
    .get(HEALTH_CHECK_URL)
    .set(UA_HEADER, OUTDATED_WITH_ENVIRONMENT)
    .expect(426);
});

it('Ignore the User Agent if is not a qualified App UA', async () => {
  await request(getApp())
    .get(HEALTH_CHECK_URL)
    .set(UA_HEADER, 'not-a-valid-ua')
    .expect(200);
});

it('Ignore the User Agent if it does not contain a valid semver version', async () => {
  await request(getApp())
    .get(HEALTH_CHECK_URL)
    .set(UA_HEADER, INVALID_VERSION)
    .expect(200);
});

it('Ignore the User Agent if it does not contain a valid platform', async () => {
  await request(getApp())
    .get(HEALTH_CHECK_URL)
    .set(UA_HEADER, INVALID_PLATFORM)
    .expect(200);
});

it('Ignore the User Agent if it does not contain a valid business', async () => {
  await request(getApp())
    .get(HEALTH_CHECK_URL)
    .set(UA_HEADER, INVALID_BUSINESS)
    .expect(200);
});
