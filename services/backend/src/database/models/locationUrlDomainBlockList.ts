import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

interface Attributes {
  domain: string;
}

type CreationAttributes = Attributes;

export interface LocationURLDomainBlockListInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initLocationURLDomainBlockList = (
  sequelize: Sequelize
): ModelCtor<LocationURLDomainBlockListInstance> => {
  return sequelize.define<LocationURLDomainBlockListInstance>(
    'LocationURLDomainBlockList',
    {
      domain: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
    },
    {
      tableName: 'LocationURLDomainBlockList',
      timestamps: false,
    }
  );
};
