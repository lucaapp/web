import { Location, LocationTables } from 'database';
import { v4 as uuid } from 'uuid';
import { setupDBRedisAndApp } from '../../testing/utils';

describe('LocationTables', () => {
  beforeEach(async () => {
    await setupDBRedisAndApp();
  });

  describe('lazy migration for adding id', () => {
    describe('when creating model without id', () => {
      it('generates new id before creation and saves it as well', async () => {
        const location = await Location.create({
          // @ts-ignore this is not part of normal creation attributes
          uuid: uuid(),
          radius: 5,
          scannerId: uuid(),
          isPrivate: false,
          publicKey: 'fake public key',
          accessId: uuid(),
          scannerAccessId: uuid(),
          formId: uuid(),
          isIndoor: false,
          isContactDataMandatory: false,
        });

        const locationTable = await LocationTables.create({
          customName: 'Some Name',
          locationId: location.uuid,
          internalNumber: 42,
          id: uuid(),
        });

        expect(typeof locationTable.id).toBe('string');
        expect(
          await LocationTables.findOne({ where: { id: locationTable.id } })
        ).toMatchObject({ id: locationTable.id });
      });
    });

    describe('when creating model without id via bulk create', () => {
      it('generates new id before creation and saves it as well', async () => {
        const location = await Location.create({
          // @ts-ignore this is not part of normal creation attributes
          uuid: uuid(),
          radius: 5,
          scannerId: uuid(),
          isPrivate: false,
          publicKey: 'fake public key',
          accessId: uuid(),
          scannerAccessId: uuid(),
          formId: uuid(),
          isIndoor: false,
          isContactDataMandatory: false,
        });

        const [locationTable] = await LocationTables.bulkCreate([
          {
            customName: 'Some Name',
            locationId: location.uuid,
            internalNumber: 42,
            id: uuid(),
          },
        ]);

        expect(typeof locationTable.id).toBe('string');
        expect(
          await LocationTables.findOne({ where: { id: locationTable.id } })
        ).toMatchObject({ id: locationTable.id });
      });
    });
  });
});
