import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  operatorId: string;
  publicKey: boolean;
  privateKeySecret: boolean;
};

export interface OperatorKeyHistoryInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initOperatorKeyHistories = (
  sequelize: Sequelize
): ModelCtor<OperatorKeyHistoryInstance> => {
  return sequelize.define<OperatorKeyHistoryInstance>('OperatorKeyHistory', {
    operatorId: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
    },
    publicKey: {
      type: DataTypes.STRING(88),
      allowNull: false,
      primaryKey: true,
    },
    privateKeySecret: {
      type: DataTypes.STRING(44),
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};
