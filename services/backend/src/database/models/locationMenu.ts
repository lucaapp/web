import { Sequelize, Model, ModelStatic, DataTypes } from 'sequelize';
import type { Models } from '..';

export type LocationMenuTranslatableInfo = {
  de: string;
  en?: string;
};

export interface LocationMenu {
  categories: {
    name?: LocationMenuTranslatableInfo;
    description?: LocationMenuTranslatableInfo;
    items: {
      name?: LocationMenuTranslatableInfo;
      description?: LocationMenuTranslatableInfo;
      additionalInfo?: LocationMenuTranslatableInfo;
      price: number;
      allergensAndAdditives: string[];
    }[];
  }[];
  allergenesAndAdditives: Record<
    string,
    LocationMenuTranslatableInfo | undefined
  >;
}
type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  locationId: string;
  menu: LocationMenu;
};

export interface LocationMenuInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initLocationMenus = (
  sequelize: Sequelize
): ModelStatic<LocationMenuInstance> => {
  return sequelize.define<LocationMenuInstance>('LocationMenu', {
    locationId: {
      allowNull: false,
      type: DataTypes.UUID,
      primaryKey: true,
    },
    menu: {
      allowNull: true,
      type: DataTypes.JSON,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};

export const associateLocationMenu = (models: Models): void => {
  models.LocationMenu.belongsTo(models.Location, {
    foreignKey: 'locationId',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  });
};
