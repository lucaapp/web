import { Sequelize, Model, ModelStatic, DataTypes } from 'sequelize';
import type { Models } from '..';
import type { LocationGroupInstance } from './locationGroup';

interface Attributes {
  uuid: string;
  firstName: string;
  lastName: string | null;
  employeeNumber: string | null;
  locationGroupId: string;
  createdAt: Date;
  updatedAt: Date;
}

type CreationAttributes = {
  firstName: string;
  lastName?: string | null;
  employeeNumber?: string | null;
  locationGroupId: string;
};

export interface LocationGroupEmployeeInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  LocationGroup?: LocationGroupInstance;
}

export const initLocationGroupEmployees = (
  sequelize: Sequelize
): ModelStatic<LocationGroupEmployeeInstance> => {
  return sequelize.define<LocationGroupEmployeeInstance>(
    'LocationGroupEmployee',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      firstName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      employeeNumber: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      locationGroupId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    }
  );
};

export const associateLocationGroupEmployees = (models: Models): void => {
  models.LocationGroupEmployee.belongsTo(models.LocationGroup, {
    foreignKey: 'locationGroupId',
    onDelete: 'CASCADE',
  });
};
