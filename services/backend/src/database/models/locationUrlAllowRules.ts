import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

interface Attributes {
  rule: string;
}

type CreationAttributes = Attributes;

export interface LocationURLAllowRulesInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initLocationURLAllowRules = (
  sequelize: Sequelize
): ModelCtor<LocationURLAllowRulesInstance> => {
  return sequelize.define<LocationURLAllowRulesInstance>(
    'LocationURLAllowRules',
    {
      rule: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
    },
    {
      tableName: 'LocationURLAllowRules',
      timestamps: false,
    }
  );
};
