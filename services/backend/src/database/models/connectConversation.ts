import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import type { Models } from '..';
import type { ConnectMessageInstance } from './connectMessage';

interface Attributes {
  uuid: string;
  departmentId: string;
  data: string;
  publicKey: string;
  iv: string;
  mac: string;
  createdAt: Date;
  lastContactedAt?: Date;
}

type CreationAttributes = {
  departmentId: string;
  data: string;
  publicKey: string;
  iv: string;
  mac: string;
};

export interface ConnectConversationInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  ConnectMessages?: ConnectMessageInstance[];
}

export const initConnectConversation = (
  sequelize: Sequelize
): ModelCtor<ConnectConversationInstance> => {
  return sequelize.define<ConnectConversationInstance>(
    'ConnectConversation',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      departmentId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'HealthDepartments',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      data: {
        type: DataTypes.STRING(4096),
        allowNull: false,
      },
      publicKey: {
        type: DataTypes.STRING(88),
        allowNull: false,
      },
      iv: {
        type: DataTypes.STRING(24),
        allowNull: false,
      },
      mac: {
        type: DataTypes.STRING(44),
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      lastContactedAt: {
        type: DataTypes.DATE,
      },
    },
    {
      timestamps: false,
    }
  );
};

export const associateConnectConversation = (models: Models): void => {
  models.ConnectConversation.belongsTo(models.HealthDepartment, {
    foreignKey: 'departmentId',
  });

  models.ConnectConversation.hasMany(models.ConnectMessage, {
    foreignKey: 'conversationId',
  });
};
