import { DataTypes, Model, ModelStatic, Sequelize } from 'sequelize';
import type { Models } from 'database';

interface Attributes {
  uuid: string;
  utm_id?: string;
  utm_source: string;
  utm_medium: string;
  utm_campaign?: string;
  operatorId: string;
  createdAt: Date;
  updatedAt: Date;
}

type CreationAttributes = {
  utm_id?: string;
  utm_source: string;
  utm_medium: string;
  utm_campaign?: string;
  operatorId: string;
};

export interface OperatorCampaignInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initOperatorCampaign = (
  sequelize: Sequelize
): ModelStatic<OperatorCampaignInstance> =>
  sequelize.define<OperatorCampaignInstance>('OperatorCampaign', {
    uuid: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    utm_id: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    utm_source: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    utm_medium: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    utm_campaign: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    operatorId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'Operators',
        key: 'uuid',
      },
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE',
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });

export const associateOperatorCampaign = (models: Models): void => {
  models.OperatorCampaign.belongsTo(models.Operator, {
    foreignKey: 'operatorId',
  });
};
