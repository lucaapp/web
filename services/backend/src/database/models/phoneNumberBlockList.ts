import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

interface Attributes {
  phoneNumber: number;
  createdAt: Date;
  updatedAt: Date;
}

type CreationAttributes = {
  phoneNumber: number;
};

export interface PhoneNumberBlockListInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initPhoneNumberBlockList = (
  sequelize: Sequelize
): ModelCtor<PhoneNumberBlockListInstance> => {
  return sequelize.define<PhoneNumberBlockListInstance>(
    'PhoneNumberBlockList',
    {
      phoneNumber: {
        type: DataTypes.STRING,
        primaryKey: true,
        unique: true,
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      tableName: 'PhoneNumberBlockList',
    }
  );
};
