import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  key: string;
  value: string;
  locationFrontend: boolean;
  healthDepartmentFrontend: boolean;
  webapp: boolean;
  ios: boolean;
  android: boolean;
  operatorApp: boolean;
};

export interface FeatureFlagInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initFeatureFlags = (
  sequelize: Sequelize
): ModelCtor<FeatureFlagInstance> => {
  return sequelize.define<FeatureFlagInstance>('FeatureFlag', {
    key: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    value: {
      type: DataTypes.STRING,
    },
    locationFrontend: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
    healthDepartmentFrontend: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
    webapp: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
    ios: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
    android: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
    operatorApp: {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};
