import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  fingerprint: string;
  name: string;
  publicKey: string;
};

export interface TestProviderInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initTestProviders = (
  sequelize: Sequelize
): ModelCtor<TestProviderInstance> => {
  return sequelize.define<TestProviderInstance>('TestProvider', {
    fingerprint: {
      type: DataTypes.STRING(32),
      allowNull: false,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    publicKey: {
      type: DataTypes.STRING(8192),
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};
