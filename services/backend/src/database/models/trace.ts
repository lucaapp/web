import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';
import type { LocationInstance } from './location';
import type { TraceDataInstance } from './traceData';
import type { Models } from '..';

interface TimerangeDate extends Date {
  value?: string;
  inclusive?: boolean;
}

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  traceId: string;
  locationId: string;
  time: [TimerangeDate, TimerangeDate] | [TimerangeDate, null];
  data: string;
  publicKey: string;
  isContactDataMandatory: boolean;
  isContactDataIncluded: boolean;
  iv?: string;
  mac?: string;
  deviceType?: number;
  expiresAt?: Date;
  authPublicKey?: string;
};

export interface TraceInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  Location?: LocationInstance;
  TraceData?: TraceDataInstance;
}

export const initTraces = (sequelize: Sequelize): ModelCtor<TraceInstance> => {
  return sequelize.define<TraceInstance>('Trace', {
    traceId: {
      type: DataTypes.STRING(24),
      allowNull: false,
      primaryKey: true,
    },
    locationId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    time: {
      type: DataTypes.RANGE(DataTypes.DATE),
      allowNull: false,
    },
    data: {
      type: DataTypes.STRING(128),
      allowNull: false,
    },
    iv: {
      type: DataTypes.STRING(24),
    },
    mac: {
      type: DataTypes.STRING(44),
    },
    publicKey: {
      type: DataTypes.STRING(88),
      allowNull: false,
    },
    deviceType: {
      type: DataTypes.INTEGER,
    },
    expiresAt: {
      type: DataTypes.DATE,
    },
    isContactDataMandatory: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    isContactDataIncluded: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    authPublicKey: {
      type: DataTypes.STRING(88),
      allowNull: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};

export const associateTrace = (models: Models): void => {
  models.Trace.hasOne(models.TraceData, {
    foreignKey: 'traceId',
    onDelete: 'CASCADE',
  });

  models.Trace.belongsTo(models.Location, {
    foreignKey: 'locationId',
  });
};
