import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import type { Models } from '..';
import type { ConnectSearchInstance } from './connectSearch';

interface Attributes {
  uuid: string;
  departmentId: string;
  createdAt: Date;
}

type CreationAttributes = {
  departmentId: string;
};

export interface ConnectSearchProcessInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  ConnectSearches: ConnectSearchInstance[];
}

export const initConnectSearchProcess = (
  sequelize: Sequelize
): ModelCtor<ConnectSearchProcessInstance> => {
  return sequelize.define<ConnectSearchProcessInstance>(
    'ConnectSearchProcess',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      departmentId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'HealthDepartments',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      timestamps: false,
    }
  );
};

export const associateConnectSearchProcess = (models: Models): void => {
  models.ConnectSearchProcess.hasMany(models.ConnectSearch, {
    foreignKey: 'processId',
  });

  models.ConnectSearchProcess.belongsTo(models.HealthDepartment, {
    foreignKey: 'departmentId',
  });
};
