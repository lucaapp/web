import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  departmentId: string;
  locationTransferId: string;
  requestingDepartmentId: string;
};

export interface SeenOverlappingLocationTranferInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initSeenOverlappingLocationTranfers = (
  sequelize: Sequelize
): ModelCtor<SeenOverlappingLocationTranferInstance> => {
  return sequelize.define<SeenOverlappingLocationTranferInstance>(
    'SeenOverlappingLocationTranfers',
    {
      departmentId: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      requestingDepartmentId: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      locationTransferId: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    }
  );
};
