import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

interface Attributes {
  uuid: string;
  name: string;
  meta: string;
  completedAt: Date;
}

type CreationAttributes = Pick<Attributes, 'name' | 'meta'>;

export interface JobCompletionsInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initJobCompletions = (
  sequelize: Sequelize
): ModelCtor<JobCompletionsInstance> => {
  return sequelize.define<JobCompletionsInstance>(
    'JobCompletions',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      meta: {
        type: DataTypes.JSON,
        allowNull: false,
      },
      completedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    },
    { timestamps: false }
  );
};
