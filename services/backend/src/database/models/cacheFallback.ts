import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

interface CreationAttributes {
  key: string;
  content: string;
  etag: string;
}

export interface CacheFallbackInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initCachFallbacks = (
  sequelize: Sequelize
): ModelCtor<CacheFallbackInstance> => {
  return sequelize.define<CacheFallbackInstance>('CacheFallbacks', {
    key: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true,
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    etag: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};
