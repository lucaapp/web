import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';
import type { LocationTransferTraceInstance } from './locationTransferTrace';
import type { Models } from '..';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = { locationTransferTraceId: string; level: number };

export interface RiskLevelInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  LocationTransferTrace?: LocationTransferTraceInstance;
}

export const initRiskLevels = (
  sequelize: Sequelize
): ModelCtor<RiskLevelInstance> => {
  return sequelize.define<RiskLevelInstance>('RiskLevel', {
    locationTransferTraceId: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
    },
    level: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};

export const associateRiskLevel = (models: Models): void => {
  models.RiskLevel.belongsTo(models.LocationTransferTrace, {
    foreignKey: 'locationTransferTraceId',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  });
};
