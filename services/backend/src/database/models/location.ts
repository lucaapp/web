/* eslint-disable max-lines */
import {
  col,
  DataTypes,
  fn,
  Model,
  ModelStatic,
  Op,
  Sequelize,
  Transaction,
} from 'sequelize';
import {
  LocationEntryPolicyInfoTypes,
  LocationEntryPolicyTypes,
  LocationMaskTypes,
  LocationVentilationTypes,
  AreaType,
  LocationType,
} from 'constants/location';
import { OpeningHours, openingHoursSchema } from 'utils/openingHours';
import { generateLocationTableId } from 'utils/locationTables';

import type { Point } from 'geojson';
import type { Models } from '..';
import type { TraceInstance } from './trace';
import type { OperatorInstance } from './operator';
import type { LocationGroupInstance } from './locationGroup';
import type { LocationTransferInstance } from './locationTransfer';
import type { AdditionalDataInstance } from './additionalDataSchema';
import type { LocationTableInstance } from './locationTables';

type Attributes = {
  uuid: string;
  radius?: number;
  scannerId: string;
  isPrivate: boolean;
  publicKey?: string;
  accessId: string;
  scannerAccessId: string;
  formId: string;
  isIndoor?: boolean;
  groupId?: string;
  name?: string | null;
  firstName?: string;
  lastName?: string;
  phone?: string;
  streetName?: string | null;
  streetNr?: string | null;
  zipCode?: string | null;
  city?: string | null;
  state?: string | null;
  geometricPoint?: Point | null;
  lat?: number | null;
  lng?: number | null;
  operator?: string;
  endsAt?: Date;
  type?: AreaType;
  averageCheckinTime?: number | null;
  entryPolicy?: LocationEntryPolicyTypes | null;
  isContactDataMandatory: boolean;
  deletedAt?: Date | null;
  entryPolicyInfo?: LocationEntryPolicyInfoTypes | null;
  masks?: LocationMaskTypes | null;
  ventilation?: LocationVentilationTypes | null;
  roomHeight?: number | null;
  roomWidth?: number | null;
  roomDepth?: number | null;
  openingHours?: OpeningHours | null;
  subtype?: string | null;
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = Omit<
  Attributes,
  | 'uuid'
  | 'scannerId'
  | 'accessId'
  | 'scannerAccessId'
  | 'formId'
  | 'isPrivate'
  | 'isContactDataMandatory'
  | 'createdAt'
  | 'updatedAt'
>;

export interface LocationInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  checkoutAllTraces: (transaction?: Transaction) => Promise<void>;
  removeAllTraces: (transaction?: Transaction) => Promise<void>;
  initLocationTables: (
    tableCount?: number | null,
    transaction?: Transaction
  ) => Promise<LocationTableInstance[]>;
  getGroupType: () => Promise<LocationType | undefined>;

  LocationTables?: Array<LocationTableInstance>;
  AdditionalDataSchemas?: Array<AdditionalDataInstance>;
  Operator?: OperatorInstance;
  LocationGroup?: LocationGroupInstance;
  Traces?: Array<TraceInstance>;
  LocationTransfers?: Array<LocationTransferInstance>;
}

export const initLocations = (
  sequelize: Sequelize
): ModelStatic<LocationInstance> => {
  const model = sequelize.define<LocationInstance>(
    'Location',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      scannerId: {
        type: DataTypes.UUID,
        allowNull: false,
        unique: true,
        defaultValue: DataTypes.UUIDV4,
      },
      accessId: {
        type: DataTypes.UUID,
        allowNull: false,
        unique: true,
        defaultValue: DataTypes.UUIDV4,
      },
      formId: {
        type: DataTypes.UUID,
        allowNull: false,
        unique: true,
        defaultValue: DataTypes.UUIDV4,
      },
      scannerAccessId: {
        type: DataTypes.UUID,
        allowNull: false,
        unique: true,
        defaultValue: DataTypes.UUIDV4,
      },
      name: {
        type: DataTypes.STRING,
      },
      firstName: {
        type: DataTypes.STRING,
      },
      lastName: {
        type: DataTypes.STRING,
      },
      phone: {
        type: DataTypes.STRING,
      },
      streetName: {
        type: DataTypes.STRING,
      },
      streetNr: {
        type: DataTypes.STRING,
      },
      zipCode: {
        type: DataTypes.STRING,
      },
      city: {
        type: DataTypes.STRING,
      },
      state: {
        type: DataTypes.STRING,
        defaultValue: null,
      },
      geometricPoint: {
        type: DataTypes.GEOGRAPHY('POINT', 4326),
        allowNull: true,
      },
      lng: {
        type: DataTypes.VIRTUAL,
        get() {
          return this.geometricPoint?.coordinates[0];
        },
      },
      lat: {
        type: DataTypes.VIRTUAL,
        get() {
          return this.geometricPoint?.coordinates[1];
        },
      },
      entryPolicy: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.ENUM({
          values: Object.values(LocationEntryPolicyTypes),
        }),
      },
      isContactDataMandatory: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      radius: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      endsAt: {
        type: DataTypes.DATE,
      },
      isPrivate: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      publicKey: {
        type: DataTypes.STRING(88),
        allowNull: true,
        defaultValue: null,
      },
      isIndoor: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      type: {
        type: DataTypes.STRING,
        allowNull: true,
        defaultValue: 'other',
      },
      subtype: {
        type: DataTypes.STRING,
        allowNull: true,
        defaultValue: null,
      },
      averageCheckinTime: {
        type: DataTypes.INTEGER,
        allowNull: true,
        defaultValue: null,
      },
      entryPolicyInfo: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.STRING,
      },
      ventilation: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.STRING,
      },
      masks: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.STRING,
      },
      roomHeight: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.FLOAT,
      },
      roomWidth: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.FLOAT,
      },
      roomDepth: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.FLOAT,
      },
      openingHours: {
        type: DataTypes.JSON,
        allowNull: true,
        defaultValue: null,
        validate: {
          customValidator(value: unknown) {
            openingHoursSchema.parse(value);
          },
        },
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      paranoid: true,
    }
  );

  model.prototype.checkoutAllTraces = async function checkoutAllTraces(
    transaction?: Transaction
  ) {
    return sequelize.models.Trace.update(
      { time: fn('tstzrange', fn('lower', col('time')), fn('now')) },
      {
        where: {
          locationId: this.get('uuid'),
          // @ts-ignore typing on a range with Op.contains
          time: {
            [Op.contains]: fn('now'),
          },
        },
        transaction,
      }
    );
  };

  model.prototype.removeAllTraces = async function removeAllTraces(
    transaction?: Transaction
  ) {
    return sequelize.models.Trace.destroy({
      force: true,
      where: {
        locationId: this.get('uuid'),
      },
      transaction,
    });
  };

  model.prototype.getGroupType = async function getGroupType() {
    return sequelize.models.LocationGroup.findByPk(this.get('groupId'), {
      attributes: ['type'],
    }).then(data => data?.getDataValue('type'));
  };

  model.prototype.initLocationTables = async function initLocationTables(
    tableCount?: number | null,
    transaction?: Transaction
  ) {
    if (tableCount) {
      const locationTables = [];
      for (
        let internalNumber = 1;
        internalNumber <= tableCount;
        internalNumber += 1
      ) {
        locationTables.push({
          id: await generateLocationTableId(),
          locationId: this.get('uuid'),
          internalNumber,
        });
      }

      return sequelize.models.LocationTable.bulkCreate(locationTables, {
        transaction,
      });
    }

    return [];
  };

  return model;
};

export const associateLocation = (models: Models): void => {
  models.Location.belongsTo(models.Operator, {
    foreignKey: 'operator',
    onDelete: 'CASCADE',
  });

  models.Location.belongsTo(models.LocationGroup, {
    foreignKey: 'groupId',
    onDelete: 'CASCADE',
  });

  models.Location.hasMany(models.AdditionalDataSchema, {
    foreignKey: 'locationId',
  });

  models.Location.hasMany(models.Trace, {
    foreignKey: 'locationId',
  });

  models.Location.hasMany(models.LocationTransfer, {
    foreignKey: 'locationId',
  });

  models.Location.hasMany(models.LocationTables, {
    foreignKey: 'locationId',
  });
};
