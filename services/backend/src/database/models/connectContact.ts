import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import type { Models } from '..';

interface Attributes {
  uuid: string;
  departmentId: string;
  namePrefix: string;
  phonePrefix: string;
  authPublicKey: string;
  referenceData: string;
  referencePublicKey: string;
  referenceIV: string;
  referenceMAC: string;
  dataData: string;
  dataPublicKey: string;
  dataIV: string;
  dataMAC: string;
  signature: string;
  createdAt: Date;
}

type CreationAttributes = {
  departmentId: string;
  namePrefix: string;
  phonePrefix: string;
  authPublicKey: string;
  referenceData: string;
  referencePublicKey: string;
  referenceIV: string;
  referenceMAC: string;
  dataData: string;
  dataPublicKey: string;
  dataIV: string;
  dataMAC: string;
  signature: string;
};

export interface ConnectContactInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initConnectContact = (
  sequelize: Sequelize
): ModelCtor<ConnectContactInstance> => {
  return sequelize.define<ConnectContactInstance>(
    'ConnectContact',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      departmentId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'HealthDepartments',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      namePrefix: {
        type: DataTypes.STRING(64),
        allowNull: false,
      },
      phonePrefix: {
        type: DataTypes.STRING(64),
        allowNull: false,
      },
      authPublicKey: {
        type: DataTypes.STRING(88),
        allowNull: false,
      },
      referenceData: {
        type: DataTypes.STRING(108),
        allowNull: false,
      },
      referencePublicKey: {
        type: DataTypes.STRING(88),
        allowNull: false,
      },
      referenceIV: {
        type: DataTypes.STRING(24),
        allowNull: false,
      },
      referenceMAC: {
        type: DataTypes.STRING(44),
        allowNull: false,
      },
      dataData: {
        type: DataTypes.STRING(4096),
        allowNull: false,
      },
      dataPublicKey: {
        type: DataTypes.STRING(88),
        allowNull: false,
      },
      dataIV: {
        type: DataTypes.STRING(24),
        allowNull: false,
      },
      dataMAC: {
        type: DataTypes.STRING(44),
        allowNull: false,
      },
      signature: {
        type: DataTypes.STRING(120),
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      timestamps: false,
    }
  );
};

export const associateConnectContact = (models: Models): void => {
  models.ConnectContact.belongsTo(models.HealthDepartment, {
    foreignKey: 'departmentId',
  });
};
