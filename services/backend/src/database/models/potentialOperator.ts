import { DataTypes, Model, ModelStatic, Sequelize } from 'sequelize';

interface Attributes {
  email: string;
  utm_id?: string;
  utm_source?: string;
  utm_medium?: string;
  utm_campaign?: string;
  createdAt: Date;
}

type CreationAttributes = {
  email: string;
  utm_id?: string;
  utm_source?: string;
  utm_medium?: string;
  utm_campaign?: string;
};

export interface PotentialOperatorInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initPotentialOperator = (
  sequelize: Sequelize
): ModelStatic<PotentialOperatorInstance> =>
  sequelize.define<PotentialOperatorInstance>(
    'PotentialOperator',
    {
      email: {
        type: DataTypes.CITEXT,
        allowNull: false,
        primaryKey: true,
      },
      utm_id: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      utm_source: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      utm_medium: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      utm_campaign: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      timestamps: false,
    }
  );
