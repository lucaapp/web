import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

type Attributes = {
  keyId: number;
  publicKey: string;
  issuerId: string;
  signature: string;
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = Omit<Attributes, 'updatedAt'>;

export interface BadgePublicKeyInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initBadgePublicKeys = (
  sequelize: Sequelize
): ModelCtor<BadgePublicKeyInstance> => {
  return sequelize.define<BadgePublicKeyInstance>('BadgePublicKey', {
    keyId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      defaultValue: 0,
    },
    publicKey: {
      type: DataTypes.STRING(88),
      allowNull: false,
    },
    issuerId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    signature: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};
