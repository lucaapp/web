import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

interface Attributes {
  id: string;
  encryptedPayload: Buffer;
  version: number;
  expirationDate: Date;
  totpSecret: Buffer;
}

export interface VaultInstance extends Model<Attributes>, Attributes {}

export const initVaultContents = (
  sequelize: Sequelize
): ModelCtor<VaultInstance> => {
  return sequelize.define<VaultInstance>(
    'VaultContents',
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      encryptedPayload: {
        type: DataTypes.BLOB,
        allowNull: false,
      },
      version: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      expirationDate: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      totpSecret: {
        type: DataTypes.BLOB,
        allowNull: false,
      },
    },
    { timestamps: false }
  );
};
