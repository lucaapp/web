import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import { hashPassword } from 'utils/hash';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  name: string;
  password: string;
  salt: string;
};

export interface BadgeGeneratorInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  checkPassword: (testPassword: string) => Promise<boolean>;
}

export const initBadgeGenerators = (
  sequelize: Sequelize
): ModelCtor<BadgeGeneratorInstance> => {
  const model = sequelize.define<BadgeGeneratorInstance>('BadgeGenerator', {
    name: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    password: {
      type: DataTypes.STRING(255),
    },
    salt: {
      type: DataTypes.STRING(255),
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });

  model.prototype.checkPassword = async function checkPassword(
    testPassword: string
  ) {
    const hash = await hashPassword(testPassword, this.get('salt'));
    return hash.toString('base64') === this.get('password');
  };

  return model;
};
