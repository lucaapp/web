import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

interface Attributes {
  rule: string;
}

type CreationAttributes = Attributes;

export interface LocationURLDenyRulesInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initLocationURLDenyRules = (
  sequelize: Sequelize
): ModelCtor<LocationURLDenyRulesInstance> => {
  return sequelize.define<LocationURLDenyRulesInstance>(
    'LocationURLDenyRules',
    {
      rule: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
    },
    {
      tableName: 'LocationURLDenyRules',
      timestamps: false,
    }
  );
};
