import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import type { Models } from '..';

type Attributes = CreationAttributes & {
  uuid: string;
  closed: boolean;
  discarded: boolean;
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  operatorId: string;
  email?: string;
  type?: string;
};

export interface EmailActivationInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initEmailActivations = (
  sequelize: Sequelize
): ModelCtor<EmailActivationInstance> => {
  return sequelize.define<EmailActivationInstance>('EmailActivation', {
    uuid: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    operatorId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    email: {
      type: DataTypes.CITEXT,
      allowNull: true,
    },
    type: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    closed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    discarded: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};

export const associateEmailActivation = (models: Models): void => {
  models.EmailActivation.belongsTo(models.Operator, {
    foreignKey: 'operatorId',
    onDelete: 'CASCADE',
  });
};
