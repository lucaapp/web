import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = { zip: string };

export interface SupportedZipCodesInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initSupportedZipCodes = (
  sequelize: Sequelize
): ModelCtor<SupportedZipCodesInstance> => {
  return sequelize.define<SupportedZipCodesInstance>('SupportedZipCode', {
    zip: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};
