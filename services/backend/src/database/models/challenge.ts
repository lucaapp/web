import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import {
  ChallengeType,
  OperatorDeviceCreationChallengeState,
} from 'constants/challenges';

type Attributes = CreationAttributes & {
  uuid: string;
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  type: ChallengeType;
  state: OperatorDeviceCreationChallengeState;
};

export interface ChallengeInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initChallenges = (
  sequelize: Sequelize
): ModelCtor<ChallengeInstance> => {
  return sequelize.define<ChallengeInstance>('Challenge', {
    uuid: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    type: {
      allowNull: false,
      type: DataTypes.STRING(32),
    },
    state: {
      allowNull: false,
      type: DataTypes.STRING(32),
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};
