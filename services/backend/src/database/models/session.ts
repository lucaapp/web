import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  sid: string;
  userId: string;
  expires: Date;
  data: string;
  type: string;
};

export interface SessionInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initSessions = (
  sequelize: Sequelize
): ModelCtor<SessionInstance> => {
  return sequelize.define<SessionInstance>('Session', {
    sid: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    userId: { type: DataTypes.UUID },
    expires: { type: DataTypes.DATE },
    data: { type: DataTypes.TEXT },
    type: { type: DataTypes.STRING(255) },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};
