import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';
import type { Models } from '..';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  departmentId: string;
  level: number;
  language: string;
  key: string;
  content: string;
};

export interface NotificationMessageInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initNotificationMessages = (
  sequelize: Sequelize
): ModelCtor<NotificationMessageInstance> => {
  return sequelize.define<NotificationMessageInstance>('NotificationMessage', {
    departmentId: {
      type: DataTypes.UUID,
      primaryKey: true,
    },
    level: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
    },
    language: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    key: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    content: {
      allowNull: false,
      type: DataTypes.TEXT,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};

export const associateNotificationMessage = (models: Models): void => {
  models.NotificationMessage.belongsTo(models.HealthDepartment, {
    foreignKey: 'departmentId',
  });
};
