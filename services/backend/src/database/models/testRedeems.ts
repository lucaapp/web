import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

type Attributes = CreationAttributes & {
  createdAt?: Date;
  updatedAt: Date;
};

type CreationAttributes = { hash: string; tag: string };

export interface TestRedeemInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initTestRedeems = (
  sequelize: Sequelize
): ModelCtor<TestRedeemInstance> => {
  return sequelize.define<TestRedeemInstance>('TestRedeem', {
    hash: {
      type: DataTypes.STRING(44),
      allowNull: false,
      primaryKey: true,
    },
    tag: {
      type: DataTypes.STRING(24),
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};
