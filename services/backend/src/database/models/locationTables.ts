import { Sequelize, Model, ModelStatic, DataTypes } from 'sequelize';
import type { Models } from '..';

interface Attributes {
  locationId: string;
  internalNumber: number;
  id: string | null;
  customName: string | null;
  createdAt: Date;
  updatedAt: Date;
}

type CreationAttributes = Omit<Attributes, 'id' | 'createdAt' | 'updatedAt'> & {
  id: string;
};

export interface LocationTableInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initLocationTables = (
  sequelize: Sequelize
): ModelStatic<LocationTableInstance> => {
  return sequelize.define<LocationTableInstance>('LocationTable', {
    locationId: {
      allowNull: false,
      type: DataTypes.UUID,
      primaryKey: true,
    },
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    internalNumber: {
      allowNull: false,
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    customName: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};

export const associateLocationTables = (models: Models): void => {
  models.LocationTables.belongsTo(models.Location, {
    foreignKey: 'locationId',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  });
};
