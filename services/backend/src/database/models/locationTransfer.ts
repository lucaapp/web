import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';
import type { HealthDepartmentInstance } from './healthDepartment';
import type { LocationInstance } from './location';
import type { LocationTransferTraceInstance } from './locationTransferTrace';
import type { TracingProcessInstance } from './tracingProcess';
import type { SeenOverlappingLocationTranferInstance } from './seenOverlappingLocationTranfers';
import type { Models } from '..';
import {
  LocationMaskTypes,
  LocationEntryPolicyInfoTypes,
  LocationVentilationTypes,
} from '../../constants/location';

interface TimerangeDate extends Date {
  value?: string;
  inclusive?: boolean;
}

interface Attributes {
  uuid: string;
  departmentId: string;
  tracingProcessId: string;
  locationId: string;
  time: [TimerangeDate, TimerangeDate];
  isCompleted: boolean;
  initialTracesCount: number | null;
  masks: LocationMaskTypes;
  ventilation: LocationVentilationTypes;
  roomHeight: number;
  roomWidth: number;
  roomDepth: number;
  contactedAt?: Date;
  signedLocationTransfer?: string;
  approvedAt?: Date;
  entryPolicyInfo?: LocationEntryPolicyInfoTypes;
  isIndoor?: boolean;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date | null;
}

interface CreationAttributes {
  departmentId: string;
  tracingProcessId: string;
  locationId: string;
  time: [TimerangeDate, TimerangeDate];
  initialTracesCount: number;
  signedLocationTransfer?: string;
  masks?: LocationMaskTypes;
  ventilation?: LocationVentilationTypes;
  roomHeight?: number;
  roomWidth?: number;
  roomDepth?: number;
  entryPolicyInfo?: LocationEntryPolicyInfoTypes;
  isIndoor?: boolean;
}

export interface LocationTransferInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  Location?: LocationInstance;
  HealthDepartment?: HealthDepartmentInstance;
  TracingProcess?: TracingProcessInstance;
  LocationTransferTraces?: Array<LocationTransferTraceInstance>;
  SeenOverlappingLocationTranfers?: Array<SeenOverlappingLocationTranferInstance>;
}

export const initLocationTransfers = (
  sequelize: Sequelize
): ModelCtor<LocationTransferInstance> => {
  return sequelize.define<LocationTransferInstance>(
    'LocationTransfer',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      departmentId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      tracingProcessId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      locationId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      time: {
        type: DataTypes.RANGE(DataTypes.DATE),
        allowNull: false,
      },
      isCompleted: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      contactedAt: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: null,
      },
      signedLocationTransfer: {
        type: DataTypes.STRING(1024),
        allowNull: true,
        defaultValue: null,
      },
      approvedAt: {
        type: DataTypes.DATE,
        allowNull: true,
        defaultValue: null,
      },
      initialTracesCount: {
        type: DataTypes.INTEGER,
        allowNull: true,
        defaultValue: null,
      },
      masks: {
        type: DataTypes.STRING,
        allowNull: true,
        defaultValue: null,
      },
      ventilation: {
        type: DataTypes.STRING,
        allowNull: true,
        defaultValue: null,
      },
      roomHeight: {
        type: DataTypes.FLOAT,
        allowNull: true,
        defaultValue: null,
      },
      roomWidth: {
        type: DataTypes.FLOAT,
        allowNull: true,
        defaultValue: null,
      },
      roomDepth: {
        type: DataTypes.FLOAT,
        allowNull: true,
        defaultValue: null,
      },
      isIndoor: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: null,
      },
      entryPolicyInfo: {
        type: DataTypes.STRING,
        allowNull: true,
        defaultValue: null,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      deletedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: true,
    }
  );
};

export const associateLocationTransfer = (models: Models): void => {
  models.LocationTransfer.belongsTo(models.Location, {
    foreignKey: 'locationId',
    onDelete: 'CASCADE',
  });

  models.LocationTransfer.belongsTo(models.TracingProcess, {
    foreignKey: 'tracingProcessId',
    onDelete: 'CASCADE',
  });

  models.LocationTransfer.hasMany(models.LocationTransferTrace, {
    foreignKey: 'locationTransferId',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  });

  models.LocationTransfer.hasMany(models.SeenOverlappingLocationTranfers, {
    foreignKey: 'locationTransferId',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  });

  models.LocationTransfer.belongsTo(models.HealthDepartment, {
    foreignKey: 'departmentId',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  });
};
