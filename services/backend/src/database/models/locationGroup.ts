import { Sequelize, Model, DataTypes, ModelStatic } from 'sequelize';
import type { LocationType } from 'constants/location';
import { OpeningHours, openingHoursSchema } from 'utils/openingHours';
import type { LocationInstance } from './location';
import type { OperatorInstance } from './operator';
import type { Models } from '..';

interface Attributes {
  uuid: string;
  operatorId: string;
  individualOpeningHours: boolean;
  name?: string;
  type?: LocationType;
  deletedAt?: Date | null;
  openingHours?: OpeningHours | null;
  discoverId: string;
  discoveryEnabled?: boolean | null;
  createdAt: Date;
  updatedAt: Date;
}

interface CreationAttributes {
  operatorId: string;
  name?: string;
  individualOpeningHours?: boolean;
  openingHours?: OpeningHours | null;
  type?: LocationType;
}

export interface LocationGroupInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  BaseLocation?: LocationInstance;
  Locations?: Array<LocationInstance>;
  Operator?: OperatorInstance;
}

export const initLocationGroups = (
  sequelize: Sequelize
): ModelStatic<LocationGroupInstance> => {
  return sequelize.define<LocationGroupInstance>(
    'LocationGroup',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      operatorId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING,
      },
      type: {
        type: DataTypes.STRING,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      deletedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      openingHours: {
        type: DataTypes.JSON,
        allowNull: true,
        defaultValue: null,
        validate: {
          customValidator(value: unknown) {
            openingHoursSchema.parse(value);
          },
        },
      },
      discoverId: {
        type: DataTypes.UUID,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
      },
      discoveryEnabled: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      individualOpeningHours: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    },
    {
      paranoid: true,
      hooks: {
        afterUpdate: (instance, { transaction }) => {
          if (
            (instance.changed('individualOpeningHours') ||
              instance.changed('openingHours')) &&
            !instance.get('individualOpeningHours')
          ) {
            // update all locations of this group with opening hours
            const uuid = instance.get('uuid');

            instance.sequelize.models.Location.update(
              {
                openingHours: instance.get('openingHours'),
              },
              {
                where: {
                  groupId: uuid,
                },
                transaction,
              }
            );
          }
        },
      },
    }
  );
};

export const associateLocationGroup = (models: Models): void => {
  models.LocationGroup.belongsTo(models.Operator, {
    foreignKey: 'operatorId',
    onDelete: 'CASCADE',
  });

  models.LocationGroup.hasMany(models.Location, {
    foreignKey: 'groupId',
  });

  models.LocationGroup.hasOne(models.Location, {
    foreignKey: 'groupId',
    as: 'BaseLocation',
  });

  models.LocationGroup.hasMany(models.LocationGroupEmployee, {
    foreignKey: 'locationGroupId',
  });
};
