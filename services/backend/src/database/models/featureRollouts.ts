import { Sequelize, Model, ModelStatic, DataTypes } from 'sequelize';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = { name: string; percentage: number; active: boolean };

export interface FeatureRolloutInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initFeatureRollouts = (
  sequelize: Sequelize
): ModelStatic<FeatureRolloutInstance> => {
  return sequelize.define<FeatureRolloutInstance>('FeatureRollout', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    percentage: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};
