import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';
import type { Models } from '..';

interface Attributes {
  departmentId: string;
  uuid: string;
  level: number;
  key: string;
  value: string;
  createdAt: Date;
  updatedAt: Date;
}

type CreationAttributes = {
  departmentId: string;
  level: number;
  key: string;
  value: string;
};

export interface NotificationConfigInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initNotificationConfigs = (
  sequelize: Sequelize
): ModelCtor<NotificationConfigInstance> => {
  return sequelize.define<NotificationConfigInstance>('NotificationConfig', {
    uuid: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    departmentId: {
      type: DataTypes.UUID,
    },
    level: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    key: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    value: {
      allowNull: false,
      type: DataTypes.TEXT,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};

export const associateNotificationConfig = (models: Models): void => {
  models.NotificationMessage.belongsTo(models.HealthDepartment, {
    foreignKey: 'departmentId',
  });
};
