import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  keyId: number;
  healthDepartmentId: string;
  issuerId: string;
  data: string;
  iv: string;
  mac: string;
  publicKey: string;
  signature: string;
  signedEncryptedPrivateDailyKey?: string;
};

export interface EncryptedDailyPrivateKeyInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initEncryptedDailyPrivateKeys = (
  sequelize: Sequelize
): ModelCtor<EncryptedDailyPrivateKeyInstance> => {
  return sequelize.define<EncryptedDailyPrivateKeyInstance>(
    'EncryptedDailyPrivateKey',
    {
      keyId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
        unique: 'primaryKey',
        primaryKey: true,
      },
      healthDepartmentId: {
        type: DataTypes.UUID,
        allowNull: false,
        unique: 'primaryKey',
        primaryKey: true,
      },
      issuerId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      data: {
        type: DataTypes.STRING(44),
        allowNull: false,
      },
      iv: {
        type: DataTypes.STRING(24),
        allowNull: false,
      },
      mac: {
        type: DataTypes.STRING(44),
        allowNull: false,
      },
      publicKey: {
        type: DataTypes.STRING(88),
        allowNull: false,
      },
      signature: {
        type: DataTypes.STRING(120),
        allowNull: false,
      },
      signedEncryptedPrivateDailyKey: {
        type: DataTypes.STRING(1024),
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    }
  );
};
