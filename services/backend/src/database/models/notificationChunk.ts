import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

type Attributes = CreationAttributes & {
  createdAt: Date;
  updatedAt: Date;
};

type CreationAttributes = {
  chunk: string | Buffer;
  hash: string;
};

export interface NotificationChunkInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initNotificationChunks = (
  sequelize: Sequelize
): ModelCtor<NotificationChunkInstance> => {
  return sequelize.define<NotificationChunkInstance>('NotificationChunk', {
    chunk: {
      type: DataTypes.BLOB,
      allowNull: false,
    },
    hash: {
      type: DataTypes.STRING(24),
      allowNull: false,
      primaryKey: true,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: DataTypes.NOW,
    },
  });
};
