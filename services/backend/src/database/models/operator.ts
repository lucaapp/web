/* eslint-disable max-lines */
import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';
import { UserType } from 'constants/user';
import { hashPassword } from 'utils/hash';
import { DEFAULT_LANGUAGE, InternalLanguages } from 'utils/language';
import type { LocationGroupInstance } from 'database/models/locationGroup';
import type { LocationInstance } from './location';
import type { Models } from '..';

interface Attributes {
  uuid: string;
  firstName: string;
  lastName: string;
  fullName: string;
  businessEntityName?: string;
  businessEntityStreetName?: string;
  businessEntityStreetNumber?: string;
  businessEntityZipCode?: string;
  businessEntityCity?: string;
  email: string;
  username: string;
  password: string;
  salt: string;
  activated: boolean;
  supportCode: string;
  lastVersionSeen: string;
  isTrusted: boolean;
  transactionalLanguage: InternalLanguages;
  publicKey?: string;
  privateKeySecret?: string;
  avvAccepted?: boolean;
  lastSeenPrivateKey?: Date;
  language?: string;
  languageOverwrite?: string;
  phone?: string;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date | null;
  bannedAt: Date | null;
}

interface CreationAttributes {
  uuid?: string;
  firstName: string;
  lastName: string;
  businessEntityName: string;
  businessEntityStreetName: string;
  businessEntityStreetNumber: string;
  businessEntityZipCode: string;
  businessEntityCity: string;
  email: string;
  username: string;
  password: string;
  phone?: string;
  salt: string;
  supportCode: string;
  lastVersionSeen?: string;
  isTrusted?: boolean;
  publicKey?: string;
  privateKeySecret?: string;
  avvAccepted?: boolean;
  language?: string;
}

export interface OperatorInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  checkPassword: (testPassword: string) => Promise<boolean>;

  LocationGroups?: Array<LocationGroupInstance>;
  Locations?: Array<LocationInstance>;
}

const hashPasswordHook = async (instance: OperatorInstance) => {
  if (!instance.changed('password')) return;
  const password = instance.get('password');
  const salt = instance.get('salt');
  const hash = await hashPassword(password, salt);
  await instance.set('password', hash.toString('base64'));
};

const destroySessionsHook = async (instance: OperatorInstance) => {
  if (!(instance.changed('password') || instance.changed('email'))) return;
  await instance.sequelize.models.Session.destroy({
    where: {
      userId: instance.get('uuid'),
      type: UserType.OPERATOR,
    },
  });
};

export const initOperators = (
  sequelize: Sequelize
): ModelCtor<OperatorInstance> => {
  const model = sequelize.define<OperatorInstance>(
    'Operator',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      firstName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      lastName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      phone: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      fullName: {
        type: DataTypes.VIRTUAL,
        get() {
          return `${this.firstName} ${this.lastName}`;
        },
        set() {
          throw new Error('Do not try to set the `fullName` value!');
        },
      },
      businessEntityName: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      businessEntityStreetName: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      businessEntityStreetNumber: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      businessEntityZipCode: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      businessEntityCity: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      email: {
        type: DataTypes.CITEXT,
        allowNull: false,
      },
      username: {
        type: DataTypes.CITEXT,
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      salt: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      publicKey: {
        type: DataTypes.STRING(88),
      },
      activated: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      privateKeySecret: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.STRING(44),
      },
      supportCode: {
        allowNull: false,
        type: DataTypes.STRING(12),
      },
      avvAccepted: {
        allowNull: false,
        defaultValue: false,
        type: DataTypes.BOOLEAN,
      },
      lastVersionSeen: {
        allowNull: false,
        defaultValue: '1.0.0',
        type: DataTypes.STRING(32),
      },
      isTrusted: {
        allowNull: false,
        defaultValue: false,
        type: DataTypes.BOOLEAN,
      },
      lastSeenPrivateKey: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.DATE,
      },
      language: {
        allowNull: true,
        type: DataTypes.STRING(5),
        defaultValue: null,
      },
      transactionalLanguage: {
        type: DataTypes.VIRTUAL,
        get() {
          return (
            this.languageOverwrite ||
            this.language ||
            DEFAULT_LANGUAGE
          ).split('-')[0];
        },
        set() {
          throw new Error(
            'Do not try to set the `transactionalLanguage` value!'
          );
        },
      },
      languageOverwrite: {
        allowNull: true,
        type: DataTypes.STRING(5),
        defaultValue: null,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      deletedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      bannedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      paranoid: true,
      hooks: {
        beforeCreate: hashPasswordHook,
        beforeUpdate: hashPasswordHook,
        afterUpdate: destroySessionsHook,
      },
    }
  );

  model.prototype.checkPassword = async function checkPassword(
    testPassword: string
  ) {
    const hash = await hashPassword(testPassword, this.get('salt'));
    return hash.toString('base64') === this.get('password');
  };

  return model;
};

export const associateOperator = (models: Models): void => {
  models.Operator.hasMany(models.LocationGroup, {
    foreignKey: 'operatorId',
    onDelete: 'CASCADE',
  });
  models.Operator.hasMany(models.Location, {
    foreignKey: 'operator',
    onDelete: 'CASCADE',
  });
};
