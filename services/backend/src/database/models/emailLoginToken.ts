import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import type { Models } from '..';
import type { OperatorInstance } from './operator';

interface Attributes {
  token: string;
  operatorId: string;
  usedAt: Date | null;
  createdAt: Date;
}

type CreationAttributes = {
  token: string;
  operatorId: string;
};

export interface EmailLoginTokenInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  Operator?: OperatorInstance;
}

export const initEmailLoginToken = (
  sequelize: Sequelize
): ModelCtor<EmailLoginTokenInstance> => {
  return sequelize.define<EmailLoginTokenInstance>(
    'EmailLoginToken',
    {
      token: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
      operatorId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      usedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
    },
    {
      timestamps: false,
    }
  );
};

export const associateEmailLoginToken = (models: Models): void => {
  models.EmailLoginToken.belongsTo(models.Operator, {
    foreignKey: 'operatorId',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  });
};
