import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import type { HealthDepartmentInstance } from './healthDepartment';
import type { Models } from '..';

interface CreationAttributes {
  traceId: string;
  healthDepartmentId: string;
}

interface Attributes extends CreationAttributes {
  createdAt: Date;
  updatedAt: Date;
}

export interface DummyTraceInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  HealthDepartment?: HealthDepartmentInstance;
}

export const initDummyTraces = (
  sequelize: Sequelize
): ModelCtor<DummyTraceInstance> => {
  // @ts-ignore sequelize types do not account for automatic timestamp addition
  return sequelize.define<DummyTraceInstance>('DummyTrace', {
    traceId: {
      type: DataTypes.STRING(24),
      allowNull: false,
      primaryKey: true,
    },
    healthDepartmentId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
  });
};

export const associateDummyTrace = (models: Models): void => {
  models.DummyTrace.belongsTo(models.HealthDepartment, {
    foreignKey: 'healthDepartmentId',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  });
};
