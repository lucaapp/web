import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addIndex('LocationTransfers', {
      fields: ['departmentId'],
      concurrently: true,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeIndex(
      'LocationTransfers',
      'location_transfers_department_id'
    );
  },
};

export default migration;
