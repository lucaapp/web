import { Migration } from 'types/umzug';

const migration: Migration = {
  up: ({ context: queryInterface }) => {
    return queryInterface.sequelize.query(
      `ALTER TYPE "enum_Locations_entryPolicy" ADD VALUE '2GPlus'`
    );
  },

  down: ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.sequelize.query(
        `UPDATE "Locations" SET "entryPolicy" = '2G' WHERE "entryPolicy" = '2GPlus'`,
        {
          transaction,
        }
      );
      await queryInterface.sequelize.query(
        `
        DELETE
        FROM
            pg_enum
        WHERE
            enumlabel = '2GPlus' AND
            enumtypid = (
                SELECT
                    oid
                FROM
                    pg_type
                WHERE
                    typname = 'enum_Locations_entryPolicy'
            )
    `,
        {
          transaction,
        }
      );
    });
  },
};

export default migration;
