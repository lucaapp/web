import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'OperatorDevices',
        'refreshedAtBefore',
        {
          allowNull: true,
          type: DataTypes.DATE,
        },
        { transaction }
      );

      await queryInterface.sequelize.query(
        'UPDATE "OperatorDevices" SET "refreshedAtBefore" = "refreshedAt"',
        { transaction }
      );
      await queryInterface.changeColumn(
        'OperatorDevices',
        'refreshedAtBefore',
        {
          allowNull: false,
          type: DataTypes.DATE,
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn(
        'OperatorDevices',
        'refreshedAtBefore',
        { transaction }
      );
    });
  },
};

export default migration;
