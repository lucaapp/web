import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const name = 'LocationTransferTraces_traceId_Traces_fk';
const migration: Migration = {
  up: ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(() => {
      return Promise.all([
        queryInterface.changeColumn('LocationTransferTraces', 'traceId', {
          allowNull: true,
          type: DataTypes.STRING(24),
        }),
        queryInterface.addConstraint('LocationTransferTraces', {
          fields: ['traceId'],
          type: 'foreign key',
          name,
          references: {
            table: 'Traces',
            field: 'traceId',
          },
          onDelete: 'set null',
          onUpdate: 'no action',
        }),
      ]);
    });
  },

  down: ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(() => {
      return Promise.all([
        queryInterface.removeConstraint('LocationTransferTraces', name),
        queryInterface.changeColumn('LocationTransferTraces', 'traceId', {
          allowNull: false,
          type: DataTypes.STRING(24),
        }),
      ]);
    });
  },
};

export default migration;
