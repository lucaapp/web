import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addIndex('Users', {
      name: 'users_device_type_index',
      fields: ['deviceType'],
      concurrently: true,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeIndex('Users', 'users_device_type_index');
  },
};

export default migration;
