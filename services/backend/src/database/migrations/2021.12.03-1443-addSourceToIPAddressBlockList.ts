import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'IPAddressBlockList',
        'source',
        {
          allowNull: true,
          type: DataTypes.STRING(250),
          defaultValue: null,
        },
        {
          transaction,
        }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('IPAddressBlockList', 'source', {
        transaction,
      });
    });
  },
};

export default migration;
