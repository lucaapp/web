import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'LocationURLDenyRules',
        {
          rule: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
          },
        },
        { transaction }
      );

      await queryInterface.addIndex('LocationURLDenyRules', {
        fields: ['rule'],
        transaction,
      });
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('LocationURLDenyRules');
  },
};

export default migration;
