import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('Traces', 'authPublicKey', {
      type: DataTypes.STRING(88),
      allowNull: true,
    });
  },
  down: async ({ context: queryInterface }) => {
    return queryInterface.removeColumn('Traces', 'authPublicKey');
  },
};

export default migration;
