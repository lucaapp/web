import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    return queryInterface.changeColumn('Locations', 'publicKey', {
      type: DataTypes.STRING(88),
      allowNull: true,
    });
  },

  down: async ({ context: queryInterface }) => {
    return queryInterface.changeColumn('Locations', 'publicKey', {
      type: DataTypes.STRING(88),
      allowNull: false,
    });
  },
};

export default migration;
