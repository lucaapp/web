import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) =>
    queryInterface.removeColumn('Operators', 'allowOperatorDevices'),

  down: ({ context: queryInterface }) => {
    return queryInterface.addColumn('Operators', 'allowOperatorDevices', {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    });
  },
};

export default migration;
