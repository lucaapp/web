import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'LocationTransferTraces',
        'isHDEncrypted',
        {
          type: DataTypes.BOOLEAN,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'LocationTransferTraces',
        'dataPublicKey',
        {
          type: DataTypes.STRING(88),
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'LocationTransferTraces',
        'dataMAC',
        {
          type: DataTypes.STRING(44),
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'LocationTransferTraces',
        'dataIV',
        {
          type: DataTypes.STRING(24),
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn(
        'LocationTransferTraces',
        'isHDEncrypted',
        { transaction }
      );
      await queryInterface.removeColumn(
        'LocationTransferTraces',
        'dataPublicKey',
        { transaction }
      );
      await queryInterface.removeColumn('LocationTransferTraces', 'dataMAC', {
        transaction,
      });
      await queryInterface.removeColumn('LocationTransferTraces', 'expireAt', {
        transaction,
      });
      await queryInterface.removeColumn('LocationTransferTraces', 'dataIV', {
        transaction,
      });
    });
  },
};

export default migration;
