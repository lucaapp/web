import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'LocationTransfers',
        'entryPolicyInfo',
        {
          allowNull: true,
          type: DataTypes.STRING,
          defaultValue: null,
        },
        {
          transaction,
        }
      );
      await queryInterface.addColumn(
        'LocationTransfers',
        'isIndoor',
        {
          allowNull: true,
          type: DataTypes.BOOLEAN,
          defaultValue: null,
        },
        {
          transaction,
        }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn(
        'LocationTransfers',
        'entryPolicyInfo',
        {
          transaction,
        }
      );
      await queryInterface.removeColumn('LocationTransfers', 'isIndoor', {
        transaction,
      });
    });
  },
};

export default migration;
