import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('IPAddressDenyList');
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.createTable('IPAddressDenyList', {
      ipStart: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      ipEnd: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
    });
  },
};

export default migration;
