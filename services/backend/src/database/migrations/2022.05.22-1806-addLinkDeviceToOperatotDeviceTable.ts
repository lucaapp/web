import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.changeColumn(
        'OperatorDevices',
        'role',
        {
          type: DataTypes.TEXT,
        },
        { transaction }
      );
      await queryInterface.sequelize.query(
        'DROP TYPE "enum_OperatorDevices_role";',
        { transaction }
      );
      await queryInterface.changeColumn(
        'OperatorDevices',
        'role',
        {
          type: DataTypes.ENUM('scanner', 'employee', 'manager', 'link'),
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.changeColumn(
        'OperatorDevices',
        'role',
        {
          type: DataTypes.TEXT,
        },
        { transaction }
      );
      await queryInterface.sequelize.query(
        'DROP TYPE "enum_OperatorDevices_role;"',
        { transaction }
      );
      await queryInterface.changeColumn(
        'OperatorDevices',
        'role',
        {
          type: DataTypes.ENUM('scanner', 'employee', 'manager'),
        },
        { transaction }
      );
    });
  },
};

export default migration;
