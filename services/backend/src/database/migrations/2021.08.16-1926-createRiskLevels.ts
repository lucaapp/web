import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('RiskLevels', {
      locationTransferTraceId: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      level: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
    });
    await queryInterface.addConstraint('RiskLevels', {
      fields: ['locationTransferTraceId'],
      type: 'foreign key',
      references: {
        table: 'LocationTransferTraces',
        field: 'uuid',
      },
      onDelete: 'cascade',
      onUpdate: 'cascade',
    });
  },
  down: ({ context: queryInterface }) => queryInterface.dropTable('RiskLevels'),
};

export default migration;
