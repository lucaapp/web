import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'PowConfigs',
        {
          type: {
            type: DataTypes.STRING(255),
            allowNull: false,
            primaryKey: true,
          },
          a: {
            type: DataTypes.DOUBLE,
            allowNull: false,
          },
          b: {
            type: DataTypes.DOUBLE,
            allowNull: false,
          },
          c: {
            type: DataTypes.DOUBLE,
            allowNull: false,
          },
        },
        { transaction }
      );
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('PowConfigs');
  },
};

export default migration;
