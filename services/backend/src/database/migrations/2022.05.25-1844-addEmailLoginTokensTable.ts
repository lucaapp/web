import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const INDEX_NAME = 'EmailLoginTokens_operatorId_index';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'EmailLoginTokens',
        {
          token: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
          },
          operatorId: {
            type: DataTypes.UUID,
            allowNull: false,
            references: {
              model: 'Operators',
              key: 'uuid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
          usedAt: {
            type: DataTypes.DATE,
            allowNull: true,
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: literal('CURRENT_TIMESTAMP'),
          },
        },
        { transaction }
      );

      await queryInterface.addIndex('EmailLoginTokens', ['operatorId'], {
        name: INDEX_NAME,
        transaction,
      });
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('EmailLoginTokens');
  },
};

export default migration;
