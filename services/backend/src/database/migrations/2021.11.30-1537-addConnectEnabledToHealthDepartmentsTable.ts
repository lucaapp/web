import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn(
      'HealthDepartments',
      'connectEnabled',

      {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      }
    );
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn('HealthDepartments', 'connectEnabled');
  },
};

export default migration;
