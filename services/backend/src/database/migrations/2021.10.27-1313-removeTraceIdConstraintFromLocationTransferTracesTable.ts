import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.removeConstraint(
      'LocationTransferTraces',
      'LocationTransferTraces_traceId_Traces_fk'
    );
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.addConstraint('LocationTransferTraces', {
      fields: ['traceId'],
      type: 'foreign key',
      name: 'LocationTransferTraces_traceId_Traces_fk',
      references: {
        table: 'Traces',
        field: 'traceId',
      },
      onDelete: 'set null',
      onUpdate: 'no action',
    });
  },
};

export default migration;
