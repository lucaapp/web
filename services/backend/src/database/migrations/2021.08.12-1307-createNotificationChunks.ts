import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: ({ context: queryInterface }) =>
    queryInterface.createTable('NotificationChunks', {
      chunk: {
        type: DataTypes.BLOB,
        allowNull: false,
      },
      hash: {
        type: DataTypes.STRING(24),
        allowNull: false,
        primaryKey: true,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
    }),
  down: ({ context: queryInterface }) =>
    queryInterface.dropTable('NotificationChunks'),
};

export default migration;
