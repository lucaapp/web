import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('BadgeRegistrators');
  },

  down: ({ context: queryInterface }) => {
    return queryInterface.createTable('BadgeRegistrators', {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      name: {
        type: DataTypes.STRING(255),
      },
    });
  },
};

export default migration;
