import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('Locations', 'averageCheckinTime', {
      type: DataTypes.INTEGER,
      defaultValue: null,
      allowNull: true,
    });
  },
  down: async ({ context: queryInterface }) => {
    return queryInterface.removeColumn('Locations', 'averageCheckinTime');
  },
};

export default migration;
