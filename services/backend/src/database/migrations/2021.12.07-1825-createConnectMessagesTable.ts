import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'ConnectMessages',
        {
          messageId: {
            type: DataTypes.STRING(24),
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
          },
          conversationId: {
            type: DataTypes.UUID,
            allowNull: false,
            references: {
              model: 'ConnectConversations',
              key: 'uuid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
          data: {
            type: DataTypes.STRING(4096),
            allowNull: false,
          },
          iv: {
            type: DataTypes.STRING(24),
            allowNull: false,
          },
          mac: {
            type: DataTypes.STRING(44),
            allowNull: false,
          },
          readSignature: {
            type: DataTypes.STRING(120),
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: literal('CURRENT_TIMESTAMP'),
          },
          receivedAt: {
            type: DataTypes.DATE,
          },
          readAt: {
            type: DataTypes.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.addIndex('ConnectMessages', {
        fields: ['conversationId'],
        transaction,
      });
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('ConnectMessages');
  },
};

export default migration;
