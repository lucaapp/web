import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await Promise.all([
        queryInterface.changeColumn(
          'HealthDepartmentEmployees',
          'firstName',
          {
            type: DataTypes.STRING,
            allowNull: true,
          },
          { transaction }
        ),
        queryInterface.changeColumn(
          'HealthDepartmentEmployees',
          'lastName',
          {
            type: DataTypes.STRING,
            allowNull: true,
          },
          { transaction }
        ),
        queryInterface.changeColumn(
          'HealthDepartmentEmployees',
          'phone',
          {
            type: DataTypes.STRING,
            allowNull: true,
          },
          { transaction }
        ),
      ]);
    });
  },

  down: ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await Promise.all([
        queryInterface.changeColumn(
          'HealthDepartmentEmployees',
          'firstName',
          {
            type: DataTypes.STRING,
            allowNull: false,
          },
          { transaction }
        ),
        queryInterface.changeColumn(
          'HealthDepartmentEmployees',
          'lastName',
          {
            type: DataTypes.STRING,
            allowNull: false,
          },
          { transaction }
        ),
        queryInterface.changeColumn(
          'HealthDepartmentEmployees',
          'phone',
          {
            type: DataTypes.STRING,
            allowNull: false,
          },
          { transaction }
        ),
      ]);
    });
  },
};

export default migration;
