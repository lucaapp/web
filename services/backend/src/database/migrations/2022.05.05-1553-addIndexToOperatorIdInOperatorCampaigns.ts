import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addIndex('OperatorCampaigns', ['operatorId'], {
      name: 'OperatorCampaigns_operatorId_index',
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeIndex(
      'OperatorCampaigns',
      'OperatorCampaigns_operatorId_index'
    );
  },
};

export default migration;
