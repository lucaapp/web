import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addIndex('LocationTransferTraces', {
      fields: ['locationTransferId'],
      concurrently: true,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeIndex(
      'LocationTransferTraces',
      'location_transfer_traces_location_transfer_id'
    );
  },
};

export default migration;
