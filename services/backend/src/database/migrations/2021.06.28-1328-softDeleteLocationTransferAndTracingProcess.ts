import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'LocationTransfers',
        'deletedAt',
        {
          allowNull: true,
          type: DataTypes.DATE,
        },
        {
          transaction,
        }
      );

      await queryInterface.addColumn(
        'TracingProcesses',
        'deletedAt',
        {
          allowNull: true,
          type: DataTypes.DATE,
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('LocationTransfers', 'deletedAt', {
        transaction,
      });
      await queryInterface.removeColumn('TracingProcesses', 'deletedAt', {
        transaction,
      });
    });
  },
};

export default migration;
