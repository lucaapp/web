import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('RetentionPolicies', {
      state: {
        allowNull: false,
        type: DataTypes.STRING,
        primaryKey: true,
      },
      retentionPeriod: {
        allowNull: false,
        type: DataTypes.INTEGER,
      },
    });

    await queryInterface.addIndex('RetentionPolicies', {
      fields: ['state'],
      concurrently: true,
    });
  },
  down: ({ context: queryInterface }) => {
    return queryInterface.dropTable('RetentionPolicies');
  },
};

export default migration;
