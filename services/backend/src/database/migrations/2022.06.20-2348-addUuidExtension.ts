import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.query(
      `CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`
    );
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.query(
      `DROP EXTENSION IF EXISTS "uuid-ossp";`
    );
  },
};

export default migration;
