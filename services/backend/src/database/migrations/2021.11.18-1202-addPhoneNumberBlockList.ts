import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'PhoneNumberBlockList',
        {
          phoneNumber: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: literal('CURRENT_TIMESTAMP'),
          },
          updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: literal('CURRENT_TIMESTAMP'),
          },
        },
        { transaction }
      );
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('PhoneNumberBlockList');
  },
};

export default migration;
