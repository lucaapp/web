import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('HealthDepartments', 'email', {
      allowNull: true,
      type: DataTypes.CITEXT,
    });
    await queryInterface.addColumn('HealthDepartments', 'phone', {
      allowNull: true,
      type: DataTypes.STRING,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn('HealthDepartments', 'email');
    await queryInterface.removeColumn('HealthDepartments', 'phone');
  },
};

export default migration;
