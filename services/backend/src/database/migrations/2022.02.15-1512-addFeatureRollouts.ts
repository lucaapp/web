import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) =>
    queryInterface.createTable('FeatureRollouts', {
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
      percentage: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      active: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
    }),

  down: async ({ context: queryInterface }) =>
    queryInterface.dropTable('FeatureRollouts'),
};

export default migration;
