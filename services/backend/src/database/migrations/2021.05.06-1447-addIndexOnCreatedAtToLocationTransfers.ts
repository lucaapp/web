import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addIndex('LocationTransfers', {
      fields: ['createdAt'],
      concurrently: true,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeIndex(
      'LocationTransfers',
      'location_transfers_created_at'
    );
  },
};

export default migration;
