import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('IPAddressAllowList', {
      ip: {
        type: DataTypes.INET,
        allowNull: false,
        primaryKey: true,
      },
      comment: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      rateLimitFactor: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
    });
    await queryInterface.addIndex('IPAddressAllowList', {
      fields: [{ name: 'ip', operator: 'inet_ops' }],
      using: 'gist',
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('IPAddressAllowList');
  },
};

export default migration;
