import { Migration } from 'types/umzug';
import { z } from 'zod';
import { randomBytes } from 'crypto';
import logger from 'utils/logger';

const LOCATION_TABLES_ID_BYTES = 16;

const generateLocationTableId = (): string =>
  randomBytes(LOCATION_TABLES_ID_BYTES).toString('base64url');

const locationTablesSelectResultSchema = z.tuple([
  z.array(z.unknown()),
  z.object({
    rows: z.array(
      z.object({
        locationId: z.string(),
        internalNumber: z.number().int().min(0),
      })
    ),
  }),
]);

const CHUNK_SIZE = 1000;

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    const queryNextChunk = async () => {
      const queryResult = await queryInterface.sequelize.query(
        `SELECT "locationId", "internalNumber" FROM "LocationTables" WHERE id IS NULL LIMIT ?`,
        { replacements: [CHUNK_SIZE] }
      );
      const [
        ,
        { rows: parsedLocationTables },
      ] = locationTablesSelectResultSchema.parse(queryResult);

      return parsedLocationTables;
    };

    let parsedLocationTables = await queryNextChunk();

    while (parsedLocationTables.length > 0) {
      logger.info(
        `Adding IDs to next batch of location tables (size = ${parsedLocationTables.length})`
      );
      for (const { locationId, internalNumber } of parsedLocationTables) {
        await queryInterface.sequelize.query(
          `UPDATE "LocationTables" SET "id" = ? WHERE "locationId" = ? AND "internalNumber" = ?`,
          {
            replacements: [
              generateLocationTableId(),
              locationId,
              internalNumber,
            ],
          }
        );
      }

      parsedLocationTables = await queryNextChunk();
    }
  },

  down: async () => {
    logger.warn(
      `Cannot actually revert this migration because location tables IDs have now been added to existing tables without one and we cannot differentiate which of these had one before.`
    );
  },
};

export default migration;
