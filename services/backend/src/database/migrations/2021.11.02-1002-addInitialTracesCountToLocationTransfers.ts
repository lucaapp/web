import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'LocationTransfers',
        'initialTracesCount',
        {
          allowNull: true,
          type: DataTypes.INTEGER,
          defaultValue: null,
        },
        {
          transaction,
        }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn(
        'LocationTransfers',
        'initialTracesCount',
        {
          transaction,
        }
      );
    });
  },
};

export default migration;
