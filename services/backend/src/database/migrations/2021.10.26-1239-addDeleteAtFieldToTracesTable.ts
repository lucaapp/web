import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('Traces', 'expiresAt', {
      type: DataTypes.DATE,
      allowNull: true,
    });

    await queryInterface.addIndex('Traces', {
      fields: ['expiresAt'],
      concurrently: true,
    });
  },
  down: ({ context: queryInterface }) => {
    return queryInterface.removeColumn('Traces', 'expiresAt');
  },
};

export default migration;
