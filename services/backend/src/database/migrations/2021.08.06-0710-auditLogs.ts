import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable('HealthDepartmentAuditLogs', {
        uuid: {
          type: DataTypes.UUID,
          allowNull: false,
          primaryKey: true,
          defaultValue: DataTypes.UUIDV4,
        },
        departmentId: {
          type: DataTypes.UUID,
          allowNull: false,
        },
        employeeId: {
          type: DataTypes.UUID,
          allowNull: false,
        },
        type: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        status: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        meta: {
          type: DataTypes.JSONB,
          allowNull: true,
          defaultValue: null,
        },
        createdAt: {
          allowNull: false,
          type: DataTypes.DATE,
          defaultValue: literal('CURRENT_TIMESTAMP'),
        },
        updatedAt: {
          allowNull: false,
          type: DataTypes.DATE,
          defaultValue: literal('CURRENT_TIMESTAMP'),
        },
      });

      await queryInterface.addConstraint('HealthDepartmentAuditLogs', {
        fields: ['departmentId'],
        type: 'foreign key',
        references: {
          table: 'HealthDepartments',
          field: 'uuid',
        },
        onDelete: '',
        onUpdate: '',
        transaction,
      });

      await queryInterface.addConstraint('HealthDepartmentAuditLogs', {
        fields: ['employeeId'],
        type: 'foreign key',
        references: {
          table: 'HealthDepartmentEmployees',
          field: 'uuid',
        },
        onDelete: '',
        onUpdate: '',
        transaction,
      });
    });
  },
  down: async ({ context: queryInterface }) => {
    return queryInterface.dropTable('HealthDepartmentAuditLogs');
  },
};

export default migration;
