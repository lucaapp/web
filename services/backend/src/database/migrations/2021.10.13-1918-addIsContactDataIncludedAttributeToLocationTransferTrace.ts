import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'LocationTransferTraces',
        'isContactDataIncluded',
        {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        {
          transaction,
        }
      );
    });

    await queryInterface.addIndex('LocationTransferTraces', {
      fields: ['isContactDataIncluded'],
      concurrently: true,
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn(
        'LocationTransferTraces',
        'isContactDataIncluded',
        {
          transaction,
        }
      );
    });
  },
};

export default migration;
