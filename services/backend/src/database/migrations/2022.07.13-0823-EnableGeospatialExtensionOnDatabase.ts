import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.query(
      'CREATE EXTENSION IF NOT EXISTS postgis;'
    );
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.query('DROP EXTENSION IF EXISTS postgis;');
  },
};

export default migration;
