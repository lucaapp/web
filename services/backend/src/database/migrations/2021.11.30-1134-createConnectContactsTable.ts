import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'ConnectContacts',
        {
          uuid: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
          },
          departmentId: {
            type: DataTypes.UUID,
            allowNull: false,
            references: {
              model: 'HealthDepartments',
              key: 'uuid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
          namePrefix: {
            type: DataTypes.STRING(64),
            allowNull: false,
          },
          phonePrefix: {
            type: DataTypes.STRING(64),
            allowNull: false,
          },
          authPublicKey: {
            type: DataTypes.STRING(88),
            allowNull: false,
          },
          referenceData: {
            type: DataTypes.STRING(108),
            allowNull: false,
          },
          referencePublicKey: {
            type: DataTypes.STRING(88),
            allowNull: false,
          },
          referenceIV: {
            type: DataTypes.STRING(24),
            allowNull: false,
          },
          referenceMAC: {
            type: DataTypes.STRING(44),
            allowNull: false,
          },
          dataData: {
            type: DataTypes.STRING(4096),
            allowNull: false,
          },
          dataPublicKey: {
            type: DataTypes.STRING(88),
            allowNull: false,
          },
          dataIV: {
            type: DataTypes.STRING(24),
            allowNull: false,
          },
          dataMAC: {
            type: DataTypes.STRING(44),
            allowNull: false,
          },
          signature: {
            type: DataTypes.STRING(120),
            allowNull: false,
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: literal('CURRENT_TIMESTAMP'),
          },
        },
        { transaction }
      );
      await queryInterface.addIndex('ConnectContacts', {
        fields: ['namePrefix'],
        transaction,
      });
      await queryInterface.addIndex('ConnectContacts', {
        fields: ['phonePrefix'],
        transaction,
      });
      await queryInterface.addIndex('ConnectContacts', {
        fields: ['departmentId'],
        transaction,
      });
      await queryInterface.addIndex('ConnectContacts', {
        fields: ['createdAt'],
        transaction,
      });
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('ConnectContacts');
  },
};

export default migration;
