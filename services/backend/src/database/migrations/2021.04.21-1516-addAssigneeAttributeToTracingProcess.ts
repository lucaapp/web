import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('TracingProcesses', 'assigneeId', {
      type: DataTypes.UUID,
      defaultValue: null,
      allowNull: true,
      references: {
        model: 'HealthDepartmentEmployees',
        key: 'uuid',
      },
    });
  },
  down: async ({ context: queryInterface }) => {
    return queryInterface.removeColumn('TracingProcesses', 'assigneeId');
  },
};

export default migration;
