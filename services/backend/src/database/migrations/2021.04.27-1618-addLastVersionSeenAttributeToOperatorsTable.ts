import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('Operators', 'lastVersionSeen', {
      type: DataTypes.STRING(32),
      defaultValue: '1.0.0',
      allowNull: false,
    });
  },
  down: async ({ context: queryInterface }) => {
    return queryInterface.removeColumn('Operators', 'lastVersionSeen');
  },
};

export default migration;
