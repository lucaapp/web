import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeConstraint(
        'NotificationMessages',
        'departmentId_level_language_unique',
        { transaction }
      );

      await queryInterface.addConstraint('NotificationMessages', {
        fields: ['departmentId', 'level', 'language', 'key'],
        type: 'unique',
        name: 'departmentId_level_language_unique',
        transaction,
      });
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeConstraint(
        'NotificationMessages',
        'departmentId_level_language_unique',
        { transaction }
      );
      await queryInterface.addConstraint('NotificationMessages', {
        fields: ['departmentId', 'level', 'language'],
        type: 'unique',
        name: 'departmentId_level_language_unique',
        transaction,
      });
    });
  },
};

export default migration;
