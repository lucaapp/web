import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('DailyPublicKeys', 'signedPublicDailyKey', {
      type: DataTypes.STRING(1024),
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn(
      'DailyPublicKeys',
      'signedPublicDailyKey'
    );
  },
};

export default migration;
