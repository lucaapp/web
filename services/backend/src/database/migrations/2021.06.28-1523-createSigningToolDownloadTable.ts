import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('SigningToolDownloads', {
      version: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
      downloadUrl: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: false,
      },
      hash: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: false,
      },
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('SigningToolDownloads');
  },
};

export default migration;
