import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('VaultContents', {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      version: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      expirationDate: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      encryptedPayload: {
        type: DataTypes.BLOB,
        allowNull: false,
      },
      totpSecret: {
        type: DataTypes.BLOB,
        allowNull: false,
      },
    });
  },

  down: ({ context: queryInterface }) => {
    return queryInterface.dropTable('VaultContents');
  },
};

export default migration;
