import { Migration } from 'types/umzug';
import { DataTypes, QueryTypes } from 'sequelize';

import { promisify } from 'util';
import crypto from 'crypto';
import { z } from 'zod';

const scrypt = promisify(crypto.scrypt);

const badgeGeneratorRowsSchema = z.array(z.object({ token: z.string() }));

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    const badgeGenerators = await queryInterface.sequelize.query(
      `SELECT token FROM "BadgeGenerators"`,
      { type: QueryTypes.SELECT }
    );
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'BadgeGenerators',
        'password',
        DataTypes.STRING(255),
        { transaction }
      );
      await queryInterface.addColumn(
        'BadgeGenerators',
        'salt',
        DataTypes.STRING(255),
        {
          transaction,
        }
      );
      for (const { token } of badgeGeneratorRowsSchema.parse(badgeGenerators)) {
        const salt = crypto.randomBytes(16).toString('base64');
        const hash = (await scrypt(token, salt, 64)) as Buffer;
        const password = hash.toString('base64');
        await queryInterface.sequelize.query(
          `UPDATE "BadgeGenerators" SET password = ?, salt = ? WHERE token = ?`,
          { replacements: [password, salt, token], transaction }
        );
      }
      await queryInterface.removeColumn('BadgeGenerators', 'token', {
        transaction,
      });
      await queryInterface.addConstraint('BadgeGenerators', {
        type: 'primary key',
        name: 'BadgeGenerators_pkey',
        fields: ['name'],
        transaction,
      });
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('BadgeGenerators', 'password', {
        transaction,
      });
      await queryInterface.removeColumn('BadgeGenerators', 'salt', {
        transaction,
      });
      await queryInterface.removeConstraint(
        'BadgeGenerators',
        'BadgeGenerators_pkey',
        { transaction }
      );
      // we don't want empty token values in the database, so delete all
      await queryInterface.sequelize.query(`TRUNCATE "BadgeGenerators"`, {
        transaction,
      });

      await queryInterface.addColumn(
        'BadgeGenerators',
        'token',
        {
          type: DataTypes.STRING(44),
          primaryKey: true,
        },
        { transaction }
      );
      await queryInterface.addConstraint('BadgeGenerators', {
        type: 'primary key',
        name: 'BadgeGenerators_pkey',
        fields: ['token'],
        transaction,
      });
    });
  },
};

export default migration;
