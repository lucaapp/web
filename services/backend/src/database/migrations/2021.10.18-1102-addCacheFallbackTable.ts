import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('CacheFallbacks', {
      key: {
        type: DataTypes.STRING(50),
        primaryKey: true,
        allowNull: false,
      },
      content: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      etag: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('CacheFallbacks');
  },
};

export default migration;
