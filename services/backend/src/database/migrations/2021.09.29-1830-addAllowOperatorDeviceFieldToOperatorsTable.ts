import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) =>
    queryInterface.addColumn('Operators', 'allowOperatorDevices', {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    }),
  down: ({ context: queryInterface }) => {
    return queryInterface.removeColumn('Operators', 'allowOperatorDevices');
  },
};

export default migration;
