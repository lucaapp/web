import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addIndex('DummyTraces', {
      fields: ['healthDepartmentId'],
      concurrently: true,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeIndex(
      'DummyTraces',
      'dummy_traces_health_department_id'
    );
  },
};

export default migration;
