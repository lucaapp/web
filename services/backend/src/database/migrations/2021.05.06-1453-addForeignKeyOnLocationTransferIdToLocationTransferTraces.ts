import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addConstraint('LocationTransferTraces', {
      fields: ['locationTransferId'],
      type: 'foreign key',
      references: {
        table: 'LocationTransfers',
        field: 'uuid',
      },
      onDelete: 'CASCADE',
      onUpdate: '',
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeConstraint(
      'LocationTransferTraces',
      'LocationTransferTraces_locationTransferId_LocationTransfers_fk'
    );
  },
};

export default migration;
