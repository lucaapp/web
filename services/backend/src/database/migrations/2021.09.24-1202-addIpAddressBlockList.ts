import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'IPAddressBlockList',
        {
          uuid: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
          },
          startIp: {
            type: DataTypes.INET,
            allowNull: false,
          },
          endIp: {
            type: DataTypes.INET,
            allowNull: false,
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: literal('CURRENT_TIMESTAMP'),
          },
        },
        { transaction }
      );

      await queryInterface.addIndex('IPAddressBlockList', {
        fields: [{ name: 'startIp', operator: 'inet_ops' }],
        using: 'gist',
        transaction,
      });
      await queryInterface.addIndex('IPAddressBlockList', {
        fields: [{ name: 'endIp', operator: 'inet_ops' }],
        using: 'gist',
        transaction,
      });
      await queryInterface.addIndex('IPAddressBlockList', {
        fields: ['createdAt'],
        transaction,
      });
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('IPAddressBlockList');
  },
};

export default migration;
