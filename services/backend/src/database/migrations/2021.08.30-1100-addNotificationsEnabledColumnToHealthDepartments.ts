import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) =>
    queryInterface.addColumn('HealthDepartments', 'notificationsEnabled', {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    }),
  down: async ({ context: queryInterface }) =>
    queryInterface.removeColumn('HealthDepartments', 'notificationsEnabled'),
};

export default migration;
