import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('LocationTransferGroupMappings');
    await queryInterface.dropTable('LocationTransferGroups');
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.createTable('LocationTransferGroups', {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      tracingProcessId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'TracingProcesses',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
    });

    await queryInterface.createTable('LocationTransferGroupMappings', {
      groupId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'LocationTransferGroups',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      transferId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'LocationTransfers',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
    });
  },
};

export default migration;
