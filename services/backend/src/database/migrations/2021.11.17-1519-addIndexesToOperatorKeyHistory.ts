import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addIndex('OperatorKeyHistories', {
      fields: ['operatorId'],
    });

    await queryInterface.addIndex('OperatorKeyHistories', {
      fields: ['publicKey'],
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.removeIndex(
      'OperatorKeyHistories',
      'operator_key_histories_operator_id'
    );
    await queryInterface.removeIndex(
      'OperatorKeyHistories',
      'operator_key_histories_public_key'
    );
  },
};

export default migration;
