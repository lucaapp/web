import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('Operators', 'bannedAt', {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn('Operators', 'bannedAt');
  },
};

export default migration;
