import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'Traces',
        'isContactDataIncluded',
        {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        {
          transaction,
        }
      );
    });

    await queryInterface.addIndex('Traces', {
      fields: ['isContactDataIncluded'],
      concurrently: true,
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeIndex(
        'LocationTransfers',
        'traces_is_contact_data_included',
        {
          transaction,
        }
      );

      await queryInterface.removeColumn('Traces', 'isContactDataIncluded', {
        transaction,
      });
    });
  },
};

export default migration;
