import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'PotentialOperators',
        {
          email: {
            type: DataTypes.CITEXT,
            allowNull: false,
            primaryKey: true,
          },
          utm_id: {
            type: DataTypes.STRING,
            allowNull: true,
          },
          utm_source: {
            type: DataTypes.STRING,
            allowNull: true,
          },
          utm_medium: {
            type: DataTypes.STRING,
            allowNull: true,
          },
          utm_campaign: {
            type: DataTypes.STRING,
            allowNull: true,
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: literal('CURRENT_TIMESTAMP'),
          },
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('PotentialOperators');
  },
};

export default migration;
