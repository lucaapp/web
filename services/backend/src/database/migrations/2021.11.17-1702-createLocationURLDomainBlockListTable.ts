import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'LocationURLDomainBlockList',
        {
          domain: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
          },
        },
        { transaction }
      );

      await queryInterface.addIndex('LocationURLDomainBlockList', {
        fields: ['domain'],
        transaction,
      });
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('LocationURLDomainBlockList');
  },
};

export default migration;
