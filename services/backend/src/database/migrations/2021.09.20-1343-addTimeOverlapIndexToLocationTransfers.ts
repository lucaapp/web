import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) =>
    queryInterface.addIndex('LocationTransfers', {
      name: 'location_transfers_time',
      fields: ['time'],
      using: 'gist',
    }),
  down: async ({ context: queryInterface }) =>
    queryInterface.removeIndex('LocationTransfers', 'location_transfers_time'),
};

export default migration;
