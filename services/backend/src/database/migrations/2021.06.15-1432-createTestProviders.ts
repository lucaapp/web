import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('TestProviders', {
      fingerprint: {
        type: DataTypes.STRING(32),
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      publicKey: {
        type: DataTypes.STRING(8192),
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('TestProviders');
  },
};

export default migration;
