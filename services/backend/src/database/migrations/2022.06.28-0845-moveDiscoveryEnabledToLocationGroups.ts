import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn('Locations', 'discoveryEnabled');
    await queryInterface.addColumn('LocationGroups', 'discoveryEnabled', {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn('LocationGroups', 'discoveryEnabled');
  },
};

export default migration;
