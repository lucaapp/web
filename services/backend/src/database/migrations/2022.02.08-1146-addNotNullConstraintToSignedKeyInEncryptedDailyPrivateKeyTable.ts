import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: ({ context: queryInterface }) =>
    queryInterface.changeColumn(
      'EncryptedDailyPrivateKeys',
      'signedEncryptedPrivateDailyKey',
      {
        type: DataTypes.STRING(1024),
        allowNull: false,
      }
    ),

  down: ({ context: queryInterface }) =>
    queryInterface.changeColumn(
      'EncryptedDailyPrivateKeys',
      'signedEncryptedPrivateDailyKey',
      {
        type: DataTypes.STRING(1024),
        allowNull: true,
      }
    ),
};

export default migration;
