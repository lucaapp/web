import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'ConnectSearches',
        {
          uuid: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
          },
          processId: {
            type: DataTypes.UUID,
            allowNull: false,
            references: {
              model: 'ConnectSearchProcesses',
              key: 'uuid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
          prefix: {
            type: DataTypes.STRING(12),
            allowNull: false,
          },
          type: {
            type: DataTypes.STRING,
            allowNull: false,
          },
          data: {
            type: DataTypes.STRING(255),
            allowNull: false,
          },
          publicKey: {
            type: DataTypes.STRING(88),
            allowNull: false,
          },
          iv: {
            type: DataTypes.STRING(24),
            allowNull: false,
          },
          mac: {
            type: DataTypes.STRING(44),
            allowNull: false,
          },
          completedAt: {
            type: DataTypes.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.addIndex('ConnectSearches', {
        fields: ['processId'],
        transaction,
      });
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('ConnectSearches');
  },
};

export default migration;
