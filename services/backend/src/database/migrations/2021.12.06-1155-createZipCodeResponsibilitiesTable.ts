import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'ZipCodeResponsibilities',
        {
          zipCode: {
            type: DataTypes.STRING(5),
            allowNull: false,
            primaryKey: true,
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
          name: {
            type: DataTypes.STRING(255),
            allowNull: false,
            primaryKey: true,
          },
          departmentId: {
            type: DataTypes.UUID,
            allowNull: true,
            references: {
              model: 'HealthDepartments',
              key: 'uuid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
        },
        { transaction }
      );
      await queryInterface.addIndex('ZipCodeResponsibilities', {
        fields: ['zipCode'],
        transaction,
      });
      await queryInterface.addIndex('ZipCodeResponsibilities', {
        fields: ['departmentId'],
        transaction,
      });
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('ZipCodeResponsibilities');
  },
};

export default migration;
