import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('Locations', 'type', {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: 'base',
    });
  },
  down: async ({ context: queryInterface }) => {
    return queryInterface.removeColumn('Locations', 'type');
  },
};

export default migration;
