import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'Operators',
        'businessEntityName',
        {
          type: DataTypes.STRING(255),
          defaultValue: null,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Operators',
        'businessEntityStreetName',
        {
          type: DataTypes.STRING(255),
          defaultValue: null,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Operators',
        'businessEntityStreetNumber',
        {
          type: DataTypes.STRING(255),
          defaultValue: null,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Operators',
        'businessEntityZipCode',
        {
          type: DataTypes.STRING(255),
          defaultValue: null,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Operators',
        'businessEntityCity',
        {
          type: DataTypes.STRING(255),
          defaultValue: null,
          allowNull: true,
        },
        { transaction }
      );
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('Operators', 'businessEntityName', {
        transaction,
      });
      await queryInterface.removeColumn(
        'Operators',
        'businessEntityStreetName',
        {
          transaction,
        }
      );
      await queryInterface.removeColumn(
        'Operators',
        'businessEntityStreetNumber',
        {
          transaction,
        }
      );
      await queryInterface.removeColumn('Operators', 'businessEntityZipCode', {
        transaction,
      });
      await queryInterface.removeColumn('Operators', 'businessEntityCity', {
        transaction,
      });
    });
  },
};

export default migration;
