import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('DummyTraces', {
      traceId: {
        type: DataTypes.STRING(24),
        allowNull: false,
        primaryKey: true,
      },
      healthDepartmentId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'HealthDepartments',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
    });
  },

  down: ({ context: queryInterface }) => {
    return queryInterface.dropTable('DummyTraces');
  },
};

export default migration;
