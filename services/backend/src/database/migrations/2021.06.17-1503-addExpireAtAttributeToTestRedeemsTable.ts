import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('TestRedeems', 'expireAt', {
      type: DataTypes.DATE,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn('TestRedeems', 'expireAt');
  },
};

export default migration;
