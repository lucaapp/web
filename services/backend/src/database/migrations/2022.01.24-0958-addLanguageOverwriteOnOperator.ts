import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('Operators', 'languageOverwrite', {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.STRING(5),
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn('Operators', 'languageOverwrite');
  },
};

export default migration;
