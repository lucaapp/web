import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('LocationMenus', {
      locationId: {
        allowNull: false,
        type: DataTypes.UUID,
        primaryKey: true,
        references: {
          model: 'Locations',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      menu: {
        allowNull: true,
        type: DataTypes.JSON,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
    });
  },

  down: ({ context: queryInterface }) => {
    return queryInterface.dropTable('LocationMenus');
  },
};

export default migration;
