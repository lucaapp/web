import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'HealthDepartments',
        'commonName',
        {
          type: DataTypes.STRING(255),
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HealthDepartments',
        'publicCertificate',
        {
          type: DataTypes.STRING(8192),
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HealthDepartments',
        'signedPublicHDSKP',
        {
          type: DataTypes.STRING(2048),
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HealthDepartments',
        'signedPublicHDEKP',
        {
          type: DataTypes.STRING(2048),
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('HealthDepartments', 'commonName', {
        transaction,
      });
      await queryInterface.removeColumn(
        'HealthDepartments',
        'publicCertificate',
        {
          transaction,
        }
      );
      await queryInterface.removeColumn(
        'HealthDepartments',
        'signedPublicHDSKP',
        {
          transaction,
        }
      );
      await queryInterface.removeColumn(
        'HealthDepartments',
        'signedPublicHDEKP',
        {
          transaction,
        }
      );
    });
  },
};

export default migration;
