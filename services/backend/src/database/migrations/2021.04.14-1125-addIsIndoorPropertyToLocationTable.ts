import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    return queryInterface.addColumn('Locations', 'isIndoor', {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    });
  },
  down: async ({ context: queryInterface }) => {
    return queryInterface.removeColumn('Locations', 'isIndoor');
  },
};

export default migration;
