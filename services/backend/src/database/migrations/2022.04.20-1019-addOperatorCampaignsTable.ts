import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('OperatorCampaigns', {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      utm_id: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      utm_source: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      utm_medium: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      utm_campaign: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      operatorId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'Operators',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
    });
  },

  down: async ({ context: queryInterface }) => {
    return queryInterface.dropTable('OperatorCampaigns');
  },
};

export default migration;
