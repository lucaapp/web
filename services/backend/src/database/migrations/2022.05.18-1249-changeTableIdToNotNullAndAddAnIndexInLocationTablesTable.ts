import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const INDEX_NAME = 'LocationTables_id_index';
const CONSTRAINT_NAME = 'LocationTables_id_unique_constraint';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.changeColumn(
        'LocationTables',
        'id',
        {
          allowNull: false,
          type: DataTypes.STRING,
        },
        { transaction }
      );
      await queryInterface.addIndex('LocationTables', ['id'], {
        name: INDEX_NAME,
        transaction,
      });
      await queryInterface.addConstraint('LocationTables', {
        fields: ['id'],
        type: 'unique',
        name: CONSTRAINT_NAME,
        transaction,
      });
    });
  },

  down: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeConstraint('LocationTables', CONSTRAINT_NAME, {
        transaction,
      });
      await queryInterface.removeIndex('LocationTables', INDEX_NAME, {
        transaction,
      });
      await queryInterface.changeColumn(
        'LocationTables',
        'id',
        {
          allowNull: true,
          type: DataTypes.STRING,
        },
        { transaction }
      );
    });
  },
};

export default migration;
