import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: ({ context: queryInterface }) =>
    queryInterface.removeColumn('LocationTransferTraces', 'isHDEncrypted'),
  down: ({ context: queryInterface }) =>
    queryInterface.addColumn('LocationTransferTraces', 'isHDEncrypted', {
      type: DataTypes.BOOLEAN,
      defaultValue: true,
    }),
};

export default migration;
