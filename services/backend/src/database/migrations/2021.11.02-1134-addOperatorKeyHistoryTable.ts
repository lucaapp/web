import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('OperatorKeyHistories', {
      operatorId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      publicKey: {
        type: DataTypes.STRING(88),
        allowNull: false,
      },
      privateKeySecret: {
        type: DataTypes.STRING(44),
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
    });
  },
  down: ({ context: queryInterface }) => {
    return queryInterface.dropTable('OperatorKeyHistories');
  },
};

export default migration;
