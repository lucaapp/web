import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('IPAddressDenyList', {
      ipStart: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      ipEnd: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
    });

    await queryInterface.addIndex('IPAddressDenyList', {
      fields: ['ipStart'],
    });

    await queryInterface.addIndex('IPAddressDenyList', {
      fields: ['ipEnd'],
    });
  },
  down: async ({ context: queryInterface }) => {
    return queryInterface.dropTable('IPAddressDenyList');
  },
};

export default migration;
