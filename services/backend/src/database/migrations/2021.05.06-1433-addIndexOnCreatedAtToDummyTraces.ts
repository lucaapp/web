import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addIndex('DummyTraces', {
      fields: ['createdAt'],
      concurrently: true,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeIndex('DummyTraces', 'dummy_traces_created_at');
  },
};

export default migration;
