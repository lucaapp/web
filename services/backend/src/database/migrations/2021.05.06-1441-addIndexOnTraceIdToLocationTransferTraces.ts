import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addIndex('LocationTransferTraces', {
      fields: ['traceId'],
      concurrently: true,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeIndex(
      'LocationTransferTraces',
      'location_transfer_traces_trace_id'
    );
  },
};

export default migration;
