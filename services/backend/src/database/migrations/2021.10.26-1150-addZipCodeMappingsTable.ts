import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('ZipCodeMappings', {
      zipCode: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
      city: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
      state: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
      community: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
    });

    await queryInterface.addIndex('ZipCodeMappings', {
      fields: ['zipCode'],
      concurrently: true,
    });

    await queryInterface.addIndex('ZipCodeMappings', {
      fields: ['state'],
      concurrently: true,
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('ZipCodeMappings');
  },
};

export default migration;
