import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.changeColumn(
        'HealthDepartmentAuditLogs',
        'employeeId',
        {
          type: DataTypes.UUID,
          allowNull: true,
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.changeColumn(
        'HealthDepartmentAuditLogs',
        'employeeId',
        {
          type: DataTypes.UUID,
          allowNull: false,
        },
        { transaction }
      );
    });
  },
};

export default migration;
