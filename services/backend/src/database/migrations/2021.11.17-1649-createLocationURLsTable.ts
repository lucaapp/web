import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'LocationURLs',
        {
          locationId: {
            type: DataTypes.UUID,
            primaryKey: true,
            allowNull: false,
            references: {
              model: 'Locations',
              key: 'uuid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
          type: {
            type: DataTypes.ENUM(
              'map',
              'menu',
              'schedule',
              'general',
              'website'
            ),
            primaryKey: true,
            allowNull: false,
          },
          url: {
            type: DataTypes.STRING,
            allowNull: true,
          },
        },
        { transaction }
      );

      await queryInterface.addIndex('LocationURLs', {
        fields: ['locationId', 'type'],
        transaction,
      });
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('LocationURLs');
    await queryInterface.sequelize.query('DROP TYPE "enum_LocationURLs_type";');
  },
};

export default migration;
