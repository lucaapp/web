import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('EmailTemplates', {
      key: {
        type: DataTypes.CITEXT,
        primaryKey: true,
        allowNull: false,
      },
      templateId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      lang: {
        type: DataTypes.STRING(2),
        allowNull: false,
        primaryKey: true,
      },
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.dropTable('EmailTemplates');
  },
};

export default migration;
