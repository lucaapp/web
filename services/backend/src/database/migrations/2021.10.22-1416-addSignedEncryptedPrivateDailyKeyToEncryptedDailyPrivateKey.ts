import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn(
      'EncryptedDailyPrivateKeys',
      'signedEncryptedPrivateDailyKey',
      {
        type: DataTypes.STRING(1024),
      }
    );
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn(
      'EncryptedDailyPrivateKeys',
      'signedEncryptedPrivateDailyKey'
    );
  },
};

export default migration;
