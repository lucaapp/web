import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'Locations',
        'openingHours',
        {
          allowNull: true,
          type: DataTypes.JSON,
          defaultValue: null,
        },
        {
          transaction,
        }
      );
      await queryInterface.addColumn(
        'LocationGroups',
        'openingHours',
        {
          allowNull: true,
          type: DataTypes.JSON,
          defaultValue: null,
        },
        {
          transaction,
        }
      );
      await queryInterface.addColumn(
        'LocationGroups',
        'individualOpeningHours',
        {
          allowNull: false,
          type: DataTypes.BOOLEAN,
          defaultValue: false,
        },
        {
          transaction,
        }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('Locations', 'openingHours', {
        transaction,
      });
      await queryInterface.removeColumn('LocationGroups', 'openingHours', {
        transaction,
      });
      await queryInterface.removeColumn(
        'LocationGroups',
        'individualOpeningHours',
        {
          transaction,
        }
      );
    });
  },
};

export default migration;
