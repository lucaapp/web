import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn('Locations', 'shouldProvideGeoLocation');
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('Locations', 'shouldProvideGeoLocation', {
      type: DataTypes.BOOLEAN,
    });
  },
};

export default migration;
