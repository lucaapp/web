import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'LocationTransfers',
        'approvedAt',
        {
          allowNull: true,
          type: DataTypes.DATE,
        },
        {
          transaction,
        }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('LocationTransfers', 'approvedAt', {
        transaction,
      });
    });
  },
};

export default migration;
