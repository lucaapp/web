import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addIndex('Users', ['updatedAt'], {
      name: 'Users_updatedAt_index',
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.removeIndex('Users', 'Users_updatedAt_index');
  },
};

export default migration;
