import { Migration } from 'types/umzug';
import { z } from 'zod';

const migrationsRowSchema = z.array(z.object({ name: z.string() }));
/**
 * Due to Windows file name restrictions we needed to rename the migrations. To not break
 * with previous migrations executed on the machines this migration needs to migrate
 * the migrations and perform the file name change here
 */
const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    const [rows] = await queryInterface.sequelize.query(
      `SELECT * FROM "_Migrations"`
    );

    for (const row of migrationsRowSchema.parse(rows)) {
      const { name } = row;
      // Remove reserved character from previously migrated tasks
      // https://docs.microsoft.com/en-us/windows/win32/fileio/naming-a-file
      const formattedName = name.split(':').join('');

      await queryInterface.sequelize.query(`
        UPDATE "_Migrations" SET name='${formattedName}' WHERE name='${name}'
      `);
    }
  },

  down: async () => {
    // do nothing
  },
};

export default migration;
