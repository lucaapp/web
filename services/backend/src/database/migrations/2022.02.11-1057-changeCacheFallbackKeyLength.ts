import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.changeColumn('CacheFallbacks', 'key', {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true,
    });
  },

  down: ({ context: queryInterface }) =>
    queryInterface.changeColumn('CacheFallbacks', 'traceId', {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true,
    }),
};

export default migration;
