import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn(
      'LocationTransferTraces',
      'isContactDataMandatory',
      {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      }
    );
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn(
      'LocationTransferTraces',
      'isContactDataMandatory'
    );
  },
};

export default migration;
