import chunk from 'lodash/chunk';
import { DataTypes, literal } from 'sequelize';
import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'LocationTables',
        {
          locationId: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
          },
          internalNumber: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
          },
          customName: {
            type: DataTypes.STRING,
            allowNull: true,
            defaultValue: null,
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: literal('CURRENT_TIMESTAMP'),
          },
          updatedAt: {
            allowNull: false,
            type: DataTypes.DATE,
            defaultValue: literal('CURRENT_TIMESTAMP'),
          },
        },
        { transaction }
      );

      const [
        locations,
      ] = (await queryInterface.sequelize.query(
        `SELECT uuid, "tableCount" FROM "Locations" WHERE "tableCount" > 0;`,
        { transaction }
      )) as [{ uuid: string; tableCount: number }[], unknown];

      const locationChunks = chunk(locations, 1000);

      for (const locationChunk of locationChunks) {
        let tableChunk: {
          locationId: string;
          internalNumber: number;
        }[] = [];

        for (const location of locationChunk) {
          const locationId = location.uuid;

          const locationTables = Array.from(
            { length: location.tableCount },
            (_, index) => index + 1
          ).map(internalNumber => {
            return {
              locationId,
              internalNumber,
            };
          });

          tableChunk = [...tableChunk, ...locationTables];
        }

        await queryInterface.bulkInsert('LocationTables', tableChunk, {
          transaction,
        });
      }

      await queryInterface.removeColumn('Locations', 'tableCount', {
        transaction,
      });
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'Locations',
        'tableCount',
        {
          type: DataTypes.INTEGER,
          allowNull: true,
          defaultValue: null,
        },
        {
          transaction,
        }
      );

      const [
        locations,
      ] = (await queryInterface.sequelize.query(
        `SELECT "locationId", COUNT("locationId") FROM "LocationTables" GROUP BY "locationId";`,
        { transaction }
      )) as [{ locationId: string; count: number }[], unknown];

      const locationChunks = chunk(locations, 1000);

      for (const locationChunk of locationChunks) {
        for (const singleChunk of locationChunk) {
          await queryInterface.bulkUpdate(
            'Locations',
            { tableCount: Number(singleChunk.count) },
            { uuid: singleChunk.locationId },
            { transaction }
          );
        }
      }

      await queryInterface.dropTable('LocationTables', { transaction });
    });
  },
};

export default migration;
