import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('Operators', 'avvAccepted', {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
      allowNull: false,
    });
  },
  down: async ({ context: queryInterface }) => {
    return queryInterface.removeColumn('Operators', 'avvAccepted');
  },
};

export default migration;
