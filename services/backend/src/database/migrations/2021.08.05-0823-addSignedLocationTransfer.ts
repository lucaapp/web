import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'LocationTransfers',
        'signedLocationTransfer',
        {
          type: DataTypes.STRING(1024),
          allowNull: true,
          defaultValue: null,
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn(
        'LocationTransfers',
        'signedLocationTransfer',
        {
          transaction,
        }
      );
    });
  },
};

export default migration;
