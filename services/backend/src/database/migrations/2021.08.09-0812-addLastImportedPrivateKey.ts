import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'Operators',
        'lastSeenPrivateKey',
        {
          type: DataTypes.DATE,
          allowNull: true,
          defaultValue: null,
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('Operators', 'lastSeenPrivateKey', {
        transaction,
      });
    });
  },
};

export default migration;
