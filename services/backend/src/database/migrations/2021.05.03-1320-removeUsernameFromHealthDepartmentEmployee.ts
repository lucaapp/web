import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.changeColumn(
        'HealthDepartmentEmployees',
        'email',
        {
          unique: true,
          allowNull: false,
          type: DataTypes.STRING,
        },
        { transaction }
      );
      await queryInterface.removeColumn(
        'HealthDepartmentEmployees',
        'username',
        {
          transaction,
        }
      );
    });
  },
  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.changeColumn(
        'HealthDepartmentEmployees',
        'email',
        {
          unique: false,
          allowNull: false,
          type: DataTypes.STRING,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'HealthDepartmentEmployees',
        'username',
        {
          unique: true,
          allowNull: true,
          type: DataTypes.STRING,
        },
        { transaction }
      );
      await queryInterface.sequelize.query(
        'UPDATE "HealthDepartmentEmployees" SET "username" = "email";',
        { transaction }
      );
      await queryInterface.changeColumn(
        'HealthDepartmentEmployees',
        'username',
        {
          unique: true,
          allowNull: false,
          type: DataTypes.STRING,
        },
        { transaction }
      );
    });
  },
};

export default migration;
