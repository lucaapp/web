import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('Operators', 'phone', {
      type: DataTypes.STRING,
      defaultValue: null,
      allowNull: true,
    });
  },
  down: async ({ context: queryInterface }) => {
    return queryInterface.removeColumn('Operators', 'phone');
  },
};

export default migration;
