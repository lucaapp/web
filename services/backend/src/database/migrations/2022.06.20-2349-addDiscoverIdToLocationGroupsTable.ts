import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'LocationGroups',
        'discoverId',
        {
          type: DataTypes.UUID,
          allowNull: false,
          defaultValue: literal('uuid_generate_v4()'),
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('LocationGroups', 'discoverId', {
        transaction,
      });
    });
  },
};

export default migration;
