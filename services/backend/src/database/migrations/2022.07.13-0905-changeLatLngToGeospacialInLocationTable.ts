import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      // Add geometricPoint Column
      await queryInterface.addColumn(
        'Locations',
        'geometricPoint',
        {
          allowNull: true,
          type: DataTypes.GEOGRAPHY('POINT', 4326),
        },
        { transaction }
      );

      // Move Data from latLng to geometricPoint
      await queryInterface.sequelize.query(
        'UPDATE "Locations" SET "geometricPoint" = ST_MakePoint("lng", "lat");',
        { transaction }
      );

      // Remove lng and lat Column
      await queryInterface.removeColumn('Locations', 'lng', { transaction });
      await queryInterface.removeColumn('Locations', 'lat', { transaction });

      // Add geometricPoint as index
      await queryInterface.addIndex('Locations', {
        fields: ['geometricPoint'],
        name: 'geometricPoint_index',
        using: 'GIST',
        transaction,
      });
    });
  },

  down: async ({ context: queryInterface }) => {
    return queryInterface.sequelize.transaction(async transaction => {
      // Add lat and lng Column
      await queryInterface.addColumn(
        'Locations',
        'lat',
        {
          allowNull: true,
          type: DataTypes.DOUBLE,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Locations',
        'lng',
        {
          allowNull: true,
          type: DataTypes.DOUBLE,
        },
        { transaction }
      );
      // Move Data from  geometricPoint to lat and lng
      await queryInterface.sequelize.query(
        'UPDATE "Locations" SET "lat" = ST_Y("geometricPoint"), "lng" = ST_X("geometricPoint");',
        { transaction }
      );

      // Drop geometricPoint_index index possibly not required
      await queryInterface.removeIndex('Locations', 'geometricPoint_index', {
        transaction,
      });

      // Remove geometricPoint Column
      await queryInterface.removeColumn('Locations', 'geometricPoint', {
        transaction,
      });
    });
  },
};

export default migration;
