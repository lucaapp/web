import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) =>
    queryInterface.addColumn('Locations', 'entryPolicy', {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.ENUM('2G', '3G'),
    }),
  down: ({ context: queryInterface }) => {
    return queryInterface.removeColumn('Locations', 'entryPolicy');
  },
};

export default migration;
