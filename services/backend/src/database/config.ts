import config from 'config';

const disableTls = config.get('db.ssl.disabled') || false;
const fingerprints = config.get('db.ssl.fingerprints').split(',');

export default {
  production: {
    dialect: 'postgres',
    username: config.get('db.username'),
    password: config.get('db.password'),
    database: config.get('db.database'),
    host: config.get('db.host'),
    replication: {
      read: [
        {
          host: config.get('db.host_read1'),
        },
        {
          host: config.get('db.host_read2'),
        },
        {
          host: config.get('db.host_read3'),
        },
      ],
      write: {
        host: config.get('db.host'),
      },
    },
    pool: {
      max: 20,
      min: 1,
    },
    dialectOptions: disableTls
      ? {}
      : {
          ssl: {
            rejectUnauthorized: true,
            host: config.get('db.host'),
            ca: config.get('db.ssl.ca'),
            checkServerIdentity: (
              host: string,
              cert: { fingerprint256: string }
            ) => {
              if (!fingerprints.includes(cert.fingerprint256)) {
                throw new Error(
                  `Fingerprint not whitelisted (${cert.fingerprint256})`
                );
              }
            },
          },
        },
  },
  development: {
    dialect: 'postgres',
    username: config.get('db.username'),
    password: config.get('db.password'),
    database: config.get('db.database'),
    host: config.get('db.host'),
    pool: {
      max: 5,
      min: 1,
    },
  },
  test: {
    dialect: 'sqlite',
    storage: ':memory:',
  },
};
