import crypto from 'crypto';
import { v4 as uuid } from 'uuid';
import faker from 'faker';
import { Seed } from 'types/umzug';
import { generateSupportCode } from '../../utils/generators';

const DEVELOPMENT_EMAIL = 'luca@nexenio.com';
const DEVELOPMENT_PHONE = '+4917612345678';

const hashPassword = (plaintextPassword: string) => {
  const salt = crypto.randomBytes(16).toString('base64');
  const password = crypto
    .scryptSync(plaintextPassword, salt, 64)
    .toString('base64');

  return {
    password,
    salt,
  };
};

const operators = [
  {
    uuid: uuid(),
    publicKey: null,
    activated: true,
    email: DEVELOPMENT_EMAIL,
    phone: DEVELOPMENT_PHONE,
    lastName: faker.name.lastName(),
    firstName: faker.name.firstName(),
    username: DEVELOPMENT_EMAIL,
    supportCode: generateSupportCode(),
    privateKeySecret: crypto.randomBytes(32).toString('base64'),
    ...hashPassword('testing'),
  },
];
const departments = [
  {
    uuid: uuid(),
    name: 'neXenio Testing',
    commonName: 'Dev Health Department',
    privateKeySecret: crypto.randomBytes(32).toString('base64'),
  },
];

const employees = [
  {
    uuid: uuid(),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    email: DEVELOPMENT_EMAIL,
    departmentId: departments[0].uuid,
    isAdmin: true,
    ...hashPassword('testing'),
  },
];

const badgeGenerators = [
  {
    name: 'dev',
    ...hashPassword('testing'),
  },
];

const seed: Seed = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.bulkInsert('Operators', operators);
    await queryInterface.bulkInsert('HealthDepartments', departments);
    await queryInterface.bulkInsert('HealthDepartmentEmployees', employees);
    await queryInterface.bulkInsert('BadgeGenerators', badgeGenerators);
  },
  down: async () => {
    console.warn('Not implemented.');
  },
};

export default seed;
