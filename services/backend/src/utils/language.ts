import { Request } from 'express';
import resolveAcceptLanguage from 'resolve-accept-language';

export type InternalLanguages = 'de' | 'en';
export type InternalLocales = 'de-DE' | 'en-US' | 'en-GB';

export const DEFAULT_LANGUAGE = 'de-DE';
export const AVAILABLE_LANGUAGES = [DEFAULT_LANGUAGE, 'en-US', 'en-GB'];

export const getTransactionalLanguage = (
  locale: InternalLocales
): InternalLanguages => locale.split('-')[0] as InternalLanguages;

const parseAcceptLanguageHeader = (header: string): InternalLocales => {
  return resolveAcceptLanguage(
    header,
    AVAILABLE_LANGUAGES,
    DEFAULT_LANGUAGE
  ) as InternalLocales;
};

export const getPreferredLanguageFromRequest = (
  request: Request<Record<string, unknown>>
): InternalLocales => {
  const acceptLanguageHeader = request.headers['accept-language'];
  if (!acceptLanguageHeader) {
    return DEFAULT_LANGUAGE;
  }
  return parseAcceptLanguageHeader(acceptLanguageHeader);
};
