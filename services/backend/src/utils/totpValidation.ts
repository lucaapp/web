import { hkdfSync, timingSafeEqual } from 'crypto';

const TIME_TOLERANCE_IN_S = 300n;
const TOTP_TOKEN_LENGTH = 32;
const TOTP_SECRET_LENGTH = 32;
const TOTP_VALIDATION_TOKEN_LENGTH = 24;
const EMPTY_SALT = '';

const calcTOTPToken = async (
  secret: Buffer,
  timeStamp: number
): Promise<Buffer> => {
  return Buffer.from(
    hkdfSync(
      'sha256',
      secret,
      EMPTY_SALT,
      Buffer.from(timeStamp.toString()),
      TOTP_VALIDATION_TOKEN_LENGTH
    )
  );
};

const difference = (left: number, right: number): number => {
  return Math.abs(left - right);
};

const equals = (left: Buffer, right: Buffer) =>
  left.length === right.length && timingSafeEqual(left, right);

export const totpValidation = async (
  nowInS: number,
  totpSecret: Buffer,
  totpToken: Buffer
): Promise<boolean> => {
  if (
    totpToken.length !== TOTP_TOKEN_LENGTH ||
    totpSecret.length !== TOTP_SECRET_LENGTH
  ) {
    return false;
  }

  const totpTime = totpToken.slice(0, 8).readBigInt64BE();
  if (totpTime > Number.MAX_SAFE_INTEGER || totpTime < 0) {
    return false;
  }

  const timeDifference = difference(Number(totpTime), nowInS);
  if (timeDifference > TIME_TOLERANCE_IN_S) {
    return false;
  }

  const totpValidationToken = totpToken.slice(8);
  return equals(
    await calcTOTPToken(totpSecret, Number(totpTime)),
    totpValidationToken
  );
};
