import forge from 'node-forge';
import assert from 'assert';
import {
  verifySignedLocationTransfer,
  verifySignedPublicHDSKP,
  verifySignedPublicHDEKP,
} from '@lucaapp/crypto';
import { certificateChain } from 'constants/certificateChain';
import { HealthDepartmentInstance } from 'database/models/healthDepartment';
import { z } from './validation';

const validate = (
  validatedContent: {
    iss: string;
    sub: string;
    name: string;
    key: string;
  },
  {
    issuer,
    subject,
    name,
    key,
  }: {
    issuer: string;
    subject: string;
    name: string;
    key?: string;
  }
) => {
  assert.equal(validatedContent.iss, issuer, 'Invalid issuer.');
  assert.equal(validatedContent.sub, subject, 'Invalid subject.');
  assert.equal(validatedContent.name, name, 'Invalid name.');
  assert.equal(validatedContent.key, key, 'Invalid key.');
};

const getFingerprint = (certificate: forge.pki.Certificate) => {
  const certDer = forge.asn1.toDer(forge.pki.certificateToAsn1(certificate));
  const md = forge.md.sha1.create();
  md.update(certDer.data);
  return md.digest().toHex();
};

export const verifySignedPublicKeys = (
  healthDepartment: HealthDepartmentInstance,
  certificatePem: string,
  signedPublicHDSKP: string,
  signedPublicHDEKP: string
): void => {
  const certificate = forge.pki.certificateFromPem(certificatePem);
  const publicKeyPem = forge.pki.publicKeyToPem(certificate.publicKey);
  const commonName = certificate.subject.getField('CN')?.value;
  const fingerprint = getFingerprint(certificate);

  assert.equal(
    commonName,
    healthDepartment.commonName,
    'Common name mismatch.'
  );

  const verifiedHDSKP = verifySignedPublicHDSKP({
    certificateChain,
    issuer: {
      publicCertificate: publicKeyPem,
      signedPublicHDSKP,
      issuerId: healthDepartment.uuid,
    },
  });

  const verifiedHDEKP = verifySignedPublicHDEKP({
    certificateChain,
    issuer: {
      publicCertificate: publicKeyPem,
      signedPublicHDSKP,
      signedPublicHDEKP,
      issuerId: healthDepartment.uuid,
    },
  });

  const commonComparePayload = {
    name: healthDepartment.name,
    subject: healthDepartment.uuid,
    issuer: fingerprint,
  };

  validate(verifiedHDSKP, {
    ...commonComparePayload,
    key: healthDepartment.publicHDSKP,
  });

  validate(verifiedHDEKP, {
    ...commonComparePayload,
    key: healthDepartment.publicHDEKP,
  });
};

const locationTransferSigningSchema = z
  .object({
    iss: z.uuid(),
    type: z.literal('locationTransfer'),
    locationId: z.uuid(),
    time: z.array(z.unixTimestamp()).length(2),
    iat: z.unixTimestamp(),
  })
  .strict();

export const extractAndVerifyLocationTransfer = ({
  issuer,
  signedLocationTransfer,
  locationId,
  time,
}: {
  issuer: HealthDepartmentInstance;
  signedLocationTransfer: string;
  locationId: string;
  time: number[];
}): {
  iss: string;
  locationId: string;
  type: string;
  time: number[];
} => {
  const content = verifySignedLocationTransfer({
    certificateChain,
    // @ts-ignore undefined commonName and signedPublicHDSKP will fail validation
    issuer,
    signedLocationTransfer,
    isIssuing: true,
  });

  const validatedContent = locationTransferSigningSchema.parse(content);

  assert.equal(validatedContent.iss, issuer.uuid, 'Invalid issuer.');
  assert.equal(validatedContent.locationId, locationId, 'Invalid location.');
  assert.equal(validatedContent.type, 'locationTransfer', 'Invalid type.');
  assert.deepEqual(validatedContent.time, time, 'Invalid timeframe.');

  return validatedContent;
};
