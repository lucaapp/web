/* eslint-disable max-classes-per-file, class-methods-use-this */
const status = require('http-status');

const API_ERRORS = {
  UNKNOWN_API_ERROR: {
    status: status.INTERNAL_SERVER_ERROR,
    message: 'Unknown API error.',
  },
  UNAUTHORIZED: {
    status: status.UNAUTHORIZED,
    message: 'Unauthorized.',
  },
  BAD_REQUEST: {
    status: status.BAD_REQUEST,
    message: 'Bad request.',
  },
  FORBIDDEN: {
    status: status.FORBIDDEN,
    message: 'Forbidden.',
  },
  CONFLICT: {
    status: status.CONFLICT,
    message: 'Conflict.',
  },
  FEATURE_DISABLED: {
    status: status.NOT_IMPLEMENTED,
    message: 'Feature is disabled.',
  },
  ISSUER_NOT_FOUND: {
    status: status.NOT_FOUND,
    message: 'Issuer not found.',
  },
  HEALTH_DEPARTMENT_NOT_FOUND: {
    status: status.NOT_FOUND,
    message: 'Health department not found.',
  },
  LOCATION_NOT_FOUND: {
    status: status.NOT_FOUND,
    message: 'Location not found.',
  },
  LOCATION_TRANSFER_NOT_FOUND: {
    status: status.NOT_FOUND,
    message: 'Location transfer not found.',
  },
  SIGNED_KEYS_ALREADY_EXIST: {
    status: status.CONFLICT,
    message: 'Signed keys already exist.',
  },
  INVALID_SIGNED_KEYS: {
    status: status.BAD_REQUEST,
    message: 'Invalid signed keys.',
  },
  INVALID_SIGNATURE: {
    status: status.BAD_REQUEST,
    message: 'Signature invalid.',
  },
  CHALLENGE_NOT_FOUND: {
    status: status.NOT_FOUND,
    message: 'Challenge does not exist.',
  },
  TOO_MANY_LOCATIONS: {
    status: status.REQUEST_ENTITY_TOO_LARGE,
    message: 'There are too many locations attached to this request',
  },
  USER_TRANSFER_NOT_FOUND: {
    status: status.NOT_FOUND,
    message: 'User transfer not found.',
  },
  DEVICE_NOT_FOUND: {
    status: status.NOT_FOUND,
    message: 'Device does not exist.',
  },
  DEVICE_EXPIRED: {
    status: status.LOCKED,
    message: 'Device refresh token is expired.',
  },
  LIMIT_REACHED: {
    status: status.LOCKED,
    message: 'Entity limit reached',
  },
  TOTP_TOKEN_INVALID: {
    status: status.UNAUTHORIZED,
    message: 'invalid Totp-Token',
  },
  VAULT_NOT_FOUND: {
    status: status.NOT_FOUND,
    message: 'vault not found',
  },
  VERSION_NOT_SUPPORTED: {
    status: status.BAD_REQUEST,
    message: 'not supported version',
  },
  VAULT_OBJECT_ALREADY_EXPIRED: {
    status: status.BAD_REQUEST,
    message: 'vault object is already expired',
  },
  VAULT_OBJECT_EXPIRES_TOO_LATE: {
    status: status.BAD_REQUEST,
    message: 'vault object expires too late',
  },
  MENU_NOT_FOUND: {
    status: status.NOT_FOUND,
    message: 'Menu not found',
  },
  NOT_FOUND: {
    status: status.NOT_FOUND,
    message: 'Entity not found',
  },
};

class ApiError extends Error {
  constructor(errorCode = 'UNKNOWN_API_ERROR', message = '') {
    super(message || API_ERRORS[String(errorCode)].message);
    this.errorCode = errorCode;
    this.statusCode = API_ERRORS[String(errorCode)].status || 500;
  }
}

module.exports = {
  ApiError,
  ApiErrorType: {
    UNKNOWN_API_ERROR: 'UNKNOWN_API_ERROR',
    UNAUTHORIZED: 'UNAUTHORIZED',
    BAD_REQUEST: 'BAD_REQUEST',
    FORBIDDEN: 'FORBIDDEN',
    CONFLICT: 'CONFLICT',
    FEATURE_DISABLED: 'FEATURE_DISABLED',
    ISSUER_NOT_FOUND: 'ISSUER_NOT_FOUND',
    CHALLENGE_NOT_FOUND: 'CHALLENGE_NOT_FOUND',
    HEALTH_DEPARTMENT_NOT_FOUND: 'HEALTH_DEPARTMENT_NOT_FOUND',
    LOCATION_TRANSFER_NOT_FOUND: 'LOCATION_TRANSFER_NOT_FOUND',
    LOCATION_NOT_FOUND: 'LOCATION_NOT_FOUND',
    SIGNED_KEYS_ALREADY_EXIST: 'SIGNED_KEYS_ALREADY_EXIST',
    INVALID_SIGNED_KEYS: 'INVALID_SIGNED_KEYS',
    INVALID_SIGNATURE: 'INVALID_SIGNATURE',
    DEVICE_NOT_FOUND: 'DEVICE_NOT_FOUND',
    TOO_MANY_LOCATIONS: 'TOO_MANY_LOCATIONS',
    USER_TRANSFER_NOT_FOUND: 'USER_TRANSFER_NOT_FOUND',
    DEVICE_EXPIRED: 'DEVICE_EXPIRED',
    LIMIT_REACHED: 'LIMIT_REACHED',
    TOTP_TOKEN_INVALID: 'TOTP_TOKEN_INVALID',
    VAULT_NOT_FOUND: 'VAULT_NOT_FOUND',
    VERSION_NOT_SUPPORTED: 'VERSION_NOT_SUPPORTED',
    VAULT_OBJECT_ALREADY_EXPIRED: 'VAULT_OBJECT_ALREADY_EXPIRED',
    VAULT_OBJECT_EXPIRES_TOO_LATE: 'VAULT_OBJECT_EXPIRES_TOO_LATE',
    MENU_NOT_FOUND: 'MENU_NOT_FOUND',
    NOT_FOUND: 'NOT_FOUND',
  },
};
