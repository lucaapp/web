import config from 'config';
import axios from 'axios';
import createHttpsProxyAgent from 'https-proxy-agent';
import { E164Number } from 'libphonenumber-js';

const httpsProxy = config.get('proxy.https');
const httpsAgent = httpsProxy ? createHttpsProxyAgent(httpsProxy) : undefined;

export const messagemobile = axios.create({
  httpsAgent,
  baseURL: config.get('messagemobile.gateway'),
  headers: {
    Authorization: `Bearer ${config.get('messagemobile.accessKey')}`,
    'Content-Type': 'application/json',
  },
  proxy: false,
  timeout: 30000,
});

export const sendSMSTan = async (
  phone: string | E164Number,
  tan: string
): Promise<string> => {
  const response = await messagemobile.post('/messages', {
    content: {
      '@type': 'Text',
      text: {
        data: `Deine Luca-TAN ist: ${tan}`,
      },
    },
    consumer: [
      {
        id: `${phone}`,
      },
    ],
  });

  return response.data.id;
};
