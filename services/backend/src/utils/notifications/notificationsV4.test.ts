import moment from 'moment';
import { getBinaryHeaderBuffer } from './notificationsV4';

const NO_HASH_INPUT_HASH_BUFFER = Buffer.alloc(16);

describe('notifications', () => {
  describe('build header', () => {
    it('can build a header without a hash', async () => {
      const chunkHeader = getBinaryHeaderBuffer(undefined);
      expect(chunkHeader.length).toEqual(32);

      const schemaVersion = chunkHeader.slice(0, 1).readInt8(0);
      const algorithmType = chunkHeader.slice(1, 2).readInt8(0);
      const hashLength = chunkHeader.slice(2, 3).readInt8(0);
      const createdAt = chunkHeader.slice(3, 11).readBigInt64BE(0);
      const lastSliceHash = chunkHeader.slice(16, 32);

      expect(schemaVersion).toEqual(1);
      expect(algorithmType).toEqual(0);
      expect(hashLength).toEqual(12);
      expect(Number(createdAt)).toBeLessThan(moment().valueOf());
      expect(lastSliceHash).toEqual(NO_HASH_INPUT_HASH_BUFFER);
    });
  });
});
