/* eslint-disable @typescript-eslint/explicit-module-boundary-types, max-lines */

import config from 'config';
import { URL } from 'url';
import { z as zod } from 'zod';
import * as crypto from '@lucaapp/crypto';
import validator from 'validator';
import parsePhoneNumber from 'libphonenumber-js/max';
import {
  DEVICE_TYPE_IOS,
  DEVICE_TYPE_ANDROID,
  DEVICE_TYPE_STATIC,
  DEVICE_TYPE_WEBAPP,
  DEVICE_TYPE_FORM,
} from 'constants/deviceTypes';
import { zxcvbn } from './zxcvbnConfig';

const SAFE_CHARACTERS_REGEX = /^[\w !&()+,./:@`|£À-ÿāăąćĉċčđēėęěĝğģĥħĩīįİıĵķĸĺļłńņōőœŗřśŝşšţŦũūŭůűųŵŷźżžơưếệ–-]*$/i;
const NO_HTTP_REGEX = /^((?!http).)*$/i;
const NO_FTP_REGEX = /^((?!ftp).)*$/i;
const ALPHANUMERIC_REGEX = /^[\da-z]+$/i;

const PASSWORD_REQUIREMENTS = {
  minLength: 9,
  minNumbers: 1,
  minLowercase: 1,
  minUppercase: 1,
  minSymbols: 1,
};

export const z = {
  ...zod,

  safeString: () =>
    zod
      .string()
      .regex(SAFE_CHARACTERS_REGEX)
      .regex(NO_HTTP_REGEX)
      .regex(NO_FTP_REGEX),

  safeText: () => zod.string().regex(NO_HTTP_REGEX).regex(NO_FTP_REGEX),

  phoneNumber: () =>
    zod
      .string()
      .max(32)
      .refine(value => !!parsePhoneNumber(value, 'DE')?.isValid(), {
        message: 'invalid phone number',
      }),

  zxcvbnPassword: (userInputs?: (string | number)[] | undefined) =>
    zod
      .string()
      .max(1024)
      .refine(
        async value => {
          const result = await zxcvbn(value, userInputs);
          return (
            result.score >=
            config.get('luca.operators.password.minDifficultyLevel')
          );
        },
        { message: 'password not strong enough' }
      ),

  strongPassword: () =>
    zod
      .string()
      .refine(value =>
        validator.isStrongPassword(value, PASSWORD_REQUIREMENTS)
      ),

  uuid: () =>
    zod
      .string()
      .length(36)
      .refine(value => validator.isUUID(value, 'all')),

  uuidv4: () =>
    zod
      .string()
      .length(36)
      .refine(value => validator.isUUID(value, '4')),

  zipCode: () =>
    zod
      .string()
      .max(255)
      .refine(value => validator.isPostalCode(value, 'any')),

  email: () =>
    zod
      .string()
      .email()
      .max(255)
      .refine(value =>
        validator.isEmail(value, {
          allow_display_name: false,
          require_display_name: false,
          allow_utf8_local_part: true,
          require_tld: true,
          allow_ip_domain: false,
          // @ts-ignore: double escape  as this is passed into new RegExp("[${blacklisted_chars}]")
          blacklisted_chars: "=',\\\\",
        })
      ),

  integerString: (options?: { maxLength?: number; lt: number; gt: number }) =>
    zod
      .string()
      .max(options?.maxLength || 17)
      .refine(value =>
        validator.isInt(value, {
          lt: options?.lt || Number.MAX_SAFE_INTEGER,
          gt: options?.gt || Number.MIN_SAFE_INTEGER,
          allow_leading_zeroes: false,
        })
      )
      .transform(value => Number.parseInt(value, 10)),
  bigIntegerString: () =>
    zod
      .string()
      .max(2048)
      .refine(value =>
        validator.isInt(value, {
          allow_leading_zeroes: false,
        })
      ),
  floatString: (options?: { maxLength?: number; lt: number; gt: number }) =>
    zod
      .string()
      .max(options?.maxLength || 17)
      .refine(value =>
        validator.isFloat(value, {
          lt: options?.lt || Number.MAX_SAFE_INTEGER,
          gt: options?.gt || Number.MIN_SAFE_INTEGER,
        })
      )
      .transform(value => Number.parseFloat(value)),
  alphaNumericString: () => zod.string().regex(ALPHANUMERIC_REGEX),

  unixTimestamp: () => zod.number().int().positive(),

  hex: ({
    min,
    max,
    length,
    rawLength,
  }: {
    min?: number;
    max?: number;
    length?: number;
    rawLength?: number;
  } = {}) =>
    zod
      .string()
      .min(min as number)
      .max(max as number)
      .length(length as number)
      .refine(value => {
        if (!validator.isHexadecimal(value)) return false;
        if (!rawLength) return true;

        return crypto.hexToBytes(value).length === rawLength;
      }),

  sha256: () => z.hex({ length: 64, rawLength: 32 }),

  base64: ({
    min,
    max,
    length,
    rawLength,
    isBase64Url = false,
  }: {
    min?: number;
    max?: number;
    length?: number;
    rawLength?: number;
    isBase64Url?: boolean;
  } = {}) =>
    zod
      .string()
      .min(min as number)
      .max(max as number)
      .length(length as number)
      .refine(value => {
        if (!validator.isBase64(value, { urlSafe: isBase64Url })) return false;
        if (!rawLength) return true;

        return (
          Buffer.from(value, isBase64Url ? 'base64url' : 'base64').length ===
          rawLength
        );
      }),

  bearerToken: () =>
    zod
      .string()
      .min(28)
      .max(255)
      .refine(value => {
        return validator.isBase64(value.replace('Bearer ', ''));
      })
      .transform(value => value.replace('Bearer ', '')),

  ecPublicKey: () => z.base64({ length: 88, rawLength: 65 }),

  ecCompressedPublicKey: () => z.base64({ length: 44, rawLength: 33 }),

  ecSignature: () => z.base64({ max: 120 }),

  iv: () => z.base64({ length: 24, rawLength: 16 }),

  mac: () => z.base64({ length: 44, rawLength: 32 }),

  traceId: () => z.base64({ length: 24, rawLength: 16 }),

  dailyKeyId: () =>
    zod
      .number()
      .int()
      .min(0)
      .max((config.get('keys.daily.max') as number) - 1),

  badgeKeyId: () =>
    zod.number().int().min(0).max(config.get('keys.badge.targetKeyId')),

  deviceType: () =>
    zod.union([
      zod.literal(DEVICE_TYPE_IOS),
      zod.literal(DEVICE_TYPE_ANDROID),
      zod.literal(DEVICE_TYPE_STATIC),
      zod.literal(DEVICE_TYPE_WEBAPP),
      zod.literal(DEVICE_TYPE_FORM),
    ]),

  jwt: ({ max }: { max: number }) =>
    zod
      .string()
      .max(max)
      .refine(value => validator.isJWT(value)),

  dsgcPayload: () =>
    zod.object({
      certificates: zod.array(
        zod.object({
          certificateType: zod.string(),
          country: zod.string(),
          kid: zod.string(),
          rawData: zod.string(),
          signature: zod.string(),
          thumbprint: zod.string(),
          timestamp: zod.string(),
        })
      ),
    }),
  dccListPayload: () =>
    zod.array(
      zod.object({
        identifier: zod.string(),
        version: zod.string(),
        country: zod.string().length(2),
        hash: zod.string(),
      })
    ),
  dccRulePayload: () =>
    zod.object({
      Identifier: zod.string(),
      Type: zod.string(),
      Country: zod.string().length(2),
      Version: zod.string(),
      SchemaVersion: zod.string(),
      Engine: zod.string(),
      EngineVersion: zod.string(),
      CertificateType: zod.string(),
      Description: zod.array(
        zod.object({ lang: zod.string().length(2), desc: zod.string() })
      ),
      ValidFrom: zod.string(),
      ValidTo: zod.string(),
      AffectedFields: zod.array(zod.string()),
      Logic: zod.any(),
    }),
  urlString: () =>
    zod
      .string()
      .max(255)
      .refine(
        value =>
          validator.isURL(value, {
            require_protocol: true,
            protocols: ['https'],
            allow_protocol_relative_urls: false,
            disallow_auth: true,
            require_host: true,
          }) && validator.isFQDN(new URL(value).hostname),
        {
          message: 'invalid url',
        }
      ),
};
