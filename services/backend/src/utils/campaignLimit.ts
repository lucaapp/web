import config from 'config';
import { OperatorCampaign } from '../database';
import { ApiError, ApiErrorType } from './apiError';

const maxCampaignsPerOperator = config.get('campaigns.maxCampaignsPerOperator');

export const checkCampaignLimit = async (operatorId: string): Promise<void> => {
  const existingCampaigns = await OperatorCampaign.count({
    where: {
      operatorId,
    },
  });

  if (existingCampaigns >= maxCampaignsPerOperator) {
    throw new ApiError(
      ApiErrorType.BAD_REQUEST,
      `Campaign limit (${maxCampaignsPerOperator}) reached.`
    );
  }
};
