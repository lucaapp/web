import { ZxcvbnOptions, zxcvbn } from '@zxcvbn-ts/core';
import zxcvbnCommonPackage from '@zxcvbn-ts/language-common';
import zxcvbnEnPackage from '@zxcvbn-ts/language-en';
import zxcvbnDePackage from '@zxcvbn-ts/language-de';

const extendedOptions = {
  graphs: zxcvbnCommonPackage.adjacencyGraphs,
  translations: {
    ...zxcvbnDePackage.translations,
    ...zxcvbnEnPackage.translations,
  },
  dictionary: {
    ...zxcvbnCommonPackage.dictionary,
    ...zxcvbnEnPackage.dictionary,
    ...zxcvbnDePackage.dictionary,
  },
};

ZxcvbnOptions.setOptions(extendedOptions);

// eslint-disable-next-line unicorn/prefer-export-from
export { zxcvbn };
