/* eslint-disable max-lines */
import { ZodEffects, ZodString } from 'zod';
import { z } from './validation';

const PHONE_NUMBER_TEST_CASES_VALID = [
  '0150 00123123',
  '030 123 456',
  '(030) 123-456',
  '+49150 00123123',
  '+4930 123 456',
  '+44 20 1234 5678',
  '(408) 123-4567',
];
const PHONE_NUMBER_TEST_CASES_INVALID = [
  null,
  undefined,
  1234,
  '123',
  'notanumber',
];

const SAFE_STRING_TEST_CASES_VALID = [
  '015100123123',
  'Standort',
  'a@bc.de',
  'CMD()',
];
const SAFE_STRING_TEST_CASES_INVALID = [
  "Carl's Bar",
  '100$ Bill',
  '=CMD()',
  'HtTp://example.com',
  'click here HtTp://example.com',
  null,
  undefined,
  1234,
];

const ALPHA_NUMERIC_STRING_TEST_CASES_VALID = [
  '015100123123',
  'Standort123',
  'API500',
];
const ALPHA_NUMERIC_STRING_TEST_CASES_INVALID = [
  "Carl's Bar",
  '100$ Bill',
  'UNDER_SCORE',
  '=CMD()',
  'äbc53de',
  'HtTp://example.com',
  'click here HtTp://example.com',
  'ABC?',
  null,
  undefined,
  1234,
];

describe('validation', () => {
  describe('object', () => {
    it('strips unknown keys', async () => {
      const schema = z.object({ test: z.string() });
      const inputObject = { test: 'test', unknown: 'test' };
      const expectedOutput = { test: 'test' };
      expect(schema.parse(inputObject)).toEqual(expectedOutput);
    });
  });

  describe('phoneNumber', () => {
    it('should accept valid phone numbers', async () => {
      const schema = z.phoneNumber();
      for (const testCase of PHONE_NUMBER_TEST_CASES_VALID) {
        expect(() => schema.parse(testCase)).not.toThrow();
        expect(schema.parse(testCase)).toEqual(testCase);
      }
    });

    it('should reject invalid phone numbers', async () => {
      const schema = z.phoneNumber();
      for (const testCase of PHONE_NUMBER_TEST_CASES_INVALID) {
        expect(() => schema.parse(testCase)).toThrow(z.ZodError);
      }
    });
  });

  describe('safeString', () => {
    it('should accept valid strings', async () => {
      const schema = z.safeString().max(255);
      for (const testCase of SAFE_STRING_TEST_CASES_VALID) {
        expect(() => schema.parse(testCase)).not.toThrow();
        expect(schema.parse(testCase)).toEqual(testCase);
      }
    });

    it('should reject invalid strings', async () => {
      const schema = z.safeString();
      for (const testCase of SAFE_STRING_TEST_CASES_INVALID) {
        expect(() => schema.parse(testCase)).toThrow(z.ZodError);
      }
    });

    it('should reject too long strings', async () => {
      const schema = z.safeString().max(3);
      expect(() => schema.parse('test')).toThrow(z.ZodError);
    });
  });

  describe('alphaNumericString', () => {
    it('should accept valid strings', async () => {
      const schema = z.alphaNumericString().max(255);
      for (const testCase of ALPHA_NUMERIC_STRING_TEST_CASES_VALID) {
        expect(() => schema.parse(testCase)).not.toThrow();
        expect(schema.parse(testCase)).toEqual(testCase);
      }
    });

    it('should reject invalid strings', async () => {
      const schema = z.alphaNumericString();
      for (const testCase of ALPHA_NUMERIC_STRING_TEST_CASES_INVALID) {
        expect(() => schema.parse(testCase)).toThrow(z.ZodError);
      }
    });
  });

  describe('strongPassword', () => {
    let schema: ZodEffects<ZodString, string, string>;
    beforeAll(() => {
      schema = z.strongPassword();
    });

    it('should accept strong passwords', async () => {
      expect(() => schema.parse('nTdRT9EjWVg(')).not.toThrow();
      expect(() => schema.parse(',867f4686?%72V+R${#[8*wAML&t')).not.toThrow();
    });

    it('should reject short passwords', async () => {
      expect(() => schema.parse('')).toThrow(z.ZodError);
      expect(() => schema.parse('abcdef')).toThrow(z.ZodError);
      expect(() => schema.parse('a1Cd%!6')).toThrow(z.ZodError);
    });
    it('should reject passwords without lowercase characters', async () => {
      expect(() => schema.parse('ABC124!@#........')).toThrow(z.ZodError);
    });
    it('should reject passwords without uppercase characters', async () => {
      expect(() => schema.parse('abc124!@#........')).toThrow(z.ZodError);
    });
    it('should reject passwords without numbers', async () => {
      expect(() => schema.parse('abcABC!@#........')).toThrow(z.ZodError);
    });
    it('should reject passwords without symbols', async () => {
      expect(() => schema.parse('abcABC123aaaaaaaa')).toThrow(z.ZodError);
    });
  });

  describe('uuid', () => {
    let schema: ZodEffects<ZodString, string, string>;
    beforeAll(() => {
      schema = z.uuid();
    });

    it('should accept v3-v5 uuid', async () => {
      // v4
      expect(() =>
        schema.parse('ebca725f-6c9c-4870-bcff-1fcb852a56b3')
      ).not.toThrow();
      // v3
      expect(() =>
        schema.parse('9817ebe4-6afe-5dcc-b76a-2a66879676a3')
      ).not.toThrow();
      // v5
      expect(() =>
        schema.parse('ec5115fc-e9cb-5be1-93fe-8984b1b2fab9')
      ).not.toThrow();
    });
    it('should accept non-v3-v5 uuid', async () => {
      expect(() =>
        schema.parse('ffa3bb2b-fd75-ffff-ffff-facab27ddcea')
      ).not.toThrow();
    });
    it('should reject invalid uuids', async () => {
      expect(() =>
        schema.parse('ffa3bb2b-fd75-ffff-ffff-facab27ddcea/')
      ).toThrow(z.ZodError);
      expect(() => schema.parse('ffa3bb2bfd75fffffffffacab27ddcea')).toThrow(
        z.ZodError
      );
    });
  });

  describe('uuidv4', () => {
    let schema: ZodEffects<ZodString, string, string>;
    beforeAll(() => {
      schema = z.uuidv4();
    });

    it('should accept v4 uuid', async () => {
      expect(() =>
        schema.parse('77ceefa8-a8c7-4a1d-a5e2-aff1374bbd36')
      ).not.toThrow();
    });
    it('should not accept v3 uuid', async () => {
      expect(() =>
        schema.parse('9817ebe4-6afe-5dcc-b76a-2a66879676a3')
      ).toThrow(z.ZodError);
    });
    it('should not accept v5 uuid', async () => {
      expect(() =>
        schema.parse('ec5115fc-e9cb-5be1-93fe-8984b1b2fab9')
      ).toThrow(z.ZodError);
    });
    it('should not accept non-v4 uuid', async () => {
      expect(() =>
        schema.parse('9d968706-a460-11ec-b909-0242ac120002')
      ).toThrow(z.ZodError);
    });
    it('should reject invalid v4 uuids', async () => {
      expect(() =>
        schema.parse('77ceefa8-a8c7-4a1d-a5e2-aff1374bbd36/')
      ).toThrow(z.ZodError);
      expect(() => schema.parse('77ceefa8a8c74a1da5e2aff1374bbd36')).toThrow(
        z.ZodError
      );
    });
  });

  describe('zipCode', () => {
    let schema: ZodEffects<ZodString, string, string>;
    beforeAll(() => {
      schema = z.zipCode();
    });

    it('should accept valid zipCodes', async () => {
      expect(() => schema.parse('12345')).not.toThrow();
    });
    it('should reject invalid zipCodes', async () => {
      expect(() =>
        schema.parse('ffa3bb2b-fd75-ffff-ffff-facab27ddcea')
      ).toThrow(z.ZodError);
      expect(() => schema.parse('abcdef')).toThrow(z.ZodError);
    });
  });

  describe('integerString', () => {
    let schema: ZodEffects<
      ZodEffects<ZodString, string, string>,
      number,
      string
    >;
    beforeAll(() => {
      schema = z.integerString();
    });

    it('should accept valid integer strings and parse them into numbers', async () => {
      expect(schema.parse(String(Number.MIN_SAFE_INTEGER + 1))).toEqual(
        Number.MIN_SAFE_INTEGER + 1
      );
      expect(schema.parse('-100')).toEqual(-100);
      expect(schema.parse('-0')).toEqual(-0);
      expect(schema.parse('0')).toEqual(0);
      expect(schema.parse('12345')).toEqual(12345);
      expect(schema.parse(String(Number.MAX_SAFE_INTEGER - 1))).toEqual(
        Number.MAX_SAFE_INTEGER - 1
      );
    });
    it('should reject hex values', async () => {
      expect(() => schema.parse('ff')).toThrow(z.ZodError);
      expect(() => schema.parse('0xff')).toThrow(z.ZodError);
    });

    it('should reject leading zeros', async () => {
      expect(() => schema.parse('0123')).toThrow(z.ZodError);
      expect(() => schema.parse('00000')).toThrow(z.ZodError);
    });
    it('should reject infinity and NaN', async () => {
      expect(() => schema.parse('Infinity')).toThrow(z.ZodError);
      expect(() => schema.parse('NaN')).toThrow(z.ZodError);
      expect(() => schema.parse('undefined')).toThrow(z.ZodError);
      expect(() => schema.parse('null')).toThrow(z.ZodError);
    });
    it('should reject too large numbers', async () => {
      expect(() => schema.parse(String(Number.MAX_SAFE_INTEGER))).toThrow(
        z.ZodError
      );
    });
    it('should reject too small numbers', async () => {
      expect(() => schema.parse(String(Number.MIN_SAFE_INTEGER))).toThrow(
        z.ZodError
      );
    });
    it('should reject floats', async () => {
      expect(() => schema.parse('0.0')).toThrow(z.ZodError);
      expect(() => schema.parse('0,0')).toThrow(z.ZodError);
      expect(() => schema.parse('2.0')).toThrow(z.ZodError);
      expect(() => schema.parse('2,0')).toThrow(z.ZodError);
    });
  });

  describe('base64', () => {
    it('should accept valid base64', async () => {
      const schema = z.base64({ rawLength: 4 });
      expect(() => schema.parse('dGVzdA==')).not.toThrow();
    });

    it('should reject invalid padding', async () => {
      const schema = z.base64();
      expect(() => schema.parse('dGVzdA')).toThrow(z.ZodError);
    });

    it('should reject non-base64', async () => {
      const schema = z.base64();
      expect(() => schema.parse('caffee')).toThrow(z.ZodError);
    });
    it('should reject too small strings', async () => {
      const schema = z.base64({ min: 100 });
      expect(() => schema.parse('dGVzdA==')).toThrow(z.ZodError);
    });
    it('should reject too long strings', async () => {
      const schema = z.base64({ max: 3 });
      expect(() => schema.parse('dGVzdA==')).toThrow(z.ZodError);
    });

    it('should reject invalid raw lengths', async () => {
      const schema = z.base64({ rawLength: 5 });
      expect(() => schema.parse('dGVzdA==')).toThrow(z.ZodError);
    });
  });

  describe('hex', () => {
    it('should accept valid hex', async () => {
      const schema = z.hex({ rawLength: 4 });
      expect(() => schema.parse('12345678')).not.toThrow();
    });

    it('should reject non-hex', async () => {
      const schema = z.hex();
      expect(() => schema.parse('12[]{}78')).toThrow(z.ZodError);
    });
    it('should reject too small strings', async () => {
      const schema = z.base64({ min: 100 });
      expect(() => schema.parse('12345678')).toThrow(z.ZodError);
    });
    it('should reject too long strings', async () => {
      const schema = z.base64({ max: 3 });
      expect(() => schema.parse('12345678')).toThrow(z.ZodError);
    });

    it('should reject invalid raw lengths', async () => {
      const schema = z.base64({ rawLength: 5 });
      expect(() => schema.parse('12345678')).toThrow(z.ZodError);
    });
  });

  describe('unixTimestamp', () => {
    it('should accept valid unixTimestamp', async () => {
      const schema = z.unixTimestamp();
      expect(() => schema.parse(123456789)).not.toThrow();
    });

    it('should reject invalid unixTimestamp', async () => {
      const schema = z.unixTimestamp();
      expect(() => schema.parse(-123456789)).toThrow(z.ZodError);
    });
  });
});
