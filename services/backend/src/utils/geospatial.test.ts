import { convertLngLatToGeoPoint } from 'utils/geospatial';
import faker from 'faker';

describe('Convert Lng Lat to Geography Point', () => {
  beforeAll(() => {
    faker.seed(0);
  });
  it('should return undefined for invalid lng lat inputs', () => {
    expect(convertLngLatToGeoPoint({ lng: null, lat: null })).not.toBeDefined();
    expect(
      convertLngLatToGeoPoint({ lng: undefined, lat: undefined })
    ).not.toBeDefined();
    expect(
      convertLngLatToGeoPoint({ lng: undefined, lat: null })
    ).not.toBeDefined();
    expect(
      convertLngLatToGeoPoint({ lng: null, lat: faker.address.latitude() })
    ).not.toBeDefined();
    expect(
      convertLngLatToGeoPoint({
        lng: faker.address.longitude(),
        lat: undefined,
      })
    ).not.toBeDefined();
  });

  it('should return a GeoJSON point for valid lng lat inputs', () => {
    let lng;
    let lat;

    lng = 0;
    lat = 0;
    expect(convertLngLatToGeoPoint({ lng, lat })).toMatchObject({
      type: 'Point',
      coordinates: [lng, lat],
    });

    lng = 13.2;
    lat = 52.1;
    expect(convertLngLatToGeoPoint({ lng, lat })).toMatchObject({
      type: 'Point',
      coordinates: [lng, lat],
    });

    lng = -29.868;
    lat = 30.99;
    expect(convertLngLatToGeoPoint({ lng, lat })).toMatchObject({
      type: 'Point',
      coordinates: [lng, lat],
    });

    lng = faker.address.longitude();
    lat = faker.address.latitude();
    expect(convertLngLatToGeoPoint({ lng, lat })).toMatchObject({
      type: 'Point',
      coordinates: [lng, lat],
    });

    lng = 0;
    lat = 39.654;
    expect(convertLngLatToGeoPoint({ lng, lat })).toMatchObject({
      type: 'Point',
      coordinates: [lng, lat],
    });
  });
});
