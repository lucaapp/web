import { randomBytes } from 'crypto';
import { promisify } from 'util';

const promisifiedRandomBytes = promisify(randomBytes);

export const LOCATION_TABLES_ID_BYTES = 16;

export const generateLocationTableId = async (): Promise<string> =>
  promisifiedRandomBytes(LOCATION_TABLES_ID_BYTES).then(buffer =>
    buffer.toString('base64url')
  );
