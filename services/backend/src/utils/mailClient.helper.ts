import { InternalLanguages } from './language';

const MAIL_TEMPLATES = {
  activateAccount: {
    de: 2,
    en: 3,
  },
  register: {
    de: 5,
    en: 6,
  },
  updateMail: {
    de: 10,
    en: 11,
  },
  locationTransferApprovalNotification: {
    de: 12,
    en: 13,
  },
  hdLocationTransferApprovalNotification: {
    de: 14,
    en: 15,
  },
  operatorUpdatePasswordNotification: {
    de: 18,
    en: 19,
  },
  hdUpdatePasswordNotification: {
    de: 20,
    en: 21,
  },
  forgotPassword: {
    de: 22,
    en: 23,
  },
  updateMailNotification: {
    de: 24,
    en: 25,
  },
  shareData: {
    de: 29,
    en: 27,
  },
  locationsSupport: {
    de: 30,
    en: 30,
  },
  registerAttemptNotification: {
    de: 34,
    en: 35,
  },
  hdSupport: {
    de: 36,
    en: 36,
  },
  deviceActivatedNotification: {
    de: 96,
    en: 98,
  },
  hdWelcome: {
    de: 54,
    en: 54,
  },
  magicLinkEmail: {
    de: 157,
    en: 159,
  },
  magicLinkRegisterEmail: {
    de: 160,
    en: 161,
  },
};

export type ValidTemplateIds = keyof typeof MAIL_TEMPLATES;

// any is needed because of the type of the templateId parameter
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type TitleGetter = string | ((argument: any) => string);

const MAIL_TEMPLATE_TITLES: Record<
  ValidTemplateIds,
  Record<InternalLanguages, TitleGetter>
> = {
  shareData: {
    de: 'Datenanfrage',
    en: 'Share data request',
  },
  register: {
    de: 'Willkommen bei luca!',
    en: 'Welcome to luca!',
  },
  activateAccount: {
    de: 'E-Mail bestätigen',
    en: 'Confirm email',
  },
  forgotPassword: {
    de: 'Passwort zurücksetzen',
    en: 'Reset password',
  },
  updateMail: {
    de: 'E-Mail aktualisieren',
    en: 'Update email',
  },
  updateMailNotification: {
    de: 'E-Mail erfolgreich geändert',
    en: 'Email successfully changed',
  },
  operatorUpdatePasswordNotification: {
    de: 'Passwort erfolgreich geändert',
    en: 'Password successfully changed',
  },
  locationsSupport: {
    de: 'Neue Support Anfrage eines Betreibers',
    en: 'Neue Support Anfrage eines Betreibers',
  },
  hdSupport: {
    de: 'Neue Support Anfrage eines Gesundheitsamts',
    en: 'Neue Support Anfrage eines Gesundheitsamts',
  },
  hdUpdatePasswordNotification: {
    de: 'Passwort erfolgreich geändert',
    en: 'Password successfully changed',
  },
  locationTransferApprovalNotification: {
    de: ({ departmentName }) =>
      `Bestätigung der Datenfreigabe an das ${departmentName}`,
    en: ({ departmentName }) =>
      `Data Sharing for Health Department ${departmentName} Completed`,
  },
  hdLocationTransferApprovalNotification: {
    de: 'Datenfreigabe akzeptiert',
    en: 'Data Sharing accepted',
  },
  registerAttemptNotification: {
    de: 'Registrierung mit bereits bestehender E-Mail-Adresse',
    en: 'Registration with already existing e-mail address',
  },
  deviceActivatedNotification: {
    de: 'Ein neues luca Locations Gerät wurde aktiviert',
    en: 'A new luca Locations device has been activated',
  },
  hdWelcome: {
    de: 'Herzlich Willkommen bei luca Gesundheitsamt',
    en: 'Herzlich Willkommen bei luca Gesundheitsamt',
  },
  magicLinkEmail: {
    de: 'Logge dich mit diesem Link ein',
    en: 'Log in with this link',
  },
  magicLinkRegisterEmail: {
    de: 'Erstelle einen luca Locations Account',
    en: 'Create a luca Locations account',
  },
};

export const getMailId = (
  id: ValidTemplateIds,
  lang: InternalLanguages | null
): number => {
  if (!(id in MAIL_TEMPLATES)) {
    throw new Error('Invalid email template id');
  }
  // eslint-disable-next-line security/detect-object-injection
  return MAIL_TEMPLATES[id][lang || 'de'];
};

export const getMailTitle = (
  id: ValidTemplateIds,
  lang: InternalLanguages | null,
  parameters = {}
): string => {
  if (!(id in MAIL_TEMPLATE_TITLES)) {
    throw new Error('Invalid email template title');
  }

  // eslint-disable-next-line security/detect-object-injection
  const title = MAIL_TEMPLATE_TITLES[id][lang || 'de'];

  if (typeof title === 'function') {
    return title(parameters);
  }

  return title;
};
