import config from 'config';
import bigInt, { BigInteger } from 'big-integer';
import { SHA256, bytesToHex } from '@lucaapp/crypto';
import redis from 'utils/redis';

const RESET_DIFFICULTY_TIME = config.get('pow.resetAfter');

const keyGenerator = (powType: string, ip: string) =>
  `pow:${powType}:${SHA256(bytesToHex(ip))}`;

const getChallengeDifficulty = (
  x: number,
  a: number,
  b: number,
  c: number
): { difficulty: number; t: BigInteger } => {
  const difficulty = a * b ** x + c;
  const tf = difficulty;
  const t = bigInt(Math.min(Math.floor(tf), Number.MAX_SAFE_INTEGER));
  return { difficulty, t };
};

interface DifficultyParameters {
  type: string;
  a: number;
  b: number;
  c: number;
}

export const getDifficulty = async (
  parameters: DifficultyParameters,
  ip: string
): Promise<{ t: BigInteger; difficulty: number }> => {
  const key = keyGenerator(parameters.type, ip);
  return new Promise((resolve, reject) => {
    redis.client
      .multi()
      .incr(key)
      .pttl(key)
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      .exec((error: any, replies: any) => {
        if (error) {
          return reject(error);
        }

        if (replies[1] === -1) {
          redis.client.pexpire(key, RESET_DIFFICULTY_TIME);
        }
        const requestCount = Number.parseInt(replies[0], 10);
        const { t, difficulty } = getChallengeDifficulty(
          requestCount,
          parameters.a,
          parameters.b,
          parameters.c
        );
        return resolve({ t, difficulty });
      });
  });
};
