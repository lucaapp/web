import config from 'config';
import redis, { RetryStrategyOptions, RedisClient, Callback } from 'redis';
import { promisify } from 'util';
import logger from './logger';
import { registerShutdownHandler } from './lifecycle';

const RETRY_INTERVAL_MS = 500;

const retryStrategy = (info: RetryStrategyOptions) => {
  logger.warn(info, `retrying redis connection`);
  return RETRY_INTERVAL_MS;
};

type CustomizedRedisClient = RedisClient & {
  get(
    key: string | Buffer,
    callback?: Callback<string | null>
  ): string | Buffer;
  set(key: string | Buffer, value: string | Buffer): Promise<void>;
};

const client = redis.createClient({
  host: config.get('redis.hostname'),
  password: config.get('redis.password'),
  enable_offline_queue: true,
  detect_buffers: true,
  db: config.get('redis.database'),
  retry_strategy: retryStrategy,
}) as CustomizedRedisClient;

client.on('error', error => {
  logger.error(error);
});

const promiseClient = {
  client,
  quit: promisify(client.quit).bind(client),
  set: promisify(client.set).bind(client),
  get: promisify(client.get).bind(client),
};

registerShutdownHandler(async () => {
  logger.info('closing redis connection');
  await promiseClient.quit();
  logger.info('redis connection closed');
});

export default promiseClient;
