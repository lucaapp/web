/* eslint no-underscore-dangle: 0 */
const config = require('config');
const moment = require('moment');
const { parentPort } = require('worker_threads');
const { BloomFilter } = require('bloomit');
const { performance } = require('perf_hooks');

const { SHA256, uuidToHex } = require('@lucaapp/crypto');

const FALSE_POSITIVE_RATE = config.get('bloomFilter.falsePositiveRate');
const KEEP_ALIVE_PING_DURATION = config.get('bloomFilter.keepAlivePing');

const unregisteredBadgeBloomFilter = async unregisteredBadges => {
  let lastKeepAlive = moment();
  const t0 = performance.now();
  const filter = BloomFilter.create(
    unregisteredBadges.length,
    FALSE_POSITIVE_RATE
  );
  unregisteredBadges.forEach(badge => {
    if (moment().diff(lastKeepAlive) > KEEP_ALIVE_PING_DURATION) {
      parentPort.postMessage({ keepAlivePing: true });
      lastKeepAlive = moment();
    }
    const uuidSHA256 = SHA256(uuidToHex(badge.uuid));
    filter.add(uuidSHA256);
  });
  return {
    bloomFilterArrayDump: filter.export(),
    time: performance.now() - t0,
  };
};

parentPort.on('message', async unregisteredBadges => {
  const bloomFilter = await unregisteredBadgeBloomFilter(unregisteredBadges);
  parentPort.postMessage(bloomFilter);
});
