import parsePhoneNumber from 'libphonenumber-js/max';
import { PhoneNumberBlockList } from 'database';
import { hashPhoneNumber } from 'utils/hash';

export async function isPhoneNumberBlocked(
  phoneNumber: string
): Promise<boolean> {
  const phone = parsePhoneNumber(phoneNumber, 'DE');

  if (!phone) return true;

  const hashedPhoneNumber = await hashPhoneNumber(phone.number as string);

  const foundBlockedNumber = await PhoneNumberBlockList.findOne({
    where: {
      phoneNumber: hashedPhoneNumber,
    },
  });

  return !!foundBlockedNumber;
}
