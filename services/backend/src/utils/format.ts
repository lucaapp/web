import parsePhoneNumber from 'libphonenumber-js/max';

import { LocationInstance } from 'database/models/location';
import { LocationGroupInstance } from 'database/models/locationGroup';

export const formatLocationName = (
  location: LocationInstance,
  group?: LocationGroupInstance
): string => {
  if (!group) {
    return location.name || '';
  }
  return location.name ? `${group.name} - ${location.name}` : `${group.name}`;
};

type FormatPhoneNumberFunction = {
  (phone: null): null;
  (phone: string | undefined): string | undefined;
};
export const formatPhoneNumber = ((
  phone: string | undefined | null
): string | undefined | null => {
  if (!phone) return phone;
  return parsePhoneNumber(phone, 'DE')?.formatInternational();
}) as FormatPhoneNumberFunction;
