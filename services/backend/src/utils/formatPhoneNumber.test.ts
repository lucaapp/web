import { formatPhoneNumber } from 'utils/format';

describe('Format phone number', () => {
  it('should return null if the value is nullish', () => {
    expect(formatPhoneNumber(undefined)).toEqual(undefined);
    expect(formatPhoneNumber(null)).toEqual(null);
    expect(formatPhoneNumber('')).toEqual('');
  });

  it('should return undefined if the value is invalid', () => {
    expect(formatPhoneNumber('    ')).toEqual(undefined);
    expect(formatPhoneNumber('abcd')).toEqual(undefined);
    expect(formatPhoneNumber('0')).toEqual(undefined);
  });

  it('should return a formatted phone number if the value is valid', () => {
    const validPhoneNumbers = [
      '+4917112345678',
      '+49 171 12345678',
      '+49/171/12345678',
      '004917112345678',
      '03012345678',
      '017112345678',
      ' 017112345678  ',
      '0 1 7 1 1 2 3 4 5 6 7 8',
      '+43 1777809864',
      '+852 7777 3333',
      '+39 347 1234567',
    ];

    const formattedPhoneNumbers = [
      '+49 171 12345678',
      '+49 171 12345678',
      '+49 171 12345678',
      '+49 171 12345678',
      '+49 30 12345678',
      '+49 171 12345678',
      '+49 171 12345678',
      '+49 171 12345678',
      '+43 1 777809864',
      '+852 7777 3333',
      '+39 347 123 4567',
    ];

    for (const [index, validPhoneNumber] of validPhoneNumbers.entries()) {
      expect(formatPhoneNumber(validPhoneNumber)).toEqual(
        formattedPhoneNumbers[index]
      );
    }
  });
});
