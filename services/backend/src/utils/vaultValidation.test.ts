import { vaultGETValidation } from './vaultValidation';

const TEST_TOTP_SECRET = Buffer.from(
  '5D5SeKpgBijxz42Q1lCujA4yxJA2UtVG/uuRjeLuFQ4=',
  'base64'
);
const TOTP_TOKEN_NOW = 'AAAAAEmWAtLY6lKgrHby7W_85pI0zQVOhCvg6fIj470';
const TEST_TIMESTAMP = 1234567890;

describe('vaultValidation', () => {
  const requestHeader = {
    'x-luca-totp-token': TOTP_TOKEN_NOW,
  };

  it('success', async () => {
    const validation = await vaultGETValidation(
      requestHeader,
      1,
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET
    );
    expect(validation).toEqual(true);
  });

  it('token not url safe', async () => {
    const notUrlSafe = {
      'x-luca-totp-token': '//eNTEunESpyTxs+afsXIWBknGJyVLmDY+bpkia4u6k=',
    };
    const validation = await vaultGETValidation(
      notUrlSafe,
      1,
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET
    );
    expect(validation).toEqual(false);
  });

  it('version is 0', async () => {
    const validation = await vaultGETValidation(
      requestHeader,
      0,
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET
    );
    expect(validation).toEqual(false);
  });

  it('version is 2', async () => {
    const validation = await vaultGETValidation(
      requestHeader,
      2,
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET
    );
    expect(validation).toEqual(false);
  });

  it('no token set', async () => {
    const wrongRequestHeader = {};
    const validation = await vaultGETValidation(
      wrongRequestHeader,
      1,
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET
    );
    expect(validation).toEqual(false);
  });

  it('token not base64', async () => {
    const wrongRequestHeader = {
      'x-luca-totp-token': 'Xs1O{}==',
    };
    const validation = await vaultGETValidation(
      wrongRequestHeader,
      1,
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET
    );
    expect(validation).toEqual(false);
  });

  it('token has an object', async () => {
    const wrongRequestHeader = {
      'x-luca-totp-token': '{ test: test, other: other}',
    };
    const validation = await vaultGETValidation(
      wrongRequestHeader,
      1,
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET
    );
    expect(validation).toEqual(false);
  });
});
