import { totpValidation } from './totpValidation';

const TEST_TOTP_SECRET = Buffer.from(
  '5D5SeKpgBijxz42Q1lCujA4yxJA2UtVG/uuRjeLuFQ4=',
  'base64'
);
const TEST_TIMESTAMP = 1234567890;
const TOTP_TOKEN_NOW = 'AAAAAEmWAtLY6lKgrHby7W_85pI0zQVOhCvg6fIj470';
const TOTP_TOKEN_MINUS_60_S = 'AAAAAEmWApbNInr60tdsdSnUl6w7WcJ6lyb2HFxwGBk';
const TOTP_TOKEN_MINUS_120_S = 'AAAAAEmWAlrik22p7N9OE7c3YJ6WzyaLLWPjK1hpiqw';
const TOTP_TOKEN_MINUS_180_S = 'AAAAAEmWAh7QywmM5mQSFxlRspB0IPll12iZxvs_VN0';
const TOTP_TOKEN_MINUS_240_S = 'AAAAAEmWAeKvzhWMq7PX_p1BX_6cx1aBtOcJohW5iRU';
const TOTP_TOKEN_MINUS_300_S = 'AAAAAEmWAaZDaBVnl-Wp9LFL5Z_xgN0icPejkPjEECQ';
const TOTP_TOKEN_MINUS_360_S = 'AAAAAEmWAWoblX7MJ4BtvYTB-aqjtediPkCWqqLJkWc';
const TOTP_TOKEN_PLUS_60_S = 'AAAAAEmWAw5aKgk1z0GM9ktGP84a5TZKvFrHiukeAoE';
const TOTP_TOKEN_PLUS_120_S = 'AAAAAEmWA0pKsqpOVC0qvNZQDjEJxdoMZi08RisI0pk';
const TOTP_TOKEN_PLUS_180_S = 'AAAAAEmWA4ZDLcVs16t-UiZ15vSCex_l1aGFaXl3oC0';
const TOTP_TOKEN_PLUS_240_S = 'AAAAAEmWA8Ifzrd8hYjsnTLoPfIbh4fT0h3fam7nDKU';
const TOTP_TOKEN_PLUS_300_S = 'AAAAAEmWA_7APKZzxOOu74dt2vR7oLtkn6X3B7dbAD0';
const TOTP_TOKEN_PLUS_360_S = 'AAAAAEmWBDoCQTcj88J6lvT4kcy-E3xh0a_pxAgKJWI';

describe('totpValidation', () => {
  it('accept now token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_NOW, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(true);
  });

  it('accept 60s in the past token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_MINUS_60_S, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(true);
  });

  it('accept 120s in the past token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_MINUS_120_S, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(true);
  });

  it('accept 180s in the past token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_MINUS_180_S, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(true);
  });

  it('accept 240s in the past token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_MINUS_240_S, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(true);
  });

  it('accept 300s in the past token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_MINUS_300_S, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(true);
  });

  it('not accept 360s in the past token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_MINUS_360_S, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(false);
  });

  it('accept 60s in the future token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_PLUS_60_S, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(true);
  });

  it('accept 120s in the future token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_PLUS_120_S, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(true);
  });

  it('accept 180s in the future token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_PLUS_180_S, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(true);
  });

  it('accept 240s in the future token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_PLUS_240_S, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(true);
  });

  it('accept 300s in the future token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_PLUS_300_S, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(true);
  });

  it('not accept 360s in the future token', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_PLUS_360_S, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(false);
  });

  it('secret too small', async () => {
    const smallSecret = Buffer.from('5D5SeK==', 'base64');
    const totpToken = Buffer.from(TOTP_TOKEN_NOW, 'base64url');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      smallSecret,
      totpToken
    );
    expect(validation).toEqual(false);
  });

  it('token too small', async () => {
    const totpToken = Buffer.from('ukkGvA==', 'base64');
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(false);
  });

  it('secret as token not valid', async () => {
    const validation = await totpValidation(
      TEST_TIMESTAMP,
      TEST_TOTP_SECRET,
      TEST_TOTP_SECRET
    );
    expect(validation).toEqual(false);
  });

  it('now not valid', async () => {
    const totpToken = Buffer.from(TOTP_TOKEN_NOW, 'base64url');
    const validation = await totpValidation(
      Math.floor(Date.now() / 1000),
      TEST_TOTP_SECRET,
      totpToken
    );
    expect(validation).toEqual(false);
  });
});
