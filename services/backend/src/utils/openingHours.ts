import { z } from 'zod';

export type OpeningHours = z.infer<typeof openingHoursSchema>;

export type SingleDayOpeningHours = z.infer<typeof singleDayOpeningHoursSchema>;

export type OpeningHoursTimespan = z.infer<typeof timeSpanSchema>;

const timeStringSchema = z
  .string()
  .refine(value => /^(2[0-4]|1\d|0\d):[0-5]\d$/.test(value), {
    message: 'invalid opening hours format. required: [HH:mm, HH:mm]',
  });
const timeSpanSchema = z.tuple([timeStringSchema, timeStringSchema]);
const singleDayOpeningHoursSchema = z.array(
  z.object({
    included: z.boolean(),
    openingHours: timeSpanSchema,
  })
);

export const openingHoursSchema = z
  .object({
    mon: singleDayOpeningHoursSchema.nullable(),
    tue: singleDayOpeningHoursSchema.nullable(),
    wed: singleDayOpeningHoursSchema.nullable(),
    thu: singleDayOpeningHoursSchema.nullable(),
    fri: singleDayOpeningHoursSchema.nullable(),
    sat: singleDayOpeningHoursSchema.nullable(),
    sun: singleDayOpeningHoursSchema.nullable(),
  })
  .nullable();
