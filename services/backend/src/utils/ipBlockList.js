const config = require('config');
const moment = require('moment');
const { Op } = require('sequelize');
const { chunk } = require('lodash');
const IPCIDR = require('ip-cidr');
const { parse } = require('csv-parse');
const { database, IPAddressBlockList } = require('../database');
const { proxyClient } = require('./proxy');
const logger = require('./logger').default;

const fetchIpAddressesFromSource = async url => {
  try {
    const list = await proxyClient.get(`${url}`, { timeout: 20000 });
    return {
      list: list.data,
      url,
    };
  } catch (error) {
    logger.warn(error, `failed to update ip blocklist from ${url}`);
    return {
      error: error.message,
      source: url,
    };
  }
};

const generateIpLists = async urls => {
  return Promise.all(urls.map(url => fetchIpAddressesFromSource(url)));
};

const getIpRange = (input, source = '') => {
  const ip = new IPCIDR(input.match('/') ? input : `${input}/32`);
  return {
    startIp: ip.start(),
    endIp: ip.end(),
    source,
  };
};

const getIpsFromNetsetFile = (content, source = '') => {
  if (content.error) {
    return [content];
  }
  return content?.list
    .split('\n')
    .filter(line => !line.startsWith('#'))
    .filter(line => IPCIDR.isValidAddress(line))
    .map(line => getIpRange(line, source));
};

const getIpsfromCSVFile = (content, index = 0, source = '') => {
  if (content.error) {
    return [content];
  }
  const records = parse(content?.list, { skip_empty_lines: true });
  return records
    .map(record => record[Number.parseInt(index, 10)])
    .filter(ip => IPCIDR.isValidAddress(ip))
    .map(line => getIpRange(line, source));
};

const getIpRangefromCSVFile = (content, first = 0, second = 1, source = '') => {
  if (content.error) {
    return [content];
  }
  const records = parse(content?.list, { skip_empty_lines: true });
  return records
    .map(record => [
      record[Number.parseInt(first, 10)],
      record[Number.parseInt(second, 10)],
    ])
    .filter(
      ([startIp, endIp]) =>
        IPCIDR.isValidAddress(startIp) && IPCIDR.isValidAddress(endIp)
    )
    .map(([startIp, endIp]) => ({
      startIp,
      endIp,
      source,
    }));
};

const getIpList = async () => {
  let listData;
  let netsetIps = [];
  let csvSingleIps = [];
  let csvDoubleIps = [];
  const netsetUrls = config.get('blockListSources.netset').split(',');
  listData = await generateIpLists(netsetUrls);
  listData.forEach(data => {
    netsetIps = [...netsetIps, ...getIpsFromNetsetFile(data, data.url)];
  });

  const csvSingleUrls = config.get('blockListSources.singleCSV').split(',');
  listData = await generateIpLists(csvSingleUrls);
  listData.forEach(data => {
    csvSingleIps = [...csvSingleIps, ...getIpsfromCSVFile(data, 0, data.url)];
  });

  const csvDoubleUrls = config.get('blockListSources.doubleCSV').split(',');
  listData = await generateIpLists(csvDoubleUrls);
  listData.forEach(data => {
    csvDoubleIps = [
      ...csvDoubleIps,
      ...getIpRangefromCSVFile(data, 2, 3, data.url),
    ];
  });
  return [...netsetIps, ...csvSingleIps, ...csvDoubleIps];
};

const updateBlockList = async () => {
  const now = moment();
  let ips;
  const failedSources = [];
  try {
    ips = await getIpList();

    const ipsWithTimestamp = ips
      .filter(ip => {
        if (ip.error) {
          failedSources.push(ip.source);
        }
        return !ip.error;
      })
      .map(ip => {
        return { ...ip, createdAt: now };
      });
    if (ipsWithTimestamp.length === 0) {
      return;
    }

    await database.transaction(async transaction => {
      for (const chunkedIps of chunk(ipsWithTimestamp, 1000)) {
        await IPAddressBlockList.bulkCreate(chunkedIps, {
          returning: false,
          transaction,
        });
      }

      await IPAddressBlockList.destroy({
        where: {
          createdAt: { [Op.lt]: now },
          source: { [Op.notIn]: failedSources },
        },
        transaction,
      });
    });
  } catch (error) {
    logger.error(error);
    return;
  }

  logger.info(`IPAddressBlockList updated. Entries count: ${ips.length}`);
};

module.exports = {
  updateBlockList,
};
