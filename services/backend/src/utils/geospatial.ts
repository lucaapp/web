import { Point } from 'geojson';
import { isNaN } from 'lodash';

interface LocationLngLat {
  lng: number | null | undefined;
  lat: number | null | undefined;
}
export const convertLngLatToGeoPoint = (
  position: LocationLngLat
): Point | undefined =>
  position.lng == null ||
  position.lat == null ||
  isNaN(position.lng) ||
  isNaN(position.lat)
    ? undefined
    : { type: 'Point', coordinates: [position.lng, position.lat] };
