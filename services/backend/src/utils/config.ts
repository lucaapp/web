import config from 'config';
import logger from 'utils/logger';
import configSchema from '../../config/schema';

export const validateConfig = (): void => {
  try {
    configSchema.parse(config.util.toObject());
  } catch (error) {
    logger.error(
      error instanceof Error ? error : { error },
      `Failed to validate config schema`
    );
    throw error;
  }
};
