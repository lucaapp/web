/* eslint-disable prefer-rest-params, @typescript-eslint/no-explicit-any */
/*  - Usage of any was required for the type of the response.end and for the usage
 *    of the applyable methods. (transforming arguments were permitted by type).
 *  - Rest params broke the applyable methods.
 */

import { Request, Response, NextFunction } from 'express';
import { performance } from 'perf_hooks';
import { JobCompletion } from 'database';

const logJobCompletion = async (
  name: string,
  meta: Record<string, string>
): Promise<void> => {
  await JobCompletion.create({
    name,
    meta: JSON.stringify(meta),
  });
};

export function jobLoggerMiddleware(
  request: Request,
  response: Response,
  next: NextFunction
): void {
  const [oldWrite, oldEnd] = [response.write, response.end];
  const chunks: Buffer[] = [];
  const t0 = performance.now();
  const jobName = request.originalUrl.split('/').pop();

  // Typescript itself prevents overwriting the response.write
  (response.write as unknown) = function write(chunk: string) {
    chunks.push(Buffer.from(chunk));
    oldWrite.apply(response, arguments as any);
  };

  (response.end as unknown) = function end(chunk: any) {
    if (chunk) {
      chunks.push(Buffer.from(chunk));
    }
    const body = Buffer.concat(chunks).toString('utf8');

    const parsedRows = JSON.parse(body);

    logJobCompletion(jobName as string, {
      ...parsedRows,
      time: performance.now() - t0,
    });

    oldEnd.apply(response, arguments as any);
  };

  next();
}
