import { IncomingHttpHeaders } from 'http';

import validator from 'validator';
import { totpValidation } from './totpValidation';

export const vaultGETValidation = async (
  requestHeader: IncomingHttpHeaders,
  vaultVersion: number,
  timestampInS: number,
  totpSecret: Buffer
): Promise<boolean> => {
  if (vaultVersion !== 1) {
    // only version 1 is valid
    return false;
  }

  const totpTokenBase64 = requestHeader['X-Luca-Totp-Token'.toLowerCase()];
  if (
    typeof totpTokenBase64 !== 'string' ||
    !validator.isBase64(totpTokenBase64, { urlSafe: true })
  ) {
    // invalid token
    return false;
  }

  const totpToken = Buffer.from(totpTokenBase64, 'base64url');

  return totpValidation(timestampInS, totpSecret, totpToken);
};
