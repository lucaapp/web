declare module 'node-sequelize-stream' {
  export default function sequelizeStream(sequelize: Sequelize): void;
}
