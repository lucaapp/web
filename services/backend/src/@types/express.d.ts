type Operator = import('../database/models/operator').OperatorInstance;
interface IOperator extends Operator {
  type: 'Operator';
}

type OperatorDevice = import('../database/models/operatorDevice').OperatorDeviceInstance;

interface IOperatorDevice extends OperatorDevice {
  type: 'OperatorDevice';
  device: {
    uuid: string;
    name: string;
    os: 'android' | 'ios';
    role: 'scanner' | 'employee' | 'manager';
  };
}

interface IOperatorDevice extends IOperator {
  deviceId: string;
  activated: boolean;
  deviceName: string;
  deviceOS: 'android' | 'ios';
}

type IOperatorOrOperatorDevice = IOperator | IOperatorDevice;

type HealthDepartmentEmployee = import('../database/models/healthDepartmentEmployee').HealthDepartmentEmployeeInstance;
type HealthDepartment = import('../database/models/healthDepartment').HealthDepartmentInstance;

interface IHealthDepartmentEmployee extends HealthDepartmentEmployee {
  type: 'HealthDepartmentEmployeee';
  HealthDepartment: HealthDepartment;
}

declare namespace Express {
  export interface Request {
    user?: IUser | IOperator | IOperatorDevice | IHealthDepartmentEmployee;
  }

  export interface Response {
    headers: Record<string, string>;
    addEtag(value: string | Buffer | unknown, etag?: string): void;
  }
}

declare module 'http' {
  export interface IncomingMessage {
    headers: Record<string, string>;
    originalUrl: string;
  }

  export interface ServerResponse {
    headers: Record<string, string>;
  }
}
