/* eslint-disable promise/no-callback-in-promise */
import config from 'config';
import moment from 'moment';
import { ApiError, ApiErrorType } from 'utils/apiError';
import { Request } from 'express';
import { Strategy, VerifiedCallback } from 'passport-custom';
import { EmailLoginToken, Operator, database } from 'database';
import { OperatorInstance } from 'database/models/operator';
import { UserType } from 'constants/user';
import { Op } from 'sequelize';

interface OperatorUser extends OperatorInstance {
  type: UserType;
}

const EMAIL_EXPIRY_HOURS = config.get('emails.expiry');

export const emailLoginTokenStrategy = new Strategy(
  async (request: Request, done: VerifiedCallback) => {
    try {
      const { token } = request.body;
      let user;
      await database.transaction(async transaction => {
        const loginToken = await EmailLoginToken.findOne({
          where: {
            token,
            usedAt: null,
            createdAt: {
              [Op.gt]: moment().subtract(EMAIL_EXPIRY_HOURS, 'hour').toDate(),
            },
          },
          transaction,
          lock: transaction.LOCK.UPDATE,
        });

        if (!loginToken) {
          return;
        }

        await loginToken.update({ usedAt: moment().toDate() }, { transaction });

        const operator = await Operator.findByPk(loginToken.operatorId);
        user = operator as OperatorUser;
        user.type = UserType.OPERATOR;
      });
      if (user) {
        return done(null, user);
      }
      return done(new ApiError(ApiErrorType.UNAUTHORIZED));
    } catch (error) {
      request.log.warn(error as Error);
      return done(new ApiError(ApiErrorType.UNAUTHORIZED));
    }
  }
);
