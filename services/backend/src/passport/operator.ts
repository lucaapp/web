import config from 'config';
import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt';
import { UserType } from 'constants/user';
import { Operator } from 'database';
import { OperatorInstance } from 'database/models/operator';

interface OperatorUser extends OperatorInstance {
  type: UserType;
}

const operatorStrategy = new JwtStrategy(
  {
    algorithms: ['ES256'],
    secretOrKey: config.get('jwk.publicKey'),
    jwtFromRequest: ExtractJwt.fromHeader('x-auth'),
  },
  async (jwtPayload, done) => {
    try {
      const operator = await Operator.findByPk(jwtPayload.sub);

      if (!operator) {
        return done({ errorType: 'OPERATOR_NOT_FOUND' }, false);
      }

      const user = operator as OperatorUser;
      user.type = UserType.OPERATOR;

      return done(null, user);
    } catch (error) {
      return done(error, false);
    }
  }
);

export { operatorStrategy };
