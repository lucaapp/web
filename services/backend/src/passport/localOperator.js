/* eslint-disable promise/no-callback-in-promise */
/* eslint-disable no-param-reassign */
const LocalStrategy = require('passport-local').Strategy;
const moment = require('moment');
const { UserType } = require('../constants/user');

const { getPreferredLanguageFromRequest } = require('../utils/language');
const { pseudoHashPassword } = require('../utils/hash');

const database = require('../database');

const localStrategy = new LocalStrategy(
  {
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true,
  },
  async (request, username, password, done) => {
    const user = await database.Operator.findOne({
      where: { username },
      paranoid: false, // allow soft-deleted operators
    });

    if (!user) {
      // prevent timing issues
      await pseudoHashPassword();
      return done({ errorType: 'USER_NOT_FOUND' }, null);
    }

    if (user.bannedAt && user.bannedAt <= moment().toDate()) {
      return done({ errorType: 'USER_BANNED' }, null);
    }

    const isValidPassword = await user.checkPassword(password);
    if (!isValidPassword) {
      return done({ errorType: 'WRONG_PASSWORD' }, null);
    }
    if (!user.activated) {
      return done({ errorType: 'UNACTIVATED' }, null);
    }

    const language = getPreferredLanguageFromRequest(request);

    if (user.language !== language) {
      await user.update({ language });
    }

    user.type = UserType.OPERATOR;
    return done(null, user);
  }
);

module.exports = localStrategy;
