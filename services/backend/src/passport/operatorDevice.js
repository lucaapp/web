/* eslint-disable promise/no-callback-in-promise */
const config = require('config');
const moment = require('moment');
const { Strategy: JWTStrategy, ExtractJwt } = require('passport-jwt');

const database = require('../database');

const REFRESH_TOKEN_GRACE_SECONDS = 60;

const operatorDeviceStrategy = new JWTStrategy(
  {
    algorithms: ['ES256'],
    secretOrKey: config.get('keys.operatorDevice.publicKey'),
    jwtFromRequest: ExtractJwt.fromBodyField('refreshToken'),
  },
  async (jwtPayload, done) => {
    try {
      const device = await database.OperatorDevice.findOne({
        where: {
          uuid: jwtPayload.deviceId,
        },
        include: {
          model: database.Operator,
          where: {
            activated: true,
          },
        },
      });

      if (!device) {
        return done({ errorType: 'DEVICE_NOT_FOUND' }, false);
      }

      const jwtIsCurrent = jwtPayload.iat >= moment(device.refreshedAt).unix();
      const jwtIsLast =
        jwtPayload.iat >= moment(device.refreshedAtBefore).unix() &&
        !jwtIsCurrent;
      const isGracePeriod =
        moment
          .duration(moment().diff(moment(device.refreshedAt)))
          .asSeconds() <= REFRESH_TOKEN_GRACE_SECONDS;

      if (!jwtIsCurrent && !(jwtIsLast && isGracePeriod)) {
        return done({ errorType: 'DEVICE_NOT_FOUND' }, false);
      }

      const isReactivationOfDeviceToOld =
        moment(device.reactivatedAt).unix() <
        moment()
          .subtract(
            config.get('keys.operatorDevice.maxReactivationAge'),
            'milliseconds'
          )
          .unix();

      const isDeviceExpired =
        moment(device.refreshedAt).unix() <
        moment()
          .subtract(config.get('keys.operatorDevice.expire'), 'milliseconds')
          .unix();

      if (
        isDeviceExpired &&
        (!device.reactivatedAt || isReactivationOfDeviceToOld)
      ) {
        return done({ errorType: 'DEVICE_EXPIRED' }, false);
      }

      const user = {
        type: 'OperatorDevice',
        deviceId: device.uuid,
        uuid: device.Operator.uuid,
        activated: device.activated,
      };
      return done(null, user);
    } catch (error) {
      return done(error, false);
    }
  }
);
module.exports = operatorDeviceStrategy;
