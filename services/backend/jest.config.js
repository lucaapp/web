/* eslint-disable unicorn/prefer-module */
/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  rootDir: '.',
  roots: ['<rootDir>/src'],
  testMatch: ['<rootDir>/src/**/*.test.ts', '<rootDir>/src/**/*.test.js'],
  moduleNameMapper: {
    '^utils/(.*)$': '<rootDir>/src/utils/$1',
    '^constants/(.*)$': '<rootDir>/src/constants/$1',
    '^database/(.*)$': '<rootDir>/src/database/$1',
    '^database$': '<rootDir>/src/database/index',
    '^middlewares/(.*)$': '<rootDir>/src/middlewares/$1',
    '^passport/(.*)$': '<rootDir>/src/passport/$1',
    '^routes/(.*)$': '<rootDir>/src/routes/$1',
    '^testing/(.*)$': '<rootDir>/src/testing/$1',
  },
  collectCoverageFrom: [
    '<rootDir>/src/**/*.{js,ts}',
    '!<rootDir>/src/index.ts',
    '!<rootDir>/src/database/config.js',
    '!<rootDir>/src/database/migrations/**',
    '!<rootDir>/src/database/seeds/**',
    '!<rootDir>/src/@types/**',
  ],
  coverageDirectory: '<rootDir>/coverage',
  coverageReporters: ['lcov', 'text', 'cobertura'],
  coverageThreshold: {
    global: {
      statements: 75,
      branches: 25,
      lines: 75,
      functions: 68,
    },
    './src/utils/': {
      statements: 50,
      branches: 21,
      lines: 49,
      functions: 25,
    },
    './src/routes/': {
      statements: 46,
      branches: 2,
      lines: 46,
      functions: 3,
    },
  },
  globals: { 'ts-jest': { tsconfig: 'tsconfig.test.json' } },
  testTimeout: 30000,
  reporters: [
    'default',
    [
      'jest-junit',
      {
        classNameTemplate: '{suitename}',
        titleTemplate: '{classname} {title}',
        addFileAttribute: 'true',
        filePathPrefix: 'services/backend',
      },
    ],
  ],
};
