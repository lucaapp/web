/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  reporters: [
    'default',
    [
      'jest-junit',
      {
        classNameTemplate: '{suitename}',
        titleTemplate: '{classname} {title}',
        addFileAttribute: 'true',
        filePathPrefix: 'services/contact-form',
      },
    ],
  ],
};
