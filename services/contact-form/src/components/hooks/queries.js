import { useQuery } from 'react-query';
import { getValidatedExtractedDailyKey } from 'utils/dailyKey';

import { getAdditionalData, getLocationTables } from 'network/api';

export const QUERY_KEYS = {
  DAILY_KEY: 'DAILY_KEY',
  LOCATION_TABLES: 'LOCATION_TABLES',
  LOCATION: 'LOCATION',
  ADDITIONAL_DATA: ' ADDITIONAL_DATA',
};

export const useDailyKey = () =>
  useQuery(QUERY_KEYS.DAILY_KEY, getValidatedExtractedDailyKey, {
    cacheTime: 0,
    retry: 0,
  });

export const useGetLocationTables = locationId =>
  useQuery(
    [QUERY_KEYS.LOCATION_TABLES, locationId],
    () => getLocationTables(locationId),
    {
      cacheTime: 0,
    }
  );

export const useAdditionalData = locationId =>
  useQuery([QUERY_KEYS.ADDITIONAL_DATA, locationId], () =>
    getAdditionalData(locationId)
  );
