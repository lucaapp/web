import styled from 'styled-components';

export const StyledLink = styled.a`
  color: rgb(78, 97, 128);

  &:hover {
    color: rgb(151, 165, 187);
  }
`;
