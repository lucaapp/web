import styled from 'styled-components';
import { Input, Select } from 'antd';

export const DefaultStyledInput = styled(Input)`
  height: 40px;
  border: 0.5px solid rgb(0, 0, 0);
  border-radius: 0px;
  background: transparent;

  :focus,
  :hover,
  :active {
    border-radius: 0;
    border: 0.5px solid rgb(0, 0, 0);
    height: 40px;
    background: transparent;
    box-shadow: none;
  }
`;

export const DefaultStyledSelect = styled(Select)`
  .ant-select-selector {
    border-radius: 0 !important;
    border: 0.5px solid #000000 !important;
    height: 40px !important;
    background-color: transparent !important;
    box-shadow: none !important;
    cursor: pointer !important;

    &:focus,
    &:hover,
    &:active {
      border-radius: 0;
      border: 0.5px solid #000000;
      height: 40px;
      background-color: transparent;
      box-shadow: none !important;
      cursor: pointer !important;
    }
  }

  .ant-select-selection-item {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .ant-select-arrow {
    margin-right: 8px;
    color: #000000;
  }
`;
