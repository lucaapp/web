import React from 'react';

import { useFormErrors } from '../hooks';

export const WithFormErrors = ({ children }) => {
  useFormErrors();

  return <>{children}</>;
};
