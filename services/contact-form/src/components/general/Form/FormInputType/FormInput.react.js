import React from 'react';
import { Form } from 'antd';
import { useIntl } from 'react-intl';
import { DefaultStyledInput } from 'components/general';
import { InputType } from './InputType';

export const FormInput = ({
  name,
  placeholder,
  disabled,
  suffix,
  style,
  autoFocus,
  dataCy,
  label,
  labelId,
  type = 'text',
  autoComplete = 'off',
  required,
}) => {
  const intl = useIntl();

  return (
    <Form.Item
      colon={false}
      label={label || intl.formatMessage({ id: labelId })}
      required={required}
    >
      <InputType
        component={DefaultStyledInput}
        autoComplete={autoComplete}
        name={name}
        placeholder={placeholder}
        disabled={disabled}
        suffix={suffix}
        style={style}
        autoFocus={autoFocus}
        data-cy={dataCy}
        type={type}
      />
    </Form.Item>
  );
};
