import React from 'react';
import { useFormikContext } from 'formik';
import { ErrorMessage } from 'components/general/Form';

export const InputType = ({
  component: Component,
  name,
  label,
  dataCy,
  ...properties
}) => {
  const {
    setFieldTouched,
    handleChange,
    errors,
    touched,
    values,
  } = useFormikContext();

  return (
    <>
      <Component
        name={name}
        id={name}
        onBlur={() => setFieldTouched(name)}
        onChange={handleChange(name)}
        value={values[name]}
        data-cy={dataCy}
        {...properties}
      >
        {label && <>{label}</>}
      </Component>

      <ErrorMessage
        error={errors[name]}
        visible={touched[name]}
        dataCy={`error-${name}`}
      />
    </>
  );
};
