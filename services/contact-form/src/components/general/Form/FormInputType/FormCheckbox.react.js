import React from 'react';
import { useFormikContext } from 'formik';
import { Checkbox } from 'antd';
import { InputType } from './InputType';

export const FormCheckbox = ({ name, label, dataCy }) => {
  const { values } = useFormikContext();

  return (
    <InputType
      component={Checkbox}
      name={name}
      checked={values[name]}
      label={label}
      dataCy={dataCy}
    />
  );
};
