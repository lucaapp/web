import React from 'react';
import { Form, Select } from 'antd';
import { useIntl } from 'react-intl';
import { useFormikContext } from 'formik';

export const FormSelect = ({
  name,
  label,
  labelId,
  options,
  StyledSelectComponent = Select,
  StyledSelectOptionComponent = Select.Option,
  dataCy,
}) => {
  const intl = useIntl();

  const { values, setFieldValue } = useFormikContext();

  const handleSelect = value => setFieldValue(name, value);

  return (
    <>
      <Form.Item
        colon={false}
        label={label || intl.formatMessage({ id: labelId })}
      >
        <StyledSelectComponent
          value={values[name]}
          name={name}
          onChange={handleSelect}
          data-cy={dataCy}
        >
          {options.map(option => {
            return (
              <StyledSelectOptionComponent
                value={option.value}
                key={option.key}
              >
                {option.label}
              </StyledSelectOptionComponent>
            );
          })}
        </StyledSelectComponent>
      </Form.Item>
    </>
  );
};
