import { FormInput } from './FormInput.react';
import { FormCheckbox } from './FormCheckbox.react';
import { FormSelect } from './FormSelect.react';

const FormInputType = {
  Checkbox: FormCheckbox,
  Input: FormInput,
  Select: FormSelect,
};

export { FormInputType };
