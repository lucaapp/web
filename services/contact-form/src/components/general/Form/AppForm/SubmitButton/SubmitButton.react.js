import React from 'react';
import { useFormikContext } from 'formik';
import { PrimaryButton } from 'components/general';
import { ButtonWrapper } from './SubmitButton.styled';

export const SubmitButton = ({
  title,
  dataCy,
  loading,
  disabledWhenInvalid = false,
}) => {
  const { handleSubmit, isValid, dirty } = useFormikContext();
  let validForm = true;

  if (disabledWhenInvalid) {
    validForm = isValid && dirty;
  }

  return (
    <ButtonWrapper>
      <PrimaryButton
        data-cy={dataCy}
        onClick={handleSubmit}
        loading={loading}
        disabled={!validForm}
      >
        {title}
      </PrimaryButton>
    </ButtonWrapper>
  );
};
