import React from 'react';
import { Formik } from 'formik';
import { Form } from 'antd';
import { WithFormErrors } from '../hoc';

export const AppForm = ({
  children,
  form,
  initialValues,
  onSubmit,
  validationSchema,
  enableReinitialize = false,
  dataCy,
  onValuesChange,
}) => {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
      enableReinitialize={enableReinitialize}
    >
      <WithFormErrors>
        <Form
          form={form}
          style={{ width: '100%' }}
          onValuesChange={onValuesChange}
          data-cy={dataCy}
          layout="vertical"
        >
          {() => <>{children}</>}
        </Form>
      </WithFormErrors>
    </Formik>
  );
};
