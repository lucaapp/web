import { useEffect } from 'react';
import { useFormikContext } from 'formik';

export const useFormErrors = () => {
  const { setFieldTouched, errors, touched } = useFormikContext();

  useEffect(() => {
    Object.keys(errors).forEach(fieldName => {
      if (Object.keys(touched).includes(fieldName)) {
        setFieldTouched(fieldName);
      }
    });
  }, [errors, touched, setFieldTouched]);
};
