import * as Yup from 'yup';
import validator from 'validator';
import { useIntl } from 'react-intl';
import { isValidCharacter } from 'utils/checkCharacter';
import { checkPhoneNumber } from 'utils/checkPhoneNumber';
import {
  MAX_CITY_LENGTH,
  MAX_HOUSE_NUMBER_LENGTH,
  MAX_NAME_LENGTH,
  MAX_POSTAL_CODE_LENGTH,
  MAX_STREET_LENGTH,
} from 'constants/valueLength';

Yup.addMethod(Yup.string, 'safeString', function handleError(errorMessage) {
  return this.test(
    'test-safe-string',
    errorMessage,
    value => value && isValidCharacter(value)
  );
});

Yup.addMethod(Yup.string, 'phoneNumber', function handleError(errorMessage) {
  return this.test(
    'test-phone-number',
    errorMessage,
    value => value && checkPhoneNumber(value)
  );
});

Yup.addMethod(Yup.string, 'zipCode', function handleError(errorMessage) {
  return this.test(
    'test-zip-code',
    errorMessage,
    value => value && validator.isPostalCode(value, 'any')
  );
});

export const useCheckinValidationSchema = () => {
  const intl = useIntl();

  const errorRequired = fieldName =>
    intl.formatMessage({
      id: `contactDataForm.isRequired.${fieldName}`,
    });
  const errorMaxLength = intl.formatMessage({
    id: 'contactDataForm.invalid.length',
  });
  const errorInvalid = fieldName =>
    intl.formatMessage({ id: `contactDataForm.invalid.${fieldName}` });

  const errorAgreement = intl.formatMessage({ id: 'error.agreement' });

  return Yup.object().shape({
    firstName: Yup.string()
      .strict(true)
      .trim()
      .required(errorRequired('firstName'))
      .max(MAX_NAME_LENGTH, errorMaxLength)
      .safeString(errorInvalid('firstName')),
    lastName: Yup.string()
      .strict(false)
      .trim()
      .required(errorRequired('lastName'))
      .max(MAX_NAME_LENGTH, errorMaxLength)
      .safeString(errorInvalid('lastName')),
    email: Yup.string().email(errorInvalid('email')),
    phone: Yup.string()
      .required(errorRequired('phone'))
      .phoneNumber(errorInvalid('phone')),
    street: Yup.string()
      .strict(false)
      .trim()
      .required(errorRequired('street'))
      .max(MAX_STREET_LENGTH, errorMaxLength)
      .safeString(errorInvalid('street')),
    number: Yup.string()
      .strict(false)
      .trim()
      .required(errorRequired('houseNo'))
      .max(MAX_HOUSE_NUMBER_LENGTH, errorMaxLength)
      .safeString(errorInvalid('houseNo')),
    zip: Yup.string()
      .strict(false)
      .trim()
      .required(errorRequired('zipCode'))
      .max(MAX_POSTAL_CODE_LENGTH, errorMaxLength)
      .zipCode(errorInvalid('zipCode')),
    city: Yup.string()
      .strict(false)
      .trim()
      .required(errorRequired('city'))
      .max(MAX_CITY_LENGTH, errorMaxLength)
      .safeString(errorInvalid('city')),
    acceptAGB: Yup.bool().oneOf([true], errorAgreement),
  });
};
