import React from 'react';

import { StyledAlert } from './ErrorMessage.styled';

export const ErrorMessage = ({ error, visible, dataCy }) => {
  if (!visible || !error) return null;

  return <StyledAlert message={error} type="error" showIcon data-cy={dataCy} />;
};
