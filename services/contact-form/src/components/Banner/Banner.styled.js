import styled from 'styled-components';

export const BannerBox = styled.div`
  background-color: #e8e7e5;
  color: black;
  border-radius: 4px;
  padding: 10px 20px;
  width: 90%;
`;
