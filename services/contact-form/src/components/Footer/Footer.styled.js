import styled from 'styled-components';

export const LinksWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-right: 20px;
  align-items: flex-end;
`;

export const LinkItemWrapper = styled.div`
  padding: 0 20px;
`;

export const StyledLink = styled.a`
  color: rgba(255, 255, 255, 0.87);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 12px;
  text-decoration: underline;
  text-decoration-color: rgba(255, 255, 255, 0.87);
  text-decoration-thickness: 0.5px;
  font-weight: 500;

  &:focus,
  &:active {
    color: rgba(255, 255, 255, 0.87);
    border-bottom: 0.5px solid rgba(255, 255, 255, 0.87);
  }
`;

export const VersionItem = styled.div`
  color: rgba(255, 255, 255, 0.87);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 12px;
  font-weight: 500;
`;

export const Description = styled.div`
  margin-bottom: 24px;
`;

export const Version = styled.span`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0px;
  line-height: 20px;
`;
