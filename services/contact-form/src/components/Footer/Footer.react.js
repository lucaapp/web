import React from 'react';
import { GITLAB_LINK } from 'constants/links';
import { LICENSES_ROUTE } from 'constants/routes';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';
import { getVersion } from 'network/static';
import { LinkItemWrapper, LinksWrapper, VersionItem } from './Footer.styled';
import { LinkItem } from './LinkItem';

export const Footer = () => {
  const intl = useIntl();

  const { isSuccess: loaded, data: info } = useQuery('version', getVersion, {
    refetchOnWindowFocus: false,
  });

  if (!loaded) return null;

  return (
    <LinksWrapper>
      <LinkItemWrapper>
        <VersionItem>
          {intl.formatMessage({ id: 'commitHashVersionDisplay' })} (
          {info.version})
        </VersionItem>
      </LinkItemWrapper>

      <LinkItem
        link={LICENSES_ROUTE}
        descriptionId="license.license"
        isUnderline
      />

      <LinkItem
        link={GITLAB_LINK}
        descriptionId="commitHashVersionDisplay.gitlab"
        isUnderline
      />
    </LinksWrapper>
  );
};
