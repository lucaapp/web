import React from 'react';
import { useIntl } from 'react-intl';
import { LinkItemWrapper, StyledLink } from 'components/Footer/Footer.styled';

export const LinkItem = ({ link, descriptionId }) => {
  const intl = useIntl();

  return (
    <LinkItemWrapper>
      <StyledLink href={link} target="_blank" rel="noopener noreferrer">
        {intl.formatMessage({
          id: descriptionId,
        })}
      </StyledLink>
    </LinkItemWrapper>
  );
};
