import React from 'react';
import { useIntl } from 'react-intl';
import {
  AdditionalDataDescription,
  StyledDivider,
} from './DividerSection.styled';

export const Divider = ({ locationTables, checkinData }) => {
  const intl = useIntl();

  return (
    (locationTables.length > 0 || checkinData?.additionalData.length !== 0) && (
      <>
        <StyledDivider />
        <AdditionalDataDescription data-cy="contactFormAdditionalDataDivider">
          {intl.formatMessage({
            id: 'contactDataForm.additionalData.divider',
          })}
        </AdditionalDataDescription>
      </>
    )
  );
};
