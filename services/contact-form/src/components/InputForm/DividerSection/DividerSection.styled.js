import styled from 'styled-components';
import { Divider } from 'antd';

export const StyledDivider = styled(Divider)`
  border: 1px solid #d8d8d8;
  margin: 32px 0 16px 0;
`;

export const AdditionalDataDescription = styled.div`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  margin-bottom: 16px;
`;
