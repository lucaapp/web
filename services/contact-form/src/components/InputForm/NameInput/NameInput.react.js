import React from 'react';
import { FormInputType } from 'components/general/Form';

export const NameInput = () => {
  return (
    <>
      <FormInputType.Input
        name="firstName"
        labelId="contactDataForm.firstName"
        autoFocus
        dataCy="firstName"
        required
      />
      <FormInputType.Input
        name="lastName"
        labelId="contactDataForm.lastName"
        dataCy="lastName"
        required
      />
    </>
  );
};
