import React from 'react';
import { FormInputType } from 'components/general/Form';

export const AdditionalData = ({ checkinData }) => {
  const { additionalData } = checkinData || {};
  if (!additionalData) return null;

  return additionalData
    .filter(entry => entry.key !== '')
    .map((entry, index) => (
      <FormInputType.Input
        name={`additionalData-${entry.uuid}`}
        dataCy={`additionalDataQuestionInputField-${index + 1}`}
        key={entry.uuid}
        label={entry.key}
      />
    ));
};
