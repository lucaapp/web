import React from 'react';
import { FormInputType } from 'components/general/Form';

export const ContactInput = () => {
  return (
    <>
      <FormInputType.Input
        name="phone"
        labelId="contactDataForm.phone"
        dataCy="phone"
        required
      />
      <FormInputType.Input
        name="email"
        labelId="contactDataForm.email"
        dataCy="email"
      />
    </>
  );
};
