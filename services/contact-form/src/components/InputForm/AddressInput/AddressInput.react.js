import React from 'react';

import { Col, Row, Grid } from 'antd';
import { FormInputType } from 'components/general/Form';

const { useBreakpoint } = Grid;

export const AddressInput = () => {
  const screens = useBreakpoint();

  return (
    <>
      <Row gutter={{ xs: 8, sm: 16, md: 24 }} wrap>
        <Col span={!screens.md ? 24 : 18}>
          <FormInputType.Input
            name="street"
            labelId="contactDataForm.street"
            dataCy="street"
            required
          />
        </Col>
        <Col span={!screens.md ? 24 : 6}>
          <FormInputType.Input
            name="number"
            labelId="contactDataForm.number"
            dataCy="number"
            required
          />
        </Col>
      </Row>

      <Row gutter={{ xs: 8, sm: 16, md: 24 }} wrap>
        <Col span={!screens.md ? 24 : 6}>
          <FormInputType.Input
            name="zip"
            labelId="contactDataForm.zip"
            dataCy="zip"
            required
          />
        </Col>
        <Col span={!screens.md ? 24 : 18}>
          <FormInputType.Input
            name="city"
            labelId="contactDataForm.city"
            dataCy="city"
            required
          />
        </Col>
      </Row>
    </>
  );
};
