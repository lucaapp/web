import React from 'react';
import { EC_KEYPAIR_GENERATE, GET_RANDOM_BYTES } from '@lucaapp/crypto';
import moment from 'moment';
import { notification } from 'antd';
import { addCheckinData, createCheckinV3, createUser } from 'network/api';
import {
  getAdditionalDataPayload,
  getTraceId,
  getUserIdPayload,
  getV3CheckinPayload,
} from 'components/hooks/useRegister.helper';
import { StyledLink } from 'components/general';

const getAdditionalDataFromValues = values => {
  const additionalData = {};
  Object.keys(values)
    .filter(value => value.startsWith('additionalData-'))
    .forEach(key => {
      additionalData[key.replace('additionalData-', '')] = values[key];
    });
  return additionalData;
};

export async function checkinContactForm(
  values,
  dailyKey,
  scanner,
  formId,
  intl,
  resetForm
) {
  const handleCheckInError = () =>
    notification.error({
      message: intl.formatMessage({
        id: 'notification.checkIn.error',
      }),
    });

  const userDataSecret = GET_RANDOM_BYTES(16);
  const userTracingSecret = GET_RANDOM_BYTES(16);
  const userKeypair = EC_KEYPAIR_GENERATE();

  const createUserIdPayload = getUserIdPayload(
    values,
    userDataSecret,
    userKeypair
  );

  try {
    const { userId } = await createUser(createUserIdPayload);

    const timestamp = moment().seconds(0).unix();
    const traceId = getTraceId(userId, userTracingSecret, timestamp);
    const v3checkinPayload = getV3CheckinPayload(
      userId,
      dailyKey,
      userDataSecret,
      scanner,
      timestamp,
      traceId
    );

    await createCheckinV3(formId, v3checkinPayload)
      .then(response => {
        if (response.status !== 201) {
          handleCheckInError();
          return;
        }
        notification.success({
          message: intl.formatMessage({
            id: 'notification.checkIn.success',
          }),
        });
        resetForm({ values: '' });
      })
      .catch(() => handleCheckInError());

    // ADD ADDITIONAL INFOS
    const additionalData = getAdditionalDataFromValues(values);
    if (values.table) {
      additionalData.table = values.table;
    }

    if (Object.keys(additionalData).length > 0) {
      const additionalDataPayload = getAdditionalDataPayload(
        scanner,
        traceId,
        additionalData
      );

      addCheckinData(additionalDataPayload);
    }
  } catch {
    handleCheckInError();
  }
}

export const getConsentBoxLabel = (intl, intlId, link) =>
  intl.formatMessage(
    {
      id: intlId,
    },
    {
      // eslint-disable-next-line react/display-name
      a: (...chunks) => (
        <StyledLink href={link} target="_blank" rel="noopener noreferrer">
          {chunks}
        </StyledLink>
      ),
    }
  );
