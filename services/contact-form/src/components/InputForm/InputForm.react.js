import React from 'react';
import { useIntl } from 'react-intl';

import {
  useAdditionalData,
  useGetLocationTables,
} from 'components/hooks/queries';
import { FormInputType, AppForm, SubmitButton } from 'components/general/Form';

import { useCheckinValidationSchema } from 'components/general/Form/hooks';
import { PRIVACY_LINK } from 'constants/links';
import { Divider } from './DividerSection';
import { checkinContactForm, getConsentBoxLabel } from './InputForm.helper';
import { AddressInput } from './AddressInput';
import { NameInput } from './NameInput';
import { ContactInput } from './ContactInput';
import { AdditionalData } from './AdditionalData';
import { DefaultStyledSelect } from '../general';

export const InputForm = ({ formId, scanner, dailyKey }) => {
  const intl = useIntl();
  const checkinSchema = useCheckinValidationSchema();

  const {
    isLoading: locationTablesLoading,
    error: locationTablesError,
    data: locationTables,
  } = useGetLocationTables(scanner.locationId);

  const {
    isLoading: additionalDataLoading,
    error: additionalDataError,
    data: checkinData,
  } = useAdditionalData(scanner.locationId);

  const handleSubmit = async (values, { resetForm }) => {
    await checkinContactForm(
      values,
      dailyKey,
      scanner,
      formId,
      intl,
      resetForm
    );
  };

  const privacyLabel = getConsentBoxLabel(
    intl,
    'registration.acceptPrivacy',
    PRIVACY_LINK
  );

  if (
    locationTablesLoading ||
    locationTablesError ||
    additionalDataLoading ||
    additionalDataError
  )
    return null;

  const selectOptions = locationTables?.map(table => ({
    key: table.id,
    value: table.customName || table.internalNumber,
    label: table.customName || table.internalNumber,
  }));

  const initialValues = {
    firstName: '',
    lastName: '',
    street: '',
    number: '',
    zip: '',
    city: '',
    email: '',
    phone: '',
    tableNumber: selectOptions[0]?.value,
    acceptAGB: false,
  };

  return (
    <AppForm
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={checkinSchema}
    >
      <NameInput />
      <AddressInput />
      <ContactInput />
      <Divider locationTables={locationTables} checkinData={checkinData} />
      {locationTables.length > 0 && (
        <FormInputType.Select
          name="tableNumber"
          labelId="contactDataForm.table"
          options={selectOptions}
          StyledSelectComponent={DefaultStyledSelect}
          dataCy="additionalDataTableNumberSelector"
        />
      )}

      <AdditionalData checkinData={checkinData} />

      <FormInputType.Checkbox
        name="acceptAGB"
        label={privacyLabel}
        dataCy="acceptAGB"
      />

      <SubmitButton
        title={intl.formatMessage({
          id: 'contactDataForm.button',
        })}
        dataCy="contactSubmit"
        disabledWhenInvalid
      />
    </AppForm>
  );
};
