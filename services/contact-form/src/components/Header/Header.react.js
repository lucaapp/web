import React from 'react';

// Assets
import { LucaLogoWhiteIconSVG } from 'assets/icons';

// Components
import { useIntl } from 'react-intl';
import {
  Wrapper,
  Logo,
  HeaderWrapper,
  StyledHeader,
  StyledSecondaryButton,
} from './Header.styled';

export const Header = ({ location }) => {
  const intl = useIntl();

  const backToLocations = () =>
    window.open(
      `${window.location.origin}/app/group/${location.groupId}/location/${location.uuid}`
    );

  return (
    <Wrapper>
      <HeaderWrapper>
        <Logo src={LucaLogoWhiteIconSVG} />
        <StyledHeader>
          {intl.formatMessage({ id: 'contactFormCard.header.contactForm' })}
        </StyledHeader>
      </HeaderWrapper>

      <StyledSecondaryButton onClick={backToLocations}>
        {intl.formatMessage({ id: 'contactFormCard.header.backToLocations' })}
      </StyledSecondaryButton>
    </Wrapper>
  );
};
