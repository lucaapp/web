import styled from 'styled-components';
import { SecondaryButton } from 'components/general';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  padding: 40px;
`;

export const StyledHeader = styled.div`
  color: rgb(255, 255, 255);
  font-size: 16px;
  font-family: Montserrat-SemiBold, sans-serif;
  font-weight: 600;
  margin: 30px 0 0 16px;
`;

export const Logo = styled.img`
  height: 48px;
`;

export const StyledSecondaryButton = styled(SecondaryButton)`
  border-color: white;
  color: white;
  margin: 6px 40px 0 0;
`;
