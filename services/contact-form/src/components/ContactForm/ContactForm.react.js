import React from 'react';
import { useIntl } from 'react-intl';
import { Helmet } from 'react-helmet-async';

import { InputForm } from 'components/InputForm';

import { Header } from 'components/Header';
import { Banner } from 'components/Banner';
import { useDailyKey } from 'components/hooks/queries';
import {
  RegistrationWrapper,
  RegistrationCard,
  LocationName,
  Description,
} from './ContactForm.styled';

export const ContactForm = ({ scanner, formId, location }) => {
  const intl = useIntl();

  const { isLoading, error, data: dailyKey } = useDailyKey();

  if (isLoading || error) return null;

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'contactForm.site.title' })}</title>
        <meta
          name="description"
          content={intl.formatMessage({ id: 'contactForm.site.meta' })}
        />
      </Helmet>

      <Header location={location} />

      <RegistrationWrapper>
        {error || !dailyKey ? (
          <Banner
            title={intl.formatMessage({ id: 'contactForm.banner.noDailyKey' })}
          />
        ) : (
          <RegistrationCard data-cy="contactFormCard">
            <LocationName>{scanner.name}</LocationName>
            <Description>
              {intl.formatMessage({
                id: 'contactFormCard.inputForm.description',
              })}
            </Description>
            <InputForm scanner={scanner} formId={formId} dailyKey={dailyKey} />
          </RegistrationCard>
        )}
      </RegistrationWrapper>
    </>
  );
};
