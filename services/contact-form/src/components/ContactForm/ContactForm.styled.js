import styled from 'styled-components';

export const RegistrationWrapper = styled.div`
  display: flex;
  max-width: 100vw;
  overflow-y: auto;
  overflow-x: hidden;
  padding: 0 40px 40px;
  word-break: break-word;
  box-sizing: border-box;
  align-items: center;
  justify-content: center;
`;

export const RegistrationCard = styled.div`
  overflow-x: hidden;
  border-radius: 4px;
  padding: 24px 32px;
  background-color: rgb(243, 245, 247);
  min-height: -o-calc(100vh - 240px); /* opera */
  min-height: -webkit-calc(100vh - 240px); /* google, safari */
  min-height: -moz-calc(100vh - 240px); /* firefox */
  width: 60%;
  overflow-y: auto;

  @media (max-width: 767px) {
    padding: 16px 32px;
    min-width: 414px;
    overflow-y: auto;
  }
`;

export const LocationName = styled.h1`
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  color: rgb(0, 0, 0);
  font-size: 20px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-size: 12px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  margin: 24px 0;
`;

export const StoreWrapper = styled.div`
  display: flex;
  margin-bottom: 15px;
`;

export const StoreLogoWrapper = styled.a`
  flex: 1;
  display: flex;
  align-content: center;
  justify-content: center;
`;
