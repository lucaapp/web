// SVGs
import ErrorIconSVG from './ErrorIcon.svg';
import LucaLogoWhiteIconSVG from './LucaLogoWhiteIcon.svg';

export { ErrorIconSVG, LucaLogoWhiteIconSVG };
