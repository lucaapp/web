import moment from 'moment';
import { Config } from './schema';

const config: Config = {
  debug: true,
  loglevel: 'info',
  defaultHttpLogLevel: 'trace',
  allowRateLimitBypass: true,
  allowMocking: true,
  shutdownDelay: 0,
  port: 8080,
  healthCheckPort: 8040,
  version: {
    commit: 'dev',
    version: 'dev',
  },
  db: {
    host: 'database',
    host_read1: 'database',
    host_read2: 'database',
    host_read3: 'database',
    username: 'luca',
    password: 'lcadmin',
    database: 'luca-backend',
  },
  redis: {
    hostname: 'redis',
    database: 0,
    password:
      'ConqsCqWd]eaR82wv%C.iDdRybor8Ms2bM*h=m?V3@x2w^UxKA9pEjMjHn^y7?78', // DEV ONLY TOKEN
  },
  queue: {
    limiter: {
      size: moment.duration(10, 'minutes').asMilliseconds(),
      max: Math.floor(1000 / 60), //
      duration: moment.duration(1, 'minute').asMilliseconds(),
    },
  },
  proxy: {
    http: null,
    https: null,
  },
  jwt: {
    remoteUrl: 'http://backend-attestation:8080/attestation/api/v1/jwks',
  },
  attestationServiceURL: 'http://backend-attestation:8080/attestation/api',
  allowAssertionVerificationsBypass: true,
  internalAccessToken: '',
  idnow: {
    apiKey: '',
    customer: '',
    gatewayUrl: '',
    ssiVcEmitterServiceUrl: '',
    pendingIdentAgeBeforeManualPull: 60000,
  },
  webhook: {
    basicAuth: {
      enabled: false,
      username: '',
      password: '',
    },
  },
};

// We cannot `export default` until this is fixed: https://github.com/node-config/node-config/issues/521
module.exports = config;
