/* eslint-disable import/no-import-module-exports */
import type { EnvironmentMapping } from 'config';
import { Config } from './schema';

const config: EnvironmentMapping<Config> = {
  port: {
    __name: 'PORT',
    __format: 'number',
  },
  loglevel: 'LOGLEVEL',
  debug: {
    __name: 'DEBUG',
    __format: 'boolean',
  },
  allowRateLimitBypass: {
    __name: 'ALLOW_RATE_LIMIT_BYPASS',
    __format: 'boolean',
  },
  allowMocking: { __name: 'ALLOW_MOCKING', __format: 'boolean' },
  version: {
    commit: 'GIT_COMMIT',
    version: 'GIT_VERSION',
  },
  db: {
    host: 'DB_HOSTNAME',
    host_read1: 'DB_HOSTNAME_READ1',
    host_read2: 'DB_HOSTNAME_READ2',
    host_read3: 'DB_HOSTNAME_READ3',
    username: 'DB_USERNAME',
    password: 'DB_PASSWORD',
    database: 'DB_DATABASE',
  },
  redis: {
    hostname: 'REDIS_HOSTNAME',
    password: 'REDIS_PASSWORD',
    database: {
      __name: 'REDIS_DATABASE',
      __format: 'number',
    },
  },
  proxy: {
    http: 'http_proxy',
    https: 'http_proxy',
  },
  attestationServiceURL: 'ATTESTATION_SERVICE_URL',
  internalAccessToken: 'INTERNAL_ACCESS_TOKEN',
  allowAssertionVerificationsBypass: {
    __name: 'ALLOW_ASSERTION_VERIFICATION_BYPASS',
    __format: 'boolean',
  },
  idnow: {
    apiKey: 'IDNOW_APIKEY',
    customer: 'IDNOW_CUSTOMER',
    gatewayUrl: 'IDNOW_GATEWAYURL',
    ssiVcEmitterServiceUrl: 'IDNOW_SSI_VC_EMITTER_SERVICE_URL',
    pendingIdentAgeBeforeManualPull: {
      __name: 'IDNOW_PENDING_IDENT_AGE_BEFORE_MANUAL_PULL',
      __format: 'number',
    },
  },
  queue: {
    limiter: {
      max: {
        __name: 'QUEUE_LIMITER_MAX',
        __format: 'number',
      },
      duration: {
        __name: 'QUEUE_LIMITER_DURATION',
        __format: 'number',
      },
      size: {
        __name: 'QUEUE_LIMITER_SIZE',
        __format: 'number',
      },
    },
  },
  webhook: {
    basicAuth: {
      enabled: { __name: 'WEBHOOK_BASIC_AUTH_ENABLED', __format: 'boolean' },
      username: 'WEBHOOK_BASIC_AUTH_USERNAME',
      password: 'WEBHOOK_BASIC_AUTH_PASSWORD',
    },
  },
};

module.exports = config;
