import type { PartialConfig } from './schema';

const config: PartialConfig = {
  loglevel: 'silent',
};

// We cannot `export default` until this is fixed: https://github.com/node-config/node-config/issues/521
module.exports = config;
