import type { PartialConfig } from './schema';

const config: PartialConfig = {
  debug: false,
  loglevel: 'info',
  defaultHttpLogLevel: 'info',
  allowRateLimitBypass: false,
  allowMocking: false,
  shutdownDelay: 15,
  webhook: {
    basicAuth: {
      enabled: true,
    },
  },
  allowAssertionVerificationsBypass: false,
  idnow: {
    pendingIdentAgeBeforeManualPull: 15 * 60000,
  },
};

// We cannot `export default` until this is fixed: https://github.com/node-config/node-config/issues/521
module.exports = config;
