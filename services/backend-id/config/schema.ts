import { z } from 'utils/validation';
import type { infer as ZodInfer } from 'zod';

const pinoLogLevelSchema = z.union([
  z.literal('debug'),
  z.literal('error'),
  z.literal('info'),
  z.literal('trace'),
  z.literal('fatal'),
  z.literal('warn'),
]);

const configSchema = z.object({
  debug: z.boolean(),
  loglevel: z.union([z.literal('silent'), pinoLogLevelSchema]),
  defaultHttpLogLevel: pinoLogLevelSchema,
  allowRateLimitBypass: z.boolean(),
  allowMocking: z.boolean(),
  shutdownDelay: z.number().int().min(0),
  port: z.number().min(0).max(65536).int(),
  healthCheckPort: z.number(),
  version: z.object({
    commit: z.string(),
    version: z.string(),
  }),
  db: z.object({
    host: z.string(),
    host_read1: z.string(),
    host_read2: z.string(),
    host_read3: z.string(),
    username: z.string(),
    password: z.string(),
    database: z.string(),
  }),
  redis: z.object({
    hostname: z.string(),
    database: z.number(),
    password: z.string(),
  }),
  queue: z.object({
    limiter: z.object({
      // Maximum acceptable wait time in ms for a new job at which we immediately reject it
      size: z.number().int().min(0),
      // Rate-limiting options for BullMQ (see see https://docs.bullmq.io/guide/rate-limiting)
      max: z.number().int().min(0),
      duration: z.number().int().min(0), // Time interval in ms in which max is applied
    }),
  }),
  proxy: z.object({
    http: z.string().nullable(),
    https: z.string().nullable(),
  }),
  jwt: z.object({
    remoteUrl: z.string(),
  }),
  attestationServiceURL: z.string(),
  allowAssertionVerificationsBypass: z.boolean(),
  internalAccessToken: z.string(),
  idnow: z.object({
    apiKey: z.string(),
    customer: z.string(),
    gatewayUrl: z.string(),
    ssiVcEmitterServiceUrl: z.string(),
    pendingIdentAgeBeforeManualPull: z.number().int(),
  }),
  webhook: z.object({
    basicAuth: z.object({
      enabled: z.boolean(),
      username: z.string(),
      password: z.string(),
    }),
  }),
});

export default configSchema;

type DeepPartial<T> = T extends Record<string, unknown>
  ? {
      [P in keyof T]?: DeepPartial<T[P]>;
    }
  : T;
export type Config = ZodInfer<typeof configSchema>;
export type PartialConfig = DeepPartial<Config>;
