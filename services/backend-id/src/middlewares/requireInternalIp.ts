import type { RequestHandler } from 'express';
import { forbidden } from '@hapi/boom';
import { isInternalIp } from 'utils/ipChecks';

export const requireInternalIp: RequestHandler = (request, response, next) => {
  if (!isInternalIp(request.ip)) {
    throw forbidden('Request did not come from an internal IP address.');
  }
  return next();
};
