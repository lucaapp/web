import type { RequestHandler, Request } from 'express';
import config from 'config';
import moment from 'moment';
import * as boom from '@hapi/boom';
import { RateLimiterRedis, IRateLimiterOptions } from 'rate-limiter-flexible';
import { sha256 } from 'utils/hash';
import { client } from 'utils/redis';
import { isInternalIp, isRateLimitExemptIp } from 'utils/ipChecks';

export type KeyGenerator = (request: Request) => string;

export const KEY_GENERATORS: Record<string, KeyGenerator> = {
  ROUTE: (request: Request): string => {
    const {
      baseUrl,
      route: { path },
      method,
    } = request;
    return `${method}:${baseUrl}${path}`;
  },
  IP: (request: Request): string => {
    const { ip } = request;
    return sha256(ip);
  },
  DEVICE_ID: (request: Request): string => {
    const { user } = request;
    return (user as Express.User).deviceId;
  },
};

const createKeyGenerator = (generators: Array<KeyGenerator>) => {
  return (request: Request): string => {
    return generators.map(generator => generator(request)).join(':');
  };
};

const shouldSkipRateLimit = async (request: Request): Promise<boolean> => {
  const { headers, ip } = request;
  const xRateLimitBypassHeader = !!(headers && headers['x-rate-limit-bypass']);
  const allowRateLimitBypass = config.get('allowRateLimitBypass');
  return (
    (allowRateLimitBypass && xRateLimitBypassHeader) ||
    isInternalIp(ip) ||
    isRateLimitExemptIp(ip)
  );
};

export const rateLimitRoute = (
  options: IRateLimiterOptions,
  keyGenerators: Array<KeyGenerator> = [KEY_GENERATORS.ROUTE, KEY_GENERATORS.IP]
): RequestHandler => {
  const rl = new RateLimiterRedis({
    storeClient: client,
    keyPrefix: `rl`,
    points: options.points,
    duration: moment.duration(1, 'hour').asSeconds(),
    inmemoryBlockOnConsumed: options.points,
    inmemoryBlockDuration: options.blockDuration,
    ...options,
  });

  const generateKey = createKeyGenerator(keyGenerators);

  return async (request, response, next) => {
    if (await shouldSkipRateLimit(request)) {
      return next();
    }
    try {
      await rl.consume(generateKey(request));
    } catch {
      throw boom.tooManyRequests();
    }

    return next();
  };
};
