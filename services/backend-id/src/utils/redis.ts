import config from 'config';
import redis from 'redis';
import { promisify } from 'util';
import logger from './logger';
import { registerShutdownHandler } from './lifecycle';

const RETRY_INTERVAL_MS = 500;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const retryStrategy = (info: any) => {
  logger.warn(info, `retrying redis connection`);
  return RETRY_INTERVAL_MS;
};

export const client = redis.createClient({
  host: config.get('redis.hostname'),
  password: config.get('redis.password'),
  enable_offline_queue: true,
  detect_buffers: true,
  db: config.get('redis.database'),
  retry_strategy: retryStrategy,
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
client.on('error', (error: any) => {
  logger.error(error);
});

const promiseClient = {
  quit: promisify(client.quit).bind(client),
  set: promisify(client.set).bind(client),
  get: promisify(client.get).bind(client),
};

registerShutdownHandler(async () => {
  logger.info('closing redis connection');
  await promiseClient.quit();
  logger.info('redis connection closed');
});

export default promiseClient;
