/* eslint-disable import/no-mutable-exports */
import config from 'config';
import moment from 'moment';
import { v4 as uuid } from 'uuid';
import defaultLogger from 'utils/logger';
import { registerShutdownHandler } from 'utils/lifecycle';
import { generateMockedReceiptJWS, generateMockVC } from 'utils/idnowMock';
import Bull, { Job } from 'bull';

import { Ident } from 'database';
import { IdentState, IdentFailureReason } from 'constants/ident';
import { createIdent } from 'utils/idnow';
import { decodeJwt } from 'jose';
import { z, ZodError } from 'zod';
import { AxiosError } from 'axios';
import { Logger } from 'pino';
import { idnowReceiptJWSPayloadSchema } from 'utils/idnow.schemas';
import { IdentInstance } from '../database/models/ident';

const QUEUE_NAME = 'backend-id';
const QUEUE_LIMITER_MAX = config.get('queue.limiter.max');
const QUEUE_LIMITER_DURATION = config.get('queue.limiter.duration');

const redis = {
  host: config.get('redis.hostname'),
  db: config.get('redis.database'),
  password: config.get('redis.password'),
  enableOfflineQueue: false,
};

const QUEUE_OPTIONS = {
  redis,
  limiter: {
    max: QUEUE_LIMITER_MAX,
    duration: QUEUE_LIMITER_DURATION,
  },
  defaultJobOptions: {
    removeOnComplete: true,
    attempts: 5,
    backoff: {
      type: 'exponential',
      delay: 1000,
    },
  },
};

interface JobPayload {
  deviceId: string;
  idPublicKey: string;
  encPublicKey: string;
  enableMock: boolean;
}

const createIdentAndDecodeReceipt = async (
  ident: IdentInstance,
  job: Job<JobPayload>,
  logger: Logger
): Promise<{
  transactionNumber: string;
  receiptJWS: string;
  decodedReceiptJWS: z.infer<typeof idnowReceiptJWSPayloadSchema>;
}> => {
  const { idPublicKey, encPublicKey } = job.data;
  const transactionNumber = uuid();
  try {
    logger.info(
      `Creating ident at IDnow API (transaction number is ${transactionNumber})`
    );

    const receiptJWS = await createIdent({
      transactionNumber,
      bindingKey: idPublicKey,
      encryptionKey: encPublicKey,
    });
    if (!receiptJWS) {
      throw new Error('received no IdentId.');
    }
    return {
      receiptJWS,
      decodedReceiptJWS: idnowReceiptJWSPayloadSchema.parse(
        decodeJwt(receiptJWS)
      ),
      transactionNumber,
    };
  } catch (error) {
    if (error instanceof ZodError) {
      logger.error(error, 'Parsing of decoded IDnow JWS receipt failed');
    } else if (error instanceof Error) {
      logger.error(
        error,
        'isAxiosError' in (error as Error | AxiosError)
          ? 'IDnow API failed.'
          : 'Unexpected error'
      );
    } else {
      logger.error({ error }, 'Unexpected error');
    }

    await ident.update({
      transactionNumber,
      state: IdentState.FAILED,
      reason: IdentFailureReason.IDNOW_API_ERROR,
    });
    throw error;
  }
};

const work: Bull.ProcessCallbackFunction<JobPayload> = async job => {
  const jobLogger = defaultLogger.child({ job });

  jobLogger.info('Job started');

  const { deviceId, enableMock, idPublicKey, encPublicKey } = job.data;
  try {
    const ident = await Ident.findOne({
      where: { deviceId, state: IdentState.QUEUED },
    });

    if (!ident) {
      throw new Error('Ident not found.');
    }

    ident.state = IdentState.PENDING;

    if (enableMock) {
      const transactionNumber = uuid();
      jobLogger.debug(
        `Generating mocked receipt JWS with transaction number ${transactionNumber}`
      );
      const { receiptJWS, decodedPayload } = await generateMockedReceiptJWS({
        identId: 'TST-MOCKED',
        transactionNumber,
        idPublicKey,
        encPublicKey,
      });

      ident.identId = decodedPayload.autoIdentId;
      ident.receiptJWS = receiptJWS;
    } else {
      const {
        transactionNumber,
        receiptJWS,
        decodedReceiptJWS,
      } = await createIdentAndDecodeReceipt(ident, job, jobLogger);

      ident.transactionNumber = transactionNumber;
      ident.identId = decodedReceiptJWS.autoIdentId;
      ident.receiptJWS = receiptJWS;
    }

    await ident.save();

    if (enableMock) {
      const mockedVCGenerationWait = moment
        .duration(1, 'minutes')
        .asMilliseconds();
      jobLogger.debug(
        `Scheduling generation of mocked VC in ${mockedVCGenerationWait}ms`
      );
      setTimeout(async () => {
        try {
          jobLogger.debug(`Generating mocked VC`);
          const vc = await generateMockVC(encPublicKey);
          await ident.update({
            state: IdentState.SUCCESS,
            data: vc,
          });
        } catch {
          await ident.update({
            state: IdentState.FAILED,
            reason: IdentFailureReason.MOCK_JWE_ERROR,
          });
        }
      }, mockedVCGenerationWait);
    }
  } catch (error) {
    jobLogger.error(
      { err: error },
      enableMock ? 'IDnow ident creation failed' : 'Mocked ident flow failed'
    );

    throw error;
  }
};

export let queue: Bull.Queue<JobPayload>;
export const startQueue = (): void => {
  defaultLogger.info('starting queue');
  queue = new Bull(QUEUE_NAME, QUEUE_OPTIONS);
  queue.process(work);
  queue.on('failed', (job, error) => {
    defaultLogger.warn({ job, error }, 'Job failed');
  });
  queue.on('completed', (job, result) => {
    defaultLogger.info({ job, result }, 'Job completed');
  });
  queue.on('stalled', job => {
    defaultLogger.warn({ job }, 'Job stalled');
  });

  registerShutdownHandler(async () => {
    defaultLogger.info('stopping queue');
    await queue.close();
    defaultLogger.info('queue stopped');
  });
};

export const estimateQueueSize = async (): Promise<number> => {
  const queueSize = await queue.count();
  return (queueSize / QUEUE_LIMITER_MAX) * QUEUE_LIMITER_DURATION;
};
