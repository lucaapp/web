import { z } from './validation';

const schemaUUID = z.uuid();

describe('validation', function () {
  describe('object', function () {
    it('strips unknown keys', async function () {
      const schema = z.object({ test: z.string() });
      const inputObject = { test: 'test', unknown: 'test' };
      const expectedOutput = { test: 'test' };
      expect(schema.parse(inputObject)).toStrictEqual(expectedOutput);
    });
  });

  describe('uuid', function () {
    it('should accept v4 uuid', async function () {
      expect(() =>
        schemaUUID.parse('ebca725f-6c9c-4870-bcff-1fcb852a56b3')
      ).not.toThrow();
    });
    it('should not accept non-v4 uuid', async function () {
      expect(() =>
        schemaUUID.parse('ffa3bb2b-fd75-ffff-ffff-facab27ddcea')
      ).toThrow(z.ZodError);
    });
    it('should reject invalid uuids', async function () {
      expect(() =>
        schemaUUID.parse('ffa3bb2b-fd75-ffff-ffff-facab27ddcea/')
      ).toThrow(z.ZodError);
      expect(() =>
        schemaUUID.parse('ffa3bb2bfd75fffffffffacab27ddcea')
      ).toThrow(z.ZodError);
    });
  });

  describe('base64', function () {
    it('should accept valid base64', async function () {
      const schema = z.base64({ rawLength: 4 });
      expect(() => schema.parse('dGVzdA==')).not.toThrow();
    });

    it('should reject invalid padding', async function () {
      const schema = z.base64();
      expect(() => schema.parse('dGVzdA')).toThrow(z.ZodError);
    });

    it('should reject non-base64', async function () {
      const schema = z.base64();
      expect(() => schema.parse('caffee')).toThrow(z.ZodError);
    });
    it('should reject too small strings', async function () {
      const schema = z.base64({ min: 100 });
      expect(() => schema.parse('dGVzdA==')).toThrow(z.ZodError);
    });
    it('should reject too long strings', async function () {
      const schema = z.base64({ max: 3 });
      expect(() => schema.parse('dGVzdA==')).toThrow(z.ZodError);
    });

    it('should reject invalid raw lengths', async function () {
      const schema = z.base64({ rawLength: 5 });
      expect(() => schema.parse('dGVzdA==')).toThrow(z.ZodError);
    });
  });
});
