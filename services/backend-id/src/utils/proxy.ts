import axios from 'axios';

import config from 'config';
import HttpsProxyAgent from 'https-proxy-agent';
import { createLoggingInterceptors } from 'utils/axios';

const httpsProxy = config.get('proxy.https');

export const proxyAgent = httpsProxy
  ? // @ts-ignore HttpsProxyAgent is missing constructor type
    new HttpsProxyAgent(httpsProxy)
  : undefined;

export const proxyClient = axios.create({
  httpsAgent: proxyAgent,
  proxy: false,
});

const loggingInterceptors = createLoggingInterceptors('Proxy Client');
proxyClient.interceptors.request.use(...loggingInterceptors.request);
proxyClient.interceptors.response.use(...loggingInterceptors.response);
