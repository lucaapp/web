import crypto from 'crypto';

const UNAMBIGIOUS_CHARSET = 'CDFGHJKLMNPQRTVWXY34679'.split('');

export const generateRevocationCode = (): string => {
  const code = [];
  for (let block = 0; block < 3; block += 1) {
    let charBlock = '';
    for (let count = 0; count < 4; count += 1) {
      charBlock +=
        UNAMBIGIOUS_CHARSET[crypto.randomInt(UNAMBIGIOUS_CHARSET.length)];
    }
    code.push(charBlock);
  }
  return code.join('-');
};
