import { boomify } from '@hapi/boom';
import axios, {
  AxiosError,
  AxiosInterceptorManager,
  AxiosRequestConfig,
  AxiosResponse,
} from 'axios';
import logger from 'utils/logger';

export const axiosClient = axios.create({
  proxy: false,
});

const serializeRequest = ({
  headers,
  method,
  url,
}: AxiosRequestConfig): unknown => ({ method, url, headers });

export const createLoggingInterceptors = (
  name: string
): {
  request: Parameters<AxiosInterceptorManager<AxiosRequestConfig>['use']>;
  response: Parameters<AxiosInterceptorManager<AxiosResponse>['use']>;
} => ({
  request: [
    request => {
      logger.info(
        { request: serializeRequest(request) },
        `[${name}] -> ${request.method?.toUpperCase()} ${request.url}`
      );
      return request;
    },
    rawError => {
      const error = boomify<unknown, Partial<AxiosError>>(rawError);
      logger.error(
        error,
        `[${name}] Request error: ${error.message}
        }`
      );

      return Promise.reject(rawError);
    },
  ],
  response: [
    response => {
      const { status, headers, config: request, data } = response;

      logger.info(
        {
          response: {
            status,
            headers,
            body: data,
          },
          request: serializeRequest(request),
        },
        `[${name}] <- ${status} ${request.method?.toUpperCase()} ${request.url}`
      );

      return response;
    },
    rawError => {
      const error = boomify<unknown, Partial<AxiosError>>(rawError);

      if (error.response) {
        logger.error(
          { err: error },
          `[${name}] <- ${
            error.response.status
          } ${error.config?.method?.toUpperCase()} ${error.config?.url}`
        );
      } else {
        logger.error(
          { err: error },
          `[${name}] Response error: ${error.message}
        }`
        );
      }

      return Promise.reject(rawError);
    },
  ],
});

const loggingInterceptors = createLoggingInterceptors('Default Client');
axiosClient.interceptors.request.use(...loggingInterceptors.request);
axiosClient.interceptors.response.use(...loggingInterceptors.response);
