/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { z as zod } from 'zod';
import validator from 'validator';

export const z = {
  ...zod,
  uuid: () =>
    zod
      .string()
      .length(36)
      .refine(value => validator.isUUID(value, 4)),
  unixTimestamp: () => zod.number().int().positive(),
  base64: ({
    min,
    max,
    length,
    rawLength,
  }: {
    min?: number;
    max?: number;
    length?: number;
    rawLength?: number;
  } = {}) =>
    zod
      .string()
      .min(min as number)
      .max(max as number)
      .length(length as number)
      .refine(value => {
        if (!validator.isBase64(value)) return false;
        if (!rawLength) return true;

        return Buffer.from(value, 'base64').length === rawLength;
      }),
  uncompressedECPublicKey: () => z.base64({ length: 88, rawLength: 65 }),
  iso8601Date: () => zod.string().refine(value => validator.isISO8601(value)),
};
