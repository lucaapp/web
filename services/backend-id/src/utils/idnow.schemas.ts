import config from 'config';
import { IDResult } from 'constants/ident';
import { infer as ZodInfer } from 'zod';
import { z } from './validation';

export const IDNOW_CUSTOMER = config.get('idnow.customer');

export const IDNOW_IDENT_ID_REGEXP = /^[A-Z]{3}-[A-Z]{5}$/;
export const idnowIdentIdSchema = z.string().regex(IDNOW_IDENT_ID_REGEXP);

export const idnowReceiptJWSPayloadSchema = z.object({
  input_data: z.object({
    bindingKey: z.uncompressedECPublicKey(),
    encryptionKey: z.uncompressedECPublicKey(),
  }),
  autoIdentId: idnowIdentIdSchema,
  transactionNumber: z.uuid(),
});

export const idnowIdentificationProcessTypeSchema = z.union([
  z.literal('APP'),
  z.literal('WEB'),
]);

// Note that while `result` types (SUCCESS to CANCELED) are completely modeled here, each of the
// object actually contain more fields than listed here. To not produce unnecessary parsing failures
// this schema only contains fields that are likely relevant or even mandatory for us.
// The docs (https://docs-autoident.idnow.io/?version=latest) list other fields and more explanation
// in the "Retrieve Ident" section.
export const idnowIdentificationProcessSchema = z.union([
  z.object({
    result: z.literal('SUCCESS'),
    companyid: z.literal(IDNOW_CUSTOMER),
    identificationtime: z.iso8601Date(),
    id: idnowIdentIdSchema,
    type: idnowIdentificationProcessTypeSchema,
    transactionnumber: z.uuid(),
  }),
  z.object({
    result: z.nativeEnum(IDResult),
    transactionnumber: z.uuid(),
  }),
]);

export const idnowUserDataSchema = z.object({
  valueIdentity: z.string(),
  valueMinimalIdentity: z.string(),
  valueFace: z.string(),
});

export type IDnowUserData = ZodInfer<typeof idnowUserDataSchema>;

export const idnowIdentificationSchema = z.object({
  // Same here: this actually contains more fields but we don't care about these at the moment.
  identificationprocess: idnowIdentificationProcessSchema,
  userdata: idnowUserDataSchema.optional(),
});

export type IDnowIdentificationProcess = ZodInfer<
  typeof idnowIdentificationProcessSchema
>;

export type IDnowIdentification = ZodInfer<typeof idnowIdentificationSchema>;

export const idNowWebhookSchema = z.object({
  identificationprocess: z.object({
    result: z.union([
      z.literal('SUCCESS'),
      z.literal('SUCCESS_DATA_CHANGED'),
      z.literal('FRAUD_SUSPICION_CONFIRMED'),
      z.literal('CANCELED'),
      z.literal('ABORTED'),
    ]),
    companyid: z.literal(IDNOW_CUSTOMER),
    identificationtime: z.iso8601Date().optional(),
    id: idnowIdentIdSchema,
    type: idnowIdentificationProcessTypeSchema,
    transactionnumber: z.uuid(),
  }),
  userdata: idnowUserDataSchema.optional(),
});
