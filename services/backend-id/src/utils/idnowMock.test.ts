import { generateMockVC } from 'utils/idnowMock';

const UNCOMPRESSED_EC_PUBLIC_KEY_BASE64 =
  'BGU6WWpmDpGUlCionFK3Itc5GY63Qq9Sd+RsD+repg4lmwMNpZrioOzrpSsYglvZRzjv10heKz596HPzF14QgHk=';

describe('generateMockVC()', function () {
  describe('when called with raw uncompressed EC public key (X9.63)', function () {
    it('returns VC', async function () {
      expect(
        await generateMockVC(UNCOMPRESSED_EC_PUBLIC_KEY_BASE64)
      ).not.toBeNull();
    });
  });
});
