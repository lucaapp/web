import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import config from 'config';
import logger from 'utils/logger';
import { proxyAgent, proxyClient } from 'utils/proxy';
import { createLoggingInterceptors } from 'utils/axios';
import {
  IDnowIdentification,
  idnowIdentificationSchema,
} from 'utils/idnow.schemas';

const GATEWAY_URL = config.get('idnow.gatewayUrl');
const SSI_EMITTER_SERVICE_URL = config.get('idnow.ssiVcEmitterServiceUrl');
const API_KEY = config.get('idnow.apiKey');
const CUSTOMER = config.get('idnow.customer');
const HEADER_AUTH_KEY = 'X-API-LOGIN-TOKEN';

const getGatewayEndpointURL = (endpointPath: string) =>
  `${GATEWAY_URL}/api/v1/${CUSTOMER}${endpointPath}`;

const loginURL = getGatewayEndpointURL(`/login`);

const getEmitterServiceEndpointURL = (endpointPath: string) =>
  `${SSI_EMITTER_SERVICE_URL}/public/autoident/${CUSTOMER}${endpointPath}`;

const getIdentURL = (transactionNumber: string) =>
  getEmitterServiceEndpointURL(`/identifications/${transactionNumber}`);

const getStartIdentUrl = (transactionNumber: string) =>
  getEmitterServiceEndpointURL(`/identifications/${transactionNumber}/start`);

const getRevokeIdentUrl = (transactionNumber: string) =>
  getEmitterServiceEndpointURL(`/identifications/${transactionNumber}/revoke`);

const login = async (): Promise<string | null> => {
  try {
    const response = await proxyClient.post(loginURL, { apiKey: API_KEY });
    if (!response.data?.authToken) {
      throw new Error('Invalid Response Payload');
    }

    return response.data.authToken;
  } catch (error) {
    logger.error(`Ident Login failed`, error);
    return null;
  }
};

let authToken: string | null;

type RetryableAxiosError = AxiosError & {
  config: AxiosRequestConfig & {
    retriedRequest: boolean;
  };
};

const idnowInstance = axios.create({
  httpsAgent: proxyAgent,
  proxy: false,
});

const loggingInterceptors = createLoggingInterceptors('IDnow Client');
idnowInstance.interceptors.request.use(...loggingInterceptors.request);
idnowInstance.interceptors.response.use(...loggingInterceptors.response);

idnowInstance.interceptors.response.use(
  response => response,
  async (error: AxiosError) => {
    const originalRequest = (error as RetryableAxiosError).config;

    const handleAs401 =
      error.response?.status === 401 ||
      error.message.includes('401') ||
      error.response?.data?.message.includes('401');

    // IDnow has no specific return code for expired tokens
    if (handleAs401 && !originalRequest.retriedRequest) {
      originalRequest.retriedRequest = true;

      authToken = await login();

      if (!authToken) {
        return Promise.reject(error);
      }

      originalRequest.headers = {
        ...(originalRequest.headers || {}),
        [HEADER_AUTH_KEY]: authToken,
      };

      return idnowInstance(originalRequest);
    }
    return Promise.reject(error);
  }
);

idnowInstance.interceptors.request.use(async request => {
  if (!authToken) {
    authToken = await login();
  }

  request.headers = {
    ...(request.headers || {}),
    ...(!!authToken && {
      [HEADER_AUTH_KEY]: authToken,
    }),
  };

  return request;
});

type TIDnowStartIdentRequest = {
  bindingKey: string;
  encryptionKey: string;
};
type TIDnowStartIdentResponse = { jws: string };

export const createIdent = async ({
  transactionNumber,
  bindingKey,
  encryptionKey,
}: {
  transactionNumber: string;
  bindingKey: string;
  encryptionKey: string;
}): Promise<string | null> => {
  try {
    const startIdentUrl = getStartIdentUrl(transactionNumber);

    const response = await idnowInstance.post<
      TIDnowStartIdentResponse,
      AxiosResponse<TIDnowStartIdentResponse>,
      TIDnowStartIdentRequest
    >(startIdentUrl, {
      bindingKey,
      encryptionKey,
    });
    return response.data.jws;
  } catch (error) {
    logger.error(
      error && typeof error === 'object' ? error : { error },
      `Failed to create IDnow identification process`
    );

    return null;
  }
};

export const getIdent = async (
  transactionNumber: string
): Promise<IDnowIdentification | null> => {
  try {
    const response = await idnowInstance.get<IDnowIdentification>(
      getIdentURL(transactionNumber),
      {
        validateStatus: status =>
          (status >= 200 && status < 300) || status === 404,
      }
    );

    if (response.status === 404) {
      logger.warn('IdentId not found at IDnow API.');
      return null;
    }

    return idnowIdentificationSchema.parse(response.data);
  } catch (error) {
    logger.error(
      error && typeof error === 'object' ? error : { error },
      `Failed to get IDnow identification process`
    );
    throw error;
  }
};

export const revokeIdent = async (transactionNumber: string): Promise<void> => {
  const revokeIdentUrl = getRevokeIdentUrl(transactionNumber);

  await idnowInstance.post<
    TIDnowStartIdentResponse,
    void,
    TIDnowStartIdentRequest
  >(revokeIdentUrl);
};
