import moment from 'moment';
import { IdentFailureReason, IdentState, IDResult } from 'constants/ident';
import { IdentInstance } from 'database/models/ident';
import { getIdent } from 'utils/idnow';
import { IDnowIdentification } from 'utils/idnow.schemas';
import logger from 'utils/logger';

const getIdentUpdatePayloadFromIDnowIdentificationProcess = (
  idNowIdent: IDnowIdentification
): Parameters<IdentInstance['update']>[0] => {
  if (idNowIdent.identificationprocess.result === IDResult.SUCCESS) {
    return {
      state: IdentState.SUCCESS,
      data: idNowIdent.userdata,
    };
  }
  return {
    state: IdentState.FAILED,
    reason: IdentFailureReason.IDNOW_WEBHOOK_ERROR,
  };
};

export const tryManualRefreshIdent = async (
  ident: IdentInstance
): Promise<void> => {
  if (!ident.transactionNumber) {
    throw new Error(
      `Cannot fetch IDnow ident from ident without transactionNumber`
    );
  }

  try {
    const idnowIdent = await getIdent(ident.transactionNumber);

    if (!idnowIdent) {
      if (moment(ident.createdAt) > moment().subtract(1, 'hour')) {
        return;
      }
      await ident.update({
        state: IdentState.FAILED,
        reason: IdentFailureReason.IDNOW_EXPIRED,
      });
    } else {
      const updatePayload = getIdentUpdatePayloadFromIDnowIdentificationProcess(
        idnowIdent
      );
      await ident.update(updatePayload);
    }
  } catch (error) {
    logger.error(
      error && typeof error === 'object' ? error : { error },
      `Failed to manual refresh IDnow identification process`
    );
  }
};
