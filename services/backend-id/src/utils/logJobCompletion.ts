import { JobCompletion } from 'database';

export const logJobCompletion = async (
  name: string,
  meta: Record<string, string>
): Promise<void> => {
  await JobCompletion.create({
    name,
    meta: JSON.stringify(meta),
  });
};
