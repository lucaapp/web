declare namespace Express {
  export interface User {
    deviceId: string;
    deviceOS: string;
  }

  export interface Request {
    mocked?: boolean;
    delayRequest?: number;
  }
}
