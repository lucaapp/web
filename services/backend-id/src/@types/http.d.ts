declare module 'http' {
  export interface IncomingMessage {
    headers: Record<string, string>;
    originalUrl: string;
    route?: string;
  }

  export interface ServerResponse {
    headers: Record<string, string>;
    _contentLength: number;
  }
}
