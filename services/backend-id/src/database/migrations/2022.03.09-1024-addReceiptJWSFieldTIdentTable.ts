import { DataTypes } from 'sequelize';
import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn('Idents', 'receiptJWS', {
      type: DataTypes.TEXT,
      allowNull: true,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn('Idents', 'receiptJWS');
  },
};

export default migration;
