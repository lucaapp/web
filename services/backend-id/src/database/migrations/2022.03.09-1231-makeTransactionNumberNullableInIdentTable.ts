import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.changeColumn('Idents', 'transactionNumber', {
      type: DataTypes.UUID,
      allowNull: true,
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.changeColumn('Idents', 'transactionNumber', {
      type: DataTypes.UUID,
      allowNull: false,
    });
  },
};

export default migration;
