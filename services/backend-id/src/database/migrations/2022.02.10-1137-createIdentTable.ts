import { DataTypes, literal } from 'sequelize';
import { Migration } from 'types/umzug';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.createTable('Idents', {
      deviceId: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      transactionNumber: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      state: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      reason: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      revocationCode: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      identId: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      data: {
        type: DataTypes.JSON,
        allowNull: true,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: literal('CURRENT_TIMESTAMP'),
      },
    });
  },
  down: ({ context: queryInterface }) => {
    return queryInterface.dropTable('Idents');
  },
};

export default migration;
