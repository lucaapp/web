import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'Idents',
        'pendingAt',
        {
          type: DataTypes.DATE,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Idents',
        'succeededAt',
        {
          type: DataTypes.DATE,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Idents',
        'failedAt',
        {
          type: DataTypes.DATE,
          allowNull: true,
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('Idents', 'pendingAt', { transaction });
      await queryInterface.removeColumn('Idents', 'succeededAt', {
        transaction,
      });
      await queryInterface.removeColumn('Idents', 'failedAt', { transaction });
    });
  },
};

export default migration;
