import { Migration } from 'types/umzug';
import { DataTypes, literal } from 'sequelize';

const unrelatedColumns = {
  transactionNumber: {
    type: DataTypes.UUID,
    allowNull: true,
  },
  state: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  reason: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  revocationCode: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  receiptJWS: {
    type: DataTypes.TEXT,
    allowNull: true,
  },
  identId: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  data: {
    type: DataTypes.JSON,
    allowNull: true,
  },
  createdAt: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: literal('CURRENT_TIMESTAMP'),
  },
  updatedAt: {
    type: DataTypes.DATE,
    allowNull: false,
    defaultValue: literal('CURRENT_TIMESTAMP'),
  },
  pendingAt: {
    type: DataTypes.DATE,
    allowNull: true,
  },
  succeededAt: {
    type: DataTypes.DATE,
    allowNull: true,
  },
  failedAt: {
    type: DataTypes.DATE,
    allowNull: true,
  },
  androidKeyAttestationCertificateChain: {
    type: DataTypes.TEXT,
    allowNull: true,
  },
};

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.dropTable('Idents');

      await queryInterface.createTable(
        'Idents',
        {
          id: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
          },
          deviceId: {
            type: DataTypes.UUID,
            allowNull: false,
          },
          deletedAt: {
            allowNull: true,
            type: DataTypes.DATE,
          },
          ...unrelatedColumns,
        },
        { transaction }
      );
    });
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.dropTable('Idents');

      await queryInterface.createTable(
        'Idents',
        {
          deviceId: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
          },
          ...unrelatedColumns,
        },
        { transaction }
      );
    });
  },
};

export default migration;
