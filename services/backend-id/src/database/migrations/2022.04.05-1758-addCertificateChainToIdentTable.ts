import { Migration } from 'types/umzug';
import { DataTypes } from 'sequelize';

const migration: Migration = {
  up: async ({ context: queryInterface }) => {
    await queryInterface.addColumn(
      'Idents',
      'androidKeyAttestationCertificateChain',
      {
        type: DataTypes.TEXT,
        allowNull: true,
      }
    );
  },

  down: async ({ context: queryInterface }) => {
    await queryInterface.removeColumn(
      'Idents',
      'androidKeyAttestationCertificateChain'
    );
  },
};

export default migration;
