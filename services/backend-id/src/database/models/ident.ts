import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';
import { IdentState, IdentFailureReason } from 'constants/ident';

export interface VC {
  valueFace: string;
  valueIdentity: string;
  valueMinimalIdentity: string;
}

interface Attributes {
  id: string;
  deviceId: string;
  transactionNumber: string | null;
  state: IdentState;
  reason: IdentFailureReason | null;
  revocationCode: string;
  identId: string | null;
  receiptJWS: string | null;
  data: VC | null;
  updatedAt: Date;
  createdAt: Date;
  pendingAt: Date;
  succeededAt: Date;
  failedAt: Date;
  deletedAt: Date;
  androidKeyAttestationCertificateChain: string | null;
}

type CreationAttributes = {
  deviceId: string;
  state: IdentState;
  revocationCode: string;
  androidKeyAttestationCertificateChain: string | null;
};

export interface IdentInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initIdent = (sequelize: Sequelize): ModelCtor<IdentInstance> => {
  return sequelize.define<IdentInstance>(
    'Ident',
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      deviceId: {
        type: DataTypes.UUID,
        allowNull: false,
        defaultValue: DataTypes.UUIDV4,
      },
      transactionNumber: {
        type: DataTypes.UUID,
        allowNull: true,
      },
      state: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      reason: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      revocationCode: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      identId: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      receiptJWS: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      data: {
        type: DataTypes.JSON,
        allowNull: true,
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      pendingAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      succeededAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      failedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      deletedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      // This will only be set on Android when creating the ident. This is the certificate chain of
      // of the ID key attestation which is performed prior to the issueing of the ident process at
      // IDnow.
      androidKeyAttestationCertificateChain: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
      hooks: {
        beforeUpdate: (ident: IdentInstance) => {
          const isStateChanging = ident.changed('state');

          if (!isStateChanging) {
            return;
          }

          if (ident.state === IdentState.PENDING) {
            ident.setDataValue('pendingAt', new Date());
          }

          if (ident.state === IdentState.SUCCESS) {
            ident.setDataValue('succeededAt', new Date());
          }

          if (ident.state === IdentState.FAILED) {
            ident.setDataValue('failedAt', new Date());
          }
        },
      },
    }
  );
};
