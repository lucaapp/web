/* eslint-disable import/no-cycle, max-lines */
import {
  Sequelize,
  ConnectionError,
  ConnectionRefusedError,
  ConnectionTimedOutError,
  Options,
} from 'sequelize';
import config from 'config';
import logger from 'utils/logger';
import client from 'utils/metrics';
import dbConfig from 'database/config';

import { initIdent } from './models/ident';
import { initInternalAccessUser } from './models/internalAccessUser';
import { initIPAddressAllowLists } from './models/ipAddressAllowList';
import { initIPAddressBlockList } from './models/ipAddressBlockList';
import { initJobCompletions } from './models/jobCompletions';

const environment = config.util.getEnv('NODE_ENV');
const databaseConfig = dbConfig[environment as keyof typeof dbConfig];

export const database = new Sequelize({
  ...(databaseConfig as Partial<Options>),
  logging: (message, time) => logger.debug({ time, message }),
  logQueryParameters: false,
  benchmark: true,
  retry: {
    name: 'database',
    max: 60 * 2,
    backoffBase: 500,
    backoffExponent: 1,
    report: (_message: string, info: { $current: number }) => {
      if (info.$current === 1) return;
      logger.warn(`Trying to connect to database (try #${info.$current})`);
    },
    match: [ConnectionError, ConnectionRefusedError, ConnectionTimedOutError],
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } as any,
});

const counter = new client.Counter({
  name: 'sequelize_table_creation_count',
  help: 'Total database rows created since process start.',
  labelNames: ['tableName'],
});

database.addHook('beforeCreate', instance => {
  counter.inc({
    tableName: instance.constructor.name,
  });
});

database.addHook('beforeBulkCreate', instances => {
  if (instances.length <= 0) return;
  counter.inc(
    {
      tableName: instances[0].constructor.name,
    },
    instances.length
  );
});

export const Ident = initIdent(database);
export const InternalAccessUser = initInternalAccessUser(database);
export const IPAddressAllowList = initIPAddressAllowLists(database);
export const IPAddressBlockList = initIPAddressBlockList(database);
export const JobCompletion = initJobCompletions(database);

const models = {
  Ident,
  InternalAccessUser,
  IPAddressAllowList,
  IPAddressBlockList,
  JobCompletion,
};

export type Models = typeof models;
