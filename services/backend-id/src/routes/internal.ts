import { Router } from 'express';
import passport from 'passport';
import { requireInternalIp } from 'middlewares/requireInternalIp';

import metricsRouter from './internal/metrics';
import jobsRouter from './internal/jobs';

const router = Router();

router.use(requireInternalIp);
router.use('/metrics', metricsRouter);
router.use(passport.authenticate('bearer-internalAccess', { session: false }));

router.use('/jobs', jobsRouter);

export default router;
