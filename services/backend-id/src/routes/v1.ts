import { Router } from 'express';
import swaggerRouter from './v1/swagger';
import identRouter from './v1/ident';
import webhooksRouter from './v1/webhooks';
import keysRouter from './v1/keys';

const router = Router();

router.use('/swagger', swaggerRouter);
router.use('/ident', identRouter);
router.use('/webhooks', webhooksRouter);
router.use('/keys', keysRouter);

export default router;
