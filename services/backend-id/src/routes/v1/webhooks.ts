import { Router } from 'express';
import config from 'config';
import status from 'http-status';
import { validateSchema } from 'middlewares/validateSchema';
import passport from 'passport';
import { Ident } from 'database';
import { notFound } from '@hapi/boom';
import { IDResult, IdentFailureReason, IdentState } from 'constants/ident';
import type { VC } from 'database/models/ident';
import { idNowWebhookSchema } from 'utils/idnow.schemas';

const basicAuthEnabled = config.get('webhook.basicAuth.enabled');

const router = Router();

if (basicAuthEnabled) {
  router.use(passport.authenticate('basic', { session: false }));
}

router.post(
  '/',
  validateSchema(idNowWebhookSchema, '1mb', true),
  async (request, response) => {
    request.log.warn(request.body, 'Received webhook on POST');

    const { result, transactionnumber } = request.body.identificationprocess;

    const ident = await Ident.findOne({
      where: {
        transactionNumber: transactionnumber,
      },
    });
    if (!ident) {
      throw notFound('transactionnumber not found');
    }

    if (result === IDResult.SUCCESS) {
      ident.state = IdentState.SUCCESS;
      ident.data = request.body.userdata as VC;
    } else {
      ident.state = IdentState.FAILED;
      ident.reason = IdentFailureReason.IDNOW_WEBHOOK_ERROR;
      request.log.error(result, IdentFailureReason.IDNOW_WEBHOOK_ERROR);
    }

    await ident.save();
    return response.sendStatus(status.OK);
  }
);

export default router;
