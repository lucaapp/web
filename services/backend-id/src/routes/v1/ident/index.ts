import config from 'config';
import moment from 'moment';
import passport from 'passport';
import status from 'http-status';
import { RequestHandler, Response, Router } from 'express';
import { validateSchema } from 'middlewares/validateSchema';
import { KEY_GENERATORS, rateLimitRoute } from 'middlewares/rateLimit';
import { queue, estimateQueueSize } from 'utils/queue';
import { generateRevocationCode } from 'utils/generators';
import { serverUnavailable, notFound, conflict, forbidden } from '@hapi/boom';
import { Ident } from 'database';
import type { VC } from 'database/models/ident';
import { IdentFailureReason, IdentState } from 'constants/ident';
import { tryManualRefreshIdent } from 'utils/idents';
import { revokeIdent } from 'utils/idnow';
import { createIdentSchema } from './ident.schemas';
import { verifyAssertionData } from './verifyAssertionData';
import {
  QUEUE_SIZE_LIMIT,
  IDNOW_PENDING_IDENT_AGE_BEFORE_MANUAL_PULL,
  IDENT_NOT_FOUND_MESSAGE,
} from './config';

const router = Router();

router.use(passport.authenticate('header-device', { session: false }));

const checkForMock: RequestHandler = (request, response, next) => {
  const mockEnabled = config.get('allowMocking') && !!request.header('x-mock');

  const delayRequest = mockEnabled
    ? Number.parseInt(request.header('x-delay') || '0', 10)
    : 0;

  request.mocked = mockEnabled;
  request.delayRequest = delayRequest;

  return next();
};

const convertDERBase64CertificateToPEM = (derBase64: string): string => {
  return `-----BEGIN CERTIFICATE-----
${derBase64}
-----END CERTIFICATE-----`;
};

router.post(
  '/',
  validateSchema(createIdentSchema),
  checkForMock,
  rateLimitRoute(
    {
      points: 5,
      duration: moment.duration(1, 'hour').asSeconds(),
      blockDuration: moment.duration(1, 'day').asSeconds(),
    },
    [KEY_GENERATORS.ROUTE, KEY_GENERATORS.DEVICE_ID]
  ),
  async (request, response) => {
    const { deviceId } = request.user as Express.User;
    const estimatedQueueSize = await estimateQueueSize();

    if (estimatedQueueSize > QUEUE_SIZE_LIMIT) {
      throw serverUnavailable('Queue too long.');
    }

    await verifyAssertionData(
      { ...request.body, deviceId },
      request.headers,
      request.log
    );

    try {
      await Ident.create({
        deviceId,
        state: IdentState.QUEUED,
        revocationCode: generateRevocationCode(),
        androidKeyAttestationCertificateChain:
          'idPublicKeyAttestationCertificates' in request.body
            ? request.body.idPublicKeyAttestationCertificates
                .map(derBase64 => convertDERBase64CertificateToPEM(derBase64))
                .join('\n\n')
            : null,
      });
    } catch {
      throw conflict('Ident already exists.');
    }

    const {
      mocked,
      delayRequest,
      body: { idPublicKey, encPublicKey },
    } = request;

    await queue.add(
      {
        deviceId,
        idPublicKey,
        encPublicKey,
        enableMock: !!mocked,
      },
      { delay: delayRequest }
    );

    response.send({
      waitForMs: estimatedQueueSize,
    });
  }
);

type TGetIdentResponseBody = {
  state: IdentState;
  reason: IdentFailureReason | null;
  identId: string | null;
  data: VC | null;
  revocationCode: string;
  receiptJWS: string | null;
};

router.get(
  '/',
  rateLimitRoute(
    {
      points: 10000,
      duration: moment.duration(1, 'hour').asSeconds(),
      blockDuration: moment.duration(1, 'day').asSeconds(),
    },
    [KEY_GENERATORS.ROUTE, KEY_GENERATORS.DEVICE_ID]
  ),
  async (request, response: Response<TGetIdentResponseBody>) => {
    const user = request.user as Express.User;
    const ident = await Ident.findOne({ where: { deviceId: user.deviceId } });
    if (!ident) throw notFound(IDENT_NOT_FOUND_MESSAGE);

    const identUnmodifiedTime = Date.now() - ident.updatedAt.getTime();
    if (
      ident.state === IdentState.PENDING &&
      identUnmodifiedTime >= IDNOW_PENDING_IDENT_AGE_BEFORE_MANUAL_PULL
    ) {
      request.log.warn(
        { ident },
        `Ident is in state PENDING for at least ${Math.round(
          identUnmodifiedTime / 1000
        )} seconds. Will try to fetch it manually from IDnow`
      );
      await tryManualRefreshIdent(ident);
    }

    response.send({
      state: ident.state,
      reason: ident.reason,
      identId: ident.identId,
      data: ident.data,
      revocationCode: ident.revocationCode,
      receiptJWS: ident.receiptJWS,
    });
  }
);

router.delete(
  '/data',
  rateLimitRoute(
    {
      points: 5,
      duration: moment.duration(1, 'hour').asSeconds(),
      blockDuration: moment.duration(1, 'day').asSeconds(),
    },
    [KEY_GENERATORS.ROUTE, KEY_GENERATORS.DEVICE_ID]
  ),
  async (request, response) => {
    const user = request.user as Express.User;
    const ident = await Ident.findOne({
      where: {
        deviceId: user.deviceId,
      },
    });
    if (!ident) throw notFound(IDENT_NOT_FOUND_MESSAGE);
    await ident.update({ data: null });
    response.sendStatus(status.NO_CONTENT);
  }
);

router.delete(
  '/',
  rateLimitRoute(
    {
      points: 5,
      duration: moment.duration(1, 'hour').asSeconds(),
      blockDuration: moment.duration(1, 'day').asSeconds(),
    },
    [KEY_GENERATORS.ROUTE, KEY_GENERATORS.DEVICE_ID]
  ),
  async (request, response) => {
    const user = request.user as Express.User;
    const ident = await Ident.findOne({
      where: {
        deviceId: user.deviceId,
      },
    });
    if (!ident) throw notFound(IDENT_NOT_FOUND_MESSAGE);
    if (ident.state === IdentState.QUEUED)
      throw forbidden('Ident can not be removed while queued.');
    if (!ident.transactionNumber)
      throw forbidden('Ident can not be removed in this state.');
    await revokeIdent(ident.transactionNumber);
    ident.data = null;
    await ident.save();
    await ident.destroy();
    response.sendStatus(status.NO_CONTENT);
  }
);

export default router;
