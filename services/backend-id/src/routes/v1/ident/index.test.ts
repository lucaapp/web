/* eslint-disable unicorn/consistent-function-scoping */
/* eslint-disable @typescript-eslint/no-explicit-any */
import express, { Request } from 'express';
import request from 'supertest';
import 'express-async-errors';
import { setupDBRedisAndApp } from 'testing/utils';
import { httpLogger } from 'utils/logger';
import { handleError } from 'middlewares/error';
import { Ident } from 'database';
import { VerifiedCallback } from 'passport-custom';
import identRouter from './index';

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(httpLogger);
app.use('/ident', identRouter);
app.use(handleError);

jest.mock('../../../middlewares/rateLimit.ts', () => {
  const originalModule = jest.requireActual('../../../middlewares/rateLimit');

  return {
    ...originalModule,
    rateLimitRoute: (_key: any) => (_: any, __: any, next: () => any) => next(),
  };
});

jest.mock('../../../passport/device', () => {
  const { Strategy } = jest.requireActual('passport-custom');
  return new Strategy(async (httpRequest: Request, done: VerifiedCallback) => {
    // eslint-disable-next-line global-require
    done(null, { deviceId: require('uuid').v4() });
  });
});

describe('DELETE /ident', () => {
  beforeAll(async () => {
    await setupDBRedisAndApp();
  });

  afterEach(async () => {
    await Ident.truncate();
  });

  describe('when no ident for device can be found', () => {
    it('responds 404', async () => {
      const response = await request(app).delete('/ident').send();

      expect(response.status).toEqual(404);
    });
  });
});
