import config from 'config';

export const QUEUE_SIZE_LIMIT = config.get('queue.limiter.size');
export const IDNOW_PENDING_IDENT_AGE_BEFORE_MANUAL_PULL = config.get(
  'idnow.pendingIdentAgeBeforeManualPull'
);
export const IDENT_NOT_FOUND_MESSAGE = 'Ident not found.';
export const ATTESTATION_SERVICE_URL = config.get('attestationServiceURL');
export const ATTESTATION_SERVICE_ASSERTION_ENDPOINT = `${ATTESTATION_SERVICE_URL}/internal/assertion`;
export const ATTESTATION_SERVICE_ANDROID_KEY_ATTESTATION_ENDPOINT = `${ATTESTATION_SERVICE_URL}/internal/android-key-attestation`;
export const INTERNAL_ACCESS_TOKEN = config.get('internalAccessToken');
