import config from 'config';
import status from 'http-status';
import { boomify } from '@hapi/boom';
import { AxiosError, AxiosRequestConfig } from 'axios';
import { axiosClient } from 'utils/axios';
import { Logger } from 'pino';
import { IncomingHttpHeaders } from 'http';
import { z } from 'zod';
import { createIdentSchema } from './ident.schemas';
import {
  INTERNAL_ACCESS_TOKEN,
  ATTESTATION_SERVICE_ASSERTION_ENDPOINT,
  ATTESTATION_SERVICE_ANDROID_KEY_ATTESTATION_ENDPOINT,
} from './config';

export const verifyAssertionData = async (
  assertionData: z.infer<typeof createIdentSchema> & { deviceId: string },
  requestHeaders: IncomingHttpHeaders,
  logger: Logger
): Promise<void> => {
  try {
    logger.info(`Verifying assertion data via attestation service`);

    // If we are supposed to bypass, we do NOT skip the request to the attestation service but
    // just pass this demand forward. This is to keep the runtime behavior closer to what it would
    // actually look like in production.
    const shouldBypassVerification =
      config.get('allowAssertionVerificationsBypass') &&
      requestHeaders['x-bypass-attestation'];

    const internalRequestOptions: AxiosRequestConfig = {
      headers: {
        'internal-access-authorization': INTERNAL_ACCESS_TOKEN,

        ...(shouldBypassVerification
          ? {
              'x-bypass-attestation': true,
            }
          : {}),
      },
      // Make sure request throws if not 2xx
      validateStatus: statusCode => statusCode < 300,
    };
    const isAndroid = 'signature' in assertionData;

    try {
      await axiosClient.post(
        ATTESTATION_SERVICE_ASSERTION_ENDPOINT,
        isAndroid
          ? {
              platform: 'android',
              deviceId: assertionData.deviceId,
              nonce: assertionData.nonce,
              clientDataBase64: assertionData.idPublicKey,
              signature: assertionData.signature,
            }
          : {
              platform: 'ios',
              deviceId: assertionData.deviceId,
              nonce: assertionData.nonce,
              clientDataBase64: assertionData.idPublicKey,
              assertion: assertionData.assertion,
            },
        internalRequestOptions
      );
    } catch (error) {
      logger.error(
        error as AxiosError,
        `Verification of assertion data via attestation service failed`
      );
      throw error;
    }

    if (isAndroid) {
      logger.info(
        `Verifying Android key attestation data for idPublicKey via attestation service`
      );
      try {
        await axiosClient.post(
          ATTESTATION_SERVICE_ANDROID_KEY_ATTESTATION_ENDPOINT,
          {
            nonce: assertionData.idPublicKeyAttestationNonce,
            publicKey: assertionData.idPublicKey,
            certificates: assertionData.idPublicKeyAttestationCertificates,
          },
          internalRequestOptions
        );
      } catch (error) {
        logger.error(
          error as AxiosError,
          `Verification of Android key attestation data for idPublicKet via attestation service failed`
        );
        throw error;
      }
    }
  } catch (error) {
    throw boomify(error as AxiosError, {
      statusCode:
        (error as AxiosError)?.response?.status ?? status.INTERNAL_SERVER_ERROR,
    });
  }
};
