import { z } from 'utils/validation';

const commonSchemaFields = {
  encPublicKey: z.uncompressedECPublicKey(),
  idPublicKey: z.uncompressedECPublicKey(),
  nonce: z.string(),
};

const createAndroidIdentSchema = z.object({
  ...commonSchemaFields,
  signature: z.string(),
  idPublicKeyAttestationNonce: z.string(),
  idPublicKeyAttestationCertificates: z.array(z.string()),
});

const createIOSIdentSchema = z.object({
  ...commonSchemaFields,
  assertion: z.string(),
});

export const createIdentSchema = z.union([
  createIOSIdentSchema,
  createAndroidIdentSchema,
]);
