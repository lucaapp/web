import { Router } from 'express';

const DEV_KEY = {
  kty: 'EC',
  use: 'enc',
  crv: 'P-256',
  kid: 'key-3',
  key_ops: ['wrapKey'],
  x: 'oKpH5GBgrIsigbNtLimjqtYneazT4FFB2r7tA6f8j38',
  y: 'VzfypScH3yyhIoFULmGB8uF4OGdQpN6UHDCGP2pjaqM',
};

const router = Router();
router.get('/', async (request, response) => {
  response.send({
    keys: [DEV_KEY],
  });
});

export default router;
