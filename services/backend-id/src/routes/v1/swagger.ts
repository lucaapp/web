import { Router } from 'express';
import logger from 'utils/logger';
import { constructSwaggerSpec } from 'utils/swagger';

const router = Router();

const swaggerSpec = {
  openapi: '3.0.0',
  info: {
    title: 'luca id API',
    version: '1.0.0',
    description: '',
  },
  servers: [{ url: '/id/api/v1', description: 'This server' }],
};

(async () => {
  try {
    const { spec, middlewares } = await constructSwaggerSpec(
      './src/routes/{v1,swagger}/**/*.openapi.yaml',
      swaggerSpec
    );

    router.get('/swagger.json', (_request, response) => response.json(spec));
    router.use('/', ...middlewares);
  } catch (error) {
    logger.error(error as Error);
  }
})();

export default router;
