import { Router } from 'express';
import metrics from 'utils/metrics';

const router = Router();

router.get('/', async (request, response) => {
  response.type('text/plain');
  response.send(await metrics.register.metrics());
});

export default router;
