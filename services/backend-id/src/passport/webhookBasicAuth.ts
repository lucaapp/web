import { BasicStrategy } from 'passport-http';
import config from 'config';

const { username: basicUsername, password: basicPassword } = config.get(
  'webhook.basicAuth'
);

const webhookBasicAuthStrategy = new BasicStrategy(
  (username, password, done) => {
    if (username === basicUsername && password === basicPassword) {
      return done(null, true);
    }
    return done(null, false);
  }
);

export default webhookBasicAuthStrategy;
