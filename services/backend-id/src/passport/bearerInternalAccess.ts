/* eslint-disable promise/no-callback-in-promise */

import passportCustom from 'passport-custom';
import logger from 'utils/logger';
import { InternalAccessUser } from '../database';
import { pseudoHashPassword } from '../utils/hash';

// eslint-disable-next-line consistent-return
const bearerStrategy = new passportCustom.Strategy(async (request, done) => {
  let token = request.headers['internal-access-authorization'];

  if (Array.isArray(token)) {
    [token] = token;
  }

  if (!token) {
    return done(new Error('missing token'));
  }

  if (token.startsWith('Bearer')) {
    token = token.split(' ').pop();
  }

  if (!token) {
    // This would happeng for "Bearer", "BearerAnythingWithoutASpace" or "BearerWithSpaceOnlyAtTheEnd "
    return done(new Error('Invalid token'));
  }

  try {
    const decoded = Buffer.from(token, 'base64').toString('ascii');
    const [name, password] = decoded.split(':');
    const internalAccessUser = await InternalAccessUser.findByPk(name);

    if (!internalAccessUser) {
      // prevent timing issues
      await pseudoHashPassword();
      return done(null, false);
    }

    const isValidPassword = await internalAccessUser.checkPassword(password);

    if (!isValidPassword) {
      return done(null, false);
    }

    return done(null, internalAccessUser);
  } catch (error) {
    // This could still happen if token cannot be decoded as base64

    logger.error(
      error instanceof Error ? error : { error },
      `Token validation of internal-access-authorization header threw error`
    );
    return done(new Error('Invalid token'));
  }
});

export default bearerStrategy;
