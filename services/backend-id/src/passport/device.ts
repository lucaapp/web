/* eslint-disable promise/no-callback-in-promise */
import config from 'config';
import moment from 'moment';
import { unauthorized } from '@hapi/boom';
import { Request } from 'express';
import * as jose from 'jose';
import { URL } from 'url';
import { Strategy, VerifiedCallback } from 'passport-custom';

const CLOCK_TOLERANCE = moment.duration(1, 'minute').asSeconds();

const JWT_HEADER_NAME = 'X-Auth';
const JWT_ALLOWED_ALGORITHMS = ['ES256'];
const JWT_ISSUER = 'luca-attestation';

const JWT_VERIFY_OPTIONS = {
  issuer: JWT_ISSUER,
  clockTolerance: CLOCK_TOLERANCE,
  algorithms: JWT_ALLOWED_ALGORITHMS,
};

const JWKS_URL = config.get('jwt.remoteUrl');
const JWKS = jose.createRemoteJWKSet(new URL(JWKS_URL));

const deviceStrategy = new Strategy(
  async (request: Request, done: VerifiedCallback) => {
    try {
      const jwt = request.header(JWT_HEADER_NAME);
      if (typeof jwt !== 'string') {
        return done(unauthorized('X-Auth header not set.'));
      }
      const { payload } = await jose.jwtVerify(jwt, JWKS, JWT_VERIFY_OPTIONS);

      const user = {
        deviceId: payload.sub,
        deviceOS: payload.os,
      };
      return done(null, user);
    } catch (error) {
      request.log.warn(error as Error);
      return done(unauthorized('X-Auth header invalid.'));
    }
  }
);

export default deviceStrategy;
