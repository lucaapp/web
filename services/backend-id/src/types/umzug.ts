import { QueryInterface } from 'sequelize';
import { MigrationParams } from 'umzug';

export type MigrationFunction = (
  parameters: MigrationParams<QueryInterface>
) => Promise<unknown>;

export type Migration = { up: MigrationFunction; down: MigrationFunction };

export type Seed = Migration;
