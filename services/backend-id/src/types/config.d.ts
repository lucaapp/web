type ConfigSchema = import('../../config/schema').Config;

// Taken from https://dev.to/tipsy_dev/advanced-typescript-reinventing-lodash-get-4fhe
type GetFieldType<Object_, Path> = Path extends `${infer Left}.${infer Right}`
  ? Left extends keyof Object_
    ?
        | GetFieldType<Exclude<Object_[Left], undefined>, Right>
        | Extract<Object_[Left], undefined>
    : undefined
  : Path extends keyof Object_
  ? Object_[Path]
  : never;

// Taken from https://stackoverflow.com/a/58436959/973481
type Join<K, P> = K extends string | number
  ? P extends string | number
    ? `${K}${'' extends P ? '' : '.'}${P}`
    : never
  : never;
type Paths<T, D extends number = 10> = [D] extends [never]
  ? never
  : T extends Record<string, unknown>
  ? {
      [K in keyof T]-?: K extends string | number
        ? `${K}` | Join<K, Paths<T[K], Prev[D]>>
        : never;
    }[keyof T]
  : '';
type Leaves<T, D extends number = 10> = [D] extends [never]
  ? never
  : T extends Record<string, unknown>
  ? { [K in keyof T]-?: Join<K, Leaves<T[K], Prev[D]>> }[keyof T]
  : '';

type ConfigSchemaPaths = Paths<ConfigSchema>;

// Not used at the moment
// type NestedObjectLeaves = Leaves<ConfigSchema>;

declare module 'config' {
  namespace c {
    interface IConfig {
      get<KeyPath extends Paths<ConfigSchema>>(
        setting: KeyPath
      ): GetFieldType<ConfigSchema, KeyPath>;
      has(setting: string): boolean;
      // Workaround beauce the usage of legacy namespaces in config's type definitions seem to break
      // declaration merging
      util: import('../../node_modules/@types/config').IUtil;
    }
  }

  type TypeToEnvironmentSpecifier<Type> = Type extends number
    ? {
        __name: string;
        __format: 'number';
      }
    : Type extends boolean
    ? {
        __name: string;
        __format: 'boolean';
      }
    :
        | string
        | {
            __name: string;
            __format: 'string';
          };

  export type EnvironmentMapping<
    TConfigObject extends Config | GetFieldType<Paths<ConfigSchema>>
  > = {
    [key in keyof TConfigObject]?: TConfigObject[key] extends Record<
      string,
      unknown
    >
      ? EnvironmentMapping<TConfigObject[key]>
      : TypeToEnvironmentSpecifier<TConfigObject[key]>;
  };

  declare let exp: c.IConfig;

  export = exp;
}
