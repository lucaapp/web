/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  rootDir: '.',
  roots: ['<rootDir>/src'],
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: ['<rootDir>/src/**/*.test.ts'],
  moduleNameMapper: {
    '^utils/(.*)$': '<rootDir>/src/utils/$1',
    '^constants/(.*)$': '<rootDir>/src/constants/$1',
    '^database/(.*)$': '<rootDir>/src/database/$1',
    '^database$': '<rootDir>/src/database/index',
    '^middlewares/(.*)$': '<rootDir>/src/middlewares/$1',
    '^passport/(.*)$': '<rootDir>/src/passport/$1',
    '^routes/(.*)$': '<rootDir>/src/routes/$1',
    '^testing/(.*)$': '<rootDir>/src/testing/$1',
  },
  collectCoverageFrom: [
    'src/**/*.{js,ts}',
    '!<rootDir>/node_modules',
    '!<rootDir>/src/index.ts',
    '!<rootDir>/src/database/config.ts',
    '!<rootDir>/src/database/migrations/**',
    '!<rootDir>/src/database/seeds/**',
    '!<rootDir>/src/@types/**',
  ],
  coverageReporters: ['lcov', 'text', 'cobertura'],
  coverageThreshold: {
    global: {
      statements: 44,
      branches: 16,
      lines: 42,
      functions: 19,
    },
    './src/utils/': {
      statements: 40,
      branches: 1,
      lines: 40,
      functions: 10,
    },
    './src/routes/': {
      statements: 58,
      branches: 0,
      lines: 59,
      functions: 5,
    },
  },
  globals: { 'ts-jest': { tsconfig: 'tsconfig.test.json' } },
  testTimeout: 60000,
  reporters: [
    'default',
    [
      'jest-junit',
      {
        classNameTemplate: '{suitename}',
        titleTemplate: '{classname} {title}',
        addFileAttribute: 'true',
        filePathPrefix: 'services/backend-id',
      },
    ],
  ],
};
