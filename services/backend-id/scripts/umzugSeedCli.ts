import { database } from 'database';
import { createUmzug } from './umzug';

export const umzug = createUmzug({
  directory: './src/database/seeds',
  tableName: '_Seeds',
});

(async () => {
  await umzug.runAsCLI();
  await database.close();
})();
