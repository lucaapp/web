import { database } from 'database';
import path from 'path';
import { QueryInterface } from 'sequelize';
import { Migration } from 'types/umzug';
import { Umzug, SequelizeStorage } from 'umzug';
import logger from 'utils/logger';

const logFunctionUmzugToPino = (logLevel: keyof typeof logger) => (
  umzugEvent: Record<string, unknown>
) => {
  // eslint-disable-next-line security/detect-object-injection
  logger[logLevel]({ umzugEvent }, `Umzug event "${umzugEvent.event}"`);
};

export function createUmzug({
  directory,
  tableName,
}: {
  directory: string;
  tableName: string;
}): Umzug<QueryInterface> {
  return new Umzug({
    migrations: {
      glob: path.join(__dirname, '../', directory, '*.{js,ts}'),
      resolve: ({ name, path: migrationFilePath }) => {
        if (!migrationFilePath) {
          throw new Error('Did not receive path to require');
        }

        // When we moved to TS files for migrations and seeds, the name of each would have changed
        // due to the file extension being included. To not have umzug consider migrations or seeds
        // as "new" because tables _Migrations/_Seeds don't contain such an entry, we convert it
        // back to the old name for now.
        const normalizedName = name.replace(/\.ts$/, '.js');

        const { up, down } = require(migrationFilePath).default;

        return { name: normalizedName, up, down };
      },
    },
    context: database.getQueryInterface(),
    storage: new SequelizeStorage({
      sequelize: database,
      tableName,
    }),
    logger: {
      debug: logFunctionUmzugToPino('debug'),
      info: logFunctionUmzugToPino('info'),
      warn: logFunctionUmzugToPino('warn'),
      error: logFunctionUmzugToPino('error'),
    },
  });
}

// The following is a workaround to assert that our `Migration` type from types/migration is
// identical to the umzug type resulting from our configuration (specifically, the `context`
// option). If the two types would be different, we wouldn't notice at compile time because we are
// dynamically require()-ing the migrations files (i.e. they are not checked by TS as part of the
// module tree).
// We could let the migration files directly reference umzug's generated types but that would have
// us produce weird import paths to this script file.
// We could also split off the umzug setup itself (excl. the runAsACLI() path) into a non-scripts
// file but that would cause ESLint rules to trigger which aren't meant to trigger for scripts
// (i.e. dynamic require() call).
type DerivedMigrationFunction = ReturnType<
  typeof createUmzug
>['_types']['migration'];
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const _: Migration = {} as {
  up: DerivedMigrationFunction;
  down: DerivedMigrationFunction;
};
