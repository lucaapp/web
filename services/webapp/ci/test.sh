#!/bin/sh
set -euxo pipefail
yarn run lint
yarn run test:ci
yarn run audit