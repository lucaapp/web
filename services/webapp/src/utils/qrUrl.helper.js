import { base64UrlToBytes, decodeUtf8 } from '@lucaapp/crypto';

const isUUID = string => {
  const regexExp = /^[\da-f]{8}(?:\b-[\da-f]{4}){3}\b-[\da-f]{12}$/gi;
  return regexExp.test(string);
};

const isCurrentHostname = string => {
  return window.location.hostname === string;
};

export const isValidLucaQrCode = qrCodeContent => {
  try {
    const url = new URL(qrCodeContent);
    const { pathname, hostname } = url;
    const pathChunks = pathname.split('/');
    if (
      pathChunks.length === 3 &&
      pathChunks[1] === 'webapp' &&
      isCurrentHostname(hostname) &&
      isUUID(pathChunks[2])
    ) {
      return true;
    }
    return false;
  } catch {
    return false;
  }
};

export const extractTableFromUrl = () => {
  const { hash } = window.location;
  if (!hash) return null;

  try {
    const chunks = hash.slice(1).split('/');
    const decodedData = JSON.parse(decodeUtf8(base64UrlToBytes(chunks[0])));
    if (!decodedData.Table && !decodedData.tableId) return null;

    // Support legacy QR codes
    if (decodedData.Table) {
      return { table: decodedData.Table };
    }
    return { tableId: decodedData.tableId };
  } catch {
    return null;
  }
};
