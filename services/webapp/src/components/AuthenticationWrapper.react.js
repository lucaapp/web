import { useCallback, useEffect } from 'react';
import { notification } from 'antd';
import { useIntl } from 'react-intl';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { getLocation } from 'connected-react-router';
import { indexDB } from 'db';
import { HOME_PATH, SETTINGS_PATH } from 'constants/routes';

export function AuthenticationWrapper({ children }) {
  const intl = useIntl();
  const history = useHistory();
  const location = useSelector(getLocation);

  const checkLocation = useCallback(
    appLocation => {
      (async () => {
        if (
          (await indexDB.users.count().catch(() => 0)) === 0 &&
          appLocation.pathname.includes(SETTINGS_PATH)
        ) {
          history.push(HOME_PATH);
        }
      })().catch(() => {
        notification.error({
          message: intl.formatMessage({
            id: 'error.headline',
          }),
          description: intl.formatMessage({
            id: 'error.description',
          }),
        });
      });
    },
    [history, intl]
  );

  useEffect(() => {
    checkLocation(location);
  }, [checkLocation, location]);

  return children;
}
