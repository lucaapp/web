import React from 'react';
import { useIntl } from 'react-intl';
import { AppStoreImage, GooglePlayImage } from 'assets/images';

import {
  StyledContainer,
  StyledHeadline,
  StyledInfoText,
  StyledStoreLink,
  StyledStoreLogo,
  StyledStoreWrapper,
} from './AppUsage.styled';

export const AppUsage = () => {
  const intl = useIntl();

  return (
    <StyledContainer>
      <StyledHeadline>
        {intl.formatMessage({
          id: 'OnBoarding.WebAppWarningStep.Headline',
        })}
      </StyledHeadline>
      <StyledStoreWrapper>
        <StyledStoreLink
          rel="noopener noreferrer"
          href="https://play.google.com/store/apps/details?id=de.culture4life.luca"
        >
          <StyledStoreLogo src={GooglePlayImage} alt="Google Play" />
        </StyledStoreLink>
        <StyledStoreLink
          rel="noopener noreferrer"
          href="https://apps.apple.com/de/app/luca-app/id1531742708"
        >
          <StyledStoreLogo src={AppStoreImage} alt="Apple App Store" />
        </StyledStoreLink>
      </StyledStoreWrapper>
      <StyledInfoText>
        {intl.formatMessage({
          id: 'OnBoarding.WebAppWarningStep.Description',
        })}
      </StyledInfoText>
    </StyledContainer>
  );
};
