import styled from 'styled-components';

export const StyledHeader = styled.div`
  padding: 16px 16px 0 16px;
  margin-bottom: 40px;
`;

export const StyledLucaLogo = styled.img`
  height: 32px;
`;
