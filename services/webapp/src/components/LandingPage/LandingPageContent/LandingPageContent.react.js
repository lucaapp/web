import React from 'react';
import { useQuery } from 'react-query';

import { getScanner } from 'network/api';
import { LocationCheckIn } from 'components/LocationCheckIn/LocationCheckIn.react';
import { extractTableFromUrl } from 'utils/qrUrl.helper';
import { Header } from './Header';
import { AppUsage } from './AppUsage';
import { LocationInfo } from './LocationInfo';

import { StyledContainer } from './LandingPageContent.styled';

export const LandingPageContent = ({ scannerId }) => {
  const {
    data: scanner,
    error: scannerError,
    isLoading: isScannerLoading,
  } = useQuery(['scanner', scannerId], () => getScanner(scannerId), {
    enabled: !!scannerId,
  });
  const tableInfo = extractTableFromUrl();

  if (scannerError || isScannerLoading) return null;

  return (
    <StyledContainer>
      <Header />
      {scanner?.locationId ? (
        <LocationInfo locationId={scanner.locationId} tableInfo={tableInfo} />
      ) : (
        <LocationCheckIn />
      )}
      <AppUsage />
    </StyledContainer>
  );
};
