import React from 'react';
import { useIntl } from 'react-intl';

import {
  MapIcon,
  TimetableIcon,
  WebsiteIcon,
  MoreInfosIcon,
} from 'assets/icons';

import { ReportFraud } from '../ReportFraud';

import {
  Wrapper,
  StyledIcon,
  LinkItemText,
  ClickableLinkWrapper,
  InfoWrapper,
} from './OtherLinksDisplay.styled';

const iconMap = {
  general: MoreInfosIcon,
  map: MapIcon,
  schedule: TimetableIcon,
  website: WebsiteIcon,
};

const getLinkItem = (key, value, intl) => {
  const Icon = iconMap[key];

  return (
    <ClickableLinkWrapper href={value} target="_blank" key={key}>
      <StyledIcon component={Icon} />
      <LinkItemText>
        {intl.formatMessage({
          id: `OnBoarding.WelcomeStep.Link.${key}`,
        })}
      </LinkItemText>
    </ClickableLinkWrapper>
  );
};

export const OtherLinksDisplay = ({ links, locationName }) => {
  const intl = useIntl();
  const linkItems = Object.keys(links)
    .filter(key => key !== 'menu')
    .filter(key => !!links[key])
    .map(key => getLinkItem(key, links[key], intl));

  if (linkItems.length === 0) return null;

  return (
    <>
      <Wrapper>
        <InfoWrapper>{intl.formatMessage({ id: 'infos' })}</InfoWrapper>
        {linkItems}
      </Wrapper>
      <ReportFraud links={links} locationName={locationName} />
    </>
  );
};
