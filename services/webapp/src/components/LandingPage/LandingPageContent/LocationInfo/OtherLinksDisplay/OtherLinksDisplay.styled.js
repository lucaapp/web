import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 24px;
`;

export const ClickableLinkWrapper = styled.a`
  display: flex;
  background: rgba(255, 255, 255, 0.15);
  border-radius: 6px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.15);
  margin-bottom: 16px;
  align-items: center;
`;

export const LinkItemText = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  padding: 20px 0;
`;

export const StyledIcon = styled(Icon)`
  margin: 0 16px;
  font-size: 30px;
`;

export const InfoWrapper = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 8px;
`;
