import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 16px;
`;

export const Divider = styled.div`
  border-bottom: 1px solid rgb(116, 116, 128);
`;

export const Separator = styled.div`
  width: 328px;
  border: 0.5px solid rgb(180 180 199);
`;
