import styled from 'styled-components';

export const ReportLinkMailTo = styled.a`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  text-decoration: underline;
  font-size: 12px;
  font-weight: 500;
  margin-bottom: 40px;
`;
