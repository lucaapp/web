import React from 'react';
import { useIntl } from 'react-intl';
import { getMailTo } from './ReportFraud.helper';
import { ReportLinkMailTo } from './ReportFraud.styled';

export const ReportFraud = ({ links, locationName }) => {
  const intl = useIntl();

  const hasNoLinks = Object.values(links).every(link => !link);

  return (
    <>
      {!hasNoLinks && (
        <ReportLinkMailTo
          href={getMailTo(locationName, links, intl)}
          rel="noopener noreferrer"
        >
          {intl.formatMessage({ id: 'reportAbuse' })}
        </ReportLinkMailTo>
      )}
    </>
  );
};
