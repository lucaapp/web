import React from 'react';
import { useQuery } from 'react-query';

import {
  getLocation,
  getLocationTableById,
  getLocationURLs,
  getPaymentActive,
  getCampaignsForLocation,
} from 'network/api';

import { Wrapper, Divider } from './LocationInfo.styled';
import { LocationNameDisplay } from './LocationNameDisplay';
import { DiscountDisplay } from './DiscountDisplay';
import { MenuLink } from './MenuLink';
import { OpeningHoursDisplay } from './OpeningHoursDisplay';
import { OtherLinksDisplay } from './OtherLinksDisplay';
import { Payment } from './Payment';
import { TableDisplay } from './TableDisplay';

export const LocationInfo = ({ locationId, tableInfo }) => {
  const { data: location } = useQuery(
    ['location', locationId],
    () => getLocation(locationId),
    { enabled: !!locationId }
  );

  const { data: links } = useQuery(
    ['locationURLs', locationId],
    () => getLocationURLs(locationId),
    { enabled: !!locationId }
  );

  const { data: payment } = useQuery(
    ['paymentActive', locationId],
    () => getPaymentActive(locationId),
    { enabled: !!locationId }
  );

  const {
    data: table,
    error: tableError,
    isLoading: isTableLoading,
  } = useQuery('table', () => getLocationTableById(tableInfo.tableId), {
    enabled: !!tableInfo && !!tableInfo.tableId,
  });

  const { data: campaigns } = useQuery(
    'campaigns',
    () => getCampaignsForLocation(locationId),
    {
      enabled: !!locationId,
      retry: false,
    }
  );

  if (!links || !location || tableError || isTableLoading) {
    return null;
  }

  return (
    <Wrapper>
      <LocationNameDisplay location={location} />
      <Divider />
      <TableDisplay tableInfo={tableInfo} table={table} />
      {Array.isArray(campaigns) && <DiscountDisplay campaigns={campaigns} />}
      <MenuLink links={links} locationName={location.name} />
      <Payment
        payment={payment}
        locationId={locationId}
        tableInfo={tableInfo}
        table={table}
      />
      <Divider />
      <OtherLinksDisplay links={links} locationName={location.name} />
      <OpeningHoursDisplay location={location} />
    </Wrapper>
  );
};
