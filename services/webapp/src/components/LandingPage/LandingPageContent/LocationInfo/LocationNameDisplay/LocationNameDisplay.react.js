import React from 'react';
import { useIntl } from 'react-intl';
import {
  LocationNameWrapper,
  LocationName,
} from './LocationNameDisplay.styled';

export const LocationNameDisplay = ({ location }) => {
  const intl = useIntl();

  return (
    <LocationNameWrapper>
      <LocationName>
        {!location.locationName
          ? intl.formatMessage(
              { id: 'welcomeToGroup' },
              { group: location.groupName }
            )
          : intl.formatMessage(
              { id: 'welcomeToLocation' },
              { group: location.groupName, location: location.locationName }
            )}
      </LocationName>
    </LocationNameWrapper>
  );
};
