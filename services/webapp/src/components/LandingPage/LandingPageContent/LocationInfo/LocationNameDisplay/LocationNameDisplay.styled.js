import styled from 'styled-components';

export const LocationName = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 24px;
  font-weight: 500;
`;

export const LocationNameWrapper = styled.div`
  padding-bottom: 24px;
`;
