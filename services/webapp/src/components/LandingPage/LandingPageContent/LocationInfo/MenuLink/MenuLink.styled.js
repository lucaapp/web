import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const ClickableLinkWrapper = styled.a`
  background: rgba(255, 255, 255, 0.15);
  border-radius: 6px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.15);
  margin-top: 24px;
  display: flex;
  align-items: center;
  margin-bottom: 24px;
`;

export const LinkItemText = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  padding: 20px 0;
`;

export const StyledMenuIcon = styled(Icon)`
  margin: 0 16px;
  font-size: 30px;
`;
