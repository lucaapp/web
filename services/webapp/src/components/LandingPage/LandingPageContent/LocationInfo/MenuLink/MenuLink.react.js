import React from 'react';
import { useIntl } from 'react-intl';
import { MenuIcon } from 'assets/icons';

import { ReportFraud } from '../ReportFraud';

import {
  StyledMenuIcon,
  LinkItemText,
  ClickableLinkWrapper,
} from './MenuLink.styled';

const MENU_KEY = 'menu';

export const MenuLink = ({ links, locationName }) => {
  const intl = useIntl();
  const linkItem = links[MENU_KEY];

  if (!linkItem) return null;
  return (
    <>
      <ClickableLinkWrapper href={linkItem} target="_blank">
        <StyledMenuIcon component={MenuIcon} />
        <LinkItemText>
          {intl.formatMessage({
            id: `OnBoarding.WelcomeStep.Link.${MENU_KEY}`,
          })}
        </LinkItemText>
      </ClickableLinkWrapper>
      <ReportFraud links={links} locationName={locationName} />
    </>
  );
};
