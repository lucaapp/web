import styled from 'styled-components';

export const PaymentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 24px;
  padding-bottom: 40px;
`;
