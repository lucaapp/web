import React from 'react';
import { useIntl } from 'react-intl';
import { PAYMENT_PATH } from 'constants/environment';
import { PrimaryButton } from 'components/Buttons';
import { PaymentWrapper } from './Payment.styled';

export const Payment = ({ payment, locationId, tableInfo, table }) => {
  const intl = useIntl();

  const isPaymentAvailable = payment?.paymentActive;

  const openPayWebapp = () =>
    window.open(
      `${PAYMENT_PATH}/${locationId}${
        table ? `?table=${tableInfo.tableId}` : ''
      }`
    );

  return (
    <>
      {isPaymentAvailable && (
        <PaymentWrapper>
          <PrimaryButton onClick={openPayWebapp}>
            {intl.formatMessage({
              id: 'OnBoarding.WelcomeStep.StartPayment',
            })}
          </PrimaryButton>
        </PaymentWrapper>
      )}
    </>
  );
};
