import React, { useMemo } from 'react';
import { useIntl } from 'react-intl';

import { AVAILABLE_CAMPAIGNS } from 'constants/campaigns';

import {
  DiscountWrapper,
  StyledStarOutlinedIcon,
} from './DiscountDisplay.styled';

export const DiscountDisplay = ({ campaigns }) => {
  const intl = useIntl();

  const appliedCampaign = useMemo(() => {
    const discountCampaigns = campaigns
      .filter(campaignItem =>
        campaignItem.campaign.startsWith(AVAILABLE_CAMPAIGNS.LUCA_DISCOUNT)
      )
      .map(discountsCampaign => discountsCampaign.paymentCampaign)
      .sort((a, b) => b.discountPercentage - a.discountPercentage);

    return discountCampaigns[0];
  }, [campaigns]);

  if (!appliedCampaign) {
    return null;
  }

  return (
    <DiscountWrapper>
      <StyledStarOutlinedIcon />
      <div>
        <span>
          {intl.formatMessage(
            { id: 'discount.description' },
            { discount: appliedCampaign.discountPercentage }
          )}
        </span>
        {appliedCampaign.discountMaxAmount && (
          <>
            <br />
            <span>
              {intl.formatMessage(
                { id: 'discount.maximum' },
                { maxAmount: appliedCampaign.discountMaxAmount }
              )}
            </span>
          </>
        )}
      </div>
    </DiscountWrapper>
  );
};
