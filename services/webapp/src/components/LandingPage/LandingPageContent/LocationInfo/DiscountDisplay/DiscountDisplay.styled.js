import styled from 'styled-components';
import { StarOutlined } from '@ant-design/icons';

export const DiscountWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin-top: 24px;
  border-radius: 6px;
  border: 1px #a0d273 solid;
  padding: 12px 16px;

  color: #a0d273;
`;

export const StyledStarOutlinedIcon = styled(StarOutlined)`
  color: #a0d273;
  font-size: 20px;
  margin-top: 5px;
  margin-right: 16px;
`;
