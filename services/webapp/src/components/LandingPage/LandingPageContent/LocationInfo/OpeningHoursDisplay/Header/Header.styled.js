import styled from 'styled-components';

export const Title = styled.p`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 12px;
  font-weight: 500;
`;

export const SubTitle = styled.p`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 12px;
  font-weight: 500;
`;
