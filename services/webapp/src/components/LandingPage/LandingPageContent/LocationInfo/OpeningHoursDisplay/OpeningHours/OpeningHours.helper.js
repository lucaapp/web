const getDayString = (day, intl) =>
  intl.formatMessage({ id: `openingHours.weekday.${day}` });

export const getOpeningHoursList = (openingHours, intl) => {
  const dayKeys = Object.keys(openingHours);

  return dayKeys.flatMap((dayKey, counter) => {
    const hoursList = openingHours[dayKey];
    return Array.isArray(hoursList)
      ? hoursList.map((_, index) => {
          return {
            id: `${counter}${index}`,
            day: index === 0 ? getDayString(dayKey, intl) : '',
            openingHour: openingHours[dayKey][index].openingHours[0],
            closingHour: openingHours[dayKey][index].openingHours[1],
            included: openingHours[dayKey][index].included,
          };
        })
      : [];
  });
};
