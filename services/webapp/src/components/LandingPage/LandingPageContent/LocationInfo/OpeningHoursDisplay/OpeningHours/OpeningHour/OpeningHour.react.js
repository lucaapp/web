import React from 'react';
import { useIntl } from 'react-intl';
import { TimeHourFormatter } from './OpeningHour.helper';

import { Day, Time, Wrapper } from './OpeningHour.styled';

export const OpeningHour = ({ openingHourEntry }) => {
  const intl = useIntl();
  const { day, included, openingHour, closingHour } = openingHourEntry;

  return (
    <Wrapper>
      <Day>{day}</Day>
      <Time>
        {included
          ? `${TimeHourFormatter(openingHour)} - ${TimeHourFormatter(
              closingHour
            )}`
          : intl.formatMessage({ id: 'openingHours.weekday.closed' })}
      </Time>
    </Wrapper>
  );
};
