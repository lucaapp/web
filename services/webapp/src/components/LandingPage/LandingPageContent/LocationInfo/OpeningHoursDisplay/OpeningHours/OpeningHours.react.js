import React from 'react';
import { useIntl } from 'react-intl';

import { OpeningHour } from './OpeningHour';
import { getOpeningHoursList } from './OpeningHours.helper';

import { Wrapper } from './OpeningHours.styled';

export const OpeningHours = ({ openingHours }) => {
  const intl = useIntl();
  const openingHoursList = getOpeningHoursList(openingHours, intl);

  return (
    <Wrapper>
      {openingHoursList.map(openingHourEntry => (
        <OpeningHour
          key={openingHourEntry.id}
          openingHourEntry={openingHourEntry}
        />
      ))}
    </Wrapper>
  );
};
