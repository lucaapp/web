import moment from 'moment';
import { getLanguage } from 'utils/language';

export const TimeHourFormatter = time => {
  if (!time) return '';
  return getLanguage() === 'en'
    ? moment(time, ['HH:mm']).format('hh:mm A')
    : time;
};
