import React from 'react';
import { useIntl } from 'react-intl';

import { TimeHourFormatter } from '../OpeningHours/OpeningHour/OpeningHour.helper';
import { useGetTodaysLastOpeningHours } from './ClosingHoursToday.helper';

import { ClosingHoursTodayTitle } from './ClosingHoursToday.styled';

export const ClosingHoursToday = ({ openingHours }) => {
  const intl = useIntl();

  const lastOpeningHourOfTheDay = useGetTodaysLastOpeningHours(openingHours);

  return (
    <ClosingHoursTodayTitle>
      {intl.formatMessage(
        {
          id: lastOpeningHourOfTheDay
            ? 'openingHours.weekday.closedAt'
            : 'openingHours.weekday.closedToday',
        },
        {
          time: TimeHourFormatter(lastOpeningHourOfTheDay),
        }
      )}
    </ClosingHoursTodayTitle>
  );
};
