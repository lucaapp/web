import moment from 'moment';

const getTodayAsWeekday = () => {
  const dt = moment(moment().locale('en'), 'YYYY-MM-DD');
  return dt.format('ddd').toLowerCase();
};

export const useGetTodaysLastOpeningHours = openingHours => {
  let lastOpeningHourOfTheDay;
  const todaysWeekdayName = getTodayAsWeekday();

  for (const [weekday] of Object.entries(openingHours)) {
    if (todaysWeekdayName === weekday) {
      lastOpeningHourOfTheDay =
        openingHours[weekday][0].included === true
          ? openingHours[weekday][0].openingHours[1]
          : openingHours[weekday][0].included;
    }
  }
  return lastOpeningHourOfTheDay;
};
