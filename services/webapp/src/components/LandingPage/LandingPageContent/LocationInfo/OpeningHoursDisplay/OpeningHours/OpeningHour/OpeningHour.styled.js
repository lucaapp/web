import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 8px;
`;

export const Time = styled.p`
  color: rgb(255, 255, 255);
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
  margin: 0;
`;

export const Day = styled(Time)`
  font-weight: bold;
`;
