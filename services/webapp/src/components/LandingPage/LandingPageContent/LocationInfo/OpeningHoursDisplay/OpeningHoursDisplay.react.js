import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { ClockIcon } from 'assets/icons';

import { Header } from './Header';
import { OpeningHours } from './OpeningHours';
import {
  StyledDrawer,
  Wrapper,
  ClickableLinkWrapper,
  StyledIcon,
  LinkItemText,
} from './OpeningHoursDisplay.styled';
import { ClosingHoursToday } from './ClosingHoursToday';
import { Divider } from '../LocationInfo.styled';

export const OpeningHoursDisplay = ({ location }) => {
  const intl = useIntl();

  const { groupName, locationName: name, openingHours } = location;
  const locationName = name || groupName;

  const [modalOpen, setModalOpen] = useState(false);

  const isOpeningHoursEmpty =
    !openingHours ||
    Object.values(openingHours).every(
      openingHour => !openingHour || !openingHour[0]?.included
    );

  return (
    <>
      {!isOpeningHoursEmpty && (
        <>
          <Divider />
          <Wrapper>
            <ClosingHoursToday openingHours={openingHours} />
            <ClickableLinkWrapper onClick={() => setModalOpen(true)}>
              <StyledIcon component={ClockIcon} />
              <LinkItemText>
                {intl.formatMessage({ id: 'openingHours.weekday.open' })}
              </LinkItemText>
            </ClickableLinkWrapper>
            <StyledDrawer
              placement="bottom"
              onClose={() => setModalOpen(false)}
              visible={modalOpen}
              title=""
            >
              <Header locationName={locationName} />
              <OpeningHours openingHours={openingHours} />
            </StyledDrawer>
          </Wrapper>
          <Divider />
        </>
      )}
    </>
  );
};
