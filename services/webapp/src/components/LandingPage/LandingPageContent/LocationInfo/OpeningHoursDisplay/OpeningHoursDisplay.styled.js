import styled from 'styled-components';
import Icon from '@ant-design/icons';

import { Drawer } from 'antd';

export const StyledDrawer = styled(Drawer).attrs({
  size: 'large',
  height: 'top',
  bodyStyle: {
    backgroundColor: 'black',
    paddingTop: '40px',
  },
  contentWrapperStyle: {
    padding: '40px 8px 0 8px',
  },
})`
  .ant-drawer-content {
    border-radius: 8px 8px 0 0;
  }

  .ant-drawer-close {
    color: #a0c8ff;
  }
`;

export const ClickableLinkWrapper = styled.div`
  background: rgba(255, 255, 255, 0.15);
  border-radius: 6px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.15);
  margin-top: 16px;
  display: flex;
  align-items: center;
`;

export const LinkItemText = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  padding: 20px 0;
`;

export const StyledIcon = styled(Icon)`
  margin: 0 16px;
  font-size: 30px;
`;

export const Wrapper = styled.div`
  margin-top: 24px;
  margin-bottom: 40px;
`;
