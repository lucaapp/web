import styled from 'styled-components';

export const ClosingHoursTodayTitle = styled.p`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
`;
