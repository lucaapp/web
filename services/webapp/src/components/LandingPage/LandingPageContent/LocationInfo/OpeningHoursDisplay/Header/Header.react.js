import React from 'react';
import { useIntl } from 'react-intl';

import { SubTitle, Title } from './Header.styled';

export const Header = ({ locationName }) => {
  const intl = useIntl();

  return (
    <>
      <Title>{locationName}</Title>
      <SubTitle>
        {intl.formatMessage({ id: 'openingHours.weekday.open' })}
      </SubTitle>
    </>
  );
};
