import styled from 'styled-components';

export const Table = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
`;

export const TableNaming = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-left: 24px;
`;

export const TableWrapper = styled.div`
  display: flex;
  margin-top: 24px;
  justify-content: center;
`;
