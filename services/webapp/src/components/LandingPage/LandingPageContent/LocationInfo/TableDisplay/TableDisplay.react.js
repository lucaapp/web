import React from 'react';
import { useIntl } from 'react-intl';
import { TableNaming, Table, TableWrapper } from './TableDisplay.styled';

export const TableDisplay = ({ tableInfo, table }) => {
  const intl = useIntl();

  return (
    <>
      {tableInfo && (
        <TableWrapper>
          <Table>{intl.formatMessage({ id: 'table' })}</Table>
          <TableNaming>
            {tableInfo.table
              ? tableInfo.table
              : table?.customName || table?.internalNumber}
          </TableNaming>
        </TableWrapper>
      )}
    </>
  );
};
