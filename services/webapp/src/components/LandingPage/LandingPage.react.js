import React from 'react';
import { isValidUUID } from 'utils/checkCharacter';

import { FeatureNotImplemented } from 'components/FeatureNotImplemented';
import { LandingPageContent } from './LandingPageContent';

export const LandingPage = ({ match: { params: parameters } }) => {
  const hasScannerId = !!parameters.scannerId;
  const isValidUrl = isValidUUID(parameters.scannerId);

  return (
    <>
      {!hasScannerId || isValidUrl ? (
        <LandingPageContent scannerId={parameters.scannerId} />
      ) : (
        <FeatureNotImplemented />
      )}
    </>
  );
};
