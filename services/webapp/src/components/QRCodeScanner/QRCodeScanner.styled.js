import styled from 'styled-components';
import QrReader from 'modern-react-qr-reader';
import { PrimaryButton } from '../Buttons';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const StyledQRReader = styled(QrReader).attrs({
  style: { width: '100%' },
})`
  > section {
    border: 2px solid rgb(217, 233, 255);
    border-radius: 10px;
    overflow: initial !important;
  }

  > section:before {
    content: '';
    position: absolute;
    top: 30px;
    left: -2px;
    width: calc(100% + 4px);
    height: calc(100% - 60px);
    background: black;
  }

  > section:after {
    content: '';
    position: absolute;
    left: 30px;
    top: -2px;
    height: calc(100% + 4px);
    width: calc(100% - 60px);
    background: black;
  }

  video {
    border-radius: 8px;
    z-index: 3;
  }
`;

export const StyledDescription = styled.div`
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
  text-align: center;
  color: rgb(255, 255, 255);
  margin-top: 1rem;
  width: 100%;
`;
export const StyledFooter = styled.div`
  height: 70px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const InvalidCode = styled.div`
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
  text-align: center;
  color: rgb(255, 255, 255);
  margin-top: 1rem;
`;

export const RescanButton = styled(PrimaryButton).attrs({
  type: 'primary',
  shape: 'round',
  size: 'large',
})`
  width: 100%;
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  background-color: black;
  margin-top: 40px;
  margin-bottom: 40px;
  padding: 16px !important;
  height: 48px !important;
  line-height: 16px;
  text-transform: uppercase;
`;

export const NotSupported = styled.h3`
  color: white;
  padding: 40px 20px;
`;
