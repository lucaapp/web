import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { hasMobileCamAccess } from 'utils/environment';
import { isValidLucaQrCode } from 'utils/qrUrl.helper';
import {
  InvalidCode,
  NotSupported,
  RescanButton,
  StyledDescription,
  StyledQRReader,
  Wrapper,
} from './QRCodeScanner.styled';

export const QRCodeScanner = ({ onScan, onError = () => {} }) => {
  const intl = useIntl();
  const canScanCode = hasMobileCamAccess();
  const [invalid, setInvalid] = useState(false);

  const handleScan = result => {
    if (!result) return;

    if (isValidLucaQrCode(result)) {
      onScan(result);
    } else {
      setInvalid(true);
    }
  };

  const rescan = () => setInvalid(false);

  const invalidCodeElement = () => (
    <>
      <InvalidCode>
        {intl.formatMessage({
          id: 'QRCodeScanner.error.invalidQRCode',
        })}
      </InvalidCode>
      <RescanButton onClick={rescan}>
        {intl.formatMessage({ id: 'QRCodeScanner.RescanButton' })}
      </RescanButton>
    </>
  );

  const readerElement = () => (
    <>
      <StyledQRReader
        onError={onError}
        onScan={handleScan}
        showViewFinder={false}
        constraints={{
          facingMode: { ideal: 'environment' },
        }}
      />
      <StyledDescription>
        {intl.formatMessage({ id: 'QRCodeScanner.description' })}
      </StyledDescription>
    </>
  );

  const notSupportedElement = () => (
    <NotSupported>
      {intl.formatMessage({ id: 'QRCodeScanner.NotSupported' })}
    </NotSupported>
  );

  return (
    <Wrapper>
      {canScanCode
        ? invalid
          ? invalidCodeElement()
          : readerElement()
        : notSupportedElement()}
    </Wrapper>
  );
};
