import React from 'react';
import { AppLayout } from 'components/AppLayout';

import { AppStoreImage, GooglePlayImage } from 'assets/images';
import { LucaLogoWhiteIconSVG, MissingFeatureIconSVG } from 'assets/icons';

import { useIntl } from 'react-intl';
import {
  StyledText,
  StyledContent,
  StyledHeadline,
  StyledLucaLogo,
  StyledStoreLogo,
  StyledStoreLink,
  StyledStoreWrapper,
  StyledMissingFeatureLogo,
} from './FeatureNotImplemented.styled';

export function FeatureNotImplemented() {
  const intl = useIntl();

  return (
    <AppLayout header={<StyledLucaLogo src={LucaLogoWhiteIconSVG} />}>
      <StyledContent>
        <StyledHeadline>
          {intl.formatMessage({ id: 'FeatureNotImplemented.Headline' })}
        </StyledHeadline>
        <StyledText>
          {intl.formatMessage({ id: 'FeatureNotImplemented.Description' })}
        </StyledText>
        <StyledMissingFeatureLogo
          src={MissingFeatureIconSVG}
          alt="Missing Feature Logo"
        />
        <StyledStoreWrapper>
          <StyledStoreLink
            rel="noopener noreferrer"
            href="https://apps.apple.com/de/app/luca-app/id1531742708"
          >
            <StyledStoreLogo src={AppStoreImage} alt="Apple App Store" />
          </StyledStoreLink>
          <StyledStoreLink
            rel="noopener noreferrer"
            href="https://play.google.com/store/apps/details?id=de.culture4life.luca"
          >
            <StyledStoreLogo src={GooglePlayImage} alt="Google Play" />
          </StyledStoreLink>
        </StyledStoreWrapper>
      </StyledContent>
    </AppLayout>
  );
}
