import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { notification } from 'antd';

import { QRCodeScanner } from 'components/QRCodeScanner';
import {
  DescriptionText,
  HeaderText,
  StyledDrawer,
  StyledModalHeadline,
  StyledModalParagraph,
  StyledPrimaryButton,
  Wrapper,
} from './LocationCheckIn.styled';

export const LocationCheckIn = () => {
  const intl = useIntl();
  const [modalOpen, setModalOpen] = useState(false);

  const onClose = () => setModalOpen(false);

  const showModal = () => setModalOpen(true);

  const handleQRCodeScan = QRData => {
    if (!QRData) {
      return;
    }

    try {
      const url = new URL(QRData);
      if (url) {
        window.location.replace(url);
      }
    } catch {
      notification.error({
        message: intl.formatMessage({
          id: 'error.headline',
        }),
        description: intl.formatMessage({
          id: 'error.description',
        }),
      });
    }
  };

  return (
    <Wrapper>
      <HeaderText>
        {intl.formatMessage({ id: 'Home.CheckIn.Header' })}
      </HeaderText>
      <DescriptionText>
        {intl.formatMessage({ id: 'Home.CheckIn.Description' })}
      </DescriptionText>
      <StyledPrimaryButton onClick={showModal}>
        {intl.formatMessage({ id: 'Home.CheckIn.CtaButton' })}
      </StyledPrimaryButton>
      <StyledDrawer
        placement="bottom"
        onClose={onClose}
        visible={modalOpen}
        title=""
      >
        <StyledModalHeadline>
          {intl.formatMessage({ id: 'Home.Scanner.Headline' })}
        </StyledModalHeadline>
        <StyledModalParagraph>
          {intl.formatMessage({ id: 'Home.Scanner.Description' })}
        </StyledModalParagraph>
        <QRCodeScanner onScan={handleQRCodeScan} />
      </StyledDrawer>
    </Wrapper>
  );
};
