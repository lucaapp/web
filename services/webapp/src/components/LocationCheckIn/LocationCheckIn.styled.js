import styled from 'styled-components';
import { Drawer } from 'antd';
import { PrimaryButton } from 'components/Buttons';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: start;
  padding: 0px 24px 24px 24px;
  border-bottom: 1px solid rgb(116, 116, 128);
`;

export const StyledPrimaryButton = styled(PrimaryButton)`
  width: 100%;
`;

export const HeaderText = styled.h1`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 24px;
  font-weight: 500;
  padding-bottom: 24px;
  border-bottom: 1px solid rgb(116, 116, 128);
`;

export const DescriptionText = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 40px;
`;

export const StyledDrawer = styled(Drawer).attrs({
  size: 'large',
  height: 'top',
  bodyStyle: {
    backgroundColor: 'black',
    paddingTop: '40px',
  },
  contentWrapperStyle: {
    padding: '40px 8px 0px 8px',
  },
})`
  .ant-drawer-content {
    border-radius: 8px 8px 0px 0px;
  }

  .ant-drawer-close {
    color: #a0c8ff;
  }
`;

export const StyledModalHeadline = styled.h1`
  font-family: Montserrat-Bold, sans-serif;
  font-size: 20px;
  font-weight: bold;
  color: white;
  line-height: 24px;
  padding-bottom: 8px;
  letter-spacing: 0px;
`;

export const StyledModalParagraph = styled.p`
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  color: white;
  line-height: 20px;
  letter-spacing: 0px;
  margin-bottom: 24px;
`;
