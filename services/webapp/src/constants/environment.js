export const VERSION = '03';
export const DEVICE_TYPE = '03';
export const IS_DEV = process.env.NODE_ENV === 'development';
export const API_PATH = process.env.REACT_APP_API_PATH || '/api';
export const PAYMENT_PATH = '/pay';
export const PAYMENT_API_PATH = `${PAYMENT_PATH}/api`;

export const MOBILE_BREAKPOINT = 390;
export const TABLET_BREAKPOINT = 730;
