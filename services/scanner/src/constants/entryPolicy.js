export const NOT_SHARED = 0x01;
export const QUICK_TESTED = 0x02;
export const PCR_TESTED = 0x04;
export const RECOVERED = 0x08;
export const VACCINATED = 0x10;
