import { checkEntryPolicy } from './entryPolicy';

const TEST_CASES = {
  HAVE_NOTHING: 0x00,
  ONLY_QUICK_TESTED: 0x02,
  ONLY_PCR_TESTED: 0x04,
  PCR_TESTED_AND_QUICK_TESTED: 0x06,
  ONLY_RECOVERED: 0x08,
  RECOVERED_AND_QUICK_TESTED: 0x0a,
  RECOVERED_AND_PCR_TESTED: 0x0c,
  RECOVERED_AND_PCR_TESTED_AND_QUICK_TESTED: 0x0e,
  ONLY_VACCINATED: 0x10,
  VACCINATED_AND_QUICK_TESTED: 0x12,
  VACCINATED_AND_PCR_TESTED: 0x14,
  VACCINATED_AND_PCR_TESTED_AND_QUICK_TESTED: 0x16,
  VACCINATED_AND_RECOVERED: 0x18,
  VACCINATED_AND_RECOVERED_AND_QUICK_TESTED: 0x1a,
  VACCINATED_AND_RECOVERED_AND_PCR_TESTED: 0x1c,
  VACCINATED_AND_RECOVERED_AND_PCR_TESTED_AND_QUICK_TESTED: 0x1e,
};

describe('Entry policy', () => {
  test('if the user has nothing', () => {
    expect(checkEntryPolicy('2G', TEST_CASES.HAVE_NOTHING)).toBe(false);
    expect(checkEntryPolicy('3G', TEST_CASES.HAVE_NOTHING)).toBe(false);
    expect(checkEntryPolicy('2GPlus', TEST_CASES.HAVE_NOTHING)).toBe(false);
  });
  test('if the user is only quick tested', () => {
    expect(checkEntryPolicy('2G', TEST_CASES.ONLY_QUICK_TESTED)).toBe(false);
    expect(checkEntryPolicy('3G', TEST_CASES.ONLY_QUICK_TESTED)).toBe(true);
    expect(checkEntryPolicy('2GPlus', TEST_CASES.ONLY_QUICK_TESTED)).toBe(
      false
    );
  });
  test('if the user is only pcr tested', () => {
    expect(checkEntryPolicy('2G', TEST_CASES.ONLY_PCR_TESTED)).toBe(false);
    expect(checkEntryPolicy('3G', TEST_CASES.ONLY_PCR_TESTED)).toBe(true);
    expect(checkEntryPolicy('2GPlus', TEST_CASES.ONLY_PCR_TESTED)).toBe(false);
  });
  test('if the user is pcr tested and quick tested', () => {
    expect(checkEntryPolicy('2G', TEST_CASES.PCR_TESTED_AND_QUICK_TESTED)).toBe(
      false
    );
    expect(checkEntryPolicy('3G', TEST_CASES.PCR_TESTED_AND_QUICK_TESTED)).toBe(
      true
    );
    expect(
      checkEntryPolicy('2GPlus', TEST_CASES.PCR_TESTED_AND_QUICK_TESTED)
    ).toBe(false);
  });
  test('if the user is only recovered', () => {
    expect(checkEntryPolicy('2G', TEST_CASES.ONLY_RECOVERED)).toBe(true);
    expect(checkEntryPolicy('3G', TEST_CASES.ONLY_RECOVERED)).toBe(true);
    expect(checkEntryPolicy('2GPlus', TEST_CASES.ONLY_RECOVERED)).toBe(false);
  });
  test('if the user is recovered and quick tested', () => {
    expect(checkEntryPolicy('2G', TEST_CASES.RECOVERED_AND_QUICK_TESTED)).toBe(
      true
    );
    expect(checkEntryPolicy('3G', TEST_CASES.RECOVERED_AND_QUICK_TESTED)).toBe(
      true
    );
    expect(
      checkEntryPolicy('2GPlus', TEST_CASES.RECOVERED_AND_QUICK_TESTED)
    ).toBe(true);
  });
  test('if the user is recovered and pcr tested', () => {
    expect(checkEntryPolicy('2G', TEST_CASES.RECOVERED_AND_PCR_TESTED)).toBe(
      true
    );
    expect(checkEntryPolicy('3G', TEST_CASES.RECOVERED_AND_PCR_TESTED)).toBe(
      true
    );
    expect(
      checkEntryPolicy('2GPlus', TEST_CASES.RECOVERED_AND_PCR_TESTED)
    ).toBe(true);
  });
  test('if the user is recovered and pcr tested and quick tested', () => {
    expect(
      checkEntryPolicy(
        '2G',
        TEST_CASES.RECOVERED_AND_PCR_TESTED_AND_QUICK_TESTED
      )
    ).toBe(true);
    expect(
      checkEntryPolicy(
        '3G',
        TEST_CASES.RECOVERED_AND_PCR_TESTED_AND_QUICK_TESTED
      )
    ).toBe(true);
    expect(
      checkEntryPolicy(
        '2GPlus',
        TEST_CASES.RECOVERED_AND_PCR_TESTED_AND_QUICK_TESTED
      )
    ).toBe(true);
  });
  test('if the user is only vaccinated', () => {
    expect(checkEntryPolicy('2G', TEST_CASES.ONLY_VACCINATED)).toBe(true);
    expect(checkEntryPolicy('3G', TEST_CASES.ONLY_VACCINATED)).toBe(true);
    expect(checkEntryPolicy('2GPlus', TEST_CASES.ONLY_VACCINATED)).toBe(false);
  });
  test('if the user is vaccinated and quick tested', () => {
    expect(
      checkEntryPolicy(
        '2G',
        TEST_CASES.VACCINATED_AND_PCR_TESTED_AND_QUICK_TESTED
      )
    ).toBe(true);
    expect(
      checkEntryPolicy(
        '3G',
        TEST_CASES.VACCINATED_AND_PCR_TESTED_AND_QUICK_TESTED
      )
    ).toBe(true);
    expect(
      checkEntryPolicy(
        '2GPlus',
        TEST_CASES.VACCINATED_AND_PCR_TESTED_AND_QUICK_TESTED
      )
    ).toBe(true);
  });
  test('if the user is vaccinated and pcr tested', () => {
    expect(checkEntryPolicy('2G', TEST_CASES.VACCINATED_AND_PCR_TESTED)).toBe(
      true
    );
    expect(checkEntryPolicy('3G', TEST_CASES.VACCINATED_AND_PCR_TESTED)).toBe(
      true
    );
    expect(
      checkEntryPolicy('2GPlus', TEST_CASES.VACCINATED_AND_PCR_TESTED)
    ).toBe(true);
  });
  // eslint-disable-next-line sonarjs/no-identical-functions
  test('if the user is vaccinated and pcr tested and quick tested', () => {
    expect(
      checkEntryPolicy(
        '2G',
        TEST_CASES.VACCINATED_AND_PCR_TESTED_AND_QUICK_TESTED
      )
    ).toBe(true);
    expect(
      checkEntryPolicy(
        '3G',
        TEST_CASES.VACCINATED_AND_PCR_TESTED_AND_QUICK_TESTED
      )
    ).toBe(true);
    expect(
      checkEntryPolicy(
        '2GPlus',
        TEST_CASES.VACCINATED_AND_PCR_TESTED_AND_QUICK_TESTED
      )
    ).toBe(true);
  });
  test('if the user is vaccinated and recovered', () => {
    expect(checkEntryPolicy('2G', TEST_CASES.VACCINATED_AND_RECOVERED)).toBe(
      true
    );
    expect(checkEntryPolicy('3G', TEST_CASES.VACCINATED_AND_RECOVERED)).toBe(
      true
    );
    expect(
      checkEntryPolicy('2GPlus', TEST_CASES.VACCINATED_AND_RECOVERED)
    ).toBe(false);
  });
  test('if the user is vaccinated and recovered and quick tested', () => {
    expect(
      checkEntryPolicy(
        '2G',
        TEST_CASES.VACCINATED_AND_RECOVERED_AND_QUICK_TESTED
      )
    ).toBe(true);
    expect(
      checkEntryPolicy(
        '3G',
        TEST_CASES.VACCINATED_AND_RECOVERED_AND_QUICK_TESTED
      )
    ).toBe(true);
    expect(
      checkEntryPolicy(
        '2GPlus',
        TEST_CASES.VACCINATED_AND_RECOVERED_AND_QUICK_TESTED
      )
    ).toBe(true);
  });
  test('if the user is vaccinated and recovered and pcr tested', () => {
    expect(
      checkEntryPolicy('2G', TEST_CASES.VACCINATED_AND_RECOVERED_AND_PCR_TESTED)
    ).toBe(true);
    expect(
      checkEntryPolicy('3G', TEST_CASES.VACCINATED_AND_RECOVERED_AND_PCR_TESTED)
    ).toBe(true);
    expect(
      checkEntryPolicy(
        '2GPlus',
        TEST_CASES.VACCINATED_AND_RECOVERED_AND_PCR_TESTED
      )
    ).toBe(true);
  });
  test('if the user is vaccinated and recovered and pcr tested and quick tested', () => {
    expect(
      checkEntryPolicy(
        '2G',
        TEST_CASES.VACCINATED_AND_RECOVERED_AND_PCR_TESTED_AND_QUICK_TESTED
      )
    ).toBe(true);
    expect(
      checkEntryPolicy(
        '3G',
        TEST_CASES.VACCINATED_AND_RECOVERED_AND_PCR_TESTED_AND_QUICK_TESTED
      )
    ).toBe(true);
    expect(
      checkEntryPolicy(
        '2GPlus',
        TEST_CASES.VACCINATED_AND_RECOVERED_AND_PCR_TESTED_AND_QUICK_TESTED
      )
    ).toBe(true);
  });
});
