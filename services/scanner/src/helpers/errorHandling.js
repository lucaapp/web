import { notification } from 'antd';

export const DECODE_FAILED = 'DECODE_FAILED';
export const TIMESTAMP_OUTDATED = 'TIMESTAMP_OUTDATED';
export const VERSION_NOT_SUPPORTED = 'VERSION_NOT_SUPPORTED';
export const DUPLICATE_CHECKIN = 'DUPLICATE_CHECKIN';
export const WRONG_LOCAL_TIME = 'WRONG_LOCAL_TIME';
export const NO_USER_DATA = 'NO_USER_DATA';
export const CONTACT_DATA_MANDATORY = 'CONTACT_DATA_MANDATORY';
export const BADGES_NOT_SUPPORTED = 'BADGES_NOT_SUPPORTED';

export const errorMessageIds = {
  DECODE_FAILED: 'error.decodeFailed',
  TIMESTAMP_OUTDATED: 'error.timestampOutdated',
  VERSION_NOT_SUPPORTED: 'error.versionNotSupported',
  DUPLICATE_CHECKIN: 'error.duplicateCheckin',
  WRONG_LOCAL_TIME: 'error.wrongLocalTime',
  NO_USER_DATA: 'error.noUserData',
  CONTACT_DATA_MANDATORY: 'error.contactDataMandatory',
  BADGES_NOT_SUPPORTED: 'error.badgesNotSupported',
  GENERIC_ERROR: 'error.genericScanner',
};

export const notifyScanError = (error, intl, onClose = () => {}, className) => {
  const id = errorMessageIds[error] || errorMessageIds.GENERIC_ERROR;
  notification.error({
    message: intl.formatMessage({ id }),
    onClose,
    ...(className && { className }),
  });
};
