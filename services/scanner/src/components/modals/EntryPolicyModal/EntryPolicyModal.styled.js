import styled from 'styled-components';
import { Media } from 'utils/media';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const ButtonRow = styled.div`
  display: flex;
  justify-content: flex-end;
  column-gap: 10px;
  width: 100%;

  ${Media.mobile`
    flex-direction: column;
      justify-content: center;
      align-items: center;
      row-gap: 10px
  `}
`;

export const Info = styled.div`
  font-weight: 600;
  margin-bottom: 24px;
`;
