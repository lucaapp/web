import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Description = styled.div`
  margin-bottom: 24px;
`;

export const Version = styled.span`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0px;
  line-height: 20px;
`;

export const Link = styled.a`
  margin-bottom: 14px;
  color: black;
  text-decoration: underline;
`;
