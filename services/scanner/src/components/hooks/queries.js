import { useQuery } from 'react-query';
import { getAdditionalData, getCurrentCount, getTotalCount } from 'network/api';
import { REFETCH_INTERVAL_MS } from 'constants/timeouts';
import { getValidatedExtractedDailyKey } from 'utils/dailyKey';

export const QUERY_KEYS = {
  CURRENT_CHECKINS_COUNT: 'CURRENT_CHECKINS_COUNT',
  TOTAL_CHECKINS_COUNT: 'TOTAL_CHECKINS_COUNT',
  ADDITIONAL_DATA: 'ADDITIONAL_DATA',
  DAILY_KEY: 'DAILY_KEY',
};

export const useGetCurrentCheckinsCount = scannerAccessId =>
  useQuery(
    [QUERY_KEYS.CURRENT_CHECKINS_COUNT, scannerAccessId],
    () => getCurrentCount(scannerAccessId).then(response => response.json()),
    {
      refetchInterval: REFETCH_INTERVAL_MS,
    }
  );

export const useGetTotalCheckinsCount = scannerAccessId =>
  useQuery(
    [QUERY_KEYS.TOTAL_CHECKINS_COUNT, scannerAccessId],
    () => getTotalCount(scannerAccessId).then(response => response.json()),
    {
      refetchInterval: REFETCH_INTERVAL_MS,
    }
  );

export const useGetAdditionalData = locationId =>
  useQuery([QUERY_KEYS.ADDITIONAL_DATA, locationId], () =>
    getAdditionalData(locationId).then(response => response.json())
  );

export const useDailyKey = () =>
  useQuery(QUERY_KEYS.DAILY_KEY, getValidatedExtractedDailyKey, {
    cacheTime: 0,
    retry: 0,
  });
