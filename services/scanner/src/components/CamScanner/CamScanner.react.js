import React, { useState, useEffect, useLayoutEffect } from 'react';
import { Tick } from 'react-crude-animated-tick';

import { useIntl } from 'react-intl';
import moment from 'moment';
import { Helmet } from 'react-helmet-async';
import { useQueryClient } from 'react-query';
import QrReader from 'modern-react-qr-reader';
import { RedoOutlined } from '@ant-design/icons';
import platform from 'platform';

import { SCAN_TIMEOUT } from 'constants/timeouts';

import { useModal } from 'components/hooks/useModal';

import { Header } from 'components/Header';
import { isMobile } from 'utils/environment';
import { notifyScanError, handleScanData } from 'helpers';
import { reloadFilter } from 'utils/bloomFilter';

import { AdditionalDataModal } from 'components/modals/AdditionalDataModal';
import { EntryPolicyModal } from 'components/modals/EntryPolicyModal';

import { Update } from 'components/Update';
import {
  QUERY_KEYS,
  useGetAdditionalData,
  useGetCurrentCheckinsCount,
  useGetTotalCheckinsCount,
  useDailyKey,
} from 'components/hooks/queries';
import { Banner } from 'components/Banner';
import {
  TopWrapper,
  BottomWrapper,
  CamScannerWrapper,
  CheckinBox,
  Content,
  Count,
  SuccessWrapper,
  QrWrapper,
} from './CamScanner.styled';

export const CamScanner = ({ scanner }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const [openModal, closeModal] = useModal();
  const [isSuccess, setIsSuccess] = useState(false);
  const [latestUpdate, setLatestUpdate] = useState(moment().unix());
  const [canScanCode, setCanScanCode] = useState(true);

  const { isLoading, error: errorDailyKey, data: dailyKey } = useDailyKey();

  const refetch = () => {
    queryClient.invalidateQueries([
      QUERY_KEYS.TOTAL_CHECKINS_COUNT,
      scanner.scannerAccessId,
    ]);
    queryClient.invalidateQueries([
      QUERY_KEYS.CURRENT_CHECKINS_COUNT,
      scanner.scannerAccessId,
    ]);
  };

  const { data: currentCount } = useGetCurrentCheckinsCount(
    scanner.scannerAccessId
  );
  const { data: totalCount } = useGetTotalCheckinsCount(
    scanner.scannerAccessId
  );
  const { data: additionalData } = useGetAdditionalData(scanner.locationId);

  useEffect(() => {
    setLatestUpdate(moment().unix());
  }, [currentCount]);

  const checkForAdditionalData = traceId => {
    if (scanner.tableCount || additionalData?.additionalData?.length > 0) {
      openModal({
        title: intl.formatMessage({
          id: 'modal.additionalData.title',
        }),
        content: (
          <AdditionalDataModal
            scanner={scanner}
            additionalData={additionalData.additionalData}
            traceId={traceId}
            close={closeModal}
          />
        ),
        closable: false,
      });
    }
  };

  const checkAndHandleEntryPolicyManually = (parameterPassDown, version) =>
    openModal({
      title: intl.formatMessage({
        id: 'modal.entryPolicy.title',
      }),
      content: (
        <EntryPolicyModal
          parameters={parameterPassDown}
          close={closeModal}
          version={version}
        />
      ),
      closable: false,
    });

  useLayoutEffect(() => {
    if (platform.os.family === 'iOS' && platform.name !== 'Safari') {
      setCanScanCode(false);
    }
  }, []);

  const handleScan = scanData => {
    handleScanData({
      scanData,
      intl,
      scanner,
      setIsSuccess,
      checkForAdditionalData,
      checkAndHandleEntryPolicyManually,
      refetch,
    });
  };

  useEffect(() => {
    reloadFilter();
  }, []);

  if (isLoading) return null;

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'camScanner.site.title' })}</title>
        <meta
          name="description"
          content={intl.formatMessage({ id: 'camScanner.site.meta' })}
        />
      </Helmet>
      <CamScannerWrapper>
        <Header />
        {errorDailyKey || !dailyKey ? (
          <Banner
            title={intl.formatMessage({ id: 'scanner.banner.noDailyKey' })}
          />
        ) : canScanCode ? (
          <>
            <TopWrapper>
              <QrWrapper>
                {isSuccess ? (
                  <QrReader
                    onError={error => notifyScanError(error, intl)}
                    onScan={() => {}}
                    showViewFinder={false}
                    constraints={{
                      facingMode: { ideal: 'environment' },
                    }}
                  />
                ) : (
                  <QrReader
                    delay={SCAN_TIMEOUT}
                    onError={error => notifyScanError(error, intl)}
                    onScan={handleScan}
                    constraints={{
                      facingMode: { ideal: 'environment' },
                    }}
                  />
                )}
                {isSuccess && (
                  <SuccessWrapper>
                    <Tick size={100} />
                  </SuccessWrapper>
                )}
              </QrWrapper>
              <Update latestUpdate={latestUpdate} />
            </TopWrapper>
            <BottomWrapper>
              <CheckinBox>
                <Content>
                  {intl.formatMessage({
                    id: 'form.checkins',
                  })}
                  {isMobile && <br />}
                  <b>{scanner.name}</b>
                  {intl.formatMessage({
                    id: 'form.checkinsSuffix',
                  })}
                  <Count>
                    {currentCount}/{totalCount}
                    <RedoOutlined
                      style={{ marginLeft: 16, transform: 'rotate(-90deg)' }}
                      onClick={refetch}
                    />
                  </Count>
                </Content>
              </CheckinBox>
            </BottomWrapper>
          </>
        ) : (
          <h3 style={{ color: 'white', padding: '40px 20px' }}>
            {intl.formatMessage({ id: 'error.browserNotSupported' })}
          </h3>
        )}
      </CamScannerWrapper>
    </>
  );
};
